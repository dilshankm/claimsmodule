package com.infoinstech.claimsmodule;

import org.apache.log4j.*;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.data.jpa.repository.config.EnableJpaAuditing;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.security.oauth2.config.annotation.web.configuration.EnableResourceServer;

@SpringBootApplication
@EnableScheduling
@EnableJpaAuditing
@EnableResourceServer
public class ClaimsModuleApplication {

    private static final Logger logger = LogManager.getLogger(ClaimsModuleApplication.class);

    public static void main(String[] args) {

        SpringApplication.run(ClaimsModuleApplication.class, args);
/*
        logger.setLevel(Level.DEBUG);

        logger.debug("Debugging log");
        logger.info("Info log");
        logger.warn("Hey, This is a warning!");
        logger.error("Oops! We have an Error. OK");
        logger.fatal("Damn! Fatal error. Please fix me.");*/

    }
}
