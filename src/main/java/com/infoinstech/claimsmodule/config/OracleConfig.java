//package com.infoinstech.claimsmodule.config;
//
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.beans.factory.annotation.Qualifier;
//import org.springframework.boot.orm.jpa.EntityManagerFactoryBuilder;
//import org.springframework.context.annotation.Bean;
//import org.springframework.context.annotation.Configuration;
//import org.springframework.context.annotation.Primary;
//import org.springframework.core.env.Environment;
//import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
//import org.springframework.jdbc.datasource.DriverManagerDataSource;
//import org.springframework.orm.jpa.JpaTransactionManager;
//import org.springframework.orm.jpa.LocalContainerEntityManagerFactoryBean;
//import org.springframework.transaction.PlatformTransactionManager;
//import org.springframework.transaction.annotation.EnableTransactionManagement;
//
//import javax.persistence.EntityManagerFactory;
//import javax.persistence.PersistenceContext;
//import javax.sql.DataSource;
//
//@Configuration
//@EnableTransactionManagement
//@EnableJpaRepositories(
//        entityManagerFactoryRef = "oracleEntityManagerFactory",
//        transactionManagerRef = "oracleTransactionManager",
//        basePackages = {"com.infoinstech.claimsmodule"}
//)
//public class OracleConfig {
//
//    @Autowired
//    private Environment env;
//
//    @Bean(name = "oracleDataSource")
//    @Primary
//    @Qualifier("oracleDataSource")
//    public DataSource dataSource() {
//        DriverManagerDataSource dataSource = new DriverManagerDataSource();
//        dataSource.setDriverClassName(env.getProperty("ds.oracle.driverClassName"));
//        dataSource.setUrl(env.getProperty("ds.oracle.url"));
//        dataSource.setUsername(env.getProperty("ds.oracle.username"));
//        dataSource.setPassword(env.getProperty("ds.oracle.password"));
//        return dataSource;
//    }
//
//    @PersistenceContext(unitName = "oraclePersistenceUnit")
//    @Bean(name = "oracleEntityManagerFactory")
//    @Primary
//    public LocalContainerEntityManagerFactoryBean oracleEntityManagerFactory(EntityManagerFactoryBuilder builder,
//                                                                             @Qualifier("oracleDataSource") DataSource oracleDataSource) {
//        return builder.dataSource(oracleDataSource).packages("com.infoinstech.claimsmodule").persistenceUnit("oraclePersistenceUnit").build();
//    }
//
//    @Bean(name = "oracleTransactionManager")
//    public PlatformTransactionManager oracleTransactionManager(@Qualifier("oracleEntityManagerFactory") EntityManagerFactory oracleEntityManagerFactory) {
//        return new JpaTransactionManager(oracleEntityManagerFactory);
//    }
//
//
//}
