package com.infoinstech.claimsmodule.config;

import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.oauth2.config.annotation.web.configuration.ResourceServerConfigurerAdapter;
import org.springframework.security.oauth2.config.annotation.web.configurers.ResourceServerSecurityConfigurer;

/**
 * @author dushman.d
 * @version 0.1
 * @date 2020/12.20
 * @copyright © 2010-2019 Informatics Limited. All Rights Reserved
 */

@Configuration
public class ResourceServerEndpointConfig extends ResourceServerConfigurerAdapter {
    @Override
    public void configure(HttpSecurity http) throws Exception {
        http.authorizeRequests().antMatchers("/*").permitAll();
    }


/**
 * this method will pass the client id of the resource server.
 * Note: check the RESOURCE_IDS of OAUTH_CLIENT_DETAILS
 *
 * @param resources
 */

    @Override
    public void configure(ResourceServerSecurityConfigurer resources) {
        resources.resourceId("payment");
    }

}