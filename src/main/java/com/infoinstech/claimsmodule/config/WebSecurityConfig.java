package com.infoinstech.claimsmodule.config;

import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpMethod;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;

@EnableWebSecurity
@Configuration
@EnableGlobalMethodSecurity(prePostEnabled = true)
class WebSecurityConfig extends WebSecurityConfigurerAdapter {


    protected void configure(HttpSecurity http) throws Exception {

        http
                .authorizeRequests()
                .antMatchers("/claims-module/api/**", "/api/support/**", "/api/web/**","/api/reports-data/**" ,"/api/claims-module/**","http://free.currencyconverterapi.com/**","/api/web/DMS/**").permitAll()
                .antMatchers(HttpMethod.OPTIONS).permitAll()
                .anyRequest()
                .fullyAuthenticated()
                .and()
                .httpBasic()
                .and()
                .csrf().disable();

//        http
//                .headers().contentSecurityPolicy("Content-Security-Policy: script-src https://trustedscripts.example.com");

//        http.headers().frameOptions().disable().addHeaderWriter(new StaticHeadersWriter("X-FRAME-OPTIONS", "ALLOW-FROM https://www.ami-insurance.com/en_US/"));
        http.headers().frameOptions().disable();
    }

//    @Bean
//    public EmbeddedServletContainerFactory servletContainerFactory()
//    {
//        TomcatEmbeddedServletContainerFactory factory = new TomcatEmbeddedServletContainerFactory();
//
//        factory.addConnectorCustomizers(new TomcatConnectorCustomizer()
//        {
//            @Override
//            public void customize(Connector connector)
//            {
//                connector.setAttribute("sslProtocols", "TLSv1.1,TLSv1.2");
//                connector.setAttribute("sslEnabledProtocols", "TLSv1.1,TLSv1.2");
//            }
//        });
//
//        return factory;
//    }

//    @Bean
//    public RestTemplate restTemplate() {
//        SimpleClientHttpRequestFactory requestFactory = new SimpleClientHttpRequestFactory();
//
//        Proxy proxy= new Proxy(Proxy.Type.HTTP, new InetSocketAddress("192.168.15.5", 8080));
//        requestFactory.setProxy(proxy);
//
//        return new RestTemplate(requestFactory);
//    }
}
