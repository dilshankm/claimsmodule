package com.infoinstech.claimsmodule.constants;

public enum INTIMATION_PROCESS {

    INITIAL,
    CLAIM_INTIMATION,
    JOB_ASSIGNMENT,
}
