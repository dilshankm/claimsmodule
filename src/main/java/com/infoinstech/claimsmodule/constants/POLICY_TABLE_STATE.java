package com.infoinstech.claimsmodule.constants;

public enum POLICY_TABLE_STATE {

    POLICY_TABLE,
    ENDORSEMENT_TABLE,
    POLICY_HISTORY_TABLE,
    ENDORSMENT_HISTORY_STATE
}
