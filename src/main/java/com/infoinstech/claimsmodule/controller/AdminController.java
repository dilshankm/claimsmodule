package com.infoinstech.claimsmodule.controller;

import com.infoinstech.claimsmodule.domain.model.salesmarketing.SystemUser;
import com.infoinstech.claimsmodule.dto.admin.UpdateUserStatusDTO;
import com.infoinstech.claimsmodule.dto.common.Response;
import com.infoinstech.claimsmodule.exceptions.types.UserCodeNotFoundException;
import com.infoinstech.claimsmodule.services.business.AdminService;
import com.infoinstech.claimsmodule.urls.ADMIN_CONFIGURATION;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("v1/system-users")
public class AdminController {

		@Autowired
		private AdminService adminService;

		@GetMapping()
		public ResponseEntity<List<SystemUser>> getSystemUsers() {
				List<SystemUser> systemUsers = adminService.fetchSystemUsers();
				return new ResponseEntity<List<SystemUser>>(systemUsers, HttpStatus.MULTI_STATUS.OK);
		}

		@PutMapping()
		public ResponseEntity<String> updateUserStatus(@RequestBody UpdateUserStatusDTO updateUserStatusDTO)  {
			if(updateUserStatusDTO.getCode() == null){
				throw new UserCodeNotFoundException("No code for the User");
			}
			return new ResponseEntity<String>(adminService.updateUserStatus(updateUserStatusDTO), HttpStatus.CREATED);
		}




}
