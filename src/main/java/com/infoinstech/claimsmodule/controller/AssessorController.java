package com.infoinstech.claimsmodule.controller;

import com.infoinstech.claimsmodule.domain.model.claims.master.ExternalPersonDevice;
import com.infoinstech.claimsmodule.dto.common.Response;
import com.infoinstech.claimsmodule.dto.common.UploadAssessorChargesRequestDTO;
import com.infoinstech.claimsmodule.dto.deviceregistration.*;
import com.infoinstech.claimsmodule.exceptions.types.AssessorException;
import com.infoinstech.claimsmodule.exceptions.types.NotEnoughInfoException;
import com.infoinstech.claimsmodule.services.business.ExternalPersonService;
import com.infoinstech.claimsmodule.services.business.LossAdjusterService;
import com.infoinstech.claimsmodule.urls.ASSIGN_LOSS_ADJUSTER;
import com.infoinstech.claimsmodule.urls.DEVICE_REGISTRATION;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("v1/assessors")
public class AssessorController {

    @Autowired
    private ExternalPersonService externalPersonService;
    @Autowired
    private LossAdjusterService lossAdjusterService;

    @GetMapping("/unregistered")
    public ResponseEntity<List<Assessor>> getAssessorList() {
        List<Assessor> assessorList = externalPersonService.getUnregisteredAssessors();
        return new ResponseEntity<List<Assessor>>(assessorList, HttpStatus.OK);
    }

    @GetMapping("/registered-devices")
    public ResponseEntity<List<AssessorDevice>> registerAssessorDevice() {
        List<AssessorDevice> assessorDeviceList = externalPersonService.getRegisteredAssessorDeviceList();
        return new ResponseEntity<List<AssessorDevice>>(assessorDeviceList, HttpStatus.OK);
    }

    @GetMapping("/active")
    public ResponseEntity<List<ActiveAssessor>> viewActiveAssessorDevice() {
        List<ActiveAssessor> activeAssessors = externalPersonService.getActiveAssessorDeviceList();
        if (activeAssessors != null) {
            return new ResponseEntity<List<ActiveAssessor> >(activeAssessors, HttpStatus.OK);
        } else {
            throw new AssessorException("No Assessor Available, Please register from Core");
        }
    }

    @GetMapping("/core-assessors")
    public ResponseEntity<List<ActiveAssessor>> viewCoreAssessorsList() {
        List<ActiveAssessor> activeAssessors = externalPersonService.getCoreAssessorsList();
        if(activeAssessors!=null){
            return new ResponseEntity<List<ActiveAssessor>>(activeAssessors, HttpStatus.OK);
        }
        else {
            throw new AssessorException("No Assessor Available, Please register from Corw");
        }
    }

    @PostMapping("/generate-token")
    public ResponseEntity<String> getRegisteredDevicesList(@RequestBody AssessorCode assessorCode) {
        return new ResponseEntity<String>(externalPersonService.registerAssessorDevice(assessorCode.getAssessorCode()), HttpStatus.OK);
    }

    @PutMapping("/unregister-device")
    public ResponseEntity<ExternalPersonDevice> unregisterDevice(@RequestBody AssessorCode assessorCode) {
        ExternalPersonDevice externalPersonDevice = externalPersonService.unregisterAssessorDevice(assessorCode.getAssessorCode());
        return new ResponseEntity<ExternalPersonDevice>(externalPersonDevice, HttpStatus.OK);
    }

    @PutMapping("/device-location")
    public ResponseEntity<Response> updateDeviceLocation(@RequestBody LocationUpdate locationUpdate) {
        Response response = externalPersonService.updateDeviceLocation(locationUpdate);
        return new ResponseEntity<Response>(response, HttpStatus.OK);
    }

    @PostMapping("/charges")
    public  ResponseEntity<Response> createJobAssignment(@RequestBody UploadAssessorChargesRequestDTO uploadAssessorChargesRequestDTO) {
        Response response;
        if (uploadAssessorChargesRequestDTO != null) {
            response = lossAdjusterService.saveLossAdjusterCharges(uploadAssessorChargesRequestDTO);
            return new ResponseEntity<Response>(response, HttpStatus.OK);
        } else {
            throw new NotEnoughInfoException("There is no adequate Information in the Request");
        }
    }

}
