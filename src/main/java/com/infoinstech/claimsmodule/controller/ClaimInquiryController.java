package com.infoinstech.claimsmodule.controller;

import com.infoinstech.claimsmodule.domain.repository.claims.transaction.ClaimIntimationRepository;
import com.infoinstech.claimsmodule.dto.claiminquiry.ClaimInquiryDTO;
import com.infoinstech.claimsmodule.dto.claiminquiry.ClaimSearchDTO;
import com.infoinstech.claimsmodule.dto.claiminquiry.JobAssignmentDTO;
import com.infoinstech.claimsmodule.dto.claiminquiry.NotificationInquiryDTO;
import com.infoinstech.claimsmodule.dto.common.Response;
import com.infoinstech.claimsmodule.domain.model.claims.transaction.ClaimIntimation;
import com.infoinstech.claimsmodule.domain.model.claims.transaction.ClaimNotification;
import com.infoinstech.claimsmodule.exceptions.types.NotEnoughInfoException;
import com.infoinstech.claimsmodule.exceptions.types.NotFoundException;
import com.infoinstech.claimsmodule.services.business.ClaimInquiryService;
import com.infoinstech.claimsmodule.services.business.ClaimNotificationInquiryService;
import com.infoinstech.claimsmodule.urls.CLAIM_INQUIRY;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("v1/claim-inquiry")
public class ClaimInquiryController {

    @Autowired
    private ClaimInquiryService claimInquiryService;

    @Autowired
    private ClaimNotificationInquiryService claimNotificationInquiryService;

    @Autowired
    private ClaimIntimationRepository claimIntimationRepository;

    // Search for Claim Details using Criteria
    @PostMapping()
    public ResponseEntity<Response> searchClaimDetails(@RequestBody ClaimSearchDTO claimSearchDTO) {
        Response response = null;
        if (claimSearchDTO != null) {
            response = claimInquiryService.searchClaimIntimations(claimSearchDTO);
            return new ResponseEntity<Response>(response, HttpStatus.OK);
        } else {
            throw new NotEnoughInfoException("There is no adequate Information in the Request");
        }
    }

    // View Claim Intimation Details
    @PostMapping("/intimations")
    public ResponseEntity<ClaimInquiryDTO> getInspectionDetails(@RequestBody ClaimIntimation claimIntimation) {
        if (claimIntimation != null) {
            ClaimInquiryDTO claimInquiryDTO = claimInquiryService.getClaimInquiryDetails(claimIntimation.getSequenceNo());
            if (claimInquiryDTO == null) {
                throw new NotFoundException("The Claim Sequence No does not Exists");
            } else {
                return new ResponseEntity<ClaimInquiryDTO>(claimInquiryDTO, HttpStatus.OK);
            }
        } else {
            throw new NotEnoughInfoException("There is no adequate Information in the Request");
        }
    }

    // View Claim Notification Details
    @PostMapping("/notifications")
    public ResponseEntity<NotificationInquiryDTO> viewClaimNotificationDetails(@RequestBody ClaimNotification claimNotification) {
        if (claimNotification != null) {
            NotificationInquiryDTO notificationInquiryDTO = claimNotificationInquiryService.getNotificationInquiryDetails(claimNotification.getSequenceNo());
            if (notificationInquiryDTO == null) {
                throw new NotFoundException("The Notification Sequence No does not Exists");
            } else {
                return new ResponseEntity<NotificationInquiryDTO>(notificationInquiryDTO, HttpStatus.OK);
            }
        } else {
            throw new NotEnoughInfoException("There is no adequate Information in the Request");
        }
    }

    // Get Job Assignments for a Claim Intimation
    @PostMapping("/job-assignments")
    public ResponseEntity<List<JobAssignmentDTO>> getJobAssignments(@RequestBody ClaimIntimation intimation) {
        if (intimation != null) {
            ClaimIntimation claimIntimation = claimIntimationRepository.findBySequenceNo(intimation.getSequenceNo());
            List<JobAssignmentDTO> jobAssignments = claimInquiryService.getJobAssignments(claimIntimation);
            return new ResponseEntity<List<JobAssignmentDTO>>(jobAssignments, HttpStatus.OK);
        } else {
            throw new NotEnoughInfoException("There is no adequate Information in the Request");
        }
    }


    // Get Notification Assignments for a Claim Notification
    @PostMapping("/notification-assignments")
    public ResponseEntity<List<JobAssignmentDTO>> getNotificationAssignments(@RequestBody ClaimNotification claimNotification) {
        if (claimNotification != null) {
            List<JobAssignmentDTO> notificationAssignments = claimNotificationInquiryService.getNotificationAssignments(claimNotification.getSequenceNo());
            return new ResponseEntity<List<JobAssignmentDTO>>(notificationAssignments, HttpStatus.OK);
        } else {
            throw new NotEnoughInfoException("There is no adequate Information in the Request");
        }
    }
}
