package com.infoinstech.claimsmodule.controller;

import com.infoinstech.claimsmodule.dto.claimintimation.ClaimIntimationDTO;
import com.infoinstech.claimsmodule.dto.claimintimation.ClaimIntimationResponseDTO;
import com.infoinstech.claimsmodule.dto.claimintimation.DebitOutstandingDTO;
import com.infoinstech.claimsmodule.dto.claimintimation.RiskValidationDTO;
import com.infoinstech.claimsmodule.dto.claimnotification.NotificationConversionDTO;
import com.infoinstech.claimsmodule.dto.common.ClaimHistoryDTO;
import com.infoinstech.claimsmodule.dto.common.ClaimHistoryRequestDTO;
import com.infoinstech.claimsmodule.dto.common.ReferenceDTO;
import com.infoinstech.claimsmodule.dto.common.Response;
import com.infoinstech.claimsmodule.services.business.ClaimIntimationService;
import com.infoinstech.claimsmodule.services.business.ClaimNotificationService;
import com.infoinstech.claimsmodule.services.reference.ReinsuranceReferenceService;
import com.infoinstech.claimsmodule.services.validation.ClaimIntimationValidation;
import com.infoinstech.claimsmodule.urls.CLAIM_INTIMATION;
import org.json.HTTP;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.Date;
import java.util.List;

@RestController
@RequestMapping("v1/claim-intimations")
public class ClaimIntimationController {

    @Autowired
    private ClaimIntimationValidation claimIntimationValidation;

    @Autowired
    private ClaimIntimationService claimIntimationService;
    @Autowired
    private ClaimNotificationService claimNotificationService;
    @Autowired
    private ReinsuranceReferenceService reinsuranceReferenceService;

    // Check Debit Outstanding Amount for
    @GetMapping("/outstanding")
    //public ResponseEntity<String> checkDebitOutstandingExists(@RequestBody DebitOutstandingDTO dto) {
    public ResponseEntity<String> checkDebitOutstandingExists(@RequestParam(name = "customer_code") String customerCode,
                                                              @RequestParam(name = "policy_number") String policyNo) {
        Boolean exists = claimIntimationValidation.checkDebitOutstandingExists(customerCode, policyNo);
        String status = exists ? "YES" : "NO";
        return new ResponseEntity<String>(status, HttpStatus.OK);
    }

    // Validate the Risk (Vehicle No) based on the Policy period and the Loss Dat
    @PostMapping(value = "/validate-risk")
    public ResponseEntity<Response> validateRisk(@RequestBody RiskValidationDTO riskValidationDTO) {
        Boolean status = claimIntimationService.validateRisk(riskValidationDTO);
        return new ResponseEntity<Response>(new Response(200, status, "Valid Loss Date"), HttpStatus.OK);
    }

    // Save the Claim Intimation Details
    @PostMapping()
    public ResponseEntity<ClaimIntimationResponseDTO> intimateClaim(@RequestBody ClaimIntimationDTO claimIntimationDTO) {
        ClaimIntimationResponseDTO claimIntimationResponseDTO = claimIntimationService.intimateClaim(claimIntimationDTO);
        return new ResponseEntity<ClaimIntimationResponseDTO>(claimIntimationResponseDTO, HttpStatus.OK);
    }

    @GetMapping(value = "/history")
    public ResponseEntity<List<ClaimHistoryDTO>> viewClaimHistory(@RequestParam(name = "policy-sequence-no") String policySequenceNo,
                                                                  @RequestParam(name = "risk-sequence-no") String riskSequenceNo) {
        ClaimHistoryRequestDTO claimRequest = new ClaimHistoryRequestDTO();
        claimRequest.setPolicySequenceNo(policySequenceNo);
        claimRequest.setRiskSequenceNo(riskSequenceNo);
        List<ClaimHistoryDTO> claimHistory = claimIntimationService.viewClaimHistoryDetails(claimRequest);
        return new ResponseEntity<List<ClaimHistoryDTO>>(claimHistory, HttpStatus.OK);
    }

    @PostMapping(value = "/convert-notification")
    private ResponseEntity<String> convertClaimNotificationToClaimIntimation(@RequestBody NotificationConversionDTO notificationConversionDTO) {
        if (notificationConversionDTO != null) {
            return new ResponseEntity<String>(claimNotificationService.convertClaimNotification(notificationConversionDTO), HttpStatus.OK);
        } else {
            return new ResponseEntity<String>("There is no adequate information", HttpStatus.NO_CONTENT);
        }
    }

    @GetMapping("/events")
    private ResponseEntity<List<ReferenceDTO>> events(@RequestParam(name = "loss-date")  String lossDate,
                                                      @RequestParam(name = "intimate-date")  String intimationDate) {
        List<ReferenceDTO> events = reinsuranceReferenceService.getEventsForClaimIntimation(lossDate, intimationDate);
        return new ResponseEntity<List<ReferenceDTO>>(events, HttpStatus.OK);
    }

}
