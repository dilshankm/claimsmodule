package com.infoinstech.claimsmodule.controller;

import com.infoinstech.claimsmodule.dto.claimnotification.ClaimNotificationDTO;
import com.infoinstech.claimsmodule.dto.claimnotification.CoverNoteValidationDTO;
import com.infoinstech.claimsmodule.dto.claimnotification.VehicleNoDTO;
import com.infoinstech.claimsmodule.dto.common.Response;
import com.infoinstech.claimsmodule.exceptions.types.NotEnoughInfoException;
import com.infoinstech.claimsmodule.services.business.ClaimNotificationService;
import com.infoinstech.claimsmodule.services.validation.ClaimNotificationValidation;
import com.infoinstech.claimsmodule.urls.CLAIM_NOTIFICATION;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("v1/claim-notifications")
public class ClaimNotificationController {

    @Autowired
    private ClaimNotificationService claimNotificationService;
    @Autowired
    private ClaimNotificationValidation claimNotificationValidation;


    @PostMapping
    public ResponseEntity<Response> saveNotification(@RequestBody ClaimNotificationDTO claimNotificationDTO) {
        Response response = null;
        if (claimNotificationDTO != null) {
            response = claimNotificationService.notifyClaim(claimNotificationDTO);
            return new ResponseEntity<Response>(response, HttpStatus.OK);
        } else {
            throw new NotEnoughInfoException("There is no adequate Information in the Request");
        }
    }

    // Validate Cover Note and Loss Date for duplication
    @RequestMapping("/validate-cover-note")
    public ResponseEntity<Response> validateCoverNoteNoAndLossDate(@RequestBody CoverNoteValidationDTO coverNoteValidationDTO) {
        Response response = null;
        if (coverNoteValidationDTO != null) {
            response = claimNotificationValidation.validateCoverNoteNoAndLossDateForDuplication(coverNoteValidationDTO);
            return new ResponseEntity<Response>(response, HttpStatus.OK);
        } else {
            throw new NotEnoughInfoException("There is no adequate Information in the Request");
        }
    }

    @PostMapping("/validate-vehicle-no")
    public  ResponseEntity<Response> validateVehicleNoFormat(@RequestBody VehicleNoDTO vehicleNoDTO) {
        Response response= null ;
        if (vehicleNoDTO != null) {
            response = claimNotificationValidation.validateVehicleNo(vehicleNoDTO);
            return new ResponseEntity<Response>(response, HttpStatus.OK);
        } else {
            throw new NotEnoughInfoException("There is no adequate Information in the Request");
        }
    }

}
