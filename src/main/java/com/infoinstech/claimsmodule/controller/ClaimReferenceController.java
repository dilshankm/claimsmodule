package com.infoinstech.claimsmodule.controller;

import com.infoinstech.claimsmodule.dto.claimreference.PolicyPerilsRequestDTO;
import com.infoinstech.claimsmodule.dto.common.ProductCodeDTO;
import com.infoinstech.claimsmodule.dto.common.ReferenceCode;
import com.infoinstech.claimsmodule.dto.common.ReferenceDTO;
import com.infoinstech.claimsmodule.dto.common.Response;
import com.infoinstech.claimsmodule.domain.model.claims.reference.RefInformationMode;
import com.infoinstech.claimsmodule.domain.model.claims.reference.RefLossCause;
import com.infoinstech.claimsmodule.domain.model.claims.reference.RefSurveyType;
import com.infoinstech.claimsmodule.domain.repository.claims.reference.RefInformationModeRepository;
import com.infoinstech.claimsmodule.exceptions.types.NotEnoughInfoException;
import com.infoinstech.claimsmodule.exceptions.types.NotFoundException;
import com.infoinstech.claimsmodule.services.business.LossAdjusterService;
import com.infoinstech.claimsmodule.services.reference.ClaimsReference;
import com.infoinstech.claimsmodule.urls.ASSIGN_LOSS_ADJUSTER;
import com.infoinstech.claimsmodule.urls.CLAIM_INTIMATION;
import com.infoinstech.claimsmodule.urls.CLAIM_NOTIFICATION;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("v1/claim-reference")
public class ClaimReferenceController {

    @Autowired
    private ClaimsReference claimsReference;
    @Autowired
    private RefInformationModeRepository refInformationModeRepository;

    @Autowired
    private LossAdjusterService lossAdjusterService;

    @GetMapping("/causes-of-loss")
    public ResponseEntity<List<RefLossCause>> getLossCaueses(@RequestParam(name = "product_code") String productCode) {
        List<RefLossCause> refLossCauses = claimsReference.getProductWiseLossCauses(productCode);
        if (refLossCauses == null) {
            throw new NotFoundException("The Product does not exists");
        }
        return new ResponseEntity<List<RefLossCause>>(refLossCauses, HttpStatus.OK);
    }

    @GetMapping("/modes-of-information")
    public ResponseEntity<List<RefInformationMode>> getListofIntimationModes() {
        List<RefInformationMode> refInformationModes = claimsReference.getModesOfInformation();
        return new ResponseEntity< List<RefInformationMode>>(refInformationModes, HttpStatus.OK);
    }

    @GetMapping("/relationship-types")
    public ResponseEntity< List<ReferenceDTO>> getRelationshipToInsuredTypes() {
        List<ReferenceDTO> refInformationModes = claimsReference.getRelationshipTypesToInsured();
        return new ResponseEntity<List<ReferenceDTO>>(refInformationModes, HttpStatus.OK);
    }

    @GetMapping("/types-of-survey")
    public ResponseEntity<Response> getSurveyTypes() {
        Response response = null;
        List<RefSurveyType> refSurveyTypeList = claimsReference.getJobAssignmentTypes();
        response = new Response(200, true, refSurveyTypeList);
        return new ResponseEntity<Response>(response, HttpStatus.OK);
    }

    @GetMapping("/charge-types")
    public ResponseEntity<Response>  getUnitTypes() {
        Response response = null;
        List<ReferenceDTO> relationshipToInsuredTypes = lossAdjusterService.getAssessorChargeTypes();
        response = new Response(200, true, relationshipToInsuredTypes);
        return new ResponseEntity<Response>(response, HttpStatus.OK);
    }

    @GetMapping("/unit-types")
    public ResponseEntity<Response> getAssessorChargeTypes() {
        Response response = null;
        List<ReferenceDTO> unitTypes = lossAdjusterService.getUnitTypes();
        response = new Response(200, true, unitTypes);
        return new ResponseEntity<Response>(response, HttpStatus.OK);
    }

    @GetMapping("/claim-handlers")
    public ResponseEntity<Response> getClaimHandlers() {
        Response response = null;
        List<ReferenceDTO> claimHandlers = claimsReference.getClaimHandlers();
        response = new Response(200, true, claimHandlers);
        return new ResponseEntity<Response>(response, HttpStatus.OK);
    }

    @GetMapping("/cover-types")
    public ResponseEntity<Response> getCoverTypes() {
        Response response = null;
        List<ReferenceDTO> coverTypes = claimsReference.getCoverTypes();
        response = new Response(200, true, coverTypes);
        return new ResponseEntity<Response>(response, HttpStatus.OK);
    }

    @GetMapping("/provision-types")
    public  ResponseEntity<Response>  getProvisionTypes() {
        Response response = null;
        List<ReferenceDTO> provisionTypes = claimsReference.getProvisionTypes();
        response = new Response(200, true, provisionTypes);
        return new ResponseEntity<Response>(response, HttpStatus.OK);
    }

    @PostMapping("/policy-perils")
    public ResponseEntity<Response> getPolicyPerils(@RequestBody PolicyPerilsRequestDTO policyPerilsRequestDTO) {
        List<ReferenceDTO> policyPerils = null;
        Response response = null;
        if (policyPerilsRequestDTO != null) {
            policyPerils = claimsReference.getPolicyPerils(policyPerilsRequestDTO);
            response = new Response(200, true, policyPerils);
            return new ResponseEntity<Response>(response, HttpStatus.OK);
        } else{
            throw new NotEnoughInfoException("There is no adequate Information in the Request");
        }
    }

    @GetMapping("/claim-number")
    public ResponseEntity<Response> getClaimNos() {
        List<String> claimNos = claimsReference.getClaimNos();
        Response response = new Response(200, true, claimNos);
        return new ResponseEntity<Response>(response, HttpStatus.OK);
    }

    @GetMapping("/policy-number")
    public ResponseEntity<Response> getPolicyNos() {
        List<String> policyNos = claimsReference.getPolicyNos();
        Response response = new Response(200, true, policyNos);
        return new ResponseEntity<Response>(response, HttpStatus.OK);
    }

    @GetMapping("/risk-name")
    public ResponseEntity<Response> getRiskNames() {
        List<String> vehicleNos = claimsReference.getRiskNames();
        Response response = new Response(200, true, vehicleNos);
        return new ResponseEntity<Response>(response, HttpStatus.OK);
    }

    @GetMapping("/customer-names")
    public ResponseEntity<Response>  getCustomerNames() {
        List<String> customerNames = claimsReference.getCustomerNames();
        Response response = new Response(200, true, customerNames);
        return new ResponseEntity<Response>(response, HttpStatus.OK);
    }

    @GetMapping("/classes")
    public ResponseEntity<Response> getClassNames() {
        List<ReferenceDTO> vehicleNos = claimsReference.getClassNames();
        Response response = new Response(200, true, vehicleNos);
        return new ResponseEntity<Response>(response, HttpStatus.OK);
    }

    @RequestMapping("/products/{code}")
    public ResponseEntity<Response> getProducts(@PathVariable("code") String code){
        List<ReferenceDTO> products = null;
        Response response = null;
        if(code!=null){
            products = claimsReference.getProducts(code);
            response = new Response(200, true, products);
            return new ResponseEntity<Response>(response, HttpStatus.OK);
        }
        else {
            throw new NotEnoughInfoException("There is no adequate Information in the Request");
        }
    }

    @GetMapping("/assessors")
    public ResponseEntity<Response> getAssessorList() {
        Response response = null;
        List<ReferenceDTO> assessorList = claimsReference.getAssessorList();
        if(assessorList!=null){
            response = new Response(200, true, assessorList);
            return new ResponseEntity<Response>(response, HttpStatus.OK);
        }
        else {
            throw new NotFoundException("No references available");
        }
    }

    @GetMapping("/actions-taken")
    public ResponseEntity<Response> getActionTakenList() {
        Response response = null;
        List<ReferenceDTO> actionTakenList = claimsReference.getActionTakenList();
        if(actionTakenList!=null){
            response = new Response(200, true, actionTakenList);
            return new ResponseEntity<Response>(response, HttpStatus.OK);
        }
        else {
            throw new NotFoundException("No references available");
        }
    }

    @GetMapping("/branches")
    public ResponseEntity<Response> getBranchesList() {
        Response response = null;
        List<ReferenceDTO> assessorList = claimsReference.getBranches();
        if(assessorList!=null){
            response = new Response(200, true, assessorList);
            return new ResponseEntity<Response>(response, HttpStatus.OK);
        }
        else {
            throw new NotFoundException("No references available");
        }
    }

    @GetMapping("/intermediary-types")
    public ResponseEntity<List<ReferenceDTO>> getIntermediaryTypes() {
        List<ReferenceDTO> intermediaryTypes = claimsReference.getIntermediaryTypes();
        if(intermediaryTypes!=null){
            return new ResponseEntity<List<ReferenceDTO>>(intermediaryTypes, HttpStatus.OK);
        }
        else {
            throw new NotFoundException("No references available");
        }
    }

    @PostMapping("/intermediaries/{code}")
    public ResponseEntity<Response> getIntermediaries(@PathVariable("code") String code) {
        Response response = null;
        List<ReferenceDTO> intermediaries = claimsReference.getIntermediaries(code);
        if(intermediaries!=null){
            response = new Response(200, true, intermediaries);
            return new ResponseEntity<Response>(response, HttpStatus.OK);
        }
        else {
            throw new NotFoundException("No references available");
        }
    }

}
