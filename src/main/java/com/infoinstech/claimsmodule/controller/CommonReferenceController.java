package com.infoinstech.claimsmodule.controller;

import com.infoinstech.claimsmodule.dto.common.ReferenceDTO;
import com.infoinstech.claimsmodule.dto.common.Response;
import com.infoinstech.claimsmodule.exceptions.types.NotEnoughInfoException;
import com.infoinstech.claimsmodule.exceptions.types.NotFoundException;
import com.infoinstech.claimsmodule.services.reference.CommonReferenceService;
import com.infoinstech.claimsmodule.urls.CLAIM_NOTIFICATION;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("v1/common-reference")
public class CommonReferenceController {

    @Autowired
    private CommonReferenceService commonReferenceService;

    @GetMapping("/vehicle-make-models")
    public ResponseEntity<Response> getVehicleMakeAndModelReferences() {
        Response response = null;
        List<ReferenceDTO> vehicleMakeAndModels = commonReferenceService.getVehicleMakeAndModels();
        if (vehicleMakeAndModels != null) {
            response = new Response(200, true, vehicleMakeAndModels);
            return new  ResponseEntity<Response>(response, HttpStatus.OK);
        } else {
            throw new NotFoundException("No references available, Create first");
        }
    }
}
