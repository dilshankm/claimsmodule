package com.infoinstech.claimsmodule.controller;

import com.infoinstech.claimsmodule.dto.common.Response;
import com.infoinstech.claimsmodule.dto.dashboard.AccidentDTO;
import com.infoinstech.claimsmodule.dto.dashboard.DashboardRequestDTO;
import com.infoinstech.claimsmodule.exceptions.types.NotEnoughInfoException;
import com.infoinstech.claimsmodule.services.business.DashboardService;
import com.infoinstech.claimsmodule.urls.ENG_DASHBOARD;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("v1/dashboard")
public class DashboardController {

    @Autowired
    private DashboardService dashboardService;

    // Search for Claim Details using Criteria
    @PostMapping("/assignments-summary")
    public ResponseEntity<Response> getDashboardSummary(@RequestBody DashboardRequestDTO dashboardRequestDTO) {
        Response response = null;
        if (dashboardRequestDTO != null) {
            response = new Response(200, true, dashboardService.getDashboardDetails(dashboardRequestDTO));
            return new ResponseEntity<Response>(response, HttpStatus.OK);
        } else {
            throw new NotEnoughInfoException("There is no adequate Information in the Request");
        }
    }

    // Search for Claim Details using Criteria
    @GetMapping("/active-accidents")
    public ResponseEntity< List<AccidentDTO>> getActiveAccidents() {
        List<AccidentDTO> activeAccidents = dashboardService.getActiveAccidents();
        return new ResponseEntity< List<AccidentDTO>>(activeAccidents, HttpStatus.OK);
    }
}
