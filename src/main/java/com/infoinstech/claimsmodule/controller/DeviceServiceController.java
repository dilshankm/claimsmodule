package com.infoinstech.claimsmodule.controller;

import com.infoinstech.claimsmodule.dto.common.*;
import com.infoinstech.claimsmodule.dto.dashboard.AccidentDTO;
import com.infoinstech.claimsmodule.exceptions.types.NotEnoughInfoException;
import com.infoinstech.claimsmodule.exceptions.types.NotFoundException;
import com.infoinstech.claimsmodule.services.business.ExternalPersonService;
import com.infoinstech.claimsmodule.services.business.JobAssignmentService;
import com.infoinstech.claimsmodule.services.business.VehicleSketchService;
import com.infoinstech.claimsmodule.services.reference.ClaimsReference;
import com.infoinstech.claimsmodule.urls.CLAIM_INTIMATION;
import com.infoinstech.claimsmodule.urls.MOBILE_API;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("v1/device")
public class DeviceServiceController {

    @Autowired
    private JobAssignmentService jobAssignmentService;
    @Autowired
    private ExternalPersonService externalPersonService;
    @Autowired
    private ClaimsReference claimsReference;
    @Autowired
    private VehicleSketchService vehicleSketchService;

    //Check the connection Android device with the server
    @RequestMapping("/test-connection")
    public ResponseEntity<Response> getConnection() {
        Response response =  new Response(200,true,true);
        return new ResponseEntity<Response>(response, HttpStatus.OK);
    }

    // Update Job Assignment Status
    @PostMapping("/job-assignment-status")
    public ResponseEntity<Response> update(@RequestBody AssignmentStatusDTO assignmentStatusDTO) {
        Response response = jobAssignmentService.updateJobAssignmentStatus(assignmentStatusDTO);
        return new ResponseEntity<>(response, HttpStatus.OK);
    }

    @PostMapping("/view-assignments-summary")
    public ResponseEntity<Response> getJobAssignmentSummary(@RequestBody AssignmentSummaryRequestDTO assignmentSummaryRequestDTO) {
        Response response = new Response();
        if (assignmentSummaryRequestDTO != null) {
            response = jobAssignmentService.viewJobAssignmentsSummary(assignmentSummaryRequestDTO);
            return new ResponseEntity<Response>(response, HttpStatus.OK);
        } else {
            throw new NotEnoughInfoException("There is no adequate Information in the Request");
        }
    }


    //Get Automotive Details
    @GetMapping("/automotive-details")
    public ResponseEntity<Response> getAutomotiveDetails() {
        List<AutoMotiveDetailsDTO> autoMotiveDetailsDTOList = externalPersonService.getAutoMotiveDetails();
        Response response = new Response(200, true, autoMotiveDetailsDTOList);
        return new ResponseEntity<Response>(response, HttpStatus.OK);
    }

    @GetMapping("/vehicle-sketches")
    public ResponseEntity<Response> getVehicleSketches() {
        List<ReferenceDTO> vehicleSketchReferences = claimsReference.getVehicleSketches();
        Response response = new Response(200, true, vehicleSketchReferences);
        return new ResponseEntity<Response>(response, HttpStatus.OK);
    }

    @PostMapping("/view-vehicle-sketch/{code}")
    public ResponseEntity<Response> getVehicleSketch(@PathVariable("code") String code) {
        Response response = null;
        if (code != null) {
            ReferenceDTO referenceDTO = vehicleSketchService.getVehicleSketch(code);
            if (code != null) {
                response = new Response(200, true, referenceDTO);
                return new ResponseEntity<Response>(response, HttpStatus.OK);
            } else {
                throw new NotFoundException("There CommonReference Code does not exists");
            }
        } else {
            throw new NotEnoughInfoException("There is no adequate Information in the Request");
        }
    }
}
