package com.infoinstech.claimsmodule.controller;

import com.infoinstech.claimsmodule.dto.common.ReferenceCode;
import com.infoinstech.claimsmodule.dto.common.Response;
import com.infoinstech.claimsmodule.dto.googlemaps.NearbySearchRequest;
import com.infoinstech.claimsmodule.exceptions.types.NotEnoughInfoException;
import com.infoinstech.claimsmodule.services.api.GoogleMapsAPIService;
import com.infoinstech.claimsmodule.urls.CLAIM_INTIMATION;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("v1/google-map")
public class GoogleMapsController {

    @Autowired
    private GoogleMapsAPIService googleMapsAPIService;

    @GetMapping("/places-auto-complete/{code}")
    public ResponseEntity<Response> getPlacesAutoComplete(@PathVariable("code") String code) {
        Response response = null;
        if(code!=null){
            response = googleMapsAPIService.getPlaceAutoComplete(code);
            return new ResponseEntity<Response>(response, HttpStatus.OK);
        }
        else {
            throw new NotEnoughInfoException("There is no adequate Information in the Request");
        }
    }

    @GetMapping("/places/{code}")
    public ResponseEntity<Response> getPlaceDetails(@PathVariable("code") String code) {
        Response response = null;
        if(code!=null){
            response = googleMapsAPIService.getPlaceDetails(code);
            return new ResponseEntity<Response>(response, HttpStatus.OK);
        }
        else {
            throw new NotEnoughInfoException("There is no adequate Information in the Request");
        }
    }

    @GetMapping("/police-stations")
    public ResponseEntity<Response> getNearbyPoliceStations(@RequestParam("lat") String lat, @RequestParam("lng") String lng){
        Response response = null;
        if(lat!=null && lng!=null){
            NearbySearchRequest nearbySearchRequest = new NearbySearchRequest();
            nearbySearchRequest.setLat(lat);
            nearbySearchRequest.setLng(lng);
            response = googleMapsAPIService.searchNearbyPoliceStations(nearbySearchRequest);
            return new ResponseEntity<Response>(response, HttpStatus.OK);
        }
        else {
            throw new NotEnoughInfoException("There is no adequate Information in the Request");
        }
    }
}
