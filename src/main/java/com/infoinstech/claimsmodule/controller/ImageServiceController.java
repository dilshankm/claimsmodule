package com.infoinstech.claimsmodule.controller;

import com.infoinstech.claimsmodule.dto.common.Response;
import com.infoinstech.claimsmodule.dto.imageservice.ImageUploadRequestDTO;
import com.infoinstech.claimsmodule.dto.imageservice.ImagesUploadRequestDTO;
import com.infoinstech.claimsmodule.dto.imageservice.ReadImageRequestDTO;
import com.infoinstech.claimsmodule.exceptions.types.NotEnoughInfoException;
import com.infoinstech.claimsmodule.services.business.ImageService;
import com.infoinstech.claimsmodule.urls.CLAIM_INTIMATION;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("v1/images")
public class ImageServiceController {

    @Autowired
    private ImageService imageService;

    @PostMapping("/upload")
    public ResponseEntity<Response> uploadClaimImages(@RequestBody ImagesUploadRequestDTO imagesUploadRequestDTO) {
        Response response = null;
        if (imagesUploadRequestDTO != null) {
            response = imageService.uploadClaimImages(imagesUploadRequestDTO);
            return new ResponseEntity<Response>(response, HttpStatus.OK);
        } else {
            throw new NotEnoughInfoException("There is no adequate Information in the Request");
        }
    }

    @PostMapping("/image-by-image")
    public ResponseEntity<Response> uploadClaimImageByImage(@RequestBody ImageUploadRequestDTO imageUploadRequestDTO) {
        Response response = null;
        if (imageUploadRequestDTO != null) {
            response = imageService.uploadClaimImageByImage(imageUploadRequestDTO);
            return new ResponseEntity<Response>(response, HttpStatus.OK);
        } else {
            throw new NotEnoughInfoException("There is no adequate Information in the Request");
        }
    }

    @PostMapping("/read")
    public ResponseEntity<Response>  readImages(@RequestBody ReadImageRequestDTO readImageRequestDTO) {
        Response response = null;
        if (readImageRequestDTO != null) {
            response = imageService.viewClaimImages(readImageRequestDTO);
            return new ResponseEntity<Response>(response, HttpStatus.OK);
        } else {
            throw new NotEnoughInfoException("There is no adequate Information in the Request");
        }
    }
}
