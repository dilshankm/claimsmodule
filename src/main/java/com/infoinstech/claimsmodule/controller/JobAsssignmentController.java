package com.infoinstech.claimsmodule.controller;

import com.infoinstech.claimsmodule.dto.claiminquiry.JobAssignmentDTO;
import com.infoinstech.claimsmodule.dto.common.JobNoDTO;
import com.infoinstech.claimsmodule.dto.common.Response;
import com.infoinstech.claimsmodule.dto.jobdetails.JobAssignmentDetails;
import com.infoinstech.claimsmodule.dto.questionnaire.UploadQuestionnaireRequestDTO;
import com.infoinstech.claimsmodule.exceptions.types.NotEnoughInfoException;
import com.infoinstech.claimsmodule.exceptions.types.NotFoundException;
import com.infoinstech.claimsmodule.services.business.JobAssignmentService;
import com.infoinstech.claimsmodule.services.business.QuestionnaireService;
import com.infoinstech.claimsmodule.urls.CLAIM_INTIMATION;
import com.infoinstech.claimsmodule.urls.CLAIM_NOTIFICATION;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("v1/job-assignment")
public class JobAsssignmentController {

    @Autowired
    private JobAssignmentService jobAssignmentService;

    @Autowired
    private QuestionnaireService questionnaireService;

    // Get Job Assignments for a Claim Intimation
    @PostMapping("")
    public ResponseEntity<Response> createJobAssignment(@RequestBody JobAssignmentDTO jobAssignmentDTO) {
        Response response = null;
        if (jobAssignmentDTO != null) {
            response = jobAssignmentService.createJobAssignment(jobAssignmentDTO);
            return new  ResponseEntity<Response>(response, HttpStatus.OK);
        } else {
            throw new NotEnoughInfoException("There is no adequate Information in the Request");
        }
    }

    // Get Job Assignments for a Claim Intimation
    @PostMapping("/notification-assignment")
    public ResponseEntity<Response>  createNotificationAssignment(@RequestBody JobAssignmentDTO jobAssignmentDTO) {
        Response response = null;
        if (jobAssignmentDTO != null) {
            response = jobAssignmentService.createNotificationAssignment(jobAssignmentDTO);
            return new  ResponseEntity<Response>(response, HttpStatus.OK);
        } else {
            throw new NotEnoughInfoException("There is no adequate Information in the Request");
        }
    }

    // Get Job Assignment Details for the given Job No
    @PostMapping("/view-job-assignment")
    public ResponseEntity<Response> viewJobAssignmentDetails(@RequestBody JobNoDTO jobNoDTO) {
        JobAssignmentDetails jobAssignmentDetails = null;
        if (jobNoDTO.getJobNo().substring(0, 2).equals("JB")) {
            jobAssignmentDetails = jobAssignmentService.viewJobAssignment(jobNoDTO.getJobNo());
        } else {
            jobAssignmentDetails = jobAssignmentService.viewNotificationAssignment(jobNoDTO.getJobNo());
        }
        Response response = new Response(200, true, jobAssignmentDetails);
        return new ResponseEntity<Response>(response, HttpStatus.OK);
    }

    // Get Questionnaire Details for the given Job No
    @PostMapping("/view-questionnaire")
    public ResponseEntity<Response> viewQuestionnaireDetails(@RequestBody JobNoDTO jobNoDTO) {
        Response response = null;
        if (jobNoDTO != null) {
            response = jobAssignmentService.getQuestionnaireDetails(jobNoDTO);
            return new  ResponseEntity<Response>(response, HttpStatus.OK);
        } else {
            throw new NotEnoughInfoException("There is no adequate Information in the Request");
        }

    }

    // Get Questionnaire Details for the given Job No
    @PostMapping("/upload-questionnaire")
    public ResponseEntity<Response> uploadQuestionnaireDetails(@RequestBody UploadQuestionnaireRequestDTO uploadQuestionnaireRequestDTO) {
        Response response = null;
        if (uploadQuestionnaireRequestDTO != null) {
            response = questionnaireService.saveQuestionnaire(uploadQuestionnaireRequestDTO);
            return new  ResponseEntity<Response>(response, HttpStatus.OK);
        } else {
            throw new NotEnoughInfoException("There is no adequate Information in the Request");
        }
    }

}
