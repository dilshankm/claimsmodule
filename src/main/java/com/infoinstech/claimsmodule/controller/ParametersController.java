package com.infoinstech.claimsmodule.controller;

import com.infoinstech.claimsmodule.dto.common.Response;
import com.infoinstech.claimsmodule.services.parameters.ClaimParametersService;
import com.infoinstech.claimsmodule.urls.CLAIM_PARAMETERS;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("v1/parameter")
public class ParametersController {

    @Autowired
    private ClaimParametersService claimParametersService;

    @GetMapping("/upload-gallery-image")
    public ResponseEntity<Response> getUploadGalleryImagesParameter() {
        Boolean status = claimParametersService.getUploadGalleryImagesParameter();
        Response response = new Response(200, true, "", status);
        return new ResponseEntity<>(response, HttpStatus.OK);
    }
}
