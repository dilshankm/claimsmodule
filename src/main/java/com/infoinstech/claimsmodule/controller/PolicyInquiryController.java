package com.infoinstech.claimsmodule.controller;

import com.infoinstech.claimsmodule.dto.common.Response;
import com.infoinstech.claimsmodule.dto.common.ViewPolicyRequestDTO;
import com.infoinstech.claimsmodule.dto.policysearch.PolicyDetailsMoreDTO;
import com.infoinstech.claimsmodule.dto.policysearch.PolicySearchDTO;
import com.infoinstech.claimsmodule.exceptions.types.NotEnoughInfoException;
import com.infoinstech.claimsmodule.services.business.PolicyInquiryService;
import com.infoinstech.claimsmodule.urls.POLICY_INQUIRY;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("v1/policy-inquiry")
public class PolicyInquiryController {

    @Autowired
    private PolicyInquiryService policyInquiryService;

    @PostMapping("/policy-details")
    public ResponseEntity<Response> searchPolicyDetails(@RequestBody PolicySearchDTO policySearchDTO) {
        Response response = null;
        if (policySearchDTO != null) {
            response = policyInquiryService.viewPolicySearchDetails(policySearchDTO);
            return new ResponseEntity<Response>(response, HttpStatus.OK);
        } else {
            throw new NotEnoughInfoException("There is no adequate Information in the Request");
        }
    }

    @GetMapping("/policy-details")
    public ResponseEntity<Response> getPolicyDetails(@RequestParam(name = "risk_sequence_no") String riskSequenceNo,
                                     @RequestParam(name = "policy_sequence_no") String policySequenceNo) {
        ViewPolicyRequestDTO viewPolicyRequestDTO = new ViewPolicyRequestDTO();
        viewPolicyRequestDTO.setPolicySequenceNo(riskSequenceNo);
        viewPolicyRequestDTO.setRiskSequenceNo(policySequenceNo);
        Response response=null;
        if (viewPolicyRequestDTO.getPolicySequenceNo() != null && viewPolicyRequestDTO.getRiskSequenceNo() != null) {
            PolicyDetailsMoreDTO policyDetailsMoreDTO = policyInquiryService.getPolicyDetails(viewPolicyRequestDTO);
            response = new Response(200, true, policyDetailsMoreDTO);
            return new ResponseEntity<Response>(response, HttpStatus.OK);
        } else {
            throw new NotEnoughInfoException("There is no adequate Information in the Request");
        }
    }
}
