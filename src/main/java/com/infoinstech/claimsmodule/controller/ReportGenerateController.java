package com.infoinstech.claimsmodule.controller;

import com.infoinstech.claimsmodule.dto.common.Response;
import com.infoinstech.claimsmodule.dto.reports.ClaimNoDTOReport;
import com.infoinstech.claimsmodule.dto.reports.claimRatioReport.ClaimRatioDTO;
import com.infoinstech.claimsmodule.dto.reports.claimregistrationreport.ClaimRegistrationRequestDTO;
import com.infoinstech.claimsmodule.dto.reports.jobassignmentdetailsreport.JobAssignmentReportRequestDTO;
import com.infoinstech.claimsmodule.dto.reports.paymentDetailReport.PaymentRequestDTO;
import com.infoinstech.claimsmodule.services.reporting.ReportGenerationService;
import com.infoinstech.claimsmodule.urls.REPORT_GENERATION;
import net.sf.jasperreports.engine.JRException;
import oracle.jdbc.proxy.annotation.Post;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.ByteArrayResource;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.io.IOException;
import java.text.ParseException;
import java.util.Objects;

/**
 * Created by dushman on 3/20/18.
 */


@RestController
@RequestMapping("v1/report-generator")
public class ReportGenerateController {

    @Autowired
    private ReportGenerationService reportGenerationService;
    private ResponseEntity responseEntity;
    private ByteArrayResource resource;
    private Response response;

    @PostMapping("/claim-registration-sheet")
    public ResponseEntity claimRegistrationSheet(@RequestBody ClaimNoDTOReport claimNoDTOReport) throws JRException, ClassNotFoundException, IOException {
        response = reportGenerationService.generateClaimRegistrationSheet(claimNoDTOReport);
        HttpHeaders headers = new HttpHeaders();
        if (response.getStatus() == Boolean.TRUE) {
            if (Objects.equals(claimNoDTOReport.getReportType(), "pdf")) {
                responseEntity = ResponseEntity.ok()
                        .headers(headers)
                        .contentType(MediaType.parseMediaType("application/pdf"))
                        .body(response.getData());
            } else if (Objects.equals(claimNoDTOReport.getReportType(), "xls")) {
                responseEntity = ResponseEntity.ok()
                        //.contentType(MediaType.parseMediaType("application/vnd.ms-excel"))
                        .header("Content-Disposition", "attachment; filename=" + "testXLS")
                        .contentType(MediaType.parseMediaType("application/csv"))
                        .body(response.getData());
            }
        } else {
            responseEntity = ResponseEntity.ok()
                    .headers(headers)
                    .contentType(MediaType.APPLICATION_JSON)
                    .body(response);
        }
        return responseEntity;
    }

    @PostMapping("/claim-registration-report")
    public ResponseEntity generateClaimRegistrationReport(@RequestBody ClaimRegistrationRequestDTO claimRegistrationRequestDTO) throws JRException, ClassNotFoundException, IOException {
//        ByteArrayResource resource;
        response = reportGenerationService.generateClaimRegistrationReport(claimRegistrationRequestDTO);
        HttpHeaders headers = new HttpHeaders();
        if (response.getStatus() == Boolean.TRUE) {
            if (Objects.equals(claimRegistrationRequestDTO.getReportType(), "pdf")) {
                responseEntity = ResponseEntity.ok()
                        .headers(headers)
                        .contentType(MediaType.parseMediaType("application/pdf"))
                        .body(response.getData());
            } else if (Objects.equals(claimRegistrationRequestDTO.getReportType(), "xls")) {
                responseEntity = ResponseEntity.ok()
                        .headers(headers)
//                    .contentType(MediaType.parseMediaType("application/vnd.ms-excel"))
                        .header("Content-Disposition", "attachment; filename=" + "testXLS")
                        .contentType(MediaType.parseMediaType("application/csv"))
                        .body(response.getData());
            }
        } else {
            responseEntity = ResponseEntity.ok()
                    .headers(headers)
                    .contentType(MediaType.APPLICATION_JSON)
                    .body(response);
        }
        return responseEntity;
    }

    @PostMapping("/iasl-report")
    public ResponseEntity iaslReport(@RequestBody ClaimNoDTOReport ClaimNoDTOReport) throws JRException, ClassNotFoundException, IOException {
        response = reportGenerationService.getIaslReportPDF(ClaimNoDTOReport);
        HttpHeaders headers = new HttpHeaders();
        if (response.getStatus() == Boolean.TRUE) {
            if (Objects.equals(ClaimNoDTOReport.getReportType(), "pdf")) {
                responseEntity = ResponseEntity.ok()
                        .headers(headers)
                        .contentType(MediaType.parseMediaType("application/pdf"))
                        .body(response.getData());
            } else if (Objects.equals(ClaimNoDTOReport.getReportType(), "xls")) {
                responseEntity = ResponseEntity.ok()
                        //.contentType(MediaType.parseMediaType("application/vnd.ms-excel"))
                        .header("Content-Disposition", "attachment; filename=" + "testXLS")
                        .contentType(MediaType.parseMediaType("application/csv"))
                        .body(response.getData());
            }
        } else {
            responseEntity = ResponseEntity.ok()
                    .headers(headers)
                    .contentType(MediaType.APPLICATION_JSON)
                    .body(response);
        }
        return responseEntity;
    }

    @PostMapping("/intimation-report")
    public ResponseEntity intimationReport(@RequestBody ClaimNoDTOReport ClaimNoDTOReport) throws JRException, ClassNotFoundException, IOException {
        response = reportGenerationService.getIntimationReport(ClaimNoDTOReport);
        HttpHeaders headers = new HttpHeaders();
        if (response.getStatus() == Boolean.TRUE) {
            if (Objects.equals(ClaimNoDTOReport.getReportType(), "pdf")) {
                responseEntity = ResponseEntity.ok()
                        .headers(headers)
                        .contentType(MediaType.parseMediaType("application/pdf"))
                        .body(response.getData());
            } else if (Objects.equals(ClaimNoDTOReport.getReportType(), "xls")) {
                responseEntity = ResponseEntity.ok()
                        //.contentType(MediaType.parseMediaType("application/vnd.ms-excel"))
                        .header("Content-Disposition", "attachment; filename=" + "testXLS")
                        .contentType(MediaType.parseMediaType("application/csv"))
                        .body(response.getData());
            }
        } else {
            responseEntity = ResponseEntity.ok()
                    .headers(headers)
                    .contentType(MediaType.APPLICATION_JSON)
                    .body(response);
        }
        return responseEntity;
    }

    @PostMapping("/payment-detail-report")
    public ResponseEntity getPaymentDetailReport(@RequestBody PaymentRequestDTO paymentRequestDTO) throws JRException, ClassNotFoundException, IOException {
        response = reportGenerationService.paymentDetailReport(paymentRequestDTO);
        HttpHeaders headers = new HttpHeaders();
        if (response.getStatus() == Boolean.TRUE) {
            if (Objects.equals(paymentRequestDTO.getReportType(), "pdf")) {
                responseEntity = ResponseEntity.ok()
                        .headers(headers)
                        .contentType(MediaType.parseMediaType("application/pdf"))
                        .body(response.getData());
            } else if (Objects.equals(paymentRequestDTO.getReportType(), "xls")) {
                responseEntity = ResponseEntity.ok()
                        //.contentType(MediaType.parseMediaType("application/vnd.ms-excel"))
                        .header("Content-Disposition", "attachment; filename=" + "testXLS")
                        .contentType(MediaType.parseMediaType("application/csv"))
                        .body(response.getData());
            }
        }else {
            responseEntity = ResponseEntity.ok()
                    .headers(headers)
                    .contentType(MediaType.APPLICATION_JSON)
                    .body(response);
        }
        return responseEntity;
    }

    @PostMapping("/job-assignment-detail-report")
    public ResponseEntity getJobAssignmentReport(@RequestBody JobAssignmentReportRequestDTO jobAssignmentReportRequestDTO) throws JRException, ClassNotFoundException, IOException, ParseException {
        response = reportGenerationService.JobAssignmentReport(jobAssignmentReportRequestDTO);
        HttpHeaders headers = new HttpHeaders();
        if (response.getStatus() == Boolean.TRUE) {
            if (Objects.equals(jobAssignmentReportRequestDTO.getReportType(), "pdf")) {
                responseEntity = ResponseEntity.ok()
                        .headers(headers)
                        .contentType(MediaType.parseMediaType("application/pdf"))
                        .body(response.getData());
            } else if (Objects.equals(jobAssignmentReportRequestDTO.getReportType(), "xls")) {
                responseEntity = ResponseEntity.ok()
                        //.contentType(MediaType.parseMediaType("application/vnd.ms-excel"))
                        .header("Content-Disposition", "attachment; filename=" + "testXLS")
                        .contentType(MediaType.parseMediaType("application/csv"))
                        .body(response.getData());
            }
        }else {
            responseEntity = ResponseEntity.ok()
                    .headers(headers)
                    .contentType(MediaType.APPLICATION_JSON)
                    .body(response);
        }
        return responseEntity;
    }

    @PostMapping("/claim-ratio")
    public ResponseEntity generateClaimRatioReport(@RequestBody ClaimRatioDTO claimRatioDTO) throws ParseException, JRException, IOException, ClassNotFoundException {
        HttpHeaders headers = new HttpHeaders();
        if (claimRatioDTO != null) {
            response = reportGenerationService.generateClaimRatioReport(claimRatioDTO);
            if (response.getStatus() == Boolean.TRUE) {
                if (Objects.equals(claimRatioDTO.getReportType(), "pdf")) {
                    responseEntity = ResponseEntity.ok()
                            .headers(headers)
                            .contentType(MediaType.parseMediaType("application/pdf"))
                            .body(response.getData());
                } else if (Objects.equals(claimRatioDTO.getReportType(), "xls")) {
                    responseEntity = ResponseEntity.ok()
                            //.contentType(MediaType.parseMediaType("application/vnd.ms-excel"))
                            .header("Content-Disposition", "attachment; filename=" + "testXLS")
                            .contentType(MediaType.parseMediaType("application/csv"))
                            .body(response.getData());
                }
            } else {
                responseEntity = ResponseEntity.ok()
                        .headers(headers)
                        .contentType(MediaType.APPLICATION_JSON)
                        .body(response);
            }
        }
        return responseEntity;
    }

//    @CrossOrigin
//    @RequestMapping(method = RequestMethod.POST, value = "/generateIntimationReportTest")
////    public ResponseEntity<ByteArrayResource> intimationReport(@RequestBody String json) throws JRException, ClassNotFoundException, IOException {
//    public ResponseEntity intimationReportTest(@RequestBody  ClaimNoDTOReport claimNoDTOReport) throws JRException, ClassNotFoundException, IOException {
//
////        JSONObject jsonObject = new JSONObject(json);
////        String claimNo = jsonObject.get("claimNo").toString();
////        String user = jsonObject.get("user").toString();
//
//        ByteArrayResource resource ;
//        resource = reportGenerationService.getIntimationReportPDF(claimNoDTOReport.getClaimNo(), claimNoDTOReport.getUser());
//
//        HttpHeaders headers = new HttpHeaders();
//
//
//        if(Objects.equals(claimNoDTOReport.getReportType(), "pdf")){
//
//            responseEntity = ResponseEntity.ok()
//                    .headers(headers)
//                    .contentType(MediaType.parseMediaType("application/pdf"))
//                    .body(resource);
//        }else if(Objects.equals(claimNoDTOReport.getReportType(), "excel")){
//
//            responseEntity = ResponseEntity.ok()
//                    .headers(headers)
//                    .contentType(MediaType.parseMediaType("application/excel"))
//                    .body(resource);
//        }
//
//        return responseEntity;
//    }
}
