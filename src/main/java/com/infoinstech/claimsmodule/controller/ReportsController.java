package com.infoinstech.claimsmodule.controller;

import com.infoinstech.claimsmodule.dto.common.Response;
import com.infoinstech.claimsmodule.dto.reports.IaslReportDTO;
import com.infoinstech.claimsmodule.dto.reports.claimregistrationreport.ClaimRegistrationReportDTO;
import com.infoinstech.claimsmodule.dto.reports.claimregistrationreport.ClaimRegistrationRequestDTO;
import com.infoinstech.claimsmodule.dto.reports.claimregistrationsheet.ClaimRegistrationSheetDTO;
import com.infoinstech.claimsmodule.dto.reports.paymentDetailReport.PaymentRequestDTO;
import com.infoinstech.claimsmodule.domain.model.common.Logo;
import com.infoinstech.claimsmodule.exceptions.types.NotEnoughInfoException;
import com.infoinstech.claimsmodule.services.reporting.ReportingService;
import oracle.jdbc.proxy.annotation.Post;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import com.infoinstech.claimsmodule.urls.REPORT_DATA;

import java.util.ArrayList;

@RestController
@RequestMapping("v1/reports")
public class ReportsController {

    @Autowired
    private ReportingService reportingService;

    @PostMapping("/intimation-report")
    public ResponseEntity<Response> generateIntimationReport(@RequestBody String json) {
        JSONObject jsonObject = new JSONObject(json);
        String claimNo = jsonObject.get("claimNo").toString();
        String user = jsonObject.get("user").toString();
        Response response = null;
        if (claimNo != null) {
            response = reportingService.getIntimationReportDTO(claimNo, user);
            return new ResponseEntity<Response>(response, HttpStatus.OK);
        } else {
            throw new NotEnoughInfoException("There is no adequate Information in the Request");
        }
    }

    @PostMapping("/intimation")
    public ResponseEntity<Response> generateIntimationReportForAgent(@RequestBody String json) {
        Response response = null;
        JSONObject jsonObject = new JSONObject(json);
        String claimNo = jsonObject.get("claimNo").toString();
        String user = jsonObject.get("user").toString();
        if (claimNo != null) {
            response = reportingService.getIntimationReportDTOForAgent(claimNo, user);
            return new ResponseEntity<Response>(response, HttpStatus.OK);
        } else {
            throw new NotEnoughInfoException("There is no adequate Information in the Request");
        }
    }

    @PostMapping("/iasl-report")
    public ResponseEntity<Response> generateIASLReport(@RequestBody String json) {
        Response response = null;
        JSONObject jsonObject = new JSONObject(json);
        String claimNo = jsonObject.get("claimNo").toString();
        IaslReportDTO iaslReportDTO = reportingService.getIASLReportDTO(claimNo, "");
        response = new Response(200, true, iaslReportDTO);
        return new ResponseEntity<Response>(response, HttpStatus.OK);
    }

    @PostMapping("/claim-registration-sheet")
    public ResponseEntity<Response> generateClaimRegistrationSheet(@RequestBody String json) {
        Response response = null;
        JSONObject jsonObject = new JSONObject(json);
        String claimNo = jsonObject.get("claimNo").toString();
        ClaimRegistrationSheetDTO claimRegistrationSheetDTO = reportingService.getClaimRegistrationSheet(claimNo, "");
        response = new Response(200, true, claimRegistrationSheetDTO);
        return new ResponseEntity<Response>(response, HttpStatus.OK);
    }

//    @CrossOrigin
//    @RequestMapping(value = REPORT_DATA.ClaimRegistrationReport,

//    @CrossOrigin
//    @RequestMapping(value = REPORT_DATA.JobAssignmentReport,
//            method = RequestMethod.POST,
//            consumes = MediaType.APPLICATION_JSON_VALUE,
//            produces = MediaType.APPLICATION_JSON_VALUE)
//    public Response generateJobAssignmentReport(@RequestBody JobNoDTO jobNoDTO) {
//
//        Response response;
//
//        if (jobNoDTO != null) {
//
//            response = reportingService.generateJobAssignmentReportDTO(jobNoDTO);
//        } else {
//
//            response = new Response(400, false, "There is no adequate information in the request");
//        }
//
//        return response;
//    }

    @PostMapping("/claim-registration-report")
    public ResponseEntity<Response> generateClaimRegistrationReport(@RequestBody ClaimRegistrationRequestDTO claimRegistrationRequestDTO) {
        Response response = null;
        ArrayList<ClaimRegistrationReportDTO> claimRegistrationReportDTO = reportingService.getClaimRegistrationReportDetails(claimRegistrationRequestDTO);
        response = new Response(200, true, claimRegistrationReportDTO);
        return new ResponseEntity<Response>(response, HttpStatus.OK);
    }

    @PostMapping("/logo-i-report")
    public ResponseEntity<Response> insertReportLogo(@RequestBody Logo logoDto) {
        Response response = null ;
        response = reportingService.saveLogo(logoDto);
        return new ResponseEntity<Response>(response, HttpStatus.OK);
    }

    @PostMapping("/payment-detail-report")
    public ResponseEntity<Response> getDataForPaymentDetailReport(@RequestBody PaymentRequestDTO paymentRequestDTO) {
        Response response = null;
        response = reportingService.getPaymentDetailsList(paymentRequestDTO);
        return new ResponseEntity<Response>(response, HttpStatus.OK);
    }


//    @CrossOrigin
//    @RequestMapping(value = "/logo-v-report",
//            method = RequestMethod.GET,
//            produces = MediaType.APPLICATION_JSON_VALUE)
//    public Response viewReportLogo(){
//        Response response;
//
//        Logo newLogo = reportingService.getLogo();
//
//        return new Response(200, true, newLogo.getCmgLogo());
//
//    }
    
}
