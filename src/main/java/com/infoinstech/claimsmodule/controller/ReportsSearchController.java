package com.infoinstech.claimsmodule.controller;

import com.infoinstech.claimsmodule.dto.common.JobNoDTO;
import com.infoinstech.claimsmodule.dto.common.Response;
import com.infoinstech.claimsmodule.dto.reports.ClaimIndividualReportsDTO;
import com.infoinstech.claimsmodule.exceptions.types.NotEnoughInfoException;
import com.infoinstech.claimsmodule.services.reporting.ReportSearchingService;
import com.infoinstech.claimsmodule.urls.REPORT_GENERATION;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("v1/report-search")
public class ReportsSearchController {

    @Autowired
    private ReportSearchingService reportSearchingService;

    @PostMapping("/individual-claims-report")
    public ResponseEntity<Response> generateJobAssignmentReport(@RequestBody ClaimIndividualReportsDTO reportsDTO){
        Response response = null ;
        if(reportsDTO!=null){
            response = reportSearchingService.searchClaimsForReportGeneration(reportsDTO);
            return new ResponseEntity<Response>(response, HttpStatus.OK);
        }
        else {
            throw new NotEnoughInfoException("There is no adequate Information in the Request");
        }
    }
}
