package com.infoinstech.claimsmodule.controller;

import com.infoinstech.claimsmodule.dto.common.Response;
import com.infoinstech.claimsmodule.domain.model.underwriting.master.ProductVehicleSketch;
import com.infoinstech.claimsmodule.domain.model.claims.reference.RefVehicleSketch;
import com.infoinstech.claimsmodule.domain.model.claims.transaction.DamageArea;
import com.infoinstech.claimsmodule.exceptions.types.NotEnoughInfoException;
import com.infoinstech.claimsmodule.services.business.VehicleSketchService;
import com.infoinstech.claimsmodule.urls.CLAIM_INTIMATION;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

/**
 * Created by thilina on 21/12/17.
 */
@RequestMapping("v1/sketch")
@RestController
public class SketchController {

    @Autowired
    private VehicleSketchService vehicleSketchService;

    @PostMapping("/upload")
    public  ResponseEntity<Response> saveSketchType(@RequestBody RefVehicleSketch refVehicleSketch) {
        Response response = null ;
        if (refVehicleSketch != null) {
            response = vehicleSketchService.saveSketchType(refVehicleSketch);
            return new ResponseEntity<Response>(response, HttpStatus.OK);
        } else {
            throw new NotEnoughInfoException("There is no adequate Information in the Request");
        }
    }

    @PostMapping("/upload-damage")
    public ResponseEntity<Response> saveDamageSketch(@RequestBody DamageArea damageArea) {
        Response response = null ;
        if (damageArea != null) {
            response = vehicleSketchService.saveDamageSketch(damageArea);
            return new ResponseEntity<Response>(response, HttpStatus.OK);
        } else {
            throw new NotEnoughInfoException("There is no adequate Information in the Request");
        }
    }

    @PostMapping("/update-damage")
    public ResponseEntity<Response> updateSketch(@RequestBody DamageArea damageArea) {
        Response response = null;
        if (damageArea != null) {
            response = vehicleSketchService.updateSketch(damageArea);
            return new ResponseEntity<Response>(response, HttpStatus.OK);
        } else {
            throw new NotEnoughInfoException("There is no adequate Information in the Request");
        }
    }

    @PostMapping("/view")
    public ResponseEntity<Response> viewSketchType(@RequestBody ProductVehicleSketch productVehicleSketch) {
        Response response = null;
        if (productVehicleSketch != null) {
            response = vehicleSketchService.viewSketchType(productVehicleSketch);
            return new ResponseEntity<Response>(response, HttpStatus.OK);
        } else {
            throw new NotEnoughInfoException("There is no adequate Information in the Request");
        }
    }

    @PostMapping("/view-damage")
    public ResponseEntity<Response> viewSketch(@RequestBody DamageArea damageArea) {
        Response response = null ;
        if (damageArea != null) {
            response = vehicleSketchService.viewSketch(damageArea);
            return new ResponseEntity<Response>(response, HttpStatus.OK);
        } else {
            throw new NotEnoughInfoException("There is no adequate Information in the Request");
        }
    }

    @GetMapping("/view-all")
    public ResponseEntity<Response> viewAllSketchType() {
        Response response = null;
        response = vehicleSketchService.viewAllSketchType();
        return new ResponseEntity<Response>(response, HttpStatus.OK);
    }

}
