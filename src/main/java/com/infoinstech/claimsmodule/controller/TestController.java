package com.infoinstech.claimsmodule.controller;

import com.infoinstech.claimsmodule.dto.common.*;
import com.infoinstech.claimsmodule.dto.reports.jobassignmentdetailsreport.JobAssignmentReportDTO;
import com.infoinstech.claimsmodule.dto.reports.jobassignmentdetailsreport.JobAssignmentReportRequestDTO;
import com.infoinstech.claimsmodule.domain.model.common.Logo;
import com.infoinstech.claimsmodule.domain.repository.common.LogoRepository;
import com.infoinstech.claimsmodule.services.api.GoogleMapsAPIService;
import com.infoinstech.claimsmodule.services.business.JobAssignmentService;
import com.infoinstech.claimsmodule.services.business.WrapperService;
import com.infoinstech.claimsmodule.services.reporting.ReportingService;
import com.infoinstech.claimsmodule.services.util.DateConversion;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import java.text.ParseException;
import java.util.Base64;
import java.util.Date;
import java.util.List;

@RestController
public class TestController {

    @Autowired
    private WrapperService wrapperService;
    @Autowired
    private DateConversion dateConversion;
    @Autowired
    private ReportingService reportingService;
    @Autowired
    private GoogleMapsAPIService googleMapsAPIService;

    @Autowired
    private LogoRepository logoRepository;

    @Autowired
    private JobAssignmentService jobAssignmentService;

    @CrossOrigin
    @RequestMapping(value = "/",
            method = RequestMethod.GET,
            produces = MediaType.APPLICATION_JSON_VALUE)
    public String getPolicyDetails(HttpServletRequest request) {

        System.out.print("Remote Address : " + request.getRemoteAddr());
        System.out.println(request.getRequestURL());

        return "Welcome to Claims Module REST API Service";
    }

    @CrossOrigin
    @RequestMapping(value = "/test/loss-date-validation/",
            method = RequestMethod.POST,
            consumes = MediaType.APPLICATION_JSON_VALUE,
            produces = MediaType.APPLICATION_JSON_VALUE)
    public Response testRiskValidation(@RequestBody TestDTO testDTO) {

        String sessionId = null;

        Date date = dateConversion.convertStringToDate(testDTO.getLossDate());

        wrapperService.buildPolicy(testDTO.getPolicyNo(), date);

        return new Response(200, true, "");
    }

    @CrossOrigin
    @RequestMapping(value = "/get-logo/",
            method = RequestMethod.GET,
            produces = MediaType.APPLICATION_JSON_VALUE)
    public Response generateBlob() {

        Logo logo = logoRepository.findByCmgActiveAndCmgRepType("Y", "H");

        String convertedLogo =  Base64.getEncoder().encodeToString(logo.getCmgLogo());

        return new Response(200, true,convertedLogo);

    }

    @CrossOrigin
    @RequestMapping(value = "/save-logo/",
            method = RequestMethod.POST,
            consumes = MediaType.APPLICATION_JSON_VALUE,
            produces = MediaType.APPLICATION_JSON_VALUE)
    public Response saveLogo(@RequestBody SaveLogo saveLogo) {

        Logo logo = new Logo();
        logo.setCmgSeqNo("0B0011100000028");
        logo.setCmgActive("Y");
        logo.setCmgRepType("H");
        logo.setCmgLogo(Base64.getDecoder().decode(saveLogo.getLogo()));

        logoRepository.save(logo);

        return new Response(200, true,"successfull");

    }

    @CrossOrigin
    @RequestMapping(value = "/get-places/",
            method = RequestMethod.GET,
            consumes = MediaType.APPLICATION_JSON_VALUE,
            produces = MediaType.APPLICATION_JSON_VALUE)
    public Response getPlaces() {

        return googleMapsAPIService.getPlaceDetails("ChIJ3eHZ1QdZ4joRr5PEhOiCmL8");
    }

    @CrossOrigin
    @RequestMapping(value = "/test-report/",
            method = RequestMethod.GET,
            produces = MediaType.APPLICATION_JSON_VALUE)
    public List<JobAssignmentReportDTO> testJobAssignmentReport() throws ParseException {

        JobAssignmentReportRequestDTO requestDTO = new JobAssignmentReportRequestDTO();
        requestDTO.setAssessorName("");
        requestDTO.setClaimNo("");
        requestDTO.setVehicleNo("");
        requestDTO.setFromDate("26-MAR-2017");
        requestDTO.setToDate("26-MAR-2018");

        return reportingService.generateJobAssignmentReportDTO(requestDTO);
    }

    @CrossOrigin
    @RequestMapping(value = "/notifications/",
            method = RequestMethod.POST,
            consumes = MediaType.APPLICATION_JSON_VALUE,
            produces = MediaType.APPLICATION_JSON_VALUE)
    public Response getJobNotifications(@RequestBody ReferenceCode referenceCode) {

        Response response;

        List<JobNotification> jobNotificationList = jobAssignmentService.getJobNotificationsList(referenceCode.getCode());

        if(jobNotificationList.size()!=0){

            response = new Response(200,true,jobNotificationList);
        }
        else{

            response = new Response(400, false, "no data found");
        }

        return  response;
    }


}
