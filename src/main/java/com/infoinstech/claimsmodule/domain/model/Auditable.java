package com.infoinstech.claimsmodule.domain.model;

import com.infoinstech.claimsmodule.domain.model.underwriting.master.ProductVehicleSketch;
import com.infoinstech.claimsmodule.domain.model.claims.reference.RefVehicleSketch;
import com.infoinstech.claimsmodule.domain.model.claims.transaction.DamageArea;
import org.springframework.data.annotation.CreatedBy;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.LastModifiedBy;
import org.springframework.data.annotation.LastModifiedDate;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import javax.persistence.*;
import java.util.Date;

/**
 * Created by thilina on 21/12/17.
 */
@MappedSuperclass
@EntityListeners({AuditingEntityListener.class, RefVehicleSketch.class, DamageArea.class, ProductVehicleSketch.class})
public abstract class Auditable<U> {
    @CreatedBy
    @Column(name = "CREATED_BY")
    protected U createdBy;

    @CreatedDate
    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "CREATED_DATE")
    protected Date createdDate;

    @LastModifiedBy
    @Column(name = "MODIFIED_BY")
    protected U modifiedBy;

    @LastModifiedDate
    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "MODIFIED_DATE")
    protected Date modifiedDate;

    public U getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(U createdBy) {
        this.createdBy = createdBy;
    }

    public Date getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(Date createdDate) {
        this.createdDate = createdDate;
    }

    public U getModifiedBy() {
        return modifiedBy;
    }

    public void setModifiedBy(U modifiedBy) {
        this.modifiedBy = modifiedBy;
    }

    public Date getModifiedDate() {
        return modifiedDate;
    }

    public void setModifiedDate(Date modifiedDate) {
        this.modifiedDate = modifiedDate;
    }
}
