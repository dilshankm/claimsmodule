package com.infoinstech.claimsmodule.domain.model;

import javax.persistence.*;
import java.util.Date;

@Entity
@Table(name = "CM_T_SESSION_DTLS")
public class Session {

    @Id
    @Column(name = "SED_SEQ_NO")
    private String sessionId;

    @Column(name = "SED_USERNAME")
    private String userName;

    @Column(name = "SED_USER_ROLE")
    private String userRole;

    @Column(name = "SED_RANDOM_ID")
    private String randomId;

    @Column(name = "SED_SESSION_TOKEN")
    private String sessionToken;

    @Column(name = "CREATED_TIME")
    @Temporal(TemporalType.TIMESTAMP)
    private Date createdTime;

    @Column(name = "SED_USER_CODE")
//    @Column(name = "SED_USERCODE")
    private String userCode;

    @Column(name = "SED_ACTIVE")
    private Character isActive;

    public String getSessionId() {
        return sessionId;
    }

    public void setSessionId(String sessionId) {
        this.sessionId = sessionId;
    }

    public String getUsername() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getUserRole() {
        return userRole;
    }

    public void setUserRole(String userRole) {
        this.userRole = userRole;
    }

    public String getRandomId() {
        return randomId;
    }

    public void setRandomId(String randomId) {
        this.randomId = randomId;
    }

    public String getSessionToken() {
        return sessionToken;
    }

    public void setSessionToken(String sessionToken) {
        this.sessionToken = sessionToken;
    }

    public Date getCreatedTime() {
        return createdTime;
    }

    public void setCreatedTime(Date createdTime) {
        this.createdTime = createdTime;
    }

    public Character getIsActive() {
        return isActive;
    }

    public void setIsActive(Character isActive) {
        this.isActive = isActive;
    }

    public String getUserCode() {
        return userCode;
    }

    public void setUserCode(String userCode) {
        this.userCode = userCode;
    }
}
