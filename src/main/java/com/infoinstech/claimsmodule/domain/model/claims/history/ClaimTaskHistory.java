package com.infoinstech.claimsmodule.domain.model.claims.history;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import java.util.Date;

@Entity
@Table(name = "CL_H_TASKHIS")
public class ClaimTaskHistory {

    public static final String SEQUENCE = "SEQ_CL_H_TASKHIS";
    @Id
    @Column(name = "HIS_SEQ_NO")
    private String sequenceneNo;

    @NotNull
    @Column(name = "HIS_INT_SEQNO")
    private String claimSequenceNo;

    @NotNull
    @Column(name = "HIS_CLAIM_NO")
    private String claimNo;

    @Column(name = "HIS_TASK_CD")
    private String taskCode;

    @Column(name = "CREATED_BY")
    private String createdBy;

    @Column(name = "CREATED_DATE")
    private Date createdDate;

    @Column(name = "MODIFIED_BY")
    private String modifiedBy;

    @Column(name = "MODIFIED_DATE")
    private Date modifiedDate;

    @Column(name = "HIS_LOSS_COMMENT")
    private String lossComments;

    @Column(name = "HIS_CLAIM_COMMENT")
    private String claimComments;


    public String getSequenceneNo() {
        return sequenceneNo;
    }

    public void setSequenceneNo(String sequenceneNo) {
        this.sequenceneNo = sequenceneNo;
    }

    public String getClaimSequenceNo() {
        return claimSequenceNo;
    }

    public void setClaimSequenceNo(String claimSequenceNo) {
        this.claimSequenceNo = claimSequenceNo;
    }

    public String getClaimNo() {
        return claimNo;
    }

    public void setClaimNo(String claimNo) {
        this.claimNo = claimNo;
    }

    public String getTaskCode() {
        return taskCode;
    }

    public void setTaskCode(String taskCode) {
        this.taskCode = taskCode;
    }

    public String getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(String createdBy) {
        this.createdBy = createdBy;
    }

    public Date getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(Date createdDate) {
        this.createdDate = createdDate;
    }

    public String getModifiedBy() {
        return modifiedBy;
    }

    public void setModifiedBy(String modifiedBy) {
        this.modifiedBy = modifiedBy;
    }

    public Date getModifiedDate() {
        return modifiedDate;
    }

    public void setModifiedDate(Date modifiedDate) {
        this.modifiedDate = modifiedDate;
    }

    public String getLossComments() {
        return lossComments;
    }

    public void setLossComments(String lossComments) {
        this.lossComments = lossComments;
    }

    public String getClaimComments() {
        return claimComments;
    }

    public void setClaimComments(String claimComments) {
        this.claimComments = claimComments;
    }
}

