package com.infoinstech.claimsmodule.domain.model.claims.history;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.util.Date;

/**
 * Created by dushman on 4/19/18.
 */


@Entity
@Table(name = "CL_H_OTH_CHARGES_PAYMENTS")
public class OtherChargesPaymentHistory {

    @Id
    @Column(name = "CTO_SEQ_NO")
    private String seqNo;

    @Column(name = "CTO_REQ_SEQ_NO")
    private String reqSequenceNo;

    @Column(name = "CTO_CLAIM_NO")
    private String claimNo;

    @Column(name = "CTO_TRANS_TYPE")
    private String transType;

    @Column(name = "CTO_INT_SEQ_NO")
    private String intSeqNo;

    @Column(name = "CTO_PAYEE_NAME")
    private String PayeeName;

    @Column(name = "CTO_PAYEE_CODE")
    private String payeeCode;

    @Column(name = "CTO_PAY_AMOUNT")
    private Integer payAmount;

    @Column(name = "CTO_PRIORITY_LEVEL")
    private String priorityLevel;

    @Column(name = "CTO_CURRENCY")
    private String currency;

    @Column(name = "CTO_EXCHANGE_RATE")
    private String exchangeRate;

    @Column(name = "CREATED_BY")
    private String createdBy;

    @Column(name = "CREATED_DATE")
    private Date createdDate;

    @Column(name = "MODIFIED_BY")
    private String modifiedBy;

    @Column(name = "MODIFIED_DATE")
    private Date modifiedDate;

    @Column(name = "CTO_CUS_CODE")
    private String cusCode;

    @Column(name = "CTO_HAND_DT")
    private Date handDate;

    @Column(name = "CTO_CHEQUE_NO")
    private String chequeNo;

    @Column(name = "CTO_RISK_SEQ")
    private String riskSequence;

    @Column(name = "CTO_PERIL_CODE")
    private String periCode;

    @Column(name = "CTO_LOC_CODE")
    private String locCode;

    @Column(name = "CTO_CRM_CODE")
    private String crmCode;

    @Column(name = "CTO_CRED_CODE")
    private String credCode;

    @Column(name = "CTO_ADDRESS_1")
    private String address1;

    @Column(name = "CTO_ADDRESS_2")
    private String address2;

    @Column(name = "CTO_ADDRESS_3")
    private String address3;

    @Column(name = "CTO_NARRATION")
    private String narration;

    @Column(name = "CTO_CHEQUE_NAME")
    private String chequenName;


    public String getSeqNo() {
        return seqNo;
    }

    public void setSeqNo(String seqNo) {
        this.seqNo = seqNo;
    }

    public String getReqSequenceNo() {
        return reqSequenceNo;
    }

    public void setReqSequenceNo(String reqSequenceNo) {
        this.reqSequenceNo = reqSequenceNo;
    }

    public String getClaimNo() {
        return claimNo;
    }

    public void setClaimNo(String claimNo) {
        this.claimNo = claimNo;
    }

    public String getTransType() {
        return transType;
    }

    public void setTransType(String transType) {
        this.transType = transType;
    }

    public String getIntSeqNo() {
        return intSeqNo;
    }

    public void setIntSeqNo(String intSeqNo) {
        this.intSeqNo = intSeqNo;
    }

    public String getPayeeName() {
        return PayeeName;
    }

    public void setPayeeName(String payeeName) {
        PayeeName = payeeName;
    }

    public String getPayeeCode() {
        return payeeCode;
    }

    public void setPayeeCode(String payeeCode) {
        this.payeeCode = payeeCode;
    }

    public Integer getPayAmount() {
        return payAmount;
    }

    public void setPayAmount(Integer payAmount) {
        this.payAmount = payAmount;
    }

    public String getPriorityLevel() {
        return priorityLevel;
    }

    public void setPriorityLevel(String priorityLevel) {
        this.priorityLevel = priorityLevel;
    }

    public String getCurrency() {
        return currency;
    }

    public void setCurrency(String currency) {
        this.currency = currency;
    }

    public String getExchangeRate() {
        return exchangeRate;
    }

    public void setExchangeRate(String exchangeRate) {
        this.exchangeRate = exchangeRate;
    }

    public String getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(String createdBy) {
        this.createdBy = createdBy;
    }

    public Date getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(Date createdDate) {
        this.createdDate = createdDate;
    }

    public String getModifiedBy() {
        return modifiedBy;
    }

    public void setModifiedBy(String modifiedBy) {
        this.modifiedBy = modifiedBy;
    }

    public Date getModifiedDate() {
        return modifiedDate;
    }

    public void setModifiedDate(Date modifiedDate) {
        this.modifiedDate = modifiedDate;
    }

    public String getCusCode() {
        return cusCode;
    }

    public void setCusCode(String cusCode) {
        this.cusCode = cusCode;
    }

    public Date getHandDate() {
        return handDate;
    }

    public void setHandDate(Date handDate) {
        this.handDate = handDate;
    }

    public String getChequeNo() {
        return chequeNo;
    }

    public void setChequeNo(String chequeNo) {
        this.chequeNo = chequeNo;
    }

    public String getRiskSequence() {
        return riskSequence;
    }

    public void setRiskSequence(String riskSequence) {
        this.riskSequence = riskSequence;
    }

    public String getPeriCode() {
        return periCode;
    }

    public void setPeriCode(String periCode) {
        this.periCode = periCode;
    }

    public String getLocCode() {
        return locCode;
    }

    public void setLocCode(String locCode) {
        this.locCode = locCode;
    }

    public String getCrmCode() {
        return crmCode;
    }

    public void setCrmCode(String crmCode) {
        this.crmCode = crmCode;
    }

    public String getCredCode() {
        return credCode;
    }

    public void setCredCode(String credCode) {
        this.credCode = credCode;
    }

    public String getAddress1() {
        return address1;
    }

    public void setAddress1(String address1) {
        this.address1 = address1;
    }

    public String getAddress2() {
        return address2;
    }

    public void setAddress2(String address2) {
        this.address2 = address2;
    }

    public String getAddress3() {
        return address3;
    }

    public void setAddress3(String address3) {
        this.address3 = address3;
    }

    public String getNarration() {
        return narration;
    }

    public void setNarration(String narration) {
        this.narration = narration;
    }

    public String getChequenName() {
        return chequenName;
    }

    public void setChequenName(String chequenName) {
        this.chequenName = chequenName;
    }
}
