package com.infoinstech.claimsmodule.domain.model.claims.history;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.util.Date;

@Entity
@Table(name = "CL_H_REQ_PAYEES")
public class RequisitionPayeeHistory {
    
    @Id
    @Column(name="RPQ_SEQ_NO")
    private String   sequenceNo;

    @Column(name="RPQ_REQ_NO")
    private String reqNo;

	@Column(name="RPQ_AMOUNT")
    private Integer amount;

	@Column(name="RPQ_CHEQUE_FAV")
    private String chequeFav;

	@Column(name="RPQ_ADDRESS")
    private String address;

	@Column(name="RPQ_COMMENTS")
    private String comments;

	@Column(name="RPQ_RECEIVED_DT")
    private Date receivedDate;

    @Column(name="RPQ_ISSUED_DT")
    private Date issuedDate;

    @Column(name="RPQ_HAND_TO")
    private String handTo;

	@Column(name="RPQ_HAND_DT")
    private Date handDate;

    @Column(name="RPQ_HAND_BY")
    private String handBy;

	@Column(name="RPQ_CHEQUE_NO")
    private String chequeNo ;

    @Column(name="CREATED_BY")
	private String createdBy;

    @Column(name="CREATED_DATE")
    private Date createdDate;

    @Column(name="MODIFIED_BY")
    private String modifiedBy;

    @Column(name="MODIFIED_DATE")
    private Date modifiedDate;

	@Column(name="RPQ_INT_SEQ")
    private String intSeq;

	@Column(name="RPQ_PVOUCHER_NO")
    private String pvoucherNo;

	@Column(name="RPQ_PAYEE_TYPE")
    private String payeeType;

	@Column(name="RPQ_HAND_AUTH_CODE")
    private String handAuthCode;

	@Column(name="RPQ_HAND_LEVEL")
    private Integer handLevel;

	@Column(name="RPQ_EFFECT_DATE")
    private Date effectDate;

    @Column(name="RPQ_PRI_LVL")
    private String priLvl;

	@Column(name="RPQ_CRM_CODE")
    private String crmCode;

	@Column(name="RPQ_ADDRESS_2")
    private String address2;

	@Column(name="RPQ_ADDRESS_3")
    private String address3;

	@Column(name="RPQ_CRED_CODE")
    private String credCode;

	@Column(name="RPQ_NARRATION")
    private String narration;

	@Column(name="RPQ_CHEQUE_NAME")
    private String chequeName;


    public String getSequenceNo() {
        return sequenceNo;
    }

    public void setSequenceNo(String sequenceNo) {
        this.sequenceNo = sequenceNo;
    }

    public Integer getAmount() {
        return amount;
    }

    public void setAmount(Integer amount) {
        this.amount = amount;
    }

    public String getChequeFav() {
        return chequeFav;
    }

    public void setChequeFav(String chequeFav) {
        this.chequeFav = chequeFav;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getComments() {
        return comments;
    }

    public void setComments(String comments) {
        this.comments = comments;
    }

    public Date getReceivedDate() {
        return receivedDate;
    }

    public void setReceivedDate(Date receivedDate) {
        this.receivedDate = receivedDate;
    }

    public Date getIssuedDate() {
        return issuedDate;
    }

    public void setIssuedDate(Date issuedDate) {
        this.issuedDate = issuedDate;
    }

    public String getHandTo() {
        return handTo;
    }

    public void setHandTo(String handTo) {
        this.handTo = handTo;
    }

    public Date getHandDate() {
        return handDate;
    }

    public void setHandDate(Date handDate) {
        this.handDate = handDate;
    }

    public String getHandBy() {
        return handBy;
    }

    public void setHandBy(String handBy) {
        this.handBy = handBy;
    }

    public String getChequeNo() {
        return chequeNo;
    }

    public void setChequeNo(String chequeNo) {
        this.chequeNo = chequeNo;
    }

    public String getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(String createdBy) {
        this.createdBy = createdBy;
    }

    public Date getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(Date createdDate) {
        this.createdDate = createdDate;
    }

    public String getModifiedBy() {
        return modifiedBy;
    }

    public void setModifiedBy(String modifiedBy) {
        this.modifiedBy = modifiedBy;
    }

    public Date getModifiedDate() {
        return modifiedDate;
    }

    public void setModifiedDate(Date modifiedDate) {
        this.modifiedDate = modifiedDate;
    }

    public String getReqNo() {
        return reqNo;
    }

    public void setReqNo(String reqNo) {
        this.reqNo = reqNo;
    }

    public String getIntSeq() {
        return intSeq;
    }

    public void setIntSeq(String intSeq) {
        this.intSeq = intSeq;
    }

    public String getPvoucherNo() {
        return pvoucherNo;
    }

    public void setPvoucherNo(String pvoucherNo) {
        this.pvoucherNo = pvoucherNo;
    }

    public String getPayeeType() {
        return payeeType;
    }

    public void setPayeeType(String payeeType) {
        this.payeeType = payeeType;
    }

    public String getHandAuthCode() {
        return handAuthCode;
    }

    public void setHandAuthCode(String handAuthCode) {
        this.handAuthCode = handAuthCode;
    }

    public Integer getHandLevel() {
        return handLevel;
    }

    public void setHandLevel(Integer handLevel) {
        this.handLevel = handLevel;
    }

    public Date getEffectDate() {
        return effectDate;
    }

    public void setEffectDate(Date effectDate) {
        this.effectDate = effectDate;
    }

    public String getPriLvl() {
        return priLvl;
    }

    public void setPriLvl(String priLvl) {
        this.priLvl = priLvl;
    }

    public String getCrmCode() {
        return crmCode;
    }

    public void setCrmCode(String crmCode) {
        this.crmCode = crmCode;
    }

    public String getAddress2() {
        return address2;
    }

    public void setAddress2(String address2) {
        this.address2 = address2;
    }

    public String getAddress3() {
        return address3;
    }

    public void setAddress3(String address3) {
        this.address3 = address3;
    }

    public String getCredCode() {
        return credCode;
    }

    public void setCredCode(String credCode) {
        this.credCode = credCode;
    }

    public String getNarration() {
        return narration;
    }

    public void setNarration(String narration) {
        this.narration = narration;
    }

    public String getChequeName() {
        return chequeName;
    }

    public void setChequeName(String chequeName) {
        this.chequeName = chequeName;
    }
}
