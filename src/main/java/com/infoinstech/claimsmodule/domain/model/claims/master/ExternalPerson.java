package com.infoinstech.claimsmodule.domain.model.claims.master;

import javax.persistence.*;
import java.util.Date;

@Entity
@Table(name = "CL_M_EXTERNAL_PER")
public class ExternalPerson {

    @Id
    @Column(name = "EXP_CODE")
    private String code;

    @Column(name = "EXP_SURNAME")
    private String surname;

    @Column(name = "EXP_CATEGORY_CODE")
    private String categoryCode;

    @Column(name = "EXP_INITIAL")
    private String initial;

    @Column(name = "EXP_TITTLE")
    private String title;

/*    @Column(name = "EXP_SEND_EMAIL")
    private String email;*/

    @Column(name = "EXP_ADDRESS")
    private String address;

    @Column(name = "EXP_ADDRESS2")
    private String address2;

    @Column(name = "EXP_ADDRESS3")
    private String address3;

    @Column(name = "EXP_CONTACT_NO")
    private String contact1;

    @Column(name = "EXP_CONTACT_NO2")
    private String contact2;

    @Column(name = "CREATED_BY")
    private String createdBy;

    @Column(name = "CREATED_DATE")
    private Date createdDate;

    @Column(name = "MODIFIED_BY")
    private String modifiedBy;

    @Column(name = "MODIFIED_DATE")
    private Date modifiedDate;

    @Column(name = "EXP_ORG_CODE")
    private String organizationCode;

    @Column(name = "EXP_STATUS")
    private Character status;

    @Column(name = "EXP_LATITUDE")
    private String latitude;

    @Column(name = "EXP_LONGITUDE")
    private String longitude;

    @OneToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "EXP_CODE", referencedColumnName = "EXP_EXD_CODE")
    private ExternalPersonDevice externalPersonDevice;


    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getSurname() {
        return surname;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public String getCategoryCode() {
        return categoryCode;
    }

    public void setCategoryCode(String categoryCode) {
        this.categoryCode = categoryCode;
    }

    public String getInitial() {
        return initial;
    }

    public void setInitial(String initial) {
        this.initial = initial;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getAddress2() {
        return address2;
    }

    public void setAddress2(String address2) {
        this.address2 = address2;
    }

    public String getAddress3() {
        return address3;
    }

    public void setAddress3(String address3) {
        this.address3 = address3;
    }

    public String getContact1() {
        return contact1;
    }

    public void setContact1(String contact1) {
        this.contact1 = contact1;
    }

    public String getContact2() {
        return contact2;
    }

    public void setContact2(String contact2) {
        this.contact2 = contact2;
    }

    public String getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(String createdBy) {
        this.createdBy = createdBy;
    }

    public Date getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(Date createdDate) {
        this.createdDate = createdDate;
    }

    public String getModifiedBy() {
        return modifiedBy;
    }

    public void setModifiedBy(String modifiedBy) {
        this.modifiedBy = modifiedBy;
    }

    public Date getModifiedDate() {
        return modifiedDate;
    }

    public void setModifiedDate(Date modifiedDate) {
        this.modifiedDate = modifiedDate;
    }

    public String getOrganizationCode() {
        return organizationCode;
    }

    public void setOrganizationCode(String organizationCode) {
        this.organizationCode = organizationCode;
    }

    public Character getStatus() {
        return status;
    }

    public void setStatus(Character status) {
        this.status = status;
    }

    public ExternalPersonDevice getExternalPersonDevice() {
        return externalPersonDevice;
    }

    public void setExternalPersonDevice(ExternalPersonDevice externalPersonDevice) {
        this.externalPersonDevice = externalPersonDevice;
    }

    public String getLatitude() {
        return latitude;
    }

    public void setLatitude(String latitude) {
        this.latitude = latitude;
    }

    public String getLongitude() {
        return longitude;
    }

    public void setLongitude(String longitude) {
        this.longitude = longitude;
    }
}

