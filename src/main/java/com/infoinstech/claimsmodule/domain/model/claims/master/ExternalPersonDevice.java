package com.infoinstech.claimsmodule.domain.model.claims.master;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.util.Date;

@Entity
@Table(name = "CL_M_EXT_DEVICE")
public class ExternalPersonDevice {

    public static final String SEQUENCE = "SEQ_CL_M_EXT_DEVICE";
    @Id
    @Column(name = "EXD_SEQ_NO")
    private String sequenceNo;

    @Column(name = "EXD_EXP_CODE")
    private String externalPersonCode;

    @Column(name = "EXD_DEVICE_ORDER")
    private String deviceOrder;

    @Column(name = "EXD_DEVICE_IMEI")
    private String deviceImei;

    @Column(name = "EXD_DEVICE_MODEL")
    private String deviceModel;

    @Column(name = "EXD_DEVICE_TOKEN")
    private String deviceToken;

    @Column(name = "EXD_API_KEY")
    private String gcmKey;

    @Column(name = "EXD_LONGITUDE")
    private Double longitude;

    @Column(name = "EXD_LATITUDE")
    private Double latitude;

    @Column(name = "EXD_LAST_UPDATED_TIME")
    private Date lastUpdatedTime;

    @Column(name = "EXD_STATUS")
    private Character status;

    @Column(name = "EXD_REGISTERED")
    private Character registered;

    @Column(name = "CREATED_BY")
    private String createdBy;

    @Column(name = "CREATED_DATE")
    private Date createdDate;

    @Column(name = "MODIFIED_BY")
    private String modifiedBy;

    @Column(name = "MODIFIED_DATE")
    private Date modifiedDate;

    @Column(name = "EXD_UNREG_BY")
    private String unregisteredBy;

    @Column(name = "EXD_UNREG_DATE")
    private Date unregisteredDate;

    public String getSequenceNo() {
        return sequenceNo;
    }

    public void setSequenceNo(String sequenceNo) {
        this.sequenceNo = sequenceNo;
    }

    public String getExternalPersonCode() {
        return externalPersonCode;
    }

    public void setExternalPersonCode(String externalPersonCode) {
        this.externalPersonCode = externalPersonCode;
    }

    public String getDeviceOrder() {
        return deviceOrder;
    }

    public void setDeviceOrder(String deviceOrder) {
        this.deviceOrder = deviceOrder;
    }

    public String getDeviceImei() {
        return deviceImei;
    }

    public void setDeviceImei(String deviceImei) {
        this.deviceImei = deviceImei;
    }

    public String getDeviceModel() {
        return deviceModel;
    }

    public void setDeviceModel(String deviceModel) {
        this.deviceModel = deviceModel;
    }

    public String getDeviceToken() {
        return deviceToken;
    }

    public void setDeviceToken(String deviceToken) {
        this.deviceToken = deviceToken;
    }

    public String getGcmKey() {
        return gcmKey;
    }

    public void setGcmKey(String gcmKey) {
        this.gcmKey = gcmKey;
    }

    public Double getLongitude() {
        return longitude;
    }

    public void setLongitude(Double longitude) {
        this.longitude = longitude;
    }

    public Double getLatitude() {
        return latitude;
    }

    public void setLatitude(Double latitude) {
        this.latitude = latitude;
    }

    public Date getLastUpdatedTime() {
        return lastUpdatedTime;
    }

    public void setLastUpdatedTime(Date lastUpdatedTime) {
        this.lastUpdatedTime = lastUpdatedTime;
    }

    public Character getStatus() {
        return status;
    }

    public void setStatus(Character status) {
        this.status = status;
    }

    public Character getRegistered() {
        return registered;
    }

    public void setRegistered(Character registered) {
        this.registered = registered;
    }

    public String getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(String createdBy) {
        this.createdBy = createdBy;
    }

    public Date getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(Date createdDate) {
        this.createdDate = createdDate;
    }

    public String getModifiedBy() {
        return modifiedBy;
    }

    public void setModifiedBy(String modifiedBy) {
        this.modifiedBy = modifiedBy;
    }

    public Date getModifiedDate() {
        return modifiedDate;
    }

    public void setModifiedDate(Date modifiedDate) {
        this.modifiedDate = modifiedDate;
    }

    public String getUnregisteredBy() {
        return unregisteredBy;
    }

    public void setUnregisteredBy(String unregisteredBy) {
        this.unregisteredBy = unregisteredBy;
    }

    public Date getUnregisteredDate() {
        return unregisteredDate;
    }

    public void setUnregisteredDate(Date unregisteredDate) {
        this.unregisteredDate = unregisteredDate;
    }
}
