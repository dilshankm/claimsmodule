package com.infoinstech.claimsmodule.domain.model.claims.paramater;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import java.util.Date;

@Entity
@Table(name = "CL_P_NO_FORMAT_PARAM")
public class ClaimsNumberFormat {


    @Id
    @Column(name = "CFP_CODE")
    private String code;

    @NotNull
    @Column(name = "CFP_DESCRIPTION")
    private String description;

    @NotNull
    @Column(name = "CFP_CLAIM_ORD")
    private Integer claimOrder;

    @NotNull
    @Column(name = "CFP_JOB_ORD")
    private Integer jobOrder;

    @NotNull
    @Column(name = "CFP_NOTICE_ORD")
    private Integer noticeOrder;

    @Column(name = "CFP_LENGTH")
    private Integer length;


    @Column(name = "CFP_SEQ_T_NO")
    private String sequenceNo;

    @NotNull
    @Column(name = "CFP_LEG_ORD")
    private String legalOrder;

    @Column(name = "CFP_LEG_LENGTH")
    private Integer legalLenth;

    @Column(name = "CFP_REQ_ORD")
    private Integer requisitionOrder;

    @Column(name = "CFP_REQ_LENGTH")
    private Integer requisitionLength;

    @Column(name = "CFP_INVOICE_NO")
    private String invoiceNo;

    @Column(name = "CFP_PRV_ORD")
    private Integer provisionOrder;

    @Column(name = "CFP_PRV_LENGTH")
    private Integer provisionLength;

    @Column(name = "CFP_REV_ORD")
    private Integer revisionOrder;

    @Column(name = "CFP_REV_LENGTH")
    private Integer revisionLenght;

    @Column(name = "CREATED_BY")
    private String createdBy;

    @Column(name = "CREATED_DATE")
    private Date createdDate;

    @Column(name = "MODIFIED_BY")
    private String modifiedBy;

    @Column(name = "MODIFIED_DATE")
    private Date modifiedDate;

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Integer getClaimOrder() {
        return claimOrder;
    }

    public void setClaimOrder(Integer claimOrder) {
        this.claimOrder = claimOrder;
    }

    public Integer getJobOrder() {
        return jobOrder;
    }

    public void setJobOrder(Integer jobOrder) {
        this.jobOrder = jobOrder;
    }

    public Integer getNoticeOrder() {
        return noticeOrder;
    }

    public void setNoticeOrder(Integer noticeOrder) {
        this.noticeOrder = noticeOrder;
    }

    public Integer getLength() {
        return length;
    }

    public void setLength(Integer length) {
        this.length = length;
    }

    public String getSequenceNo() {
        return sequenceNo;
    }

    public void setSequenceNo(String sequenceNo) {
        this.sequenceNo = sequenceNo;
    }

    public String getLegalOrder() {
        return legalOrder;
    }

    public void setLegalOrder(String legalOrder) {
        this.legalOrder = legalOrder;
    }

    public Integer getLegalLenth() {
        return legalLenth;
    }

    public void setLegalLenth(Integer legalLenth) {
        this.legalLenth = legalLenth;
    }

    public Integer getRequisitionOrder() {
        return requisitionOrder;
    }

    public void setRequisitionOrder(Integer requisitionOrder) {
        this.requisitionOrder = requisitionOrder;
    }

    public Integer getRequisitionLength() {
        return requisitionLength;
    }

    public void setRequisitionLength(Integer requisitionLength) {
        this.requisitionLength = requisitionLength;
    }

    public String getInvoiceNo() {
        return invoiceNo;
    }

    public void setInvoiceNo(String invoiceNo) {
        this.invoiceNo = invoiceNo;
    }

    public Integer getProvisionOrder() {
        return provisionOrder;
    }

    public void setProvisionOrder(Integer provisionOrder) {
        this.provisionOrder = provisionOrder;
    }

    public Integer getProvisionLength() {
        return provisionLength;
    }

    public void setProvisionLength(Integer provisionLength) {
        this.provisionLength = provisionLength;
    }

    public Integer getRevisionOrder() {
        return revisionOrder;
    }

    public void setRevisionOrder(Integer revisionOrder) {
        this.revisionOrder = revisionOrder;
    }

    public Integer getRevisionLenght() {
        return revisionLenght;
    }

    public void setRevisionLenght(Integer revisionLenght) {
        this.revisionLenght = revisionLenght;
    }

    public String getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(String createdBy) {
        this.createdBy = createdBy;
    }

    public Date getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(Date createdDate) {
        this.createdDate = createdDate;
    }

    public String getModifiedBy() {
        return modifiedBy;
    }

    public void setModifiedBy(String modifiedBy) {
        this.modifiedBy = modifiedBy;
    }

    public Date getModifiedDate() {
        return modifiedDate;
    }

    public void setModifiedDate(Date modifiedDate) {
        this.modifiedDate = modifiedDate;
    }
}
