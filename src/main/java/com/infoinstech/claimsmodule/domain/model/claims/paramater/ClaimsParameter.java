package com.infoinstech.claimsmodule.domain.model.claims.paramater;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import java.util.Date;

@Entity
@Table(name = "CL_P_CLAIM_PARAMS")
public class ClaimsParameter {

    @Id
    @Column(name = "CPM_SEQ_T_NO")
    private String sequenceNo;

    @NotNull
    @Column(name = "CPM_BRN_CODE")
    private String branchCode;

    @NotNull
    @Column(name = "CPM_CLA_CODE")
    private String classCode;

    @NotNull
    @Column(name = "CPM_PRD_CODE")
    private String productCode;

    @NotNull
    @Column(name = "CPM_CLAIM_SEQUENCE")
    private String claimSequence;

    @Column(name = "CPM_CLAIM_CHAR")
    private String claimsPrefix;

    @Column(name = "CPM_YEAR")
    private Integer year;

    @Column(name = "CREATED_BY")
    private String createdBy;

    @Column(name = "CREATED_DATE")
    private Date createdDate;

    @Column(name = "MODIFIED_BY")
    private String modifiedBy;

    @Column(name = "MODIFIED_DATE")
    private Date modifiedDate;

    public String getSequenceNo() {
        return sequenceNo;
    }

    public void setSequenceNo(String sequenceNo) {
        this.sequenceNo = sequenceNo;
    }

    public String getBranchCode() {
        return branchCode;
    }

    public void setBranchCode(String branchCode) {
        this.branchCode = branchCode;
    }

    public String getClassCode() {
        return classCode;
    }

    public void setClassCode(String classCode) {
        this.classCode = classCode;
    }

    public String getProductCode() {
        return productCode;
    }

    public void setProductCode(String productCode) {
        this.productCode = productCode;
    }

    public String getClaimSequence() {
        return claimSequence;
    }

    public void setClaimSequence(String claimSequence) {
        this.claimSequence = claimSequence;
    }

    public String getClaimsPrefix() {
        return claimsPrefix;
    }

    public void setClaimsPrefix(String claimsPrefix) {
        this.claimsPrefix = claimsPrefix;
    }

    public Integer getYear() {
        return year;
    }

    public void setYear(Integer year) {
        this.year = year;
    }

    public String getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(String createdBy) {
        this.createdBy = createdBy;
    }

    public Date getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(Date createdDate) {
        this.createdDate = createdDate;
    }

    public String getModifiedBy() {
        return modifiedBy;
    }

    public void setModifiedBy(String modifiedBy) {
        this.modifiedBy = modifiedBy;
    }

    public Date getModifiedDate() {
        return modifiedDate;
    }

    public void setModifiedDate(Date modifiedDate) {
        this.modifiedDate = modifiedDate;
    }
}
