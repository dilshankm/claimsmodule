package com.infoinstech.claimsmodule.domain.model.claims.paramater;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.util.Date;

@Entity
@Table(name = "CL_P_SYS_PARAM")
public class ClaimsSystemParameter {

    @Id
    @Column(name = "SPM_INDEX_NO")
    private String spmIndexNo;

    @Column(name = "SPM_DESCRIPTION")
    private String spmDescription;

    @Column(name = "SPM_NUMBER_VALUE")
    private Double numberValue;

    @Column(name = "SPM_DATE_VALUE")
    private Date dateValue;

    @Column(name = "SPM_CHAR_VALUE")
    private String characterValue;

    @Column(name = "CREATED_BY")
    private String createdBy;

    @Column(name = "CREATED_DATE")
    private Date createdDate;

    @Column(name = "MODIFIED_BY")
    private String modifiedBy;

    @Column(name = "MODIFIED_DATE")
    private Date modifiedDate;

    @Column(name = "SPM_SEQ_T_NO")
    private String spmSeqTNo;

    public String getSpmIndexNo() {
        return spmIndexNo;
    }

    public void setSpmIndexNo(String spmIndexNo) {
        this.spmIndexNo = spmIndexNo;
    }

    public String getSpmDescription() {
        return spmDescription;
    }

    public void setSpmDescription(String spmDescription) {
        this.spmDescription = spmDescription;
    }

    public Double getNumberValue() {
        return numberValue;
    }

    public void setNumberValue(Double numberValue) {
        this.numberValue = numberValue;
    }

    public Date getDateValue() {
        return dateValue;
    }

    public void setDateValue(Date dateValue) {
        this.dateValue = dateValue;
    }

    public String getCharacterValue() {
        return characterValue;
    }

    public void setCharacterValue(String characterValue) {
        this.characterValue = characterValue;
    }

    public String getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(String createdBy) {
        this.createdBy = createdBy;
    }

    public Date getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(Date createdDate) {
        this.createdDate = createdDate;
    }

    public String getModifiedBy() {
        return modifiedBy;
    }

    public void setModifiedBy(String modifiedBy) {
        this.modifiedBy = modifiedBy;
    }

    public Date getModifiedDate() {
        return modifiedDate;
    }

    public void setModifiedDate(Date modifiedDate) {
        this.modifiedDate = modifiedDate;
    }

    public String getSpmSeqTNo() {
        return spmSeqTNo;
    }

    public void setSpmSeqTNo(String spmSeqTNo) {
        this.spmSeqTNo = spmSeqTNo;
    }
}
