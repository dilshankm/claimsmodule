package com.infoinstech.claimsmodule.domain.model.claims.reference;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "CL_R_POLICE_STATION")
public class PolicyStation {

    @Id
    @Column(name = "CPS_CODE")
    private String cpsCode;

    @Column(name = "CPS_POLICE_STATION")
    private String cpsPoliceStation;

    @Column(name = "CPS_ADDRESS")
    private String cpsAddress;

    @Column(name = "CPS_POLICE_DIVISION")
    private String cpsPoliceDivision;

    public String getCpsCode() {
        return cpsCode;
    }

    public void setCpsCode(String cpsCode) {
        this.cpsCode = cpsCode;
    }

    public String getCpsPoliceStation() {
        return cpsPoliceStation;
    }

    public void setCpsPoliceStation(String cpsPoliceStation) {
        this.cpsPoliceStation = cpsPoliceStation;
    }

    public String getCpsAddress() {
        return cpsAddress;
    }

    public void setCpsAddress(String cpsAddress) {
        this.cpsAddress = cpsAddress;
    }

    public String getCpsPoliceDivision() {
        return cpsPoliceDivision;
    }

    public void setCpsPoliceDivision(String cpsPoliceDivision) {
        this.cpsPoliceDivision = cpsPoliceDivision;
    }
}
