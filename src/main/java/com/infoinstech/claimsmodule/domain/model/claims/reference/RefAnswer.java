package com.infoinstech.claimsmodule.domain.model.claims.reference;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "CL_R_QUE_ANSWERS")
public class RefAnswer {

    @Id
    @Column(name = "QDA_CODE")
    private String code;

    @Column(name = "QDA_QUE_CODE")
    private String questionSequenceNo;

    @Column(name = "QDA_ANSWER")
    private String answer;

    @Column(name = "QDA_STATUS")
    private Character status;

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getQuestionSequenceNo() {
        return questionSequenceNo;
    }

    public void setQuestionSequenceNo(String questionSequenceNo) {
        this.questionSequenceNo = questionSequenceNo;
    }

    public String getAnswer() {
        return answer;
    }

    public void setAnswer(String answer) {
        this.answer = answer;
    }

    public Character getStatus() {
        return status;
    }

    public void setStatus(Character status) {
        this.status = status;
    }
}
