package com.infoinstech.claimsmodule.domain.model.claims.reference;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "CL_R_EXCESS_CODE")
public class RefExcessType {

    @Id
    @Column(name = "EXC_CODE")
    private String code;

    @Column(name = "EXC_DESC")
    private String description;

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }
}
