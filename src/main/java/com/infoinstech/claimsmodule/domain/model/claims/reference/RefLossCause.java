package com.infoinstech.claimsmodule.domain.model.claims.reference;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "CL_R_CAUSE_OF_LOSS")
public class RefLossCause {

    @Id
    @Column(name = "CLO_CODE")
    private String code;

    @Column(name = "CLO_DESC")
    private String description;

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }
}
