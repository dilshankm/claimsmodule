package com.infoinstech.claimsmodule.domain.model.claims.reference;

import javax.persistence.*;
import java.util.Date;
import java.util.List;

@Entity
@Table(name = "CL_R_QUESTIONS")
public class RefQuestion {

    @Id
    @Column(name = "QUE_CODE")
    private String code;

    @Column(name = "QUE_DESCRIPTION")
    private String description;

    @Column(name = "QUE_QST_CODE")
    private String type;

    @Column(name = "QUE_MANDATORY")
    private Character mandatory;

    @Column(name = "QUE_HINT")
    private String hint;

    @Column(name = "QUE_HINT_ON")
    private Character hintOn;

    @Column(name = "QUE_ANSWERS")
    private Character defaultAnswers;

    @Column(name = "QUE_STATUS")
    private Character isActive;

    @Column(name = "CREATED_BY")
    private String createdBy;

    @Column(name = "CREATED_DATE")
    private Date createdDate;

    @Column(name = "MODIFIED_BY")
    private String modifiedBy;

    @Column(name = "MODIFIED_DATE")
    private Date modifiedDate;

    @OneToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "QUE_QST_CODE", referencedColumnName = "QST_CODE", insertable = false, updatable = false)
    private RefQuestionType refQuestionType;

    @OneToMany
    @JoinColumn(name = "QDA_QUE_CODE", referencedColumnName = "QUE_CODE", insertable = false, updatable = false)
    private List<RefAnswer> defaultAnswerList;

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public Character getMandatory() {
        return mandatory;
    }

    public void setMandatory(Character mandatory) {
        this.mandatory = mandatory;
    }

    public String getHint() {
        return hint;
    }

    public void setHint(String hint) {
        this.hint = hint;
    }

    public Character getHintOn() {
        return hintOn;
    }

    public void setHintOn(Character hintOn) {
        this.hintOn = hintOn;
    }

    public Character getDefaultAnswers() {
        return defaultAnswers;
    }

    public void setDefaultAnswers(Character defaultAnswers) {
        this.defaultAnswers = defaultAnswers;
    }

    public Character getIsActive() {
        return isActive;
    }

    public void setIsActive(Character isActive) {
        this.isActive = isActive;
    }

    public String getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(String createdBy) {
        this.createdBy = createdBy;
    }

    public Date getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(Date createdDate) {
        this.createdDate = createdDate;
    }

    public String getModifiedBy() {
        return modifiedBy;
    }

    public void setModifiedBy(String modifiedBy) {
        this.modifiedBy = modifiedBy;
    }

    public Date getModifiedDate() {
        return modifiedDate;
    }

    public void setModifiedDate(Date modifiedDate) {
        this.modifiedDate = modifiedDate;
    }

    public RefQuestionType getRefQuestionType() {
        return refQuestionType;
    }

    public void setRefQuestionType(RefQuestionType refQuestionType) {
        this.refQuestionType = refQuestionType;
    }

    public List<RefAnswer> getDefaultAnswerList() {
        return defaultAnswerList;
    }

    public void setDefaultAnswerList(List<RefAnswer> defaultAnswerList) {
        this.defaultAnswerList = defaultAnswerList;
    }
}
