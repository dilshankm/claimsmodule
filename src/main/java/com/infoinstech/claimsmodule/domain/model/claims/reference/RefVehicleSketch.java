package com.infoinstech.claimsmodule.domain.model.claims.reference;

import com.infoinstech.claimsmodule.domain.model.Auditable;

import javax.persistence.*;

/**
 * Created by thilina on 21/12/17.
 */
@Entity
@Table(name = "CL_R_SKETCHES")
public class RefVehicleSketch extends Auditable<String> {

    @Id
    @Column(name = "SKS_CODE")
    private String code;

    @Column(name = "SKS_DESCRIPTION")
    private String description;

    @Lob
    @Column(name = "SKS_IMAGE", nullable = false, columnDefinition = "mediumblob")
    private byte[] image;

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public byte[] getImage() {
        return image;
    }

    public void setImage(byte[] image) {
        this.image = image;
    }
}
