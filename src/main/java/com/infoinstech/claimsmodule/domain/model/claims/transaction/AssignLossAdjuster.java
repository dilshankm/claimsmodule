package com.infoinstech.claimsmodule.domain.model.claims.transaction;

import javax.persistence.*;
import java.util.Date;

@Entity
@Table(name = "CL_T_ASSIGN_LOSS")
public class AssignLossAdjuster {

    public static final String SEQUENCE = "SEQ_CL_T_ASSIGN_LOSS";

    @EmbeddedId
    private AssignLossAdjusterPK assignLossAdjusterPK;

    @Column(name = "AAS_CURRENCY")
    private String currency;

    @Column(name = "AAS_REP_DUE_DT")
    private Date reportDueDate;

    @Column(name = "AAS_SUBMIT_DT")
    private Date submitDate;

    @Column(name = "AAS_TOTAL")
    private Double total;

    @Column(name = "AAS_ASSOS_TYPE")
    private Character externalPersonType;

    @Column(name = "AAS_VISIT_DUE_DT")
    private Date visitDueDate;

    @Column(name = "AAS_CODE")
    private String externalPersonCode;

    @Column(name = "AAS_APPOINTED_DT")
    private Date appointedDate;

    @Column(name = "AAS_COMMENTS")
    private String comments;

    @Column(name = "CREATED_BY")
    private String createdBy;

    @Column(name = "CREATED_DATE")
    private Date createdDate;

    @Column(name = "MODIFIED_BY")
    private String modifiedBy;

    @Column(name = "MODIFIED_DATE")
    private Date modifiedDate;

    @Column(name = "AAS_AUTH_BY")
    private String authorizedBy;

    @Column(name = "AAS_AUTH_DT")
    private Date authorizedDate;

    @Column(name = "AAS_REP_SUBMITTED_DT")
    private Date submittedDate;

    @Column(name = "AAS_VEHICLE_LIST")
    private String vehicleList;

    @Column(name = "AAS_CUR_LOCATION")
    private String currentLocation;

    @Column(name = "AAS_POINT_OF_IMPACT")
    private String pointOfContact;

    @Column(name = "AAS_PROMIS_TIME")
    private String promiseTime;

    @Column(name = "AAS_ASSIGN_STATUS")
    private Character status;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "AAS_INT_SEQ", referencedColumnName = "INT_SEQ_NO", insertable = false, updatable = false)
    private ClaimIntimation claimIntimation;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumns({
            @JoinColumn(name = "AAS_REP_SEQ", referencedColumnName = "AAD_SEQ_NO", insertable = false, updatable = false),
            @JoinColumn(name = "AAS_INT_SEQ", referencedColumnName = "AAD_INT_SEQ", insertable = false, updatable = false)
    })
    private JobAssignment jobAssignment;


    public AssignLossAdjuster() {
    }

    public AssignLossAdjuster(Date createdDate) {

        authorizedBy = "N";
        currency = "LKR";
        this.createdDate = createdDate;
    }

    public AssignLossAdjusterPK getAssignLossAdjusterPK() {
        return assignLossAdjusterPK;
    }

    public void setAssignLossAdjusterPK(AssignLossAdjusterPK assignLossAdjusterPK) {
        this.assignLossAdjusterPK = assignLossAdjusterPK;
    }

    public String getCurrency() {
        return currency;
    }

    public void setCurrency(String currency) {
        this.currency = currency;
    }

    public Date getReportDueDate() {
        return reportDueDate;
    }

    public void setReportDueDate(Date reportDueDate) {
        this.reportDueDate = reportDueDate;
    }

    public Date getSubmitDate() {
        return submitDate;
    }

    public void setSubmitDate(Date submitDate) {
        this.submitDate = submitDate;
    }

    public Double getTotal() {
        return total;
    }

    public void setTotal(Double total) {
        this.total = total;
    }

    public Character getExternalPersonType() {
        return externalPersonType;
    }

    public void setExternalPersonType(Character externalPersonType) {
        this.externalPersonType = externalPersonType;
    }

    public Date getVisitDueDate() {
        return visitDueDate;
    }

    public void setVisitDueDate(Date visitDueDate) {
        this.visitDueDate = visitDueDate;
    }

    public String getExternalPersonCode() {
        return externalPersonCode;
    }

    public void setExternalPersonCode(String externalPersonCode) {
        this.externalPersonCode = externalPersonCode;
    }

    public Date getAppointedDate() {
        return appointedDate;
    }

    public void setAppointedDate(Date appointedDate) {
        this.appointedDate = appointedDate;
    }

    public String getComments() {
        return comments;
    }

    public void setComments(String comments) {
        this.comments = comments;
    }

    public String getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(String createdBy) {
        this.createdBy = createdBy;
    }

    public Date getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(Date createdDate) {
        this.createdDate = createdDate;
    }

    public String getModifiedBy() {
        return modifiedBy;
    }

    public void setModifiedBy(String modifiedBy) {
        this.modifiedBy = modifiedBy;
    }

    public Date getModifiedDate() {
        return modifiedDate;
    }

    public void setModifiedDate(Date modifiedDate) {
        this.modifiedDate = modifiedDate;
    }

    public String getAuthorizedBy() {
        return authorizedBy;
    }

    public void setAuthorizedBy(String authorizedBy) {
        this.authorizedBy = authorizedBy;
    }

    public Date getAuthorizedDate() {
        return authorizedDate;
    }

    public void setAuthorizedDate(Date authorizedDate) {
        this.authorizedDate = authorizedDate;
    }

    public Date getSubmittedDate() {
        return submittedDate;
    }

    public void setSubmittedDate(Date submittedDate) {
        this.submittedDate = submittedDate;
    }

    public String getVehicleList() {
        return vehicleList;
    }

    public void setVehicleList(String vehicleList) {
        this.vehicleList = vehicleList;
    }

    public String getCurrentLocation() {
        return currentLocation;
    }

    public void setCurrentLocation(String currentLocation) {
        this.currentLocation = currentLocation;
    }

    public String getPointOfContact() {
        return pointOfContact;
    }

    public void setPointOfContact(String pointOfContact) {
        this.pointOfContact = pointOfContact;
    }

    public String getPromiseTime() {
        return promiseTime;
    }

    public void setPromiseTime(String promiseTime) {
        this.promiseTime = promiseTime;
    }

    public Character getStatus() {
        return status;
    }

    public void setStatus(Character status) {
        this.status = status;
    }

    public ClaimIntimation getClaimIntimation() {
        return claimIntimation;
    }

    public void setClaimIntimation(ClaimIntimation claimIntimation) {
        this.claimIntimation = claimIntimation;
    }

    public JobAssignment getJobAssignment() {
        return jobAssignment;
    }

    public void setJobAssignment(JobAssignment jobAssignment) {
        this.jobAssignment = jobAssignment;
    }
}

