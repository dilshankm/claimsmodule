package com.infoinstech.claimsmodule.domain.model.claims.transaction;

import javax.persistence.Column;
import javax.persistence.Embeddable;
import java.io.Serializable;

@Embeddable
public class AssignLossAdjusterPK implements Serializable {

    @Column(name = "AAS_INT_SEQ")
    private String intimationSequenceNo;

    @Column(name = "AAS_REP_SEQ")
    private String jobSequenceNo;

    @Column(name = "AAS_SEQ_NO")
    private String sequenceNo;

    public AssignLossAdjusterPK() {
    }

    public AssignLossAdjusterPK(String intimationSequenceNo, String jobSequenceNo, String sequenceNo) {
        this.intimationSequenceNo = intimationSequenceNo;
        this.jobSequenceNo = jobSequenceNo;
        this.sequenceNo = sequenceNo;
    }

    public String getIntimationSequenceNo() {
        return intimationSequenceNo;
    }

    public void setIntimationSequenceNo(String intimationSequenceNo) {
        this.intimationSequenceNo = intimationSequenceNo;
    }

    public String getJobSequenceNo() {
        return jobSequenceNo;
    }

    public void setJobSequenceNo(String jobSequenceNo) {
        this.jobSequenceNo = jobSequenceNo;
    }

    public String getSequenceNo() {
        return sequenceNo;
    }

    public void setSequenceNo(String sequenceNo) {
        this.sequenceNo = sequenceNo;
    }
}