package com.infoinstech.claimsmodule.domain.model.claims.transaction;

import com.infoinstech.claimsmodule.domain.model.claims.reference.RefQuestion;

import javax.persistence.*;
import java.util.Date;

@Entity
@Table(name = "CL_T_ASSIGN_QUESTIONS")
public class AssignQuestion {

    public static final String SEQUENCE = "SEQ_CL_T_ASSIGN_QUESTIONS";
    @Id
    @Column(name = "ASQ_SEQ_NO")
    private String sequenceNo;

    @Column(name = "ASQ_QUE_CODE")
    private String questionCode;

    @Column(name = "ASQ_QUE_DESCRIPTION")
    private String description;

    @Column(name = "ASQ_ANSWER")
    private String answer;

    @Column(name = "ASQ_QDA_CODE")
    private String answerCode;

    @Column(name = "ASQ_AAD_SEQ_NO")
    private String jobAssignmentSequenceNo;

    @Column(name = "ASQ_AAD_JOB_NO")
    private String jobAssignmentNo;

    @Column(name = "ASQ_INT_SEQ_NO")
    private String claimSequenceNo;

    @Column(name = "ASQ_NAD_SEQ_NO")
    private String notificationAssignmentSequenceNo;

    @Column(name = "ASQ_NAD_JOB_NO")
    private String notificationAssignmentNo;

    @Column(name = "ASQ_NOT_SEQ_NO")
    private String notificationSequenceNo;

    @Column(name = "ASQ_REV_NO")
    private String revisionNo;

    @Column(name = "CREATED_BY")
    private String createdBy;

    @Column(name = "CREATED_DATE")
    private Date createdDate;

    @Column(name = "MODIFIED_BY")
    private String modifiedBy;

    @Column(name = "MODIFIED_DATE")
    private Date modifiedDate;

    @OneToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "ASQ_QUE_CODE", referencedColumnName = "QUE_CODE", insertable = false, updatable = false)
    private RefQuestion refQuestion;


    public String getSequenceNo() {
        return sequenceNo;
    }

    public void setSequenceNo(String sequenceNo) {
        this.sequenceNo = sequenceNo;
    }

    public String getQuestionCode() {
        return questionCode;
    }

    public void setQuestionCode(String questionCode) {
        this.questionCode = questionCode;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getAnswer() {
        return answer;
    }

    public void setAnswer(String answer) {
        this.answer = answer;
    }

    public String getAnswerCode() {
        return answerCode;
    }

    public void setAnswerCode(String answerCode) {
        this.answerCode = answerCode;
    }

    public String getJobAssignmentSequenceNo() {
        return jobAssignmentSequenceNo;
    }

    public void setJobAssignmentSequenceNo(String jobAssignmentSequenceNo) {
        this.jobAssignmentSequenceNo = jobAssignmentSequenceNo;
    }

    public String getJobAssignmentNo() {
        return jobAssignmentNo;
    }

    public void setJobAssignmentNo(String jobAssignmentNo) {
        this.jobAssignmentNo = jobAssignmentNo;
    }

    public String getClaimSequenceNo() {
        return claimSequenceNo;
    }

    public void setClaimSequenceNo(String claimSequenceNo) {
        this.claimSequenceNo = claimSequenceNo;
    }

    public String getNotificationAssignmentSequenceNo() {
        return notificationAssignmentSequenceNo;
    }

    public void setNotificationAssignmentSequenceNo(String notificationAssignmentSequenceNo) {
        this.notificationAssignmentSequenceNo = notificationAssignmentSequenceNo;
    }

    public String getNotificationAssignmentNo() {
        return notificationAssignmentNo;
    }

    public void setNotificationAssignmentNo(String notificationAssignmentNo) {
        this.notificationAssignmentNo = notificationAssignmentNo;
    }

    public String getNotificationSequenceNo() {
        return notificationSequenceNo;
    }

    public void setNotificationSequenceNo(String notificationSequenceNo) {
        this.notificationSequenceNo = notificationSequenceNo;
    }

    public String getRevisionNo() {
        return revisionNo;
    }

    public void setRevisionNo(String revisionNo) {
        this.revisionNo = revisionNo;
    }

    public String getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(String createdBy) {
        this.createdBy = createdBy;
    }

    public Date getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(Date createdDate) {
        this.createdDate = createdDate;
    }

    public String getModifiedBy() {
        return modifiedBy;
    }

    public void setModifiedBy(String modifiedBy) {
        this.modifiedBy = modifiedBy;
    }

    public Date getModifiedDate() {
        return modifiedDate;
    }

    public void setModifiedDate(Date modifiedDate) {
        this.modifiedDate = modifiedDate;
    }

    public RefQuestion getRefQuestion() {
        return refQuestion;
    }

    public void setRefQuestion(RefQuestion refQuestion) {
        this.refQuestion = refQuestion;
    }
}
