package com.infoinstech.claimsmodule.domain.model.claims.transaction;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import java.util.Date;

@Entity
@Table(name = "CL_T_CURTASK")
public class ClaimCurrentTask {

    public static final String SEQUENCE = "SEQ_CL_T_CURTASK";
    @Id
    @Column(name = "CUR_SEQ_NO")
    private String sequenceNo;

    @NotNull
    @Column(name = "CUR_INT_SEQNO")
    private String claimSequenceNo;

    @NotNull
    @Column(name = "CUR_CLAIM_NO")
    private String claimNo;

    @Column(name = "CUR_TASK_CODE")
    private String taskCode;

    @Column(name = "CREATED_BY")
    private String createdBy;

    @Column(name = "CREATED_DATE")
    private Date createdDate;

    @Column(name = "MODIFIED_BY")
    private String modifiedBy;

    @Column(name = "MODIFIED_DATE")
    private Date modifiedDate;

    @Column(name = "CUR_LOSS_COMMENT")
    private String lossComments;

    @Column(name = "CUR_CLAIM_COMMENT")
    private String claimComments;


    public String getSequenceNo() {
        return sequenceNo;
    }

    public void setSequenceNo(String sequenceNo) {
        this.sequenceNo = sequenceNo;
    }

    public String getClaimSequenceNo() {
        return claimSequenceNo;
    }

    public void setClaimSequenceNo(String claimSequenceNo) {
        this.claimSequenceNo = claimSequenceNo;
    }

    public String getClaimNo() {
        return claimNo;
    }

    public void setClaimNo(String claimNo) {
        this.claimNo = claimNo;
    }

    public String getTaskCode() {
        return taskCode;
    }

    public void setTaskCode(String taskCode) {
        this.taskCode = taskCode;
    }

    public String getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(String createdBy) {
        this.createdBy = createdBy;
    }

    public Date getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(Date createdDate) {
        this.createdDate = createdDate;
    }

    public String getModifiedBy() {
        return modifiedBy;
    }

    public void setModifiedBy(String modifiedBy) {
        this.modifiedBy = modifiedBy;
    }

    public Date getModifiedDate() {
        return modifiedDate;
    }

    public void setModifiedDate(Date modifiedDate) {
        this.modifiedDate = modifiedDate;
    }

    public String getLossComments() {
        return lossComments;
    }

    public void setLossComments(String lossComments) {
        this.lossComments = lossComments;
    }

    public String getClaimComments() {
        return claimComments;
    }

    public void setClaimComments(String claimComments) {
        this.claimComments = claimComments;
    }
}

