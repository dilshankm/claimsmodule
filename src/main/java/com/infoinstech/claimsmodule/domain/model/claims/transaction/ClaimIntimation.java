package com.infoinstech.claimsmodule.domain.model.claims.transaction;

import com.infoinstech.claimsmodule.domain.model.claims.reference.RefInformationMode;
import com.infoinstech.claimsmodule.domain.model.claims.reference.RefLossCause;
import com.infoinstech.claimsmodule.domain.model.salesmarketing.master.SalesLocation;
import com.infoinstech.claimsmodule.domain.model.underwriting.master.Customer;
import com.infoinstech.claimsmodule.domain.model.underwriting.master.Product;
import com.infoinstech.claimsmodule.domain.model.underwriting.reference.RefClass;
import lombok.Data;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;
import java.util.List;

@Data
@Entity
@Table(name = "CL_T_INTIMATION")
@NamedStoredProcedureQueries(
        {
                @NamedStoredProcedureQuery(
                        name = "PU_GEN_CLAIM_NO", procedureName = "PK_CL_CALL_CENTER.PU_GEN_CLAIM_NONEW",
                        parameters = {
                                @StoredProcedureParameter(mode = ParameterMode.IN, name = "WKBRANCH", type = String.class),
                                @StoredProcedureParameter(mode = ParameterMode.IN, name = "WKCLASS", type = String.class),
                                @StoredProcedureParameter(mode = ParameterMode.IN, name = "WKPRODUCT", type = String.class),
                                @StoredProcedureParameter(mode = ParameterMode.REF_CURSOR, name = "cv_results", type = void.class)

                        }
                )


        })
public class ClaimIntimation implements Serializable {

    public static final String SEQUENCE = "SEQ_CL_T_INTIMATION";

    @Id
    @Column(name = "INT_SEQ_NO")
    private String sequenceNo;

    @Column(name = "INT_CLAIM_NO")
    private String claimNo;

    @Column(name = "INT_POLICY_SEQ")
    private String policySequenceNo;

    @Column(name = "INT_DATE_DESC")
    private String dateDescription;

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "INT_INITIMATE_DT")
    private Date intimationDate;

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "INT_DATE_LOSs")
    private Date lossDate;

    @Column(name = "INT_MODE")
    private String modeOfInformation;

    @Column(name = "INT_LOCATION")
    private String location;

    @Column(name = "INT_EVENT_CODE")
    private String eventCode;

    @Column(name = "INT_INSPECTION")
    private Character claimInspected;

    @Column(name = "INT_ESTIMATE_AMT")
    private Double estimatedAmount;

    @Column(name = "INT_PLACE_LOSS")
    private String placeOfLoss;

    @Column(name = "INT_CONT_ADDRESS")
    private String contactAddress;

    @Column(name = "INT_CONT_NO")
    private String contactNo;

    @Column(name = "INT_CONT_FAX")
    private String contactFax;

    @Column(name = "INT_CONT_EMAIL")
    private String contactEmail;

    @Column(name = "INT_ACCIDENT_DESC")
    private String accidentDescription;

    @Column(name = "INT_INFORM_PERSON")
    private String informPerson;

    @Column(name = "INT_CLI_NO")
    private String cliNo;

    @Column(name = "INT_COMMENTS")
    private String comments;

    @Column(name = "INT_CLAIMED_AMT")
    private Double claimedAmount;

    @Column(name = "INT_EXCESS")
    private Double excessAmount;

    @Column(name = "INT_PARTIAL_PAY")
    private String partialPayment;

    @Column(name = "INT_SALVAGE_VAL")
    private String salvageValue;

    @Column(name = "INT_LOSS_REMARKS")
    private String lossRemarks;

    @Column(name = "INT_PAYABLE_AMT")
    private Double payableAmount;

    @Column(name = "INT_CURRENCY")
    private String currency;

    @Column(name = "INT_CONTACT_PER")
    private String contactPerson;

    @Column(name = "INT_CAUSE_LOSS_CODE")
    private String causeOfLoss;

    @Column(name = "INT_SURVEY_TYPE")
    private String surveyType;

    @Column(name = "INT_POLICY_NO")
    private String policyNo;

    @Column(name = "INT_PRS_SEQ_NO")
    private String riskSequenceNo;

    @Column(name = "INT_CLASS_CODE")
    private String classCode;

    @Column(name = "INT_PROD_CODE")
    private String productCode;

    @Column(name = "INT_CUS_CODE")
    private String customerCode;

    @Column(name = "INT_SUM_INSURED")
    private Double sumInsured;

    @Column(name = "INT_PERIOD_FROM")
    private Date periodFrom;

    @Column(name = "INT_PERIOD_TO")
    private Date periodTo;

    @Column(name = "INT_BRANCH_CODE")
    private String branchCode;

    @Column(name = "INT_STATUS")
    private Character claimStatus;

    @Column(name = "CREATED_BY")
    private String createdBy;

    @Column(name = "CREATED_DATE")
    private Date createdDate;

    @Column(name = "MODIFIED_BY")
    private String modifiedBy;

    @Column(name = "MODIFIED_DATE")
    private Date modifiedDate;

    @Column(name = "INT_PLC_SEQ_NO")
    private String locationSeqNo;

    @Column(name = "INT_POL_BRANCH")
    private String policyBranchCode;

//    @Column(name = "INT_RISK_SEQ")
//    private String riskOrder;

    @Column(name = "INT_PRS_R_SEQ")
    private String riskOrderNo;

    @Column(name = "INT_PRS_TITLE")
    private String riskTitle;

    @Column(name = "INT_PRS_NAME")
    private String riskName;

    @Column(name = "INT_RET_OCCUPATION_CODE")
    private String occupationCode;

    @Column(name = "INT_CLAIMS_PAID")
    private Double claimsPaid;

    @Column(name = "INT_SECTION_CODE")
    private String sectionCode;

    @Column(name = "INT_CSD_REF_NO")
    private String csdRefNo;

    @Column(name = "INT_CANCEL_REMARK")
    private String cancellationRemarks;

    @Column(name = "INT_CANCEL_DATE")
    private Date cancelledDate;

    @Column(name = "INT_TOTAL_LOSS")
    private Character totalLoss;

    @Column(name = "INT_GRACE_PERIOD")
    private Double gracePeriod;

    @Column(name = "INT_RISK_SUM_INSURED")
    private Double riskSumInsured;

    @Column(name = "INT_MULTI_RISKS")
    private Character multiRisks;

    @Column(name = "INT_DEBT_OUT")
    private Double outstandingDebitAmount;

    @Column(name = "INT_CHQ_OUT")
    private Double outstandingChequeAmount;

    @Column(name = "INT_ADD_SEQ")
    private String addressRefSeq;

    @Column(name = "INT_TOT_PROVISION")
    private Double totalProvision;

    @Column(name = "INT_TOT_REVISION")
    private Double totalRevision;

    @Column(name = "INT_INTIMATED_CAUSE_OF_LOSS")
    private String causeOfLossSpecifiedAtClaim;

    @Column(name = "INT_INTIMATED_DATE_OF_LOSS")
    private Date dateOfLossSpecifiedAtClaim;

    @Column(name = "INT_REIN_INFORM")
    private Character reinsuranceInformed;

    @Column(name = "INT_REIN_PERIL")
    private String reinsurancePeril;

    @Column(name = "INT_RCT_EXESS")
    private Double excessValue;

    @Column(name = "INT_RCT_EXESS_PAID")
    private Double excessPaidValue;

    @Column(name = "INT_DRIVER_NAME")
    private String driverName;

    @Column(name = "INT_DRIVER_LICEN_NO")
    private String driverLicenseNo;

    @Column(name = "INT_LICEN_FROM_DT")
    private Date licenseFromDate;

    @Column(name = "INT_LICEN_TO_DT")
    private Date licenseToDate;

    @Column(name = "INT_REIN_REC")
    private Character reinsuranceRecovered;

    @Column(name = "INT_CLOSE_EFFECT_DATE")
    private Date claimFileClosureEffectiveDate;

    @Column(name = "INT_INSURED_AT_FAULT")
    private Character insuredAtFault;

    @Column(name = "INT_CLAIM_DOUBTFUL")
    private Character claimDoubtful;

    @Column(name = "INT_CCT_CODE")
    private String claimCategory;

    @Column(name = "INT_CVT_CODE")
    private String vehicleTypeCode;

    @Column(name = "INT_CAC_CODE")
    private String attendanceCircumstanceCode;

    @Column(name = "INT_DRIVER_AGE")
    private Double driversAge;

    @Column(name = "INT_DRIVER_SEX")
    private Character driversSex;

    @Column(name = "INT_POLICE_STATION")
    private String policeStation;

    @Column(name = "INT_INJURED_PERSONS")
    private String injuredPersons;

    @Column(name = "INT_CONT_AMIABLE_FLG")
    private Character notApplicable;

    @Column(name = "INT_BSS_CODE")
    private String businessPartyTypeCode;

    @Column(name = "INT_CLOSE_BATCH_NO")
    private String closeBatchNo;

    @Column(name = "INT_WRK_HRS")
    private Character workHours;

    @Column(name = "INT_CATPROC_FLAG")
    private Character catastrophicProvisionApplicable;

    @Column(name = "INT_CATPROC_PROV_FLAG")
    private Character catastrophicProvisionApplied;

    @Column(name = "INT_POLICE_CODE")
    private String policeStationCode;

    @Column(name = "INT_ME_CODE")
    private String marketingExecutiveCode;

    @Column(name = "INT_AAS_CODE")
    private String assignedLossAdjusterCode;

    @Column(name = "INT_END_NO")
    private String endorsementNo;

    @Column(name = "INT_DRIVER_ID")
    private String driverId;

    @Column(name = "INT_DRIVER_CONT_NO")
    private String driverContactNo;

    @Column(name = "INT_LOSS_TIME")
    private String lossTime;

    @Column(name = "INT_UW_YEAR")
    private String underwritingYear;

    @Column(name = "INT_REIN_PROV")
    private Character reinsuranceProvisioned;

    @Column(name = "INT_WEB_TRACK_NO")
    private String webTrackingNo;

    @Column(name = "INT_POLICE_DIVISION")
    private String policeDivision;

    @Column(name = "INT_CALL_CENTER")
    private Character callCenterFlag;

    @Column(name = "INT_LICEN_CATEGORY")
    private String licenseCategory;

    @Column(name = "INT_LONGITUDE")
    private String longitude;

    @Column(name = "INT_LATITUDE")
    private String latitude;

    @Column(name = "INT_POLICE_REPORT")
    private Character policeReport;

    @Column(name = "INT_RELATION")
    private String relationshipToInsured;

    @Column(name = "INT_RELATION_DESC")
    private String relationshipToInsuredDescription;

    @Column(name = "INT_THIRD_PARTY_DTLS")
    private String thirdPartyDetails;

    @Column(name = "INT_ACTION_TAKEN")
    private String actionTaken;

    @Column(name = "INT_NOT_SEQ_NO")
    private String notificationSequenceNo;

    @Column(name = "INT_NOT_NOT_NO")
    private String notificationNo;

    @Column(name = "INT_BPARTY_CODE")
    private String bPartyCode;

    @Column(name = "INT_COR_CODE")
    private String corCode;

    @OneToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "INT_CUS_CODE", referencedColumnName = "CUS_CODE", insertable = false, updatable = false)
    private Customer customer;

    @OneToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "INT_CAUSE_LOSS_CODE", referencedColumnName = "CLO_CODE", insertable = false, updatable = false)
    private RefLossCause refLossCause;

    @OneToMany(fetch = FetchType.LAZY)
    @JoinColumn(name = "MRD_INT_SEQ", referencedColumnName = "INT_SEQ_NO", insertable = false, updatable = false)
    private List<ProvisionPaymentDetails> provisionPaymentDetailsList;

    @OneToOne
    @JoinColumn(name = "INT_MODE", referencedColumnName = "MDE_CODE", insertable = false, updatable = false)
    private RefInformationMode refInformationMode;

    @OneToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "INT_PROD_CODE", referencedColumnName = "PRD_CODE", insertable = false, updatable = false)
    private Product product;

    @OneToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "INT_POL_BRANCH", referencedColumnName = "SLC_BRN_CODE", insertable = false, updatable = false)
    private SalesLocation policyBranch;

    @OneToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "INT_CLASS_CODE", referencedColumnName = "CLA_CODE", insertable = false, updatable = false)
    private RefClass refClass;
//    INT_CALL_CENTER VARCHAR(1),
//    INT_LICEN_CATEGORY VARCHAR(20),
//    INT_LONGITUDE VARCHAR(20),
//    INT_LATITUDE VARCHAR(20),
//    INT_POLICE_REPORT VARCHAR(1),
//    INT_RELATION VARCHAR(20),
//    INT_RELATION_DESC VARCHAR(100),
//    INT_THIRD_PARTY_DTLS VARCHAR(100)

    public ClaimIntimation() {
    }

    public ClaimIntimation(Date createdDate) {

        intimationDate = createdDate;
        claimInspected = 'Y';
        branchCode = "HEAD";
        multiRisks = 'N';
        reinsuranceInformed = 'N';
        claimFileClosureEffectiveDate = createdDate;
        workHours = 'A';
        totalLoss = 'N';
        reinsuranceProvisioned = 'N';
        notApplicable = 'N';
        claimStatus = 'O';
    }


}
