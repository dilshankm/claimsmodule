package com.infoinstech.claimsmodule.domain.model.claims.transaction;

import com.infoinstech.claimsmodule.domain.model.claims.reference.RefLossCause;

import javax.persistence.*;
import java.util.Date;
import java.util.List;

@Entity
@Table(name = "CL_T_NOTIFICATION")
public class ClaimNotification {

    public static String SEQUENCE = "SEQ_CL_T_NOTIFICATION";

    @Id
    @Column(name = "NOT_SEQ_NO")
    private String sequenceNo;

    @Column(name = "NOT_NOTIFICATION_NO")
    private String notificationNo;

    @Column(name = "NOT_COVER_NOTE_NO")
    private String coverNoteNo;

    @Column(name = "NOT_DATE_DESC")
    private String dateDescription;

    @Column(name = "NOT_NOTIFIED_DT")
    private Date notifiedDate;

    @Column(name = "NOT_DATE_LOSS")
    private Date lossDate;

    @Column(name = "NOT_MODE")
    private String modeOfInformation;

    @Column(name = "NOT_LOCATION")
    private String location;

    @Column(name = "NOT_EVENT_CODE")
    private String eventCode;

    @Column(name = "NOT_INSPECTION")
    private Character claimInspected;

    @Column(name = "NOT_ESTIMATE_AMT")
    private Double estimatedAmount;

    @Column(name = "NOT_PLACE_LOSS")
    private String placeOfLoss;

    @Column(name = "NOT_CONT_ADDRESS")
    private String contactAddress;

    @Column(name = "NOT_CONT_NO")
    private String contactNo;

    @Column(name = "NOT_CONT_FAX")
    private String contactFax;

    @Column(name = "NOT_ACCIDENT_DESC")
    private String accidentDescription;

    @Column(name = "NOT_INFORM_PERSON")
    private String informPerson;

    @Column(name = "NOT_CLI_NO")
    private String cliNo;

    @Column(name = "NOT_CONT_EMAIL")
    private String contactEmail;

    @Column(name = "NOT_COMMENTS")
    private String comments;

    @Column(name = "NOT_CLAIMED_AMT")
    private Double claimedAmount;

    @Column(name = "NOT_EXCESS")
    private Double excessAmount;

    @Column(name = "NOT_PARTIAL_PAY")
    private String partialPayment;

    @Column(name = "NOT_SALVAGE_VAL")
    private String salvageValue;

    @Column(name = "NOT_LOSS_REMARKS")
    private String lossRemarks;

    @Column(name = "NOT_PAYABLE_AMT")
    private Double payableAmount;

    @Column(name = "NOT_CURRENCY")
    private String currency;

    @Column(name = "NOT_CONTACT_PER")
    private String contactPerson;

    @Column(name = "NOT_CAUSE_LOSS_CODE")
    private String causeOfLoss;

    @Column(name = "NOT_SURVEY_TYPE")
    private String surveyType;

    @Column(name = "NOT_CLASS_CODE")
    private String classCode;

    @Column(name = "NOT_PROD_CODE")
    private String coverNoteType;

    @Column(name = "NOT_CUS_CODE")
    private String customerCode;

    @Column(name = "NOT_SUM_INSURED")
    private Double sumInsured;

    @Column(name = "NOT_PERIOD_FROM")
    private Date periodFrom;

    @Column(name = "NOT_PERIOD_TO")
    private Date periodTo;

    @Column(name = "NOT_BRANCH_CODE")
    private String branchCode;

    @Column(name = "NOT_STATUS")
    private Character status;

    @Column(name = "CREATED_BY")
    private String createdBy;

    @Column(name = "CREATED_DATE")
    private Date createdDate;

    @Column(name = "MODIFIED_BY")
    private String modifiedBy;

    @Column(name = "MODIFIED_DATE")
    private Date modifiedDate;

    @Column(name = "NOT_RISK_NAME")
    private String riskName;

    @Column(name = "NOT_CLAIMS_PAID")
    private Double claimsPaid;

    @Column(name = "NOT_CSD_REF_NO")
    private String csdRefNo;

    @Column(name = "NOT_CANCEL_REMARK")
    private String cancellationRemarks;

    @Column(name = "NOT_CANCEL_DATE")
    private Date cancelledDate;

    @Column(name = "NOT_TOTAL_LOSS")
    private Character totalLoss;

    @Column(name = "NOT_GRACE_PERIOD")
    private Double gracePeriod;

    @Column(name = "NOT_RISK_SUM_INSURED")
    private Double riskSumInsured;

    @Column(name = "NOT_MULTI_RISKS")
    private Character multiRisks;

    @Column(name = "NOT_DEBT_OUT")
    private Double outstandingDebitAmount;

    @Column(name = "NOT_CHQ_OUT")
    private Double outstandingChequeAmount;

    @Column(name = "NOT_TOT_PROVISION")
    private Double totalProvision;

    @Column(name = "NOT_TOT_REVISION")
    private Double totalRevision;

    @Column(name = "NOT_NOTIFIED_CAUSE_OF_LOSS")
    private String causeOfLossSpecifiedAtClaim;

    @Column(name = "NOT_NOTIFIED_DATE_OF_LOSS")
    private Date dateOfLossSpecifiedAtClaim;

    @Column(name = "NOT_REIN_INFORM")
    private Character reinsuranceInformed;

    @Column(name = "NOT_REIN_PERIL")
    private String reinsurancePeril;

    @Column(name = "NOT_RCT_EXESS")
    private Double excessValue;

    @Column(name = "NOT_RCT_EXESS_PAID")
    private Double excessPaidValue;

    @Column(name = "NOT_DRIVER_NAME")
    private String driverName;

    @Column(name = "NOT_DRIVER_LICEN_NO")
    private String driverLicenseNo;

    @Column(name = "NOT_LICEN_FROM_DT")
    private Date licenseFromDate;

    @Column(name = "NOT_LICEN_TO_DT")
    private Date licenseToDate;

    @Column(name = "NOT_CLOSE_EFFECT_DATE")
    private Date claimFileClosureEffectiveDate;

    @Column(name = "NOT_INSURED_AT_FAULT")
    private Character insuredAtFault;

    @Column(name = "NOT_CLAIM_DOUBTFUL")
    private Character claimDoubtful;

    @Column(name = "NOT_CCT_CODE")
    private String claimCategory;

    @Column(name = "NOT_CVT_CODE")
    private String vehicleTypeCode;

    @Column(name = "NOT_CAC_CODE")
    private String attendanceCircumstanceCode;

    @Column(name = "NOT_DRIVER_AGE")
    private Double driversAge;

    @Column(name = "NOT_DRIVER_SEX")
    private Character driversSex;

    @Column(name = "NOT_POLICE_STATION")
    private String policeStation;

    @Column(name = "NOT_INJURED_PERSONS")
    private String injuredPersons;

    @Column(name = "NOT_CONT_AMIABLE_FLG")
    private Character notApplicable;

    @Column(name = "NOT_BSS_CODE")
    private String businessPartyTypeCode;

    @Column(name = "NOT_CLOSE_BATCH_NO")
    private String closeBatchNo;

    @Column(name = "NOT_WRK_HRS")
    private Character workHours;

    @Column(name = "NOT_POLICE_CODE")
    private String policeStationCode;

    @Column(name = "NOT_ME_CODE")
    private String marketingExecutiveCode;

    @Column(name = "NOT_AAS_CODE")
    private String assignedLossAdjusterCode;

    @Column(name = "NOT_END_NO")
    private String endorsementNo;

    @Column(name = "NOT_DRIVER_ID")
    private String driverId;

    @Column(name = "NOT_LOSS_TIME")
    private String lossTime;

    @Column(name = "NOT_UW_YEAR")
    private String underwritingYear;

    @Column(name = "NOT_WEB_TRACK_NO")
    private String webTrackingNo;

    @Column(name = "NOT_POLICE_DIVISION")
    private String policeDivision;

    @Column(name = "NOT_LATITUDE")
    private String latitude;

    @Column(name = "NOT_LONGITUDE")
    private String longitude;

    @Column(name = "NOT_DRIVER_CONT_NO")
    private String driverContactNo;

    @Column(name = "NOT_CALL_CENTER")
    private Character callCenterFlag;

    @Column(name = "NOT_LICEN_CATEGORY")
    private String licenseCategory;

    @Column(name = "NOT_POLICE_REPORT")
    private Character policeReport;

    @Column(name = "NOT_RELATION")
    private String relationshipToInsured;

    @Column(name = "NOT_RELATION_DESC")
    private String relationshipToInsuredDescription;

    @Column(name = "NOT_THIRD_PARTY_DTLS")
    private String thirdPartyDetails;

    @Column(name = "NOT_ACTION_TAKEN")
    private String actionTaken;

    @Column(name = "NOT_COV_NT_START_DATE")
    private Date effectiveStartDate;

    @Column(name = "NOT_COV_NT_MANU_YEAR")
    private String yearOfManufacture;

    @Column(name = "NOT_COV_NT_MAKE_MODEL")
    private String makeAndModel;

    @Column(name = "NOT_HANDLER_CODE")
    private String handlerCode;

    @Column(name = "NOT_HANDLER_NAME")
    private String handlerName;

    @OneToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "NOT_CAUSE_LOSS_CODE", referencedColumnName = "CLO_CODE", insertable = false, updatable = false)
    private RefLossCause refLossCause;

    @OneToMany(fetch = FetchType.LAZY)
    @JoinColumn(name = "NAD_NOT_SEQ", referencedColumnName = "NOT_SEQ_NO", insertable = false, updatable = false)
    private List<NotificationAssignment> notificationAssignments;


    public String getSequenceNo() {
        return sequenceNo;
    }

    public void setSequenceNo(String sequenceNo) {
        this.sequenceNo = sequenceNo;
    }

    public String getNotificationNo() {
        return notificationNo;
    }

    public void setNotificationNo(String notificationNo) {
        this.notificationNo = notificationNo;
    }

    public String getCoverNoteNo() {
        return coverNoteNo;
    }

    public void setCoverNoteNo(String coverNoteNo) {
        this.coverNoteNo = coverNoteNo;
    }

    public String getDateDescription() {
        return dateDescription;
    }

    public void setDateDescription(String dateDescription) {
        this.dateDescription = dateDescription;
    }


    public Date getLossDate() {
        return lossDate;
    }

    public void setLossDate(Date lossDate) {
        this.lossDate = lossDate;
    }

    public String getModeOfInformation() {
        return modeOfInformation;
    }

    public void setModeOfInformation(String modeOfInformation) {
        this.modeOfInformation = modeOfInformation;
    }

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    public String getEventCode() {
        return eventCode;
    }

    public void setEventCode(String eventCode) {
        this.eventCode = eventCode;
    }

    public Character getClaimInspected() {
        return claimInspected;
    }

    public void setClaimInspected(Character claimInspected) {
        this.claimInspected = claimInspected;
    }

    public Double getEstimatedAmount() {
        return estimatedAmount;
    }

    public void setEstimatedAmount(Double estimatedAmount) {
        this.estimatedAmount = estimatedAmount;
    }

    public String getPlaceOfLoss() {
        return placeOfLoss;
    }

    public void setPlaceOfLoss(String placeOfLoss) {
        this.placeOfLoss = placeOfLoss;
    }

    public String getContactAddress() {
        return contactAddress;
    }

    public void setContactAddress(String contactAddress) {
        this.contactAddress = contactAddress;
    }

    public String getContactNo() {
        return contactNo;
    }

    public void setContactNo(String contactNo) {
        this.contactNo = contactNo;
    }

    public String getContactFax() {
        return contactFax;
    }

    public void setContactFax(String contactFax) {
        this.contactFax = contactFax;
    }

    public String getContactEmail() {
        return contactEmail;
    }

    public void setContactEmail(String contactEmail) {
        this.contactEmail = contactEmail;
    }

    public String getAccidentDescription() {
        return accidentDescription;
    }

    public void setAccidentDescription(String accidentDescription) {
        this.accidentDescription = accidentDescription;
    }

    public String getInformPerson() {
        return informPerson;
    }

    public void setInformPerson(String informPerson) {
        this.informPerson = informPerson;
    }

    public String getCliNo() {
        return cliNo;
    }

    public void setCliNo(String cliNo) {
        this.cliNo = cliNo;
    }

    public String getComments() {
        return comments;
    }

    public void setComments(String comments) {
        this.comments = comments;
    }

    public Double getClaimedAmount() {
        return claimedAmount;
    }

    public void setClaimedAmount(Double claimedAmount) {
        this.claimedAmount = claimedAmount;
    }

    public Double getExcessAmount() {
        return excessAmount;
    }

    public void setExcessAmount(Double excessAmount) {
        this.excessAmount = excessAmount;
    }

    public String getPartialPayment() {
        return partialPayment;
    }

    public void setPartialPayment(String partialPayment) {
        this.partialPayment = partialPayment;
    }

    public String getSalvageValue() {
        return salvageValue;
    }

    public void setSalvageValue(String salvageValue) {
        this.salvageValue = salvageValue;
    }

    public String getLossRemarks() {
        return lossRemarks;
    }

    public void setLossRemarks(String lossRemarks) {
        this.lossRemarks = lossRemarks;
    }

    public Double getPayableAmount() {
        return payableAmount;
    }

    public void setPayableAmount(Double payableAmount) {
        this.payableAmount = payableAmount;
    }

    public String getCurrency() {
        return currency;
    }

    public void setCurrency(String currency) {
        this.currency = currency;
    }

    public String getContactPerson() {
        return contactPerson;
    }

    public void setContactPerson(String contactPerson) {
        this.contactPerson = contactPerson;
    }

    public String getCauseOfLoss() {
        return causeOfLoss;
    }

    public void setCauseOfLoss(String causeOfLoss) {
        this.causeOfLoss = causeOfLoss;
    }

    public String getSurveyType() {
        return surveyType;
    }

    public void setSurveyType(String surveyType) {
        this.surveyType = surveyType;
    }

    public String getClassCode() {
        return classCode;
    }

    public void setClassCode(String classCode) {
        this.classCode = classCode;
    }

    public String getCoverNoteType() {
        return coverNoteType;
    }

    public void setCoverNoteType(String coverNoteType) {
        this.coverNoteType = coverNoteType;
    }

    public String getCustomerCode() {
        return customerCode;
    }

    public void setCustomerCode(String customerCode) {
        this.customerCode = customerCode;
    }

    public Double getSumInsured() {
        return sumInsured;
    }

    public void setSumInsured(Double sumInsured) {
        this.sumInsured = sumInsured;
    }

    public Date getPeriodFrom() {
        return periodFrom;
    }

    public void setPeriodFrom(Date periodFrom) {
        this.periodFrom = periodFrom;
    }

    public Date getPeriodTo() {
        return periodTo;
    }

    public void setPeriodTo(Date periodTo) {
        this.periodTo = periodTo;
    }

    public String getBranchCode() {
        return branchCode;
    }

    public void setBranchCode(String branchCode) {
        this.branchCode = branchCode;
    }

    public Date getNotifiedDate() {
        return notifiedDate;
    }

    public void setNotifiedDate(Date notifiedDate) {
        this.notifiedDate = notifiedDate;
    }

    public Character getStatus() {
        return status;
    }

    public void setStatus(Character status) {
        this.status = status;
    }

    public String getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(String createdBy) {
        this.createdBy = createdBy;
    }

    public Date getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(Date createdDate) {
        this.createdDate = createdDate;
    }

    public String getModifiedBy() {
        return modifiedBy;
    }

    public void setModifiedBy(String modifiedBy) {
        this.modifiedBy = modifiedBy;
    }

    public Date getModifiedDate() {
        return modifiedDate;
    }

    public void setModifiedDate(Date modifiedDate) {
        this.modifiedDate = modifiedDate;
    }

    public String getRiskName() {
        return riskName;
    }

    public void setRiskName(String riskName) {
        this.riskName = riskName;
    }

    public Double getClaimsPaid() {
        return claimsPaid;
    }

    public void setClaimsPaid(Double claimsPaid) {
        this.claimsPaid = claimsPaid;
    }

    public String getCsdRefNo() {
        return csdRefNo;
    }

    public void setCsdRefNo(String csdRefNo) {
        this.csdRefNo = csdRefNo;
    }

    public String getCancellationRemarks() {
        return cancellationRemarks;
    }

    public void setCancellationRemarks(String cancellationRemarks) {
        this.cancellationRemarks = cancellationRemarks;
    }

    public Date getCancelledDate() {
        return cancelledDate;
    }

    public void setCancelledDate(Date cancelledDate) {
        this.cancelledDate = cancelledDate;
    }

    public Character getTotalLoss() {
        return totalLoss;
    }

    public void setTotalLoss(Character totalLoss) {
        this.totalLoss = totalLoss;
    }

    public Double getGracePeriod() {
        return gracePeriod;
    }

    public void setGracePeriod(Double gracePeriod) {
        this.gracePeriod = gracePeriod;
    }

    public Double getRiskSumInsured() {
        return riskSumInsured;
    }

    public void setRiskSumInsured(Double riskSumInsured) {
        this.riskSumInsured = riskSumInsured;
    }

    public Character getMultiRisks() {
        return multiRisks;
    }

    public void setMultiRisks(Character multiRisks) {
        this.multiRisks = multiRisks;
    }

    public Double getOutstandingDebitAmount() {
        return outstandingDebitAmount;
    }

    public void setOutstandingDebitAmount(Double outstandingDebitAmount) {
        this.outstandingDebitAmount = outstandingDebitAmount;
    }

    public Double getOutstandingChequeAmount() {
        return outstandingChequeAmount;
    }

    public void setOutstandingChequeAmount(Double outstandingChequeAmount) {
        this.outstandingChequeAmount = outstandingChequeAmount;
    }

    public Double getTotalProvision() {
        return totalProvision;
    }

    public void setTotalProvision(Double totalProvision) {
        this.totalProvision = totalProvision;
    }

    public Double getTotalRevision() {
        return totalRevision;
    }

    public void setTotalRevision(Double totalRevision) {
        this.totalRevision = totalRevision;
    }

    public String getCauseOfLossSpecifiedAtClaim() {
        return causeOfLossSpecifiedAtClaim;
    }

    public void setCauseOfLossSpecifiedAtClaim(String causeOfLossSpecifiedAtClaim) {
        this.causeOfLossSpecifiedAtClaim = causeOfLossSpecifiedAtClaim;
    }

    public Date getDateOfLossSpecifiedAtClaim() {
        return dateOfLossSpecifiedAtClaim;
    }

    public void setDateOfLossSpecifiedAtClaim(Date dateOfLossSpecifiedAtClaim) {
        this.dateOfLossSpecifiedAtClaim = dateOfLossSpecifiedAtClaim;
    }

    public Character getReinsuranceInformed() {
        return reinsuranceInformed;
    }

    public void setReinsuranceInformed(Character reinsuranceInformed) {
        this.reinsuranceInformed = reinsuranceInformed;
    }

    public String getReinsurancePeril() {
        return reinsurancePeril;
    }

    public void setReinsurancePeril(String reinsurancePeril) {
        this.reinsurancePeril = reinsurancePeril;
    }

    public Double getExcessValue() {
        return excessValue;
    }

    public void setExcessValue(Double excessValue) {
        this.excessValue = excessValue;
    }

    public Double getExcessPaidValue() {
        return excessPaidValue;
    }

    public void setExcessPaidValue(Double excessPaidValue) {
        this.excessPaidValue = excessPaidValue;
    }

    public String getDriverName() {
        return driverName;
    }

    public void setDriverName(String driverName) {
        this.driverName = driverName;
    }

    public String getDriverLicenseNo() {
        return driverLicenseNo;
    }

    public void setDriverLicenseNo(String driverLicenseNo) {
        this.driverLicenseNo = driverLicenseNo;
    }

    public Date getLicenseFromDate() {
        return licenseFromDate;
    }

    public void setLicenseFromDate(Date licenseFromDate) {
        this.licenseFromDate = licenseFromDate;
    }

    public Date getLicenseToDate() {
        return licenseToDate;
    }

    public void setLicenseToDate(Date licenseToDate) {
        this.licenseToDate = licenseToDate;
    }

    public Date getClaimFileClosureEffectiveDate() {
        return claimFileClosureEffectiveDate;
    }

    public void setClaimFileClosureEffectiveDate(Date claimFileClosureEffectiveDate) {
        this.claimFileClosureEffectiveDate = claimFileClosureEffectiveDate;
    }

    public Character getInsuredAtFault() {
        return insuredAtFault;
    }

    public void setInsuredAtFault(Character insuredAtFault) {
        this.insuredAtFault = insuredAtFault;
    }

    public Character getClaimDoubtful() {
        return claimDoubtful;
    }

    public void setClaimDoubtful(Character claimDoubtful) {
        this.claimDoubtful = claimDoubtful;
    }

    public String getClaimCategory() {
        return claimCategory;
    }

    public void setClaimCategory(String claimCategory) {
        this.claimCategory = claimCategory;
    }

    public String getVehicleTypeCode() {
        return vehicleTypeCode;
    }

    public void setVehicleTypeCode(String vehicleTypeCode) {
        this.vehicleTypeCode = vehicleTypeCode;
    }

    public String getAttendanceCircumstanceCode() {
        return attendanceCircumstanceCode;
    }

    public void setAttendanceCircumstanceCode(String attendanceCircumstanceCode) {
        this.attendanceCircumstanceCode = attendanceCircumstanceCode;
    }

    public Double getDriversAge() {
        return driversAge;
    }

    public void setDriversAge(Double driversAge) {
        this.driversAge = driversAge;
    }

    public Character getDriversSex() {
        return driversSex;
    }

    public void setDriversSex(Character driversSex) {
        this.driversSex = driversSex;
    }

    public String getPoliceStation() {
        return policeStation;
    }

    public void setPoliceStation(String policeStation) {
        this.policeStation = policeStation;
    }

    public String getInjuredPersons() {
        return injuredPersons;
    }

    public void setInjuredPersons(String injuredPersons) {
        this.injuredPersons = injuredPersons;
    }

    public Character getNotApplicable() {
        return notApplicable;
    }

    public void setNotApplicable(Character notApplicable) {
        this.notApplicable = notApplicable;
    }

    public String getBusinessPartyTypeCode() {
        return businessPartyTypeCode;
    }

    public void setBusinessPartyTypeCode(String businessPartyTypeCode) {
        this.businessPartyTypeCode = businessPartyTypeCode;
    }

    public String getCloseBatchNo() {
        return closeBatchNo;
    }

    public void setCloseBatchNo(String closeBatchNo) {
        this.closeBatchNo = closeBatchNo;
    }

    public Character getWorkHours() {
        return workHours;
    }

    public void setWorkHours(Character workHours) {
        this.workHours = workHours;
    }

    public String getPoliceStationCode() {
        return policeStationCode;
    }

    public void setPoliceStationCode(String policeStationCode) {
        this.policeStationCode = policeStationCode;
    }

    public String getMarketingExecutiveCode() {
        return marketingExecutiveCode;
    }

    public void setMarketingExecutiveCode(String marketingExecutiveCode) {
        this.marketingExecutiveCode = marketingExecutiveCode;
    }

    public String getAssignedLossAdjusterCode() {
        return assignedLossAdjusterCode;
    }

    public void setAssignedLossAdjusterCode(String assignedLossAdjusterCode) {
        this.assignedLossAdjusterCode = assignedLossAdjusterCode;
    }

    public String getEndorsementNo() {
        return endorsementNo;
    }

    public void setEndorsementNo(String endorsementNo) {
        this.endorsementNo = endorsementNo;
    }

    public String getDriverId() {
        return driverId;
    }

    public void setDriverId(String driverId) {
        this.driverId = driverId;
    }

    public String getLossTime() {
        return lossTime;
    }

    public void setLossTime(String lossTime) {
        this.lossTime = lossTime;
    }

    public String getUnderwritingYear() {
        return underwritingYear;
    }

    public void setUnderwritingYear(String underwritingYear) {
        this.underwritingYear = underwritingYear;
    }

    public String getWebTrackingNo() {
        return webTrackingNo;
    }

    public void setWebTrackingNo(String webTrackingNo) {
        this.webTrackingNo = webTrackingNo;
    }

    public String getPoliceDivision() {
        return policeDivision;
    }

    public void setPoliceDivision(String policeDivision) {
        this.policeDivision = policeDivision;
    }

    public String getLatitude() {
        return latitude;
    }

    public void setLatitude(String latitude) {
        this.latitude = latitude;
    }

    public String getLongitude() {
        return longitude;
    }

    public void setLongitude(String longitude) {
        this.longitude = longitude;
    }

    public String getDriverContactNo() {
        return driverContactNo;
    }

    public void setDriverContactNo(String driverContactNo) {
        this.driverContactNo = driverContactNo;
    }

    public Character getCallCenterFlag() {
        return callCenterFlag;
    }

    public void setCallCenterFlag(Character callCenterFlag) {
        this.callCenterFlag = callCenterFlag;
    }

    public String getLicenseCategory() {
        return licenseCategory;
    }

    public void setLicenseCategory(String licenseCategory) {
        this.licenseCategory = licenseCategory;
    }

    public Character getPoliceReport() {
        return policeReport;
    }

    public void setPoliceReport(Character policeReport) {
        this.policeReport = policeReport;
    }

    public String getRelationshipToInsured() {
        return relationshipToInsured;
    }

    public void setRelationshipToInsured(String relationshipToInsured) {
        this.relationshipToInsured = relationshipToInsured;
    }

    public String getRelationshipToInsuredDescription() {
        return relationshipToInsuredDescription;
    }

    public void setRelationshipToInsuredDescription(String relationshipToInsuredDescription) {
        this.relationshipToInsuredDescription = relationshipToInsuredDescription;
    }

    public String getThirdPartyDetails() {
        return thirdPartyDetails;
    }

    public void setThirdPartyDetails(String thirdPartyDetails) {
        this.thirdPartyDetails = thirdPartyDetails;
    }

    public String getActionTaken() {
        return actionTaken;
    }

    public void setActionTaken(String actionTaken) {
        this.actionTaken = actionTaken;
    }

    public Date getEffectiveStartDate() {
        return effectiveStartDate;
    }

    public void setEffectiveStartDate(Date effectiveStartDate) {
        this.effectiveStartDate = effectiveStartDate;
    }

    public String getYearOfManufacture() {
        return yearOfManufacture;
    }

    public void setYearOfManufacture(String yearOfManufacture) {
        this.yearOfManufacture = yearOfManufacture;
    }

    public String getMakeAndModel() {
        return makeAndModel;
    }

    public void setMakeAndModel(String makeAndModel) {
        this.makeAndModel = makeAndModel;
    }

    public String getHandlerCode() {
        return handlerCode;
    }

    public void setHandlerCode(String handlerCode) {
        this.handlerCode = handlerCode;
    }

    public String getHandlerName() {
        return handlerName;
    }

    public void setHandlerName(String handlerName) {
        this.handlerName = handlerName;
    }

    public RefLossCause getRefLossCause() {
        return refLossCause;
    }

    public void setRefLossCause(RefLossCause refLossCause) {
        this.refLossCause = refLossCause;
    }

    public List<NotificationAssignment> getNotificationAssignments() {
        return notificationAssignments;
    }

    public void setNotificationAssignments(List<NotificationAssignment> notificationAssignments) {
        this.notificationAssignments = notificationAssignments;
    }
}
