package com.infoinstech.claimsmodule.domain.model.claims.transaction;

import com.infoinstech.claimsmodule.domain.model.Auditable;

import javax.persistence.*;

@Entity
@Table(name = "CL_T_DAMAGE_SKETCH")
public class DamageArea extends Auditable<String> {

    public static final String SEQUENCE = "SEQ_CL_T_DAMAGE_SKETCH";

    @Id
    @Column(name = "SKE_SEQ_NO")
    private String sequenceNo;

    @Column(name = "SKE_INT_SEQ_NO")
    private String intimationSequenceNo;

    @Column(name = "SKE_CLAIM_NO")
    private String claimNo;

    @Column(name = "SKE_NOT_SEQ_NO")
    private String notificationSequenceNo;

    @Column(name = "SKE_NOT_NOT_NO")
    private String notificationNo;

    @Column(name = "SKE_USER_TYPE")
    private String userType;

    @Lob
    @Column(name = "SKE_IMAGE", nullable = false, columnDefinition = "mediumblob")
    private byte[] image;

    public static String getSEQUENCE() {
        return SEQUENCE;
    }

    public String getSequenceNo() {
        return sequenceNo;
    }

    public void setSequenceNo(String sequenceNo) {
        this.sequenceNo = sequenceNo;
    }

    public String getIntimationSequenceNo() {
        return intimationSequenceNo;
    }

    public void setIntimationSequenceNo(String intimationSequenceNo) {
        this.intimationSequenceNo = intimationSequenceNo;
    }

    public String getClaimNo() {
        return claimNo;
    }

    public void setClaimNo(String claimNo) {
        this.claimNo = claimNo;
    }

    public String getNotificationSequenceNo() {
        return notificationSequenceNo;
    }

    public void setNotificationSequenceNo(String notificationSequenceNo) {
        this.notificationSequenceNo = notificationSequenceNo;
    }

    public String getNotificationNo() {
        return notificationNo;
    }

    public void setNotificationNo(String notificationNo) {
        this.notificationNo = notificationNo;
    }

    public String getUserType() {
        return userType;
    }

    public void setUserType(String userType) {
        this.userType = userType;
    }

    public byte[] getImage() {
        return image;
    }

    public void setImage(byte[] image) {
        this.image = image;
    }


}
