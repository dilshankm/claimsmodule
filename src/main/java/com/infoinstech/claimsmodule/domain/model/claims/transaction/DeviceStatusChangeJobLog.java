package com.infoinstech.claimsmodule.domain.model.claims.transaction;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.util.Date;

@Entity
@Table(name = "CL_T_DEV_STATUS_CHANGE_JOB_LOG")
public class DeviceStatusChangeJobLog {

    public static final String SEQUENCE = "SEQ_CL_T_DEVICE_STATUS_CHANGE";
    @Id
    @Column(name = "DJL_SEQ_NO")
    private String sequenceNo;

    @Column(name = "DJL_RUNNING_TIME")
    private Date runningTime;

    public String getSequenceNo() {
        return sequenceNo;
    }

    public void setSequenceNo(String sequenceNo) {
        this.sequenceNo = sequenceNo;
    }

    public Date getRunningTime() {
        return runningTime;
    }

    public void setRunningTime(Date runningTime) {
        this.runningTime = runningTime;
    }
}
