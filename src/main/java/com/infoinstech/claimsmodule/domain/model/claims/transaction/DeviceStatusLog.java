package com.infoinstech.claimsmodule.domain.model.claims.transaction;

import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.Table;
import java.util.Date;

@Entity
@Table(name = "CL_T_EXTDEV_STATUS")
public class DeviceStatusLog {

    public static final String SEQUENCE = "SEQ_CL_T_EXTDEV_STATUS";

    @EmbeddedId
    private DeviceStatusLogPK deviceStatusLogPK;

    @Column(name = "XDS_LOG_ORDER")
    private Integer logOrder;

    @Column(name = "XDS_DEVICE_TOKEN")
    private Integer deviceToken;

    @Column(name = "XDS_LAST_STATUS")
    private String lastStatus;

    @Column(name = "XDS_CURRENT_STATUS")
    private String currentStatus;

    @Column(name = "XDS_LAST_UPDATED_TIME")
    private Date lastUpdatedTime;

    @Column(name = "XDS_CURRENT_UPDATED_TIME")
    private Date currentUpdatedTime;

    @Column(name = "CREATED_BY")
    private String createdBy;

    @Column(name = "CREATED_DATE")
    private Date createdDate;

    @Column(name = "MODIFIED_BY")
    private String modifiedBy;

    @Column(name = "MODIFIED_DATE")
    private Date modifiedDate;

    @Column(name = "XDS_LATITUDE")
    private String latitude;

    @Column(name = "XDS_LONGITUDE")
    private String longitude;


    public DeviceStatusLogPK getDeviceStatusLogPK() {
        return deviceStatusLogPK;
    }

    public void setDeviceStatusLogPK(DeviceStatusLogPK deviceStatusLogPK) {
        this.deviceStatusLogPK = deviceStatusLogPK;
    }

    public Integer getLogOrder() {
        return logOrder;
    }

    public void setLogOrder(Integer logOrder) {
        this.logOrder = logOrder;
    }

    public Integer getDeviceToken() {
        return deviceToken;
    }

    public void setDeviceToken(Integer deviceToken) {
        this.deviceToken = deviceToken;
    }

    public String getLastStatus() {
        return lastStatus;
    }

    public void setLastStatus(String lastStatus) {
        this.lastStatus = lastStatus;
    }

    public String getCurrentStatus() {
        return currentStatus;
    }

    public void setCurrentStatus(String currentStatus) {
        this.currentStatus = currentStatus;
    }

    public Date getLastUpdatedTime() {
        return lastUpdatedTime;
    }

    public void setLastUpdatedTime(Date lastUpdatedTime) {
        this.lastUpdatedTime = lastUpdatedTime;
    }

    public Date getCurrentUpdatedTime() {
        return currentUpdatedTime;
    }

    public void setCurrentUpdatedTime(Date currentUpdatedTime) {
        this.currentUpdatedTime = currentUpdatedTime;
    }

    public String getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(String createdBy) {
        this.createdBy = createdBy;
    }

    public Date getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(Date createdDate) {
        this.createdDate = createdDate;
    }

    public String getModifiedBy() {
        return modifiedBy;
    }

    public void setModifiedBy(String modifiedBy) {
        this.modifiedBy = modifiedBy;
    }

    public Date getModifiedDate() {
        return modifiedDate;
    }

    public void setModifiedDate(Date modifiedDate) {
        this.modifiedDate = modifiedDate;
    }

    public String getLatitude() {
        return latitude;
    }

    public void setLatitude(String latitude) {
        this.latitude = latitude;
    }

    public String getLongitude() {
        return longitude;
    }

    public void setLongitude(String longitude) {
        this.longitude = longitude;
    }
}
