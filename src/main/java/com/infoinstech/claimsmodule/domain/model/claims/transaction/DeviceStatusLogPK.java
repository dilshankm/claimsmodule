package com.infoinstech.claimsmodule.domain.model.claims.transaction;

import javax.persistence.Column;
import javax.persistence.Embeddable;
import java.io.Serializable;

@Embeddable
public class DeviceStatusLogPK implements Serializable {

    @Column(name = "XDS_SEQ_NO")
    private String sequenceNo;

    @Column(name = "XDS_EXP_CODE")
    private String assessorCode;

    @Column(name = "XDS_EXD_SEQ_NO")
    private String deviceSequenceNo;

    public DeviceStatusLogPK() {
    }

    public DeviceStatusLogPK(String sequenceNo, String assessorCode, String deviceSequenceNo) {
        this.sequenceNo = sequenceNo;
        this.assessorCode = assessorCode;
        this.deviceSequenceNo = deviceSequenceNo;
    }

    public String getSequenceNo() {
        return sequenceNo;
    }

    public void setSequenceNo(String sequenceNo) {
        this.sequenceNo = sequenceNo;
    }

    public String getAssessorCode() {
        return assessorCode;
    }

    public void setAssessorCode(String assessorCode) {
        this.assessorCode = assessorCode;
    }

    public String getDeviceSequenceNo() {
        return deviceSequenceNo;
    }

    public void setDeviceSequenceNo(String deviceSequenceNo) {
        this.deviceSequenceNo = deviceSequenceNo;
    }

}
