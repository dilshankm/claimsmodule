package com.infoinstech.claimsmodule.domain.model.claims.transaction;

import com.infoinstech.claimsmodule.domain.model.claims.reference.RefImageType;
import com.infoinstech.claimsmodule.domain.model.claims.reference.RefImageUploadType;

import javax.persistence.*;
import java.util.Date;

@Entity
@Table(name = "CL_T_IMAGES")
public class Image {

    public static final String SEQUENCE = "SEQ_CL_T_IMAGES";
    @Id
    @Column(name = "IMG_SEQ_NO")
    private String sequenceNo;

    @Column(name = "IMG_INT_SEQ")
    private String intimationSequenceNo;

    @Column(name = "IMG_CLAIM_NO")
    private String claimNo;

    @Column(name = "IMG_AAD_SEQ")
    private String assignmentSequenceNo;

    @Column(name = "IMG_JOB_NO")
    private String jobNo;

    @Column(name = "IMG_NOT_SEQ")
    private String notificationSequeneNo;

    @Column(name = "IMG_NOT_NOT_NO")
    private String notificationNo;

    @Column(name = "IMG_NAD_SEQ")
    private String notificationAssignmentSequenceNo;

    @Column(name = "IMG_ASS_NO")
    private String assignmentNo;

    @Column(name = "IMG_IMT_CODE")
    private String imageTypeCode;

    @Column(name = "IMG_IUT_CODE")
    private String uploadTypeCode;

    @Lob
    @Column(name = "IMG_IMAGE")
    private byte[] image;

    @Column(name = "IMG_CAPTURE_DATE")
    private Date capturedDate;

    @Column(name = "IMG_NAME")
    private String name;

    @OneToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "IMG_IMT_CODE", referencedColumnName = "IMT_CODE", insertable = false, updatable = false)
    private RefImageType refImageType;

    @OneToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "IMG_IUT_CODE", referencedColumnName = "IUT_CODE", insertable = false, updatable = false)
    private RefImageUploadType refImageUploadType;

    @Column(name = "CREATED_BY")
    private String createdBy;

    @Column(name = "CREATED_DATE")
    private Date createdDate;

    @Column(name = "MODIFIED_BY")
    private String modifiedBy;

    @Column(name = "MODIFIED_DATE")
    private Date modifiedDate;

    public String getSequenceNo() {
        return sequenceNo;
    }

    public void setSequenceNo(String sequenceNo) {
        this.sequenceNo = sequenceNo;
    }

    public String getIntimationSequenceNo() {
        return intimationSequenceNo;
    }

    public void setIntimationSequenceNo(String intimationSequenceNo) {
        this.intimationSequenceNo = intimationSequenceNo;
    }

    public String getClaimNo() {
        return claimNo;
    }

    public void setClaimNo(String claimNo) {
        this.claimNo = claimNo;
    }

    public String getAssignmentSequenceNo() {
        return assignmentSequenceNo;
    }

    public void setAssignmentSequenceNo(String assignmentSequenceNo) {
        this.assignmentSequenceNo = assignmentSequenceNo;
    }

    public String getJobNo() {
        return jobNo;
    }

    public void setJobNo(String jobNo) {
        this.jobNo = jobNo;
    }

    public String getNotificationSequeneNo() {
        return notificationSequeneNo;
    }

    public void setNotificationSequeneNo(String notificationSequeneNo) {
        this.notificationSequeneNo = notificationSequeneNo;
    }

    public String getNotificationNo() {
        return notificationNo;
    }

    public void setNotificationNo(String notificationNo) {
        this.notificationNo = notificationNo;
    }

    public String getNotificationAssignmentSequenceNo() {
        return notificationAssignmentSequenceNo;
    }

    public void setNotificationAssignmentSequenceNo(String notificationAssignmentSequenceNo) {
        this.notificationAssignmentSequenceNo = notificationAssignmentSequenceNo;
    }

    public String getAssignmentNo() {
        return assignmentNo;
    }

    public void setAssignmentNo(String assignmentNo) {
        this.assignmentNo = assignmentNo;
    }

    public String getImageTypeCode() {
        return imageTypeCode;
    }

    public void setImageTypeCode(String imageTypeCode) {
        this.imageTypeCode = imageTypeCode;
    }

    public String getUploadTypeCode() {
        return uploadTypeCode;
    }

    public void setUploadTypeCode(String uploadTypeCode) {
        this.uploadTypeCode = uploadTypeCode;
    }

    public byte[] getImage() {
        return image;
    }

    public void setImage(byte[] image) {
        this.image = image;
    }

    public Date getCapturedDate() {
        return capturedDate;
    }

    public void setCapturedDate(Date capturedDate) {
        this.capturedDate = capturedDate;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(String createdBy) {
        this.createdBy = createdBy;
    }

    public Date getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(Date createdDate) {
        this.createdDate = createdDate;
    }

    public String getModifiedBy() {
        return modifiedBy;
    }

    public void setModifiedBy(String modifiedBy) {
        this.modifiedBy = modifiedBy;
    }

    public Date getModifiedDate() {
        return modifiedDate;
    }

    public void setModifiedDate(Date modifiedDate) {
        this.modifiedDate = modifiedDate;
    }

    public RefImageType getRefImageType() {
        return refImageType;
    }

    public void setRefImageType(RefImageType refImageType) {
        this.refImageType = refImageType;
    }

    public RefImageUploadType getRefImageUploadType() {
        return refImageUploadType;
    }

    public void setRefImageUploadType(RefImageUploadType refImageUploadType) {
        this.refImageUploadType = refImageUploadType;
    }
}
