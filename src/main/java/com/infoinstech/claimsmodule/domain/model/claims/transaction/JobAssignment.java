package com.infoinstech.claimsmodule.domain.model.claims.transaction;

import com.infoinstech.claimsmodule.domain.model.claims.reference.RefSurveyType;

import javax.persistence.*;
import java.util.Date;

@Entity
@Table(name = "CL_T_REP_MASTER")
public class JobAssignment {

    public static final String SEQUENCE = "SEQ_CL_T_REP_MASTER";
    public static final String JOB_NO_SEQUENCE = "SEQ_CL_GEN_JOB";
    public static final String JOB_NO_PREFIX = "JB";

    @EmbeddedId
    private JobAssignmentPK jobAssignmentPK;


    @Column(name="AAD_JOB_NO")
    private String jobNo;

    @Column(name="AAD_SURVEY_CODE")
    private String surveyCode;

    @Column(name = "CREATED_BY")
    private String createdBy;

    @Column(name = "CREATED_DATE")
    private Date createdDate;

    @Column(name ="MODIFIED_BY")
    private String modifiedBy;

    @Column(name ="MODIFIED_DATE")
    private Date modifiedDate;

    @Column(name="AAD_SUB_CODE")
    private String subjectCode;

    @Column(name="AAD_FURTHER_INS")
    private String furtherInspections;

    @Column(name="AAD_CANCEL_BY")
    private String cancelledBy;

    @Column(name="AAD_CANCEL_DATE")
    private Date cancelledDate;

    @Column(name="AAD_CANCEL_FLAG")
    private String cancelledFlag;

    @Column(name = "AAD_STATUS")
    private Character status;

    @Column(name = "AAD_LATITUDE")
    private String latitude;

    @Column(name = "AAD_LONGITUDE")
    private String longitude;

    @Column(name = "AAD_NEAREST_TOWN")
    private String nearestTown;

    @OneToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "AAD_SURVEY_CODE", referencedColumnName = "SUR_CODE",insertable = false,updatable = false)
    private RefSurveyType surveyType;

    public String getJobNo() {
        return jobNo;
    }

    public void setJobNo(String jobNo) {
        this.jobNo = jobNo;
    }

    public String getSurveyCode() {
        return surveyCode;
    }

    public void setSurveyCode(String surveyCode) {
        this.surveyCode = surveyCode;
    }

    public String getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(String createdBy) {
        this.createdBy = createdBy;
    }

    public Date getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(Date createdDate) {
        this.createdDate = createdDate;
    }

    public String getModifiedBy() {
        return modifiedBy;
    }

    public void setModifiedBy(String modifiedBy) {
        this.modifiedBy = modifiedBy;
    }

    public Date getModifiedDate() {
        return modifiedDate;
    }

    public void setModifiedDate(Date modifiedDate) {
        this.modifiedDate = modifiedDate;
    }

    public String getSubjectCode() {
        return subjectCode;
    }

    public void setSubjectCode(String subjectCode) {
        this.subjectCode = subjectCode;
    }

    public String getFurtherInspections() {
        return furtherInspections;
    }

    public void setFurtherInspections(String furtherInspections) {
        this.furtherInspections = furtherInspections;
    }

    public String getCancelledBy() {
        return cancelledBy;
    }

    public void setCancelledBy(String cancelledBy) {
        this.cancelledBy = cancelledBy;
    }

    public Date getCancelledDate() {
        return cancelledDate;
    }

    public void setCancelledDate(Date cancelledDate) {
        this.cancelledDate = cancelledDate;
    }

    public String getCancelledFlag() {
        return cancelledFlag;
    }

    public void setCancelledFlag(String cancelledFlag) {
        this.cancelledFlag = cancelledFlag;
    }

    public Character getStatus() {
        return status;
    }

    public void setStatus(Character status) {
        this.status = status;
    }

    public String getLatitude() {
        return latitude;
    }

    public void setLatitude(String latitude) {
        this.latitude = latitude;
    }

    public String getLongitude() {
        return longitude;
    }

    public void setLongitude(String longitude) {
        this.longitude = longitude;
    }

    public String getNearestTown() {
        return nearestTown;
    }

    public void setNearestTown(String nearestTown) {
        this.nearestTown = nearestTown;
    }

    public JobAssignmentPK getJobAssignmentPK() {
        return jobAssignmentPK;
    }

    public void setJobAssignmentPK(JobAssignmentPK jobAssignmentPK) {
        this.jobAssignmentPK = jobAssignmentPK;
    }

    public RefSurveyType getSurveyType() {
        return surveyType;
    }


    public void setSurveyType(RefSurveyType surveyType) {
        this.surveyType = surveyType;
    }

    //    public String getIntimationSeqNo() {
//        return intimationSeqNo;
//    }
//
//    public void setIntimationSeqNo(String intimationSeqNo) {
//        this.intimationSeqNo = intimationSeqNo;
//    }
//
//    public String getJobSeqNo() {
//        return jobSeqNo;
//    }
//
//    public void setJobSeqNo(String jobSeqNo) {
//        this.jobSeqNo = jobSeqNo;
//    }
}
