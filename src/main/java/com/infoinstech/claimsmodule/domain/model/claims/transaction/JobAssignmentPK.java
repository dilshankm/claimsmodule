package com.infoinstech.claimsmodule.domain.model.claims.transaction;

import javax.persistence.Column;
import javax.persistence.Embeddable;
import java.io.Serializable;

@Embeddable
public class JobAssignmentPK implements Serializable {

    @Column(name = "AAD_INT_SEQ")
    private String intimationSequenceNo;

    @Column(name = "AAD_SEQ_NO")
    private String sequenceNo;

    public JobAssignmentPK() {
    }

    public JobAssignmentPK(String intimationSequenceNo, String sequenceNo) {
        this.intimationSequenceNo = intimationSequenceNo;
        this.sequenceNo = sequenceNo;
    }

    public String getIntimationSequenceNo() {
        return intimationSequenceNo;
    }

    public void setIntimationSequenceNo(String intimationSequenceNo) {
        this.intimationSequenceNo = intimationSequenceNo;
    }

    public String getSequenceNo() {
        return sequenceNo;
    }

    public void setSequenceNo(String sequenceNo) {
        this.sequenceNo = sequenceNo;
    }
}
