package com.infoinstech.claimsmodule.domain.model.claims.transaction;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import java.util.Date;

@Entity
@Table(name = "CL_T_LOSS_CHARGES")
public class LossAdjusterCharge {

    public static final String SEQUENCE = "SEQ_CL_T_LOSS_CHARGES";
    @Id
    @Column(name = "LCH_SEQ_NO")
    private String sequenceNo;

    @Column(name = "CREATED_BY")
    private String createdBy;

    @Column(name = "CREATED_DATE")
    private Date createdDate;

    @Column(name = "MODIFIED_BY")
    private String modifiedBy;

    @Column(name = "MODIFIED_DATE")
    private Date modifiedDate;

    @NotNull
    @Column(name = "LCH_INT_SEQ")
    private String intimationSequenceNo;

    @NotNull
    @Column(name = "LCH_REP_SEQ")
    private String jobAssignmentSequenceNo;

    @NotNull
    @Column(name = "LCH_AAS_SEQ")
    private String assignLossAdjusterSequenceNo;

    @Column(name = "LCH_DATE_VISITED")
    private Date dateVisited;

    @Column(name = "LCH_CHARGES")
    private Double charges;

    @Column(name = "LCH_PAID")
    private Character paid;

    @Column(name = "LCH_CHARGE_CODE")
    private String chargeCode;

    @Column(name = "LCH_PAID_AMOUNT")
    private String paidAmount;

    @Column(name = "LCH_AUTH_BY")
    private String authorizedBy;

    @Column(name = "LCH_AUTH_DATE")
    private Date authorizedDate;

    @Column(name = "LCH_ADJ_CODE")
    private String lossAdjusterCode;

    @Column(name = "LCH_SENT_PAYMNT")
    private Character paymentSent;

    @Column(name = "LCH_CL_UNIT_CODE")
    private String unitTypeCode;

    @Column(name = "LCH_CL_UNIT_PRICE")
    private Double unitPrice;

    @Column(name = "LCH_CL_NO_OF_UNITS")
    private Integer noOfUnits;

    @Column(name = "LCH_PAY_UNIT_PRICE")
    private Double payableUnitPrice;

    @Column(name = "LCH_PAY_NO_OF_UNITS")
    private Integer payableNoOfUnits;

    @Column(name = "LCH_REQ_SEQ_NO")
    private String requisitionSequenceNo;

    public String getSequenceNo() {
        return sequenceNo;
    }

    public void setSequenceNo(String sequenceNo) {
        this.sequenceNo = sequenceNo;
    }

    public String getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(String createdBy) {
        this.createdBy = createdBy;
    }

    public Date getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(Date createdDate) {
        this.createdDate = createdDate;
    }

    public String getModifiedBy() {
        return modifiedBy;
    }

    public void setModifiedBy(String modifiedBy) {
        this.modifiedBy = modifiedBy;
    }

    public Date getModifiedDate() {
        return modifiedDate;
    }

    public void setModifiedDate(Date modifiedDate) {
        this.modifiedDate = modifiedDate;
    }

    public String getIntimationSequenceNo() {
        return intimationSequenceNo;
    }

    public void setIntimationSequenceNo(String intimationSequenceNo) {
        this.intimationSequenceNo = intimationSequenceNo;
    }

    public String getJobAssignmentSequenceNo() {
        return jobAssignmentSequenceNo;
    }

    public void setJobAssignmentSequenceNo(String jobAssignmentSequenceNo) {
        this.jobAssignmentSequenceNo = jobAssignmentSequenceNo;
    }

    public String getAssignLossAdjusterSequenceNo() {
        return assignLossAdjusterSequenceNo;
    }

    public void setAssignLossAdjusterSequenceNo(String assignLossAdjusterSequenceNo) {
        this.assignLossAdjusterSequenceNo = assignLossAdjusterSequenceNo;
    }

    public Date getDateVisited() {
        return dateVisited;
    }

    public void setDateVisited(Date dateVisited) {
        this.dateVisited = dateVisited;
    }

    public Double getCharges() {
        return charges;
    }

    public void setCharges(Double charges) {
        this.charges = charges;
    }

    public Character getPaid() {
        return paid;
    }

    public void setPaid(Character paid) {
        this.paid = paid;
    }

    public String getChargeCode() {
        return chargeCode;
    }

    public void setChargeCode(String chargeCode) {
        this.chargeCode = chargeCode;
    }

    public String getPaidAmount() {
        return paidAmount;
    }

    public void setPaidAmount(String paidAmount) {
        this.paidAmount = paidAmount;
    }

    public String getAuthorizedBy() {
        return authorizedBy;
    }

    public void setAuthorizedBy(String authorizedBy) {
        this.authorizedBy = authorizedBy;
    }

    public Date getAuthorizedDate() {
        return authorizedDate;
    }

    public void setAuthorizedDate(Date authorizedDate) {
        this.authorizedDate = authorizedDate;
    }

    public String getLossAdjusterCode() {
        return lossAdjusterCode;
    }

    public void setLossAdjusterCode(String lossAdjusterCode) {
        this.lossAdjusterCode = lossAdjusterCode;
    }

    public Character getPaymentSent() {
        return paymentSent;
    }

    public void setPaymentSent(Character paymentSent) {
        this.paymentSent = paymentSent;
    }

    public String getUnitTypeCode() {
        return unitTypeCode;
    }

    public void setUnitTypeCode(String unitTypeCode) {
        this.unitTypeCode = unitTypeCode;
    }

    public Double getUnitPrice() {
        return unitPrice;
    }

    public void setUnitPrice(Double unitPrice) {
        this.unitPrice = unitPrice;
    }

    public Integer getNoOfUnits() {
        return noOfUnits;
    }

    public void setNoOfUnits(Integer noOfUnits) {
        this.noOfUnits = noOfUnits;
    }

    public Double getPayableUnitPrice() {
        return payableUnitPrice;
    }

    public void setPayableUnitPrice(Double payableUnitPrice) {
        this.payableUnitPrice = payableUnitPrice;
    }

    public Integer getPayableNoOfUnits() {
        return payableNoOfUnits;
    }

    public void setPayableNoOfUnits(Integer payableNoOfUnits) {
        this.payableNoOfUnits = payableNoOfUnits;
    }

    public String getRequisitionSequenceNo() {
        return requisitionSequenceNo;
    }

    public void setRequisitionSequenceNo(String requisitionSequenceNo) {
        this.requisitionSequenceNo = requisitionSequenceNo;
    }
}
