package com.infoinstech.claimsmodule.domain.model.claims.transaction;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import java.util.Date;

@Entity
@Table(name = "CL_T_NOT_LOSS_CHARGES")
public class NotificationAdjusterCharge {

    public static final String SEQUENCE = "SEQ_CL_T_NOT_LOSS_CHARGES";

    @Id
    @Column(name = "NCH_SEQ_NO")
    private String sequenceNo;

    @Column(name = "CREATED_BY")
    private String createdBy;

    @Column(name = "CREATED_DATE")
    private Date createdDate;

    @Column(name = "MODIFIED_BY")
    private String modifiedBy;

    @Column(name = "MODIFIED_DATE")
    private Date modifiedDate;

    @NotNull
    @Column(name = "NCH_NOT_SEQ")
    private String notificationSequenceNo;

    @NotNull
    @Column(name = "NCH_NAD_SEQ")
    private String notificationAssignmentSequenceNo;

    @NotNull
    @Column(name = "NCH_NAS_SEQ")
    private String notificationLossAdjusterSequenceNo;

    @Column(name = "NCH_DATE_VISITED")
    private Date dateVisited;

    @Column(name = "NCH_CHARGES")
    private Double charges;

    @Column(name = "NCH_PAID")
    private Character paid;

    @Column(name = "NCH_CHARGE_CODE")
    private String chargeTypeCode;

    @Column(name = "NCH_PAID_AMOUNT")
    private String paidAmount;

    @Column(name = "NCH_AUTH_BY")
    private String authorizedBy;

    @Column(name = "NCH_AUTH_DATE")
    private Date authorizedDate;

    @Column(name = "NCH_ADJ_CODE")
    private String lossAdjusterCode;

    @Column(name = "NCH_SENT_PAYMNT")
    private Character paymentSent;

    @Column(name = "NCH_CL_UNIT_CODE")
    private String unitTypeCode;

    @Column(name = "NCH_CL_UNIT_PRICE")
    private Double unitPrice;

    @Column(name = "NCH_CL_NO_OF_UNITS")
    private Integer noOfUnits;

    @Column(name = "NCH_PAY_UNIT_PRICE")
    private Double payableUnitPrice;

    @Column(name = "NCH_PAY_NO_OF_UNITS")
    private Integer payableNoOfUnits;

    public String getSequenceNo() {
        return sequenceNo;
    }

    public void setSequenceNo(String sequenceNo) {
        this.sequenceNo = sequenceNo;
    }

    public String getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(String createdBy) {
        this.createdBy = createdBy;
    }

    public Date getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(Date createdDate) {
        this.createdDate = createdDate;
    }

    public String getModifiedBy() {
        return modifiedBy;
    }

    public void setModifiedBy(String modifiedBy) {
        this.modifiedBy = modifiedBy;
    }

    public Date getModifiedDate() {
        return modifiedDate;
    }

    public void setModifiedDate(Date modifiedDate) {
        this.modifiedDate = modifiedDate;
    }

    public String getNotificationSequenceNo() {
        return notificationSequenceNo;
    }

    public void setNotificationSequenceNo(String notificationSequenceNo) {
        this.notificationSequenceNo = notificationSequenceNo;
    }

    public String getNotificationAssignmentSequenceNo() {
        return notificationAssignmentSequenceNo;
    }

    public void setNotificationAssignmentSequenceNo(String notificationAssignmentSequenceNo) {
        this.notificationAssignmentSequenceNo = notificationAssignmentSequenceNo;
    }

    public String getNotificationLossAdjusterSequenceNo() {
        return notificationLossAdjusterSequenceNo;
    }

    public void setNotificationLossAdjusterSequenceNo(String notificationLossAdjusterSequenceNo) {
        this.notificationLossAdjusterSequenceNo = notificationLossAdjusterSequenceNo;
    }

    public Date getDateVisited() {
        return dateVisited;
    }

    public void setDateVisited(Date dateVisited) {
        this.dateVisited = dateVisited;
    }

    public Double getCharges() {
        return charges;
    }

    public void setCharges(Double charges) {
        this.charges = charges;
    }

    public Character getPaid() {
        return paid;
    }

    public void setPaid(Character paid) {
        this.paid = paid;
    }

    public String getChargeTypeCode() {
        return chargeTypeCode;
    }

    public void setChargeTypeCode(String chargeTypeCode) {
        this.chargeTypeCode = chargeTypeCode;
    }

    public String getPaidAmount() {
        return paidAmount;
    }

    public void setPaidAmount(String paidAmount) {
        this.paidAmount = paidAmount;
    }

    public String getAuthorizedBy() {
        return authorizedBy;
    }

    public void setAuthorizedBy(String authorizedBy) {
        this.authorizedBy = authorizedBy;
    }

    public Date getAuthorizedDate() {
        return authorizedDate;
    }

    public void setAuthorizedDate(Date authorizedDate) {
        this.authorizedDate = authorizedDate;
    }

    public String getLossAdjusterCode() {
        return lossAdjusterCode;
    }

    public void setLossAdjusterCode(String lossAdjusterCode) {
        this.lossAdjusterCode = lossAdjusterCode;
    }

    public Character getPaymentSent() {
        return paymentSent;
    }

    public void setPaymentSent(Character paymentSent) {
        this.paymentSent = paymentSent;
    }

    public String getUnitTypeCode() {
        return unitTypeCode;
    }

    public void setUnitTypeCode(String unitTypeCode) {
        this.unitTypeCode = unitTypeCode;
    }

    public Double getUnitPrice() {
        return unitPrice;
    }

    public void setUnitPrice(Double unitPrice) {
        this.unitPrice = unitPrice;
    }

    public Integer getNoOfUnits() {
        return noOfUnits;
    }

    public void setNoOfUnits(Integer noOfUnits) {
        this.noOfUnits = noOfUnits;
    }

    public Double getPayableUnitPrice() {
        return payableUnitPrice;
    }

    public void setPayableUnitPrice(Double payableUnitPrice) {
        this.payableUnitPrice = payableUnitPrice;
    }

    public Integer getPayableNoOfUnits() {
        return payableNoOfUnits;
    }

    public void setPayableNoOfUnits(Integer payableNoOfUnits) {
        this.payableNoOfUnits = payableNoOfUnits;
    }
}
