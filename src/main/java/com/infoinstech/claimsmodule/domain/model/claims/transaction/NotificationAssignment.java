package com.infoinstech.claimsmodule.domain.model.claims.transaction;

import com.infoinstech.claimsmodule.domain.model.claims.reference.RefSurveyType;

import javax.persistence.*;
import java.util.Date;
import java.util.List;

@Entity
@Table(name = "CL_T_NOT_REP_MASTER")
public class NotificationAssignment {

    public static final String SEQUENCE = "SEQ_CL_T_NOT_REP_MASTER";

    @EmbeddedId
    private NotificationAssignmentPK notificationAssignmentPK;

    @Column(name = "NAD_JOB_NO")
    private String jobNo;

    @Column(name = "NAD_SURVEY_CODE")
    private String surveryCode;

    @Column(name = "CREATED_BY")
    private String createdBy;

    @Column(name = "CREATED_DATE")
    private Date createdDate;

    @Column(name = "MODIFIED_BY")
    private String modifiedBy;

    @Column(name = "MODIFIED_DATE")
    private Date modifiedDate;

    @Column(name = "NAD_SUB_CODE")
    private String subjectCode;

    @Column(name = "NAD_FURTHER_INS")
    private Character furtherInspections;

    @Column(name = "NAD_CANCEL_BY")
    private String cancelledBy;

    @Column(name = "NAD_CANCEL_DATE")
    private Date cancelledDate;

    @Column(name = "NAD_CANCEL_FLAG")
    private Character cancelledFlag;

    @Column(name = "NAD_STATUS")
    private Character status;

    @Column(name = "NAD_LATITUDE")
    private String latitude;

    @Column(name = "NAD_LONGITUDE")
    private String longitude;

    @Column(name = "NAD_NEAREST_TOWN")
    private String nearestTown;

    @OneToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "NAD_SURVEY_CODE", referencedColumnName = "SUR_CODE", insertable = false, updatable = false)
    private RefSurveyType refSurveyType;

    @OneToMany(fetch = FetchType.LAZY)
    @JoinColumns({
            @JoinColumn(name = "NAS_NOT_SEQ", referencedColumnName = "NAD_NOT_SEQ", insertable = false, updatable = false),
            @JoinColumn(name = "NAS_REP_SEQ", referencedColumnName = "NAD_SEQ_NO", insertable = false, updatable = false)
    })
    private List<NotificationLossAdjuster> lossAdjusters;

    public NotificationAssignmentPK getNotificationAssignmentPK() {
        return notificationAssignmentPK;
    }

    public void setNotificationAssignmentPK(NotificationAssignmentPK notificationAssignmentPK) {
        this.notificationAssignmentPK = notificationAssignmentPK;
    }

    public String getJobNo() {
        return jobNo;
    }

    public void setJobNo(String jobNo) {
        this.jobNo = jobNo;
    }

    public String getSurveryCode() {
        return surveryCode;
    }

    public void setSurveryCode(String surveryCode) {
        this.surveryCode = surveryCode;
    }

    public String getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(String createdBy) {
        this.createdBy = createdBy;
    }

    public Date getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(Date createdDate) {
        this.createdDate = createdDate;
    }

    public String getModifiedBy() {
        return modifiedBy;
    }

    public void setModifiedBy(String modifiedBy) {
        this.modifiedBy = modifiedBy;
    }

    public Date getModifiedDate() {
        return modifiedDate;
    }

    public void setModifiedDate(Date modifiedDate) {
        this.modifiedDate = modifiedDate;
    }

    public String getSubjectCode() {
        return subjectCode;
    }

    public void setSubjectCode(String subjectCode) {
        this.subjectCode = subjectCode;
    }

    public Character getFurtherInspections() {
        return furtherInspections;
    }

    public void setFurtherInspections(Character furtherInspections) {
        this.furtherInspections = furtherInspections;
    }

    public String getCancelledBy() {
        return cancelledBy;
    }

    public void setCancelledBy(String cancelledBy) {
        this.cancelledBy = cancelledBy;
    }

    public Date getCancelledDate() {
        return cancelledDate;
    }

    public void setCancelledDate(Date cancelledDate) {
        this.cancelledDate = cancelledDate;
    }

    public Character getCancelledFlag() {
        return cancelledFlag;
    }

    public void setCancelledFlag(Character cancelledFlag) {
        this.cancelledFlag = cancelledFlag;
    }

    public Character getStatus() {
        return status;
    }

    public void setStatus(Character status) {
        this.status = status;
    }

    public String getLatitude() {
        return latitude;
    }

    public void setLatitude(String latitude) {
        this.latitude = latitude;
    }

    public String getLongitude() {
        return longitude;
    }

    public void setLongitude(String longitude) {
        this.longitude = longitude;
    }

    public String getNearestTown() {
        return nearestTown;
    }

    public void setNearestTown(String nearestTown) {
        this.nearestTown = nearestTown;
    }

    public RefSurveyType getRefSurveyType() {
        return refSurveyType;
    }

    public void setRefSurveyType(RefSurveyType refSurveyType) {
        this.refSurveyType = refSurveyType;
    }

    public List<NotificationLossAdjuster> getLossAdjusters() {
        return lossAdjusters;
    }

    public void setLossAdjusters(List<NotificationLossAdjuster> lossAdjusters) {
        this.lossAdjusters = lossAdjusters;
    }
}
