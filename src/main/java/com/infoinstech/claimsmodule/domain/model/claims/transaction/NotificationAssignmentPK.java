package com.infoinstech.claimsmodule.domain.model.claims.transaction;

import javax.persistence.Column;
import javax.persistence.Embeddable;
import java.io.Serializable;

@Embeddable
public class NotificationAssignmentPK implements Serializable {

    @Column(name = "NAD_SEQ_NO")
    private String sequenceNo;

    @Column(name = "NAD_NOT_SEQ")
    private String notificationSequenceNo;

    public NotificationAssignmentPK() {
    }

    public NotificationAssignmentPK(String sequenceNo, String notificationSequenceNo) {
        this.sequenceNo = sequenceNo;
        this.notificationSequenceNo = notificationSequenceNo;
    }

    public String getSequenceNo() {
        return sequenceNo;
    }

    public void setSequenceNo(String sequenceNo) {
        this.sequenceNo = sequenceNo;
    }

    public String getNotificationSequenceNo() {
        return notificationSequenceNo;
    }

    public void setNotificationSequenceNo(String notificationSequenceNo) {
        this.notificationSequenceNo = notificationSequenceNo;
    }
}
