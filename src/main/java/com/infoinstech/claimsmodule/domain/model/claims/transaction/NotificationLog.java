package com.infoinstech.claimsmodule.domain.model.claims.transaction;


import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.util.Date;

@Entity
@Table(name = "CL_T_FCM_LOG")
public class NotificationLog {

    public static final String SEQUENCE = "SEQ_CL_T_FCM_LOG";

    @Id
    @Column(name = "FCL_SEQ_NO")
    private String sequenceNo;

    @Column(name = "FCL_AAD_SEQ_NO")
    private String jobSeqNo;

    @Column(name = "FCL_AAD_JOB_NO")
    private String jobNo;

    @Column(name = "FCL_NAD_SEQ_NO")
    private String assignmentSeqNo;

    @Column(name = "FCL_NAD_ASS_NO")
    private String assignmentNo;

    @Column(name = "FCL_EXP_CODE")
    private String externalPersonCode;

    @Column(name = "FCL_INT_SEQ_NO")
    private String claimSequenceNo;

    @Column(name = "FCL_INT_CLAIM_NO")
    private String claimNo;

    @Column(name = "FCL_NOT_SEQ_NO")
    private String notificationSequenceNo;

    @Column(name = "FCL_NOT_NOT_NO")
    private String notificationNo;

    @Column(name = "FCL_EXD_SEQ_NO")
    private String deviceSeqNo;

    @Column(name = "FCL_EXD_API_KEY")
    private String apiKey;

    @Column(name = "FCL_EXD_DEVICE_TOKEN")
    private String deviceToken;

    @Column(name = "FCL_RESPONSE_CODE")
    private String responseCode;

    @Column(name = "FCL_RESPONSE_MSG")
    private String responseMessage;

    @Column(name = "FCL_RESPONSE_TIME")
    private String responseTime;

    @Column(name = "FCL_STATUS")
    private Character status;

    @Column(name = "CREATED_BY")
    private String createdBy;

    @Column(name = "CREATED_DATE")
    private Date createdDate;

    @Column(name = "MODIFIED_BY")
    private String modifiedBy;

    @Column(name = "MODIFIED_DATE")
    private Date modifiedDate;

    public String getSequenceNo() {
        return sequenceNo;
    }

    public void setSequenceNo(String sequenceNo) {
        this.sequenceNo = sequenceNo;
    }

    public String getJobSeqNo() {
        return jobSeqNo;
    }

    public void setJobSeqNo(String jobSeqNo) {
        this.jobSeqNo = jobSeqNo;
    }

    public String getJobNo() {
        return jobNo;
    }

    public void setJobNo(String jobNo) {
        this.jobNo = jobNo;
    }

    public String getAssignmentSeqNo() {
        return assignmentSeqNo;
    }

    public void setAssignmentSeqNo(String assignmentSeqNo) {
        this.assignmentSeqNo = assignmentSeqNo;
    }

    public String getAssignmentNo() {
        return assignmentNo;
    }

    public void setAssignmentNo(String assignmentNo) {
        this.assignmentNo = assignmentNo;
    }

    public String getExternalPersonCode() {
        return externalPersonCode;
    }

    public void setExternalPersonCode(String externalPersonCode) {
        this.externalPersonCode = externalPersonCode;
    }

    public String getClaimSequenceNo() {
        return claimSequenceNo;
    }

    public void setClaimSequenceNo(String claimSequenceNo) {
        this.claimSequenceNo = claimSequenceNo;
    }

    public String getClaimNo() {
        return claimNo;
    }

    public void setClaimNo(String claimNo) {
        this.claimNo = claimNo;
    }

    public String getNotificationSequenceNo() {
        return notificationSequenceNo;
    }

    public void setNotificationSequenceNo(String notificationSequenceNo) {
        this.notificationSequenceNo = notificationSequenceNo;
    }

    public String getNotificationNo() {
        return notificationNo;
    }

    public void setNotificationNo(String notificationNo) {
        this.notificationNo = notificationNo;
    }

    public String getDeviceSeqNo() {
        return deviceSeqNo;
    }

    public void setDeviceSeqNo(String deviceSeqNo) {
        this.deviceSeqNo = deviceSeqNo;
    }

    public String getApiKey() {
        return apiKey;
    }

    public void setApiKey(String apiKey) {
        this.apiKey = apiKey;
    }

    public String getDeviceToken() {
        return deviceToken;
    }

    public void setDeviceToken(String deviceToken) {
        this.deviceToken = deviceToken;
    }

    public String getResponseCode() {
        return responseCode;
    }

    public void setResponseCode(String responseCode) {
        this.responseCode = responseCode;
    }

    public String getResponseMessage() {
        return responseMessage;
    }

    public void setResponseMessage(String responseMessage) {
        this.responseMessage = responseMessage;
    }

    public String getResponseTime() {
        return responseTime;
    }

    public void setResponseTime(String responseTime) {
        this.responseTime = responseTime;
    }

    public Character getStatus() {
        return status;
    }

    public void setStatus(Character status) {
        this.status = status;
    }

    public String getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(String createdBy) {
        this.createdBy = createdBy;
    }

    public Date getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(Date createdDate) {
        this.createdDate = createdDate;
    }

    public String getModifiedBy() {
        return modifiedBy;
    }

    public void setModifiedBy(String modifiedBy) {
        this.modifiedBy = modifiedBy;
    }

    public Date getModifiedDate() {
        return modifiedDate;
    }

    public void setModifiedDate(Date modifiedDate) {
        this.modifiedDate = modifiedDate;
    }
}
