package com.infoinstech.claimsmodule.domain.model.claims.transaction;

import javax.persistence.*;
import java.util.Date;
import java.util.List;

@Entity
@Table(name = "CL_T_NOT_ASSIGN_LOSS")
public class NotificationLossAdjuster {

    public static final String SEQUENCE = "SEQ_CL_T_NOT_ASSIGN_LOSS";

    @EmbeddedId
    private NotificationLossAdjusterPK notificationLossAdjusterPK;

    @Column(name = "NAS_CURRENCY")
    private String currency;

    @Column(name = "NAS_REP_DUE_DT")
    private Date reportDueDate;

    @Column(name = "NAS_SUBMIT_DT")
    private Date submitDate;

    @Column(name = "NAS_TOTAL")
    private Double total;

    @Column(name = "NAS_ASSOS_TYPE")
    private Character externalPersonType;

    @Column(name = "NAS_VISIT_DUE_DT")
    private Date visitDueDate;

    @Column(name = "NAS_CODE")
    private String externalPersonCode;

    @Column(name = "NAS_APPOINTED_DT")
    private Date appointedDate;

    @Column(name = "NAS_COMMENTS")
    private String comments;

    @Column(name = "CREATED_BY")
    private String createdBy;

    @Column(name = "CREATED_DATE")
    private Date createdDate;

    @Column(name = "MODIFIED_BY")
    private String modifiedBy;

    @Column(name = "MODIFIED_DATE")
    private Date modifiedDate;

    @Column(name = "NAS_AUTH_BY")
    private String authorizedBy;

    @Column(name = "NAS_AUTH_DT")
    private Date authorizedDate;

    @Column(name = "NAS_REP_SUBMITTED_DT")
    private Date submittedDate;

    @Column(name = "NAS_VEHICLE_LIST")
    private String vehicleList;

    @Column(name = "NAS_CUR_LOCATION")
    private String currentLocation;

    @Column(name = "NAS_POINT_OF_IMPACT")
    private String pointOfContact;

    @Column(name = "NAS_PROMIS_TIME")
    private String promiseTime;

    @Column(name = "NAS_ASSIGN_STATUS")
    private Character status;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "NAS_NOT_SEQ", referencedColumnName = "NOT_SEQ_NO", insertable = false, updatable = false)
    private ClaimNotification claimNotification;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumns({
            @JoinColumn(name = "NAS_REP_SEQ", referencedColumnName = "NAD_SEQ_NO", insertable = false, updatable = false),
            @JoinColumn(name = "NAS_NOT_SEQ", referencedColumnName = "NAD_NOT_SEQ", insertable = false, updatable = false)
    })
    private NotificationAssignment notificationAssignment;

    @OneToMany(fetch = FetchType.LAZY)
    @JoinColumns({
            @JoinColumn(name = "NCH_NOT_SEQ", referencedColumnName = "NAS_NOT_SEQ", insertable = false, updatable = false),
            @JoinColumn(name = "NCH_NAD_SEQ", referencedColumnName = "NAS_REP_SEQ", insertable = false, updatable = false),
            @JoinColumn(name = "NCH_NAS_SEQ", referencedColumnName = "NAS_SEQ_NO", insertable = false, updatable = false)
    })
    private List<NotificationAdjusterCharge> notificationAdjusterCharges;

    public NotificationLossAdjusterPK getNotificationLossAdjusterPK() {
        return notificationLossAdjusterPK;
    }

    public void setNotificationLossAdjusterPK(NotificationLossAdjusterPK notificationLossAdjusterPK) {
        this.notificationLossAdjusterPK = notificationLossAdjusterPK;
    }

    public String getCurrency() {
        return currency;
    }

    public void setCurrency(String currency) {
        this.currency = currency;
    }

    public Date getReportDueDate() {
        return reportDueDate;
    }

    public void setReportDueDate(Date reportDueDate) {
        this.reportDueDate = reportDueDate;
    }

    public Date getSubmitDate() {
        return submitDate;
    }

    public void setSubmitDate(Date submitDate) {
        this.submitDate = submitDate;
    }

    public Double getTotal() {
        return total;
    }

    public void setTotal(Double total) {
        this.total = total;
    }

    public Character getExternalPersonType() {
        return externalPersonType;
    }

    public void setExternalPersonType(Character externalPersonType) {
        this.externalPersonType = externalPersonType;
    }

    public Date getVisitDueDate() {
        return visitDueDate;
    }

    public void setVisitDueDate(Date visitDueDate) {
        this.visitDueDate = visitDueDate;
    }

    public String getExternalPersonCode() {
        return externalPersonCode;
    }

    public void setExternalPersonCode(String externalPersonCode) {
        this.externalPersonCode = externalPersonCode;
    }

    public Date getAppointedDate() {
        return appointedDate;
    }

    public void setAppointedDate(Date appointedDate) {
        this.appointedDate = appointedDate;
    }

    public String getComments() {
        return comments;
    }

    public void setComments(String comments) {
        this.comments = comments;
    }

    public String getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(String createdBy) {
        this.createdBy = createdBy;
    }

    public Date getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(Date createdDate) {
        this.createdDate = createdDate;
    }

    public String getModifiedBy() {
        return modifiedBy;
    }

    public void setModifiedBy(String modifiedBy) {
        this.modifiedBy = modifiedBy;
    }

    public Date getModifiedDate() {
        return modifiedDate;
    }

    public void setModifiedDate(Date modifiedDate) {
        this.modifiedDate = modifiedDate;
    }

    public String getAuthorizedBy() {
        return authorizedBy;
    }

    public void setAuthorizedBy(String authorizedBy) {
        this.authorizedBy = authorizedBy;
    }

    public Date getAuthorizedDate() {
        return authorizedDate;
    }

    public void setAuthorizedDate(Date authorizedDate) {
        this.authorizedDate = authorizedDate;
    }

    public Date getSubmittedDate() {
        return submittedDate;
    }

    public void setSubmittedDate(Date submittedDate) {
        this.submittedDate = submittedDate;
    }

    public String getVehicleList() {
        return vehicleList;
    }

    public void setVehicleList(String vehicleList) {
        this.vehicleList = vehicleList;
    }

    public String getCurrentLocation() {
        return currentLocation;
    }

    public void setCurrentLocation(String currentLocation) {
        this.currentLocation = currentLocation;
    }

    public String getPointOfContact() {
        return pointOfContact;
    }

    public void setPointOfContact(String pointOfContact) {
        this.pointOfContact = pointOfContact;
    }

    public String getPromiseTime() {
        return promiseTime;
    }

    public void setPromiseTime(String promiseTime) {
        this.promiseTime = promiseTime;
    }

    public Character getStatus() {
        return status;
    }

    public void setStatus(Character status) {
        this.status = status;
    }

    public ClaimNotification getClaimNotification() {
        return claimNotification;
    }

    public void setClaimNotification(ClaimNotification claimNotification) {
        this.claimNotification = claimNotification;
    }

    public NotificationAssignment getNotificationAssignment() {
        return notificationAssignment;
    }

    public void setNotificationAssignment(NotificationAssignment notificationAssignment) {
        this.notificationAssignment = notificationAssignment;
    }

    public List<NotificationAdjusterCharge> getNotificationAdjusterCharges() {
        return notificationAdjusterCharges;
    }

    public void setNotificationAdjusterCharges(List<NotificationAdjusterCharge> notificationAdjusterCharges) {
        this.notificationAdjusterCharges = notificationAdjusterCharges;
    }
}
