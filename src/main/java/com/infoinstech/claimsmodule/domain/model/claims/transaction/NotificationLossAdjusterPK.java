package com.infoinstech.claimsmodule.domain.model.claims.transaction;

import javax.persistence.Column;
import javax.persistence.Embeddable;
import java.io.Serializable;

@Embeddable
public class NotificationLossAdjusterPK implements Serializable {

    @Column(name = "NAS_NOT_SEQ")
    private String notificationSequenceNo;

    @Column(name = "NAS_REP_SEQ")
    private String assignmentSequenceNo;

    @Column(name = "NAS_SEQ_NO")
    private String sequenceNo;

    public NotificationLossAdjusterPK() {
    }

    public NotificationLossAdjusterPK(String notificationSequenceNo, String assignmentSequenceNo, String sequenceNo) {
        this.notificationSequenceNo = notificationSequenceNo;
        this.assignmentSequenceNo = assignmentSequenceNo;
        this.sequenceNo = sequenceNo;
    }

    public String getNotificationSequenceNo() {
        return notificationSequenceNo;
    }

    public void setNotificationSequenceNo(String notificationSequenceNo) {
        this.notificationSequenceNo = notificationSequenceNo;
    }

    public String getAssignmentSequenceNo() {
        return assignmentSequenceNo;
    }

    public void setAssignmentSequenceNo(String assignmentSequenceNo) {
        this.assignmentSequenceNo = assignmentSequenceNo;
    }

    public String getSequenceNo() {
        return sequenceNo;
    }

    public void setSequenceNo(String sequenceNo) {
        this.sequenceNo = sequenceNo;
    }
}
