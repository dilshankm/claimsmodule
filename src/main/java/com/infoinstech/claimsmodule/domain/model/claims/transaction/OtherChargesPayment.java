package com.infoinstech.claimsmodule.domain.model.claims.transaction;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.util.Date;

@Entity
@Table(name = "CL_T_OTH_CHARGES_PAYMENTS")
public class OtherChargesPayment {

    @Id
    @Column(name = "CTO_SEQ_NO")
    private String sequence;

    @Column(name = "CTO_REQ_SEQ_NO")
    private String reqSeqNo;

    @Column(name = "CTO_CLAIM_NO")
    private String claimNo;

    @Column(name = "CTO_TRANS_TYPE")
    private String transactionType;

    @Column(name = "CTO_INT_SEQ_NO")
    private String intimationSequence;

    @Column(name = "CTO_PAYEE_NAME")
    private String payeeName;

    @Column(name = "CTO_PAYEE_CODE")
    private String payeeCode;

    @Column(name = "CTO_PAY_AMOUNT")
    private Double payAmount;

    @Column(name = "CTO_PRIORITY_LEVEL")
    private String priorityLevel;

    @Column(name = "CTO_CURRENCY")
    private String currency;

    @Column(name = "CTO_EXCHANGE_RATE")
    private String exchangeRate;

    @Column(name = "CREATED_BY")
    private String createdBy;

    @Column(name = "CREATED_DATE")
    private Date createdDate;

    @Column(name = "MODIFIED_BY")
    private String modifiedBy;

    @Column(name = "MODIFIED_DATE")
    private Date modifiedDate;

    @Column(name = "CTO_CUS_CODE")
    private String customerCode;

    @Column(name = "CTO_HAND_DT")
    private Date handDate;

    @Column(name = "CTO_CHEQUE_NO")
    private String chequeNo;

    @Column(name = "CTO_RISK_SEQ")
    private String riskSequence;

    @Column(name = "CTO_PERIL_CODE")
    private String perilCode;

    @Column(name = "CTO_LOC_CODE")
    private String locationCode;

    @Column(name = "CTO_CRM_CODE")
    private String crmCode;

    @Column(name = "CTO_CRED_CODE")
    private String creditorCode;

    @Column(name = "CTO_ADDRESS_1")
    private String address1;

    @Column(name = "CTO_ADDRESS_2")
    private String address2;

    @Column(name = "CTO_ADDRESS_3")
    private String address3;

    @Column(name = "CTO_NARRATION")
    private String narration;

    @Column(name = "CTO_CHEQUE_NAME")
    private String chequeName;

    public String getSequence() {
        return sequence;
    }

    public void setSequence(String sequence) {
        this.sequence = sequence;
    }

    public String getReqSeqNo() {
        return reqSeqNo;
    }

    public void setReqSeqNo(String reqSeqNo) {
        this.reqSeqNo = reqSeqNo;
    }

    public String getClaimNo() {
        return claimNo;
    }

    public void setClaimNo(String claimNo) {
        this.claimNo = claimNo;
    }

    public String getTransactionType() {
        return transactionType;
    }

    public void setTransactionType(String transactionType) {
        this.transactionType = transactionType;
    }

    public String getIntimationSequence() {
        return intimationSequence;
    }

    public void setIntimationSequence(String intimationSequence) {
        this.intimationSequence = intimationSequence;
    }

    public String getPayeeName() {
        return payeeName;
    }

    public void setPayeeName(String payeeName) {
        this.payeeName = payeeName;
    }

    public String getPayeeCode() {
        return payeeCode;
    }

    public void setPayeeCode(String payeeCode) {
        this.payeeCode = payeeCode;
    }

    public Double getPayAmount() {
        return payAmount;
    }

    public void setPayAmount(Double payAmount) {
        this.payAmount = payAmount;
    }

    public String getPriorityLevel() {
        return priorityLevel;
    }

    public void setPriorityLevel(String priorityLevel) {
        this.priorityLevel = priorityLevel;
    }

    public String getCurrency() {
        return currency;
    }

    public void setCurrency(String currency) {
        this.currency = currency;
    }

    public String getExchangeRate() {
        return exchangeRate;
    }

    public void setExchangeRate(String exchangeRate) {
        this.exchangeRate = exchangeRate;
    }

    public String getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(String createdBy) {
        this.createdBy = createdBy;
    }

    public Date getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(Date createdDate) {
        this.createdDate = createdDate;
    }

    public String getModifiedBy() {
        return modifiedBy;
    }

    public void setModifiedBy(String modifiedBy) {
        this.modifiedBy = modifiedBy;
    }

    public Date getModifiedDate() {
        return modifiedDate;
    }

    public void setModifiedDate(Date modifiedDate) {
        this.modifiedDate = modifiedDate;
    }

    public String getCustomerCode() {
        return customerCode;
    }

    public void setCustomerCode(String customerCode) {
        this.customerCode = customerCode;
    }

    public Date getHandDate() {
        return handDate;
    }

    public void setHandDate(Date handDate) {
        this.handDate = handDate;
    }

    public String getChequeNo() {
        return chequeNo;
    }

    public void setChequeNo(String chequeNo) {
        this.chequeNo = chequeNo;
    }

    public String getRiskSequence() {
        return riskSequence;
    }

    public void setRiskSequence(String riskSequence) {
        this.riskSequence = riskSequence;
    }

    public String getPerilCode() {
        return perilCode;
    }

    public void setPerilCode(String perilCode) {
        this.perilCode = perilCode;
    }

    public String getLocationCode() {
        return locationCode;
    }

    public void setLocationCode(String locationCode) {
        this.locationCode = locationCode;
    }

    public String getCrmCode() {
        return crmCode;
    }

    public void setCrmCode(String crmCode) {
        this.crmCode = crmCode;
    }

    public String getCreditorCode() {
        return creditorCode;
    }

    public void setCreditorCode(String creditorCode) {
        this.creditorCode = creditorCode;
    }

    public String getAddress1() {
        return address1;
    }

    public void setAddress1(String address1) {
        this.address1 = address1;
    }

    public String getAddress2() {
        return address2;
    }

    public void setAddress2(String address2) {
        this.address2 = address2;
    }

    public String getAddress3() {
        return address3;
    }

    public void setAddress3(String address3) {
        this.address3 = address3;
    }

    public String getNarration() {
        return narration;
    }

    public void setNarration(String narration) {
        this.narration = narration;
    }

    public String getChequeName() {
        return chequeName;
    }

    public void setChequeName(String chequeName) {
        this.chequeName = chequeName;
    }
}
