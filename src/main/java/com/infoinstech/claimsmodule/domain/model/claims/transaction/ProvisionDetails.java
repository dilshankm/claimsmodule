package com.infoinstech.claimsmodule.domain.model.claims.transaction;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import java.util.Date;

@Entity
@Table(name = "CL_T_PROVISION_DTLS")
public class ProvisionDetails {

    public static final String SEQUENCE = "SEQ_CL_T_PROVISION_DTLS";

    @Id
    @Column(name = "PRD_SEQ_NO")
    private String provisionSeqNo;

    @Column(name = "PRD_COMMENTS")
    private String comments;

    @Column(name = "PRD_CLAIM_NO")
    private String claimNo;

    @Column(name = "PRD_VALUE")
    private Double provisionValue;

    @Column(name = "CREATED_BY")
    private String createdBy;

    @Column(name = "CREATED_DATE")
    private Date createdDate;

    @Column(name = "MODIFIED_BY")
    private String modifiedBy;

    @Column(name = "MODIFIED_DATE")
    private Date modifiedDate;

    @Column(name = "PRD_INT_SEQ")
    private String intimationSequenceNo;

    @Column(name = "PRD_PRS_SEQ_NO")
    private String riskSeqNo;

    @NotNull
    @Column(name = "PRD_PRV_TYPE")
    private String provisionType;

    @Column(name = "PRD_FUNCTION_ID")
    private String functionId;

    @Column(name = "PRD_RI_FLAG")
    private Character reinsuredFlag;

    @Column(name = "PRD_CRD_SEQ_NO")
    private String crdSeqNo;

    @NotNull
    @Column(name = "PRD_LOC_CODE")
    private String locationCode;

    @NotNull
    @Column(name = "PRD_PERIL_CODE")
    private String perilCode;

    @NotNull
    @Column(name = "PRD_PRS_R_SEQ")
    private String riskOrderNo;

    @Column(name = "PRD_CREATED_DATE")
    private Date provisionCreatedDate;

    @Column(name = "PRD_REQ_SEQ")
    private String requisitionSeqNo;

    @Column(name = "PRD_RI_RECO_FLAG")
    private Character reinsuranceRecoveredFlag;

    @Column(name = "PRD_GL_VALUE")
    private Double glValue;

    @Column(name = "PRD_TP_VOUCHER_NO")
    private String paymentVoucherNo;

    @Column(name = "PRD_MIGRATED_REC")
    private Character migratedRecord;

    @Column(name = "PRD_CAT_XOL_PROV_FLAG")
    private Character catXOLProvisionFlag;

    @Column(name = "PRD_CAT_XOL_RECO_FLAG")
    private Character catXOLRecoveredFlag;

    public String getProvisionSeqNo() {
        return provisionSeqNo;
    }

    public void setProvisionSeqNo(String provisionSeqNo) {
        this.provisionSeqNo = provisionSeqNo;
    }

    public String getComments() {
        return comments;
    }

    public void setComments(String comments) {
        this.comments = comments;
    }

    public String getClaimNo() {
        return claimNo;
    }

    public void setClaimNo(String claimNo) {
        this.claimNo = claimNo;
    }

    public Double getProvisionValue() {
        return provisionValue;
    }

    public void setProvisionValue(Double provisionValue) {
        this.provisionValue = provisionValue;
    }

    public String getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(String createdBy) {
        this.createdBy = createdBy;
    }

    public Date getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(Date createdDate) {
        this.createdDate = createdDate;
    }

    public String getModifiedBy() {
        return modifiedBy;
    }

    public void setModifiedBy(String modifiedBy) {
        this.modifiedBy = modifiedBy;
    }

    public Date getModifiedDate() {
        return modifiedDate;
    }

    public void setModifiedDate(Date modifiedDate) {
        this.modifiedDate = modifiedDate;
    }

    public String getIntimationSequenceNo() {
        return intimationSequenceNo;
    }

    public void setIntimationSequenceNo(String intimationSequenceNo) {
        this.intimationSequenceNo = intimationSequenceNo;
    }

    public String getRiskSeqNo() {
        return riskSeqNo;
    }

    public void setRiskSeqNo(String riskSeqNo) {
        this.riskSeqNo = riskSeqNo;
    }

    public String getProvisionType() {
        return provisionType;
    }

    public void setProvisionType(String provisionType) {
        this.provisionType = provisionType;
    }

    public String getFunctionId() {
        return functionId;
    }

    public void setFunctionId(String functionId) {
        this.functionId = functionId;
    }

    public Character getReinsuredFlag() {
        return reinsuredFlag;
    }

    public void setReinsuredFlag(Character reinsuredFlag) {
        this.reinsuredFlag = reinsuredFlag;
    }

    public String getCrdSeqNo() {
        return crdSeqNo;
    }

    public void setCrdSeqNo(String crdSeqNo) {
        this.crdSeqNo = crdSeqNo;
    }

    public String getLocationCode() {
        return locationCode;
    }

    public void setLocationCode(String locationCode) {
        this.locationCode = locationCode;
    }

    public String getPerilCode() {
        return perilCode;
    }

    public void setPerilCode(String perilCode) {
        this.perilCode = perilCode;
    }

    public String getRiskOrderNo() {
        return riskOrderNo;
    }

    public void setRiskOrderNo(String riskOrderNo) {
        this.riskOrderNo = riskOrderNo;
    }

    public Date getProvisionCreatedDate() {
        return provisionCreatedDate;
    }

    public void setProvisionCreatedDate(Date provisionCreatedDate) {
        this.provisionCreatedDate = provisionCreatedDate;
    }

    public String getRequisitionSeqNo() {
        return requisitionSeqNo;
    }

    public void setRequisitionSeqNo(String requisitionSeqNo) {
        this.requisitionSeqNo = requisitionSeqNo;
    }

    public Character getReinsuranceRecoveredFlag() {
        return reinsuranceRecoveredFlag;
    }

    public void setReinsuranceRecoveredFlag(Character reinsuranceRecoveredFlag) {
        this.reinsuranceRecoveredFlag = reinsuranceRecoveredFlag;
    }

    public Double getGlValue() {
        return glValue;
    }

    public void setGlValue(Double glValue) {
        this.glValue = glValue;
    }

    public String getPaymentVoucherNo() {
        return paymentVoucherNo;
    }

    public void setPaymentVoucherNo(String paymentVoucherNo) {
        this.paymentVoucherNo = paymentVoucherNo;
    }

    public Character getMigratedRecord() {
        return migratedRecord;
    }

    public void setMigratedRecord(Character migratedRecord) {
        this.migratedRecord = migratedRecord;
    }

    public Character getCatXOLProvisionFlag() {
        return catXOLProvisionFlag;
    }

    public void setCatXOLProvisionFlag(Character catXOLProvisionFlag) {
        this.catXOLProvisionFlag = catXOLProvisionFlag;
    }

    public Character getCatXOLRecoveredFlag() {
        return catXOLRecoveredFlag;
    }

    public void setCatXOLRecoveredFlag(Character catXOLRecoveredFlag) {
        this.catXOLRecoveredFlag = catXOLRecoveredFlag;
    }
}

