package com.infoinstech.claimsmodule.domain.model.claims.transaction;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;

@Entity
@Table(name = "CL_T_PROV_PAYMENT_DTLS")
public class ProvisionPaymentDetails implements Serializable {

    @Id
    @Column(name = "MRD_SEQ_NO")
    private String SequenceNo;

    @Column(name = "MRD_COMMENTS")
    private String comments;

    @Column(name = "MRD_CLAIM_NO")
    private String claimNo;

    @Column(name = "MRD_INT_SEQ")
    private String intimationSequenceNo;

    @Column(name = "MRD_VALUE")
    private Double value;

    @Column(name = "CREATED_BY")
    private String createdBy;

    @Column(name = "CREATED_DATE")
    private Date createdDate;

    @Column(name = "MODIFIED_BY")
    private String modifiedBy;

    @Column(name = "MODIFIED_DATE")
    private Date modifiedDate;

    @Column(name = "MRD_REV_TYPE")
    private String revisionType;

    @Column(name = "MRD_FUNCTION_ID")
    private String functionId;

    @Column(name = "MRD_REQ_SEQ")
    private String requisitionSequenceNo;

    @Column(name = "MRD_RI_FLAG")
    private Character reinsuranceFlag;

//    @Column(name="MRD_CRD_SEQ_NO VARCHAR2(15),

    @Column(name = "MRD_LOC_CODE")
    private String locationCode;

    @Column(name = "MRD_PERIL_CODE")
    private String perilCode;

    @Column(name = "MRD_PRS_R_SEQ")
    private String riskOrderNo;

    @Column(name = "MRD_CREATED_DATE")
    private Date paymentCreatedDate;

    @Column(name = "MRD_RI_RECO_FLAG")
    private Character reinsuranceRecoveryFlag;

    @Column(name = "MRD_GL_VALUE")
    private Double glValue;

    @Column(name = "MRD_MIGRATED_REC")
    private Character migratedRecord;

    @Column(name = "MRD_CAT_XOL_PROV_FLAG")
    private Character catastrophicExcessOfLossProvisionFlag;

    @Column(name = "MRD_CAT_XOL_RECO_FLAG")
    private Character catastrophicExcessOfLossRecordFlag;

    @ManyToOne
    @JoinColumn(name = "MRD_INT_SEQ", referencedColumnName = "INT_SEQ_NO", insertable = false, updatable = false)
    private ClaimIntimation claimIntimation;

    public String getSequenceNo() {
        return SequenceNo;
    }

    public void setSequenceNo(String sequenceNo) {
        SequenceNo = sequenceNo;
    }

    public String getComments() {
        return comments;
    }

    public void setComments(String comments) {
        this.comments = comments;
    }

    public String getClaimNo() {
        return claimNo;
    }

    public void setClaimNo(String claimNo) {
        this.claimNo = claimNo;
    }

    public String getIntimationSequenceNo() {
        return intimationSequenceNo;
    }

    public void setIntimationSequenceNo(String intimationSequenceNo) {
        this.intimationSequenceNo = intimationSequenceNo;
    }

    public Double getValue() {
        return value;
    }

    public void setValue(Double value) {
        this.value = value;
    }

    public String getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(String createdBy) {
        this.createdBy = createdBy;
    }

    public Date getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(Date createdDate) {
        this.createdDate = createdDate;
    }

    public String getModifiedBy() {
        return modifiedBy;
    }

    public void setModifiedBy(String modifiedBy) {
        this.modifiedBy = modifiedBy;
    }

    public Date getModifiedDate() {
        return modifiedDate;
    }

    public void setModifiedDate(Date modifiedDate) {
        this.modifiedDate = modifiedDate;
    }

    public String getRevisionType() {
        return revisionType;
    }

    public void setRevisionType(String revisionType) {
        this.revisionType = revisionType;
    }

    public String getFunctionId() {
        return functionId;
    }

    public void setFunctionId(String functionId) {
        this.functionId = functionId;
    }

    public String getRequisitionSequenceNo() {
        return requisitionSequenceNo;
    }

    public void setRequisitionSequenceNo(String requisitionSequenceNo) {
        this.requisitionSequenceNo = requisitionSequenceNo;
    }

    public Character getReinsuranceFlag() {
        return reinsuranceFlag;
    }

    public void setReinsuranceFlag(Character reinsuranceFlag) {
        this.reinsuranceFlag = reinsuranceFlag;
    }

    public String getLocationCode() {
        return locationCode;
    }

    public void setLocationCode(String locationCode) {
        this.locationCode = locationCode;
    }

    public String getPerilCode() {
        return perilCode;
    }

    public void setPerilCode(String perilCode) {
        this.perilCode = perilCode;
    }

    public String getRiskOrderNo() {
        return riskOrderNo;
    }

    public void setRiskOrderNo(String riskOrderNo) {
        this.riskOrderNo = riskOrderNo;
    }

    public Date getPaymentCreatedDate() {
        return paymentCreatedDate;
    }

    public void setPaymentCreatedDate(Date paymentCreatedDate) {
        this.paymentCreatedDate = paymentCreatedDate;
    }

    public Character getReinsuranceRecoveryFlag() {
        return reinsuranceRecoveryFlag;
    }

    public void setReinsuranceRecoveryFlag(Character reinsuranceRecoveryFlag) {
        this.reinsuranceRecoveryFlag = reinsuranceRecoveryFlag;
    }

    public Double getGlValue() {
        return glValue;
    }

    public void setGlValue(Double glValue) {
        this.glValue = glValue;
    }

    public Character getMigratedRecord() {
        return migratedRecord;
    }

    public void setMigratedRecord(Character migratedRecord) {
        this.migratedRecord = migratedRecord;
    }

    public Character getCatastrophicExcessOfLossProvisionFlag() {
        return catastrophicExcessOfLossProvisionFlag;
    }

    public void setCatastrophicExcessOfLossProvisionFlag(Character catastrophicExcessOfLossProvisionFlag) {
        this.catastrophicExcessOfLossProvisionFlag = catastrophicExcessOfLossProvisionFlag;
    }

    public Character getCatastrophicExcessOfLossRecordFlag() {
        return catastrophicExcessOfLossRecordFlag;
    }

    public void setCatastrophicExcessOfLossRecordFlag(Character catastrophicExcessOfLossRecordFlag) {
        this.catastrophicExcessOfLossRecordFlag = catastrophicExcessOfLossRecordFlag;
    }

    public ClaimIntimation getClaimIntimation() {
        return claimIntimation;
    }

    public void setClaimIntimation(ClaimIntimation claimIntimation) {
        this.claimIntimation = claimIntimation;
    }
}
