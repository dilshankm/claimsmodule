package com.infoinstech.claimsmodule.domain.model.claims.transaction;

import javax.persistence.*;
import java.util.Date;

@Entity
@Table(name = "CL_T_REQUISITION")
public class Requisition {

    @EmbeddedId
    private RequisitionPK requisitionPK;

    @Column(name="REQ_REQUISITION_NO")
    private String requisitioNo;

    @Column(name="REQ_AMOUNT")
    private Double requisitionAmount;

    @Column(name="REQ_COMMENTS")
    private String comments;

    @Column(name ="CREATED_BY")
    private String createdBy;

    @Column(name = "CREATED_DATE")
    private Date createdDate;

    @Column(name = "MODIFIED_BY")
    private String modifiedBy;

    @Column(name = "MODIFIED_DATE")
    private Date modifiedDate;

    @Column(name="REQ_PPY_SEQ")
    private String partialPaySequnceNo;

    @Column(name="REQ_PAY_TYPE")
    private Character paymentType;

    @Column(name="REQ_STATUS_FLG")
    private Character status;

    @Column(name="REQ_PVOUCHER_NO")
    private String voucherNo;

    @Column(name="REQ_SETMENT_TYPE")
    private String settlementType;

    @Column(name="REQ_LOSS_TYPE")
    private String lossType;

    @Column(name="REQ_DATE")
    @Temporal(TemporalType.TIMESTAMP)
    private Date requisitionDate;

    @Column(name="REQ_SER_SEQ")
    private String serviceSequenceNo;

    @Column(name="REQ_CONV_RATE")
    private Double conversionRate;
    @Column(name="REQ_SET_BRANCH")
    private String settlementBranch;

    @Column(name="REQ_CONV_RATE_TYPE")
    private Character conversionRateType;

    @Column(name="REQ_RCT_EXESS")
    private Double excess;

    @Column(name="REQ_RRD_SEQ")
    private String revisionDetailsSequenceNo;

    @Column(name="REQ_EXGRATIA")
    private Double exgratia;

    @Column(name="REQ_SALVAGE")
    private Double requisitionSalvage;

    public RequisitionPK getRequisitionPK() {
        return requisitionPK;
    }

    public void setRequisitionPK(RequisitionPK requisitionPK) {
        this.requisitionPK = requisitionPK;
    }

    public String getRequisitioNo() {
        return requisitioNo;
    }

    public void setRequisitioNo(String requisitioNo) {
        this.requisitioNo = requisitioNo;
    }

    public Double getRequisitionAmount() {
        return requisitionAmount;
    }

    public void setRequisitionAmount(Double requisitionAmount) {
        this.requisitionAmount = requisitionAmount;
    }

    public String getComments() {
        return comments;
    }

    public void setComments(String comments) {
        this.comments = comments;
    }

    public String getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(String createdBy) {
        this.createdBy = createdBy;
    }

    public Date getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(Date createdDate) {
        this.createdDate = createdDate;
    }

    public String getModifiedBy() {
        return modifiedBy;
    }

    public void setModifiedBy(String modifiedBy) {
        this.modifiedBy = modifiedBy;
    }

    public Date getModifiedDate() {
        return modifiedDate;
    }

    public void setModifiedDate(Date modifiedDate) {
        this.modifiedDate = modifiedDate;
    }

    public String getPartialPaySequnceNo() {
        return partialPaySequnceNo;
    }

    public void setPartialPaySequnceNo(String partialPaySequnceNo) {
        this.partialPaySequnceNo = partialPaySequnceNo;
    }

    public Character getPaymentType() {
        return paymentType;
    }

    public void setPaymentType(Character paymentType) {
        this.paymentType = paymentType;
    }

    public Character getStatus() {
        return status;
    }

    public void setStatus(Character status) {
        this.status = status;
    }

    public String getVoucherNo() {
        return voucherNo;
    }

    public void setVoucherNo(String voucherNo) {
        this.voucherNo = voucherNo;
    }

    public String getSettlementType() {
        return settlementType;
    }

    public void setSettlementType(String settlementType) {
        this.settlementType = settlementType;
    }

    public String getLossType() {
        return lossType;
    }

    public void setLossType(String lossType) {
        this.lossType = lossType;
    }

    public Date getRequisitionDate() {
        return requisitionDate;
    }

    public void setRequisitionDate(Date requisitionDate) {
        this.requisitionDate = requisitionDate;
    }

    public String getServiceSequenceNo() {
        return serviceSequenceNo;
    }

    public void setServiceSequenceNo(String serviceSequenceNo) {
        this.serviceSequenceNo = serviceSequenceNo;
    }

    public Double getConversionRate() {
        return conversionRate;
    }

    public void setConversionRate(Double conversionRate) {
        this.conversionRate = conversionRate;
    }

    public String getSettlementBranch() {
        return settlementBranch;
    }

    public void setSettlementBranch(String settlementBranch) {
        this.settlementBranch = settlementBranch;
    }

    public Character getConversionRateType() {
        return conversionRateType;
    }

    public void setConversionRateType(Character conversionRateType) {
        this.conversionRateType = conversionRateType;
    }

    public Double getExcess() {
        return excess;
    }

    public void setExcess(Double excess) {
        this.excess = excess;
    }

    public String getRevisionDetailsSequenceNo() {
        return revisionDetailsSequenceNo;
    }

    public void setRevisionDetailsSequenceNo(String revisionDetailsSequenceNo) {
        this.revisionDetailsSequenceNo = revisionDetailsSequenceNo;
    }

    public Double getExgratia() {
        return exgratia;
    }

    public void setExgratia(Double exgratia) {
        this.exgratia = exgratia;
    }

    public Double getRequisitionSalvage() {
        return requisitionSalvage;
    }

    public void setRequisitionSalvage(Double requisitionSalvage) {
        this.requisitionSalvage = requisitionSalvage;
    }
}