package com.infoinstech.claimsmodule.domain.model.claims.transaction;

import javax.persistence.Column;
import javax.persistence.Embeddable;
import java.io.Serializable;

@Embeddable
public class RequisitionPK implements Serializable {

    @Column(name="REQ_SEQ_NO")
    private String sequenceNo;

    @Column(name="REQ_INT_SEQ")
    private String intimationSequenceNo;

    public RequisitionPK() {
    }

    public String getSequenceNo() {
        return sequenceNo;
    }

    public void setSequenceNo(String sequenceNo) {
        this.sequenceNo = sequenceNo;
    }

    public String getIntimationSequenceNo() {
        return intimationSequenceNo;
    }

    public void setIntimationSequenceNo(String intimationSequenceNo) {
        this.intimationSequenceNo = intimationSequenceNo;
    }


}
