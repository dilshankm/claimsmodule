package com.infoinstech.claimsmodule.domain.model.claims.transaction;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.util.Date;
import java.util.List;

@Entity
@Table(name = "CL_T_REQ_PAYEES")
public class RequisitionPayee {

    @EmbeddedId
    private RequisitionPayeePK requisitionPayeePK;

    @OneToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "RPQ_INT_SEQ", referencedColumnName = "INT_SEQ_NO", insertable = false, updatable = false)
    private ClaimIntimation rpqIntSeqNo;

    @Column(name = "RPQ_ADDRESS_2")
    private String address2;

    @Column(name = "RPQ_ADDRESS_3")
    private String address3;

    @Column(name = "RPQ_NARRATION")
    private String narration;

    @Column(name = "RPQ_HAND_AUTH_CODE")
    private String handAuthCode;

    @Column(name = "RPQ_CRM_CODE")
    private String crmCode;

    @Column(name = "RPQ_HAND_BY")
    private String handBy;

    @Column(name = "RPQ_CHEQUE_NAME")
    private String chequeName;

    @Column(name = "RPQ_CHEQUE_FAV")
    private String chequeFav;

    @Column(name = "RPQ_CHEQUE_NO")
    private String chequeNo;

    @Column(name = "RPQ_CRED_CODE")
    private String credCode;

    @Column(name = "RPQ_PRI_LVL")
    private String prvLevel;

    @Column(name = "CREATED_BY")
    private String createdBy;

    @Column(name = "MODIFIED_BY")
    private String modifiedBy;

    @Column(name = "RPQ_PVOUCHER_NO")
    private String pvoucherNo;

    @Column(name = "RPQ_COMMENTS")
    private String comment;

    @Column(name = "RPQ_ADDRESS")
    private String address;

    @Column(name = "RPQ_HAND_TO")
    private String handTo;

    @Column(name = "RPQ_PAYEE_TYPE")
    private String payeeType;

    @Column(name = "RPQ_HAND_LEVEL")
    private Integer handLevel;

    @Column(name = "RPQ_AMOUNT")
    private Double amount;

    @Column(name = "MODIFIED_DATE")
    private Date modifiedDate;

    @Column(name = "CREATED_DATE")
    private Date createdDate;

    @Column(name = "RPQ_HAND_DT")
    private Date handDate;

    @Column(name = "RPQ_ISSUED_DT")
    private Date issuedDate;

    @Column(name = "RPQ_RECEIVED_DT")
    private Date receivedDate;

    @Column(name = "RPQ_EFFECT_DATE")
    private Date effectDdate;

    public ClaimIntimation getRpqIntSeqNo() {
        return rpqIntSeqNo;
    }

    public void setRpqIntSeqNo(ClaimIntimation rpqIntSeqNo) {
        this.rpqIntSeqNo = rpqIntSeqNo;
    }

    public RequisitionPayeePK getRequisitionPayeePK() {
        return requisitionPayeePK;
    }

    public void setRequisitionPayeePK(RequisitionPayeePK requisitionPayeePK) {
        this.requisitionPayeePK = requisitionPayeePK;
    }

    public String getAddress2() {
        return address2;
    }

    public void setAddress2(String address2) {
        this.address2 = address2;
    }

    public String getAddress3() {
        return address3;
    }

    public void setAddress3(String address3) {
        this.address3 = address3;
    }

    public String getNarration() {
        return narration;
    }

    public void setNarration(String narration) {
        this.narration = narration;
    }

    public String getHandAuthCode() {
        return handAuthCode;
    }

    public void setHandAuthCode(String handAuthCode) {
        this.handAuthCode = handAuthCode;
    }

    public String getCrmCode() {
        return crmCode;
    }

    public void setCrmCode(String crmCode) {
        this.crmCode = crmCode;
    }

    public String getHandBy() {
        return handBy;
    }

    public void setHandBy(String handBy) {
        this.handBy = handBy;
    }

    public String getChequeName() {
        return chequeName;
    }

    public void setChequeName(String chequeName) {
        this.chequeName = chequeName;
    }

    public String getChequeFav() {
        return chequeFav;
    }

    public void setChequeFav(String chequeFav) {
        this.chequeFav = chequeFav;
    }

    public String getChequeNo() {
        return chequeNo;
    }

    public void setChequeNo(String chequeNo) {
        this.chequeNo = chequeNo;
    }

    public String getCredCode() {
        return credCode;
    }

    public void setCredCode(String credCode) {
        this.credCode = credCode;
    }

    public String getPrvLevel() {
        return prvLevel;
    }

    public void setPrvLevel(String prvLevel) {
        this.prvLevel = prvLevel;
    }

    public String getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(String createdBy) {
        this.createdBy = createdBy;
    }

    public String getModifiedBy() {
        return modifiedBy;
    }

    public void setModifiedBy(String modifiedBy) {
        this.modifiedBy = modifiedBy;
    }

    public String getPvoucherNo() {
        return pvoucherNo;
    }

    public void setPvoucherNo(String pvoucherNo) {
        this.pvoucherNo = pvoucherNo;
    }

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getHandTo() {
        return handTo;
    }

    public void setHandTo(String handTo) {
        this.handTo = handTo;
    }

    public String getPayeeType() {
        return payeeType;
    }

    public void setPayeeType(String payeeType) {
        this.payeeType = payeeType;
    }

    public Integer getHandLevel() {
        return handLevel;
    }

    public void setHandLevel(Integer handLevel) {
        this.handLevel = handLevel;
    }

    public Double getAmount() {
        return amount;
    }

    public void setAmount(Double amount) {
        this.amount = amount;
    }

    public Date getModifiedDate() {
        return modifiedDate;
    }

    public void setModifiedDate(Date modifiedDate) {
        this.modifiedDate = modifiedDate;
    }

    public Date getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(Date createdDate) {
        this.createdDate = createdDate;
    }

    public Date getHandDate() {
        return handDate;
    }

    public void setHandDate(Date handDate) {
        this.handDate = handDate;
    }

    public Date getIssuedDate() {
        return issuedDate;
    }

    public void setIssuedDate(Date issuedDate) {
        this.issuedDate = issuedDate;
    }

    public Date getReceivedDate() {
        return receivedDate;
    }

    public void setReceivedDate(Date receivedDate) {
        this.receivedDate = receivedDate;
    }

    public Date getEffectDdate() {
        return effectDdate;
    }

    public void setEffectDdate(Date effectDdate) {
        this.effectDdate = effectDdate;
    }
}
