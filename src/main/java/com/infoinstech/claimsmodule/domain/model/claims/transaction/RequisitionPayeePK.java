package com.infoinstech.claimsmodule.domain.model.claims.transaction;

import javax.persistence.Column;
import javax.persistence.Embeddable;
import java.io.Serializable;

@Embeddable
public class RequisitionPayeePK implements Serializable {

    @Column(name = "RPQ_SEQ_NO")
    private String sequenceNo;

    @Column(name = "RPQ_REQ_NO")
    private String requisitionNo;

    @Column(name = "RPQ_INT_SEQ")
    private String intimationSequenceNo;

    public String getSequenceNo() {
        return sequenceNo;
    }

    public void setSequenceNo(String sequenceNo) {
        this.sequenceNo = sequenceNo;
    }

    public String getRequisitionNo() {
        return requisitionNo;
    }

    public void setRequisitionNo(String requisitionNo) {
        this.requisitionNo = requisitionNo;
    }

    public String getIntimationSequenceNo() {
        return intimationSequenceNo;
    }

    public void setIntimationSequenceNo(String intimationSequenceNo) {
        this.intimationSequenceNo = intimationSequenceNo;
    }
}
