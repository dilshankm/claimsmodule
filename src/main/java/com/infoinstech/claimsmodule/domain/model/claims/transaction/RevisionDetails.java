package com.infoinstech.claimsmodule.domain.model.claims.transaction;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import java.util.Date;

@Entity
@Table(name = "CL_T_PROV_REVISION_DTLS")
public class RevisionDetails {

    public static final String SEQUENCE = "SEQ_CL_T_PROV_REVISION_DTLS";
    @Id
    @Column(name = "RRD_SEQ_NO")
    private String revisionSeqNo;

    @Column(name = "RRD_COMMENTS")
    private String comments;

    @Column(name = "RRD_CLAIM_NO")
    private String claimNo;

    @NotNull
    @Column(name = "RRD_INT_SEQ")
    private String intimationSeqNo;

    @Column(name = "RRD_VALUE")
    private Double revisionValue;

    @Column(name = "CREATED_BY")
    private String createdBy;

    @NotNull
    @Column(name = "CREATED_DATE")
    private Date createdDate;

    @Column(name = "MODIFIED_BY")
    private String modifiedBy;

    @Column(name = "MODIFIED_DATE")
    private Date modifiedDate;

    @NotNull
    @Column(name = "RRD_REV_TYPE")
    private String revisionType;


    @Column(name = "RRD_FUNCTION_ID")
    private String functionId;

    @Column(name = "RRD_REQ_SEQ")
    private String requisitionSeqNo;

    @Column(name = "RRD_RI_FLAG")
    private Character reinsuranceFlag;

    @Column(name = "RRD_CRD_SEQ_NO")
    private String creditorSeqNo;

    @NotNull
    @Column(name = "RRD_LOC_CODE")
    private String locationCode;

    @NotNull
    @Column(name = "RRD_PERIL_CODE")
    private String perilCode;

    @NotNull
    @Column(name = "RRD_PRS_R_SEQ")
    private String riskOrderNo;


    @Column(name = "RRD_CREATED_DATE")
    private Date provisionCreatedDate;

    @Column(name = "RRD_RI_RECO_FLAG")
    private Character recordFlag;

    @Column(name = "RRD_GL_VALUE")
    private Double glValue;

    @Column(name = "RRD_MIGRATED_REC")
    private Character migratedRecord;

//    @Column(name="RRD_MEM_BAL_ANNU_LIMIT")
//    private Double annualLimit;

//    @Column(name="RRD_MEM_BAL_EVENT_LIMIT")
//    private Double eventLimit;

    @Column(name = "RRD_CAT_XOL_PROV_FLAG")
    private Character catXolProvisionFlag;

    @Column(name = "RRD_CAT_XOL_RECO_FLAG")
    private Character catXolRecordFlag;

//    @Column(name="RRD_MIGRATED")
//    private Character migrated;

    public String getRevisionSeqNo() {
        return revisionSeqNo;
    }

    public void setRevisionSeqNo(String revisionSeqNo) {
        this.revisionSeqNo = revisionSeqNo;
    }

    public String getComments() {
        return comments;
    }

    public void setComments(String comments) {
        this.comments = comments;
    }

    public String getClaimNo() {
        return claimNo;
    }

    public void setClaimNo(String claimNo) {
        this.claimNo = claimNo;
    }

    public String getIntimationSeqNo() {
        return intimationSeqNo;
    }

    public void setIntimationSeqNo(String intimationSeqNo) {
        this.intimationSeqNo = intimationSeqNo;
    }

    public Double getRevisionValue() {
        return revisionValue;
    }

    public void setRevisionValue(Double revisionValue) {
        this.revisionValue = revisionValue;
    }

    public String getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(String createdBy) {
        this.createdBy = createdBy;
    }

    public Date getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(Date createdDate) {
        this.createdDate = createdDate;
    }

    public String getModifiedBy() {
        return modifiedBy;
    }

    public void setModifiedBy(String modifiedBy) {
        this.modifiedBy = modifiedBy;
    }

    public Date getModifiedDate() {
        return modifiedDate;
    }

    public void setModifiedDate(Date modifiedDate) {
        this.modifiedDate = modifiedDate;
    }

    public String getRevisionType() {
        return revisionType;
    }

    public void setRevisionType(String revisionType) {
        this.revisionType = revisionType;
    }

    public String getFunctionId() {
        return functionId;
    }

    public void setFunctionId(String functionId) {
        this.functionId = functionId;
    }

    public String getRequisitionSeqNo() {
        return requisitionSeqNo;
    }

    public void setRequisitionSeqNo(String requisitionSeqNo) {
        this.requisitionSeqNo = requisitionSeqNo;
    }

    public Character getReinsuranceFlag() {
        return reinsuranceFlag;
    }

    public void setReinsuranceFlag(Character reinsuranceFlag) {
        this.reinsuranceFlag = reinsuranceFlag;
    }

    public String getCreditorSeqNo() {
        return creditorSeqNo;
    }

    public void setCreditorSeqNo(String creditorSeqNo) {
        this.creditorSeqNo = creditorSeqNo;
    }

    public String getLocationCode() {
        return locationCode;
    }

    public void setLocationCode(String locationCode) {
        this.locationCode = locationCode;
    }

    public String getPerilCode() {
        return perilCode;
    }

    public void setPerilCode(String perilCode) {
        this.perilCode = perilCode;
    }

    public String getRiskOrderNo() {
        return riskOrderNo;
    }

    public void setRiskOrderNo(String riskOrderNo) {
        this.riskOrderNo = riskOrderNo;
    }

    public Date getProvisionCreatedDate() {
        return provisionCreatedDate;
    }

    public void setProvisionCreatedDate(Date provisionCreatedDate) {
        this.provisionCreatedDate = provisionCreatedDate;
    }

    public Character getRecordFlag() {
        return recordFlag;
    }

    public void setRecordFlag(Character recordFlag) {
        this.recordFlag = recordFlag;
    }

    public Double getGlValue() {
        return glValue;
    }

    public void setGlValue(Double glValue) {
        this.glValue = glValue;
    }

    public Character getMigratedRecord() {
        return migratedRecord;
    }

    public void setMigratedRecord(Character migratedRecord) {
        this.migratedRecord = migratedRecord;
    }

//    public Double getAnnualLimit() {
//        return annualLimit;
//    }
//
//    public void setAnnualLimit(Double annualLimit) {
//        this.annualLimit = annualLimit;
//    }

/*    public Double getEventLimit() {
        return eventLimit;
    }

    public void setEventLimit(Double eventLimit) {
        this.eventLimit = eventLimit;
    }*/

    public Character getCatXolProvisionFlag() {
        return catXolProvisionFlag;
    }

    public void setCatXolProvisionFlag(Character catXolProvisionFlag) {
        this.catXolProvisionFlag = catXolProvisionFlag;
    }

    public Character getCatXolRecordFlag() {
        return catXolRecordFlag;
    }

    public void setCatXolRecordFlag(Character catXolRecordFlag) {
        this.catXolRecordFlag = catXolRecordFlag;
    }

/*    public Character getMigrated() {
        return migrated;
    }

    public void setMigrated(Character migrated) {
        this.migrated = migrated;
    }*/
}

