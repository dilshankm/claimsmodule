package com.infoinstech.claimsmodule.domain.model.common;

import com.infoinstech.claimsmodule.domain.model.salesmarketing.SystemUser;

import javax.persistence.*;

@Entity
@Table(name = "CM_T_LOG_ATTEMPTS")
public class LoginAttempt {

		@Id
		@Column(name = "LGA_SEQ_NO")
		private String sequenceNo;

		@Column(name = "LGA_SFC_CODE")
		private String code;

		@Column(name = "LGA_SFC_USERNAME")
		private String username;

		@Column(name = "LGA_FAIL_ATTEMPTS")
		private int failAttempts;

		@Column(name = "LGA_ACTIVE")
		private String active;

		public String getSequenceNo() {
				return sequenceNo;
		}

		public void setSequenceNo(String sequenceNo) {
				this.sequenceNo = sequenceNo;
		}

		public String getCode() {
				return code;
		}

		public void setCode(String code) {
				this.code = code;
		}

		public String getUsername() {
				return username;
		}

		public void setUsername(String username) {
				this.username = username;
		}

		public int getFailAttempts() {
				return failAttempts;
		}

		public void setFailAttempts(int failAttempts) {
				this.failAttempts = failAttempts;
		}

		public String getActive() {
				return active;
		}

		public void setActive(String active) {
				this.active = active;
		}

}
