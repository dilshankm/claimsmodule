package com.infoinstech.claimsmodule.domain.model.common;

import javax.persistence.*;

@Entity
@Table(name = "CMC_REPORT_LOGO")
public class Logo {

    @Id
    @Column(name = "cmg_seq_no")
    private String cmgSeqNo;

    @Column(name = "cmg_rep_type")
    private String cmgRepType;

    @Column(name = "cmg_active")
    private String cmgActive;

    @Lob
    @Column(name = "cmg_logo", nullable = false, columnDefinition = "mediumblob")
    private byte[] cmgLogo;


    public String getCmgSeqNo() {
        return cmgSeqNo;
    }

    public void setCmgSeqNo(String cmgSeqNo) {
        this.cmgSeqNo = cmgSeqNo;
    }

    public String getCmgRepType() {
        return cmgRepType;
    }

    public void setCmgRepType(String cmgRepType) {
        this.cmgRepType = cmgRepType;
    }

    public String getCmgActive() {
        return cmgActive;
    }

    public void setCmgActive(String cmgActive) {
        this.cmgActive = cmgActive;
    }

    public byte[] getCmgLogo() {
        return cmgLogo;
    }

    public void setCmgLogo(byte[] cmgLogo) {
        this.cmgLogo = cmgLogo;
    }


}
