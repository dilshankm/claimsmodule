package com.infoinstech.claimsmodule.domain.model.common;

import javax.persistence.*;
import java.util.Date;

@Entity
@Table(name = "SM_T_SEND_SMS")

@NamedStoredProcedureQueries(

        {

                @NamedStoredProcedureQuery(
                        name = "PU_SEND_SMS", procedureName = "pk_infoins_sms_generation.PU_INSERT_SMS_CLTASK",
                        parameters = {
                                @StoredProcedureParameter(mode = ParameterMode.IN, name = "PaTaskCode", type = String.class),
                                @StoredProcedureParameter(mode = ParameterMode.IN, name = "PaClaimNo", type = String.class),
                                @StoredProcedureParameter(mode = ParameterMode.IN, name = "PaTPName", type = String.class),
                                @StoredProcedureParameter(mode = ParameterMode.IN, name = "PaTPRskName", type = String.class),
                                @StoredProcedureParameter(mode = ParameterMode.IN, name = "PaAasCode", type = String.class),
                                @StoredProcedureParameter(mode = ParameterMode.IN, name = "PaRepairer", type = String.class),
                                @StoredProcedureParameter(mode = ParameterMode.IN, name = "PaChqNo", type = String.class),
                                @StoredProcedureParameter(mode = ParameterMode.IN, name = "PaChqAmt", type = Double.class),
                                @StoredProcedureParameter(mode = ParameterMode.IN, name = "PaPrdCode", type = String.class)
                        }),


                @NamedStoredProcedureQuery(
                        name = "PU_SEND_EMAIL", procedureName = "pk_infoins_mails_generation.PU_GEN_CLTASK_EMAIL",
                        parameters = {
                                @StoredProcedureParameter(mode = ParameterMode.IN, name = "PaTaskCode", type = String.class),
                                @StoredProcedureParameter(mode = ParameterMode.IN, name = "PaClaimNo", type = String.class),
                                @StoredProcedureParameter(mode = ParameterMode.IN, name = "PaTPName", type = String.class),
                                @StoredProcedureParameter(mode = ParameterMode.IN, name = "PaTPRskName", type = String.class),
                                @StoredProcedureParameter(mode = ParameterMode.IN, name = "PaAasCode", type = String.class),
                                @StoredProcedureParameter(mode = ParameterMode.IN, name = "PaRepairer", type = String.class),
                                @StoredProcedureParameter(mode = ParameterMode.IN, name = "PaChqNo", type = String.class),
                                @StoredProcedureParameter(mode = ParameterMode.IN, name = "PaChqAmt", type = Double.class),
                                @StoredProcedureParameter(mode = ParameterMode.IN, name = "PaPrdCode", type = String.class)

                        })
        }
)


public class SendSMS {

    @Column(name = "SSS_SOURCE_ID")
    private String sourceId;

    @Column(name = "SSS_RECEIPIENT")
    private String recipientNo;

    @Column(name = "SSS_APPLICATION_ID")
    private String applicationId;

    @Column(name = "SSS_MESSAGE")
    private String message;

    @Column(name = "SSS_REQUESTED_DATE")
    private Date requestedDate;

    @Column(name = "SSS_IS_UPLOADED")
    private Character isUploaded;

    @Column(name = "SSS_UPLOADED_DATE")
    private Date uploadedDate;

    @Column(name = "SSS_IS_SENT")
    private Character isSent;

    @Column(name = "SSS_SENT_DATE")
    private Date sentDate;

    @Column(name = "CREATED_BY")
    private String createdBy;

    @Column(name = "CREATED_DATE")
    private Date createdDate;

    @Column(name = "MODIFIED_BY")
    private String modifiedBy;

    @Column(name = "MODIFIED_DATE")
    private Date modifiedDate;

    @Column(name = "SSS_POLICY_NO")
    private String policyNo;

    @Column(name = "SSS_CLAIM_NO")
    private String claimNo;

    @Column(name = "SSS_CUS_CODE")
    private String customerCode;

    @Column(name = "SSS_BSS_CODE")
    private String businessPartyCode;

    @Column(name = "SSS_ME_CODE")
    private String marketingExecutiveCode;

    @Column(name = "SSS_RECPT_TYPE")
    private String recipientType;

    @Column(name = "SSS_MODULE")
    private String moduleName;

    @Column(name = "SSS_FUNC_ID")
    private String functionId;

    @Id
    @Column(name = "SSS_SEQ_NO")
    private String sequenceNo;

    @Column(name = "SSS_SENDER")
    private String sender;

    @Column(name = "SSS_EXT_CODE")
    private String externalPersonCode;

    SendSMS(String sequenceNo, String recipientNo, String message, String externalPersonCode) {

        this.sequenceNo = sequenceNo;
        this.recipientNo = recipientNo;
        this.message = message;
        this.externalPersonCode = externalPersonCode;
        sourceId = "ZMS";
        applicationId = "APP001";
        moduleName = "CL";
        isUploaded = 'N';
        functionId = "CALL CENTER";
        requestedDate = new Date();

    }

    public String getSourceId() {
        return sourceId;
    }

    public void setSourceId(String sourceId) {
        this.sourceId = sourceId;
    }

    public String getRecipientNo() {
        return recipientNo;
    }

    public void setRecipientNo(String recipientNo) {
        this.recipientNo = recipientNo;
    }

    public String getApplicationId() {
        return applicationId;
    }

    public void setApplicationId(String applicationId) {
        this.applicationId = applicationId;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public Date getRequestedDate() {
        return requestedDate;
    }

    public void setRequestedDate(Date requestedDate) {
        this.requestedDate = requestedDate;
    }

    public Character getIsUploaded() {
        return isUploaded;
    }

    public void setIsUploaded(Character isUploaded) {
        this.isUploaded = isUploaded;
    }

    public Date getUploadedDate() {
        return uploadedDate;
    }

    public void setUploadedDate(Date uploadedDate) {
        this.uploadedDate = uploadedDate;
    }

    public Character getIsSent() {
        return isSent;
    }

    public void setIsSent(Character isSent) {
        this.isSent = isSent;
    }

    public Date getSentDate() {
        return sentDate;
    }

    public void setSentDate(Date sentDate) {
        this.sentDate = sentDate;
    }

    public String getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(String createdBy) {
        this.createdBy = createdBy;
    }

    public Date getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(Date createdDate) {
        this.createdDate = createdDate;
    }

    public String getModifiedBy() {
        return modifiedBy;
    }

    public void setModifiedBy(String modifiedBy) {
        this.modifiedBy = modifiedBy;
    }

    public Date getModifiedDate() {
        return modifiedDate;
    }

    public void setModifiedDate(Date modifiedDate) {
        this.modifiedDate = modifiedDate;
    }

    public String getPolicyNo() {
        return policyNo;
    }

    public void setPolicyNo(String policyNo) {
        this.policyNo = policyNo;
    }

    public String getClaimNo() {
        return claimNo;
    }

    public void setClaimNo(String claimNo) {
        this.claimNo = claimNo;
    }

    public String getCustomerCode() {
        return customerCode;
    }

    public void setCustomerCode(String customerCode) {
        this.customerCode = customerCode;
    }

    public String getBusinessPartyCode() {
        return businessPartyCode;
    }

    public void setBusinessPartyCode(String businessPartyCode) {
        this.businessPartyCode = businessPartyCode;
    }

    public String getMarketingExecutiveCode() {
        return marketingExecutiveCode;
    }

    public void setMarketingExecutiveCode(String marketingExecutiveCode) {
        this.marketingExecutiveCode = marketingExecutiveCode;
    }

    public String getRecipientType() {
        return recipientType;
    }

    public void setRecipientType(String recipientType) {
        this.recipientType = recipientType;
    }

    public String getModuleName() {
        return moduleName;
    }

    public void setModuleName(String moduleName) {
        this.moduleName = moduleName;
    }

    public String getFunctionId() {
        return functionId;
    }

    public void setFunctionId(String functionId) {
        this.functionId = functionId;
    }

    public String getSequenceNo() {
        return sequenceNo;
    }

    public void setSequenceNo(String sequenceNo) {
        this.sequenceNo = sequenceNo;
    }

    public String getSender() {
        return sender;
    }

    public void setSender(String sender) {
        this.sender = sender;
    }

    public String getExternalPersonCode() {
        return externalPersonCode;
    }

    public void setExternalPersonCode(String externalPersonCode) {
        this.externalPersonCode = externalPersonCode;
    }
}

