package com.infoinstech.claimsmodule.domain.model.common.reference;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

@Entity
@Table(name = "CM_R_REFERENCE_TWO")
public class CommonReference {

    @Id
    @Column(name = "RFT_SEQ_NO")
    private String sequenceNo;

    @NotNull
    @Column(name = "RFT_TYPE")
    private String type;

    @Column(name = "RFT_CODE")
    private String code;

    @Column(name = "RFT_DESCRIPTION")
    private String description;

    public String getSequenceNo() {
        return sequenceNo;
    }

    public void setSequenceNo(String sequenceNo) {
        this.sequenceNo = sequenceNo;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }
}
