package com.infoinstech.claimsmodule.domain.model.common.reference;

import lombok.Data;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

@Data
@Entity
@Table(name = "CM_R_REF_TWO_TYPES")
public class CommonReferenceType {

    @Id
    @Column(name = "RTT_TYPE")
    private String type;

    @Column(name = "RTT_SEQ_NO")
    private String sequenceNo;

    @NotNull
    @Column(name = "RTT_DESCRIPTION")
    private String description;

    @Column(name = "RTT_TABLE_NAME")
    private String tableName;

    @Column(name = "RTT_CODE_LENGTH")
    private Integer codeLength;

    @Column(name = "RTT_DESC_LENGTH")
    private Integer descriptionLength;

    @Column(name = "RTT_CODE_NAME")
    private String codeName;

    @Column(name = "RTT_DESC_NAME")
    private String descriptionName;

    @NotNull
    @Column(name = "RTT_MODULE")
    private String module;
}
