package com.infoinstech.claimsmodule.domain.model.payments;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import java.util.Date;

@Entity
@Table(name = "PY_R_TRAN_SUB_TYPE")
public class ProvisionType {

    @Id
    @Column(name = "TST_SUB_TYPE_CODE")
    private String code;

    @NotNull
    @Column(name = "TST_SUB_TYPE_DESC")
    private String description;

    @Column(name = "TST_GL_CODE")
    private String glCode;

    @Column(name = "TST_CREATED_USER")
    private String createdUser;

    @Column(name = "TST_CREATED_DATE")
    private Date createDate;

    @Column(name = "TST_MODIFIED_USER")
    private String modifiedUser;

    @Column(name = "TST_MODIFIED_DATE")
    private Date modifiedDate;

    @Column(name = "TST_FIN_MAP_CODE")
    private String mapCode;

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getGlCode() {
        return glCode;
    }

    public void setGlCode(String glCode) {
        this.glCode = glCode;
    }

    public String getCreatedUser() {
        return createdUser;
    }

    public void setCreatedUser(String createdUser) {
        this.createdUser = createdUser;
    }

    public Date getCreateDate() {
        return createDate;
    }

    public void setCreateDate(Date createDate) {
        this.createDate = createDate;
    }

    public String getModifiedUser() {
        return modifiedUser;
    }

    public void setModifiedUser(String modifiedUser) {
        this.modifiedUser = modifiedUser;
    }

    public Date getModifiedDate() {
        return modifiedDate;
    }

    public void setModifiedDate(Date modifiedDate) {
        this.modifiedDate = modifiedDate;
    }

    public String getMapCode() {
        return mapCode;
    }

    public void setMapCode(String mapCode) {
        this.mapCode = mapCode;
    }
}
