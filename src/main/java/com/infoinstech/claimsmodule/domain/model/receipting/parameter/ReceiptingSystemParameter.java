package com.infoinstech.claimsmodule.domain.model.receipting.parameter;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.util.Date;

@Entity
@Table(name = "RC_P_SYS_PARAMS  ")
public class ReceiptingSystemParameter {

    @Id
    @Column(name = "PMS_INDEX_NO")
    private String indexNo;

    @Column(name = "PMS_DESCRIPTION")
    private String description;

    @Column(name = "PMS_CHAR_VALUE")
    private String characterValue;

    @Column(name = "PMS_NUMBER_VALUE")
    private Double numberValue;

    @Column(name = "PMS_DATE_VALUE")
    private Date dateValue;

    @Column(name = "CREATED_BY")
    private String createdBy;

    @Column(name = "CREATED_DATE")
    private Date createdDate;

    @Column(name = "MODIFIED_BY")
    private String modifiedBy;

    @Column(name = "MODIFIED_DATE")
    private Date modifiedDate;
/*
    @Column(name="PMS_DATA_TYPE")
    private Character dataType;

    @Column(name="PMS_ALLOW_CHANGE")
    private Character allowChange;

    @Column(name="PMS_PARAM_GROUP")
    private Character parameterGroup;

    @Column(name="PMS_DEPENDENT")
    private Character dependant;

    @Column(name="PMS_REFER_TABLE")
    private String referenceTable;

    @Column(name="PMS_REFER_COLUMN")
    private String referenceColumn;

    @Column(name="PMS_Y_N")
    private Character yOrNo;

    @Column(name="PMS_YES_NO")
    private Character yesOrNo;

    @Column(name="PMS_LOV_NAME")
    private String lovName;

    @Column(name="PMS_ADMIN_ONLY")
    private Character adminOnly;*/

    public String getIndexNo() {
        return indexNo;
    }

    public void setIndexNo(String indexNo) {
        this.indexNo = indexNo;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getCharacterValue() {
        return characterValue;
    }

    public void setCharacterValue(String characterValue) {
        this.characterValue = characterValue;
    }

    public Double getNumberValue() {
        return numberValue;
    }

    public void setNumberValue(Double numberValue) {
        this.numberValue = numberValue;
    }

    public Date getDateValue() {
        return dateValue;
    }

    public void setDateValue(Date dateValue) {
        this.dateValue = dateValue;
    }

    public String getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(String createdBy) {
        this.createdBy = createdBy;
    }

    public Date getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(Date createdDate) {
        this.createdDate = createdDate;
    }

    public String getModifiedBy() {
        return modifiedBy;
    }

    public void setModifiedBy(String modifiedBy) {
        this.modifiedBy = modifiedBy;
    }

    public Date getModifiedDate() {
        return modifiedDate;
    }

    public void setModifiedDate(Date modifiedDate) {
        this.modifiedDate = modifiedDate;
    }
/*
    public Character getDataType() {
        return dataType;
    }

    public void setDataType(Character dataType) {
        this.dataType = dataType;
    }

    public Character getAllowChange() {
        return allowChange;
    }

    public void setAllowChange(Character allowChange) {
        this.allowChange = allowChange;
    }

    public Character getParameterGroup() {
        return parameterGroup;
    }

    public void setParameterGroup(Character parameterGroup) {
        this.parameterGroup = parameterGroup;
    }

    public Character getDependant() {
        return dependant;
    }

    public void setDependant(Character dependant) {
        this.dependant = dependant;
    }

    public String getReferenceTable() {
        return referenceTable;
    }

    public void setReferenceTable(String referenceTable) {
        this.referenceTable = referenceTable;
    }

    public String getReferenceColumn() {
        return referenceColumn;
    }

    public void setReferenceColumn(String referenceColumn) {
        this.referenceColumn = referenceColumn;
    }

    public Character getyOrNo() {
        return yOrNo;
    }

    public void setyOrNo(Character yOrNo) {
        this.yOrNo = yOrNo;
    }

    public Character getYesOrNo() {
        return yesOrNo;
    }

    public void setYesOrNo(Character yesOrNo) {
        this.yesOrNo = yesOrNo;
    }

    public String getLovName() {
        return lovName;
    }

    public void setLovName(String lovName) {
        this.lovName = lovName;
    }

    public Character getAdminOnly() {
        return adminOnly;
    }

    public void setAdminOnly(Character adminOnly) {
        this.adminOnly = adminOnly;
    }*/
}
