package com.infoinstech.claimsmodule.domain.model.receipting.transaction;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.Date;

@Entity
@Table(name = "RC_T_CREDIT_NOTE")
public class CreditNote {

    @Id
    @Column(name = "CRN_SEQ_NO")
    private String creditNoteSeqNo;

    @Column(name = "CRN_PRE_PRINT_NO")
    private String prePrintedNo;

    @Column(name = "CRN_CREDIT_NOTE_NO")
    private String creditNoteNo;

    @Column(name = "CRN_VOUCHER_NO")
    private String voucherNo;

    @Column(name = "CRN_TIT_CODE")
    private String customerTitleCode;

    @NotNull
    @Column(name = "CRN_CUS_FULL_NAME")
    private String customerFullName;


    @Column(name = "CRN_CUS_ADDR_LINE_1")
    private String customerAddressLine1;

    @Column(name = "CRN_CUS_ADDR_LINE_2")
    private String customerAddressLine2;

    @Column(name = "CRN_CUS_ADDR_LINE_3")
    private String customerAddressLine3;

    @Column(name = "CRN_POL_POLICY_NO")
    private String policyNo;

    @Column(name = "CRN_END_ENDORSEMENT_NO")
    private String endorsementNo;

    @Column(name = "CRN_SUM_INSURED")
    private BigDecimal sumInsured;

    @NotNull
    @Column(name = "CRN_CUR_CODE")
    private String currencyCode;

    @NotNull
    @Column(name = "CRN_TOTAL_AMOUNT")
    private BigDecimal totalAmount;

    @NotNull
    @Column(name = "CRN_TOTAL_NPHY_REF")
    private BigDecimal nonPhysicalAmount;

    @NotNull
    @Column(name = "CRN_TOTAL_PHY_REF")
    private BigDecimal physicalAmount;

    @NotNull
    @Column(name = "CRN_COPY_NO")
    private BigInteger noOfCopies;

    @NotNull
    @Column(name = "CRN_STATUS")
    private String status;

    @Column(name = "CRN_CANCEL_REASON")
    private String cancelledReason;

    @Column(name = "CRN_CAN_USER")
    private String cancelledUser;

    @Column(name = "CRN_CAN_DATE")
    private Date cancelledDate;

    @NotNull
    @Column(name = "CRN_CLA_CODE")
    private String classCode;

    @NotNull
    @Column(name = "CRN_PRD_CODE")
    private String productCode;

    @NotNull
    @Column(name = "CRN_CUS_CODE")
    private String customerCode;

    @NotNull
    @Column(name = "CRN_RFR_CODE")
    private String refundReasonCode;

    @NotNull
    @Column(name = "CRN_TRN_DATE")
    private Date transactionDate;

    @Column(name = "CREATED_BY")
    private String createdBy;

    @Column(name = "CREATED_DATE")
    private Date createdDate;

    @Column(name = "MODIFIED_BY")
    private String modifiedBy;

    @Column(name = "MODIFIED_DATE")
    private Date modifiedDate;

    @NotNull
    @Column(name = "CRN_BRN_CODE")
    private String branchCode;

    @NotNull
    @Column(name = "CRN_OUTSTAND_POL")
    private BigDecimal policyOutstandingAmount;

    @NotNull
    @Column(name = "CRN_OUTSTAND_CUS")
    private BigDecimal customerOutstandingAmount;

    @NotNull
    @Column(name = "CRN_POL_SEQ_NO")
    private String policySeqNo;

    @NotNull
    @Column(name = "CRN_TRAN_TYPE")
    private String policyTransactionType;

    @Column(name = "CRN_PREPARED_BY")
    private String preparedBy;

    @Column(name = "CRN_CHECKED_BY")
    private String checkedBy;

    @Column(name = "CRN_ME_CODE")
    private String marketingExecutiveCode;

    @Column(name = "CRN_AGENT_CODE")
    private String agentCode;

    @Column(name = "CRN_REF_DATE")
    private Date referencedDate;

    @Column(name = "CRN_SALES_TYPE")
    private String salesType;

    @Column(name = "CRN_PAYEE_NAME")
    private String payeeName;

    @Column(name = "CRN_PST_PRIORITY_CODE")
    private String priorityCode;

    @Column(name = "CRN_SIG_SEQ_NO")
    private String signOnOffSeqNo;

    @NotNull
    @Column(name = "CRN_POL_PERIOD_FROM")
    private Date policyPeriodFrom;

    @NotNull
    @Column(name = "CRN_POL_PERIOD_TO")
    private Date policyPeriodTo;

    @Column(name = "CRN_PREV_SUM_INSURED")
    private BigDecimal previousSumInsured;

    @Column(name = "CRN_EFFECT_DATE")
    private Date effectiveDate;

    @Column(name = "CRN_COMM_AMMENDED")
    private Character commissionAmmended;

    @Column(name = "CRN_REL_POLICY_NO")
    private String relatedPolicyNo;

    @Column(name = "CRN_ADDR_SEQ_NO")
    private String addressSeqNo;

    @Column(name = "CRN_GL_ENTRY_NO")
    private String glEntryNo;

    @Column(name = "CRN_STTL_CHQ_AMOUNT")
    private BigDecimal settledChequeAmount;

    @Column(name = "CRN_UPR_PROCESSED")
    private Character uprProcessed;

    @Column(name = "CRN_PROCESS_BRN_CODE")
    private String creditNoteBranchCode;

    @Column(name = "CRN_POL_BSS_CODE")
    private String policyBranchCode;

    @Column(name = "CRN_REFUND_REASON")
    private String refundReason;

    @Column(name = "CRN_UPR_CAN_PROCESSED")
    private Character uprCanProcessed;

    public String getCreditNoteSeqNo() {
        return creditNoteSeqNo;
    }

    public void setCreditNoteSeqNo(String creditNoteSeqNo) {
        this.creditNoteSeqNo = creditNoteSeqNo;
    }

    public String getPrePrintedNo() {
        return prePrintedNo;
    }

    public void setPrePrintedNo(String prePrintedNo) {
        this.prePrintedNo = prePrintedNo;
    }

    public String getCreditNoteNo() {
        return creditNoteNo;
    }

    public void setCreditNoteNo(String creditNoteNo) {
        this.creditNoteNo = creditNoteNo;
    }

    public String getVoucherNo() {
        return voucherNo;
    }

    public void setVoucherNo(String voucherNo) {
        this.voucherNo = voucherNo;
    }

    public String getCustomerTitleCode() {
        return customerTitleCode;
    }

    public void setCustomerTitleCode(String customerTitleCode) {
        this.customerTitleCode = customerTitleCode;
    }

    public String getCustomerFullName() {
        return customerFullName;
    }

    public void setCustomerFullName(String customerFullName) {
        this.customerFullName = customerFullName;
    }

    public String getCustomerAddressLine1() {
        return customerAddressLine1;
    }

    public void setCustomerAddressLine1(String customerAddressLine1) {
        this.customerAddressLine1 = customerAddressLine1;
    }

    public String getCustomerAddressLine2() {
        return customerAddressLine2;
    }

    public void setCustomerAddressLine2(String customerAddressLine2) {
        this.customerAddressLine2 = customerAddressLine2;
    }

    public String getCustomerAddressLine3() {
        return customerAddressLine3;
    }

    public void setCustomerAddressLine3(String customerAddressLine3) {
        this.customerAddressLine3 = customerAddressLine3;
    }

    public String getPolicyNo() {
        return policyNo;
    }

    public void setPolicyNo(String policyNo) {
        this.policyNo = policyNo;
    }

    public String getEndorsementNo() {
        return endorsementNo;
    }

    public void setEndorsementNo(String endorsementNo) {
        this.endorsementNo = endorsementNo;
    }

    public BigDecimal getSumInsured() {
        return sumInsured;
    }

    public void setSumInsured(BigDecimal sumInsured) {
        this.sumInsured = sumInsured;
    }

    public String getCurrencyCode() {
        return currencyCode;
    }

    public void setCurrencyCode(String currencyCode) {
        this.currencyCode = currencyCode;
    }

    public BigDecimal getTotalAmount() {
        return totalAmount;
    }

    public void setTotalAmount(BigDecimal totalAmount) {
        this.totalAmount = totalAmount;
    }

    public BigDecimal getNonPhysicalAmount() {
        return nonPhysicalAmount;
    }

    public void setNonPhysicalAmount(BigDecimal nonPhysicalAmount) {
        this.nonPhysicalAmount = nonPhysicalAmount;
    }

    public BigDecimal getPhysicalAmount() {
        return physicalAmount;
    }

    public void setPhysicalAmount(BigDecimal physicalAmount) {
        this.physicalAmount = physicalAmount;
    }

    public BigInteger getNoOfCopies() {
        return noOfCopies;
    }

    public void setNoOfCopies(BigInteger noOfCopies) {
        this.noOfCopies = noOfCopies;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getCancelledReason() {
        return cancelledReason;
    }

    public void setCancelledReason(String cancelledReason) {
        this.cancelledReason = cancelledReason;
    }

    public String getCancelledUser() {
        return cancelledUser;
    }

    public void setCancelledUser(String cancelledUser) {
        this.cancelledUser = cancelledUser;
    }

    public Date getCancelledDate() {
        return cancelledDate;
    }

    public void setCancelledDate(Date cancelledDate) {
        this.cancelledDate = cancelledDate;
    }

    public String getClassCode() {
        return classCode;
    }

    public void setClassCode(String classCode) {
        this.classCode = classCode;
    }

    public String getProductCode() {
        return productCode;
    }

    public void setProductCode(String productCode) {
        this.productCode = productCode;
    }

    public String getCustomerCode() {
        return customerCode;
    }

    public void setCustomerCode(String customerCode) {
        this.customerCode = customerCode;
    }

    public String getRefundReasonCode() {
        return refundReasonCode;
    }

    public void setRefundReasonCode(String refundReasonCode) {
        this.refundReasonCode = refundReasonCode;
    }

    public Date getTransactionDate() {
        return transactionDate;
    }

    public void setTransactionDate(Date transactionDate) {
        this.transactionDate = transactionDate;
    }

    public String getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(String createdBy) {
        this.createdBy = createdBy;
    }

    public Date getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(Date createdDate) {
        this.createdDate = createdDate;
    }

    public String getModifiedBy() {
        return modifiedBy;
    }

    public void setModifiedBy(String modifiedBy) {
        this.modifiedBy = modifiedBy;
    }

    public Date getModifiedDate() {
        return modifiedDate;
    }

    public void setModifiedDate(Date modifiedDate) {
        this.modifiedDate = modifiedDate;
    }

    public String getBranchCode() {
        return branchCode;
    }

    public void setBranchCode(String branchCode) {
        this.branchCode = branchCode;
    }

    public BigDecimal getPolicyOutstandingAmount() {
        return policyOutstandingAmount;
    }

    public void setPolicyOutstandingAmount(BigDecimal policyOutstandingAmount) {
        this.policyOutstandingAmount = policyOutstandingAmount;
    }

    public BigDecimal getCustomerOutstandingAmount() {
        return customerOutstandingAmount;
    }

    public void setCustomerOutstandingAmount(BigDecimal customerOutstandingAmount) {
        this.customerOutstandingAmount = customerOutstandingAmount;
    }

    public String getPolicySeqNo() {
        return policySeqNo;
    }

    public void setPolicySeqNo(String policySeqNo) {
        this.policySeqNo = policySeqNo;
    }

    public String getPolicyTransactionType() {
        return policyTransactionType;
    }

    public void setPolicyTransactionType(String policyTransactionType) {
        this.policyTransactionType = policyTransactionType;
    }

    public String getPreparedBy() {
        return preparedBy;
    }

    public void setPreparedBy(String preparedBy) {
        this.preparedBy = preparedBy;
    }

    public String getCheckedBy() {
        return checkedBy;
    }

    public void setCheckedBy(String checkedBy) {
        this.checkedBy = checkedBy;
    }

    public String getMarketingExecutiveCode() {
        return marketingExecutiveCode;
    }

    public void setMarketingExecutiveCode(String marketingExecutiveCode) {
        this.marketingExecutiveCode = marketingExecutiveCode;
    }

    public String getAgentCode() {
        return agentCode;
    }

    public void setAgentCode(String agentCode) {
        this.agentCode = agentCode;
    }

    public Date getReferencedDate() {
        return referencedDate;
    }

    public void setReferencedDate(Date referencedDate) {
        this.referencedDate = referencedDate;
    }

    public String getSalesType() {
        return salesType;
    }

    public void setSalesType(String salesType) {
        this.salesType = salesType;
    }

    public String getPayeeName() {
        return payeeName;
    }

    public void setPayeeName(String payeeName) {
        this.payeeName = payeeName;
    }

    public String getPriorityCode() {
        return priorityCode;
    }

    public void setPriorityCode(String priorityCode) {
        this.priorityCode = priorityCode;
    }

    public String getSignOnOffSeqNo() {
        return signOnOffSeqNo;
    }

    public void setSignOnOffSeqNo(String signOnOffSeqNo) {
        this.signOnOffSeqNo = signOnOffSeqNo;
    }

    public Date getPolicyPeriodFrom() {
        return policyPeriodFrom;
    }

    public void setPolicyPeriodFrom(Date policyPeriodFrom) {
        this.policyPeriodFrom = policyPeriodFrom;
    }

    public Date getPolicyPeriodTo() {
        return policyPeriodTo;
    }

    public void setPolicyPeriodTo(Date policyPeriodTo) {
        this.policyPeriodTo = policyPeriodTo;
    }

    public BigDecimal getPreviousSumInsured() {
        return previousSumInsured;
    }

    public void setPreviousSumInsured(BigDecimal previousSumInsured) {
        this.previousSumInsured = previousSumInsured;
    }

    public Date getEffectiveDate() {
        return effectiveDate;
    }

    public void setEffectiveDate(Date effectiveDate) {
        this.effectiveDate = effectiveDate;
    }

    public Character getCommissionAmmended() {
        return commissionAmmended;
    }

    public void setCommissionAmmended(Character commissionAmmended) {
        this.commissionAmmended = commissionAmmended;
    }

    public String getRelatedPolicyNo() {
        return relatedPolicyNo;
    }

    public void setRelatedPolicyNo(String relatedPolicyNo) {
        this.relatedPolicyNo = relatedPolicyNo;
    }

    public String getAddressSeqNo() {
        return addressSeqNo;
    }

    public void setAddressSeqNo(String addressSeqNo) {
        this.addressSeqNo = addressSeqNo;
    }

    public String getGlEntryNo() {
        return glEntryNo;
    }

    public void setGlEntryNo(String glEntryNo) {
        this.glEntryNo = glEntryNo;
    }

    public BigDecimal getSettledChequeAmount() {
        return settledChequeAmount;
    }

    public void setSettledChequeAmount(BigDecimal settledChequeAmount) {
        this.settledChequeAmount = settledChequeAmount;
    }

    public Character getUprProcessed() {
        return uprProcessed;
    }

    public void setUprProcessed(Character uprProcessed) {
        this.uprProcessed = uprProcessed;
    }

    public String getCreditNoteBranchCode() {
        return creditNoteBranchCode;
    }

    public void setCreditNoteBranchCode(String creditNoteBranchCode) {
        this.creditNoteBranchCode = creditNoteBranchCode;
    }

    public String getPolicyBranchCode() {
        return policyBranchCode;
    }

    public void setPolicyBranchCode(String policyBranchCode) {
        this.policyBranchCode = policyBranchCode;
    }

    public String getRefundReason() {
        return refundReason;
    }

    public void setRefundReason(String refundReason) {
        this.refundReason = refundReason;
    }

    public Character getUprCanProcessed() {
        return uprCanProcessed;
    }

    public void setUprCanProcessed(Character uprCanProcessed) {
        this.uprCanProcessed = uprCanProcessed;
    }
}

