package com.infoinstech.claimsmodule.domain.model.receipting.transaction;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.math.BigDecimal;
import java.util.Date;

@Entity
@Table(name = "RC_T_DEBIT_NOTE")
public class DebitNote {
    @Id
    @Column(name = "DEB_SEQ_NO")
    private String sequenceNo;

    @Column(name = "DEB_PRE_PRINT_NO")
    private String prePrintedNo;

    @Column(name = "DEB_DEB_NOTE_NO")
    private String debitNoteNo;

    @Column(name = "DEB_DEB_DATE")
    private Date date;

    @Column(name = "DEB_TIT_CODE")
    private String titleCode;

    @Column(name = "DEB_CUS_FULL_NAME")
    private String customerFullName;

    @Column(name = "DEB_CUS_ADDR_LINE_1")
    private String customerAddressLine1;

    @Column(name = "DEB_CUS_ADDR_LINE_2")
    private String customerAddressLine2;

    @Column(name = "DEB_CUS_ADDR_LINE_3")
    private String customerAddressLine3;

    @Column(name = "DEB_SUM_INSURED")
    private Double sumInsured;

    @Column(name = "DEB_CUR_CODE")
    private String currencyCode;

    @Column(name = "DEB_TOTAL_AMOUNT")
    private Double totalAmount;

    @Column(name = "DEB_TOTAL_PAID")
    private Double totalPaid;

    @Column(name = "DEB_SETTLED")
    private Character debitSettled;

    @Column(name = "DEB_TOTAL_SETTLED")
    private Double totalSettled;

    @Column(name = "DEB_COPY_NO")
    private int copyNo;

    @Column(name = "DEB_TRT_CODE")
    private String treatyCode;

    @Column(name = "DEB_CLA_CODE")
    private String classCode;

    @Column(name = "DEB_PRD_CODE")
    private String productCode;

    @Column(name = "DEB_CUS_CODE")
    private String customerCode;

    @Column(name = "DEB_TRN_DATE")
    private Date transactionDate;

    @Column(name = "CREATED_BY")
    private String createdBy;

    @Column(name = "CREATED_DATE")
    private Date createdDate;

    @Column(name = "MODIFIED_BY")
    private String modifiedBy;

    @Column(name = "MODIFIED_DATE")
    private Date modifiedDate;

    @Column(name = "DEB_BRN_CODE")
    private String branchCode;

    @Column(name = "DEB_VOUCHER_NO")
    private String voucherNo;

    @Column(name = "DEB_POLICY_NO")
    private String policyNo;

    @Column(name = "DEB_ENDORSEMENT_NO")
    private String endorsementNo;

    @Column(name = "DEB_OUTSTAND_POL")
    private Double outstandingPolicyAmount;

    @Column(name = "DEB_OUTSTAND_CUS")
    private Double customerOutstanding;

    @Column(name = "DEB_POL_SEQ_NO")
    private String policySequenceNo;

    @Column(name = "DEB_TRAN_TYPE")
    private String transactionType;

    @Column(name = "DEB_PREPARED_BY")
    private String preparedBy;

    @Column(name = "DEB_CHECKED_BY")
    private String checkedBy;

    @Column(name = "DEB_ME_CODE")
    private String intermediaryCode;

    @Column(name = "DEB_AGENT_CODE")
    private String agentCode;

    @Column(name = "DEB_SALES_TYPE")
    private String salesType;

    @Column(name = "DEB_REF_DATE")
    private Date referenceDate;

    @Column(name = "DEB_AUTH_REQ")
    private Character authorizationRequired;

    @Column(name = "DEB_DEB_DEBTOR_CODE")
    private String debtorCode;

    @Column(name = "DEB_DEB_PROFIT_CENTER")
    private String profitCenter;

    @Column(name = "DEB_SP_SRC_SEQ_NO")
    private String srcSequenceNo;

    @Column(name = "DEB_SIG_SEQ_NO")
    private String signatureSequenceNo;

    @Column(name = "DEB_POL_PERIOD_FROM")
    private Date polPeriodFrom;

    @Column(name = "DEB_POL_PERIOD_TO")
    private Date polPeriodTo;

    @Column(name = "DEB_STATUS")
    private Character status;

    @Column(name = "DEB_CANCEL_REASON")
    private String cancelledReason;

    @Column(name = "DEB_CAN_USER")
    private String cancelledUser;

    @Column(name = "DEB_CAN_DATE")
    private Date cancelledDate;

    @Column(name = "DEB_PREV_SUM_INSURED")
    private BigDecimal previousSumInsured;

    @Column(name = "DEB_EFFECT_DATE")
    private Date effectiveDate;

    @Column(name = "DEB_COMM_AMMENDED")
    private String commissionAmmended;

    @Column(name = "DEB_REL_POLICY_NO")
    private String relatedPolicyNo;

    @Column(name = "DEB_ADDR_SEQ_NO")
    private String addressSequenceNo;

    @Column(name = "DEB_GL_ENTRY_NO")
    private String glEntryNo;

    @Column(name = "DEB_ART_CODE")
    private String arrangementCode;

    @Column(name = "DEB_POL_PAYMENT_CAT")
    private String paymentCategory;

    @Column(name = "DEB_UPR_PROCESSED")
    private String uprProcessed;

    @Column(name = "DEB_PROCESS_BRN_CODE")
    private String proccessedBranchCode;

    @Column(name = "DEB_POL_BSS_CODE")
    private String bssCode;

    @Column(name = "DEB_MIGRATED")
    private Character migrated;

//    @Column(name = "DEB_BPARTY_CODE")
//    private String bPartyCode;

//    @Column(name = "DEB_PREAUTH_FLG")
//    private String preAuthflag;

    @Column(name = "DEB_UPR_CAN_PROCESSED")
    private Character uprCancellationProcessed;

//    @Column(name = "DEB_BPARTY_BSS_CODE")
//    private String bPartyBssCode;


//    @Column(name = "DEB_GL_REF_DATE")
//    private Date glRefDate;

//    @Column(name = "DEB_GL_REF_DATE_CAN")
//    private Date glRefDateCancelled;

//    @Column(name = "DEB_INVOICE_TYPE")
//    private String invoiceType;

//    @Column(name = "DEB_CUS_ADDR_FULL")
//    private String cusAddFull;

//    @Column(name = "DEB_TRAN_STATUS")
//    private String transactionStatus;

    //    @Column(name = "DEB_CAN_ENDORSEMENT_NO")
//    private String cancelledEndorseNo;


    public String getSequenceNo() {
        return sequenceNo;
    }

    public void setSequenceNo(String sequenceNo) {
        this.sequenceNo = sequenceNo;
    }

    public String getPrePrintedNo() {
        return prePrintedNo;
    }

    public void setPrePrintedNo(String prePrintedNo) {
        this.prePrintedNo = prePrintedNo;
    }

    public String getDebitNoteNo() {
        return debitNoteNo;
    }

    public void setDebitNoteNo(String debitNoteNo) {
        this.debitNoteNo = debitNoteNo;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public String getTitleCode() {
        return titleCode;
    }

    public void setTitleCode(String titleCode) {
        this.titleCode = titleCode;
    }

    public String getCustomerFullName() {
        return customerFullName;
    }

    public void setCustomerFullName(String customerFullName) {
        this.customerFullName = customerFullName;
    }

    public String getCustomerAddressLine1() {
        return customerAddressLine1;
    }

    public void setCustomerAddressLine1(String customerAddressLine1) {
        this.customerAddressLine1 = customerAddressLine1;
    }

    public String getCustomerAddressLine2() {
        return customerAddressLine2;
    }

    public void setCustomerAddressLine2(String customerAddressLine2) {
        this.customerAddressLine2 = customerAddressLine2;
    }

    public String getCustomerAddressLine3() {
        return customerAddressLine3;
    }

    public void setCustomerAddressLine3(String customerAddressLine3) {
        this.customerAddressLine3 = customerAddressLine3;
    }

    public Double getSumInsured() {
        return sumInsured;
    }

    public void setSumInsured(Double sumInsured) {
        this.sumInsured = sumInsured;
    }

    public String getCurrencyCode() {
        return currencyCode;
    }

    public void setCurrencyCode(String currencyCode) {
        this.currencyCode = currencyCode;
    }

    public Double getTotalAmount() {
        return totalAmount;
    }

    public void setTotalAmount(Double totalAmount) {
        this.totalAmount = totalAmount;
    }

    public Double getTotalPaid() {
        return totalPaid;
    }

    public void setTotalPaid(Double totalPaid) {
        this.totalPaid = totalPaid;
    }

    public Character getDebitSettled() {
        return debitSettled;
    }

    public void setDebitSettled(Character debitSettled) {
        this.debitSettled = debitSettled;
    }

    public Double getTotalSettled() {
        return totalSettled;
    }

    public void setTotalSettled(Double totalSettled) {
        this.totalSettled = totalSettled;
    }

    public int getCopyNo() {
        return copyNo;
    }

    public void setCopyNo(int copyNo) {
        this.copyNo = copyNo;
    }

    public String getTreatyCode() {
        return treatyCode;
    }

    public void setTreatyCode(String treatyCode) {
        this.treatyCode = treatyCode;
    }

    public String getClassCode() {
        return classCode;
    }

    public void setClassCode(String classCode) {
        this.classCode = classCode;
    }

    public String getProductCode() {
        return productCode;
    }

    public void setProductCode(String productCode) {
        this.productCode = productCode;
    }

    public String getCustomerCode() {
        return customerCode;
    }

    public void setCustomerCode(String customerCode) {
        this.customerCode = customerCode;
    }

    public Date getTransactionDate() {
        return transactionDate;
    }

    public void setTransactionDate(Date transactionDate) {
        this.transactionDate = transactionDate;
    }

    public String getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(String createdBy) {
        this.createdBy = createdBy;
    }

    public Date getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(Date createdDate) {
        this.createdDate = createdDate;
    }

    public String getModifiedBy() {
        return modifiedBy;
    }

    public void setModifiedBy(String modifiedBy) {
        this.modifiedBy = modifiedBy;
    }

    public Date getModifiedDate() {
        return modifiedDate;
    }

    public void setModifiedDate(Date modifiedDate) {
        this.modifiedDate = modifiedDate;
    }

    public String getBranchCode() {
        return branchCode;
    }

    public void setBranchCode(String branchCode) {
        this.branchCode = branchCode;
    }

    public String getVoucherNo() {
        return voucherNo;
    }

    public void setVoucherNo(String voucherNo) {
        this.voucherNo = voucherNo;
    }

    public String getPolicyNo() {
        return policyNo;
    }

    public void setPolicyNo(String policyNo) {
        this.policyNo = policyNo;
    }

    public String getEndorsementNo() {
        return endorsementNo;
    }

    public void setEndorsementNo(String endorsementNo) {
        this.endorsementNo = endorsementNo;
    }

    public Double getOutstandingPolicyAmount() {
        return outstandingPolicyAmount;
    }

    public void setOutstandingPolicyAmount(Double outstandingPolicyAmount) {
        this.outstandingPolicyAmount = outstandingPolicyAmount;
    }

    public Double getCustomerOutstanding() {
        return customerOutstanding;
    }

    public void setCustomerOutstanding(Double customerOutstanding) {
        this.customerOutstanding = customerOutstanding;
    }

    public String getPolicySequenceNo() {
        return policySequenceNo;
    }

    public void setPolicySequenceNo(String policySequenceNo) {
        this.policySequenceNo = policySequenceNo;
    }

    public String getTransactionType() {
        return transactionType;
    }

    public void setTransactionType(String transactionType) {
        this.transactionType = transactionType;
    }

    public String getPreparedBy() {
        return preparedBy;
    }

    public void setPreparedBy(String preparedBy) {
        this.preparedBy = preparedBy;
    }

    public String getCheckedBy() {
        return checkedBy;
    }

    public void setCheckedBy(String checkedBy) {
        this.checkedBy = checkedBy;
    }

    public String getIntermediaryCode() {
        return intermediaryCode;
    }

    public void setIntermediaryCode(String intermediaryCode) {
        this.intermediaryCode = intermediaryCode;
    }

    public String getAgentCode() {
        return agentCode;
    }

    public void setAgentCode(String agentCode) {
        this.agentCode = agentCode;
    }

    public String getSalesType() {
        return salesType;
    }

    public void setSalesType(String salesType) {
        this.salesType = salesType;
    }

    public Date getReferenceDate() {
        return referenceDate;
    }

    public void setReferenceDate(Date referenceDate) {
        this.referenceDate = referenceDate;
    }

    public Character getAuthorizationRequired() {
        return authorizationRequired;
    }

    public void setAuthorizationRequired(Character authorizationRequired) {
        this.authorizationRequired = authorizationRequired;
    }

    public String getDebtorCode() {
        return debtorCode;
    }

    public void setDebtorCode(String debtorCode) {
        this.debtorCode = debtorCode;
    }

    public String getProfitCenter() {
        return profitCenter;
    }

    public void setProfitCenter(String profitCenter) {
        this.profitCenter = profitCenter;
    }

    public String getSrcSequenceNo() {
        return srcSequenceNo;
    }

    public void setSrcSequenceNo(String srcSequenceNo) {
        this.srcSequenceNo = srcSequenceNo;
    }

    public String getSignatureSequenceNo() {
        return signatureSequenceNo;
    }

    public void setSignatureSequenceNo(String signatureSequenceNo) {
        this.signatureSequenceNo = signatureSequenceNo;
    }

    public Date getPolPeriodFrom() {
        return polPeriodFrom;
    }

    public void setPolPeriodFrom(Date polPeriodFrom) {
        this.polPeriodFrom = polPeriodFrom;
    }

    public Date getPolPeriodTo() {
        return polPeriodTo;
    }

    public void setPolPeriodTo(Date polPeriodTo) {
        this.polPeriodTo = polPeriodTo;
    }

    public Character getStatus() {
        return status;
    }

    public void setStatus(Character status) {
        this.status = status;
    }

    public String getCancelledReason() {
        return cancelledReason;
    }

    public void setCancelledReason(String cancelledReason) {
        this.cancelledReason = cancelledReason;
    }

    public String getCancelledUser() {
        return cancelledUser;
    }

    public void setCancelledUser(String cancelledUser) {
        this.cancelledUser = cancelledUser;
    }

    public Date getCancelledDate() {
        return cancelledDate;
    }

    public void setCancelledDate(Date cancelledDate) {
        this.cancelledDate = cancelledDate;
    }

    public BigDecimal getPreviousSumInsured() {
        return previousSumInsured;
    }

    public void setPreviousSumInsured(BigDecimal previousSumInsured) {
        this.previousSumInsured = previousSumInsured;
    }

    public Date getEffectiveDate() {
        return effectiveDate;
    }

    public void setEffectiveDate(Date effectiveDate) {
        this.effectiveDate = effectiveDate;
    }

    public String getCommissionAmmended() {
        return commissionAmmended;
    }

    public void setCommissionAmmended(String commissionAmmended) {
        this.commissionAmmended = commissionAmmended;
    }

    public String getRelatedPolicyNo() {
        return relatedPolicyNo;
    }

    public void setRelatedPolicyNo(String relatedPolicyNo) {
        this.relatedPolicyNo = relatedPolicyNo;
    }

    public String getAddressSequenceNo() {
        return addressSequenceNo;
    }

    public void setAddressSequenceNo(String addressSequenceNo) {
        this.addressSequenceNo = addressSequenceNo;
    }

    public String getGlEntryNo() {
        return glEntryNo;
    }

    public void setGlEntryNo(String glEntryNo) {
        this.glEntryNo = glEntryNo;
    }

    public String getArrangementCode() {
        return arrangementCode;
    }

    public void setArrangementCode(String arrangementCode) {
        this.arrangementCode = arrangementCode;
    }

    public String getPaymentCategory() {
        return paymentCategory;
    }

    public void setPaymentCategory(String paymentCategory) {
        this.paymentCategory = paymentCategory;
    }

    public String getUprProcessed() {
        return uprProcessed;
    }

    public void setUprProcessed(String uprProcessed) {
        this.uprProcessed = uprProcessed;
    }

    public String getProccessedBranchCode() {
        return proccessedBranchCode;
    }

    public void setProccessedBranchCode(String proccessedBranchCode) {
        this.proccessedBranchCode = proccessedBranchCode;
    }

    public String getBssCode() {
        return bssCode;
    }

    public void setBssCode(String bssCode) {
        this.bssCode = bssCode;
    }

    public Character getMigrated() {
        return migrated;
    }

    public void setMigrated(Character migrated) {
        this.migrated = migrated;
    }

    public Character getUprCancellationProcessed() {
        return uprCancellationProcessed;
    }

    public void setUprCancellationProcessed(Character uprCancellationProcessed) {
        this.uprCancellationProcessed = uprCancellationProcessed;
    }
}

