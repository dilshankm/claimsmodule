package com.infoinstech.claimsmodule.domain.model.receipting.transaction;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.io.Serializable;
import java.util.Date;

@Entity
@Table(name = "RC_T_DEBIT_SETTLE")
public class DebitSettlement implements Serializable {

    @Id
    @Column(name = "DST_SEQ_NO")
    private String id;

    @Column(name = "DST_PRE_PRINT_NO")
    private String prePrintedReceiptNo;

    @Column(name = "DST_DEB_SETTLE_NO")
    private String debitSettlementReceiptNo;

    @Column(name = "DST_SETTLED_AMOUNT")
    private Double SettledAmount;

    @Column(name = "DST_PAID_AMOUNT")
    private Double paidAmount;

    @Column(name = "DST_COPY_NO")
    private Integer receiptCopyNo;

    @Column(name = "DST_STATUS")
    private Character status;

    @Column(name = "DST_CANCEL_REASON")
    private String cancellationReason;

    @Column(name = "DST_CAN_USER")
    private String cancellationUser;

    @Column(name = "DST_CAN_DATE")
    private Date cancellationDate;

    @Column(name = "DST_TRN_DATE")
    private Date transactionDate;

    @Column(name = "DST_BRN_CODE") //(FK - SM_M_SALESLOC)
    private String branchCode;

    @Column(name = "DST_DEB_DEBTOR_CODE") // (FK - AC_M_DEBTOR)
    private String debtorCode;

    @Column(name = "DST_DEB_PROFIT_CENTER")
    private String debtorProfitCenter;

    @Column(name = "DST_EX_SRC_SEQ_NO") //(FK - RC_T_SUNDRY_RECEIPT)
    private String sundryReceiptId;

    @Column(name = "DST_AUTH_REQ")
    private Character requiredAuthorization;

    @Column(name = "DST_NEW_CONV_AMT")
    private Double newConversionRate;

    @Column(name = "DST_DEB_NOTE_NO")
    private String debitNoteNo;

    @Column(name = "DST_POLICY_NO")
    private String policyNo;

    @Column(name = "DST_CUS_CODE") //(FK - UW_M_CUSTOMERS)
    private String customerCode;

    @Column(name = "DST_SFC_CODE") //(FK - SM_M_SALES_FORCE)
    private String salesForceCode;

    @Column(name = "DST_SIG_SEQ_NO")
    private String signatureId;

    @Column(name = "DST_ADDR_SEQ_NO")
    private String addressId;

    @Column(name = "DST_CHG_STATUS")
    private Character changeStatus;

    @Column(name = "DST_PROCESS_BRN_CODE")
    private String transactionProcessedBranchCode;

    @Column(name = "DST_RISK_NAME")
    private String riskName;

    @Column(name = "DST_BPARTY_CODE")
    private String businessPartyCode;

    @Column(name = "DST_PAY_RESPONSIBLE")
    private Character paymentResponsiblePersonType;

    @Column(name = "DST_REMARKS")
    private String remarks;

    @Column(name = "DST_DISHON_DATE")
    private Date dishonoredChequeDate;

    @Column(name = "DST_DISHON_CHQ_SEQ_NO")
    private String dishonoredChequeId;

    /*Audit Fields*/
    @Column(name = "CREATED_BY")
    private String createdByUser;

    @Column(name = "CREATED_DATE")
    private Date createdDate;

    @Column(name = "MODIFIED_BY")
    private String modifiedByUser;

    @Column(name = "MODIFIED_DATE")
    private Date modifiedDate;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getPrePrintedReceiptNo() {
        return prePrintedReceiptNo;
    }

    public void setPrePrintedReceiptNo(String prePrintedReceiptNo) {
        this.prePrintedReceiptNo = prePrintedReceiptNo;
    }

    public String getDebitSettlementReceiptNo() {
        return debitSettlementReceiptNo;
    }

    public void setDebitSettlementReceiptNo(String debitSettlementReceiptNo) {
        this.debitSettlementReceiptNo = debitSettlementReceiptNo;
    }

    public Double getSettledAmount() {
        return SettledAmount;
    }

    public void setSettledAmount(Double settledAmount) {
        SettledAmount = settledAmount;
    }

    public Double getPaidAmount() {
        return paidAmount;
    }

    public void setPaidAmount(Double paidAmount) {
        this.paidAmount = paidAmount;
    }

    public Integer getReceiptCopyNo() {
        return receiptCopyNo;
    }

    public void setReceiptCopyNo(Integer receiptCopyNo) {
        this.receiptCopyNo = receiptCopyNo;
    }

    public Character getStatus() {
        return status;
    }

    public void setStatus(Character status) {
        this.status = status;
    }

    public String getCancellationReason() {
        return cancellationReason;
    }

    public void setCancellationReason(String cancellationReason) {
        this.cancellationReason = cancellationReason;
    }

    public String getCancellationUser() {
        return cancellationUser;
    }

    public void setCancellationUser(String cancellationUser) {
        this.cancellationUser = cancellationUser;
    }

    public Date getCancellationDate() {
        return cancellationDate;
    }

    public void setCancellationDate(Date cancellationDate) {
        this.cancellationDate = cancellationDate;
    }

    public Date getTransactionDate() {
        return transactionDate;
    }

    public void setTransactionDate(Date transactionDate) {
        this.transactionDate = transactionDate;
    }

    public String getBranchCode() {
        return branchCode;
    }

    public void setBranchCode(String branchCode) {
        this.branchCode = branchCode;
    }

    public String getDebtorCode() {
        return debtorCode;
    }

    public void setDebtorCode(String debtorCode) {
        this.debtorCode = debtorCode;
    }

    public String getDebtorProfitCenter() {
        return debtorProfitCenter;
    }

    public void setDebtorProfitCenter(String debtorProfitCenter) {
        this.debtorProfitCenter = debtorProfitCenter;
    }

    public String getSundryReceiptId() {
        return sundryReceiptId;
    }

    public void setSundryReceiptId(String sundryReceiptId) {
        this.sundryReceiptId = sundryReceiptId;
    }

    public Character getRequiredAuthorization() {
        return requiredAuthorization;
    }

    public void setRequiredAuthorization(Character requiredAuthorization) {
        this.requiredAuthorization = requiredAuthorization;
    }

    public Double getNewConversionRate() {
        return newConversionRate;
    }

    public void setNewConversionRate(Double newConversionRate) {
        this.newConversionRate = newConversionRate;
    }

    public String getDebitNoteNo() {
        return debitNoteNo;
    }

    public void setDebitNoteNo(String debitNoteNo) {
        this.debitNoteNo = debitNoteNo;
    }

    public String getPolicyNo() {
        return policyNo;
    }

    public void setPolicyNo(String policyNo) {
        this.policyNo = policyNo;
    }

    public String getCustomerCode() {
        return customerCode;
    }

    public void setCustomerCode(String customerCode) {
        this.customerCode = customerCode;
    }

    public String getSalesForceCode() {
        return salesForceCode;
    }

    public void setSalesForceCode(String salesForceCode) {
        this.salesForceCode = salesForceCode;
    }

    public String getSignatureId() {
        return signatureId;
    }

    public void setSignatureId(String signatureId) {
        this.signatureId = signatureId;
    }

    public String getAddressId() {
        return addressId;
    }

    public void setAddressId(String addressId) {
        this.addressId = addressId;
    }

    public Character getChangeStatus() {
        return changeStatus;
    }

    public void setChangeStatus(Character changeStatus) {
        this.changeStatus = changeStatus;
    }

    public String getTransactionProcessedBranchCode() {
        return transactionProcessedBranchCode;
    }

    public void setTransactionProcessedBranchCode(String transactionProcessedBranchCode) {
        this.transactionProcessedBranchCode = transactionProcessedBranchCode;
    }

    public String getRiskName() {
        return riskName;
    }

    public void setRiskName(String riskName) {
        this.riskName = riskName;
    }

    public String getBusinessPartyCode() {
        return businessPartyCode;
    }

    public void setBusinessPartyCode(String businessPartyCode) {
        this.businessPartyCode = businessPartyCode;
    }

    public Character getPaymentResponsiblePersonType() {
        return paymentResponsiblePersonType;
    }

    public void setPaymentResponsiblePersonType(Character paymentResponsiblePersonType) {
        this.paymentResponsiblePersonType = paymentResponsiblePersonType;
    }

    public String getRemarks() {
        return remarks;
    }

    public void setRemarks(String remarks) {
        this.remarks = remarks;
    }

    public Date getDishonoredChequeDate() {
        return dishonoredChequeDate;
    }

    public void setDishonoredChequeDate(Date dishonoredChequeDate) {
        this.dishonoredChequeDate = dishonoredChequeDate;
    }

    public String getDishonoredChequeId() {
        return dishonoredChequeId;
    }

    public void setDishonoredChequeId(String dishonoredChequeId) {
        this.dishonoredChequeId = dishonoredChequeId;
    }

}
