package com.infoinstech.claimsmodule.domain.model.receipting.transaction;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "RC_T_FUNDS")
public class Fund {

    @Id
    @Column(name = "FND_SEQ_NO")
    private String fundSeqNo;

    @Column(name = "FND_DEB_SEQ_NO")
    private String fundDebitSeq;

    @Column(name = "FND_CRN_SEQ_NO")
    private String fundCreditSeq;

    @Column(name = "FND_AMONT")
    private Double fundAmount;

    @Column(name = "FND_FUN_CODE")
    private String fundCode;

    public String getFundSeqNo() {
        return fundSeqNo;
    }

    public void setFundSeqNo(String fundSeqNo) {
        this.fundSeqNo = fundSeqNo;
    }

    public String getFundDebitSeq() {
        return fundDebitSeq;
    }

    public void setFundDebitSeq(String fundDebitSeq) {
        this.fundDebitSeq = fundDebitSeq;
    }

    public String getFundCreditSeq() {
        return fundCreditSeq;
    }

    public void setFundCreditSeq(String fundCreditSeq) {
        this.fundCreditSeq = fundCreditSeq;
    }

    public Double getFundAmount() {
        return fundAmount;
    }

    public void setFundAmount(Double fundAmount) {
        this.fundAmount = fundAmount;
    }

    public String getFundCode() {
        return fundCode;
    }

    public void setFundCode(String fundCode) {
        this.fundCode = fundCode;
    }
}
