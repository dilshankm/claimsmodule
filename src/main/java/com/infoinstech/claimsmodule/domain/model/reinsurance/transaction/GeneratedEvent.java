package com.infoinstech.claimsmodule.domain.model.reinsurance.transaction;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import java.util.Date;

@Entity
@Table(name = "RI_T_EVENT_GENERATE")
public class GeneratedEvent {

    @Id
    @Column(name = "EGE_EVENT_SEQ")
    private String sequenceNo;

    @NotNull
    @Column(name = "EGE_EVENT_NO")
    private String eventNo;

    @Column(name = "EGE_ALLOCATE_DT")
    private Date allocatedDate;

    @NotNull
    @Column(name = "EGE_EVENT_DURATION")
    private String eventDuration;

    @Column(name = "EGE_REAMRKS")
    private String remarks;

    @Column(name = "EGE_CLOSE_DT")
    private Date closedDate;

    @Column(name = "EGE_APPLIED_FROM_DT")
    private Date appliedFromDate;

    @Column(name = "EGE_APPLIED_TO_DT")
    private Date appliedToDate;

    @NotNull
    @Column(name = "EGE_TREATY_SEQ")
    private String treatySequenceNo;

    @Column(name = "EGE_AUTH_DATE")
    private Date authorizedDate;

    @NotNull
    @Column(name = "EGE_EVENT_STATUS")
    private Character status;

    @NotNull
    @Column(name = "EGE_TREATY_CODE")
    private String treatyCode;

    @Column(name = "EGE_AUTH_CODE")
    private String authorizationCode;

    @Column(name = "CREATED_BY")
    private String createdBy;

    @Column(name = "CREATED_DATE")
    private Date createdDate;

    @Column(name = "MODIFIED_BY")
    private String modifiedBy;

    @Column(name = "MODIFIED_DATE")
    private Date modifiedDate;

    @Column(name = "EGE_PERIL_CODE")
    private String perilCode;

    @NotNull
    @Column(name = "EGE_VERSION_NO")
    private String versionNo;

    @Column(name = "EGE_UW_YEAR")
    private String underwritingYear;

    public String getSequenceNo() {
        return sequenceNo;
    }

    public void setSequenceNo(String sequenceNo) {
        this.sequenceNo = sequenceNo;
    }

    public String getEventNo() {
        return eventNo;
    }

    public void setEventNo(String eventNo) {
        this.eventNo = eventNo;
    }

    public Date getAllocatedDate() {
        return allocatedDate;
    }

    public void setAllocatedDate(Date allocatedDate) {
        this.allocatedDate = allocatedDate;
    }

    public String getEventDuration() {
        return eventDuration;
    }

    public void setEventDuration(String eventDuration) {
        this.eventDuration = eventDuration;
    }

    public String getRemarks() {
        return remarks;
    }

    public void setRemarks(String remarks) {
        this.remarks = remarks;
    }

    public Date getClosedDate() {
        return closedDate;
    }

    public void setClosedDate(Date closedDate) {
        this.closedDate = closedDate;
    }

    public Date getAppliedFromDate() {
        return appliedFromDate;
    }

    public void setAppliedFromDate(Date appliedFromDate) {
        this.appliedFromDate = appliedFromDate;
    }

    public Date getAppliedToDate() {
        return appliedToDate;
    }

    public void setAppliedToDate(Date appliedToDate) {
        this.appliedToDate = appliedToDate;
    }

    public String getTreatySequenceNo() {
        return treatySequenceNo;
    }

    public void setTreatySequenceNo(String treatySequenceNo) {
        this.treatySequenceNo = treatySequenceNo;
    }

    public Date getAuthorizedDate() {
        return authorizedDate;
    }

    public void setAuthorizedDate(Date authorizedDate) {
        this.authorizedDate = authorizedDate;
    }

    public Character getStatus() {
        return status;
    }

    public void setStatus(Character status) {
        this.status = status;
    }

    public String getTreatyCode() {
        return treatyCode;
    }

    public void setTreatyCode(String treatyCode) {
        this.treatyCode = treatyCode;
    }

    public String getAuthorizationCode() {
        return authorizationCode;
    }

    public void setAuthorizationCode(String authorizationCode) {
        this.authorizationCode = authorizationCode;
    }

    public String getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(String createdBy) {
        this.createdBy = createdBy;
    }

    public Date getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(Date createdDate) {
        this.createdDate = createdDate;
    }

    public String getModifiedBy() {
        return modifiedBy;
    }

    public void setModifiedBy(String modifiedBy) {
        this.modifiedBy = modifiedBy;
    }

    public Date getModifiedDate() {
        return modifiedDate;
    }

    public void setModifiedDate(Date modifiedDate) {
        this.modifiedDate = modifiedDate;
    }

    public String getPerilCode() {
        return perilCode;
    }

    public void setPerilCode(String perilCode) {
        this.perilCode = perilCode;
    }

    public String getVersionNo() {
        return versionNo;
    }

    public void setVersionNo(String versionNo) {
        this.versionNo = versionNo;
    }

    public String getUnderwritingYear() {
        return underwritingYear;
    }

    public void setUnderwritingYear(String underwritingYear) {
        this.underwritingYear = underwritingYear;
    }
}
