package com.infoinstech.claimsmodule.domain.model.salesmarketing;

import com.infoinstech.claimsmodule.domain.model.common.LoginAttempt;

import javax.persistence.*;
import java.util.List;

@Entity
@Table(name = "SM_M_SALES_FORCE")
public class SystemUser {

		@Id
		@Column(name = "SFC_CODE")
		private String code;

		@Column(name = "SFC_ACTIVE")
		private Character isActive;

		@Column(name = "SFC_SYSTEM_USER")
		private Character systemUser;

		@Column(name = "SFC_USERNAME")
		private String userName;

		@Column(name = "SFC_PASSWORD")
		private String password;

		@Column(name = "SFC_INITIALS")
		private String initials;

		@Column(name = "SFC_FIRST_NAME")
		private String firstName;

		@Column(name = "SFC_SURNAME")
		private String surname;

		@Column(name = "SFC_INT_EXT")
		private String businessTypeCode;

		@OneToMany(fetch = FetchType.LAZY)
		@JoinColumn(
						name = "LGA_SFC_CODE", referencedColumnName = "SFC_CODE",
						insertable = false, updatable = false
		)
		private List<LoginAttempt> loginAttempt;

		@Transient
		private boolean isBlocked;

		public String getCode() {
				return code;
		}

		public void setCode(String code) {
				this.code = code;
		}

		public Character getIsActive() {
				return isActive;
		}

		public void setIsActive(Character isActive) {
				this.isActive = isActive;
		}

		public Character getSystemUser() {
				return systemUser;
		}

		public void setSystemUser(Character systemUser) {
				this.systemUser = systemUser;
		}

		public String getUserName() {
				return userName;
		}

		public void setUserName(String userName) {
				this.userName = userName;
		}

		public String getPassword() {
				return password;
		}

		public void setPassword(String password) {
				this.password = password;
		}

		public String getInitials() {
				return initials;
		}

		public void setInitials(String initials) {
				this.initials = initials;
		}

		public String getFirstName() {
				return firstName;
		}

		public void setFirstName(String firstName) {
				this.firstName = firstName;
		}

		public String getSurname() {
				return surname;
		}

		public void setSurname(String surname) {
				this.surname = surname;
		}

		public String getBusinessTypeCode() {
				return businessTypeCode;
		}

		public void setBusinessTypeCode(String businessTypeCode) {
				this.businessTypeCode = businessTypeCode;
		}

		public List<LoginAttempt> getLoginAttempt() {
				return loginAttempt;
		}

		public void setLoginAttempt(List<LoginAttempt> loginAttempt) {
				this.loginAttempt = loginAttempt;
		}

		public void setIsBlocked(boolean isBlocked){
				this.isBlocked = isBlocked;
				this.loginAttempt.get(0).setActive(isBlocked?"Y":"N");
		}

		public boolean getIsBlocked(){

				if(loginAttempt == null || loginAttempt.size()==0){
						isBlocked = false;
				}else {
						isBlocked = loginAttempt.get(0).getActive().equals("N") || loginAttempt.get(0).getFailAttempts() == 3 ;
				}
				return isBlocked;
		}
}
