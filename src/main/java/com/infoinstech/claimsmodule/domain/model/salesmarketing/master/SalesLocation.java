package com.infoinstech.claimsmodule.domain.model.salesmarketing.master;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import java.util.Date;

@Entity
@Table(name = "SM_M_SALESLOC")
public class SalesLocation {

    @Id
    @Column(name = "SLC_BRN_CODE")
    private String code;

    @NotNull
    @Column(name = "SLC_LEVEL")
    private String level;

    @Column(name = "SLC_REPORT_CODE")
    private String reportCode;

    @Column(name = "SLC_ACC_CODE")
    private String accountCode;

    @NotNull
    @Column(name = "SLC_BRN_DESC")
    private String description;

    @Column(name = "SLC_PERMNT_ADD")
    private String permanentAddress;

    @Column(name = "SLC_PERMNT_PHONE")
    private String permanentPhoneNo;

    @Column(name = "SLC_PERMNT_FAX")
    private String permanentFaxNo;

    @Column(name = "SLC_CORSPND_ADD")
    private String correspondingAddress;

    @Column(name = "SLC_CORSPND_PHONE")
    private String correspondingPhoneNo;

    @Column(name = "SLC_ACTIVE")
    private Character isActive;

    @NotNull
    @Column(name = "SLC_SALES_PNT")
    private String salesPoint;

    @Column(name = "SLC_AREA_CODE")
    private String areaCode;

    @Column(name = "CREATED_BY")
    private String createdBy;

    @Column(name = "CREATED_DATE")
    private Date createdDate;

    @Column(name = "MODIFIED_BY")
    private String modifiedBy;

    @Column(name = "MODIFIED_DATE")
    private Date modifiedDate;

    //@Column(name="SLC__BELONG_NT VARCHAR2(5),

    @Column(name = "SLC_CORSPND_FAX")
    private String correspondingFaxNo;

    @Column(name = "SLC_PROFIT_CODE")
    private String profitCode;

//    @Column(name="SLC__RPT_ALLOC_DATE DATE,
//    @Column(name="SLC__ACC_ALLOC_DATE DATE,
//    @Column(name="SLC__REIN_FLAG VARCHAR2(1),
//    @Column(name="SLC__ORA_SEQ VARCHAR2(15),
//    @Column(name="SLC__PERMNT_PHONE2 VARCHAR2(30),
//    @Column(name="SLC__CORSPND_PHONE2 VARCHAR2(30),
//    @Column(name="SLC__BRANCH_TYPE VARCHAR2(5) not null,
//    @Column(name="SLC__DBASE_CODE VARCHAR2(5),
//    @Column(name="SLC__DBASE_DESC VARCHAR2(50),
//    @Column(name="SLC__DB_RPT_CODE VARCHAR2(5),
//    @Column(name="SLC__DB_RPT_BRN_CODE VARCHAR2(5),
//    @Column(name="SLC__DB_STATUS VARCHAR2(1),
//    @Column(name="SLC__CORSPND_EMAIL VARCHAR2(50),
//    @Column(name="SLC__PERMNT_EMAIL VARCHAR2(50),
//    @Column(name="SLC__PHY_LOGIC VARCHAR2(1), "
//            @Column(name="SLC__CRM_ISSUE VARCHAR2(1),
//  @Column(name="SLC__ABRIV_CODE VARCHAR2(5),
//  @Column(name="SLC__BACKUP_STATUS VARCHAR2(1) default NULL,
//  @Column(name="SLC__BACKUP_DATE DATE,
//  @Column(name="SLC__INTL_EXTL VARCHAR2(1),
//  @Column(name="SLC__CENTRAL_UNIT VARCHAR2(1),
//  @Column(name="SLC__FIN_MAP_CODE VARCHAR2(20)


    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getLevel() {
        return level;
    }

    public void setLevel(String level) {
        this.level = level;
    }

    public String getReportCode() {
        return reportCode;
    }

    public void setReportCode(String reportCode) {
        this.reportCode = reportCode;
    }

    public String getAccountCode() {
        return accountCode;
    }

    public void setAccountCode(String accountCode) {
        this.accountCode = accountCode;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getPermanentAddress() {
        return permanentAddress;
    }

    public void setPermanentAddress(String permanentAddress) {
        this.permanentAddress = permanentAddress;
    }

    public String getPermanentPhoneNo() {
        return permanentPhoneNo;
    }

    public void setPermanentPhoneNo(String permanentPhoneNo) {
        this.permanentPhoneNo = permanentPhoneNo;
    }

    public String getPermanentFaxNo() {
        return permanentFaxNo;
    }

    public void setPermanentFaxNo(String permanentFaxNo) {
        this.permanentFaxNo = permanentFaxNo;
    }

    public String getCorrespondingAddress() {
        return correspondingAddress;
    }

    public void setCorrespondingAddress(String correspondingAddress) {
        this.correspondingAddress = correspondingAddress;
    }

    public String getCorrespondingPhoneNo() {
        return correspondingPhoneNo;
    }

    public void setCorrespondingPhoneNo(String correspondingPhoneNo) {
        this.correspondingPhoneNo = correspondingPhoneNo;
    }

    public Character getIsActive() {
        return isActive;
    }

    public void setIsActive(Character isActive) {
        this.isActive = isActive;
    }

    public String getSalesPoint() {
        return salesPoint;
    }

    public void setSalesPoint(String salesPoint) {
        this.salesPoint = salesPoint;
    }

    public String getAreaCode() {
        return areaCode;
    }

    public void setAreaCode(String areaCode) {
        this.areaCode = areaCode;
    }

    public String getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(String createdBy) {
        this.createdBy = createdBy;
    }

    public Date getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(Date createdDate) {
        this.createdDate = createdDate;
    }

    public String getModifiedBy() {
        return modifiedBy;
    }

    public void setModifiedBy(String modifiedBy) {
        this.modifiedBy = modifiedBy;
    }

    public Date getModifiedDate() {
        return modifiedDate;
    }

    public void setModifiedDate(Date modifiedDate) {
        this.modifiedDate = modifiedDate;
    }

    public String getCorrespondingFaxNo() {
        return correspondingFaxNo;
    }

    public void setCorrespondingFaxNo(String correspondingFaxNo) {
        this.correspondingFaxNo = correspondingFaxNo;
    }

    public String getProfitCode() {
        return profitCode;
    }

    public void setProfitCode(String profitCode) {
        this.profitCode = profitCode;
    }
}
