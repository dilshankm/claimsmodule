package com.infoinstech.claimsmodule.domain.model.salesmarketing.reference;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import java.util.Date;

@Entity
@Table(name = "SM_R_BUSINESS_TYP")
public class BusinessType {

    @Id
    @Column(name = "BSS_BSS_CODE")
    private String code;

    @Column(name = "BSS_BSS_DESC")
    private String description;

    @Column(name = "CREATED_BY")
    private String createdBy;

    @Column(name = "CREATED_DATE")
    private Date createdDate;

    @Column(name = "MODIFIED_BY")
    private String modifiedBy;

    @Column(name = "MODIFIED_DATE")
    private Date modifiedDate;

    @NotNull
    @Column(name = "BSS_SOURCE_DEBTOR")
    private Character sourceDebtor;

    @NotNull
    @Column(name = "BSS_PREPAID_COMM")
    private Character prepaidCommission;

    @NotNull
    @Column(name = "BSS_WITH_HOLD_TX")
    private Character withholdTax;

    @Column(name = "BSS_FIN_MAP_CODE")
    private String financeMappingCode;

//    @Column(name="BSS_BSS_FLAG")
//    private Character businessFlag;

//    @Column(name="BSS_PAY_RESPONSIBLE")
//    private Character paymentResponsible;
//
//    @Column(name="BSS_MAP_CODE")
//    private Character mappingCode;
//
//    @Column(name="BSS_COMMISSION_ALLOWED")
//    private Character commissionAllowed;

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(String createdBy) {
        this.createdBy = createdBy;
    }

    public Date getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(Date createdDate) {
        this.createdDate = createdDate;
    }

    public String getModifiedBy() {
        return modifiedBy;
    }

    public void setModifiedBy(String modifiedBy) {
        this.modifiedBy = modifiedBy;
    }

    public Date getModifiedDate() {
        return modifiedDate;
    }

    public void setModifiedDate(Date modifiedDate) {
        this.modifiedDate = modifiedDate;
    }

    public Character getSourceDebtor() {
        return sourceDebtor;
    }

    public void setSourceDebtor(Character sourceDebtor) {
        this.sourceDebtor = sourceDebtor;
    }

    public Character getPrepaidCommission() {
        return prepaidCommission;
    }

    public void setPrepaidCommission(Character prepaidCommission) {
        this.prepaidCommission = prepaidCommission;
    }

    public Character getWithholdTax() {
        return withholdTax;
    }

    public void setWithholdTax(Character withholdTax) {
        this.withholdTax = withholdTax;
    }

    public String getFinanceMappingCode() {
        return financeMappingCode;
    }

    public void setFinanceMappingCode(String financeMappingCode) {
        this.financeMappingCode = financeMappingCode;
    }

//    public Character getBusinessFlag() {
//        return businessFlag;
//    }
//
//    public void setBusinessFlag(Character businessFlag) {
//        this.businessFlag = businessFlag;
//    }
//0.

}
