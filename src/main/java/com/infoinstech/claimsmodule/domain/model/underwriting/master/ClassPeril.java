package com.infoinstech.claimsmodule.domain.model.underwriting.master;

import com.infoinstech.claimsmodule.domain.model.underwriting.reference.RefClass;
import com.infoinstech.claimsmodule.domain.model.underwriting.reference.RefPeril;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.util.Date;

@Entity
@Table(name = "UW_M_PERILS")
public class ClassPeril {

    @EmbeddedId
    ClassPerilPK classPerilPK;

    @Column(name = "PER_SEQ_NO")
    private String sequenceNo;

    @NotNull
    @Column(name = "PER_BASIS")
    private Character perBasis;

    @Column(name = "CREATED_BY")
    private String createdBy;

    @Column(name = "CREATED_DATE")
    private Date createdDate;

    @Column(name = "MODIFIED_BY")
    private String modifiedBy;

    @Column(name = "MODIFIED_DATE")
    private Date modifiedDate;

    @OneToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "PER_PRL_CODE", referencedColumnName = "PRL_CODE", insertable = false, updatable = false)
    private RefPeril refPeril;

    @OneToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "PER_CLA_CODE", referencedColumnName = "CLA_CODE", insertable = false, updatable = false)
    private RefClass refClass;

    public ClassPerilPK getClassPerilPK() {
        return classPerilPK;
    }

    public void setClassPerilPK(ClassPerilPK classPerilPK) {
        this.classPerilPK = classPerilPK;
    }

    public String getSequenceNo() {
        return sequenceNo;
    }

    public void setSequenceNo(String sequenceNo) {
        this.sequenceNo = sequenceNo;
    }

    public Character getPerBasis() {
        return perBasis;
    }

    public void setPerBasis(Character perBasis) {
        this.perBasis = perBasis;
    }

    public String getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(String createdBy) {
        this.createdBy = createdBy;
    }

    public Date getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(Date createdDate) {
        this.createdDate = createdDate;
    }

    public String getModifiedBy() {
        return modifiedBy;
    }

    public void setModifiedBy(String modifiedBy) {
        this.modifiedBy = modifiedBy;
    }

    public Date getModifiedDate() {
        return modifiedDate;
    }

    public void setModifiedDate(Date modifiedDate) {
        this.modifiedDate = modifiedDate;
    }

    public RefPeril getRefPeril() {
        return refPeril;
    }

    public void setRefPeril(RefPeril refPeril) {
        this.refPeril = refPeril;
    }

    public RefClass getRefClass() {
        return refClass;
    }

    public void setRefClass(RefClass refClass) {
        this.refClass = refClass;
    }
}
