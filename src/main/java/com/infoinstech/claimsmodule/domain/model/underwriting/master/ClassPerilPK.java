package com.infoinstech.claimsmodule.domain.model.underwriting.master;

import javax.persistence.Column;
import javax.persistence.Embeddable;
import javax.validation.constraints.NotNull;
import java.io.Serializable;

@Embeddable
public class ClassPerilPK implements Serializable {

    @NotNull
    @Column(name = "PER_PRL_CODE")
    private String perilCode;

    @NotNull
    @Column(name = "PER_CLA_CODE")
    private String classCode;

    public ClassPerilPK() {
    }

    public ClassPerilPK(String perilCode, String classCode) {
        this.perilCode = perilCode;
        this.classCode = classCode;
    }

    public String getPerilCode() {
        return perilCode;
    }

    public void setPerilCode(String perilCode) {
        this.perilCode = perilCode;
    }

    public String getClassCode() {
        return classCode;
    }

    public void setClassCode(String classCode) {
        this.classCode = classCode;
    }
}
