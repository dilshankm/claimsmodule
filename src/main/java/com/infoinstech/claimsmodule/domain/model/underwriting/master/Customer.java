package com.infoinstech.claimsmodule.domain.model.underwriting.master;

import javax.persistence.*;
import java.util.Date;
import java.util.List;

@Entity
@Table(name = "UW_M_CUSTOMERS")
public class Customer {

    @Id
// @GeneratedValue(strategy = GenerationType.SEQUENCE)
    @Column(name = "CUS_CODE")
    private String customerCode;

    @Column(name = "CUS_TYPE")
    private Character customerType;

    @Column(name = "CUS_INDV_SURNAME")
    private String indvSurname;

    @Column(name = "CUS_INDV_INITIALS")
    private String indvInitials;

    @Column(name = "CUS_INDV_OTHER_NAMES")
    private String indvOtherNames;

    @Column(name = "CUS_INDV_NIC_NO")
    private String indvNIC;

    @Column(name = "CUS_CORP_NAME")
    private String corpName;

    @Column(name = "CUS_DISTRICT")
    private String district;

    @Column(name = "CUS_PROVINCE")
    private String province;

    @Column(name = "CUS_POSTAL_CODE")
    private String postalCode;

    @Column(name = "CUS_PHONE_1")
    private String phoneNo1;

    @Column(name = "CUS_PHONE_2")
    private String phoneNo2;

    @Column(name = "CUS_FAX")
    private String faxNo;

    @Column(name = "CUS_EMAIL")
    private String emailAddress;

    @Column(name = "CUS_WEB_PAGE")
    private String webPage;

    @Column(name = "CUS_ADDRESS_1")
    private String address1;

    @Column(name = "CUS_ADDRESS_2")
    private String address2;

    @Column(name = "CUS_ADDRESS_3")
    private String address3;

    @Column(name = "CREATED_BY")
    private String createdBy;

    @Column(name = "CREATED_DATE")
    private Date createdDate;

    @Column(name = "MODIFIED_BY")
    private String modifiedBy;

    @Column(name = "MODIFIED_DATE")
    private String modifiedDate;

    @Column(name = "CUS_RLC_RLOC_CODE")
    private String rlcRlcCode;

    @Column(name = "CUS_INDV_GENDER")
    private String indvGender;

    @Column(name = "CUS_OUTSTAND_AMT")
    private Double outstandAmount;

    @Column(name = "CUS_GRP_CODE")
    private String groupCode;

    @Column(name = "CUS_CORP_REG_NO")
    private String corpRegNo;

    @Column(name = "CUS_INDV_TITLE")
    private String indvTitle;

    @Column(name = "CUS_RC_OUTSTAND_CHQ_AMT")
    private Double outstandCheckAmount;

    @Column(name = "CUS_CTY_CODE")
    private String cityCode;

    @Column(name = "CUS_CITY")
    private String city;

    @Column(name = "CUS_STREET")
    private String street;

    @Column(name = "CUS_BUILDING")
    private String building;

    @Column(name = "CUS_NUMBER")
    private String number;

    @Column(name = "CUS_LOC_DESCRIPTION")
    private String locationDescription;

    @Column(name = "CUS_JOINT")
    private String jointCustomer;

    @Column(name = "CUS_FATHERS_NAME")
    private String fathersName;

    @Column(name = "CUS_DEBTOR_INSERT")
    private String insertDebtor;

    @Column(name = "CUS_CHANGE_STATUS")
    private Character changeStatus;

    @Column(name = "CUS_CVL_CODE")
    private String civilStatusCode;

    @Column(name = "CUS_CNL_CODE")
    private String nationlityCode;

    @Column(name = "CUS_INDV_FIRST_NAME")
    private String indvFirstName;

    @Column(name = "CUS_INDV_DOB")
    private String indvDatOfBirth;

    @Column(name = "CUS_INDV_PP_NO")
    private String indvPassportNo;

    @Column(name = "CUS_INDV_DL_NO")
    private String indvDrivingLiscenceNo;

    @Column(name = "CUS_STATUS")
    private String customerStatus;

    @Column(name = "CUS_ONLINE_LOGIN")
    private String onilineLogin;

    @Column(name = "CUS_ONLINE_PASSWORD")
    private String onlinePassword;

//    @Column(name = "CUS_BRN_CODE")
//    private String branchCode;
//
//    @Column(name = "CUS_BPARTY_CODE")
//    private String accountHanlder;
//
//    @Column(name = "CUS_SEND_EMAIL")
//    private String sendEmail;
//
//    @Column(name = "CUS_SEND_SMS")
//    private String sendSms;

    @OneToMany(fetch = FetchType.LAZY)
    @JoinColumn(name = "CCT_CUS_CODE", referencedColumnName = "CUS_CODE")
    List<CustomerContact> customerContactList;

    public List<CustomerContact> getCustomerContactList() {
        return customerContactList;
    }

    public void setCustomerContactList(List<CustomerContact> customerContactList) {
        this.customerContactList = customerContactList;
    }


    public String getIndvInitials() {
        return indvInitials;
    }

    public void setIndvInitials(String indvInitials) {
        this.indvInitials = indvInitials;
    }

    public String getCustomerCode() {
        return customerCode;
    }

    public void setCustomerCode(String customerCode) {
        this.customerCode = customerCode;
    }

    public Character getCustomerType() {
        return customerType;
    }

    public void setCustomerType(Character customerType) {
        this.customerType = customerType;
    }

    public String getIndvSurname() {
        return indvSurname;
    }

    public void setIndvSurname(String indvSurname) {
        this.indvSurname = indvSurname;
    }

    public String getIndvOtherNames() {
        return indvOtherNames;
    }

    public void setIndvOtherNames(String indvOtherNames) {
        this.indvOtherNames = indvOtherNames;
    }

    public String getIndvNIC() {
        return indvNIC;
    }

    public void setIndvNIC(String indvNIC) {
        this.indvNIC = indvNIC;
    }

    public String getCorpName() {
        return corpName;
    }

    public void setCorpName(String corpName) {
        this.corpName = corpName;
    }

    public String getDistrict() {
        return district;
    }

    public void setDistrict(String district) {
        this.district = district;
    }

    public String getProvince() {
        return province;
    }

    public void setProvince(String province) {
        this.province = province;
    }

    public String getPostalCode() {
        return postalCode;
    }

    public void setPostalCode(String postalCode) {
        this.postalCode = postalCode;
    }

    public String getPhoneNo1() {
        return phoneNo1;
    }

    public void setPhoneNo1(String phoneNo1) {
        this.phoneNo1 = phoneNo1;
    }

    public String getPhoneNo2() {
        return phoneNo2;
    }

    public void setPhoneNo2(String phoneNo2) {
        this.phoneNo2 = phoneNo2;
    }

    public String getFaxNo() {
        return faxNo;
    }

    public void setFaxNo(String faxNo) {
        this.faxNo = faxNo;
    }

    public String getEmailAddress() {
        return emailAddress;
    }

    public void setEmailAddress(String emailAddress) {
        this.emailAddress = emailAddress;
    }

    public String getWebPage() {
        return webPage;
    }

    public void setWebPage(String webPage) {
        this.webPage = webPage;
    }

    public String getAddress1() {
        return address1;
    }

    public void setAddress1(String address1) {
        this.address1 = address1;
    }

    public String getAddress2() {
        return address2;
    }

    public void setAddress2(String address2) {
        this.address2 = address2;
    }

    public String getAddress3() {
        return address3;
    }

    public void setAddress3(String address3) {
        this.address3 = address3;
    }

    public String getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(String createdBy) {
        this.createdBy = createdBy;
    }

    public Date getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(Date createdDate) {
        this.createdDate = createdDate;
    }

    public String getModifiedBy() {
        return modifiedBy;
    }

    public void setModifiedBy(String modifiedBy) {
        this.modifiedBy = modifiedBy;
    }

    public String getModifiedDate() {
        return modifiedDate;
    }

    public void setModifiedDate(String modifiedDate) {
        this.modifiedDate = modifiedDate;
    }

    public String getRlcRlcCode() {
        return rlcRlcCode;
    }

    public void setRlcRlcCode(String rlcRlcCode) {
        this.rlcRlcCode = rlcRlcCode;
    }

    public String getIndvGender() {
        return indvGender;
    }

    public void setIndvGender(String indvGender) {
        this.indvGender = indvGender;
    }

    public Double getOutstandAmount() {
        return outstandAmount;
    }

    public void setOutstandAmount(Double outstandAmount) {
        this.outstandAmount = outstandAmount;
    }

    public String getGroupCode() {
        return groupCode;
    }

    public void setGroupCode(String groupCode) {
        this.groupCode = groupCode;
    }

    public String getCorpRegNo() {
        return corpRegNo;
    }

    public void setCorpRegNo(String corpRegNo) {
        this.corpRegNo = corpRegNo;
    }

    public String getIndvTitle() {
        return indvTitle;
    }

    public void setIndvTitle(String indvTitle) {
        this.indvTitle = indvTitle;
    }

    public Double getOutstandCheckAmount() {
        return outstandCheckAmount;
    }

    public void setOutstandCheckAmount(Double outstandCheckAmount) {
        this.outstandCheckAmount = outstandCheckAmount;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getCityCode() {
        return cityCode;
    }

    public void setCityCode(String cityCode) {
        this.cityCode = cityCode;
    }

    public String getStreet() {
        return street;
    }

    public void setStreet(String street) {
        this.street = street;
    }

    public String getBuilding() {
        return building;
    }

    public void setBuilding(String building) {
        this.building = building;
    }

    public String getNumber() {
        return number;
    }

    public void setNumber(String number) {
        this.number = number;
    }

    public String getLocationDescription() {
        return locationDescription;
    }

    public void setLocationDescription(String locationDescription) {
        this.locationDescription = locationDescription;
    }

    public String getJointCustomer() {
        return jointCustomer;
    }

    public void setJointCustomer(String jointCustomer) {
        this.jointCustomer = jointCustomer;
    }

    public String getFathersName() {
        return fathersName;
    }

    public void setFathersName(String fathersName) {
        this.fathersName = fathersName;
    }

    public String getInsertDebtor() {
        return insertDebtor;
    }

    public void setInsertDebtor(String insertDebtor) {
        this.insertDebtor = insertDebtor;
    }

    public Character getChangeStatus() {
        return changeStatus;
    }

    public void setChangeStatus(Character changeStatus) {
        this.changeStatus = changeStatus;
    }

    public String getCivilStatusCode() {
        return civilStatusCode;
    }

    public void setCivilStatusCode(String civilStatusCode) {
        this.civilStatusCode = civilStatusCode;
    }

    public String getNationlityCode() {
        return nationlityCode;
    }

    public void setNationlityCode(String nationlityCode) {
        this.nationlityCode = nationlityCode;
    }

    public String getIndvFirstName() {
        return indvFirstName;
    }

    public void setIndvFirstName(String indvFirstName) {
        this.indvFirstName = indvFirstName;
    }

    public String getIndvDatOfBirth() {
        return indvDatOfBirth;
    }

    public void setIndvDatOfBirth(String indvDatOfBirth) {
        this.indvDatOfBirth = indvDatOfBirth;
    }

    public String getIndvPassportNo() {
        return indvPassportNo;
    }

    public void setIndvPassportNo(String indvPassportNo) {
        this.indvPassportNo = indvPassportNo;
    }

    public String getIndvDrivingLiscenceNo() {
        return indvDrivingLiscenceNo;
    }

    public void setIndvDrivingLiscenceNo(String indvDrivingLiscenceNo) {
        this.indvDrivingLiscenceNo = indvDrivingLiscenceNo;
    }

    public String getCustomerStatus() {
        return customerStatus;
    }

    public void setCustomerStatus(String customerStatus) {
        this.customerStatus = customerStatus;
    }

    public String getOnilineLogin() {
        return onilineLogin;
    }

    public void setOnilineLogin(String onilineLogin) {
        this.onilineLogin = onilineLogin;
    }

    public String getOnlinePassword() {
        return onlinePassword;
    }

    public void setOnlinePassword(String onlinePassword) {
        this.onlinePassword = onlinePassword;
    }
//
//    public String getBranchCode() {
//        return branchCode;
//    }
//
//    public void setBranchCode(String branchCode) {
//        this.branchCode = branchCode;
//    }
//
//    public String getAccountHanlder() {
//        return accountHanlder;
//    }
//
//    public void setAccountHanlder(String accountHanlder) {
//        this.accountHanlder = accountHanlder;
//    }
//
//    public String getSendEmail() {
//        return sendEmail;
//    }
//
//    public void setSendEmail(String sendEmail) {
//        this.sendEmail = sendEmail;
//    }
//
//    public String getSendSms() {
//        return sendSms;
//    }
//
//    public void setSendSms(String sendSms) {
//        this.sendSms = sendSms;
//    }


}
