package com.infoinstech.claimsmodule.domain.model.underwriting.master;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.util.Date;

@Entity
@Table(name = "UW_M_CUST_ADDRESSES")
public class CustomerAddress {

    @Id
    @Column(name = "ADR_SEQ_NO")
    private String sequenceNo;

    @Column(name = "ADR_CUS_CODE")
    private String customerCode;

    @Column(name = "ADR_CITY")
    private String city;

    @Column(name = "ADR_STREET")
    private String street;

    @Column(name = "ADR_BUILDING")
    private String building;

    @Column(name = "ADR_NUMBER")
    private String number;

    @Column(name = "ADR_LOC_DESCRIPTION")
    private String locationDescription;

    @Column(name = "ADR_DEFAULT")
    private Character defaultAddress;

    @Column(name = "ADR_PROVINCE")
    private String province;
    @Column(name = "ADR_DISTRICT")
    private String district;

    @Column(name = "ADR_POSTAL_CODE")
    private String postalCode;

    @Column(name = "CREATED_BY")
    private String createdBy;

    @Column(name = "CREATED_DATE")
    private Date createdDate;

    @Column(name = "MODIFIED_BY")
    private String modifiedBy;

    @Column(name = "MODIFIED_DATE")
    private Date modifiedDate;

    @Column(name = "ADR_EARTHQK")
    private String earthquakeZone;

    @Column(name = "ADR_CYCLONE")
    private String cycloneZone;

    public String getCustomerCode() {
        return customerCode;
    }

    public void setCustomerCode(String customerCode) {
        this.customerCode = customerCode;
    }

    public Character getDefaultAddress() {
        return defaultAddress;
    }

    public void setDefaultAddress(Character defaultAddress) {
        this.defaultAddress = defaultAddress;
    }

    public String getSequenceNo() {
        return sequenceNo;
    }

    public void setSequenceNo(String sequenceNo) {
        this.sequenceNo = sequenceNo;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getStreet() {
        return street;
    }

    public void setStreet(String street) {
        this.street = street;
    }

    public String getBuilding() {
        return building;
    }

    public void setBuilding(String building) {
        this.building = building;
    }

    public String getNumber() {
        return number;
    }

    public void setNumber(String number) {
        this.number = number;
    }

    public String getLocationDescription() {
        return locationDescription;
    }

    public void setLocationDescription(String locationDescription) {
        this.locationDescription = locationDescription;
    }

    public String getProvince() {
        return province;
    }

    public void setProvince(String province) {
        this.province = province;
    }

    public String getDistrict() {
        return district;
    }

    public void setDistrict(String district) {
        this.district = district;
    }

    public String getPostalCode() {
        return postalCode;
    }

    public void setPostalCode(String postalCode) {
        this.postalCode = postalCode;
    }

    public String getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(String createdBy) {
        this.createdBy = createdBy;
    }

    public Date getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(Date createdDate) {
        this.createdDate = createdDate;
    }

    public String getModifiedBy() {
        return modifiedBy;
    }

    public void setModifiedBy(String modifiedBy) {
        this.modifiedBy = modifiedBy;
    }

    public Date getModifiedDate() {
        return modifiedDate;
    }

    public void setModifiedDate(Date modifiedDate) {
        this.modifiedDate = modifiedDate;
    }

    public String getEarthquakeZone() {
        return earthquakeZone;
    }

    public void setEarthquakeZone(String earthquakeZone) {
        this.earthquakeZone = earthquakeZone;
    }

    public String getCycloneZone() {
        return cycloneZone;
    }

    public void setCycloneZone(String cycloneZone) {
        this.cycloneZone = cycloneZone;
    }
}
