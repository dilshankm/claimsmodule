package com.infoinstech.claimsmodule.domain.model.underwriting.master;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.util.Date;

@Entity
@Table(name = "UW_M_CUST_CONTACTS")
public class CustomerContact {

    //@GeneratedValue(strategy = GenerationType.SEQUENCE)
    @Id
    @Column(name = "CCT_SEQ_NO")
    private String contactCode;

//    @ManyToOne
//    @JoinColumn(name = "CCT_CUS_CODE", referencedColumnName = "CUS_CODE")
//    private Customer customer;

    @Column(name = "CCT_NAME")
    private String contactName;

//    @Column(name = "CCT_DESIGNATION")
//    private String Designation;

    @Column(name = "CCT_PHONE_NO")
    private String phoneNo;

    @Column(name = "CCT_EMAIL")
    private String emailAddress;

    @Column(name = "CCT_CUS_CODE")
    private String customerCode;

    @Column(name = "CREATED_BY")
    private String createdBy;

    @Column(name = "CREATED_DATE")
    private Date createdDate;

    @Column(name = "MODIFIED_BY")
    private String modifiedBy;

    @Column(name = "MODIFIED_DATE")
    private Date modifiedDate;

    @Column(name = "CCT_TITLE")
    private String contactTitle;


    public String getContactCode() {
        return contactCode;
    }

    public void setContactCode(String contactCode) {
        this.contactCode = contactCode;
    }

    public String getContactName() {
        return contactName;
    }

    public void setContactName(String contactName) {
        this.contactName = contactName;
    }

//    public String getDesignation() {
//        return Designation;
//    }
//
//    public void setDesignation(String designation) {
//        Designation = designation;
//    }


    public String getPhoneNo() {
        return phoneNo;
    }

    public void setPhoneNo(String phoneNo) {
        this.phoneNo = phoneNo;
    }

    public String getEmailAddress() {
        return emailAddress;
    }

    public void setEmailAddress(String emailAddress) {
        this.emailAddress = emailAddress;
    }

//    public Customer getCustomer() {
//        return customer;
//    }
//
//    public void setCustomer(Customer customer) {
//        this.customer = customer;
//    }

    public String getCustomerCode() {
        return customerCode;
    }

    public void setCustomerCode(String customerCode) {
        this.customerCode = customerCode;
    }

    public String getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(String createdBy) {
        this.createdBy = createdBy;
    }

    public Date getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(Date createdDate) {
        this.createdDate = createdDate;
    }

    public String getModifiedBy() {
        return modifiedBy;
    }

    public void setModifiedBy(String modifiedBy) {
        this.modifiedBy = modifiedBy;
    }

    public Date getModifiedDate() {
        return modifiedDate;
    }

    public void setModifiedDate(Date modifiedDate) {
        this.modifiedDate = modifiedDate;
    }

    public String getContactTitle() {
        return contactTitle;
    }

    public void setContactTitle(String contactTitle) {
        this.contactTitle = contactTitle;
    }

}
