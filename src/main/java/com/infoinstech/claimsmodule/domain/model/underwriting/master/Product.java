package com.infoinstech.claimsmodule.domain.model.underwriting.master;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "UW_M_PRODUCTS")
public class Product {

    @Id
    @Column(name = "PRD_CODE")
    private String code;

    @Column(name = "PRD_CLA_CODE")
    private String classCode;

    @Column(name = "PRD_DESCRIPTION")
    private String description;

    @Column(name = "PRD_DEF_PROV")
    private Double defaultProvision;

    @Column(name = "PRD_STATUS")
    private Character status;

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getClassCode() {
        return classCode;
    }

    public void setClassCode(String classCode) {
        this.classCode = classCode;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Double getDefaultProvision() {
        return defaultProvision;
    }

    public void setDefaultProvision(Double defaultProvision) {
        this.defaultProvision = defaultProvision;
    }

    public Character getStatus() {
        return status;
    }

    public void setStatus(Character status) {
        this.status = status;
    }
}