package com.infoinstech.claimsmodule.domain.model.underwriting.master;

import com.infoinstech.claimsmodule.domain.model.underwriting.reference.RefClass;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.util.Date;

@Entity
@Table(name = "UW_M_PROD_CLASSES")
public class ProductClass {

    @EmbeddedId
    private ProductClassPK productClassPK;

    @NotNull
    @Column(name = "PRC_CLA_CODE")
    private String classCode;

    @Column(name = "CREATED_BY")
    private String createdBy;

    @Column(name = "CREATED_DATE")
    private Date createdDate;

    @Column(name = "MODIFIED_BY")
    private String modifiedBy;


    @Column(name = "MODIFIED_DATE")
    private Date modifiedDate;

    @OneToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "PRC_CLA_CODE", referencedColumnName = "CLA_CODE", updatable = false, insertable = false)
    private RefClass refClass;


    public ProductClassPK getProductClassPK() {
        return productClassPK;
    }

    public void setProductClassPK(ProductClassPK productClassPK) {
        this.productClassPK = productClassPK;
    }

    public String getClassCode() {
        return classCode;
    }

    public void setClassCode(String classCode) {
        this.classCode = classCode;
    }

    public String getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(String createdBy) {
        this.createdBy = createdBy;
    }

    public Date getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(Date createdDate) {
        this.createdDate = createdDate;
    }

    public String getModifiedBy() {
        return modifiedBy;
    }

    public void setModifiedBy(String modifiedBy) {
        this.modifiedBy = modifiedBy;
    }

    public Date getModifiedDate() {
        return modifiedDate;
    }

    public void setModifiedDate(Date modifiedDate) {
        this.modifiedDate = modifiedDate;
    }

    public RefClass getRefClass() {
        return refClass;
    }

    public void setRefClass(RefClass refClass) {
        this.refClass = refClass;
    }
}
