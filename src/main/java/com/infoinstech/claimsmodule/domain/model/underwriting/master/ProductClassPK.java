package com.infoinstech.claimsmodule.domain.model.underwriting.master;

import javax.persistence.Column;
import javax.persistence.Embeddable;
import javax.validation.constraints.NotNull;
import java.io.Serializable;

@Embeddable
public class ProductClassPK implements Serializable {

    @Column(name = "PRC_SEQ_NO")
    private String prodClassSeqNo;

    @NotNull
    @Column(name = "PRC_PRD_CODE")
    private String productCode;

    public ProductClassPK() {
    }

    public String getProdClassSeqNo() {
        return prodClassSeqNo;
    }

    public ProductClassPK(String prodClassSeqNo, String productCode) {
        this.prodClassSeqNo = prodClassSeqNo;
        this.productCode = productCode;
    }

    public void setProdClassSeqNo(String prodClassSeqNo) {
        this.prodClassSeqNo = prodClassSeqNo;
    }

    public String getProductCode() {
        return productCode;
    }

    public void setProductCode(String productCode) {
        this.productCode = productCode;
    }
}
