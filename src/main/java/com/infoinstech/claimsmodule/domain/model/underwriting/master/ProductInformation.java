package com.infoinstech.claimsmodule.domain.model.underwriting.master;

import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.Table;
import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

@Entity
@Table(name = "UW_M_PROD_INFORMATION")
public class ProductInformation implements Serializable {

    //PRIMARY KEY (PIF_PRD_CODE, PIF_SEQ_NO),

    @EmbeddedId
    ProductInformationPK productInformationPK;

    @Column(name = "PIF_DESCRIPTION")
    private String description;

    @Column(name = "PIF_TYPE")
    private Character type;

    @Column(name = "PIF_LENGTH")
    private Integer length;

    @Column(name = "PIF_MANDATORY")
    private Character isMandatory;

    @Column(name = "PIF_RECORD_TYPE")
    private Character recordType;

    @Column(name = "PIF_PRINTED_IN_RECEIPT")
    private Character printedInReceipt;

    @Column(name = "PIF_I_SEQ")
    private String orderId;

    @Column(name = "PIF_DECIMAL")
    private BigDecimal noOfDecimals;

    @Column(name = "PIF_ACTIVE")
    private Character active;

    @Column(name = "PIF_RFT_CODE")
    private String referenceTypeCode;

    @Column(name = "PIF_LEVEL")
    private Character level;

    @Column(name = "PIF_PSC_SEQ_NO")
    private String productSectionSequenceNo;

    @Column(name = "PIF_PSC_SEC_CODE")
    private String productSectionCode;

    @Column(name = "PIF_PROPOSAL_ACTIVE")
    private Character proposalActive;

//    @Column(name = "PIF_MAND_QUOT")
//    private Character mandatoryQuotation;

    @Column(name = "CREATED_BY")
    private String createdBy;

    @Column(name = "CREATED_DATE")
    private Date createdDate;

    @Column(name = "MODIFIED_BY")
    private String modifiedBy;

    @Column(name = "MODIFIED_DATE")
    private Date modifiedDate;

    public ProductInformationPK getProductInformationPK() {
        return productInformationPK;
    }

    public void setProductInformationPK(ProductInformationPK productInformationPK) {
        this.productInformationPK = productInformationPK;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Character getType() {
        return type;
    }

    public void setType(Character type) {
        this.type = type;
    }

    public Integer getLength() {
        return length;
    }

    public void setLength(Integer length) {
        this.length = length;
    }

    public Character getIsMandatory() {
        return isMandatory;
    }

    public void setIsMandatory(Character isMandatory) {
        this.isMandatory = isMandatory;
    }

    public Character getRecordType() {
        return recordType;
    }

    public void setRecordType(Character recordType) {
        this.recordType = recordType;
    }

    public Character getPrintedInReceipt() {
        return printedInReceipt;
    }

    public void setPrintedInReceipt(Character printedInReceipt) {
        this.printedInReceipt = printedInReceipt;
    }

    public String getOrderId() {
        return orderId;
    }

    public void setOrderId(String orderId) {
        this.orderId = orderId;
    }

    public BigDecimal getNoOfDecimals() {
        return noOfDecimals;
    }

    public void setNoOfDecimals(BigDecimal noOfDecimals) {
        this.noOfDecimals = noOfDecimals;
    }

    public Character getActive() {
        return active;
    }

    public void setActive(Character active) {
        this.active = active;
    }

    public String getReferenceTypeCode() {
        return referenceTypeCode;
    }

    public void setReferenceTypeCode(String referenceTypeCode) {
        this.referenceTypeCode = referenceTypeCode;
    }

    public Character getLevel() {
        return level;
    }

    public void setLevel(Character level) {
        this.level = level;
    }

    public String getProductSectionSequenceNo() {
        return productSectionSequenceNo;
    }

    public void setProductSectionSequenceNo(String productSectionSequenceNo) {
        this.productSectionSequenceNo = productSectionSequenceNo;
    }

    public String getProductSectionCode() {
        return productSectionCode;
    }

    public void setProductSectionCode(String productSectionCode) {
        this.productSectionCode = productSectionCode;
    }

    public Character getProposalActive() {
        return proposalActive;
    }

    public void setProposalActive(Character proposalActive) {
        this.proposalActive = proposalActive;
    }

//    public Character getMandatoryQuotation() {
//        return mandatoryQuotation;
//    }
//
//    public void setMandatoryQuotation(Character mandatoryQuotation) {
//        this.mandatoryQuotation = mandatoryQuotation;
//    }

    public String getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(String createdBy) {
        this.createdBy = createdBy;
    }

    public Date getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(Date createdDate) {
        this.createdDate = createdDate;
    }

    public String getModifiedBy() {
        return modifiedBy;
    }

    public void setModifiedBy(String modifiedBy) {
        this.modifiedBy = modifiedBy;
    }

    public Date getModifiedDate() {
        return modifiedDate;
    }

    public void setModifiedDate(Date modifiedDate) {
        this.modifiedDate = modifiedDate;
    }
}