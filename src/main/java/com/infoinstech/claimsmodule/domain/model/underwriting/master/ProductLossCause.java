package com.infoinstech.claimsmodule.domain.model.underwriting.master;

import com.infoinstech.claimsmodule.domain.model.claims.reference.RefLossCause;

import javax.persistence.*;
import java.util.Date;

@Entity
@Table(name = "CL_M_PROD_CAUSE_LOSS")
public class ProductLossCause {

    @EmbeddedId
    private ProductLossCausePK productLossCausePK;

    @Column(name = "PCL_SEQ_T_NO")
    private String prodLossSeqNo;

    @Column(name = "CREATED_BY")
    private String createdBy;

    @Column(name = "CREATED_DATE")
    private Date createdDate;

    @Column(name = "MODIFIED_BY")
    private String modifiedBy;

    @Column(name = "MODIFIED_DATE")
    private String modifiedDate;

    @OneToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "PCL_PROD_CODE", referencedColumnName = "PRD_CODE", insertable = false, updatable = false)
    private Product product;

    @OneToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "PCL_CODE", referencedColumnName = "CLO_CODE", insertable = false, updatable = false)
    private RefLossCause refLossCause;

    public ProductLossCausePK getProductLossCausePK() {
        return productLossCausePK;
    }

    public void setProductLossCausePK(ProductLossCausePK productLossCausePK) {
        this.productLossCausePK = productLossCausePK;
    }

    public String getProdLossSeqNo() {
        return prodLossSeqNo;
    }

    public void setProdLossSeqNo(String prodLossSeqNo) {
        this.prodLossSeqNo = prodLossSeqNo;
    }

    public String getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(String createdBy) {
        this.createdBy = createdBy;
    }

    public Date getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(Date createdDate) {
        this.createdDate = createdDate;
    }

    public String getModifiedBy() {
        return modifiedBy;
    }

    public void setModifiedBy(String modifiedBy) {
        this.modifiedBy = modifiedBy;
    }

    public String getModifiedDate() {
        return modifiedDate;
    }

    public void setModifiedDate(String modifiedDate) {
        this.modifiedDate = modifiedDate;
    }

    public Product getProduct() {
        return product;
    }

    public void setProduct(Product product) {
        this.product = product;
    }

    public RefLossCause getRefLossCause() {
        return refLossCause;
    }

    public void setRefLossCause(RefLossCause refLossCause) {
        this.refLossCause = refLossCause;
    }
}
