package com.infoinstech.claimsmodule.domain.model.underwriting.master;


import javax.persistence.Column;
import javax.persistence.Embeddable;
import java.io.Serializable;

@Embeddable
public class ProductLossCausePK implements Serializable {

    @Column(name = "PCL_PROD_CODE")
    private String productCode;

    @Column(name = "PCL_CODE")
    private String lossCode;

    public ProductLossCausePK() {
    }

    public ProductLossCausePK(String productCode, String lossCode) {
        this.productCode = productCode;
        this.lossCode = lossCode;
    }

    public String getProductCode() {
        return productCode;
    }

    public void setProductCode(String productCode) {
        this.productCode = productCode;
    }

    public String getLossCode() {
        return lossCode;
    }

    public void setLossCode(String lossCode) {
        this.lossCode = lossCode;
    }
}
