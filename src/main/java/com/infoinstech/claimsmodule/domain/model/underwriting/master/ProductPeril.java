package com.infoinstech.claimsmodule.domain.model.underwriting.master;

import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.Table;
import java.util.Date;

@Entity
@Table(name = "UW_M_PROD_PERILS")
public class ProductPeril {

    @EmbeddedId
    private ProductPerilPK productPerilPK;

    @Column(name = "PPL_PRINTED")
    private Character printed;

    @Column(name = "PPL_NO_UNITS")
    private Double noOfUnits;

    @Column(name = "PPL_VALUE_PER_UNIT")
    private Double valuePerUnit;

    @Column(name = "PPL_PERCENTAGE")
    private String percentage;

    @Column(name = "PPL_RATE")
    private String rate;

    @Column(name = "PPL_EXCESS")
    private Double excess;

    @Column(name = "PPL_EVENT_LIMIT")
    private Double eventLimit;

    @Column(name = "PPL_ANNUAL_LIMIT")
    private Double annualLimit;

    @Column(name = "PPL_P_SEQ")
    private String sequence;

    @Column(name = "PPL_PER_PRL_CODE")
    private String perilCode;

    @Column(name = "CREATED_BY")
    private String createdBy;

    @Column(name = "CREATED_DATE")
    private Date createdDate;

    @Column(name = "MODIFIED_BY")
    private String modifiedBy;

    @Column(name = "MODIFIED_DATE")
    private Date modifiedDate;

    @Column(name = "PPL_ALL_INCLUSIVE")
    private Character allInclusive;

    @Column(name = "PPL_PER_CLA_CODE")
    private String classCode;

    @Column(name = "PPL_CALC_METHOD")
    private Character calculationMethod;

    @Column(name = "PPL_SUB_PERILS")
    private Character subPerils;

    @Column(name = "PPL_CAUSE_OF_LOSS")
    private Character causeOfLoss;

    @Column(name = "PPL_POL_FORMULA")
    private String policyFormula;

    @Column(name = "PPL_EVENT_LIMIT_REQ")
    private Character eventLimitRequired;

    @Column(name = "PPL_ANNUAL_LIMIT_REQ")
    private Character annualLimitRequired;

    @Column(name = "PPL_NO_UNITS_REQ")
    private Character noOfUnitsRequired;

    @Column(name = "PPL_VALUE_PER_UNIT_REQ")
    private Character valuePerUnitRequired;

    @Column(name = "PPL_PERCENTAGE_REQ")
    private Character percentageRequired;

    @Column(name = "PPL_STATUS")
    private Character status;

    @Column(name = "PPL_RI_SUM_INSURED")
    private Character reinsuredSumInsured;
    @Column(name = "PPL_QOT_FORMULA")
    private Character quotationFormula;

    @Column(name = "PPL_CL_REDUCE_ANNUAL_LIMIT")
    private Character claimReducedAnnualLimit;

    @Column(name = "PPL_PSC_SEQ_NO")
    private String sectionSeqNo;

    @Column(name = "PPL_PSC_SEC_CODE")
    private String sectionCode;

    @Column(name = "PPL_VALUE_PER_UNIT_DESC")
    private String valuePerUnitDescription;

    @Column(name = "PPL_NO_UNITS_DESC")
    private String noOfUnitsDescription;

    @Column(name = "PPL_EVENT_LIMIT_DESC")
    private String eventLimitDescription;

    @Column(name = "PPL_ANNUAL_LIMIT_DESC")
    private String annualLimitDescription;

    @Column(name = "PPL_EXCESS_DESC")
    private String excessDescription;

    @Column(name = "PPL_EXCESS_PERCENTAGE_DESC")
    private String excessPercentageDescription;

    @Column(name = "PPL_PCL_SEQ_NO")
    private String pclSeqNo;

    @Column(name = "PPL_SCHEDULE_LINK")
    private String scheduleLink;

    @Column(name = "PPL_MIN_PERCENTAGE")
    private Double minimumPercentage;

    @Column(name = "PPL_MIN_RATE")
    private Double minimumRate;

    @Column(name = "PPL_EXCESS_NARR_REQ")
    private Character excessNarrativeRequired;

    @Column(name = "PPL_LIMIT_NARR_REQ")
    private Character limitNarrativeRequired;

    @Column(name = "PPL_EXCESS_NARR_DESC")
    private String excessNarrativeDescription;

    @Column(name = "PPL_LIMIT_NARR_DESC")
    private String limitNarrativeDescription;

    @Column(name = "PPL_PRINT_DESC_SCHD")
    private String printDescriptionScheduled;

    @Column(name = "PPL_EXCESS_AMT")
    private Double excessAmount;

    @Column(name = "PPL_EXCESS_PERCENTAGE_AMT")
    private Double excessPercentageAmount;

    @Column(name = "PPL_CLAIM_AFTER")
    private Double claimAfter;

    @Column(name = "PPL_NCB_FLAG")
    private Character ncbFlag;
//
//    @Column(name="PPL_UPFRONT_PERCENTAGE")
//    private Double upFrontPercentage;

//    @OneToOne
//    @JoinColumns( { @JoinColumn(name = "PPL_PER_CLA_CODE", referencedColumnName = "PER_CLA_CODE", insertable = false, updatable = false),
//                    @JoinColumn(name = "PPL_PER_PRL_CODE", referencedColumnName = "PER_PRL_CODE", insertable = false, updatable = false)})
//    private ClassPeril classPeril;


    public ProductPerilPK getProductPerilPK() {
        return productPerilPK;
    }

    public void setProductPerilPK(ProductPerilPK productPerilPK) {
        this.productPerilPK = productPerilPK;
    }

    public Character getPrinted() {
        return printed;
    }

    public void setPrinted(Character printed) {
        this.printed = printed;
    }

    public Double getNoOfUnits() {
        return noOfUnits;
    }

    public void setNoOfUnits(Double noOfUnits) {
        this.noOfUnits = noOfUnits;
    }

    public Double getValuePerUnit() {
        return valuePerUnit;
    }

    public void setValuePerUnit(Double valuePerUnit) {
        this.valuePerUnit = valuePerUnit;
    }

    public String getPercentage() {
        return percentage;
    }

    public void setPercentage(String percentage) {
        this.percentage = percentage;
    }

    public String getRate() {
        return rate;
    }

    public void setRate(String rate) {
        this.rate = rate;
    }

    public Double getExcess() {
        return excess;
    }

    public void setExcess(Double excess) {
        this.excess = excess;
    }

    public Double getEventLimit() {
        return eventLimit;
    }

    public void setEventLimit(Double eventLimit) {
        this.eventLimit = eventLimit;
    }

    public Double getAnnualLimit() {
        return annualLimit;
    }

    public void setAnnualLimit(Double annualLimit) {
        this.annualLimit = annualLimit;
    }

    public String getSequence() {
        return sequence;
    }

    public void setSequence(String sequence) {
        this.sequence = sequence;
    }

    public String getPerilCode() {
        return perilCode;
    }

    public void setPerilCode(String perilCode) {
        this.perilCode = perilCode;
    }

    public String getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(String createdBy) {
        this.createdBy = createdBy;
    }

    public Date getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(Date createdDate) {
        this.createdDate = createdDate;
    }

    public String getModifiedBy() {
        return modifiedBy;
    }

    public void setModifiedBy(String modifiedBy) {
        this.modifiedBy = modifiedBy;
    }

    public Date getModifiedDate() {
        return modifiedDate;
    }

    public void setModifiedDate(Date modifiedDate) {
        this.modifiedDate = modifiedDate;
    }

    public Character getAllInclusive() {
        return allInclusive;
    }

    public void setAllInclusive(Character allInclusive) {
        this.allInclusive = allInclusive;
    }

    public String getClassCode() {
        return classCode;
    }

    public void setClassCode(String classCode) {
        this.classCode = classCode;
    }

    public Character getCalculationMethod() {
        return calculationMethod;
    }

    public void setCalculationMethod(Character calculationMethod) {
        this.calculationMethod = calculationMethod;
    }

    public Character getSubPerils() {
        return subPerils;
    }

    public void setSubPerils(Character subPerils) {
        this.subPerils = subPerils;
    }

    public Character getCauseOfLoss() {
        return causeOfLoss;
    }

    public void setCauseOfLoss(Character causeOfLoss) {
        this.causeOfLoss = causeOfLoss;
    }

    public String getPolicyFormula() {
        return policyFormula;
    }

    public void setPolicyFormula(String policyFormula) {
        this.policyFormula = policyFormula;
    }

    public Character getEventLimitRequired() {
        return eventLimitRequired;
    }

    public void setEventLimitRequired(Character eventLimitRequired) {
        this.eventLimitRequired = eventLimitRequired;
    }

    public Character getAnnualLimitRequired() {
        return annualLimitRequired;
    }

    public void setAnnualLimitRequired(Character annualLimitRequired) {
        this.annualLimitRequired = annualLimitRequired;
    }

    public Character getNoOfUnitsRequired() {
        return noOfUnitsRequired;
    }

    public void setNoOfUnitsRequired(Character noOfUnitsRequired) {
        this.noOfUnitsRequired = noOfUnitsRequired;
    }

    public Character getValuePerUnitRequired() {
        return valuePerUnitRequired;
    }

    public void setValuePerUnitRequired(Character valuePerUnitRequired) {
        this.valuePerUnitRequired = valuePerUnitRequired;
    }

    public Character getPercentageRequired() {
        return percentageRequired;
    }

    public void setPercentageRequired(Character percentageRequired) {
        this.percentageRequired = percentageRequired;
    }

    public Character getStatus() {
        return status;
    }

    public void setStatus(Character status) {
        this.status = status;
    }

    public Character getReinsuredSumInsured() {
        return reinsuredSumInsured;
    }

    public void setReinsuredSumInsured(Character reinsuredSumInsured) {
        this.reinsuredSumInsured = reinsuredSumInsured;
    }

    public Character getQuotationFormula() {
        return quotationFormula;
    }

    public void setQuotationFormula(Character quotationFormula) {
        this.quotationFormula = quotationFormula;
    }

    public Character getClaimReducedAnnualLimit() {
        return claimReducedAnnualLimit;
    }

    public void setClaimReducedAnnualLimit(Character claimReducedAnnualLimit) {
        this.claimReducedAnnualLimit = claimReducedAnnualLimit;
    }

    public String getSectionSeqNo() {
        return sectionSeqNo;
    }

    public void setSectionSeqNo(String sectionSeqNo) {
        this.sectionSeqNo = sectionSeqNo;
    }

    public String getSectionCode() {
        return sectionCode;
    }

    public void setSectionCode(String sectionCode) {
        this.sectionCode = sectionCode;
    }

    public String getValuePerUnitDescription() {
        return valuePerUnitDescription;
    }

    public void setValuePerUnitDescription(String valuePerUnitDescription) {
        this.valuePerUnitDescription = valuePerUnitDescription;
    }

    public String getNoOfUnitsDescription() {
        return noOfUnitsDescription;
    }

    public void setNoOfUnitsDescription(String noOfUnitsDescription) {
        this.noOfUnitsDescription = noOfUnitsDescription;
    }

    public String getEventLimitDescription() {
        return eventLimitDescription;
    }

    public void setEventLimitDescription(String eventLimitDescription) {
        this.eventLimitDescription = eventLimitDescription;
    }

    public String getAnnualLimitDescription() {
        return annualLimitDescription;
    }

    public void setAnnualLimitDescription(String annualLimitDescription) {
        this.annualLimitDescription = annualLimitDescription;
    }

    public String getExcessDescription() {
        return excessDescription;
    }

    public void setExcessDescription(String excessDescription) {
        this.excessDescription = excessDescription;
    }

    public String getExcessPercentageDescription() {
        return excessPercentageDescription;
    }

    public void setExcessPercentageDescription(String excessPercentageDescription) {
        this.excessPercentageDescription = excessPercentageDescription;
    }

    public String getPclSeqNo() {
        return pclSeqNo;
    }

    public void setPclSeqNo(String pclSeqNo) {
        this.pclSeqNo = pclSeqNo;
    }

    public String getScheduleLink() {
        return scheduleLink;
    }

    public void setScheduleLink(String scheduleLink) {
        this.scheduleLink = scheduleLink;
    }

    public Double getMinimumPercentage() {
        return minimumPercentage;
    }

    public void setMinimumPercentage(Double minimumPercentage) {
        this.minimumPercentage = minimumPercentage;
    }

    public Double getMinimumRate() {
        return minimumRate;
    }

    public void setMinimumRate(Double minimumRate) {
        this.minimumRate = minimumRate;
    }

    public Character getExcessNarrativeRequired() {
        return excessNarrativeRequired;
    }

    public void setExcessNarrativeRequired(Character excessNarrativeRequired) {
        this.excessNarrativeRequired = excessNarrativeRequired;
    }

    public Character getLimitNarrativeRequired() {
        return limitNarrativeRequired;
    }

    public void setLimitNarrativeRequired(Character limitNarrativeRequired) {
        this.limitNarrativeRequired = limitNarrativeRequired;
    }

    public String getExcessNarrativeDescription() {
        return excessNarrativeDescription;
    }

    public void setExcessNarrativeDescription(String excessNarrativeDescription) {
        this.excessNarrativeDescription = excessNarrativeDescription;
    }

    public String getLimitNarrativeDescription() {
        return limitNarrativeDescription;
    }

    public void setLimitNarrativeDescription(String limitNarrativeDescription) {
        this.limitNarrativeDescription = limitNarrativeDescription;
    }

    public String getPrintDescriptionScheduled() {
        return printDescriptionScheduled;
    }

    public void setPrintDescriptionScheduled(String printDescriptionScheduled) {
        this.printDescriptionScheduled = printDescriptionScheduled;
    }

    public Double getExcessAmount() {
        return excessAmount;
    }

    public void setExcessAmount(Double excessAmount) {
        this.excessAmount = excessAmount;
    }

    public Double getExcessPercentageAmount() {
        return excessPercentageAmount;
    }

    public void setExcessPercentageAmount(Double excessPercentageAmount) {
        this.excessPercentageAmount = excessPercentageAmount;
    }

    public Double getClaimAfter() {
        return claimAfter;
    }

    public void setClaimAfter(Double claimAfter) {
        this.claimAfter = claimAfter;
    }

    public Character getNcbFlag() {
        return ncbFlag;
    }

    public void setNcbFlag(Character ncbFlag) {
        this.ncbFlag = ncbFlag;
    }

//    public Double getUpFrontPercentage() {
//        return upFrontPercentage;
//    }
//
//    public void setUpFrontPercentage(Double upFrontPercentage) {
//        this.upFrontPercentage = upFrontPercentage;
//    }
}
