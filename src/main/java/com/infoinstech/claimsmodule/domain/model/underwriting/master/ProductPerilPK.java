package com.infoinstech.claimsmodule.domain.model.underwriting.master;

import javax.persistence.Column;
import javax.persistence.Embeddable;
import java.io.Serializable;

@Embeddable
public class ProductPerilPK implements Serializable {

    @Column(name = "PPL_SEQ_NO")
    private String sequenceNo;

    @Column(name = "PPL_PRC_SEQ_NO")
    private String productClassSequenceNo;

    @Column(name = "PPL_PRC_PRD_CODE")
    private String productCode;

    public ProductPerilPK() {
    }

    public ProductPerilPK(String sequenceNo, String productClassSequenceNo, String productCode) {
        this.sequenceNo = sequenceNo;
        this.productClassSequenceNo = productClassSequenceNo;
        this.productCode = productCode;
    }

    public String getProductCode() {
        return productCode;
    }

    public void setProductCode(String productCode) {
        this.productCode = productCode;
    }

    public String getSequenceNo() {
        return sequenceNo;
    }

    public void setSequenceNo(String sequenceNo) {
        this.sequenceNo = sequenceNo;
    }

    public String getProductClassSequenceNo() {
        return productClassSequenceNo;
    }

    public void setProductClassSequenceNo(String productClassSequenceNo) {
        this.productClassSequenceNo = productClassSequenceNo;
    }
}
