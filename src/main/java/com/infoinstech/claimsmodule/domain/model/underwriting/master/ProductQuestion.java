package com.infoinstech.claimsmodule.domain.model.underwriting.master;

import com.infoinstech.claimsmodule.domain.model.claims.reference.RefQuestion;

import javax.persistence.*;
import java.util.Date;

@Entity
@Table(name = "UW_M_PROD_QUESTIONS")
public class ProductQuestion {

    @Id
    @Column(name = "PQS_SEQ_NO")
    private String sequenceNo;

    @Column(name = "PQS_QUE_CODE")
    private String questionCode;

    @Column(name = "PQS_PRD_CODE")
    private String productCode;

    @Column(name = "PQS_STATUS")
    private Character status;

    @Column(name = "CREATED_BY")
    private String createdBy;

    @Column(name = "CREATED_DATE")
    private Date createdDate;

    @Column(name = "MODIFIED_BY")
    private String modifiedBy;

    @Column(name = "MODIFIED_DATE")
    private Date modifiedDate;

    @OneToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "PQS_QUE_CODE", referencedColumnName = "QUE_CODE", insertable = false, updatable = false)
    private RefQuestion refQuestion;

    public String getSequenceNo() {
        return sequenceNo;
    }

    public void setSequenceNo(String sequenceNo) {
        this.sequenceNo = sequenceNo;
    }

    public String getQuestionCode() {
        return questionCode;
    }

    public void setQuestionCode(String questionCode) {
        this.questionCode = questionCode;
    }

    public String getProductCode() {
        return productCode;
    }

    public void setProductCode(String productCode) {
        this.productCode = productCode;
    }

    public Character getStatus() {
        return status;
    }

    public void setStatus(Character status) {
        this.status = status;
    }

    public String getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(String createdBy) {
        this.createdBy = createdBy;
    }

    public Date getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(Date createdDate) {
        this.createdDate = createdDate;
    }

    public String getModifiedBy() {
        return modifiedBy;
    }

    public void setModifiedBy(String modifiedBy) {
        this.modifiedBy = modifiedBy;
    }

    public Date getModifiedDate() {
        return modifiedDate;
    }

    public void setModifiedDate(Date modifiedDate) {
        this.modifiedDate = modifiedDate;
    }

    public RefQuestion getRefQuestion() {
        return refQuestion;
    }

    public void setRefQuestion(RefQuestion refQuestion) {
        this.refQuestion = refQuestion;
    }
}
