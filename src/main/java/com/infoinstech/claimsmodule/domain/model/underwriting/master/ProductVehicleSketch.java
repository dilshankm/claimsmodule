package com.infoinstech.claimsmodule.domain.model.underwriting.master;

import com.infoinstech.claimsmodule.domain.model.Auditable;
import com.infoinstech.claimsmodule.domain.model.claims.reference.RefVehicleSketch;
import lombok.Data;

import javax.persistence.*;

/**
 * Created by thilina on 22/12/17.
 */
@Data
@Entity
@Table(name = "UW_M_PROD_SKETCHES")
public class ProductVehicleSketch extends Auditable<String> {

    @Id
    @Column(name = "PSK_SQE_NO")
    private String seqNo;

    @Column(name = "PSK_PRD_CODE")
    private String productCode;

    @Column(name = "PSK_SKS_CODE")
    private String refVehicleSketchCode;

    @OneToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "PSK_SKS_CODE", referencedColumnName = "SKS_CODE", insertable = false, updatable = false)
    private RefVehicleSketch refVehicleSketch;
}
