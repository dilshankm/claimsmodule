package com.infoinstech.claimsmodule.domain.model.underwriting.reference;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "UW_R_CLASSES")
public class RefClass {
    @Id
    @Column(name = "CLA_CODE")
    private String code;

    @Column(name = "CLA_DESCRIPTION")
    private String description;

//    @Column(name = "CLA_PREFIX")
//    private String prefix;

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

//    public String getPrefix() {
//        return prefix;
//    }
//
//    public void setPrefix(String prefix) {
//        this.prefix = prefix;
//    }
}
