package com.infoinstech.claimsmodule.domain.model.underwriting.reference;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.util.Date;

@Entity
@Table(name = "UW_R_LOCATIONS")
public class RefLocation {

    @Id
    @Column(name = "LOC_CODE")
    private String code;

    @NotNull
    @Column(name = "LOC_DESCRIPTION")
    private String description;

    @Column(name = "LOC_DISTRICT")
    private String district;

    @Column(name = "LOC_PROVINCE")
    private String province;

    @Column(name = "LOC_POSTAL_CODE")
    private String postalCode;

    @NotNull
    @Column(name = "LOC_LTY_CODE")
    private String typeCode;

    @Column(name = "LOC_ACC_ACCUMULATE_CODE")
    private String accumilationCode;

    @Column(name = "LOC_CITY")
    private String city;

    @Column(name = "LOC_STREET")
    private String street;

    @Column(name = "LOC_BUILDING")
    private String building;

    @Column(name = "LOC_NUMBER")
    private String number;

    @Column(name = "LOC_EARTHQK")
    private String earthquackZone;

    @Column(name = "LOC_CYCLONE")
    private String cycloneZone;

    @Column(name = "LOC_CHANGE_STATUS")
    private Character changeStatus;

    @Column(name = "CREATED_BY")
    private String createdBy;

    @Column(name = "CREATED_DATE")
    private Date createdDate;

    @Column(name = "MODIFIED_BY")
    private String modifiedBy;

    @Column(name = "MODIFIED_DATE")
    private Date modifiedDate;

    @OneToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "LOC_LTY_CODE", referencedColumnName = "LTY_CODE", insertable = false, updatable = false)
    private RefLocationType refLocationType;

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getDistrict() {
        return district;
    }

    public void setDistrict(String district) {
        this.district = district;
    }

    public String getProvince() {
        return province;
    }

    public void setProvince(String province) {
        this.province = province;
    }

    public String getPostalCode() {
        return postalCode;
    }

    public void setPostalCode(String postalCode) {
        this.postalCode = postalCode;
    }

    public String getTypeCode() {
        return typeCode;
    }

    public void setTypeCode(String typeCode) {
        this.typeCode = typeCode;
    }

    public String getAccumilationCode() {
        return accumilationCode;
    }

    public void setAccumilationCode(String accumilationCode) {
        this.accumilationCode = accumilationCode;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getStreet() {
        return street;
    }

    public void setStreet(String street) {
        this.street = street;
    }

    public String getBuilding() {
        return building;
    }

    public void setBuilding(String building) {
        this.building = building;
    }

    public String getNumber() {
        return number;
    }

    public void setNumber(String number) {
        this.number = number;
    }

    public String getEarthquackZone() {
        return earthquackZone;
    }

    public void setEarthquackZone(String earthquackZone) {
        this.earthquackZone = earthquackZone;
    }

    public String getCycloneZone() {
        return cycloneZone;
    }

    public void setCycloneZone(String cycloneZone) {
        this.cycloneZone = cycloneZone;
    }

    public Character getChangeStatus() {
        return changeStatus;
    }

    public void setChangeStatus(Character changeStatus) {
        this.changeStatus = changeStatus;
    }

    public String getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(String createdBy) {
        this.createdBy = createdBy;
    }

    public Date getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(Date createdDate) {
        this.createdDate = createdDate;
    }

    public String getModifiedBy() {
        return modifiedBy;
    }

    public void setModifiedBy(String modifiedBy) {
        this.modifiedBy = modifiedBy;
    }

    public Date getModifiedDate() {
        return modifiedDate;
    }

    public void setModifiedDate(Date modifiedDate) {
        this.modifiedDate = modifiedDate;
    }

    public RefLocationType getRefLocationType() {
        return refLocationType;
    }

    public void setRefLocationType(RefLocationType refLocationType) {
        this.refLocationType = refLocationType;
    }
}

