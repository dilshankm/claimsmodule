package com.infoinstech.claimsmodule.domain.model.underwriting.reference;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "UW_R_PERILS")
public class RefPeril {
    @Id
    @Column(name = "PRL_CODE")
    private String code;

    @Column(name = "PRL_DESCRIPTION")
    private String description;

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }
}
