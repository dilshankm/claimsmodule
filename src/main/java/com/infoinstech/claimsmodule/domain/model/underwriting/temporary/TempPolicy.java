package com.infoinstech.claimsmodule.domain.model.underwriting.temporary;


import lombok.Data;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.validation.constraints.Size;
import java.util.Date;

@Data
@Entity
@Table(name = "UW_X_POLICIES")
public class TempPolicy {


    @Id
    @Column(name = "POL_SEQ_NO")
    private String sequenceNo;

    @Column(name = "POL_POL_SEQ_NO")
    private String policySequenceNo;

    @Column(name = "POL_TABLE")
    private String table;

    @Column(name = "POL_SESSION_ID")
    private String sessionId;

    @Column(name = "POL_PROPOSAL_NO")
    private String proposalNo;

    @Column(name = "POL_POLICY_NO")
    private String policyNo;

    @Column(name = "POL_CLA_CODE")
    private String classCode;

    @Column(name = "POL_SURVEY")
    private Character survey;

    @Column(name = "POL_OPEN_POLICY")
    private Character openPolicy;

    @Column(name = "POL_PRD_CODE")
    private String productCode;

    @Column(name = "POL_PERIOD_FROM")
    private Date periodFrom;

    @Column(name = "POL_PERIOD_TO")
    private Date periodTo;

    @Column(name = "POL_PREMIUM")
    private Double premium;

    @Column(name = "POL_QOT_SEQ_NO")
    private String quotationSequenceNo;

    @Column(name = "POL_CURRENCY")
    private String currency;

    @Column(name = "POL_CUS_CODE")
    private String customerCode;

    @Column(name = "POL_VOUCHER_NO")
    private String voucherNo;


    @Column(name = "POL_DATE")
    private Date policyDate;

    @Column(name = "POL_DAYS")
    private Integer policyDays;

    @Column(name = "POL_SUM_INSURED")
    private Double sumInsured;

    @Column(name = "POL_GROUP")
    private Character groupPolicy;

    @Column(name = "POL_CERTIFICATE")
    private Character certificate;

    @Column(name = "POL_CLAIMS_AFTER")
    private Integer claimsAfter;

    @Column(name = "POL_REMARKS")
    private String remarks;

    @Column(name = "POL_TARGET")
    private String target;

    @Column(name = "POL_NET_OF_COMMISSION")
    private Character netOfCommission;

    @Column(name = "POL_AUTHORIZED_BY")
    private String authorizedBy;

    @Column(name = "POL_AUTHORIZED_DATE")
    private Date authorizedDate;

    @Column(name = "POL_STATUS")
    private String status;

    @Column(name = "POL_CANCELLED_BY")
    private String cancelledBy;

    @Column(name = "POL_CANCELLED_DATE")
    private Date cancelledDate;

    @Column(name = "POL_COINSURANCE")
    private Character coinsurance;

    @Column(name = "POL_COINSURANCE_PCT")
    private Double coinsurancePercentage;

    @Column(name = "CREATED_BY")
    private String createdBy;

    @Column(name = "CREATED_DATE")
    private Date createdDate;

    @Column(name = "MODIFIED_BY")
    private String modifiedBy;

    @Column(name = "MODIFIED_DATE")
    private Date modifiedDate;

    @Column(name = "POL_MARKETING_EXECUTIVE_CODE")
    private String intermediaryCode;

    @Column(name = "POL_SLC_BRN_CODE")
    private String branchCode;

    @Column(name = "POL_ANY_ENDORSEMENTS")
    private Character anyEndorsements;

    @Column(name = "POL_LAST_ENDORSED_DATE")
    private Date lastEndorsedDate;

    @Column(name = "POL_ANY_HISTORY")
    private Character anyHistory;

    @Column(name = "POL_LAST_HISTORY_DATE")
    private Date lastHistoryDate;

    @Column(name = "POL_BCH_CODE")
    private String businessChannelCode;

    @Column(name = "POL_TRANSACTION_TYPE")
    private String transactionType;

    @Column(name = "POL_RI_TRANSFER_FLAG")
    private Character reinsuranceTransferFlag;

    @Column(name = "POL_RI_SUM_INSURED")
    private Double reinsuranceSumInsured;

    @Column(name = "POL_RC_OUTSTAND_AMT")
    private Double outstandingAmount;

    @Column(name = "POL_ENDORSEMENT_NO")
    private String endorsementNo;

    @Column(name = "POL_ENDORSEMENT_REMARKS")
    private String endorsementRemarks;

    @Column(name = "POL_RI_TREATY_DISTRIBUTED_FLAG")
    private Character reinsuranceTreatyDistributedFlag;

    @Column(name = "POL_NO_OF_COPIES")
    private Integer noOfCopies;

    @Column(name = "POL_TRANSACTION_AMOUNT")
    private Double transactionAmount;

    @Column(name = "POL_CALCULATION_TYPE")
    private String calculationType;

    @Column(name = "POL_PREV_POL_SEQ_NO")
    private String previousSequenceNo;

    @Column(name = "POL_REINSURANCE_REQ")
    private String reinsuranceRequired;

    @Column(name = "POL_UP_STEP_NO")
    private Integer uprStepNo;

    @Column(name = "POL_UP_TRANS_SEQ")
    private Integer uprTransactionSequenceNo;

    @Column(name = "POL_RC_OUTSTAND_CHQ_AMT")
    private Double outstandingChequeAmount;

    @Column(name = "POL_RC_RECEIPTING_DATE")
    private Date receiptingDate;

    @Column(name = "POL_RC_CURRENCY_RATE")
    private Double currencyRate;

    @Column(name = "POL_UP_NRP_PROCESSED")
    private String nrpprocessed;

    @Column(name = "POL_CANCELLED_TYPE")
    private Character cancelledType;

    @Column(name = "POL_FAMILY_UNIT")
    private Character familyUnit;

    @Column(name = "POL_BSS_BSS_CODE")
    private String intermediaryTypeCode;

    @Column(name = "POL_CDB_DEB_DEBTOR_CODE")
    private String debtorCode;

    @Column(name = "POL_CDB_DEB_PROFIT_CENTER")
    private String profitCenter;

    @Column(name = "POL_ANY_CLAIMS")
    private Character anyClaims;

    @Column(name = "POL_CLOSED_BY")
    private String closedBy;

    @Column(name = "POL_CLOSED_DATE")
    private Date closedDate;

    @Column(name = "POL_UP_OPEN_PERCENTAGE")
    private Double uprOpenPercentage;

    @Column(name = "POL_UP_PREM_ADV_PROCESSED")
    private Character premiumAdvancedProcessed;

    @Column(name = "POL_RC_PROCESSED")
    private Character processed;

    @Column(name = "POL_TOTAL_PREMIUM")
    private Double totalPremium;

    @Column(name = "POL_TOTAL_TRANSACTION_AMOUNT")
    private Double totalTransactionAmount;

    @Column(name = "POL_CANCELLED_AMOUNT")
    private Double cancelledAmount;

    @Column(name = "POL_EFFECTIVE_START_DATE")
    private Date effectiveStartDate;

    @Column(name = "POL_UP_CANCELLED_PROCESS")
    private Character uprCancelledProcess;

    @Column(name = "POL_CANCEL_EFFECTIVE_DATE")
    private Date cancelEffectiveDate;

    @Column(name = "POL_RC_CANCELLED_DATE")
    private Date receiptingCancelledDate;

    @Column(name = "POL_RC_CANCELLED_CURR_RATE")
    private Double receiptingCancelledCurrencyRate;

    @Column(name = "POL_COMM_AMMENDED")
    private Character commissionAmended;

    @Column(name = "POL_CREATED_BY")
    private String policyCreatedBy;

    @Column(name = "POL_CREATED_DATE")
    private Date policyCreatedDate;

    @Column(name = "POL_CANCELLED_TRANSACTION_AMT")
    private Double cancelledTransactionAmount;

    @Column(name = "POL_COMM_AMMENDED_DATE")
    private Date commissionAmendedDate;

    @Column(name = "POL_COMM_AMMEND_EFFECTIVE_DATE")
    private Date commissionAmendedEffectiveDate;

    @Column(name = "POL_REL_POLICY_NO")
    private String relatedPolicyNo;

    @Column(name = "POL_ADR_SEQ_NO")
    private String customerAddressSequenceNo;

    @Column(name = "POL_CUS_REMARKS")
    private String customerRemarks;

    @Column(name = "POL_PAYMENT_CAT")
    private String paymentCategory;

    @Column(name = "POL_ART_CODE")
    private String arrangementType;

    @Column(name = "POL_EXAMINED_BY")
    private String examinedBy;

    @Column(name = "POL_EXAMINED_DATE")
    private Date examinedDate;

    @Column(name = "POL_QUICK_UW")
    private Character quickUnderwriting;

    @Column(name = "POL_MARK_CANCELLED_BY")
    private String markCancelledBy;

    @Column(name = "POL_MARK_CANCELLED_DATE")
    private Date markCancelledDate;

    @Column(name = "POL_EXMD_CANCELLED_BY")
    private String examinedCancelledBy;

    @Column(name = "POL_EXMD_CANCELLED_DATE")
    private Date examinedCancelledDate;

    @Size(min = 0, max = 10)
    @Column(name = "POL_BPARTY_CODE")
    private String bPartyCode;
}
