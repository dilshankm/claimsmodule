package com.infoinstech.claimsmodule.domain.model.underwriting.temporary;

import com.infoinstech.claimsmodule.domain.model.claims.reference.RefExcessType;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.util.Date;

@Entity
@Table(name = "UW_X_POL_EXCESS_TYPES")
public class TempPolicyExcessType {

    @Id
    @Column(name = "PET_SEQ_NO")
    private String sequenceNo;

    @Column(name = "PET_PRS_PLC_POL_SEQ_NO")
    private String policySequenceNo;

    @Column(name = "PET_PRS_PLC_SEQ_NO")
    private String locationSequenceNo;

    @Column(name = "PET_PRS_SEQ_NO")
    private String riskSequenceNo;

    @Column(name = "CREATED_BY")
    private String createdBy;

    @Column(name = "CREATED_DATE")
    private Date createdDate;

    @Column(name = "MODIFIED_BY")
    private String modifiedBy;

    @Column(name = "MODIFIED_DATE")
    private Date modifiedDate;

    @NotNull
    @Column(name = "PET_EXC_CODE")
    private String excessCode;

    @Column(name = "PET_EXC_PERCENTAGE")
    private Double excessPercentage;

    @Column(name = "PET_EXC_RATE")
    private Double excessRate;

    @OneToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "PET_EXC_CODE", referencedColumnName = "EXC_CODE", insertable = false, updatable = false)
    private RefExcessType refExcessType;

    public String getSequenceNo() {
        return sequenceNo;
    }

    public void setSequenceNo(String sequenceNo) {
        this.sequenceNo = sequenceNo;
    }

    public String getPolicySequenceNo() {
        return policySequenceNo;
    }

    public void setPolicySequenceNo(String policySequenceNo) {
        this.policySequenceNo = policySequenceNo;
    }

    public String getLocationSequenceNo() {
        return locationSequenceNo;
    }

    public void setLocationSequenceNo(String locationSequenceNo) {
        this.locationSequenceNo = locationSequenceNo;
    }

    public String getRiskSequenceNo() {
        return riskSequenceNo;
    }

    public void setRiskSequenceNo(String riskSequenceNo) {
        this.riskSequenceNo = riskSequenceNo;
    }

    public String getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(String createdBy) {
        this.createdBy = createdBy;
    }

    public Date getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(Date createdDate) {
        this.createdDate = createdDate;
    }

    public String getModifiedBy() {
        return modifiedBy;
    }

    public void setModifiedBy(String modifiedBy) {
        this.modifiedBy = modifiedBy;
    }

    public Date getModifiedDate() {
        return modifiedDate;
    }

    public void setModifiedDate(Date modifiedDate) {
        this.modifiedDate = modifiedDate;
    }

    public String getExcessCode() {
        return excessCode;
    }

    public void setExcessCode(String excessCode) {
        this.excessCode = excessCode;
    }

    public Double getExcessPercentage() {
        return excessPercentage;
    }

    public void setExcessPercentage(Double excessPercentage) {
        this.excessPercentage = excessPercentage;
    }

    public Double getExcessRate() {
        return excessRate;
    }

    public void setExcessRate(Double excessRate) {
        this.excessRate = excessRate;
    }

    public RefExcessType getRefExcessType() {
        return refExcessType;
    }

    public void setRefExcessType(RefExcessType refExcessType) {
        this.refExcessType = refExcessType;
    }
}
