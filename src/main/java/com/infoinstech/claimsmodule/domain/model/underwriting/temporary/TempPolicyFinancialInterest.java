package com.infoinstech.claimsmodule.domain.model.underwriting.temporary;

import com.infoinstech.claimsmodule.domain.model.underwriting.reference.RefFinancialInterest;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.util.Date;

@Entity
@Table(name = "UW_X_POL_FIN_INTEREST")
public class TempPolicyFinancialInterest {

    @EmbeddedId
    private TempPolicyFinancialInterestPK tempPolicyFinancialInterestPK;

    @Column(name = "PFI_FIN_CODE")
    private String interestCode;

    @NotNull
    @Column(name = "PFI_COVERED_AMOUNT")
    private Double coveredAmount;

    @NotNull
    @Column(name = "PFI_SUM_INSURED")
    private Double sumInsured;

    @Column(name = "CREATED_BY")
    private String createdBy;

    @Column(name = "CREATED_DATE")
    private Date createdDate;

    @Column(name = "MODIFIED_BY")
    private String modifiedBy;

    @Column(name = "MODIFIED_DATE")
    private Date modifiedDate;

    @OneToOne
    @JoinColumn(name = "PFI_FIN_CODE", referencedColumnName = "FIN_CODE", insertable = false, updatable = false)
    private RefFinancialInterest refFinancialInterest;


    public TempPolicyFinancialInterestPK getTempPolicyFinancialInterestPK() {
        return tempPolicyFinancialInterestPK;
    }

    public void setTempPolicyFinancialInterestPK(TempPolicyFinancialInterestPK tempPolicyFinancialInterestPK) {
        this.tempPolicyFinancialInterestPK = tempPolicyFinancialInterestPK;
    }

    public String getInterestCode() {
        return interestCode;
    }

    public void setInterestCode(String interestCode) {
        this.interestCode = interestCode;
    }

    public Double getCoveredAmount() {
        return coveredAmount;
    }

    public void setCoveredAmount(Double coveredAmount) {
        this.coveredAmount = coveredAmount;
    }

    public Double getSumInsured() {
        return sumInsured;
    }

    public void setSumInsured(Double sumInsured) {
        this.sumInsured = sumInsured;
    }

    public String getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(String createdBy) {
        this.createdBy = createdBy;
    }

    public Date getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(Date createdDate) {
        this.createdDate = createdDate;
    }

    public String getModifiedBy() {
        return modifiedBy;
    }

    public void setModifiedBy(String modifiedBy) {
        this.modifiedBy = modifiedBy;
    }

    public Date getModifiedDate() {
        return modifiedDate;
    }

    public void setModifiedDate(Date modifiedDate) {
        this.modifiedDate = modifiedDate;
    }

    public RefFinancialInterest getRefFinancialInterest() {
        return refFinancialInterest;
    }

    public void setRefFinancialInterest(RefFinancialInterest refFinancialInterest) {
        this.refFinancialInterest = refFinancialInterest;
    }
}
