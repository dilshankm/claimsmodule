package com.infoinstech.claimsmodule.domain.model.underwriting.temporary;

import javax.persistence.Column;
import javax.persistence.Embeddable;
import java.io.Serializable;

@Embeddable
public class TempPolicyFinancialInterestPK implements Serializable {

    @Column(name = "PFI_SEQ_NO")
    private String sequenceNo;

    @Column(name = "PFI_POL_SEQ_NO")
    private String policySequenceNo;

    public TempPolicyFinancialInterestPK() {
    }

    public TempPolicyFinancialInterestPK(String sequenceNo, String policySequenceNo) {
        this.sequenceNo = sequenceNo;
        this.policySequenceNo = policySequenceNo;
    }

    public String getSequenceNo() {
        return sequenceNo;
    }

    public void setSequenceNo(String sequenceNo) {
        this.sequenceNo = sequenceNo;
    }

    public String getPolicySequenceNo() {
        return policySequenceNo;
    }

    public void setPolicySequenceNo(String policySequenceNo) {
        this.policySequenceNo = policySequenceNo;
    }
}
