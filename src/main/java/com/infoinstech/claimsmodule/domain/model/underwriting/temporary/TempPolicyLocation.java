package com.infoinstech.claimsmodule.domain.model.underwriting.temporary;

import javax.persistence.*;
import java.util.Date;

@Entity
@Table(name = "UW_X_POL_LOCATIONS")
public class TempPolicyLocation {

    @EmbeddedId
    private TempPolicyLocationPK tempPolicyLocationPK;

    @Column(name = "PLC_DATE_TIME")
    private Date dateAndTime;

    @Column(name = "PLC_LOC_CODE")
    private String locationCode;

    @Column(name = "CREATED_BY")
    private String createdBy;

    @Column(name = "CREATED_DATE")
    private Date createdDate;

    @Column(name = "MODIFIED_BY")
    private String modifiedBy;

    @Column(name = "MODIFIED_DATE")
    private Date modifiedDate;

    @Column(name = "PLC_RISK_NEXT")
    private Integer riskNext;

    @Column(name = "PLC_SUM_INSURED")
    private Double sumInsured;

    @Column(name = "PLC_PREMIUM")
    private Double premium;

    @Column(name = "PLC_NEW_LOC")
    private Character newLocation;

    @Column(name = "PLC_EVENT_LIMIT")
    private Double eventLimit;

    @Column(name = "PLC_ANNUAL_LIMIT")
    private Double annualLimit;

    @Column(name = "PLC_EXCESS_TXT")
    private String excessText;

    @Column(name = "PLC_LIMIT_TXT")
    private String limitText;

    @Column(name = "PLC_LOC_DESCRIPTION")
    private String locationDescription;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "PLC_POL_SEQ_NO", referencedColumnName = "POL_SEQ_NO", insertable = false, updatable = false)
    private TempPolicy tempPolicy;

    public TempPolicyLocationPK getTempPolicyLocationPK() {
        return tempPolicyLocationPK;
    }

    public void setTempPolicyLocationPK(TempPolicyLocationPK tempPolicyLocationPK) {
        this.tempPolicyLocationPK = tempPolicyLocationPK;
    }

    public Date getDateAndTime() {
        return dateAndTime;
    }

    public void setDateAndTime(Date dateAndTime) {
        this.dateAndTime = dateAndTime;
    }

    public String getLocationCode() {
        return locationCode;
    }

    public void setLocationCode(String locationCode) {
        this.locationCode = locationCode;
    }

    public String getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(String createdBy) {
        this.createdBy = createdBy;
    }

    public Date getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(Date createdDate) {
        this.createdDate = createdDate;
    }

    public String getModifiedBy() {
        return modifiedBy;
    }

    public void setModifiedBy(String modifiedBy) {
        this.modifiedBy = modifiedBy;
    }

    public Date getModifiedDate() {
        return modifiedDate;
    }

    public void setModifiedDate(Date modifiedDate) {
        this.modifiedDate = modifiedDate;
    }

    public Integer getRiskNext() {
        return riskNext;
    }

    public void setRiskNext(Integer riskNext) {
        this.riskNext = riskNext;
    }

    public Double getSumInsured() {
        return sumInsured;
    }

    public void setSumInsured(Double sumInsured) {
        this.sumInsured = sumInsured;
    }

    public Double getPremium() {
        return premium;
    }

    public void setPremium(Double premium) {
        this.premium = premium;
    }

    public Character getNewLocation() {
        return newLocation;
    }

    public void setNewLocation(Character newLocation) {
        this.newLocation = newLocation;
    }

    public Double getEventLimit() {
        return eventLimit;
    }

    public void setEventLimit(Double eventLimit) {
        this.eventLimit = eventLimit;
    }

    public Double getAnnualLimit() {
        return annualLimit;
    }

    public void setAnnualLimit(Double annualLimit) {
        this.annualLimit = annualLimit;
    }

    public String getExcessText() {
        return excessText;
    }

    public void setExcessText(String excessText) {
        this.excessText = excessText;
    }

    public String getLimitText() {
        return limitText;
    }

    public void setLimitText(String limitText) {
        this.limitText = limitText;
    }

    public String getLocationDescription() {
        return locationDescription;
    }

    public void setLocationDescription(String locationDescription) {
        this.locationDescription = locationDescription;
    }

    public TempPolicy getTempPolicy() {
        return tempPolicy;
    }

    public void setTempPolicy(TempPolicy tempPolicy) {
        this.tempPolicy = tempPolicy;
    }
}

