package com.infoinstech.claimsmodule.domain.model.underwriting.temporary;

import com.infoinstech.claimsmodule.domain.model.underwriting.reference.RefPeril;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.util.Date;

@Entity
@Table(name = "UW_X_POL_PERILS")
public class TempPolicyPeril {

    @EmbeddedId
    private TempPolicyPerilPK tempPolicyPerilPK;

    @Column(name = "PPR_SUM_INSURED")
    private Double perilSumInsured;

    @Column(name = "PPR_PERCENTAGE")
    private Double percentage;

    @Column(name = "PPR_RATE")
    private Double rate;

    @Column(name = "PPR_EXCESS")
    private Double excess;

    @Column(name = "PPR_PER_UNIT")
    private Double perUnit;

    @Column(name = "PPR_NO_UNITS")
    private Integer noOfUnits;

    @Column(name = "PPR_DISCOUNT_PERCENTAGE")
    private Double discountPercentage;

    @Column(name = "PPR_DISCOUNT_RATE")
    private Double discountRate;

    @Column(name = "PPR_EVENT_LIMIT")
    private Double eventLimit;

    @Column(name = "PPR_ANNUAL_LIMIT")
    private Double annualLimit;

    @Column(name = "PPR_PREMIUM")
    private Double premium;

    @Column(name = "PPR_REMARKS")
    private String remarks;

    @NotNull
    @Column(name = "PPR_PER_PRL_CODE")
    private String perilCode;

    @Column(name = "CREATED_BY")
    private String createdBy;

    @Column(name = "CREATED_DATE")
    private Date createdDate;

    @Column(name = "MODIFIED_BY")
    private String modifiedBy;

    @Column(name = "MODIFIED_DATE")
    private Date modifiedDate;

    @NotNull
    @Column(name = "PPR_PER_CLA_CODE")
    private String classCode;

    @Column(name = "PPR_FIRST_LOSS_PERCENTAGE")
    private Double firstLossPercentage;

    @Column(name = "PPR_CL_CLAIMS_PENDING")
    private Double claimPending;

    @Column(name = "PPR_CL_CLAIMS_PAID")
    private Double claimsPaid;

    @Column(name = "PPR_RI_SUM_INSURED")
    private Double reinsuranceSumInsured;

    @Column(name = "PPR_EXCESS_PERCENTAGE")
    private Double excessPercentage;

    @Column(name = "PPR_TRANSACTION_AMOUNT")
    private Double transactionAmount;

    @Column(name = "PPR_CANCELLED_AMOUNT")
    private Double cancelledAmount;

    @Column(name = "PPR_PREV_SEQ_NO")
    private String previousSequenceNo;

    @Column(name = "PPR_PREV_SUM_INSURED")
    private Double previousSumInsured;

    @Column(name = "PPR_PREV_PREMIUM")
    private Double previousPremium;

    @Column(name = "PPR_PSC_SEC_CODE")
    private String productSectionCode;

    @Column(name = "PPR_DFLT_PERCENTAGE")
    private Double defaultPercentage;

    @Column(name = "PPR_DFLT_RATE")
    private Double defaultRate;

    @Column(name = "PPR_DFLT_PREMIUM")
    private Double defaultPremium;

    @Column(name = "PPR_INS_POL_SEQ_NO")
    private String insPolicySequenceNo;

    @Column(name = "PPR_EFFECT_DATE")
    private Date effectiveDate;

    @Column(name = "PPR_DFLT_EXCESS")
    private Double defaultExcess;

    @Column(name = "PPR_DFLT_EXCESS_PERCENTAGE")
    private Double defaultExcessPercentage;

    @Column(name = "PPR_EXCESS_TXT")
    private String excessText;

    @Column(name = "PPR_LIMIT_TXT")
    private String limitText;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumns({
            @JoinColumn(name = "PPR_PRS_PLC_POL_SEQ_NO", referencedColumnName = "PRS_PLC_POL_SEQ_NO", insertable = false, updatable = false),
            @JoinColumn(name = "PPR_PRS_PLC_SEQ_NO", referencedColumnName = "PRS_PLC_SEQ_NO", insertable = false, updatable = false),
            @JoinColumn(name = "PPR_PRS_SEQ_NO", referencedColumnName = "PRS_SEQ_NO", insertable = false, updatable = false)})
    private TempPolicyRisk tempPolicyRisk;

    @OneToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "PPR_PER_PRL_CODE", referencedColumnName = "PRL_CODE", insertable = false, updatable = false)
    private RefPeril refPeril;

    public TempPolicyPerilPK getTempPolicyPerilPK() {
        return tempPolicyPerilPK;
    }

    public void setTempPolicyPerilPK(TempPolicyPerilPK tempPolicyPerilPK) {
        this.tempPolicyPerilPK = tempPolicyPerilPK;
    }

    public Double getPerilSumInsured() {
        return perilSumInsured;
    }

    public void setPerilSumInsured(Double perilSumInsured) {
        this.perilSumInsured = perilSumInsured;
    }

    public Double getPercentage() {
        return percentage;
    }

    public void setPercentage(Double percentage) {
        this.percentage = percentage;
    }

    public Double getRate() {
        return rate;
    }

    public void setRate(Double rate) {
        this.rate = rate;
    }

    public Double getExcess() {
        return excess;
    }

    public void setExcess(Double excess) {
        this.excess = excess;
    }

    public Double getPerUnit() {
        return perUnit;
    }

    public void setPerUnit(Double perUnit) {
        this.perUnit = perUnit;
    }

    public Integer getNoOfUnits() {
        return noOfUnits;
    }

    public void setNoOfUnits(Integer noOfUnits) {
        this.noOfUnits = noOfUnits;
    }

    public Double getDiscountPercentage() {
        return discountPercentage;
    }

    public void setDiscountPercentage(Double discountPercentage) {
        this.discountPercentage = discountPercentage;
    }

    public Double getDiscountRate() {
        return discountRate;
    }

    public void setDiscountRate(Double discountRate) {
        this.discountRate = discountRate;
    }

    public Double getEventLimit() {
        return eventLimit;
    }

    public void setEventLimit(Double eventLimit) {
        this.eventLimit = eventLimit;
    }

    public Double getAnnualLimit() {
        return annualLimit;
    }

    public void setAnnualLimit(Double annualLimit) {
        this.annualLimit = annualLimit;
    }

    public Double getPremium() {
        return premium;
    }

    public void setPremium(Double premium) {
        this.premium = premium;
    }

    public String getRemarks() {
        return remarks;
    }

    public void setRemarks(String remarks) {
        this.remarks = remarks;
    }

    public String getPerilCode() {
        return perilCode;
    }

    public void setPerilCode(String perilCode) {
        this.perilCode = perilCode;
    }

    public String getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(String createdBy) {
        this.createdBy = createdBy;
    }

    public Date getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(Date createdDate) {
        this.createdDate = createdDate;
    }

    public String getModifiedBy() {
        return modifiedBy;
    }

    public void setModifiedBy(String modifiedBy) {
        this.modifiedBy = modifiedBy;
    }

    public Date getModifiedDate() {
        return modifiedDate;
    }

    public void setModifiedDate(Date modifiedDate) {
        this.modifiedDate = modifiedDate;
    }

    public String getClassCode() {
        return classCode;
    }

    public void setClassCode(String classCode) {
        this.classCode = classCode;
    }

    public Double getFirstLossPercentage() {
        return firstLossPercentage;
    }

    public void setFirstLossPercentage(Double firstLossPercentage) {
        this.firstLossPercentage = firstLossPercentage;
    }

    public Double getClaimPending() {
        return claimPending;
    }

    public void setClaimPending(Double claimPending) {
        this.claimPending = claimPending;
    }

    public Double getClaimsPaid() {
        return claimsPaid;
    }

    public void setClaimsPaid(Double claimsPaid) {
        this.claimsPaid = claimsPaid;
    }

    public Double getReinsuranceSumInsured() {
        return reinsuranceSumInsured;
    }

    public void setReinsuranceSumInsured(Double reinsuranceSumInsured) {
        this.reinsuranceSumInsured = reinsuranceSumInsured;
    }

    public Double getExcessPercentage() {
        return excessPercentage;
    }

    public void setExcessPercentage(Double excessPercentage) {
        this.excessPercentage = excessPercentage;
    }

    public Double getTransactionAmount() {
        return transactionAmount;
    }

    public void setTransactionAmount(Double transactionAmount) {
        this.transactionAmount = transactionAmount;
    }

    public Double getCancelledAmount() {
        return cancelledAmount;
    }

    public void setCancelledAmount(Double cancelledAmount) {
        this.cancelledAmount = cancelledAmount;
    }

    public String getPreviousSequenceNo() {
        return previousSequenceNo;
    }

    public void setPreviousSequenceNo(String previousSequenceNo) {
        this.previousSequenceNo = previousSequenceNo;
    }

    public Double getPreviousSumInsured() {
        return previousSumInsured;
    }

    public void setPreviousSumInsured(Double previousSumInsured) {
        this.previousSumInsured = previousSumInsured;
    }

    public Double getPreviousPremium() {
        return previousPremium;
    }

    public void setPreviousPremium(Double previousPremium) {
        this.previousPremium = previousPremium;
    }

    public String getProductSectionCode() {
        return productSectionCode;
    }

    public void setProductSectionCode(String productSectionCode) {
        this.productSectionCode = productSectionCode;
    }

    public Double getDefaultPercentage() {
        return defaultPercentage;
    }

    public void setDefaultPercentage(Double defaultPercentage) {
        this.defaultPercentage = defaultPercentage;
    }

    public Double getDefaultRate() {
        return defaultRate;
    }

    public void setDefaultRate(Double defaultRate) {
        this.defaultRate = defaultRate;
    }

    public Double getDefaultPremium() {
        return defaultPremium;
    }

    public void setDefaultPremium(Double defaultPremium) {
        this.defaultPremium = defaultPremium;
    }

    public String getInsPolicySequenceNo() {
        return insPolicySequenceNo;
    }

    public void setInsPolicySequenceNo(String insPolicySequenceNo) {
        this.insPolicySequenceNo = insPolicySequenceNo;
    }

    public Date getEffectiveDate() {
        return effectiveDate;
    }

    public void setEffectiveDate(Date effectiveDate) {
        this.effectiveDate = effectiveDate;
    }

    public Double getDefaultExcess() {
        return defaultExcess;
    }

    public void setDefaultExcess(Double defaultExcess) {
        this.defaultExcess = defaultExcess;
    }

    public Double getDefaultExcessPercentage() {
        return defaultExcessPercentage;
    }

    public void setDefaultExcessPercentage(Double defaultExcessPercentage) {
        this.defaultExcessPercentage = defaultExcessPercentage;
    }

    public String getExcessText() {
        return excessText;
    }

    public void setExcessText(String excessText) {
        this.excessText = excessText;
    }

    public String getLimitText() {
        return limitText;
    }

    public void setLimitText(String limitText) {
        this.limitText = limitText;
    }

    public TempPolicyRisk getTempPolicyRisk() {
        return tempPolicyRisk;
    }

    public void setTempPolicyRisk(TempPolicyRisk tempPolicyRisk) {
        this.tempPolicyRisk = tempPolicyRisk;
    }

    public RefPeril getRefPeril() {
        return refPeril;
    }

    public void setRefPeril(RefPeril refPeril) {
        this.refPeril = refPeril;
    }
}
