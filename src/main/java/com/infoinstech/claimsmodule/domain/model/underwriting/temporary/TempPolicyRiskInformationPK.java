package com.infoinstech.claimsmodule.domain.model.underwriting.temporary;

import javax.persistence.Column;
import javax.persistence.Embeddable;
import java.io.Serializable;

@Embeddable
public class TempPolicyRiskInformationPK implements Serializable {

    @Column(name = "PIN_SEQ_NO")
    private String sequenceNo;

    @Column(name = "PIN_PRS_SEQ_NO")
    private String riskSequenceNo;

    @Column(name = "PIN_PRS_PLC_SEQ_NO")
    private String locationSequenceNo;

    @Column(name = "PIN_PRS_PLC_POL_SEQ_NO")
    private String policySequenceNo;

    public TempPolicyRiskInformationPK() {
    }

    public TempPolicyRiskInformationPK(String sequenceNo, String riskSequenceNo, String locationSequenceNo, String policySequenceNo) {
        this.sequenceNo = sequenceNo;
        this.riskSequenceNo = riskSequenceNo;
        this.locationSequenceNo = locationSequenceNo;
        this.policySequenceNo = policySequenceNo;
    }

    public String getSequenceNo() {
        return sequenceNo;
    }

    public void setSequenceNo(String sequenceNo) {
        this.sequenceNo = sequenceNo;
    }

    public String getRiskSequenceNo() {
        return riskSequenceNo;
    }

    public void setRiskSequenceNo(String riskSequenceNo) {
        this.riskSequenceNo = riskSequenceNo;
    }

    public String getLocationSequenceNo() {
        return locationSequenceNo;
    }

    public void setLocationSequenceNo(String locationSequenceNo) {
        this.locationSequenceNo = locationSequenceNo;
    }

    public String getPolicySequenceNo() {
        return policySequenceNo;
    }

    public void setPolicySequenceNo(String policySequenceNo) {
        this.policySequenceNo = policySequenceNo;
    }
}
