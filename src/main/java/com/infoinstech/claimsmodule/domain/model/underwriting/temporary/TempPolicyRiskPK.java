package com.infoinstech.claimsmodule.domain.model.underwriting.temporary;

import javax.persistence.Column;
import javax.persistence.Embeddable;
import java.io.Serializable;

@Embeddable
public class TempPolicyRiskPK implements Serializable {

    @Column(name = "PRS_SEQ_NO")
    private String sequenceNo;

    @Column(name = "PRS_PLC_SEQ_NO")
    private String locationSequenceNo;

    @Column(name = "PRS_PLC_POL_SEQ_NO")
    private String policySequenceNo;

    public TempPolicyRiskPK() {
    }

    public TempPolicyRiskPK(String sequenceNo, String locationSequenceNo, String policySequenceNo) {
        this.sequenceNo = sequenceNo;
        this.locationSequenceNo = locationSequenceNo;
        this.policySequenceNo = policySequenceNo;
    }

    public String getSequenceNo() {
        return sequenceNo;
    }

    public void setSequenceNo(String sequenceNo) {
        this.sequenceNo = sequenceNo;
    }

    public String getLocationSequenceNo() {
        return locationSequenceNo;
    }

    public void setLocationSequenceNo(String locationSequenceNo) {
        this.locationSequenceNo = locationSequenceNo;
    }

    public String getPolicySequenceNo() {
        return policySequenceNo;
    }

    public void setPolicySequenceNo(String policySequenceNo) {
        this.policySequenceNo = policySequenceNo;
    }
}
