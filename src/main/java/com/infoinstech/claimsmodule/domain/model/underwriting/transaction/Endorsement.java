package com.infoinstech.claimsmodule.domain.model.underwriting.transaction;

import com.infoinstech.claimsmodule.domain.model.salesmarketing.master.SalesLocation;
import com.infoinstech.claimsmodule.domain.model.salesmarketing.reference.BusinessType;
import com.infoinstech.claimsmodule.domain.model.underwriting.master.Customer;
import com.infoinstech.claimsmodule.domain.model.underwriting.master.CustomerAddress;
import com.infoinstech.claimsmodule.domain.model.underwriting.master.Product;
import com.infoinstech.claimsmodule.domain.model.underwriting.reference.BusinessChannel;
import com.infoinstech.claimsmodule.domain.model.underwriting.reference.RefClass;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.util.Date;

@Entity
@Table(name = "UW_T_ENDORSEMENTS")
public class Endorsement {

    @Id
    @Column(name = "EDT_SEQ_NO")
    private String sequenceNo;

    @Column(name = "EDT_ENDORSEMENT_NO")
    private String endorsementNo;

    @NotNull
    @Column(name = "EDT_PROPOSAL_NO")
    private String proposalNo;

    @Column(name = "EDT_QOT_SEQ_NO")
    private String quotationSequenceNo;

    @NotNull
    @Column(name = "EDT_CLA_CODE")
    private String classCode;

    @Column(name = "EDT_CERTIFICATE")
    private Character certificate;

    @Column(name = "EDT_CURRENCY")
    private String currency;

    @NotNull
    @Column(name = "EDT_CUS_CODE")
    private String customerCode;

    @NotNull
    @Column(name = "EDT_PRD_CODE")
    private String productCode;

    @Column(name = "EDT_DATE")
    private Date endorsementDate;

    @Column(name = "EDT_PERIOD_FROM")
    private Date periodFrom;

    @Column(name = "EDT_PERIOD_TO")
    private Date periodTo;

    @Column(name = "EDT_DAYS")
    private Integer days;

    @Column(name = "EDT_SUM_INSURED")
    private Double sumInsured;

    @Column(name = "EDT_PREMIUM")
    private Double premium;

    @Column(name = "EDT_GROUP")
    private Character groupPolicy;

    @Column(name = "EDT_SURVEY")
    private Character survey;

    @Column(name = "EDT_CLAIMS_AFTER")
    private Double claimsAfter;

    @Column(name = "EDT_REMARKS")
    private String remarks;

    @Column(name = "EDT_TARGET")
    private String target;

    @Column(name = "EDT_NET_OF_COMMISSION")
    private Character netOfCommission;

    @Column(name = "EDT_AUTHORIZED_BY")
    private String authorizedBy;

    @Column(name = "EDT_AUTHORIZED_DATE")
    private Date authorizedDate;

    @NotNull
    @Column(name = "EDT_STATUS")
    private String status;

    @Column(name = "EDT_COINSURANCE")
    private Character coinsurance;

    @Column(name = "EDT_COINSURANCE_PCT")
    private Double coinsurancePercentage;

    @Column(name = "CREATED_BY")
    private String createdBy;

    @Column(name = "CREATED_DATE")
    private Date createdDate;

    @Column(name = "MODIFIED_BY")
    private String modifiedBy;

    @Column(name = "MODIFIED_DATE")
    private Date modifiedDate;

    @NotNull
    @Column(name = "EDT_SLC_BRN_CODE")
    private String branchCode;

    @NotNull
    @Column(name = "EDT_POLICY_NO")
    private String policyNo;

    @NotNull
    @Column(name = "EDT_MARKETING_EXECUTIVE_CODE")
    private String intermediaryCode;

    @Column(name = "EDT_BCH_CODE")
    private String businessChannelCode;

    @Column(name = "EDT_TRANSACTION_TYPE")
    private String transactionType;

    @Column(name = "EDT_ENDORSEMENT_REMARKS")
    private String endorsementRemarks;

    @Column(name = "EDT_TRANSACTION_AMOUNT")
    private Double transactionAmount;

    @Column(name = "EDT_CALCULATION_TYPE")
    private String calculationType;

    @Column(name = "EDT_PREV_POL_SEQ_NO")
    private String previousPolicySequenceNo;


    @Column(name = "EDT_VOUCHER_NO")
    private String voucherNo;

    @Column(name = "EDT_CANCELLED_BY")
    private String cancelledBy;

    @Column(name = "EDT_CANCELLED_DATE")
    private Date cancelledDate;

    @NotNull
    @Column(name = "EDT_ANY_ENDORSEMENTS")
    private Character anyEndorsements;

    @Column(name = "EDT_LAST_ENDORSED_DATE")
    private Date lastEndorsedDate;

    @NotNull
    @Column(name = "EDT_ANY_HISTORY")
    private Character anyHistory;

    @Column(name = "EDT_LAST_HISTORY_DATE")
    private Date lastHistoryDate;

    @Column(name = "EDT_RI_TRANSFER_FLAG")
    private Character reinsuranceTransferFlag;

    @Column(name = "EDT_RI_SUM_INSURED")
    private Double reinsuranceSumInsured;

    @Column(name = "EDT_RC_OUTSTAND_AMT")
    private Double outstandingAmount;

    @Column(name = "EDT_RI_TREATY_DISTRIBUTED_FLAG")
    private Character reinsuranceTreatyDistributedFlag;

    @Column(name = "EDT_NO_OF_COPIES")
    private Integer noOfCopies;

    @Column(name = "EDT_OPEN_POLICY")
    private Character openPolicy;

    @Column(name = "EDT_REINSURANCE_REQ")
    private String reinsuranceRequired;

    @Column(name = "EDT_RENEWAL_NOTICE")
    private Character renewalNotice;

    @Column(name = "EDT_UP_STEP_NO")
    private Integer uprStepNo;

    @Column(name = "EDT_UP_TRANS_SEQ")
    private Integer uprTransactionSequenceNo;

    @Column(name = "EDT_RC_OUTSTAND_CHQ_AMT")
    private Double outstandingChequeAmount;

    @Column(name = "EDT_RC_RECEIPTING_DATE")
    private Date receiptingDate;

    @Column(name = "EDT_RC_CURRENCY_RATE")
    private Double currencyRate;

    @Column(name = "EDT_UP_NRP_PROCESSED")
    private String nrpprocessed;

    @Column(name = "EDT_BSS_BSS_CODE")
    private String intermediaryTypeCode;

    @Column(name = "EDT_FAMILY_UNIT")
    private Character familyUnit;

    @Column(name = "EDT_CDB_DEB_DEBTOR_CODE")
    private String debtorCode;

    @Column(name = "EDT_CDB_DEB_PROFIT_CENTER")
    private String profitCenter;

    @Column(name = "EDT_ANY_CLAIMS")
    private Character anyClaims;

    @Column(name = "EDT_UP_OPEN_PERCENTAGE")
    private Double uprOpenPercentage;

    @Column(name = "EDT_UP_PREM_ADV_PROCESSED")
    private Character premiumAdvancedProcessed;

    @Column(name = "EDT_RC_PROCESSED")
    private Character processed;

    @Column(name = "EDT_TOTAL_PREMIUM")
    private Double totalPremium;

    @Column(name = "EDT_TOTAL_TRANSACTION_AMOUNT")
    private Double totalTransactionAmount;

    @Column(name = "EDT_EFFECTIVE_START_DATE")
    private Date effectiveStartDate;

    @Column(name = "EDT_END_DATE")
    private Date endDate;

    @Column(name = "EDT_APORTIONED_FLAG")
    private Character aportionedFlag;

    @Column(name = "EDT_COMM_AMMENDED")
    private Character commissionAmended;

    @Column(name = "EDT_CREATED_BY")
    private String endorsementCreatedBy;

    @Column(name = "EDT_CREATED_DATE")
    private Date ensorsementCreatedBy;

    @Column(name = "EDT_COMM_AMMENDED_DATE")
    private Date commissionAmendedDate;

    @Column(name = "EDT_COMM_AMMEND_EFFECTIVE_DATE")
    private Date commissionAmendedEffectiveDate;

    @Column(name = "EDT_REL_POLICY_NO")
    private String relatedPolicyNo;

    @Column(name = "EDT_ADR_SEQ_NO")
    private String customerAddressSequenceNo;

    @Column(name = "EDT_CUS_REMARKS")
    private String customerRemarks;

    @Column(name = "EDT_PAYMENT_CAT")
    private String paymentCategory;

    @Column(name = "EDT_ART_CODE")
    private String arrangementType;

    @Column(name = "EDT_EXAMINED_BY")
    private String examinedBy;

    @Column(name = "EDT_EXAMINED_DATE")
    private Date examinedDate;

    @Column(name = "EDT_QUICK_UW")
    private Character quickUnderwriting;

    @Column(name = "EDT_ART_NO_OF_INSTALLMNTS")
    private String noOfInstallments;

    @NotNull
    @Column(name = "EDT_MIGRATED_REC")
    private Character migratedRecord;

    @Column(name = "EDT_OWNERSHIP_TRANSFER")
    private String ownershipTransfer;

    @Column(name = "EDT_TRANSFER_DATE")
    private Date transferDate;

    @Column(name = "EDT_BLIST_AUTH_REQ")
    private Character blackListAuthorizationRequired;

    @Column(name = "EDT_RI_EXT_AUTH_REQ")
    private Character riExtensionAuthorizationRequired;

    @Column(name = "EDT_INWD_OTWD_FLAG")
    private Character inWardOutWardFlag;

    @Column(name = "EDT_SP_REMARKS")
    private String specialRemarks;

    @Column(name = "EDT_EVENT_LIMIT")
    private Double eventLimit;

    @Column(name = "EDT_ANNUAL_LIMIT")
    private Double annualLimit;

    @Column(name = "EDT_EXCESS_TXT")
    private String excessText;

    @Column(name = "EDT_LIMIT_TXT")
    private String limitText;

    @Column(name = "EDT_CREDIT_AUTHORIZE")
    private Character creditAuthorized;

    @Column(name = "EDT_CREDIT_AUTHORIZED_BY")
    private String creditAuthorizedBy;

    @Column(name = "EDT_CREDIT_AUTHRIZED_DATE")
    private Date creditAuthorizedDate;

    @Column(name = "EDT_SPECIAL_ENDORSE_FLAG")
    private Character specialEndorsementFlag;

    @Column(name = "EDT_SPECIAL_POLICY_FLAG")
    private Character specialPolicyFlag;

//    @Column(name = "EDT_BPARTY_CODE")
//    private String businessPartyCode;
//
//    @Column(name = "EDT_FINALIZED_BY")
//    private String finalizedBy;
//
//    @Column(name = "EDT_FINALIZED_DATE")
//    private Date finalizedDate;

//    @Column(name = "EDT_RI_EXT_AUTH_GRANT")
//    private Character reinsuranceExtensionAuthorizationGranted;

//    @Column(name = "EDT_INSPECTION")
//    private Character inspection;
//
//    @Column(name = "EDT_RATES_FOR_COVERS")
//    private Character ratesForCovers;
//
//    @Column(name = "EDT_INVOICE_TYPE")
//    private Character invoiceType;
//
//    @Column(name = "EDT_CREATED_BRANCH")
//    private String createdBranch;

//    @Column(name = "EDT_RETROACTVIE_DATE")
//    private Date retroActiveDate;

//    @Column(name = "EDT_MAINT_END_DATE")
//    private Date maintenanceEndDate;
//
//    @Column(name = "EDT_CANCEL_REMARKS")
//    private String cancellationRemarks;

//    @Column(name = "EDT_TRAN_STATUS")
//    private String transactionStatus;
//
//    @Column(name = "EDT_BPARTY_BSS_CODE")
//    private String businessPartyBssCode;
//
//    @Column(name = "EDT_AVERAGE_PREMIUM")
//    private Double averagePremium;
//
//    @Column(name = "EDT_GRP_DISCOUNT")
//    private Double groupDiscount;
//
//    @Column(name = "EDT_GRP_SIZE")
//    private Integer groupSize;
//
//    @Column(name = "EDT_NEW_CUS_FLG")
//    private Character customerPolicyStatus;
//
//    @Column(name = "EDT_INWD_COINS_REF_NO")
//    private String inwardCoinsuranceReferenceNo;

    @OneToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "EDT_CLA_CODE", referencedColumnName = "CLA_CODE", insertable = false, updatable = false)
    private RefClass refClass;

    @OneToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "EDT_PRD_CODE", referencedColumnName = "PRD_CODE", insertable = false, updatable = false)
    private Product product;

    //OnetoOne on Quotation

    @OneToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "EDT_CUS_CODE", referencedColumnName = "CUS_CODE", insertable = false, updatable = false)
    private Customer customer;

    @OneToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "EDT_BCH_CODE", referencedColumnName = "BCH_CODE", insertable = false, updatable = false)
    private BusinessChannel businessChannel;

    @OneToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "EDT_BSS_BSS_CODE", referencedColumnName = "BSS_BSS_CODE", insertable = false, updatable = false)
    private BusinessType businessType;

    @OneToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "EDT_ADR_SEQ_NO", referencedColumnName = "ADR_SEQ_NO", insertable = false, updatable = false)
    private CustomerAddress customerAddress;

    @OneToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "EDT_SLC_BRN_CODE", referencedColumnName = "SLC_BRN_CODE", insertable = false, updatable = false)
    private SalesLocation salesLocation;

    public String getSequenceNo() {
        return sequenceNo;
    }

    public void setSequenceNo(String sequenceNo) {
        this.sequenceNo = sequenceNo;
    }

    public String getEndorsementNo() {
        return endorsementNo;
    }

    public void setEndorsementNo(String endorsementNo) {
        this.endorsementNo = endorsementNo;
    }

    public String getProposalNo() {
        return proposalNo;
    }

    public void setProposalNo(String proposalNo) {
        this.proposalNo = proposalNo;
    }

    public String getQuotationSequenceNo() {
        return quotationSequenceNo;
    }

    public void setQuotationSequenceNo(String quotationSequenceNo) {
        this.quotationSequenceNo = quotationSequenceNo;
    }

    public String getClassCode() {
        return classCode;
    }

    public void setClassCode(String classCode) {
        this.classCode = classCode;
    }

    public Character getCertificate() {
        return certificate;
    }

    public void setCertificate(Character certificate) {
        this.certificate = certificate;
    }

    public String getCurrency() {
        return currency;
    }

    public void setCurrency(String currency) {
        this.currency = currency;
    }

    public String getCustomerCode() {
        return customerCode;
    }

    public void setCustomerCode(String customerCode) {
        this.customerCode = customerCode;
    }

    public String getProductCode() {
        return productCode;
    }

    public void setProductCode(String productCode) {
        this.productCode = productCode;
    }

    public Date getEndorsementDate() {
        return endorsementDate;
    }

    public void setEndorsementDate(Date endorsementDate) {
        this.endorsementDate = endorsementDate;
    }

    public Date getPeriodFrom() {
        return periodFrom;
    }

    public void setPeriodFrom(Date periodFrom) {
        this.periodFrom = periodFrom;
    }

    public Date getPeriodTo() {
        return periodTo;
    }

    public void setPeriodTo(Date periodTo) {
        this.periodTo = periodTo;
    }

    public Integer getDays() {
        return days;
    }

    public void setDays(Integer days) {
        this.days = days;
    }

    public Double getSumInsured() {
        return sumInsured;
    }

    public void setSumInsured(Double sumInsured) {
        this.sumInsured = sumInsured;
    }

    public Double getPremium() {
        return premium;
    }

    public void setPremium(Double premium) {
        this.premium = premium;
    }

    public Character getGroupPolicy() {
        return groupPolicy;
    }

    public void setGroupPolicy(Character groupPolicy) {
        this.groupPolicy = groupPolicy;
    }

    public Character getSurvey() {
        return survey;
    }

    public void setSurvey(Character survey) {
        this.survey = survey;
    }

    public Double getClaimsAfter() {
        return claimsAfter;
    }

    public void setClaimsAfter(Double claimsAfter) {
        this.claimsAfter = claimsAfter;
    }

    public String getRemarks() {
        return remarks;
    }

    public void setRemarks(String remarks) {
        this.remarks = remarks;
    }

    public String getTarget() {
        return target;
    }

    public void setTarget(String target) {
        this.target = target;
    }

    public Character getNetOfCommission() {
        return netOfCommission;
    }

    public void setNetOfCommission(Character netOfCommission) {
        this.netOfCommission = netOfCommission;
    }

    public String getAuthorizedBy() {
        return authorizedBy;
    }

    public void setAuthorizedBy(String authorizedBy) {
        this.authorizedBy = authorizedBy;
    }

    public Date getAuthorizedDate() {
        return authorizedDate;
    }

    public void setAuthorizedDate(Date authorizedDate) {
        this.authorizedDate = authorizedDate;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public Character getCoinsurance() {
        return coinsurance;
    }

    public void setCoinsurance(Character coinsurance) {
        this.coinsurance = coinsurance;
    }

    public Double getCoinsurancePercentage() {
        return coinsurancePercentage;
    }

    public void setCoinsurancePercentage(Double coinsurancePercentage) {
        this.coinsurancePercentage = coinsurancePercentage;
    }

    public String getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(String createdBy) {
        this.createdBy = createdBy;
    }

    public Date getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(Date createdDate) {
        this.createdDate = createdDate;
    }

    public String getModifiedBy() {
        return modifiedBy;
    }

    public void setModifiedBy(String modifiedBy) {
        this.modifiedBy = modifiedBy;
    }

    public Date getModifiedDate() {
        return modifiedDate;
    }

    public void setModifiedDate(Date modifiedDate) {
        this.modifiedDate = modifiedDate;
    }

    public String getBranchCode() {
        return branchCode;
    }

    public void setBranchCode(String branchCode) {
        this.branchCode = branchCode;
    }

    public String getPolicyNo() {
        return policyNo;
    }

    public void setPolicyNo(String policyNo) {
        this.policyNo = policyNo;
    }

    public String getIntermediaryCode() {
        return intermediaryCode;
    }

    public void setIntermediaryCode(String intermediaryCode) {
        this.intermediaryCode = intermediaryCode;
    }

    public String getBusinessChannelCode() {
        return businessChannelCode;
    }

    public void setBusinessChannelCode(String businessChannelCode) {
        this.businessChannelCode = businessChannelCode;
    }

    public String getTransactionType() {
        return transactionType;
    }

    public void setTransactionType(String transactionType) {
        this.transactionType = transactionType;
    }

    public String getEndorsementRemarks() {
        return endorsementRemarks;
    }

    public void setEndorsementRemarks(String endorsementRemarks) {
        this.endorsementRemarks = endorsementRemarks;
    }

    public Double getTransactionAmount() {
        return transactionAmount;
    }

    public void setTransactionAmount(Double transactionAmount) {
        this.transactionAmount = transactionAmount;
    }

    public String getCalculationType() {
        return calculationType;
    }

    public void setCalculationType(String calculationType) {
        this.calculationType = calculationType;
    }

    public String getPreviousPolicySequenceNo() {
        return previousPolicySequenceNo;
    }

    public void setPreviousPolicySequenceNo(String previousPolicySequenceNo) {
        this.previousPolicySequenceNo = previousPolicySequenceNo;
    }

    public String getVoucherNo() {
        return voucherNo;
    }

    public void setVoucherNo(String voucherNo) {
        this.voucherNo = voucherNo;
    }

    public String getCancelledBy() {
        return cancelledBy;
    }

    public void setCancelledBy(String cancelledBy) {
        this.cancelledBy = cancelledBy;
    }

    public Date getCancelledDate() {
        return cancelledDate;
    }

    public void setCancelledDate(Date cancelledDate) {
        this.cancelledDate = cancelledDate;
    }

    public Character getAnyEndorsements() {
        return anyEndorsements;
    }

    public void setAnyEndorsements(Character anyEndorsements) {
        this.anyEndorsements = anyEndorsements;
    }

    public Date getLastEndorsedDate() {
        return lastEndorsedDate;
    }

    public void setLastEndorsedDate(Date lastEndorsedDate) {
        this.lastEndorsedDate = lastEndorsedDate;
    }

    public Character getAnyHistory() {
        return anyHistory;
    }

    public void setAnyHistory(Character anyHistory) {
        this.anyHistory = anyHistory;
    }

    public Date getLastHistoryDate() {
        return lastHistoryDate;
    }

    public void setLastHistoryDate(Date lastHistoryDate) {
        this.lastHistoryDate = lastHistoryDate;
    }

    public Character getReinsuranceTransferFlag() {
        return reinsuranceTransferFlag;
    }

    public void setReinsuranceTransferFlag(Character reinsuranceTransferFlag) {
        this.reinsuranceTransferFlag = reinsuranceTransferFlag;
    }

    public Double getReinsuranceSumInsured() {
        return reinsuranceSumInsured;
    }

    public void setReinsuranceSumInsured(Double reinsuranceSumInsured) {
        this.reinsuranceSumInsured = reinsuranceSumInsured;
    }

    public Double getOutstandingAmount() {
        return outstandingAmount;
    }

    public void setOutstandingAmount(Double outstandingAmount) {
        this.outstandingAmount = outstandingAmount;
    }

    public Character getReinsuranceTreatyDistributedFlag() {
        return reinsuranceTreatyDistributedFlag;
    }

    public void setReinsuranceTreatyDistributedFlag(Character reinsuranceTreatyDistributedFlag) {
        this.reinsuranceTreatyDistributedFlag = reinsuranceTreatyDistributedFlag;
    }

    public Integer getNoOfCopies() {
        return noOfCopies;
    }

    public void setNoOfCopies(Integer noOfCopies) {
        this.noOfCopies = noOfCopies;
    }

    public Character getOpenPolicy() {
        return openPolicy;
    }

    public void setOpenPolicy(Character openPolicy) {
        this.openPolicy = openPolicy;
    }

    public String getReinsuranceRequired() {
        return reinsuranceRequired;
    }

    public void setReinsuranceRequired(String reinsuranceRequired) {
        this.reinsuranceRequired = reinsuranceRequired;
    }

    public Character getRenewalNotice() {
        return renewalNotice;
    }

    public void setRenewalNotice(Character renewalNotice) {
        this.renewalNotice = renewalNotice;
    }

    public Integer getUprStepNo() {
        return uprStepNo;
    }

    public void setUprStepNo(Integer uprStepNo) {
        this.uprStepNo = uprStepNo;
    }

    public Integer getUprTransactionSequenceNo() {
        return uprTransactionSequenceNo;
    }

    public void setUprTransactionSequenceNo(Integer uprTransactionSequenceNo) {
        this.uprTransactionSequenceNo = uprTransactionSequenceNo;
    }

    public Double getOutstandingChequeAmount() {
        return outstandingChequeAmount;
    }

    public void setOutstandingChequeAmount(Double outstandingChequeAmount) {
        this.outstandingChequeAmount = outstandingChequeAmount;
    }

    public Date getReceiptingDate() {
        return receiptingDate;
    }

    public void setReceiptingDate(Date receiptingDate) {
        this.receiptingDate = receiptingDate;
    }

    public Double getCurrencyRate() {
        return currencyRate;
    }

    public void setCurrencyRate(Double currencyRate) {
        this.currencyRate = currencyRate;
    }

    public String getNrpprocessed() {
        return nrpprocessed;
    }

    public void setNrpprocessed(String nrpprocessed) {
        this.nrpprocessed = nrpprocessed;
    }

    public String getIntermediaryTypeCode() {
        return intermediaryTypeCode;
    }

    public void setIntermediaryTypeCode(String intermediaryTypeCode) {
        this.intermediaryTypeCode = intermediaryTypeCode;
    }

    public Character getFamilyUnit() {
        return familyUnit;
    }

    public void setFamilyUnit(Character familyUnit) {
        this.familyUnit = familyUnit;
    }

    public String getDebtorCode() {
        return debtorCode;
    }

    public void setDebtorCode(String debtorCode) {
        this.debtorCode = debtorCode;
    }

    public String getProfitCenter() {
        return profitCenter;
    }

    public void setProfitCenter(String profitCenter) {
        this.profitCenter = profitCenter;
    }

    public Character getAnyClaims() {
        return anyClaims;
    }

    public void setAnyClaims(Character anyClaims) {
        this.anyClaims = anyClaims;
    }

    public Double getUprOpenPercentage() {
        return uprOpenPercentage;
    }

    public void setUprOpenPercentage(Double uprOpenPercentage) {
        this.uprOpenPercentage = uprOpenPercentage;
    }

    public Character getPremiumAdvancedProcessed() {
        return premiumAdvancedProcessed;
    }

    public void setPremiumAdvancedProcessed(Character premiumAdvancedProcessed) {
        this.premiumAdvancedProcessed = premiumAdvancedProcessed;
    }

    public Character getProcessed() {
        return processed;
    }

    public void setProcessed(Character processed) {
        this.processed = processed;
    }

    public Double getTotalPremium() {
        return totalPremium;
    }

    public void setTotalPremium(Double totalPremium) {
        this.totalPremium = totalPremium;
    }

    public Double getTotalTransactionAmount() {
        return totalTransactionAmount;
    }

    public void setTotalTransactionAmount(Double totalTransactionAmount) {
        this.totalTransactionAmount = totalTransactionAmount;
    }

    public Date getEffectiveStartDate() {
        return effectiveStartDate;
    }

    public void setEffectiveStartDate(Date effectiveStartDate) {
        this.effectiveStartDate = effectiveStartDate;
    }

    public Date getEndDate() {
        return endDate;
    }

    public void setEndDate(Date endDate) {
        this.endDate = endDate;
    }

    public Character getAportionedFlag() {
        return aportionedFlag;
    }

    public void setAportionedFlag(Character aportionedFlag) {
        this.aportionedFlag = aportionedFlag;
    }

    public Character getCommissionAmended() {
        return commissionAmended;
    }

    public void setCommissionAmended(Character commissionAmended) {
        this.commissionAmended = commissionAmended;
    }

    public String getEndorsementCreatedBy() {
        return endorsementCreatedBy;
    }

    public void setEndorsementCreatedBy(String endorsementCreatedBy) {
        this.endorsementCreatedBy = endorsementCreatedBy;
    }

    public Date getEnsorsementCreatedBy() {
        return ensorsementCreatedBy;
    }

    public void setEnsorsementCreatedBy(Date ensorsementCreatedBy) {
        this.ensorsementCreatedBy = ensorsementCreatedBy;
    }

    public Date getCommissionAmendedDate() {
        return commissionAmendedDate;
    }

    public void setCommissionAmendedDate(Date commissionAmendedDate) {
        this.commissionAmendedDate = commissionAmendedDate;
    }

    public Date getCommissionAmendedEffectiveDate() {
        return commissionAmendedEffectiveDate;
    }

    public void setCommissionAmendedEffectiveDate(Date commissionAmendedEffectiveDate) {
        this.commissionAmendedEffectiveDate = commissionAmendedEffectiveDate;
    }

    public String getRelatedPolicyNo() {
        return relatedPolicyNo;
    }

    public void setRelatedPolicyNo(String relatedPolicyNo) {
        this.relatedPolicyNo = relatedPolicyNo;
    }

    public String getCustomerAddressSequenceNo() {
        return customerAddressSequenceNo;
    }

    public void setCustomerAddressSequenceNo(String customerAddressSequenceNo) {
        this.customerAddressSequenceNo = customerAddressSequenceNo;
    }

    public String getCustomerRemarks() {
        return customerRemarks;
    }

    public void setCustomerRemarks(String customerRemarks) {
        this.customerRemarks = customerRemarks;
    }

    public String getPaymentCategory() {
        return paymentCategory;
    }

    public void setPaymentCategory(String paymentCategory) {
        this.paymentCategory = paymentCategory;
    }

    public String getArrangementType() {
        return arrangementType;
    }

    public void setArrangementType(String arrangementType) {
        this.arrangementType = arrangementType;
    }

    public String getExaminedBy() {
        return examinedBy;
    }

    public void setExaminedBy(String examinedBy) {
        this.examinedBy = examinedBy;
    }

    public Date getExaminedDate() {
        return examinedDate;
    }

    public void setExaminedDate(Date examinedDate) {
        this.examinedDate = examinedDate;
    }

    public Character getQuickUnderwriting() {
        return quickUnderwriting;
    }

    public void setQuickUnderwriting(Character quickUnderwriting) {
        this.quickUnderwriting = quickUnderwriting;
    }

    public String getNoOfInstallments() {
        return noOfInstallments;
    }

    public void setNoOfInstallments(String noOfInstallments) {
        this.noOfInstallments = noOfInstallments;
    }

    public Character getMigratedRecord() {
        return migratedRecord;
    }

    public void setMigratedRecord(Character migratedRecord) {
        this.migratedRecord = migratedRecord;
    }

    public String getOwnershipTransfer() {
        return ownershipTransfer;
    }

    public void setOwnershipTransfer(String ownershipTransfer) {
        this.ownershipTransfer = ownershipTransfer;
    }

    public Date getTransferDate() {
        return transferDate;
    }

    public void setTransferDate(Date transferDate) {
        this.transferDate = transferDate;
    }

    public Character getBlackListAuthorizationRequired() {
        return blackListAuthorizationRequired;
    }

    public void setBlackListAuthorizationRequired(Character blackListAuthorizationRequired) {
        this.blackListAuthorizationRequired = blackListAuthorizationRequired;
    }

    public Character getRiExtensionAuthorizationRequired() {
        return riExtensionAuthorizationRequired;
    }

    public void setRiExtensionAuthorizationRequired(Character riExtensionAuthorizationRequired) {
        this.riExtensionAuthorizationRequired = riExtensionAuthorizationRequired;
    }

    public Character getInWardOutWardFlag() {
        return inWardOutWardFlag;
    }

    public void setInWardOutWardFlag(Character inWardOutWardFlag) {
        this.inWardOutWardFlag = inWardOutWardFlag;
    }

    public String getSpecialRemarks() {
        return specialRemarks;
    }

    public void setSpecialRemarks(String specialRemarks) {
        this.specialRemarks = specialRemarks;
    }

    public Double getEventLimit() {
        return eventLimit;
    }

    public void setEventLimit(Double eventLimit) {
        this.eventLimit = eventLimit;
    }

    public Double getAnnualLimit() {
        return annualLimit;
    }

    public void setAnnualLimit(Double annualLimit) {
        this.annualLimit = annualLimit;
    }

    public String getExcessText() {
        return excessText;
    }

    public void setExcessText(String excessText) {
        this.excessText = excessText;
    }

    public String getLimitText() {
        return limitText;
    }

    public void setLimitText(String limitText) {
        this.limitText = limitText;
    }

    public Character getCreditAuthorized() {
        return creditAuthorized;
    }

    public void setCreditAuthorized(Character creditAuthorized) {
        this.creditAuthorized = creditAuthorized;
    }

    public String getCreditAuthorizedBy() {
        return creditAuthorizedBy;
    }

    public void setCreditAuthorizedBy(String creditAuthorizedBy) {
        this.creditAuthorizedBy = creditAuthorizedBy;
    }

    public Date getCreditAuthorizedDate() {
        return creditAuthorizedDate;
    }

    public void setCreditAuthorizedDate(Date creditAuthorizedDate) {
        this.creditAuthorizedDate = creditAuthorizedDate;
    }

    public Character getSpecialEndorsementFlag() {
        return specialEndorsementFlag;
    }

    public void setSpecialEndorsementFlag(Character specialEndorsementFlag) {
        this.specialEndorsementFlag = specialEndorsementFlag;
    }

    public Character getSpecialPolicyFlag() {
        return specialPolicyFlag;
    }

    public void setSpecialPolicyFlag(Character specialPolicyFlag) {
        this.specialPolicyFlag = specialPolicyFlag;
    }

//    public String getBusinessPartyCode() {
//        return businessPartyCode;
//    }
//
//    public void setBusinessPartyCode(String businessPartyCode) {
//        this.businessPartyCode = businessPartyCode;
//    }
//
//    public String getFinalizedBy() {
//        return finalizedBy;
//    }
//
//    public void setFinalizedBy(String finalizedBy) {
//        this.finalizedBy = finalizedBy;
//    }
//
//    public Date getFinalizedDate() {
//        return finalizedDate;
//    }
//
//    public void setFinalizedDate(Date finalizedDate) {
//        this.finalizedDate = finalizedDate;
//    }

//    public Character getReinsuranceExtensionAuthorizationGranted() {
//        return reinsuranceExtensionAuthorizationGranted;
//    }
//
//    public void setReinsuranceExtensionAuthorizationGranted(Character reinsuranceExtensionAuthorizationGranted) {
//        this.reinsuranceExtensionAuthorizationGranted = reinsuranceExtensionAuthorizationGranted;
//    }

//    public Character getInspection() {
//        return inspection;
//    }
//
//    public void setInspection(Character inspection) {
//        this.inspection = inspection;
//    }
//
//    public Character getRatesForCovers() {
//        return ratesForCovers;
//    }
//
//    public void setRatesForCovers(Character ratesForCovers) {
//        this.ratesForCovers = ratesForCovers;
//    }
//
//    public Character getInvoiceType() {
//        return invoiceType;
//    }
//
//    public void setInvoiceType(Character invoiceType) {
//        this.invoiceType = invoiceType;
//    }
//
//    public String getCreatedBranch() {
//        return createdBranch;
//    }
//
//    public void setCreatedBranch(String createdBranch) {
//        this.createdBranch = createdBranch;
//    }

//    public Date getRetroActiveDate() {
//        return retroActiveDate;
//    }
//
//    public void setRetroActiveDate(Date retroActiveDate) {
//        this.retroActiveDate = retroActiveDate;
//    }

//    public Date getMaintenanceEndDate() {
//        return maintenanceEndDate;
//    }
//
//    public void setMaintenanceEndDate(Date maintenanceEndDate) {
//        this.maintenanceEndDate = maintenanceEndDate;
//    }
//
//    public String getCancellationRemarks() {
//        return cancellationRemarks;
//    }
//
//    public void setCancellationRemarks(String cancellationRemarks) {
//        this.cancellationRemarks = cancellationRemarks;
//    }

//    public String getTransactionStatus() {
//        return transactionStatus;
//    }
//
//    public void setTransactionStatus(String transactionStatus) {
//        this.transactionStatus = transactionStatus;
//    }

//    public String getBusinessPartyBssCode() {
//        return businessPartyBssCode;
//    }
//
//    public void setBusinessPartyBssCode(String businessPartyBssCode) {
//        this.businessPartyBssCode = businessPartyBssCode;
//    }
//
//    public Double getAveragePremium() {
//        return averagePremium;
//    }
//
//    public void setAveragePremium(Double averagePremium) {
//        this.averagePremium = averagePremium;
//    }
//
//    public Double getGroupDiscount() {
//        return groupDiscount;
//    }
//
//    public void setGroupDiscount(Double groupDiscount) {
//        this.groupDiscount = groupDiscount;
//    }
//
//    public Integer getGroupSize() {
//        return groupSize;
//    }
//
//    public void setGroupSize(Integer groupSize) {
//        this.groupSize = groupSize;
//    }
//
//    public Character getCustomerPolicyStatus() {
//        return customerPolicyStatus;
//    }
//
//    public void setCustomerPolicyStatus(Character customerPolicyStatus) {
//        this.customerPolicyStatus = customerPolicyStatus;
//    }
//
//    public String getInwardCoinsuranceReferenceNo() {
//        return inwardCoinsuranceReferenceNo;
//    }
//
//    public void setInwardCoinsuranceReferenceNo(String inwardCoinsuranceReferenceNo) {
//        this.inwardCoinsuranceReferenceNo = inwardCoinsuranceReferenceNo;
//    }

    public RefClass getRefClass() {
        return refClass;
    }

    public void setRefClass(RefClass refClass) {
        this.refClass = refClass;
    }

    public Product getProduct() {
        return product;
    }

    public void setProduct(Product product) {
        this.product = product;
    }

    public Customer getCustomer() {
        return customer;
    }

    public void setCustomer(Customer customer) {
        this.customer = customer;
    }

    public BusinessChannel getBusinessChannel() {
        return businessChannel;
    }

    public void setBusinessChannel(BusinessChannel businessChannel) {
        this.businessChannel = businessChannel;
    }

    public BusinessType getBusinessType() {
        return businessType;
    }

    public void setBusinessType(BusinessType businessType) {
        this.businessType = businessType;
    }

    public CustomerAddress getCustomerAddress() {
        return customerAddress;
    }

    public void setCustomerAddress(CustomerAddress customerAddress) {
        this.customerAddress = customerAddress;
    }

    public SalesLocation getSalesLocation() {
        return salesLocation;
    }

    public void setSalesLocation(SalesLocation salesLocation) {
        this.salesLocation = salesLocation;
    }
}

