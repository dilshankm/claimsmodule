package com.infoinstech.claimsmodule.domain.model.underwriting.transaction;

import com.infoinstech.claimsmodule.domain.model.underwriting.reference.RefLocation;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.util.Date;

@Entity
@Table(name = "UW_T_END_LOCATIONS")
public class EndorsementLocation {

    @EmbeddedId
    private EndorsementLocationPK endorsementLocationPK;

    @Column(name = "ELC_DATE_TIME")
    private Date dateAndTime;

    @NotNull
    @Column(name = "ELC_LOC_CODE")
    private String locationCode;

    @Column(name = "CREATED_BY")
    private String createdBy;

    @Column(name = "CREATED_DATE")
    private Date createdDate;

    @Column(name = "MODIFIED_BY")
    private String modifiedBy;

    @Column(name = "MODIFIED_DATE")
    private Date modifiedDate;

    @Column(name = "ELC_RISK_NEXT")
    private Integer riskNext;

    @Column(name = "ELC_SUM_INSURED")
    private Double sumInsured;

    @Column(name = "ELC_PREMIUM")
    private Double premium;

    @NotNull
    @Column(name = "ELC_NEW_LOC")
    private Character newLocation;

    @Column(name = "ELC_INSERT_ENDORSE_NO")
    private String insertedEndorsementNo;

    @Column(name = "ELC_UPDATE_ENDORSE_NO")
    private String updatedEndorsementNo;

    @Column(name = "ELC_DELETE_ENDORSE_NO")
    private String deletedEndorsementNo;

    @Column(name = "ELC_DATE_DELETED")
    private Date deletedDate;

    @Column(name = "ELC_PREV_SEQ_NO")
    private String previouseSequenceNo;

    @Column(name = "ELC_EVENT_LIMIT")
    private Double eventLimit;

    @Column(name = "ELC_ANNUAL_LIMIT")
    private Double annualLimit;

    @Column(name = "ELC_EXCESS_TXT")
    private String excessText;

    @Column(name = "ELC_LIMIT_TXT")
    private String limitText;

    @Column(name = "ELC_LOC_DESCRIPTION")
    private String locationDescription;

    @OneToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "ELC_LOC_CODE", referencedColumnName = "LOC_CODE", insertable = false, updatable = false)
    private RefLocation refLocation;

    public EndorsementLocationPK getEndorsementLocationPK() {
        return endorsementLocationPK;
    }

    public void setEndorsementLocationPK(EndorsementLocationPK endorsementLocationPK) {
        this.endorsementLocationPK = endorsementLocationPK;
    }

    public Date getDateAndTime() {
        return dateAndTime;
    }

    public void setDateAndTime(Date dateAndTime) {
        this.dateAndTime = dateAndTime;
    }

    public String getLocationCode() {
        return locationCode;
    }

    public void setLocationCode(String locationCode) {
        this.locationCode = locationCode;
    }

    public String getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(String createdBy) {
        this.createdBy = createdBy;
    }

    public Date getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(Date createdDate) {
        this.createdDate = createdDate;
    }

    public String getModifiedBy() {
        return modifiedBy;
    }

    public void setModifiedBy(String modifiedBy) {
        this.modifiedBy = modifiedBy;
    }

    public Date getModifiedDate() {
        return modifiedDate;
    }

    public void setModifiedDate(Date modifiedDate) {
        this.modifiedDate = modifiedDate;
    }

    public Integer getRiskNext() {
        return riskNext;
    }

    public void setRiskNext(Integer riskNext) {
        this.riskNext = riskNext;
    }

    public Double getSumInsured() {
        return sumInsured;
    }

    public void setSumInsured(Double sumInsured) {
        this.sumInsured = sumInsured;
    }

    public Double getPremium() {
        return premium;
    }

    public void setPremium(Double premium) {
        this.premium = premium;
    }

    public Character getNewLocation() {
        return newLocation;
    }

    public void setNewLocation(Character newLocation) {
        this.newLocation = newLocation;
    }

    public String getInsertedEndorsementNo() {
        return insertedEndorsementNo;
    }

    public void setInsertedEndorsementNo(String insertedEndorsementNo) {
        this.insertedEndorsementNo = insertedEndorsementNo;
    }

    public String getUpdatedEndorsementNo() {
        return updatedEndorsementNo;
    }

    public void setUpdatedEndorsementNo(String updatedEndorsementNo) {
        this.updatedEndorsementNo = updatedEndorsementNo;
    }

    public String getDeletedEndorsementNo() {
        return deletedEndorsementNo;
    }

    public void setDeletedEndorsementNo(String deletedEndorsementNo) {
        this.deletedEndorsementNo = deletedEndorsementNo;
    }

    public Date getDeletedDate() {
        return deletedDate;
    }

    public void setDeletedDate(Date deletedDate) {
        this.deletedDate = deletedDate;
    }

    public String getPreviouseSequenceNo() {
        return previouseSequenceNo;
    }

    public void setPreviouseSequenceNo(String previouseSequenceNo) {
        this.previouseSequenceNo = previouseSequenceNo;
    }

    public Double getEventLimit() {
        return eventLimit;
    }

    public void setEventLimit(Double eventLimit) {
        this.eventLimit = eventLimit;
    }

    public Double getAnnualLimit() {
        return annualLimit;
    }

    public void setAnnualLimit(Double annualLimit) {
        this.annualLimit = annualLimit;
    }

    public String getExcessText() {
        return excessText;
    }

    public void setExcessText(String excessText) {
        this.excessText = excessText;
    }

    public String getLimitText() {
        return limitText;
    }

    public void setLimitText(String limitText) {
        this.limitText = limitText;
    }

    public String getLocationDescription() {
        return locationDescription;
    }

    public void setLocationDescription(String locationDescription) {
        this.locationDescription = locationDescription;
    }

    public RefLocation getRefLocation() {
        return refLocation;
    }

    public void setRefLocation(RefLocation refLocation) {
        this.refLocation = refLocation;
    }
}
