package com.infoinstech.claimsmodule.domain.model.underwriting.transaction;

import javax.persistence.Column;
import javax.persistence.Embeddable;
import javax.validation.constraints.NotNull;
import java.io.Serializable;

@Embeddable
public class EndorsementLocationPK implements Serializable {

    @Column(name = "ELC_SEQ_NO")
    private String sequenceNo;
    @NotNull
    @Column(name = "ELC_EDT_SEQ_NO")
    private String endorsementSequenceNo;

    public EndorsementLocationPK() {
    }

    public EndorsementLocationPK(String sequenceNo, String endorsementSequenceNo) {
        this.sequenceNo = sequenceNo;
        this.endorsementSequenceNo = endorsementSequenceNo;
    }

    public String getSequenceNo() {
        return sequenceNo;
    }

    public void setSequenceNo(String sequenceNo) {
        this.sequenceNo = sequenceNo;
    }

    public String getEndorsementSequenceNo() {
        return endorsementSequenceNo;
    }

    public void setEndorsementSequenceNo(String endorsementSequenceNo) {
        this.endorsementSequenceNo = endorsementSequenceNo;
    }


}
