package com.infoinstech.claimsmodule.domain.model.underwriting.transaction;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.util.Date;
import java.util.List;

@Entity
@Table(name = "UW_T_END_RISKS")
public class EndorsementRisk {

    @EmbeddedId
    private EndorsementRiskPK endorsementRiskPK;

    @NotNull
    @Column(name = "ERS_R_SEQ")
    private String riskOrderNo;

    @Column(name = "ERS_RISK_REF_NO")
    private String riskReferenceNo;

    @Column(name = "ERS_REFERENCE_FIELD")
    private String referenceField;

    @Column(name = "ERS_TITLE")
    private String title;

    @Column(name = "ERS_NAME")
    private String name;

    @Column(name = "ERS_NIC_NO")
    private String nicNumber;

    @Column(name = "ERS_SUM_INSURED")
    private Double sumInsured;

    @Column(name = "ERS_PREMIUM")
    private Double premium;

    @Column(name = "ERS_RELATIONSHIP_TYPE")
    private String relationshipType;

    @Column(name = "ERS_RELATIONSHIP_SEQ")
    private String relationshipSequence;

    @Column(name = "ERS_ALL_INCLUSIVE")
    private Character allInclusive;

    @Column(name = "ERS_ALL_INCLUSIVE_PCT")
    private Double allInclusivePercentage;

    @Column(name = "ERS_PLN_SEQ_NO")
    private String planSequenceNo;

    @Column(name = "CREATED_BY")
    private String createdBy;

    @Column(name = "CREATED_DATE")
    private Date createdDate;

    @Column(name = "MODIFIED_BY")
    private String modifiedBy;

    @Column(name = "MODIFIED_DATE")
    private Date modifiedDate;

    @Column(name = "ERS_EVENT_LIMIT")
    private Double eventLimit;

    @Column(name = "ERS_ANNUAL_LIMIT")
    private Double annualLimit;

    @Column(name = "ERS_RET_OCCUPATION_CODE")
    private String retentionOccupationCode;

    @Column(name = "ERS_CERTIFICATE_NO")
    private String certificateNo;

    @Column(name = "ERS_NO_CERTIFICATES")
    private Integer noOfCertificates;

    @Column(name = "ERS_PREV_SEQ_NO")
    private String previousRiskSequenceNo;

    @Column(name = "ERS_PREV_SUM_INSURED")
    private Double previousSumInsured;

    @Column(name = "ERS_PREV_PREMIUM")
    private Double previousPremium;

    @Column(name = "ERS_CL_CLAIMS_PAID")
    private Double claimsPaid;

    @Column(name = "ERS_CL_CLAIMS_PENDING")
    private Double claimsPending;

    @Column(name = "ERS_DEPENDANT_TYPE")
    private String dependantType;

    @Column(name = "ERS_DEPENDANT_SEQ")
    private String dependantId;

    @Column(name = "ERS_TO_BE_CHARGED")
    private Character toBeCharged;

    @Column(name = "ERS_CHARGE_AMOUNT")
    private Double chargedAmount;

    @Column(name = "ERS_DATE_OF_BIRTH")
    private Date dateOfBirth;

    @Column(name = "ERS_PSC_SEC_CODE")
    private String sectionCode;

//    @Column(name = "ERS_ENDORSED")
//    private Character endorsed;

    @NotNull
    @Column(name = "ERS_DATE_ENDORSED")
    private Date endorsedDate;

    @Column(name = "ERS_POLICY_NO")
    private String policyNo;

    @NotNull
    @Column(name = "ERS_LOC_CODE")
    private String locationCode;

    @NotNull
    @Column(name = "ERS_NEW_RISK")
    private Character newRisk;

    @Column(name = "ERS_INSERT_ENDORSE_NO")
    private String insertEndorsementNo;

    @Column(name = "ERS_UPDATE_ENDORSE_NO")
    private String updateEndorsementNo;

    @Column(name = "ERS_DATE_DELETED")
    private Date deletedDate;

    @Column(name = "ERS_DELETE_ENDORSE_NO")
    private String deleteEndorsementNo;

    @NotNull
    @Column(name = "ERS_TRANSACTION_DATE")
    private Date transactionDate;

    @Column(name = "ERS_EXCESS_TXT")
    private String excessText;

    @Column(name = "ERS_LIMIT_TXT")
    private String limitText;

    @Column(name = "ERS_NAME_DESC")
    private String nameDescription;

//    @Column(name="ERS_MAIN_MEDILINK_NO")
//    private String mainMediaLinkNo;
//
//    @Column(name="ERS_INSPECTION_REQ")
//    private Character inspectionRequired;
//
//    @Column(name="ERS_OVERAGE_MEM")
//    private Character overageMembership;
//
//    @Column(name="ERS_EFFECT_FROM_DATE")
//    private Date effectiveFromDate;
//
//    @Column(name="ERS_EFFECT_TO_DATE")
//    private Date effectiveToDate;
//
//    @Column(name="ERS_RISK_ORDER_NO")
//    private Integer riskOrderNo;
//
//    @Column(name="ERS_PREV_EFFECT_TO_DATE")
//    private Date previousEffectiveToDate;

    @OneToMany(fetch = FetchType.LAZY)
    @JoinColumns({@JoinColumn(name = "EIN_ERS_SEQ_NO", referencedColumnName = "ERS_SEQ_NO"),
            @JoinColumn(name = "EIN_ERS_ELC_SEQ_NO", referencedColumnName = "ERS_ELC_SEQ_NO"),
            @JoinColumn(name = "EIN_ERS_ELC_EDT_SEQ_NO", referencedColumnName = "ERS_ELC_EDT_SEQ_NO")})

    private List<EndorsementRiskInformation> endorsementRiskInformationList;

    public EndorsementRiskPK getEndorsementRiskPK() {
        return endorsementRiskPK;
    }

    public void setEndorsementRiskPK(EndorsementRiskPK endorsementRiskPK) {
        this.endorsementRiskPK = endorsementRiskPK;
    }

    public String getRiskOrderNo() {
        return riskOrderNo;
    }

    public void setRiskOrderNo(String riskOrderNo) {
        this.riskOrderNo = riskOrderNo;
    }

    public String getRiskReferenceNo() {
        return riskReferenceNo;
    }

    public void setRiskReferenceNo(String riskReferenceNo) {
        this.riskReferenceNo = riskReferenceNo;
    }

    public String getReferenceField() {
        return referenceField;
    }

    public void setReferenceField(String referenceField) {
        this.referenceField = referenceField;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getNicNumber() {
        return nicNumber;
    }

    public void setNicNumber(String nicNumber) {
        this.nicNumber = nicNumber;
    }

    public Double getSumInsured() {
        return sumInsured;
    }

    public void setSumInsured(Double sumInsured) {
        this.sumInsured = sumInsured;
    }

    public Double getPremium() {
        return premium;
    }

    public void setPremium(Double premium) {
        this.premium = premium;
    }

    public String getRelationshipType() {
        return relationshipType;
    }

    public void setRelationshipType(String relationshipType) {
        this.relationshipType = relationshipType;
    }

    public String getRelationshipSequence() {
        return relationshipSequence;
    }

    public void setRelationshipSequence(String relationshipSequence) {
        this.relationshipSequence = relationshipSequence;
    }

    public Character getAllInclusive() {
        return allInclusive;
    }

    public void setAllInclusive(Character allInclusive) {
        this.allInclusive = allInclusive;
    }

    public Double getAllInclusivePercentage() {
        return allInclusivePercentage;
    }

    public void setAllInclusivePercentage(Double allInclusivePercentage) {
        this.allInclusivePercentage = allInclusivePercentage;
    }

    public String getPlanSequenceNo() {
        return planSequenceNo;
    }

    public void setPlanSequenceNo(String planSequenceNo) {
        this.planSequenceNo = planSequenceNo;
    }

    public String getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(String createdBy) {
        this.createdBy = createdBy;
    }

    public Date getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(Date createdDate) {
        this.createdDate = createdDate;
    }

    public String getModifiedBy() {
        return modifiedBy;
    }

    public void setModifiedBy(String modifiedBy) {
        this.modifiedBy = modifiedBy;
    }

    public Date getModifiedDate() {
        return modifiedDate;
    }

    public void setModifiedDate(Date modifiedDate) {
        this.modifiedDate = modifiedDate;
    }

    public Double getEventLimit() {
        return eventLimit;
    }

    public void setEventLimit(Double eventLimit) {
        this.eventLimit = eventLimit;
    }

    public Double getAnnualLimit() {
        return annualLimit;
    }

    public void setAnnualLimit(Double annualLimit) {
        this.annualLimit = annualLimit;
    }

    public String getRetentionOccupationCode() {
        return retentionOccupationCode;
    }

    public void setRetentionOccupationCode(String retentionOccupationCode) {
        this.retentionOccupationCode = retentionOccupationCode;
    }

    public String getCertificateNo() {
        return certificateNo;
    }

    public void setCertificateNo(String certificateNo) {
        this.certificateNo = certificateNo;
    }

    public Integer getNoOfCertificates() {
        return noOfCertificates;
    }

    public void setNoOfCertificates(Integer noOfCertificates) {
        this.noOfCertificates = noOfCertificates;
    }

    public String getPreviousRiskSequenceNo() {
        return previousRiskSequenceNo;
    }

    public void setPreviousRiskSequenceNo(String previousRiskSequenceNo) {
        this.previousRiskSequenceNo = previousRiskSequenceNo;
    }

    public Double getPreviousSumInsured() {
        return previousSumInsured;
    }

    public void setPreviousSumInsured(Double previousSumInsured) {
        this.previousSumInsured = previousSumInsured;
    }

    public Double getPreviousPremium() {
        return previousPremium;
    }

    public void setPreviousPremium(Double previousPremium) {
        this.previousPremium = previousPremium;
    }

    public Double getClaimsPaid() {
        return claimsPaid;
    }

    public void setClaimsPaid(Double claimsPaid) {
        this.claimsPaid = claimsPaid;
    }

    public Double getClaimsPending() {
        return claimsPending;
    }

    public void setClaimsPending(Double claimsPending) {
        this.claimsPending = claimsPending;
    }

    public String getDependantType() {
        return dependantType;
    }

    public void setDependantType(String dependantType) {
        this.dependantType = dependantType;
    }

    public String getDependantId() {
        return dependantId;
    }

    public void setDependantId(String dependantId) {
        this.dependantId = dependantId;
    }

    public Character getToBeCharged() {
        return toBeCharged;
    }

    public void setToBeCharged(Character toBeCharged) {
        this.toBeCharged = toBeCharged;
    }

    public Double getChargedAmount() {
        return chargedAmount;
    }

    public void setChargedAmount(Double chargedAmount) {
        this.chargedAmount = chargedAmount;
    }

    public Date getDateOfBirth() {
        return dateOfBirth;
    }

    public void setDateOfBirth(Date dateOfBirth) {
        this.dateOfBirth = dateOfBirth;
    }

    public String getSectionCode() {
        return sectionCode;
    }

    public void setSectionCode(String sectionCode) {
        this.sectionCode = sectionCode;
    }

//    public Character getEndorsed() {
//        return endorsed;
//    }
//
//    public void setEndorsed(Character endorsed) {
//        this.endorsed = endorsed;
//    }

    public Date getEndorsedDate() {
        return endorsedDate;
    }

    public void setEndorsedDate(Date endorsedDate) {
        this.endorsedDate = endorsedDate;
    }

    public String getPolicyNo() {
        return policyNo;
    }

    public void setPolicyNo(String policyNo) {
        this.policyNo = policyNo;
    }

    public String getLocationCode() {
        return locationCode;
    }

    public void setLocationCode(String locationCode) {
        this.locationCode = locationCode;
    }

    public Character getNewRisk() {
        return newRisk;
    }

    public void setNewRisk(Character newRisk) {
        this.newRisk = newRisk;
    }

    public String getInsertEndorsementNo() {
        return insertEndorsementNo;
    }

    public void setInsertEndorsementNo(String insertEndorsementNo) {
        this.insertEndorsementNo = insertEndorsementNo;
    }

    public String getUpdateEndorsementNo() {
        return updateEndorsementNo;
    }

    public void setUpdateEndorsementNo(String updateEndorsementNo) {
        this.updateEndorsementNo = updateEndorsementNo;
    }

    public Date getDeletedDate() {
        return deletedDate;
    }

    public void setDeletedDate(Date deletedDate) {
        this.deletedDate = deletedDate;
    }

    public String getDeleteEndorsementNo() {
        return deleteEndorsementNo;
    }

    public void setDeleteEndorsementNo(String deleteEndorsementNo) {
        this.deleteEndorsementNo = deleteEndorsementNo;
    }

    public Date getTransactionDate() {
        return transactionDate;
    }

    public void setTransactionDate(Date transactionDate) {
        this.transactionDate = transactionDate;
    }

    public String getExcessText() {
        return excessText;
    }

    public void setExcessText(String excessText) {
        this.excessText = excessText;
    }

    public String getLimitText() {
        return limitText;
    }

    public void setLimitText(String limitText) {
        this.limitText = limitText;
    }

    public String getNameDescription() {
        return nameDescription;
    }

    public void setNameDescription(String nameDescription) {
        this.nameDescription = nameDescription;
    }

//    public String getMainMediaLinkNo() {
//        return mainMediaLinkNo;
//    }
//
//    public void setMainMediaLinkNo(String mainMediaLinkNo) {
//        this.mainMediaLinkNo = mainMediaLinkNo;
//    }
//
//    public Character getInspectionRequired() {
//        return inspectionRequired;
//    }
//
//    public void setInspectionRequired(Character inspectionRequired) {
//        this.inspectionRequired = inspectionRequired;
//    }
//
//    public Character getOverageMembership() {
//        return overageMembership;
//    }
//
//    public void setOverageMembership(Character overageMembership) {
//        this.overageMembership = overageMembership;
//    }
//
//    public Date getEffectiveFromDate() {
//        return effectiveFromDate;
//    }
//
//    public void setEffectiveFromDate(Date effectiveFromDate) {
//        this.effectiveFromDate = effectiveFromDate;
//    }
//
//    public Date getEffectiveToDate() {
//        return effectiveToDate;
//    }
//
//    public void setEffectiveToDate(Date effectiveToDate) {
//        this.effectiveToDate = effectiveToDate;
//    }
//
//    public Integer getRiskOrderNo() {
//        return riskOrderNo;
//    }
//
//    public void setRiskOrderNo(Integer riskOrderNo) {
//        this.riskOrderNo = riskOrderNo;
//    }
//
//    public Date getPreviousEffectiveToDate() {
//        return previousEffectiveToDate;
//    }
//
//    public void setPreviousEffectiveToDate(Date previousEffectiveToDate) {
//        this.previousEffectiveToDate = previousEffectiveToDate;
//    }

    public List<EndorsementRiskInformation> getEndorsementRiskInformationList() {
        return endorsementRiskInformationList;
    }

    public void setEndorsementRiskInformationList(List<EndorsementRiskInformation> endorsementRiskInformationList) {
        this.endorsementRiskInformationList = endorsementRiskInformationList;
    }
}
