package com.infoinstech.claimsmodule.domain.model.underwriting.transaction;

import com.infoinstech.claimsmodule.domain.model.underwriting.master.ProductInformation;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.util.Date;

@Entity
@Table(name = "UW_T_END_INFORMATION")
public class EndorsementRiskInformation {

    @EmbeddedId
    private EndorsementRiskInformationPK endorsementRiskInformationPK;

    @NotNull
    @Column(name = "EIN_DESCRIPTION")
    private String description;

    @Column(name = "EIN_CHAR_VALUE")
    private String characterValue;

    @Column(name = "EIN_NUMBER_VALUE")
    private Double numberValue;

    @Column(name = "EIN_DATE_VALUE")
    private Date dateValue;

    @NotNull
    @Column(name = "EIN_PIF_PRD_CODE")
    private String productInformationProductCode;

    @NotNull
    @Column(name = "EIN_PIF_SEQ_NO")
    private String productInformationSequenceNo;

    @Column(name = "CREATED_BY")
    private String createdBy;

    @Column(name = "CREATED_DATE")
    private Date createdDate;

    @Column(name = "MODIFIED_BY")
    private String modifiedBy;

    @Column(name = "MODIFIED_DATE")
    private Date modifiedDate;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumns({
            @JoinColumn(name = "EIN_PIF_SEQ_NO", referencedColumnName = "PIF_SEQ_NO", insertable = false, updatable = false),
            @JoinColumn(name = "EIN_PIF_PRD_CODE", referencedColumnName = "PIF_PRD_CODE", insertable = false, updatable = false)
    })
    private ProductInformation productInformation;

    public EndorsementRiskInformationPK getEndorsementRiskInformationPK() {
        return endorsementRiskInformationPK;
    }

    public void setEndorsementRiskInformationPK(EndorsementRiskInformationPK endorsementRiskInformationPK) {
        this.endorsementRiskInformationPK = endorsementRiskInformationPK;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getCharacterValue() {
        return characterValue;
    }

    public void setCharacterValue(String characterValue) {
        this.characterValue = characterValue;
    }

    public Double getNumberValue() {
        return numberValue;
    }

    public void setNumberValue(Double numberValue) {
        this.numberValue = numberValue;
    }

    public Date getDateValue() {
        return dateValue;
    }

    public void setDateValue(Date dateValue) {
        this.dateValue = dateValue;
    }

    public String getProductInformationProductCode() {
        return productInformationProductCode;
    }

    public void setProductInformationProductCode(String productInformationProductCode) {
        this.productInformationProductCode = productInformationProductCode;
    }

    public String getProductInformationSequenceNo() {
        return productInformationSequenceNo;
    }

    public void setProductInformationSequenceNo(String productInformationSequenceNo) {
        this.productInformationSequenceNo = productInformationSequenceNo;
    }

    public String getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(String createdBy) {
        this.createdBy = createdBy;
    }

    public Date getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(Date createdDate) {
        this.createdDate = createdDate;
    }

    public String getModifiedBy() {
        return modifiedBy;
    }

    public void setModifiedBy(String modifiedBy) {
        this.modifiedBy = modifiedBy;
    }

    public Date getModifiedDate() {
        return modifiedDate;
    }

    public void setModifiedDate(Date modifiedDate) {
        this.modifiedDate = modifiedDate;
    }

    public ProductInformation getProductInformation() {
        return productInformation;
    }

    public void setProductInformation(ProductInformation productInformation) {
        this.productInformation = productInformation;
    }
}
