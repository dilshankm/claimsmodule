package com.infoinstech.claimsmodule.domain.model.underwriting.transaction;

import javax.persistence.Column;
import javax.persistence.Embeddable;
import java.io.Serializable;

@Embeddable
public class EndorsementRiskInformationPK implements Serializable {

    @Column(name = "EIN_SEQ_NO")
    private String sequenceNo;

    @Column(name = "EIN_ERS_SEQ_NO")
    private String riskSequenceNo;

    @Column(name = "EIN_ERS_ELC_SEQ_NO")
    private String locationSequenceNo;

    @Column(name = "EIN_ERS_ELC_EDT_SEQ_NO")
    private String endorsementSequenceNo;

    public EndorsementRiskInformationPK() {
    }

    public EndorsementRiskInformationPK(String sequenceNo, String riskSequenceNo, String locationSequenceNo, String endorsementSequenceNo) {
        this.sequenceNo = sequenceNo;
        this.riskSequenceNo = riskSequenceNo;
        this.locationSequenceNo = locationSequenceNo;
        this.endorsementSequenceNo = endorsementSequenceNo;
    }

    public String getSequenceNo() {
        return sequenceNo;
    }

    public void setSequenceNo(String sequenceNo) {
        this.sequenceNo = sequenceNo;
    }

    public String getRiskSequenceNo() {
        return riskSequenceNo;
    }

    public void setRiskSequenceNo(String riskSequenceNo) {
        this.riskSequenceNo = riskSequenceNo;
    }

    public String getLocationSequenceNo() {
        return locationSequenceNo;
    }

    public void setLocationSequenceNo(String locationSequenceNo) {
        this.locationSequenceNo = locationSequenceNo;
    }

    public String getEndorsementSequenceNo() {
        return endorsementSequenceNo;
    }

    public void setEndorsementSequenceNo(String endorsementSequenceNo) {
        this.endorsementSequenceNo = endorsementSequenceNo;
    }
}
