package com.infoinstech.claimsmodule.domain.model.underwriting.transaction;

import javax.persistence.Column;
import javax.persistence.Embeddable;
import java.io.Serializable;

@Embeddable
public class EndorsementRiskPK implements Serializable {

    @Column(name = "ERS_SEQ_NO")
    private String sequenceNo;

    @Column(name = "ERS_ELC_SEQ_NO")
    private String locationSequenceNo;

    @Column(name = "ERS_ELC_EDT_SEQ_NO")
    private String endorsementSequenceNo;

    public EndorsementRiskPK() {
    }

    public EndorsementRiskPK(String sequenceNo, String locationSequenceNo, String endorsementSequenceNo) {
        this.sequenceNo = sequenceNo;
        this.locationSequenceNo = locationSequenceNo;
        this.endorsementSequenceNo = endorsementSequenceNo;
    }

    public String getSequenceNo() {
        return sequenceNo;
    }

    public void setSequenceNo(String sequenceNo) {
        this.sequenceNo = sequenceNo;
    }

    public String getLocationSequenceNo() {
        return locationSequenceNo;
    }

    public void setLocationSequenceNo(String locationSequenceNo) {
        this.locationSequenceNo = locationSequenceNo;
    }

    public String getEndorsementSequenceNo() {
        return endorsementSequenceNo;
    }

    public void setEndorsementSequenceNo(String endorsementSequenceNo) {
        this.endorsementSequenceNo = endorsementSequenceNo;
    }
}
