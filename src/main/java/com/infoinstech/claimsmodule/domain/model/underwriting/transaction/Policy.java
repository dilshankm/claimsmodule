package com.infoinstech.claimsmodule.domain.model.underwriting.transaction;

import com.infoinstech.claimsmodule.domain.model.salesmarketing.master.SalesLocation;
import com.infoinstech.claimsmodule.domain.model.salesmarketing.reference.BusinessType;
import com.infoinstech.claimsmodule.domain.model.underwriting.master.Customer;
import com.infoinstech.claimsmodule.domain.model.underwriting.master.CustomerAddress;
import com.infoinstech.claimsmodule.domain.model.underwriting.master.Product;
import com.infoinstech.claimsmodule.domain.model.underwriting.reference.BusinessChannel;
import com.infoinstech.claimsmodule.domain.model.underwriting.reference.RefClass;
import lombok.Data;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.math.BigDecimal;
import java.util.Date;

@Data
@Entity
@Table(name = "UW_T_POLICIES")
@NamedStoredProcedureQueries(
        {
                @NamedStoredProcedureQuery(
                        name = "PU_GET_POLICY_RISK_CURRENT_PICTURE", procedureName = "PK_UW_WRAPPER.PU_BUILD_POLICY",
                        parameters = {
                                @StoredProcedureParameter(mode = ParameterMode.IN, name = "wkpolseqno", type = String.class),
                                @StoredProcedureParameter(mode = ParameterMode.IN, name = "wkdate", type = Date.class),
                                @StoredProcedureParameter(mode = ParameterMode.IN, name = "wkbase", type = String.class),
                                @StoredProcedureParameter(mode = ParameterMode.IN, name = "wkins_locations", type = String.class),
                                @StoredProcedureParameter(mode = ParameterMode.IN, name = "wkins_risks", type = String.class),
                                @StoredProcedureParameter(mode = ParameterMode.IN, name = "wkins_perils", type = String.class),
                                @StoredProcedureParameter(mode = ParameterMode.IN, name = "wkins_sub_perils", type = String.class),
                                @StoredProcedureParameter(mode = ParameterMode.IN, name = "wkins_peril_inv", type = String.class),
                                @StoredProcedureParameter(mode = ParameterMode.IN, name = "wkins_peril_inv_cat", type = String.class),
                                @StoredProcedureParameter(mode = ParameterMode.IN, name = "wkins_inventory", type = String.class),
                                @StoredProcedureParameter(mode = ParameterMode.IN, name = "wkins_inv_det", type = String.class),
                                @StoredProcedureParameter(mode = ParameterMode.IN, name = "wkins_occupations", type = String.class),
                                @StoredProcedureParameter(mode = ParameterMode.IN, name = "wkins_sub_occ", type = String.class),
                                @StoredProcedureParameter(mode = ParameterMode.IN, name = "wkins_funds", type = String.class),
                                @StoredProcedureParameter(mode = ParameterMode.IN, name = "wkins_comm_funds", type = String.class),
                                @StoredProcedureParameter(mode = ParameterMode.IN, name = "wkins_oth_chg", type = String.class),
                                @StoredProcedureParameter(mode = ParameterMode.IN, name = "wkins_taxes", type = String.class),
                                @StoredProcedureParameter(mode = ParameterMode.IN, name = "wkins_tax_funds", type = String.class),
                                @StoredProcedureParameter(mode = ParameterMode.IN, name = "wkins_com_info", type = String.class),
                                @StoredProcedureParameter(mode = ParameterMode.IN, name = "wkins_com_info_det", type = String.class),
                                @StoredProcedureParameter(mode = ParameterMode.IN, name = "wkins_fin_int", type = String.class),
                                @StoredProcedureParameter(mode = ParameterMode.IN, name = "wkins_docs", type = String.class),
                                @StoredProcedureParameter(mode = ParameterMode.IN, name = "wkins_gen_perils", type = String.class),
                                @StoredProcedureParameter(mode = ParameterMode.IN, name = "wkins_risk_fin_int", type = String.class),
                                @StoredProcedureParameter(mode = ParameterMode.IN, name = "wkins_info", type = String.class),
                                @StoredProcedureParameter(mode = ParameterMode.IN, name = "wkins_info_det", type = String.class),
                                @StoredProcedureParameter(mode = ParameterMode.IN, name = "wkins_clauses", type = String.class),
                                @StoredProcedureParameter(mode = ParameterMode.IN, name = "wkins_warranties", type = String.class),
                                @StoredProcedureParameter(mode = ParameterMode.IN, name = "wkins_conditions", type = String.class),
                                @StoredProcedureParameter(mode = ParameterMode.IN, name = "wkins_others", type = String.class),
                                @StoredProcedureParameter(mode = ParameterMode.IN, name = "wkins_commissions", type = String.class),
                                @StoredProcedureParameter(mode = ParameterMode.IN, name = "wkins_targets", type = String.class),
                                @StoredProcedureParameter(mode = ParameterMode.IN, name = "wkins_rel_pol", type = String.class),
                                @StoredProcedureParameter(mode = ParameterMode.OUT, name = "wknewpolseq", type = String.class),
                                @StoredProcedureParameter(mode = ParameterMode.OUT, name = "wksessionid", type = BigDecimal.class)
                        }
                ),

                @NamedStoredProcedureQuery(
                        name = "PU_GET_POLICY_RISK_INFO", procedureName = "PK_CL_CALL_CENTER.PU_GET_POLICY_RISK_INFO",
                        parameters = {
                                @StoredProcedureParameter(mode = ParameterMode.IN, name = "PA_PRS_SEQ", type = String.class),
                                @StoredProcedureParameter(mode = ParameterMode.IN, name = "PA_POL_SEQ", type = String.class),
                                @StoredProcedureParameter(mode = ParameterMode.REF_CURSOR, name = "cv_results", type = void.class)
                        }
                ),

                @NamedStoredProcedureQuery(
                        name = "PU_GET_RISK_INFO", procedureName = "PK_CL_CALL_CENTER.PU_GET_RISK_INFO",
                        parameters = {
                                @StoredProcedureParameter(mode = ParameterMode.IN, name = "PA_PRS_SEQ_NO", type = String.class),
                                @StoredProcedureParameter(mode = ParameterMode.IN, name = "PA_PIN_DESCRIPTION", type = String.class),
                                @StoredProcedureParameter(mode = ParameterMode.REF_CURSOR, name = "cv_results", type = void.class)
                        }
                )

        })

//@NamedStoredProcedureQueries(
//        {
//                @NamedStoredProcedureQuery(
//                        name = "SELECT_POL_INFO_TEST",procedureName="SELECT_POL_INFO_TEST",
//                        parameter = {
//
//                               @StoredProcedureParameter(mode=ParameterMode.REF_CURSOR,name = "cv_results",type = void.class)
//                        }
//                )
//        })
public class Policy {
    @Id
    @Column(name = "POL_SEQ_NO")
    private String sequenceNo;

    @NotNull
    @Column(name = "POL_PROPOSAL_NO")
    private String proposalNo;

    @Column(name = "POL_POLICY_NO")
    private String policyNo;

    @NotNull
    @Column(name = "POL_CLA_CODE")
    private String classCode;

    @Column(name = "POL_SURVEY")
    private Character survey;

    @Column(name = "POL_OPEN_POLICY")
    private Character openPolicy;

    @NotNull
    @Column(name = "POL_PRD_CODE")
    private String productCode;

    @Column(name = "POL_PERIOD_FROM")
    private Date periodFrom;

    @Column(name = "POL_PERIOD_TO")
    private Date periodTo;

    @Column(name = "POL_PREMIUM")
    private Double premium;

    @Column(name = "POL_QOT_SEQ_NO")
    private String quotationSequenceNo;

    @Column(name = "POL_CURRENCY")
    private String currency;

    @Column(name = "POL_CUS_CODE")
    private String customerCode;

    @Column(name = "POL_VOUCHER_NO")
    private String voucherNo;

    @NotNull
    @Column(name = "POL_DATE")
    private Date policyDate;

    @Column(name = "POL_DAYS")
    private Integer policyDays;

    @Column(name = "POL_SUM_INSURED")
    private Double sumInsured;

    @Column(name = "POL_GROUP")
    private Character groupPolicy;

    @Column(name = "POL_CERTIFICATE")
    private Character certificate;

    @Column(name = "POL_CLAIMS_AFTER")
    private Integer claimsAfter;

    @Column(name = "POL_REMARKS")
    private String remarks;

    @Column(name = "POL_TARGET")
    private String target;

    @Column(name = "POL_NET_OF_COMMISSION")
    private Character netOfCommission;

    @Column(name = "POL_AUTHORIZED_BY")
    private String authorizedBy;

    @Column(name = "POL_AUTHORIZED_DATE")
    private Date authorizedDate;

    @NotNull
    @Column(name = "POL_STATUS")
    private String status;

    @Column(name = "POL_CANCELLED_BY")
    private String cancelledBy;

    @Column(name = "POL_CANCELLED_DATE")
    private Date cancelledDate;

    @Column(name = "POL_COINSURANCE")
    private Character coinsurance;

    @Column(name = "POL_COINSURANCE_PCT")
    private Double coinsurancePercentage;

    @Column(name = "CREATED_BY")
    private String createdBy;

    @Column(name = "CREATED_DATE")
    private Date createdDate;

    @Column(name = "MODIFIED_BY")
    private String modifiedBy;

    @Column(name = "MODIFIED_DATE")
    private Date modifiedDate;

    @Column(name = "POL_MARKETING_EXECUTIVE_CODE")
    private String intermediaryCode;

    @Column(name = "POL_SLC_BRN_CODE")
    private String branchCode;

    @Column(name = "POL_ANY_ENDORSEMENTS")
    private Character anyEndorsements;

    @Column(name = "POL_LAST_ENDORSED_DATE")
    private Date lastEndorsedDate;

    @NotNull
    @Column(name = "POL_ANY_HISTORY")
    private Character anyHistory;

    @Column(name = "POL_LAST_HISTORY_DATE")
    private Date lastHistoryDate;

    @Column(name = "POL_BCH_CODE")
    private String businessChannelCode;

    @NotNull
    @Column(name = "POL_TRANSACTION_TYPE")
    private String transactionType;

    @Column(name = "POL_RI_TRANSFER_FLAG")
    private Character reinsuranceTransferFlag;

    @Column(name = "POL_RI_SUM_INSURED")
    private Double reinsuranceSumInsured;

    @Column(name = "POL_RC_OUTSTAND_AMT")
    private Double outstandingAmount;

    @Column(name = "POL_ENDORSEMENT_NO")
    private String endorsementNo;

    @Column(name = "POL_ENDORSEMENT_REMARKS")
    private String endorsementRemarks;

    @Column(name = "POL_RI_TREATY_DISTRIBUTED_FLAG")
    private Character reinsuranceTreatyDistributedFlag;

    @Column(name = "POL_NO_OF_COPIES")
    private Integer noOfCopies;

    @Column(name = "POL_TRANSACTION_AMOUNT")
    private Double transactionAmount;

    @Column(name = "POL_CALCULATION_TYPE")
    private String calculationType;

    @Column(name = "POL_PREV_POL_SEQ_NO")
    private String previousSequenceNo;

    @Column(name = "POL_REINSURANCE_REQ")
    private String reinsuranceRequired;

    @Column(name = "POL_UP_STEP_NO")
    private Integer uprStepNo;

    @Column(name = "POL_UP_TRANS_SEQ")
    private Integer uprTransactionSequenceNo;

    @Column(name = "POL_RC_OUTSTAND_CHQ_AMT")
    private Double outstandingChequeAmount;

    @Column(name = "POL_RC_RECEIPTING_DATE")
    private Date receiptingDate;

    @Column(name = "POL_RC_CURRENCY_RATE")
    private Double currencyRate;

    @Column(name = "POL_UP_NRP_PROCESSED")
    private String nrpprocessed;

    @Column(name = "POL_CANCELLED_TYPE")
    private Character cancelledType;

    @Column(name = "POL_FAMILY_UNIT")
    private Character familyUnit;

    @Column(name = "POL_BSS_BSS_CODE")
    private String intermediaryTypeCode;

    @Column(name = "POL_CDB_DEB_DEBTOR_CODE")
    private String debtorCode;

    @Column(name = "POL_CDB_DEB_PROFIT_CENTER")
    private String profitCenter;

    @Column(name = "POL_ANY_CLAIMS")
    private Character anyClaims;

    @Column(name = "POL_CLOSED_BY")
    private String closedBy;

    @Column(name = "POL_CLOSED_DATE")
    private Date closedDate;

    @Column(name = "POL_UP_OPEN_PERCENTAGE")
    private Double uprOpenPercentage;

    @Column(name = "POL_UP_PREM_ADV_PROCESSED")
    private Character premiumAdvancedProcessed;

    @Column(name = "POL_RC_PROCESSED")
    private Character processed;

    @Column(name = "POL_TOTAL_PREMIUM")
    private Double totalPremium;

    @Column(name = "POL_TOTAL_TRANSACTION_AMOUNT")
    private Double totalTransactionAmount;

    @Column(name = "POL_CANCELLED_AMOUNT")
    private Double cancelledAmount;

    @Column(name = "POL_EFFECTIVE_START_DATE")
    private Date effectiveStartDate;

    @Column(name = "POL_UP_CANCELLED_PROCESS")
    private Character uprCancelledProcess;

    @Column(name = "POL_CANCEL_EFFECTIVE_DATE")
    private Date cancelEffectiveDate;

    @Column(name = "POL_RC_CANCELLED_DATE")
    private Date receiptingCancelledDate;

    @Column(name = "POL_RC_CANCELLED_CURR_RATE")
    private Double receiptingCancelledCurrencyRate;

    @Column(name = "POL_COMM_AMMENDED")
    private Character commissionAmended;

    @Column(name = "POL_CREATED_BY")
    private String policyCreatedBy;

    @Column(name = "POL_CREATED_DATE")
    private Date policyCreatedDate;

    @Column(name = "POL_CANCELLED_TRANSACTION_AMT")
    private Double cancelledTransactionAmount;

    @Column(name = "POL_COMM_AMMENDED_DATE")
    private Date commissionAmendedDate;

    @Column(name = "POL_COMM_AMMEND_EFFECTIVE_DATE")
    private Date commissionAmendedEffectiveDate;

    @Column(name = "POL_REL_POLICY_NO")
    private String relatedPolicyNo;

    @Column(name = "POL_ADR_SEQ_NO")
    private String customerAddressSequenceNo;

    @Column(name = "POL_CUS_REMARKS")
    private String customerRemarks;

    @Column(name = "POL_PAYMENT_CAT")
    private String paymentCategory;

    @Column(name = "POL_ART_CODE")
    private String arrangementType;

    @Column(name = "POL_EXAMINED_BY")
    private String examinedBy;

    @Column(name = "POL_EXAMINED_DATE")
    private Date examinedDate;

    @Column(name = "POL_QUICK_UW")
    private Character quickUnderwriting;

    @Column(name = "POL_CANCELLED_REASON_CODE")
    private String cancelledReasonCode;

    @Column(name = "POL_MARK_CANCELLED_BY")
    private String markCancelledBy;

    @Column(name = "POL_MARK_CANCELLED_DATE")
    private Date markCancelledDate;

    @Column(name = "POL_EXMD_CANCELLED_BY")
    private String examinedCancelledBy;

    @Column(name = "POL_EXMD_CANCELLED_DATE")
    private Date examinedCancelledDate;

    @Column(name = "POL_ART_NO_OF_INSTALLMNTS")
    private String noOfInstallments;

    @NotNull
    @Column(name = "POL_MIGRATED_REC")
    private Character migratedRecord;

    @Column(name = "POL_OWNERSHIP_TRANSFER")
    private String ownershipTransfer;

    @Column(name = "POL_BLIST_AUTH_REQ")
    private Character blackListAuthorizationRequired;

    @Column(name = "POL_RI_EXT_AUTH_REQ")
    private Character riExtensionAuthorizationRequired;

    @Column(name = "POL_INWD_OTWD_FLAG")
    private Character inWardOutWardFlag;

    @Column(name = "POL_SP_REMARKS")
    private String specialRemarks;

    @Column(name = "POL_EVENT_LIMIT")
    private Double eventLimit;

    @Column(name = "POL_ANNUAL_LIMIT")
    private Double annualLimit;

    @Column(name = "POL_EXCESS_TXT")
    private String excessText;

    @Column(name = "POL_LIMIT_TXT")
    private String limitText;

    @Column(name = "POL_CREDIT_AUTHORIZE")
    private Character creditAuthorized;

    @Column(name = "POL_CREDIT_AUTHORIZED_BY")
    private String creditAuthorizedBy;

    @Column(name = "POL_CREDIT_AUTHRIZED_DATE")
    private Date creditAuthorizedDate;

    @Column(name = "POL_SPECIAL_ENDORSE_FLAG")
    private Character specialEndorsementFlag;

    @Column(name = "POL_SPECIAL_POLICY_FLAG")
    private Character specialPolicyFlag;
//
//    @Column(name="POL_BPARTY_CODE")
//    private String businessPartyCode;
//
//    @Column(name="POL_FINALIZED_BY")
//    private String finalizedBy;
//
//    @Column(name="POL_FINALIZED_DATE")
//    private Date finalizedDate;
//
//    @Column(name="POL_RI_EXT_AUTH_GRANT")
//    private Character reinsuranceExtensionAuthorizationGranted;
//
//    @Column(name="POL_INSPECTION")
//    private Character inspection;
//
//    @Column(name="POL_AUTO_CANCEL")
//    private Character autoCancelled;
//
//    @Column(name="POL_RATES_FOR_COVERS")
//    private Character ratesForCovers;
//
////    @Column(name="POL_RESET_REASON")
////    private String resetReason;
//
//    @Column(name="POL_INVOICE_TYPE")
//    private Character invoiceType;
//
//    @Column(name="POL_CREATED_BRANCH")
//    private String createdBranch;
//
////    @Column(name="POL_RETROACTVIE_DATE")
////    private Date retroActiveDate;
//
//    @Column(name="POL_MAINT_END_DATE")
//    private Date maintenanceEndDate;
//
//    @Column(name="POL_CANCEL_REMARKS")
//    private String cancellationRemarks;
//
////    @Column(name="POL_TRAN_STATUS")
////    private String transactionStatus;
//
//    @Column(name="POL_BPARTY_BSS_CODE")
//    private String businessPartyBssCode;
//
//    @Column(name="POL_AVERAGE_PREMIUM")
//    private Double averagePremium;
//
//    @Column(name="POL_GRP_DISCOUNT")
//    private Double groupDiscount;
//
//    @Column(name="POL_GRP_SIZE")
//    private Integer groupSize;
//
//    @Column(name="POL_NEW_CUS_FLG")
//    private Character customerPolicyStatus;
//
//    @Column(name="POL_CAN_ENDORSEMENT_NO")
//    private String cancelledEndorsementNo;
//
//    @Column(name = "DMS_REF_ID")
//    private String dmsReferenceId;
//
//    @Column(name="POL_INWD_COINS_REF_NO")
//    private String inwardCoinsuranceReferenceNo;

//    @Column(name="POL_RENWL_TRACK_NO")
//    private String renewalTrackNo;

    @OneToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "POL_CLA_CODE", referencedColumnName = "CLA_CODE", insertable = false, updatable = false)
    private RefClass refClass;

    @OneToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "POL_PRD_CODE", referencedColumnName = "PRD_CODE", insertable = false, updatable = false)
    private Product product;

    //OnetoOne on Quotation

    @OneToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "POL_CUS_CODE", referencedColumnName = "CUS_CODE", insertable = false, updatable = false)
    private Customer customer;

    @OneToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "POL_BCH_CODE", referencedColumnName = "BCH_CODE", insertable = false, updatable = false)
    private BusinessChannel businessChannel;

    @OneToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "POL_BSS_BSS_CODE", referencedColumnName = "BSS_BSS_CODE", insertable = false, updatable = false)
    private BusinessType businessType;

    @OneToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "POL_ADR_SEQ_NO", referencedColumnName = "ADR_SEQ_NO", insertable = false, updatable = false)
    private CustomerAddress customerAddress;

    @OneToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "POL_SLC_BRN_CODE", referencedColumnName = "SLC_BRN_CODE", insertable = false, updatable = false)
    private SalesLocation salesLocation;

    //OneToOne on ArrangementTypes;
}
