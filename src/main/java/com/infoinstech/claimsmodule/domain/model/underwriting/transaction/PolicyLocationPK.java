package com.infoinstech.claimsmodule.domain.model.underwriting.transaction;

import javax.persistence.Column;
import javax.persistence.Embeddable;
import javax.validation.constraints.NotNull;
import java.io.Serializable;

@Embeddable
public class PolicyLocationPK implements Serializable {

    @Column(name = "PLC_SEQ_NO")
    private String sequenceNo;
    @NotNull
    @Column(name = "PLC_POL_SEQ_NO")
    private String policySequenceNo;

    public PolicyLocationPK() {
    }

    public PolicyLocationPK(String sequenceNo, String policySequenceNo) {
        this.sequenceNo = sequenceNo;
        this.policySequenceNo = policySequenceNo;
    }

    public String getSequenceNo() {
        return sequenceNo;
    }

    public void setSequenceNo(String sequenceNo) {
        this.sequenceNo = sequenceNo;
    }

    public String getPolicySequenceNo() {
        return policySequenceNo;
    }

    public void setPolicySequenceNo(String policySequenceNo) {
        this.policySequenceNo = policySequenceNo;
    }
}
