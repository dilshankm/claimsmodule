package com.infoinstech.claimsmodule.domain.model.underwriting.transaction;

import javax.persistence.Column;
import javax.persistence.Embeddable;
import java.io.Serializable;

@Embeddable
public class PolicyPerilPK implements Serializable {

    @Column(name = "PPR_PRS_PLC_POL_SEQ_NO")
    private String policySequenceNo;

    @Column(name = "PPR_PRS_PLC_SEQ_NO")
    private String locationSequenceNo;

    @Column(name = "PPR_PRS_SEQ_NO")
    private String riskSequenceNo;

    @Column(name = "PPR_SEQ_NO")
    private String sequenceNo;

    public PolicyPerilPK() {
    }

    public PolicyPerilPK(String policySequenceNo, String locationSequenceNo, String riskSequenceNo, String sequenceNo) {
        this.policySequenceNo = policySequenceNo;
        this.locationSequenceNo = locationSequenceNo;
        this.riskSequenceNo = riskSequenceNo;
        this.sequenceNo = sequenceNo;
    }

    public String getPolicySequenceNo() {
        return policySequenceNo;
    }

    public void setPolicySequenceNo(String policySequenceNo) {
        this.policySequenceNo = policySequenceNo;
    }

    public String getLocationSequenceNo() {
        return locationSequenceNo;
    }

    public void setLocationSequenceNo(String locationSequenceNo) {
        this.locationSequenceNo = locationSequenceNo;
    }

    public String getRiskSequenceNo() {
        return riskSequenceNo;
    }

    public void setRiskSequenceNo(String riskSequenceNo) {
        this.riskSequenceNo = riskSequenceNo;
    }

    public String getSequenceNo() {
        return sequenceNo;
    }

    public void setSequenceNo(String sequenceNo) {
        this.sequenceNo = sequenceNo;
    }
}
