package com.infoinstech.claimsmodule.domain.packages;

import com.infoinstech.claimsmodule.dto.common.PolicyDetailSpecDTO;
import com.infoinstech.claimsmodule.domain.model.claims.transaction.ClaimIntimation;
import com.infoinstech.claimsmodule.services.util.DateConversion;
import oracle.jdbc.OracleTypes;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.StoredProcedureQuery;
import javax.sql.DataSource;
import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.ResultSet;
import java.util.*;

@Service
public class PK_CL_CALL_CENTER {

    @Autowired
    private DataSource dataSource;

    @PersistenceContext
    private EntityManager entityManager;

    //Get Claim ID- INT_CLAIM_NO  PK_CL_ANDROID_APP_DEV.FN_GEN_CLAIM_NO
    public String getClaimNo(String classCode, String productCode, String branchCode) {
        Set<ClaimIntimation> result = null;
        try {
            StoredProcedureQuery query = this.entityManager.createNamedStoredProcedureQuery("PU_GEN_CLAIM_NO");
            query.setParameter("WKBRANCH", branchCode);
            query.setParameter("WKCLASS", classCode);
            query.setParameter("WKPRODUCT", productCode);

            result = new HashSet<ClaimIntimation>(query.getResultList());

            String claimNo = result.toArray()[0].toString();
            System.out.println(claimNo);
            return claimNo;
        } catch (Exception ex) {
            ex.printStackTrace();
            return "";
        }
        finally {

            this.entityManager.close();
        }
    }


}
