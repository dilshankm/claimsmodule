package com.infoinstech.claimsmodule.domain.packages;

import com.infoinstech.claimsmodule.services.parameters.ReceiptingParametersService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.sql.DataSource;
import java.math.BigDecimal;

@Service
public class PK_CL_TAB_SEQUENCE {

    @Autowired
    private DataSource dataSource;

    @PersistenceContext
    private EntityManager entityManager;

    @Autowired
    private ReceiptingParametersService receiptingParametersService;

    //Table CL_T_INTIMATION
    //Get Claim Sequence  -INT_SEQ_NO
    public String getIntimationSequenceNo(String branchCode) {
        String polclaimNo = (String) entityManager.createNativeQuery("SELECT PK_CL_TAB_SEQUENCE.FN_CL_T_INTIMATION() FROM DUAL")
                .getSingleResult();

        String formattedSequenceNo = (String) entityManager.createNativeQuery("SELECT PK_CL_COMMON.FN_MAKE_SEQUENCE(?1,?2) FROM DUAL")
                .setParameter(1, Integer.parseInt(polclaimNo))
                .setParameter(2, branchCode)
                .getSingleResult();

        return formattedSequenceNo;
    }

    //Table CL_T_REP_MASTER
    // Get loss sequence AAD_SEQ_NO
    public String getJobAssignmentSequenceNo(String branchCode) {
        String repseqNo = (String) entityManager.createNativeQuery("SELECT pk_cl_tab_sequence.FN_CL_T_REP_MASTER() FROM DUAL")
                .getSingleResult();

        String formattedSequenceNo = (String) entityManager.createNativeQuery("SELECT PK_CL_COMMON.FN_MAKE_SEQUENCE(?1,?2) FROM DUAL")
                .setParameter(1, Integer.parseInt(repseqNo))
                .setParameter(2, branchCode)
                .getSingleResult();

        return formattedSequenceNo;
    }

    // Get loss job no ADD_JOB_NO
    public String getjobNo(String branchCode) {
        BigDecimal repseqNo = (BigDecimal) entityManager.createNativeQuery("SELECT PK_CL_TAB_SEQUENCE.FN_SEQ_CL_GEN_JOB() FROM DUAL")
                .getSingleResult();

        String formattedSequenceNo = (String) entityManager.createNativeQuery("SELECT PK_CL_COMMON.FN_MAKE_SEQUENCE2(?1,?2) FROM DUAL")
                .setParameter(1, repseqNo)
                .setParameter(2, branchCode)
                .getSingleResult();

        return formattedSequenceNo;
    }

    // Table  CL_T_ASSIGN_LOSS
    //get assign loss sequence AAS_SEQ_NO
    public String getAssignLossAdjusterSequenceNo(String branchCode) {

        String sequenceNo = (String) entityManager.createNativeQuery("SELECT pk_cl_tab_sequence.FN_CL_T_ASSIGN_LOSS() FROM DUAL")
                .getSingleResult();

        String formattedSequenceNo = (String) entityManager.createNativeQuery("SELECT PK_CL_COMMON.FN_MAKE_SEQUENCE(?1,?2) FROM DUAL")
                .setParameter(1, Integer.parseInt(sequenceNo))
                .setParameter(2, branchCode)
                .getSingleResult();

        return formattedSequenceNo;
    }

    public String getClaimHandlerSequenceNo(String branchCode) {

        BigDecimal sequenceNo = (BigDecimal) entityManager.createNativeQuery("SELECT PK_CL_TAB_SEQUENCE.FU_CL_T_CLAIM_HANDLER FROM DUAL")
                .getSingleResult();

        String formattedSequenceNo = (String) entityManager.createNativeQuery("SELECT PK_CL_COMMON.FN_MAKE_SEQUENCE(?1,?2) FROM DUAL")
                .setParameter(1, sequenceNo)
                .setParameter(2, branchCode)
                .getSingleResult();

        return formattedSequenceNo;

    }


    public String getProvisionDetailsSequenceNo(String branchCode) {

        BigDecimal sequenceNo = (BigDecimal) entityManager.createNativeQuery(
                " SELECT PK_CL_TAB_SEQUENCE.FN_CL_T_PROVISION_DTLS FROM DUAL").getSingleResult();

        String formattedSequenceNo = (String) entityManager.createNativeQuery("SELECT PK_CL_COMMON.FN_MAKE_SEQUENCE(?1,?2) FROM DUAL")
                .setParameter(1, sequenceNo)
                .setParameter(2, branchCode)
                .getSingleResult();

        return formattedSequenceNo;

    }

    public String getRevisionDetailsSequenceNo(String branchCode) {

        String sequenceNo = (String) entityManager.createNativeQuery(" SELECT PK_CL_TAB_SEQUENCE.FN_CL_T_PROVISION_DTLS FROM DUAL")
                .getSingleResult();

        String formattedSequenceNo = (String) entityManager.createNativeQuery("SELECT PK_CL_COMMON.FN_MAKE_SEQUENCE(?1,?2) FROM DUAL")
                .setParameter(1, Integer.parseInt(sequenceNo))
                .setParameter(2, branchCode)
                .getSingleResult();

        return formattedSequenceNo;

    }


    public String getClaimCurrentTaskSequenceNo() {

        String sequenceNo = (String) entityManager.createNativeQuery("SELECT PK_CL_TAB_SEQUENCE.FN_CL_T_CURTASK FROM DUAL")
                .getSingleResult();

        return sequenceNo;

    }

    public String getClaimTaskHistorySequenceNo() {

        String sequenceNo = (String) entityManager.createNativeQuery("SELECT PK_CL_TAB_SEQUENCE.FN_CL_H_TASKHIS FROM DUAL")
                .getSingleResult();

        return sequenceNo;

    }

    public String getExternalPersonDeviceSequenceNo() {

        String sequenceNo = (String) entityManager.createNativeQuery("SELECT PK_CL_TAB_SEQUENCE.FN_CL_M_EXT_DEVICE FROM DUAL")
                .getSingleResult();

        String formattedSequenceNo = (String) entityManager.createNativeQuery("SELECT PK_CL_COMMON.FN_MAKE_SEQUENCE(?1,?2) FROM DUAL")
                .setParameter(1, Integer.parseInt(sequenceNo))
                .setParameter(2, receiptingParametersService.getBaseAccountBranch())
                .getSingleResult();

        return formattedSequenceNo;

    }

    public String getLossAdjusterChargeSequenceNo(String branchCode) {
        String sequenceNo = (String) entityManager.createNativeQuery("SELECT PK_CL_TAB_SEQUENCE.FN_CL_T_LOSS_CHARGES() FROM DUAL")
                .getSingleResult();

        int clm = Integer.parseInt(sequenceNo);
        String formattedSequenceNo = (String) entityManager.createNativeQuery("SELECT PK_CL_COMMON.FN_MAKE_SEQUENCE(?1,?2) FROM DUAL")
                .setParameter(1, clm)
                .setParameter(2, branchCode)
                .getSingleResult();

        return formattedSequenceNo;
    }

    public String getImagesSequenceNo() {
        String sequenceNo = (String) entityManager.createNativeQuery("SELECT PK_CL_TAB_SEQUENCE.FN_CL_T_IMAGES() FROM DUAL")
                .getSingleResult();

        String formattedSequenceNo = (String) entityManager.createNativeQuery("SELECT PK_CL_COMMON.FN_MAKE_SEQUENCE(?1,?2) FROM DUAL")
                .setParameter(1, Integer.parseInt(sequenceNo))
                .setParameter(2, receiptingParametersService.getBaseAccountBranch())
                .getSingleResult();

        return formattedSequenceNo;
    }

    public String getNotificationLogSequenceNo() {

        String sequenceNo = (String) entityManager.createNativeQuery("SELECT PK_CL_TAB_SEQUENCE.FN_CL_T_FCM_LOG() FROM DUAL")
                .getSingleResult();

        String formattedSequenceNo = (String) entityManager.createNativeQuery("SELECT PK_CL_COMMON.FN_MAKE_SEQUENCE(?1,?2) FROM DUAL")
                .setParameter(1, Integer.parseInt(sequenceNo))
                .setParameter(2, receiptingParametersService.getBaseAccountBranch())
                .getSingleResult();

        return formattedSequenceNo;
    }

    public String getAssignQuestionSequenceNo() {

        String sequenceNo = (String) entityManager.createNativeQuery("SELECT PK_CL_TAB_SEQUENCE.FN_CL_T_ASSIGN_QUESTIONS() FROM DUAL")
                .getSingleResult();

        String formattedSequenceNo = (String) entityManager.createNativeQuery("SELECT PK_CL_COMMON.FN_MAKE_SEQUENCE(?1,?2) FROM DUAL")
                .setParameter(1, Integer.parseInt(sequenceNo))
                .setParameter(2, receiptingParametersService.getBaseAccountBranch())
                .getSingleResult();

        return formattedSequenceNo;
    }

    public String getDeviceStatusSequenceNo() {

        String sequenceNo = (String) entityManager.createNativeQuery("SELECT PK_CL_TAB_SEQUENCE.FN_CL_T_EXTDEV_STATUS() FROM DUAL")
                .getSingleResult();

        String formattedSequenceNo = (String) entityManager.createNativeQuery("SELECT PK_CL_COMMON.FN_MAKE_SEQUENCE(?1,?2) FROM DUAL")
                .setParameter(1, Integer.parseInt(sequenceNo))
                .setParameter(2, receiptingParametersService.getBaseAccountBranch())
                .getSingleResult();

        return formattedSequenceNo;
    }

    public String getNextSketchType() {

        return (String) entityManager.createNativeQuery("SELECT PK_CM_TAB_SEQUENCE.FN_CL_R_SKETCHES() FROM DUAL")
                .getSingleResult();
    }

    public String getNextSketch() {

        return (String) entityManager.createNativeQuery("SELECT PK_CM_TAB_SEQUENCE.FN_CL_T_DAMAGE_SKETCH() FROM DUAL")
                .getSingleResult();
    }
}
