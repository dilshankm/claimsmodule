package com.infoinstech.claimsmodule.domain.packages;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.sql.DataSource;
import java.math.BigDecimal;
import java.util.Date;

@Service
public class PK_CL_T_INTIMATION {

    @Autowired
    private DataSource dataSource;

    @PersistenceContext
    private EntityManager entityManager;

    //    String q1 = "SELECT DISTINCT POL_CUS_CODE FROM UW_T_POLICIES WHERE POL_MARKETING_EXECUTIVE_CODE = ?";
    private String q1 = "SELECT NVL(SUM(PRD_VALUE),0) FROM CL_T_PROVISION_DTLS WHERE PRD_INT_SEQ = ? AND PRD_PRV_TYPE = 'TRA000' AND PRD_FUNCTION_ID IN ('1.1','1.1.1','1.1.2')";


    public String getPolicySequenceNo(String policyNo, Date lossDate) {
        String polSeqNo = null;
        try {
            polSeqNo = (String) entityManager.createNativeQuery("SELECT PK_CL_T_INTIMATION.FN_POLICY_SEQNO(?1,?2) FROM DUAL")
                    .setParameter(1, policyNo)
                    .setParameter(2, lossDate)
                    .getSingleResult();
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        return polSeqNo;
    }

    public Double getEstAmt(String claimSeq) {
        BigDecimal estAmount = null;
        try {
            estAmount = (BigDecimal) entityManager.createNativeQuery("SELECT NVL(SUM(PRD_VALUE),0) FROM CL_T_PROVISION_DTLS WHERE PRD_INT_SEQ = ?1 AND PRD_PRV_TYPE = 'TRA000' AND PRD_FUNCTION_ID IN ('1.1','1.1.1','1.1.2')")
                    .setParameter(1, claimSeq)
                    .getSingleResult();
        } catch (Exception ex) {
            ex.printStackTrace();
        }

        return estAmount.doubleValue();
    }

    public String getLossAdjusterName(String code) {
        String name = null;
        try {
            name = (String) entityManager.createNativeQuery("SELECT PK_CL_M_EXTERNAL_PER.FN_GET_NAME_ONLY(?1) FROM DUAL")
                    .setParameter(1, code)
                    .getSingleResult();
        } catch (Exception ex) {
            ex.printStackTrace();
        }

        return name;
    }

}
