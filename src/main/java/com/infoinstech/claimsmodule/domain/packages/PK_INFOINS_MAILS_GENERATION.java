package com.infoinstech.claimsmodule.domain.packages;

import org.springframework.stereotype.Service;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.StoredProcedureQuery;

@Service
public class PK_INFOINS_MAILS_GENERATION {

    @PersistenceContext
    EntityManager entityManager;

    public void sendEmail(String taskCode, String claimNo) {
        StoredProcedureQuery query = this.entityManager.createNamedStoredProcedureQuery("PU_SEND_EMAIL");
        query.setParameter("PaTaskCode", taskCode);
        query.setParameter("PaClaimNo", claimNo);
        query.setParameter("PaTPName", "");
        query.setParameter("PaTPRskName", "");
        query.setParameter("PaAasCode", "");
        query.setParameter("PaRepairer", "");
        query.setParameter("PaChqNo", "");
        query.setParameter("PaChqAmt", 0.00);
        query.setParameter("PaPrdCode", "");
        query.execute();

        entityManager.close();

    }
}
