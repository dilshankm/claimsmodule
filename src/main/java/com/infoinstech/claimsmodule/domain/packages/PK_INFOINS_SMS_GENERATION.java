package com.infoinstech.claimsmodule.domain.packages;

import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.StoredProcedureQuery;

@Service
@Slf4j
public class PK_INFOINS_SMS_GENERATION {

    @PersistenceContext
    EntityManager entityManager;

    public void sendSMS(String taskCode, String claimNo) {
        log.info("Sending SMS via PK_INFOINS_SMS_GENERATION");
        log.debug("Task Code : " + taskCode + " Claim No : " +  claimNo);
        StoredProcedureQuery query = this.entityManager.createNamedStoredProcedureQuery("PU_SEND_SMS");
        query.setParameter("PaTaskCode", taskCode);
        query.setParameter("PaClaimNo", claimNo);
        query.setParameter("PaTPName", "");
        query.setParameter("PaTPRskName", "");
        query.setParameter("PaAasCode", "");
        query.setParameter("PaRepairer", "");
        query.setParameter("PaChqNo", "");
        query.setParameter("PaChqAmt", 0.00);
        query.setParameter("PaPrdCode", "");
        query.execute();

        entityManager.close();
        log.info("SMS Sent via PK_INFOINS_SMS_GENERATION");
    }
}
