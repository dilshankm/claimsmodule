package com.infoinstech.claimsmodule.domain.packages;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.sql.DataSource;
import java.math.BigDecimal;

@Service
public class PK_RC_DEBIT_NOTE {

    @Autowired
    private DataSource dataSource;

    @PersistenceContext
    private EntityManager entityManager;

    public BigDecimal getDebitOutstandingAmountForPolicyNo(String policyNo) {

        BigDecimal outstandingAmount = (BigDecimal) entityManager.createNativeQuery("SELECT PK_RC_T_DEBIT_NOTE.FN_GET_OUTSTANDING_AMT_POL(?1) FROM DUAL")
                .setParameter(1, policyNo)
                .getSingleResult();

        entityManager.close();

        return outstandingAmount;
    }
}
