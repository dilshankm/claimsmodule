package com.infoinstech.claimsmodule.domain.repository;

import com.infoinstech.claimsmodule.domain.model.Session;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;

public interface SessionRepository extends JpaRepository<Session, String> {
    Optional<Session> findBySessionTokenLike(String sessionToken);

    Optional<Session> findBySessionTokenAndIsActive(String sessionToken, Character isActive);

}

