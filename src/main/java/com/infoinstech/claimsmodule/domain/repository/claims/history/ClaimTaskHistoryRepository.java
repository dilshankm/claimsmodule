package com.infoinstech.claimsmodule.domain.repository.claims.history;

import com.infoinstech.claimsmodule.domain.model.claims.history.ClaimTaskHistory;
import org.springframework.data.jpa.repository.JpaRepository;

public interface ClaimTaskHistoryRepository extends JpaRepository<ClaimTaskHistory, String> {
}

