package com.infoinstech.claimsmodule.domain.repository.claims.history;

import com.infoinstech.claimsmodule.domain.model.claims.history.OtherChargesPaymentHistory;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;

/**
 * Created by dushman on 4/20/18.
 */
public interface OtherChargesPaymentHistoryRepository extends JpaRepository<OtherChargesPaymentHistory, String> {

    Optional<OtherChargesPaymentHistory> findByReqSequenceNo(String reqSequenceNo);
}
