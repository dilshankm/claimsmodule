package com.infoinstech.claimsmodule.domain.repository.claims.history;

import com.infoinstech.claimsmodule.domain.model.claims.history.RequisitionHistory;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;

/**
 * Created by dushman on 4/20/18.
 */

public interface RequisitionHistoryRepository extends JpaRepository<RequisitionHistory, String> {

    Optional<RequisitionHistory> findBySequenceNo(String sequenceNo);

}
