package com.infoinstech.claimsmodule.domain.repository.claims.master;

import com.infoinstech.claimsmodule.domain.model.claims.master.ExternalPersonDevice;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface ExternalPersonDeviceRepository extends JpaRepository<ExternalPersonDevice, String> {

    ExternalPersonDevice findFirstByExternalPersonCodeAndRegisteredAndStatusOrderByDeviceOrderAsc
            (String externalPersonCode, Character registered, Character status);

    ExternalPersonDevice findFirstByOrderByDeviceTokenDesc();

    ExternalPersonDevice findByExternalPersonCodeAndRegistered(String externalPersonCode, Character registered);

    ExternalPersonDevice findByDeviceToken(String deviceToken);

    ExternalPersonDevice findByDeviceTokenAndGcmKey(String deviceToken, String gcmKey);

    ExternalPersonDevice findByExternalPersonCodeAndDeviceToken(String externalPerson, String deviceToken);

    List<ExternalPersonDevice> findByRegisteredAndStatusIn(Character registered, List<Character> status);

}
