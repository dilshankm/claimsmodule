package com.infoinstech.claimsmodule.domain.repository.claims.master;

import com.infoinstech.claimsmodule.domain.model.claims.master.ExternalPerson;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface ExternalPersonRepository extends JpaRepository<ExternalPerson, String> {

    ExternalPerson findByCode(String code);

    List<ExternalPerson> findByCategoryCodeAndStatus(String categoryCode, Character status);

    List<ExternalPerson> findByCategoryCode(String categoryCode);

    List<ExternalPerson> findAllByCategoryCodeInAndStatus(List<String> categoryCodes, Character status);


}
