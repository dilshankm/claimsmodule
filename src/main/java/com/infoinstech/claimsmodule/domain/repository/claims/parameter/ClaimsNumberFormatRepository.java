package com.infoinstech.claimsmodule.domain.repository.claims.parameter;

import com.infoinstech.claimsmodule.domain.model.claims.paramater.ClaimsNumberFormat;
import org.springframework.data.jpa.repository.JpaRepository;

public interface ClaimsNumberFormatRepository extends JpaRepository<ClaimsNumberFormat, String> {


}
