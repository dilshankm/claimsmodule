package com.infoinstech.claimsmodule.domain.repository.claims.parameter;

import com.infoinstech.claimsmodule.domain.model.claims.paramater.ClaimsParameter;
import org.springframework.data.jpa.repository.JpaRepository;

public interface ClaimsParameterRepository extends JpaRepository<ClaimsParameter, String> {

    ClaimsParameter findByBranchCodeAndClassCodeAndProductCode(String branchCode, String classCode, String productCode);
}
