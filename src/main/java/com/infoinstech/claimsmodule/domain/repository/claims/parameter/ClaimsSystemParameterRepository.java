package com.infoinstech.claimsmodule.domain.repository.claims.parameter;

import com.infoinstech.claimsmodule.domain.model.claims.paramater.ClaimsSystemParameter;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface ClaimsSystemParameterRepository extends JpaRepository<ClaimsSystemParameter, String> {

    ClaimsSystemParameter findBySpmIndexNo(String indexNo);

    List<ClaimsSystemParameter> findAllBySpmIndexNoIn(List<String> spmIndices);
}
