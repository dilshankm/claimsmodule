package com.infoinstech.claimsmodule.domain.repository.claims.reference;

import com.infoinstech.claimsmodule.domain.model.claims.reference.ChargeType;
import org.springframework.data.jpa.repository.JpaRepository;

public interface ChargeTypeRepository extends JpaRepository<ChargeType, String> {
}
