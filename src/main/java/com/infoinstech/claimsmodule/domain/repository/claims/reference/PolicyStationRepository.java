package com.infoinstech.claimsmodule.domain.repository.claims.reference;

import com.infoinstech.claimsmodule.domain.model.claims.reference.PolicyStation;
import org.springframework.data.jpa.repository.JpaRepository;

public interface PolicyStationRepository extends JpaRepository<PolicyStation, String> {

    PolicyStation findAllByCpsCode(String code);

}
