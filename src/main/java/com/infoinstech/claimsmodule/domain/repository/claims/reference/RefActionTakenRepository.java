package com.infoinstech.claimsmodule.domain.repository.claims.reference;

import com.infoinstech.claimsmodule.domain.model.claims.reference.RefActionTaken;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface RefActionTakenRepository extends JpaRepository<RefActionTaken,String> {

    List<RefActionTaken> findByStatus(Character status);
}
