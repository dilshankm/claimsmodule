package com.infoinstech.claimsmodule.domain.repository.claims.reference;

import com.infoinstech.claimsmodule.domain.model.claims.reference.RefAnswer;
import org.springframework.data.jpa.repository.JpaRepository;

public interface RefAnswerRepository extends JpaRepository<RefAnswer, String> {


}
