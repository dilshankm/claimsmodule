package com.infoinstech.claimsmodule.domain.repository.claims.reference;

import com.infoinstech.claimsmodule.domain.model.claims.reference.RefExcessType;
import org.springframework.data.jpa.repository.JpaRepository;

public interface RefExcessTypeRepository extends JpaRepository<RefExcessType, String> {


}

