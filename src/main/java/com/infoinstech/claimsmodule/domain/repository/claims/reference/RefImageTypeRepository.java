package com.infoinstech.claimsmodule.domain.repository.claims.reference;

import com.infoinstech.claimsmodule.domain.model.claims.reference.RefImageType;
import org.springframework.data.jpa.repository.JpaRepository;

public interface RefImageTypeRepository extends JpaRepository<RefImageType, String> {
}
