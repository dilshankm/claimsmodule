package com.infoinstech.claimsmodule.domain.repository.claims.reference;

import com.infoinstech.claimsmodule.domain.model.claims.reference.RefImageUploadType;
import org.springframework.data.jpa.repository.JpaRepository;

public interface RefImageUploadTypeRepository extends JpaRepository<RefImageUploadType, String> {
}
