package com.infoinstech.claimsmodule.domain.repository.claims.reference;

import com.infoinstech.claimsmodule.domain.model.claims.reference.RefInformationMode;
import org.springframework.data.jpa.repository.JpaRepository;

public interface RefInformationModeRepository extends JpaRepository<RefInformationMode, String> {
}

