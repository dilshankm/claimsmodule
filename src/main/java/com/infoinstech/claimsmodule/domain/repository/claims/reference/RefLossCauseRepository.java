package com.infoinstech.claimsmodule.domain.repository.claims.reference;

import com.infoinstech.claimsmodule.domain.model.claims.reference.RefLossCause;
import org.springframework.data.jpa.repository.JpaRepository;

public interface RefLossCauseRepository extends JpaRepository<RefLossCause, String> {

    public RefLossCause findByCode(String code);
}
