package com.infoinstech.claimsmodule.domain.repository.claims.reference;

import com.infoinstech.claimsmodule.domain.model.claims.reference.RefQuestion;
import org.springframework.data.jpa.repository.JpaRepository;

public interface RefQuestionRepository extends JpaRepository<RefQuestion, String> {

}
