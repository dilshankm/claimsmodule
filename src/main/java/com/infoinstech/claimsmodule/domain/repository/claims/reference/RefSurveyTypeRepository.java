package com.infoinstech.claimsmodule.domain.repository.claims.reference;

import com.infoinstech.claimsmodule.domain.model.claims.reference.RefSurveyType;
import org.springframework.data.jpa.repository.JpaRepository;

public interface RefSurveyTypeRepository extends JpaRepository<RefSurveyType, String> {

}
