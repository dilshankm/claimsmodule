package com.infoinstech.claimsmodule.domain.repository.claims.reference;

import com.infoinstech.claimsmodule.domain.model.claims.reference.RefVehicleSketch;
import org.springframework.data.jpa.repository.JpaRepository;

public interface RefVehicleSketchRepository extends JpaRepository<RefVehicleSketch, String> {
}
