package com.infoinstech.claimsmodule.domain.repository.claims.transaction;

import com.infoinstech.claimsmodule.domain.model.claims.transaction.AssignLossAdjuster;
import com.infoinstech.claimsmodule.domain.model.claims.transaction.AssignLossAdjusterPK;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Date;
import java.util.List;

public interface AssignLossAdjusterRepository extends JpaRepository<AssignLossAdjuster, AssignLossAdjusterPK> {


    AssignLossAdjuster findTopByAssignLossAdjusterPK_IntimationSequenceNoAndAssignLossAdjusterPK_JobSequenceNo(
            String intimationSequenceNo, String jobSequenceNo);

    AssignLossAdjuster findByAssignLossAdjusterPK_JobSequenceNo(String jobSequenceNo);

    List<AssignLossAdjuster> findByExternalPersonCodeAndCreatedDateBetween(String externalPersonCode, Date beforeDate, Date afterDate);

    List<AssignLossAdjuster> findByStatus(Character status);

    List<AssignLossAdjuster> findByCreatedDateBetweenAndStatus(Date beforeDate, Date afterDate, Character status);

    List<AssignLossAdjuster> findByExternalPersonCodeAndStatus(String externalPersonCode, Character status);

    List<AssignLossAdjuster> findByExternalPersonCodeAndCreatedDateBetweenAndStatus(String externalPersonCode, Date beforeDate, Date afterDate, Character status);

    List<AssignLossAdjuster> findByStatusIsIn(List<Character> status);

    long countByStatus(Character status);

    long countByExternalPersonCodeAndStatus(String externalPersonCode, Character status);

    long countByCreatedDateIsBetweenAndStatus(Date fromDate, Date toDate, Character status);

    long countByExternalPersonCodeAndCreatedDateIsBetweenAndStatus(String externalPersonCode, Date fromDate, Date toDate, Character status);

    List<AssignLossAdjuster> findByAssignLossAdjusterPK_IntimationSequenceNo(String claimSequence);

    List<AssignLossAdjuster> findByExternalPersonCode(String externalPersonCode);


}
