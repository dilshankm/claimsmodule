package com.infoinstech.claimsmodule.domain.repository.claims.transaction;

import com.infoinstech.claimsmodule.domain.model.claims.transaction.AssignQuestion;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface AssignQuestionRepository extends JpaRepository<AssignQuestion, String> {

    AssignQuestion findFirstByClaimSequenceNoAndJobAssignmentSequenceNoOrderByRevisionNoDesc(
            String claimSequenceNo, String jobAssignmentSequenceNo);

    AssignQuestion findFirstByJobAssignmentSequenceNoOrderByRevisionNoDesc(String jobSequenceNo);

    List<AssignQuestion> findByJobAssignmentSequenceNoAndRevisionNo(String jobSequenceNo, String revisionNo);


    AssignQuestion findFirstByNotificationSequenceNoAndNotificationAssignmentSequenceNoOrderByRevisionNoDesc(
            String notificationSequenceNo, String notificationAssignmentSequenceNo);

    AssignQuestion findFirstByNotificationAssignmentSequenceNoOrderByRevisionNoDesc(String notificationAssignmentSequenceNo);

    List<AssignQuestion> findByNotificationAssignmentSequenceNoAndRevisionNo(String notificationAssignmentSequenceNo, String revisionNo);

    List<AssignQuestion> findByNotificationAssignmentSequenceNo(String notificationAssignmentSequenceNo);
}
