package com.infoinstech.claimsmodule.domain.repository.claims.transaction;

import com.infoinstech.claimsmodule.domain.model.claims.transaction.ClaimCurrentTask;
import org.springframework.data.jpa.repository.JpaRepository;

public interface ClaimCurrentTaskRepository extends JpaRepository<ClaimCurrentTask, String> {

    ClaimCurrentTask findByClaimNo(String claimNo);
}
