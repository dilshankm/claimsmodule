package com.infoinstech.claimsmodule.domain.repository.claims.transaction;

import com.infoinstech.claimsmodule.domain.model.claims.transaction.ClaimHandler;
import org.springframework.data.jpa.repository.JpaRepository;

public interface ClaimHandlerRepository extends JpaRepository<ClaimHandler, String> {
}
