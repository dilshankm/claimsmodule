package com.infoinstech.claimsmodule.domain.repository.claims.transaction;

import com.infoinstech.claimsmodule.domain.model.claims.transaction.ClaimIntimation;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.*;

public interface ClaimIntimationRepository extends JpaRepository<ClaimIntimation, String> {

    ClaimIntimation findByClaimNo(String claimNo);

    ClaimIntimation findBySequenceNo(String sequenceNo);

    Set<ClaimIntimation> findByPolicyNoAndRiskName(String policyNo, String riskName);

    List<ClaimIntimation> findByPolicyNoAndRiskSequenceNoOrderByIntimationDateDesc(String policyNo, String riskSequenceNo);

    Set<ClaimIntimation> findByPolicyNoAndRiskNameAndLossDate(String policyNo, String riskName, Date lossDate);

    List<ClaimIntimation> findByPolicySequenceNoAndRiskSequenceNo(String policySequenceNo, String riskSequenceNo);

    List<ClaimIntimation> findByProductCodeAndClassCodeAndPolicyBranchAndCurrencyOrderByPolicyBranchAscClassCodeAscProductCodeAsc(
            String productCode, String classCode, String policyBranch, String currency);

    List<ClaimIntimation> findByOrderByPolicyBranchAscClassCodeAscProductCodeAsc();

    List<ClaimIntimation> findByClaimNoContaining(String claimNo);

    ArrayList<ClaimIntimation> findByIntimationDateBetweenAndCurrencyAndBranchCodeContainingAndClassCodeContainingAndProductCodeContainingOrderByBranchCodeAscClaimStatusAscCurrencyAscPolicyNoAsc(Date from, Date to, String currency, String branch, String cls, String product);

}
