package com.infoinstech.claimsmodule.domain.repository.claims.transaction;

import com.infoinstech.claimsmodule.domain.model.claims.transaction.ClaimNotification;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Date;
import java.util.List;

public interface ClaimNotificationRepository extends JpaRepository<ClaimNotification, String> {

    ClaimNotification findBySequenceNo(String sequenceNo);

    ClaimNotification findByNotificationNo(String notificationNo);

    List<ClaimNotification> findAllByNotificationNoContainingAndCoverNoteNoContainingAndRiskNameContainingAndContactNoContaining(String notificationNo, String coverNoteNo, String riskName, String contactNo);

    List<ClaimNotification> findByCoverNoteNoAndLossDate(String coverNoteNo, Date lossDate);
}
