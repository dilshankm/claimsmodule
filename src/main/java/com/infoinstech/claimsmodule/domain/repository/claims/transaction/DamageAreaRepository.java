package com.infoinstech.claimsmodule.domain.repository.claims.transaction;

import com.infoinstech.claimsmodule.domain.model.claims.transaction.DamageArea;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface DamageAreaRepository extends JpaRepository<DamageArea, String> {

    List<DamageArea> findAllByClaimNo(String claimNo);

    List<DamageArea> findByNotificationNo(String notificationNo);

    DamageArea findByClaimNoAndUserType(String claimNo, String userType);
}
