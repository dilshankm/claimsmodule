package com.infoinstech.claimsmodule.domain.repository.claims.transaction;

import com.infoinstech.claimsmodule.domain.model.claims.transaction.DeviceStatusChangeJobLog;
import org.springframework.data.jpa.repository.JpaRepository;

public interface DeviceStatusChangeJobLogRepository extends JpaRepository<DeviceStatusChangeJobLog,String> {
}
