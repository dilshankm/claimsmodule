package com.infoinstech.claimsmodule.domain.repository.claims.transaction;

import com.infoinstech.claimsmodule.domain.model.claims.transaction.DeviceStatusLog;
import com.infoinstech.claimsmodule.domain.model.claims.transaction.DeviceStatusLogPK;
import org.springframework.data.jpa.repository.JpaRepository;

public interface DeviceStatusLogRepository extends JpaRepository<DeviceStatusLog, DeviceStatusLogPK> {

    DeviceStatusLog findFirstByDeviceStatusLogPK_AssessorCodeAndDeviceStatusLogPK_DeviceSequenceNoAndDeviceTokenOrderByLogOrderDesc
            (String assessorCode, String deviceSequenceNo, Integer deviceToken);
}
