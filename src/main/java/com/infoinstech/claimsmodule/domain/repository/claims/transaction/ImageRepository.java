package com.infoinstech.claimsmodule.domain.repository.claims.transaction;

import com.infoinstech.claimsmodule.domain.model.claims.transaction.Image;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface ImageRepository extends JpaRepository<Image, String> {

    List<Image> findByJobNoAndImageTypeCode(String jobNo, String imageTypeCode);

    List<Image> findByAssignmentNoAndImageTypeCode(String assignmentNo, String imageTypeCode);

    List<Image> findByNotificationNoAndAssignmentNo(String notificationNo, String assignmentNo);

}


