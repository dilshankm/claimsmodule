package com.infoinstech.claimsmodule.domain.repository.claims.transaction;

import com.infoinstech.claimsmodule.domain.model.claims.transaction.JobAssignment;
import com.infoinstech.claimsmodule.domain.model.claims.transaction.JobAssignmentPK;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.HashSet;

public interface JobAssignmentRepository extends JpaRepository<JobAssignment, JobAssignmentPK> {

    JobAssignment findByJobNo(String jobNo);

    HashSet<JobAssignment> findAllByJobAssignmentPK_IntimationSequenceNo(String intimationSequenceNo);

    JobAssignment findByJobAssignmentPK_IntimationSequenceNoAndJobAssignmentPK_SequenceNo(String intimationSequenceNo, String sequenceNo);

    JobAssignment findByJobAssignmentPK_SequenceNo(String sequenceNo);


}
