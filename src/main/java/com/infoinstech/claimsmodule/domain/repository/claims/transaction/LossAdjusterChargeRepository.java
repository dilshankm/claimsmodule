package com.infoinstech.claimsmodule.domain.repository.claims.transaction;

import com.infoinstech.claimsmodule.domain.model.claims.transaction.LossAdjusterCharge;
import org.springframework.data.jpa.repository.JpaRepository;

public interface LossAdjusterChargeRepository extends JpaRepository<LossAdjusterCharge, String> {
}
