package com.infoinstech.claimsmodule.domain.repository.claims.transaction;

import com.infoinstech.claimsmodule.domain.model.claims.transaction.NotificationAdjusterCharge;
import org.springframework.data.jpa.repository.JpaRepository;

public interface NotificationAdjusterChargeRepository extends JpaRepository<NotificationAdjusterCharge, String> {


}
