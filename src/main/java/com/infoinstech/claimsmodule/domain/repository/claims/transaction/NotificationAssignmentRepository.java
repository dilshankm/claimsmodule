package com.infoinstech.claimsmodule.domain.repository.claims.transaction;

import com.infoinstech.claimsmodule.domain.model.claims.transaction.NotificationAssignment;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface NotificationAssignmentRepository extends JpaRepository<NotificationAssignment, String> {

    List<NotificationAssignment> findByNotificationAssignmentPK_NotificationSequenceNo(String notificationSequenceNo);

    NotificationAssignment findByNotificationAssignmentPK_SequenceNo(String sequenceNo);

    NotificationAssignment findByJobNo(String jobNo);
}
