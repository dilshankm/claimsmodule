package com.infoinstech.claimsmodule.domain.repository.claims.transaction;

import com.infoinstech.claimsmodule.domain.model.claims.transaction.NotificationLog;
import org.springframework.data.jpa.repository.JpaRepository;

public interface NotificationLogRepository extends JpaRepository<NotificationLog, String> {

    NotificationLog findByClaimSequenceNo(String claimSequenceNo);
}


