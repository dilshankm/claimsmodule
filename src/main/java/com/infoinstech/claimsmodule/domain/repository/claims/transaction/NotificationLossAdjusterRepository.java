package com.infoinstech.claimsmodule.domain.repository.claims.transaction;

import com.infoinstech.claimsmodule.domain.model.claims.transaction.NotificationLossAdjuster;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Date;
import java.util.List;

public interface NotificationLossAdjusterRepository extends JpaRepository<NotificationLossAdjuster, String> {

    NotificationLossAdjuster findByNotificationLossAdjusterPK_AssignmentSequenceNo(String assignmentSequenceNo);

    List<NotificationLossAdjuster> findByStatusIsIn(List<Character> statuses);

    List<NotificationLossAdjuster> findByExternalPersonCodeAndStatus(String externalPersonCode,Character status);

    List<NotificationLossAdjuster> findByExternalPersonCodeAndCreatedDateBetweenAndStatus(String externalPersonCode, Date fromDate, Date toDate, Character status);

    List<NotificationLossAdjuster> findByStatus(Character status);

    List<NotificationLossAdjuster> findByCreatedDateBetweenAndStatus(Date  fromDate, Date  toDate, Character status);

    long countByExternalPersonCodeAndStatus(String externalPersonCode,Character status);

    long countByExternalPersonCodeAndCreatedDateBetweenAndStatus(String externalPersonCode, Date fromDate, Date toDate, Character status);

    long countByStatus(Character status);

    long countByCreatedDateBetweenAndStatus(Date  fromDate, Date  toDate, Character status);

    List<NotificationLossAdjuster> findByExternalPersonCodeAndCreatedDateBetween(String externalPersonCode, Date fromDate, Date toDate);




}
