package com.infoinstech.claimsmodule.domain.repository.claims.transaction;

import com.infoinstech.claimsmodule.domain.model.claims.transaction.OtherChargesPayment;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.ArrayList;
import java.util.Optional;

public interface OtherChargePaymentsRepository extends JpaRepository<OtherChargesPayment, String> {

    ArrayList<OtherChargesPayment> findByIntimationSequenceOrderByPayeeName(String intimationSequence);

//    ArrayList<OtherChargesPayment> findBySequence(String SequenceNo);

    Optional<OtherChargesPayment> findBySequence(String SequenceNo);

}
