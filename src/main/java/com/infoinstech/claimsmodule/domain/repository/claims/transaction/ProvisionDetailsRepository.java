package com.infoinstech.claimsmodule.domain.repository.claims.transaction;

import com.infoinstech.claimsmodule.domain.model.claims.transaction.ProvisionDetails;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.HashSet;
import java.util.List;

public interface ProvisionDetailsRepository extends JpaRepository<ProvisionDetails, String> {

    HashSet<ProvisionDetails> findAllByIntimationSequenceNo(String intimationSeqNo);

    List<ProvisionDetails> findAllByIntimationSequenceNoAndProvisionTypeAndFunctionIdIn
            (String intimationSequenceNo, String provisionType, List<String> functionIds);
}

