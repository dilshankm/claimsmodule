package com.infoinstech.claimsmodule.domain.repository.claims.transaction;

import com.infoinstech.claimsmodule.domain.model.claims.transaction.ProvisionPaymentDetails;
import com.infoinstech.claimsmodule.domain.model.underwriting.master.Product;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public interface ProvisionPaymentDetailsRepository extends JpaRepository<ProvisionPaymentDetails, String> {

    List<ProvisionPaymentDetails> findByClaimIntimation_ProductCodeAndClaimIntimation_ClassCodeAndClaimIntimation_PolicyBranchAndClaimIntimation_Currency(
            String productCode, String classCode, String policyBranch, String currency);

//    findByIntimationDateBetweenAndCurrencyAndBranchCodeContainingAndClassCodeContainingAndProductCodeContainingOrderByBranchCodeAscClaimStatusAscCurrencyAscPolicyNoAsc

    ArrayList<ProvisionPaymentDetails> findByPaymentCreatedDateBetweenAndClaimIntimation_CurrencyAndClaimIntimation_BranchCodeContainingAndClaimIntimation_ClassCodeContainingAndClaimIntimation_ProductCodeContainingAndClaimIntimation_businessPartyTypeCodeContainingOrderByClaimIntimation_BranchCodeAscClaimIntimation_ClaimStatusAscClaimIntimation_CurrencyAscClaimIntimation_PolicyNoAsc(Date from, Date to, String currency, String branch, String cls, String productCode, String businessPartyTypeCode);


}
