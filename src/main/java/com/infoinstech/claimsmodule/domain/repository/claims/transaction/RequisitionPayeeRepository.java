package com.infoinstech.claimsmodule.domain.repository.claims.transaction;

import com.infoinstech.claimsmodule.domain.model.claims.transaction.RequisitionPayee;
import com.infoinstech.claimsmodule.domain.model.claims.transaction.RequisitionPayeePK;
import com.infoinstech.claimsmodule.domain.model.claims.transaction.RequisitionPayee;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.ArrayList;
import java.util.Optional;
import java.util.List;

public interface RequisitionPayeeRepository extends JpaRepository<RequisitionPayee, RequisitionPayeePK> {

    ArrayList<RequisitionPayee> findByRequisitionPayeePK_IntimationSequenceNoOrderByChequeFav(String intimtionSequence);

//    ArrayList<RequisitionPayee> findByRequisitionPayeePK_RequisitionNo(String intimtionSequence);

    RequisitionPayee findByRequisitionPayeePK_RequisitionNo(String requisitionNo);

    Optional<RequisitionPayee> findByRequisitionPayeePK_SequenceNo(String sequenceNo);

}
