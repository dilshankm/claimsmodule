package com.infoinstech.claimsmodule.domain.repository.claims.transaction;

import com.infoinstech.claimsmodule.domain.model.claims.transaction.Requisition;
import com.infoinstech.claimsmodule.domain.model.claims.transaction.RequisitionPK;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.ArrayList;
import java.util.Optional;

public interface RequisitionRepository extends JpaRepository<Requisition,RequisitionPK> {


//    ArrayList<Requisition> findByRequisitionPK_SequenceNo(String sequenceNo);
    
    Optional<Requisition> findByRequisitionPK_SequenceNo(String sequenceNo);

}
