package com.infoinstech.claimsmodule.domain.repository.claims.transaction;

import com.infoinstech.claimsmodule.domain.model.claims.transaction.RevisionDetails;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.HashSet;

public interface RevisionDetailsRepository extends JpaRepository<RevisionDetails, String> {
    HashSet<RevisionDetails> findAllByIntimationSeqNo(String intimationSeqNo);
}
