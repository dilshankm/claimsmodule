package com.infoinstech.claimsmodule.domain.repository.common;

import com.infoinstech.claimsmodule.domain.model.common.LoginAttempt;
import org.springframework.data.jpa.repository.JpaRepository;

public interface LoginAttemptRepository extends JpaRepository<LoginAttempt, String> {
		LoginAttempt findByCode(String code);
}
