package com.infoinstech.claimsmodule.domain.repository.common;

import com.infoinstech.claimsmodule.domain.model.common.Logo;
import org.springframework.data.jpa.repository.JpaRepository;


public interface LogoRepository extends JpaRepository<Logo, String> {

    Logo findByCmgActiveAndCmgRepType(String active, String report);

}
