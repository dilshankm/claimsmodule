package com.infoinstech.claimsmodule.domain.repository.common;

import com.infoinstech.claimsmodule.domain.model.common.SendSMS;
import org.springframework.data.jpa.repository.JpaRepository;

public interface SendSMSRepository extends JpaRepository<SendSMS, String> {
}
