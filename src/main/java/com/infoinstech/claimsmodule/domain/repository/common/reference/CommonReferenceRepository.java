package com.infoinstech.claimsmodule.domain.repository.common.reference;

import com.infoinstech.claimsmodule.domain.model.common.reference.CommonReference;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface CommonReferenceRepository extends JpaRepository<CommonReference, String> {


    List<CommonReference> findAllByTypeOrderByDescription(String type);

    CommonReference findByTypeAndCode(String type, String code);
}
