package com.infoinstech.claimsmodule.domain.repository.common.reference;

import com.infoinstech.claimsmodule.domain.model.common.reference.CommonReferenceType;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface CommonReferenceTypeRepository extends JpaRepository<CommonReferenceType, String> {

    List<CommonReferenceType> findByTableName(String tableName);
}
