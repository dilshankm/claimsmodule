package com.infoinstech.claimsmodule.domain.repository.payments;

import com.infoinstech.claimsmodule.domain.model.payments.ProvisionType;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface ProvisionTypeRepository extends JpaRepository<ProvisionType, String> {

    List<ProvisionType> findByCodeStartingWithAndCodeNotIn(String code, List<String> codes);


}
