package com.infoinstech.claimsmodule.domain.repository.receipting.parameter;

import com.infoinstech.claimsmodule.domain.model.receipting.parameter.ReceiptingSystemParameter;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;

public interface ReceiptingSystemParameterRepository extends JpaRepository<ReceiptingSystemParameter, String> {

    Optional<ReceiptingSystemParameter> findByIndexNo(String indexNo);
}
