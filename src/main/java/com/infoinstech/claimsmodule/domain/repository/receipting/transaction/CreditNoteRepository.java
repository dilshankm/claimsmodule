package com.infoinstech.claimsmodule.domain.repository.receipting.transaction;

import com.infoinstech.claimsmodule.domain.model.receipting.transaction.CreditNote;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.ArrayList;
import java.util.Date;

public interface CreditNoteRepository extends JpaRepository<CreditNote, String> {

//    ArrayList<CreditNote> findByTransactionDateLessThanEqualAndTransactionDateGreaterThanEqualAndClassCodeContainingAndProductCodeContaining(Date debTranDateFrom, Date debTranDateTo, String classCode, String productCode);

}
