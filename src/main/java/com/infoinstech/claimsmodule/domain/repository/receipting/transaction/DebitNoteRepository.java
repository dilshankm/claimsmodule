package com.infoinstech.claimsmodule.domain.repository.receipting.transaction;

import com.infoinstech.claimsmodule.domain.model.receipting.transaction.DebitNote;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.ArrayList;
import java.util.Date;

public interface DebitNoteRepository extends JpaRepository<DebitNote, String> {

//    DebitNote findByCustomerCodeAndPolicyNoAndStatusAndDebitSettled(String customerCode, String policyNo, Character debitStatus, Character debitSettled);

    ArrayList<DebitNote> findByCustomerCodeAndPolicyNoAndStatusAndDebitSettled(String customerCode, String policyNo, Character debitStatus, Character debitSettled);

//    ArrayList<DebitNote> findByTransactionDateLessThanEqualAndTransactionDateGreaterThanEqualAndClassCodeContainingAndProductCodeContaining(Date debTranDateFrom, Date debTranDateTo, String classCode, String productCode);

}
