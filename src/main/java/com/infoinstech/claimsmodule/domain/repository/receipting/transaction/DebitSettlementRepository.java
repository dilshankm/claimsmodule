package com.infoinstech.claimsmodule.domain.repository.receipting.transaction;

import com.infoinstech.claimsmodule.domain.model.receipting.transaction.DebitSettlement;
import org.springframework.data.jpa.repository.JpaRepository;

public interface DebitSettlementRepository extends JpaRepository<DebitSettlement, String> {

}