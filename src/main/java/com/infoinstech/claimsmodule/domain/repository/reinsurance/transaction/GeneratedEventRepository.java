package com.infoinstech.claimsmodule.domain.repository.reinsurance.transaction;

import com.infoinstech.claimsmodule.domain.model.reinsurance.transaction.GeneratedEvent;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Date;
import java.util.List;

public interface GeneratedEventRepository extends JpaRepository<GeneratedEvent, String> {

    List<GeneratedEvent> findByAppliedFromDateIsBeforeAndAppliedToDateAfterAndStatus(Date fromDate, Date toDate, Character status);
}
