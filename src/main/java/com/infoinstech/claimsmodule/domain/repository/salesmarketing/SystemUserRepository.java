package com.infoinstech.claimsmodule.domain.repository.salesmarketing;

import com.infoinstech.claimsmodule.domain.model.salesmarketing.SystemUser;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;

public interface SystemUserRepository extends JpaRepository<SystemUser, String> {

    SystemUser findByUserNameAndIsActiveAndSystemUser(String userName, Character isActive, Character systemUser);

    SystemUser findByCode(String code);

    List<SystemUser> findByIsActive(Character isActive);

    List<SystemUser> findByIsActiveAndBusinessTypeCodeContaining(Character isActive,String businessTypeCode);

    List<SystemUser> findByIsActiveAndSystemUser(Character isActive , Character systemUser);

}
