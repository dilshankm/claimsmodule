package com.infoinstech.claimsmodule.domain.repository.salesmarketing.master;

import com.infoinstech.claimsmodule.domain.model.salesmarketing.master.SalesLocation;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.ArrayList;

public interface SalesLocationRepository extends JpaRepository<SalesLocation, String> {

    SalesLocation findByCode(String code);

    ArrayList<SalesLocation> findByIsActiveOrderByDescription(Character isActive);
}
