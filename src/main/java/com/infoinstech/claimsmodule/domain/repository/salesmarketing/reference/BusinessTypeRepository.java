package com.infoinstech.claimsmodule.domain.repository.salesmarketing.reference;

import com.infoinstech.claimsmodule.domain.model.salesmarketing.reference.BusinessType;
import org.springframework.data.jpa.repository.JpaRepository;

public interface BusinessTypeRepository extends JpaRepository<BusinessType, String> {

    BusinessType findByCode(String code);
}
