package com.infoinstech.claimsmodule.domain.repository.underwriting.master;

import com.infoinstech.claimsmodule.domain.model.underwriting.master.ClassPeril;
import com.infoinstech.claimsmodule.domain.model.underwriting.master.ClassPerilPK;
import org.springframework.data.jpa.repository.JpaRepository;

public interface ClassPerilRepository extends JpaRepository<ClassPeril, ClassPerilPK> {
}
