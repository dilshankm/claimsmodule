package com.infoinstech.claimsmodule.domain.repository.underwriting.master;

import com.infoinstech.claimsmodule.domain.model.underwriting.master.CustomerAddress;
import org.springframework.data.jpa.repository.JpaRepository;

public interface CustomerAddressRepository extends JpaRepository<CustomerAddress, String> {

    CustomerAddress findByCustomerCodeAndDefaultAddress(String customerCode, Character defaultAddress);
}
