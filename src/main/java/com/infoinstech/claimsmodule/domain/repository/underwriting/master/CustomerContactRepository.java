package com.infoinstech.claimsmodule.domain.repository.underwriting.master;

import com.infoinstech.claimsmodule.domain.model.underwriting.master.CustomerContact;
import org.springframework.data.jpa.repository.JpaRepository;

public interface CustomerContactRepository extends JpaRepository<CustomerContact, String> {


}
