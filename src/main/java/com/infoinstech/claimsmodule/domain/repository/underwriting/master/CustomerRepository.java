package com.infoinstech.claimsmodule.domain.repository.underwriting.master;

import com.infoinstech.claimsmodule.domain.model.underwriting.master.Customer;
import org.springframework.data.jpa.repository.JpaRepository;

public interface CustomerRepository extends JpaRepository<Customer, String> {

    Customer findByCustomerCode(String customerCode);


}