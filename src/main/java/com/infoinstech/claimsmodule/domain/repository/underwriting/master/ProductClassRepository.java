package com.infoinstech.claimsmodule.domain.repository.underwriting.master;

import com.infoinstech.claimsmodule.domain.model.underwriting.master.ProductClass;
import com.infoinstech.claimsmodule.domain.model.underwriting.master.ProductClassPK;
import org.springframework.data.jpa.repository.JpaRepository;

public interface ProductClassRepository extends JpaRepository<ProductClass, ProductClassPK> {
}
