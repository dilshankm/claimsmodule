package com.infoinstech.claimsmodule.domain.repository.underwriting.master;

import com.infoinstech.claimsmodule.domain.model.underwriting.master.ProductInformation;
import com.infoinstech.claimsmodule.domain.model.underwriting.master.ProductInformationPK;
import org.springframework.data.jpa.repository.JpaRepository;

public interface ProductInformationRepository extends JpaRepository<ProductInformation, ProductInformationPK> {

    public ProductInformation findByProductInformationPKAndDescription(String productCode, String description);


}
