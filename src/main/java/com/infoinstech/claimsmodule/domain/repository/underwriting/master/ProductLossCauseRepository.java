package com.infoinstech.claimsmodule.domain.repository.underwriting.master;

import com.infoinstech.claimsmodule.domain.model.underwriting.master.ProductLossCause;
import com.infoinstech.claimsmodule.domain.model.underwriting.master.ProductLossCausePK;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface ProductLossCauseRepository extends JpaRepository<ProductLossCause, ProductLossCausePK> {

    List<ProductLossCause> findByProductLossCausePK_ProductCode(String productCode);
}
