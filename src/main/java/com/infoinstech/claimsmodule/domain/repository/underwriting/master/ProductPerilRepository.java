package com.infoinstech.claimsmodule.domain.repository.underwriting.master;

import com.infoinstech.claimsmodule.domain.model.underwriting.master.ProductPeril;
import com.infoinstech.claimsmodule.domain.model.underwriting.master.ProductPerilPK;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface ProductPerilRepository extends JpaRepository<ProductPeril, ProductPerilPK> {

    List<ProductPeril> findAllByProductPerilPK_ProductCodeAndStatusOrderBySequenceAsc(String productCode, Character status);

    ProductPeril findFirstByProductPerilPK_ProductCodeAndStatusOrderBySequenceAsc(String productCode, Character status);



}

