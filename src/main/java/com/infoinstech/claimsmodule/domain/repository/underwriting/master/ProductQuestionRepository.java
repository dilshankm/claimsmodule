package com.infoinstech.claimsmodule.domain.repository.underwriting.master;

import com.infoinstech.claimsmodule.domain.model.underwriting.master.ProductQuestion;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface ProductQuestionRepository extends JpaRepository<ProductQuestion, String> {

    List<ProductQuestion> findByProductCodeAndStatus(String productCode, Character status);

    ProductQuestion findByQuestionCodeAndProductCodeAndStatus(String questionCode, String productCode, Character status);


}
