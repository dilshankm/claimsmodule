package com.infoinstech.claimsmodule.domain.repository.underwriting.master;

import com.infoinstech.claimsmodule.domain.model.underwriting.master.Product;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface ProductRepository extends JpaRepository<Product,String> {

    Product findByCodeAndClassCode(String productCode, String classCode);

    List<Product> findByClassCodeAndStatus(String classCode, Character status);

    List<Product> findByClassCodeContainingAndStatus(String classCode, Character status);
}