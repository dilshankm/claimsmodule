package com.infoinstech.claimsmodule.domain.repository.underwriting.master;

import com.infoinstech.claimsmodule.domain.model.underwriting.master.ProductVehicleSketch;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface ProductVehicleSketchRepository extends JpaRepository<ProductVehicleSketch, String> {

    List<ProductVehicleSketch> findAllByProductCode(String product);
}
