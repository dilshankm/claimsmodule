package com.infoinstech.claimsmodule.domain.repository.underwriting.parameter;

import com.infoinstech.claimsmodule.domain.model.underwriting.parameter.UnderwritingSystemParameter;
import org.springframework.data.jpa.repository.JpaRepository;

public interface UnderwritingSystemParameterRepository extends JpaRepository<UnderwritingSystemParameter, String> {
}
