package com.infoinstech.claimsmodule.domain.repository.underwriting.reference;

import com.infoinstech.claimsmodule.domain.model.underwriting.reference.BusinessChannel;
import org.springframework.data.jpa.repository.JpaRepository;

public interface BusinessChannelRepository extends JpaRepository<BusinessChannel, String> {
}
