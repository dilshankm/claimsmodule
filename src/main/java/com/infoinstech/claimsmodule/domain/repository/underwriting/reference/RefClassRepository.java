package com.infoinstech.claimsmodule.domain.repository.underwriting.reference;

import com.infoinstech.claimsmodule.domain.model.underwriting.reference.RefClass;
import org.springframework.data.jpa.repository.JpaRepository;

public interface RefClassRepository extends JpaRepository<RefClass,String> {
}
