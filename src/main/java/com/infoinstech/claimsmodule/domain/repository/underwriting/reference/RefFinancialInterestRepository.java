package com.infoinstech.claimsmodule.domain.repository.underwriting.reference;

import com.infoinstech.claimsmodule.domain.model.underwriting.reference.RefFinancialInterest;
import org.springframework.data.jpa.repository.JpaRepository;

public interface RefFinancialInterestRepository extends JpaRepository<RefFinancialInterest, String> {
}
