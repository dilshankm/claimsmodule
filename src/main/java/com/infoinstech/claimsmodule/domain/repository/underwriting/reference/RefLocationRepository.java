package com.infoinstech.claimsmodule.domain.repository.underwriting.reference;

import com.infoinstech.claimsmodule.domain.model.underwriting.reference.RefLocation;
import org.springframework.data.jpa.repository.JpaRepository;

public interface RefLocationRepository extends JpaRepository<RefLocation, String> {
}
