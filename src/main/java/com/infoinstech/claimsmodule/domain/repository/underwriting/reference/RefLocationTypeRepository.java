package com.infoinstech.claimsmodule.domain.repository.underwriting.reference;

import com.infoinstech.claimsmodule.domain.model.underwriting.reference.RefLocationType;
import org.springframework.data.jpa.repository.JpaRepository;

public interface RefLocationTypeRepository extends JpaRepository<RefLocationType, String> {
}
