package com.infoinstech.claimsmodule.domain.repository.underwriting.temporary;

import com.infoinstech.claimsmodule.domain.model.underwriting.temporary.TempPolicyExcessType;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface TempPolicyExcessTypeRepository extends JpaRepository<TempPolicyExcessType, String> {

    List<TempPolicyExcessType> findByRiskSequenceNo(String riskSequenceNo);

    List<TempPolicyExcessType> findByPolicySequenceNoAndRiskSequenceNoIsNull(String policySequenceNo);
}
