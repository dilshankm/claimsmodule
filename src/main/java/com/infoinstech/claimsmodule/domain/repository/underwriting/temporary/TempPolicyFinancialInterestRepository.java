package com.infoinstech.claimsmodule.domain.repository.underwriting.temporary;

import com.infoinstech.claimsmodule.domain.model.underwriting.temporary.TempPolicyFinancialInterest;
import com.infoinstech.claimsmodule.domain.model.underwriting.temporary.TempPolicyFinancialInterestPK;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface TempPolicyFinancialInterestRepository extends JpaRepository<TempPolicyFinancialInterest, TempPolicyFinancialInterestPK> {


    List<TempPolicyFinancialInterest> findByTempPolicyFinancialInterestPK_PolicySequenceNo(String policySequenceNo);
}
