package com.infoinstech.claimsmodule.domain.repository.underwriting.temporary;

import com.infoinstech.claimsmodule.domain.model.underwriting.temporary.TempPolicyLocation;
import com.infoinstech.claimsmodule.domain.model.underwriting.temporary.TempPolicyLocationPK;
import org.springframework.data.jpa.repository.JpaRepository;

public interface TempPolicyLocationRepository extends JpaRepository<TempPolicyLocation, TempPolicyLocationPK> {

    TempPolicyLocation findByTempPolicyLocationPK(TempPolicyLocationPK tempPolicyLocationPK);
}
