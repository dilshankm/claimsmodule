package com.infoinstech.claimsmodule.domain.repository.underwriting.temporary;

import com.infoinstech.claimsmodule.domain.model.underwriting.temporary.TempPolicyPeril;
import com.infoinstech.claimsmodule.domain.model.underwriting.temporary.TempPolicyPerilPK;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface TempPolicyPerilRepository extends JpaRepository<TempPolicyPeril, TempPolicyPerilPK> {

    List<TempPolicyPeril> findByTempPolicyRisk_TempPolicyLocation_TempPolicy_SequenceNoAndTempPolicyRisk_TempPolicyLocation_LocationCodeAndTempPolicyRisk_RiskOrderNo(
            String sequenceNo, String locationCode, String riskOrderNo);

    List<TempPolicyPeril> findAllByTempPolicyPerilPK_PolicySequenceNoAndTempPolicyPerilPK_LocationSequenceNoAndTempPolicyPerilPK_RiskSequenceNo(
            String policySequenceNo, String locationSequenceNo, String riskSequenceNo);

}
