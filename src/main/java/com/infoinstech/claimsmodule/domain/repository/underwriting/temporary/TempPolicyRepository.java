package com.infoinstech.claimsmodule.domain.repository.underwriting.temporary;

import com.infoinstech.claimsmodule.domain.model.underwriting.temporary.TempPolicy;
import org.springframework.data.jpa.repository.JpaRepository;

public interface TempPolicyRepository extends JpaRepository<TempPolicy, String> {

    public TempPolicy findBySequenceNoAndSessionId(String sequenceNo, String sessionId);

    public TempPolicy findBySequenceNo(String sequenceNo);
}
