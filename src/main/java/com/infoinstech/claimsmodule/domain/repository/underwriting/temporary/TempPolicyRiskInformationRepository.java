package com.infoinstech.claimsmodule.domain.repository.underwriting.temporary;

import com.infoinstech.claimsmodule.domain.model.underwriting.temporary.TempPolicyRiskInformation;
import com.infoinstech.claimsmodule.domain.model.underwriting.temporary.TempPolicyRiskInformationPK;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;

public interface TempPolicyRiskInformationRepository extends JpaRepository<TempPolicyRiskInformation, TempPolicyRiskInformationPK> {

    Optional<TempPolicyRiskInformation> findByTempPolicyRiskInformationPK_PolicySequenceNoAndTempPolicyRiskInformationPK_LocationSequenceNoAndTempPolicyRiskInformationPK_RiskSequenceNoAndDescription(
            String policySequenceNo, String policyLocationSequenceNo, String policyRiskSequenceNo, String description);


}
