package com.infoinstech.claimsmodule.domain.repository.underwriting.temporary;

import com.infoinstech.claimsmodule.domain.model.underwriting.temporary.TempPolicyRisk;
import com.infoinstech.claimsmodule.domain.model.underwriting.temporary.TempPolicyRiskPK;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Set;

public interface TempPolicyRiskRepository extends JpaRepository<TempPolicyRisk, TempPolicyRiskPK> {

    Set<TempPolicyRisk> findByTempPolicyRiskPK_PolicySequenceNo(String policySequenceNo);

    TempPolicyRisk findByTempPolicyRiskPK_PolicySequenceNoAndRiskOrderNo(String policySequenceNo, String riskOrderNo);

    TempPolicyRisk findByTempPolicyRiskPK_PolicySequenceNoAndName(String policySequenceNo, String riskName);

    TempPolicyRisk findByTempPolicyRiskPK_SequenceNo(String riskSequenceNo);

    TempPolicyRisk findByName(String name);


}
