package com.infoinstech.claimsmodule.domain.repository.underwriting.transaction;

import com.infoinstech.claimsmodule.domain.model.underwriting.transaction.EndorsementLocation;
import com.infoinstech.claimsmodule.domain.model.underwriting.transaction.EndorsementLocationPK;
import org.springframework.data.jpa.repository.JpaRepository;

public interface EndorsementLocationRepository extends JpaRepository<EndorsementLocation, EndorsementLocationPK> {

    EndorsementLocation findByEndorsementLocationPK(EndorsementLocationPK endorsementLocationPK);

    EndorsementLocation findByEndorsementLocationPK_SequenceNoAndEndorsementLocationPK_EndorsementSequenceNo(String sequenceNo, String endorsementSequenceNo);

}
