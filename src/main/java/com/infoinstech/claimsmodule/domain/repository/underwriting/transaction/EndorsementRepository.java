package com.infoinstech.claimsmodule.domain.repository.underwriting.transaction;

import com.infoinstech.claimsmodule.domain.model.underwriting.transaction.Endorsement;
import org.springframework.data.jpa.repository.JpaRepository;

public interface EndorsementRepository extends JpaRepository<Endorsement, String> {

    Endorsement findBySequenceNoAndTransactionType(String sequenceNo, Character transactionType);

    Endorsement findBySequenceNo(String sequenceNo);
}
