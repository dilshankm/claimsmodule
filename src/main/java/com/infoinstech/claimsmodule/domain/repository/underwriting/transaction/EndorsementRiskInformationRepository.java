package com.infoinstech.claimsmodule.domain.repository.underwriting.transaction;

import com.infoinstech.claimsmodule.domain.model.underwriting.transaction.EndorsementRiskInformation;
import com.infoinstech.claimsmodule.domain.model.underwriting.transaction.EndorsementRiskInformationPK;
import org.springframework.data.jpa.repository.JpaRepository;

public interface EndorsementRiskInformationRepository extends JpaRepository<EndorsementRiskInformation, EndorsementRiskInformationPK> {


}
