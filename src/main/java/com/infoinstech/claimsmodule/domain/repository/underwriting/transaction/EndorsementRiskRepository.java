package com.infoinstech.claimsmodule.domain.repository.underwriting.transaction;

import com.infoinstech.claimsmodule.domain.model.underwriting.transaction.EndorsementRisk;
import com.infoinstech.claimsmodule.domain.model.underwriting.transaction.EndorsementRiskPK;
import org.springframework.data.jpa.repository.JpaRepository;

public interface EndorsementRiskRepository extends JpaRepository<EndorsementRisk, EndorsementRiskPK> {

    EndorsementRisk findByEndorsementRiskPK_SequenceNo(String sequenceNo);
}
