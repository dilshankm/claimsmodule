package com.infoinstech.claimsmodule.domain.repository.underwriting.transaction;

import com.infoinstech.claimsmodule.domain.model.underwriting.transaction.PolicyExcessType;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface PolicyExcessTypeRepository extends JpaRepository<PolicyExcessType, String> {

    List<PolicyExcessType> findByRiskSequenceNo(String riskSequenceNo);
}
