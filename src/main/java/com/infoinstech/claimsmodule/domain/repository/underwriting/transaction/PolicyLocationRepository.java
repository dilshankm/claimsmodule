package com.infoinstech.claimsmodule.domain.repository.underwriting.transaction;

import com.infoinstech.claimsmodule.domain.model.underwriting.transaction.PolicyLocation;
import com.infoinstech.claimsmodule.domain.model.underwriting.transaction.PolicyLocationPK;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;

public interface PolicyLocationRepository extends JpaRepository<PolicyLocation, PolicyLocationPK> {

    Optional<PolicyLocation> findByPolicyLocationPK(PolicyLocationPK policyLocationPK);


}
