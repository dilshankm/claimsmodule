package com.infoinstech.claimsmodule.domain.repository.underwriting.transaction;

import com.infoinstech.claimsmodule.domain.model.underwriting.transaction.PolicyPeril;
import com.infoinstech.claimsmodule.domain.model.underwriting.transaction.PolicyPerilPK;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface PolicyPerilRepository extends JpaRepository<PolicyPeril, PolicyPerilPK> {

    List<PolicyPeril> findAllByPolicyPerilPK_PolicySequenceNoAndPolicyPerilPK_LocationSequenceNoAndPolicyPerilPK_RiskSequenceNo
            (String policySequenceNo, String locationSequenceNo, String riskSequenceNo);


}
