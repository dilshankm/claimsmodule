package com.infoinstech.claimsmodule.domain.repository.underwriting.transaction;

import com.infoinstech.claimsmodule.domain.model.underwriting.transaction.Policy;
import org.springframework.data.jpa.repository.JpaRepository;

import javax.swing.text.html.Option;
import java.util.Date;
import java.util.List;
import java.util.Optional;

public interface PolicyRepository extends JpaRepository<Policy, String> {
//    Policy findBySequenceNo(String sequenceNo);

    Policy findBySequenceNo(String sequenceNo);

    Policy findByPolicyNo(String policyNo);

    Optional<Policy> findByPolicyNoAndStatusNotIn(String policyNo, List<String> statusesNotIn);

    Optional<Policy> findByPolicyNoAndCancelledTypeAndStatusInAndCancelEffectiveDateGreaterThanEqualOrderByCancelEffectiveDateDesc(
            String policyNo, Character cancelledType,List<String> statusesIn,Date cancelEffectiveDate);


}
