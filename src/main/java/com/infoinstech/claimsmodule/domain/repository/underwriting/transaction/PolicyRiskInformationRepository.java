package com.infoinstech.claimsmodule.domain.repository.underwriting.transaction;

import com.infoinstech.claimsmodule.domain.model.underwriting.transaction.PolicyRiskInformation;
import com.infoinstech.claimsmodule.domain.model.underwriting.transaction.PolicyRiskInformationPK;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;
import java.util.Optional;

public interface PolicyRiskInformationRepository extends JpaRepository<PolicyRiskInformation, PolicyRiskInformationPK> {

    PolicyRiskInformation findByPolicyRiskInformationPKAndDescription(PolicyRiskInformationPK policyRiskInformationPK, String description);

    Optional<PolicyRiskInformation> findByPolicyRiskInformationPK_PolicySequenceNoAndPolicyRiskInformationPK_LocationSequenceNoAndPolicyRiskInformationPK_RiskSequenceNoAndDescription
            (String policySequenceNo, String locationSequenceNo, String riskSequenceNo, String description);

    List<PolicyRiskInformation> findAllByPolicyRiskInformationPK_PolicySequenceNoAndPolicyRiskInformationPK_LocationSequenceNoAndPolicyRiskInformationPK_RiskSequenceNo
            (String policySequenceNo, String locationSequenceNo, String riskSequenceNo);

}
