package com.infoinstech.claimsmodule.domain.repository.underwriting.transaction;

import com.infoinstech.claimsmodule.domain.model.underwriting.transaction.PolicyRisk;
import org.springframework.data.jpa.repository.JpaRepository;

import javax.swing.text.html.Option;
import java.util.List;
import java.util.Optional;

public interface PolicyRiskRepository extends JpaRepository<PolicyRisk, String> {

    Optional<PolicyRisk> findByPolicyRiskPK_SequenceNo(String riskSequenceNo);

    Optional<PolicyRisk> findFirstByNameOrderByEndorsedDateDesc(String name);

}
