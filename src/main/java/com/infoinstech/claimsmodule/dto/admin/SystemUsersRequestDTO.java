package com.infoinstech.claimsmodule.dto.admin;

public class SystemUsersRequestDTO {

		private String code;
		private String userName;
		private String firstName;
		private String surname;
		private String isActive;

		public SystemUsersRequestDTO() {
		}

		public SystemUsersRequestDTO(String code, String userName, String firstName, String surname, String isActive) {
				this.code = code;
				this.userName = userName;
				this.firstName = firstName;
				this.surname = surname;
				this.isActive = isActive;
		}

		public String getCode() {
				return code;
		}

		public void setCode(String code) {
				this.code = code;
		}

		public String getUserName() {
				return userName;
		}

		public void setUserName(String userName) {
				this.userName = userName;
		}

		public String getFirstName() {
				return firstName;
		}

		public void setFirstName(String firstName) {
				this.firstName = firstName;
		}

		public String getSurname() {
				return surname;
		}

		public void setSurname(String surname) {
				this.surname = surname;
		}

		public String getIsActive() {
				return isActive;
		}

		public void setIsActive(String isActive) {
				this.isActive = isActive;
		}

		@Override
		public String toString() {
				return "SystemUsersRequestDTO{" +
								"code='" + code + '\'' +
								", userName='" + userName + '\'' +
								", firstName='" + firstName + '\'' +
								", surname='" + surname + '\'' +
								", isActive='" + isActive + '\'' +
								'}';
		}
}
