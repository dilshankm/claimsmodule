package com.infoinstech.claimsmodule.dto.admin;

public class UpdateUserStatusDTO {
		private String code;
		private boolean isBlocked;

		public String getCode() {
				return code;
		}

		public void setCode(String code) {
				this.code = code;
		}

		public boolean getIsBlocked() {
				return isBlocked;
		}

		public void setIsBlocked(boolean isBlocked) {
				this.isBlocked = isBlocked;
		}

		@Override
		public String toString() {
				return "UpdateUserStatusDTO{" +
								"code='" + code + '\'' +
								", isBlocked='" + isBlocked + '\'' +
								'}';
		}
}
