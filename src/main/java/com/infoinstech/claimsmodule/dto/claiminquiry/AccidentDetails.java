package com.infoinstech.claimsmodule.dto.claiminquiry;

import java.util.List;

public class AccidentDetails {

    private String placeOfAccident;
    private String dateOfAccident;
    private String nearestTown;
    private String lossRemarks;
    private String thirdPartyDetails;
    private String latitude;
    private String longitude;
    private List<byte[]> damageImages;

    public List<byte[]> getDamageImages() {
        return damageImages;
    }

    public void setDamageImages(List<byte[]> damageImages) {
        this.damageImages = damageImages;
    }

    public String getPlaceOfAccident() {
        return placeOfAccident;
    }

    public void setPlaceOfAccident(String placeOfAccident) {
        this.placeOfAccident = placeOfAccident;
    }

    public String getDateOfAccident() {
        return dateOfAccident;
    }

    public void setDateOfAccident(String dateOfAccident) {
        this.dateOfAccident = dateOfAccident;
    }

    public String getNearestTown() {
        return nearestTown;
    }

    public void setNearestTown(String nearestTown) {
        this.nearestTown = nearestTown;
    }

    public String getLossRemarks() {
        return lossRemarks;
    }

    public void setLossRemarks(String lossRemarks) {
        this.lossRemarks = lossRemarks;
    }

    public String getThirdPartyDetails() {
        return thirdPartyDetails;
    }

    public void setThirdPartyDetails(String thirdPartyDetails) {
        this.thirdPartyDetails = thirdPartyDetails;
    }

    public String getLatitude() {
        return latitude;
    }

    public void setLatitude(String latitude) {
        this.latitude = latitude;
    }

    public String getLongitude() {
        return longitude;
    }

    public void setLongitude(String longitude) {
        this.longitude = longitude;
    }
}
