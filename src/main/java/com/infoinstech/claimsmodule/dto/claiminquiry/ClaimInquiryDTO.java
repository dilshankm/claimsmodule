package com.infoinstech.claimsmodule.dto.claiminquiry;

public class ClaimInquiryDTO {

    private CustomerDetails customerDetails;
    private PolicyDetails policyDetails;
    private AccidentDetails accidentDetails;
    private IntimationDetails intimationDetails;
    private DriverDetails driverDetails;

    public CustomerDetails getCustomerDetails() {
        return customerDetails;
    }

    public void setCustomerDetails(CustomerDetails customerDetails) {
        this.customerDetails = customerDetails;
    }

    public PolicyDetails getPolicyDetails() {
        return policyDetails;
    }

    public void setPolicyDetails(PolicyDetails policyDetails) {
        this.policyDetails = policyDetails;
    }

    public AccidentDetails getAccidentDetails() {
        return accidentDetails;
    }

    public void setAccidentDetails(AccidentDetails accidentDetails) {
        this.accidentDetails = accidentDetails;
    }

    public IntimationDetails getIntimationDetails() {
        return intimationDetails;
    }

    public void setIntimationDetails(IntimationDetails intimationDetails) {
        this.intimationDetails = intimationDetails;
    }

    public DriverDetails getDriverDetails() {
        return driverDetails;
    }

    public void setDriverDetails(DriverDetails driverDetails) {
        this.driverDetails = driverDetails;
    }
}
