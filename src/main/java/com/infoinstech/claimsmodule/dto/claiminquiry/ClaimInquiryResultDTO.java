package com.infoinstech.claimsmodule.dto.claiminquiry;

import java.util.List;

public class ClaimInquiryResultDTO {

    private List<ClaimSearchResultDTO> claimIntimations;
    private List<NotificationSearchResultDTO> claimNotifications;

    public List<ClaimSearchResultDTO> getClaimIntimations() {
        return claimIntimations;
    }

    public void setClaimIntimations(List<ClaimSearchResultDTO> claimIntimations) {
        this.claimIntimations = claimIntimations;
    }

    public List<NotificationSearchResultDTO> getClaimNotifications() {
        return claimNotifications;
    }

    public void setClaimNotifications(List<NotificationSearchResultDTO> claimNotifications) {
        this.claimNotifications = claimNotifications;
    }
}
