package com.infoinstech.claimsmodule.dto.claiminquiry;

public class CustomerDetails {

    private String claimNo;
    private String insuredName;
    private String insuredNIC;
    private String contactNo;

    public String getClaimNo() {
        return claimNo;
    }

    public void setClaimNo(String claimNo) {
        this.claimNo = claimNo;
    }

    public String getInsuredName() {
        return insuredName;
    }

    public void setInsuredName(String insuredName) {
        this.insuredName = insuredName;
    }

    public String getInsuredNIC() {
        return insuredNIC;
    }

    public void setInsuredNIC(String insuredNIC) {
        this.insuredNIC = insuredNIC;
    }

    public String getContactNo() {
        return contactNo;
    }

    public void setContactNo(String contactNo) {
        this.contactNo = contactNo;
    }
}
