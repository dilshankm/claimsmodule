package com.infoinstech.claimsmodule.dto.claiminquiry;

public class DriverDetails {

    private String driverName;
    private String dlNumber;
    private String dlCategory;
    private String licenseFrom;
    private String licenseTo;
    private String relation;
    private String relationDetails;

    public String getDriverName() {
        return driverName;
    }

    public void setDriverName(String driverName) {
        this.driverName = driverName;
    }

    public String getDlNumber() {
        return dlNumber;
    }

    public void setDlNumber(String dlNumber) {
        this.dlNumber = dlNumber;
    }

    public String getDlCategory() {
        return dlCategory;
    }

    public void setDlCategory(String dlCategory) {
        this.dlCategory = dlCategory;
    }

    public String getLicenseFrom() {
        return licenseFrom;
    }

    public void setLicenseFrom(String licenseFrom) {
        this.licenseFrom = licenseFrom;
    }

    public String getLicenseTo() {
        return licenseTo;
    }

    public void setLicenseTo(String licenseTo) {
        this.licenseTo = licenseTo;
    }

    public String getRelation() {
        return relation;
    }

    public void setRelation(String relation) {
        this.relation = relation;
    }

    public String getRelationDetails() {
        return relationDetails;
    }

    public void setRelationDetails(String relationDetails) {
        this.relationDetails = relationDetails;
    }
}
