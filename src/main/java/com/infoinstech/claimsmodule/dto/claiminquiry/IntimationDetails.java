package com.infoinstech.claimsmodule.dto.claiminquiry;

public class IntimationDetails {

    private String claimNo;
    private String intimationDate;
    private String completionDate;
    private String policeStation;
    private String callerName;
    private String claimStatus;
    private String causeOfLoss;
    private String acr;
    private String claimDoubtful;
    private String callPoliceReport;

    public String getClaimNo() {
        return claimNo;
    }

    public void setClaimNo(String claimNo) {
        this.claimNo = claimNo;
    }

    public String getIntimationDate() {
        return intimationDate;
    }

    public void setIntimationDate(String intimationDate) {
        this.intimationDate = intimationDate;
    }

    public String getCompletionDate() {
        return completionDate;
    }

    public void setCompletionDate(String completionDate) {
        this.completionDate = completionDate;
    }

    public String getPoliceStation() {
        return policeStation;
    }

    public void setPoliceStation(String policeStation) {
        this.policeStation = policeStation;
    }

    public String getCallerName() {
        return callerName;
    }

    public void setCallerName(String callerName) {
        this.callerName = callerName;
    }

    public String getClaimStatus() {
        return claimStatus;
    }

    public void setClaimStatus(String claimStatus) {
        this.claimStatus = claimStatus;
    }

    public String getCauseOfLoss() {
        return causeOfLoss;
    }

    public void setCauseOfLoss(String causeOfLoss) {
        this.causeOfLoss = causeOfLoss;
    }

    public String getAcr() {
        return acr;
    }

    public void setAcr(String acr) {
        this.acr = acr;
    }

    public String getClaimDoubtful() {
        return claimDoubtful;
    }

    public void setClaimDoubtful(String claimDoubtful) {
        this.claimDoubtful = claimDoubtful;
    }

    public String getCallPoliceReport() {
        return callPoliceReport;
    }

    public void setCallPoliceReport(String callPoliceReport) {
        this.callPoliceReport = callPoliceReport;
    }
}
