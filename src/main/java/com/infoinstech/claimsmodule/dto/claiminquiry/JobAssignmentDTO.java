package com.infoinstech.claimsmodule.dto.claiminquiry;

import com.infoinstech.claimsmodule.dto.questionnaire.ViewQuestionDTO;

import java.util.List;

public class JobAssignmentDTO {

    private String intimationSequenceNo;

    private String sequenceNo;
    private String jobNo;
    private String claimNo;
    private String typeCode;
    private String type;
    private String status;
    private String promiseTime;
    private Double longitude;
    private Double latitude;
    private String nearestTown;

    private String externalPersonCode;
    private String externalPersonName;
    private String appointedDate;
    private String reportDueDate;
    private String visitDueDate;
    private String reportSubmittedDate;
    private String createdBy;
    private String actionTaken;

    private List<ViewQuestionDTO> questionnaire;
//    private List<ViewImageDTO> documentImages;
//    private List<ViewImageDTO> accidentImages;

    public String getIntimationSequenceNo() {
        return intimationSequenceNo;
    }

    public void setIntimationSequenceNo(String intimationSequenceNo) {
        this.intimationSequenceNo = intimationSequenceNo;
    }

    public String getSequenceNo() {
        return sequenceNo;
    }

    public void setSequenceNo(String sequenceNo) {
        this.sequenceNo = sequenceNo;
    }

    public String getJobNo() {
        return jobNo;
    }

    public void setJobNo(String jobNo) {
        this.jobNo = jobNo;
    }

    public String getClaimNo() {
        return claimNo;
    }

    public void setClaimNo(String claimNo) {
        this.claimNo = claimNo;
    }

    public String getTypeCode() {
        return typeCode;
    }

    public void setTypeCode(String typeCode) {
        this.typeCode = typeCode;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getPromiseTime() {
        return promiseTime;
    }

    public void setPromiseTime(String promiseTime) {
        this.promiseTime = promiseTime;
    }

    public Double getLongitude() {
        return longitude;
    }

    public void setLongitude(Double longitude) {
        this.longitude = longitude;
    }

    public Double getLatitude() {
        return latitude;
    }

    public void setLatitude(Double latitude) {
        this.latitude = latitude;
    }

    public String getNearestTown() {
        return nearestTown;
    }

    public void setNearestTown(String nearestTown) {
        this.nearestTown = nearestTown;
    }

    public String getExternalPersonCode() {
        return externalPersonCode;
    }

    public void setExternalPersonCode(String externalPersonCode) {
        this.externalPersonCode = externalPersonCode;
    }

    public String getExternalPersonName() {
        return externalPersonName;
    }

    public void setExternalPersonName(String externalPersonName) {
        this.externalPersonName = externalPersonName;
    }

    public String getAppointedDate() {
        return appointedDate;
    }

    public void setAppointedDate(String appointedDate) {
        this.appointedDate = appointedDate;
    }

    public String getReportDueDate() {
        return reportDueDate;
    }

    public void setReportDueDate(String reportDueDate) {
        this.reportDueDate = reportDueDate;
    }

    public String getVisitDueDate() {
        return visitDueDate;
    }

    public void setVisitDueDate(String visitDueDate) {
        this.visitDueDate = visitDueDate;
    }

    public String getReportSubmittedDate() {
        return reportSubmittedDate;
    }

    public void setReportSubmittedDate(String reportSubmittedDate) {
        this.reportSubmittedDate = reportSubmittedDate;
    }

    public String getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(String createdBy) {
        this.createdBy = createdBy;
    }

    public List<ViewQuestionDTO> getQuestionnaire() {
        return questionnaire;
    }

    public void setQuestionnaire(List<ViewQuestionDTO> questionnaire) {
        this.questionnaire = questionnaire;
    }

//    public List<ViewImageDTO> getDocumentImages() {
//        return documentImages;
//    }
//
//    public void setDocumentImages(List<ViewImageDTO> documentImages) {
//        this.documentImages = documentImages;
//    }
//
//    public List<ViewImageDTO> getAccidentImages() {
//        return accidentImages;
//    }
//
//    public void setAccidentImages(List<ViewImageDTO> accidentImages) {
//        this.accidentImages = accidentImages;
//    }


    public String getActionTaken() {
        return actionTaken;
    }

    public void setActionTaken(String actionTaken) {
        this.actionTaken = actionTaken;
    }
}

