package com.infoinstech.claimsmodule.dto.claiminquiry;

public class NotificationDetails {

    private String notificationNo;
    private String notifiedDate;
    private String completionDate;
    private String policeStation;
    private String callerName;
    private String notificationStatus;
    private String causeOfLoss;
    private String acr;
    private String claimDoubtful;
    private String callPoliceReport;

    public String getNotificationNo() {
        return notificationNo;
    }

    public void setNotificationNo(String notificationNo) {
        this.notificationNo = notificationNo;
    }

    public String getNotifiedDate() {
        return notifiedDate;
    }

    public void setNotifiedDate(String notifiedDate) {
        this.notifiedDate = notifiedDate;
    }

    public String getCompletionDate() {
        return completionDate;
    }

    public void setCompletionDate(String completionDate) {
        this.completionDate = completionDate;
    }

    public String getPoliceStation() {
        return policeStation;
    }

    public void setPoliceStation(String policeStation) {
        this.policeStation = policeStation;
    }

    public String getCallerName() {
        return callerName;
    }

    public void setCallerName(String callerName) {
        this.callerName = callerName;
    }

    public String getNotificationStatus() {
        return notificationStatus;
    }

    public void setNotificationStatus(String notificationStatus) {
        this.notificationStatus = notificationStatus;
    }

    public String getCauseOfLoss() {
        return causeOfLoss;
    }

    public void setCauseOfLoss(String causeOfLoss) {
        this.causeOfLoss = causeOfLoss;
    }

    public String getAcr() {
        return acr;
    }

    public void setAcr(String acr) {
        this.acr = acr;
    }

    public String getClaimDoubtful() {
        return claimDoubtful;
    }

    public void setClaimDoubtful(String claimDoubtful) {
        this.claimDoubtful = claimDoubtful;
    }

    public String getCallPoliceReport() {
        return callPoliceReport;
    }

    public void setCallPoliceReport(String callPoliceReport) {
        this.callPoliceReport = callPoliceReport;
    }
}
