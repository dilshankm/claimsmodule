package com.infoinstech.claimsmodule.dto.claiminquiry;

public class NotificationInquiryDTO {

    private NotificationDetails notificationDetails;
    private CoverNoteDetails coverNoteDetails;
    private AccidentDetails accidentDetails;
    private DriverDetails driverDetails;

    public NotificationDetails getNotificationDetails() {
        return notificationDetails;
    }

    public void setNotificationDetails(NotificationDetails notificationDetails) {
        this.notificationDetails = notificationDetails;
    }

    public CoverNoteDetails getCoverNoteDetails() {
        return coverNoteDetails;
    }

    public void setCoverNoteDetails(CoverNoteDetails coverNoteDetails) {
        this.coverNoteDetails = coverNoteDetails;
    }

    public AccidentDetails getAccidentDetails() {
        return accidentDetails;
    }

    public void setAccidentDetails(AccidentDetails accidentDetails) {
        this.accidentDetails = accidentDetails;
    }

    public DriverDetails getDriverDetails() {
        return driverDetails;
    }

    public void setDriverDetails(DriverDetails driverDetails) {
        this.driverDetails = driverDetails;
    }
}
