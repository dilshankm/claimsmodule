package com.infoinstech.claimsmodule.dto.claiminquiry;

public class PolicyDetails {

    private String policyNo;
    private String dateFrom;
    private String dateTo;
    private String vehicleNo;
    private String makeAndModel;
    private String vehicleCategory;
    private String yearOfManufucture;

    public String getPolicyNo() {
        return policyNo;
    }

    public void setPolicyNo(String policyNo) {
        this.policyNo = policyNo;
    }

    public String getDateFrom() {
        return dateFrom;
    }

    public void setDateFrom(String dateFrom) {
        this.dateFrom = dateFrom;
    }

    public String getDateTo() {
        return dateTo;
    }

    public void setDateTo(String dateTo) {
        this.dateTo = dateTo;
    }

    public String getVehicleNo() {
        return vehicleNo;
    }

    public void setVehicleNo(String vehicleNo) {
        this.vehicleNo = vehicleNo;
    }

    public String getMakeAndModel() {
        return makeAndModel;
    }

    public void setMakeAndModel(String makeAndModel) {
        this.makeAndModel = makeAndModel;
    }

    public String getVehicleCategory() {
        return vehicleCategory;
    }

    public void setVehicleCategory(String vehicleCategory) {
        this.vehicleCategory = vehicleCategory;
    }

    public String getYearOfManufucture() {
        return yearOfManufucture;
    }

    public void setYearOfManufucture(String yearOfManufucture) {
        this.yearOfManufucture = yearOfManufucture;
    }
}
