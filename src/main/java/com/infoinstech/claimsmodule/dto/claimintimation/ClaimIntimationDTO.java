package com.infoinstech.claimsmodule.dto.claimintimation;

import com.infoinstech.claimsmodule.dto.imageservice.ImageDTO;
import lombok.Data;

import java.util.Date;
import java.util.List;

@Data
public class ClaimIntimationDTO {

    private List<ImageDTO> accidentImages;
    private String uploadType;

    private String intimationSeqNo;

    private String intimationType;
    private String comments;

    private String contactName;
    private String contactMobile;

    private String customerMobile;
    private String callerName;
    private String callerMobile;
    private String callerAddress;
    private String callerEmail;

    private String lossRemarks;
    private String intimationRemarks;
    private String eventCode;
    private String classCode;

    private String nearestTown;
    private String placeOfAccident;
    private String causeOfLoss;
    private String causeDescription;

    private String driverName;
    private String driverLicenseNo;
    private String relationshipToInsured;
    private String relationshipDescription;

    private Character insuredAtFault;
    private Character claimDoubtful;
    private Character policeReport;
    private Character totalLoss;

    private String promisedTime;
    private String policeStation;
    private String thirdPartyDetails;

    private String policySequenceNo;
    private String policyNo;
    private String calledTime;
    private String riskOrder;

    private String lossTime;
    private Date lossDate;

    private Double longitude;
    private Double latitude;

    private String assessorCode;
    private String assignmentTypeCode;

    private String createdBy;
    private String modifiedBy;

    private String actionTaken;
    private byte[] baseString;
    private List<ClaimProvisionDTO> provisionList;
}
