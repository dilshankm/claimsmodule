package com.infoinstech.claimsmodule.dto.claimintimation;

public class ClaimIntimationResponseDTO {

		private String claimNo;

		public ClaimIntimationResponseDTO(String claimNo) {
				this.claimNo = claimNo;
		}

		public String getClaimNo() {
				return claimNo;
		}

		public void setClaimNo(String claimNo) {
				this.claimNo = claimNo;
		}
}
