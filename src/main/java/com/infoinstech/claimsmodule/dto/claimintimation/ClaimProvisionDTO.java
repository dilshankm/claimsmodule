package com.infoinstech.claimsmodule.dto.claimintimation;

public class ClaimProvisionDTO {

    private String peril;
    private String provisionType;
    private Double provisionAmount;
    private String provisionComment;

    public String getPeril() {
        return peril;
    }

    public void setPeril(String peril) {
        this.peril = peril;
    }

    public String getProvisionType() {
        return provisionType;
    }

    public void setProvisionType(String provisionType) {
        this.provisionType = provisionType;
    }

    public Double getProvisionAmount() {
        return provisionAmount;
    }

    public void setProvisionAmount(Double provisionAmount) {
        this.provisionAmount = provisionAmount;
    }

    public String getProvisionComment() {
        return provisionComment;
    }

    public void setProvisionComment(String provisionComment) {
        this.provisionComment = provisionComment;
    }
}
