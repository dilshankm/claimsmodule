package com.infoinstech.claimsmodule.dto.claimintimation;

public class DebitOutstandingDTO {

    private String customerCode;
    private String policyNo;

    public String getCustomerCode() {
        return customerCode;
    }

    public void setCustomerCode(String customerCode) {
        this.customerCode = customerCode;
    }

    public String getPolicyNo() {
        return policyNo;
    }

    public void setPolicyNo(String policyNo) {
        this.policyNo = policyNo;
    }
}
