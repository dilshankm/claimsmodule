package com.infoinstech.claimsmodule.dto.claimintimation;

import java.util.Date;

public class RiskValidationDTO {

    private String policyNo;
    private String riskSequenceNo;
    private Date lossDate;
    private String riskOrder;

    private String policySequenceNo;                //Added by Dumindu   05/04/2019

    public String getPolicyNo() {
        return policyNo;
    }

    public void setPolicyNo(String policyNo) {
        this.policyNo = policyNo;
    }

    public String getRiskSequenceNo() {
        return riskSequenceNo;
    }

    public void setRiskSequenceNo(String riskSequenceNo) {
        this.riskSequenceNo = riskSequenceNo;
    }

    public Date getLossDate() {
        return lossDate;
    }

    public void setLossDate(Date lossDate) {
        this.lossDate = lossDate;
    }

    public String getRiskOrder() {
        return riskOrder;
    }

    public void setRiskOrder(String riskOrder) {
        this.riskOrder = riskOrder;
    }

    public String getPolicySequenceNo() {
        return policySequenceNo;
    }

    public void setPolicySequenceNo(String policySequenceNo) {
        this.policySequenceNo = policySequenceNo;
    }

    @Override
    public String toString() {
        return "RiskValidationDTO{" +
                "policyNo='" + policyNo + '\'' +
                ", riskSequenceNo='" + riskSequenceNo + '\'' +
                ", lossDate=" + lossDate +
                ", riskOrder='" + riskOrder + '\'' +
                '}';
    }
}
