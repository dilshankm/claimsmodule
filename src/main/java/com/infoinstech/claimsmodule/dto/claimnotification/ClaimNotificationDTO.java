package com.infoinstech.claimsmodule.dto.claimnotification;

import java.util.Date;

public class ClaimNotificationDTO {

    private String coverNoteNo;
    private String coverNoteType;
    private String effectiveStartDate;
    private String riskName;
    private String makeAndModel;
    private String yearOfManufacture;

    private Date lossDate;
    private String lossTime;
    private String intimationType;

    private String customerMobile;
    private String callerName;
    private String callerMobile;
    private String callerAddress;
    private String callerEmail;
    private String comments;

    private String lossRemarks;
    private String nearestTown;
    private String placeOfAccident;
    private String causeOfLoss;
    private String causeDescription;

    private String driverName;
    private String driverLicenseNo;
    private String relationshipToInsured;
    private String relationshipDescription;

    private Character insuredAtFault;
    private Character claimDoubtful;
    private Character policeReport;

    private String policeStation;
    private String thirdPartyDetails;

    private String calledTime;

    private Double longitude;
    private Double latitude;

    private String actionTaken;
    private String assessorCode;
    private String assignmentTypeCode;
    private String promisedTime;

    private String handlerCode;
    private String handlerName;

    private String createdBy;
    private String modifiedBy;

    private byte[] baseString;

    public String getCoverNoteNo() {
        return coverNoteNo;
    }

    public void setCoverNoteNo(String coverNoteNo) {
        this.coverNoteNo = coverNoteNo;
    }

    public String getCoverNoteType() {
        return coverNoteType;
    }

    public void setCoverNoteType(String coverNoteType) {
        this.coverNoteType = coverNoteType;
    }

    public String getEffectiveStartDate() {
        return effectiveStartDate;
    }

    public void setEffectiveStartDate(String effectiveStartDate) {
        this.effectiveStartDate = effectiveStartDate;
    }

    public String getRiskName() {
        return riskName;
    }

    public void setRiskName(String riskName) {
        this.riskName = riskName;
    }

    public String getMakeAndModel() {
        return makeAndModel;
    }

    public void setMakeAndModel(String makeAndModel) {
        this.makeAndModel = makeAndModel;
    }

    public String getYearOfManufacture() {
        return yearOfManufacture;
    }

    public void setYearOfManufacture(String yearOfManufacture) {
        this.yearOfManufacture = yearOfManufacture;
    }

    public Date getLossDate() {
        return lossDate;
    }

    public void setLossDate(Date lossDate) {
        this.lossDate = lossDate;
    }

    public String getLossTime() {
        return lossTime;
    }

    public void setLossTime(String lossTime) {
        this.lossTime = lossTime;
    }

    public String getIntimationType() {
        return intimationType;
    }

    public void setIntimationType(String intimationType) {
        this.intimationType = intimationType;
    }

    public String getCustomerMobile() {
        return customerMobile;
    }

    public void setCustomerMobile(String customerMobile) {
        this.customerMobile = customerMobile;
    }

    public String getCallerName() {
        return callerName;
    }

    public void setCallerName(String callerName) {
        this.callerName = callerName;
    }

    public String getCallerMobile() {
        return callerMobile;
    }

    public void setCallerMobile(String callerMobile) {
        this.callerMobile = callerMobile;
    }

    public String getCallerAddress() {
        return callerAddress;
    }

    public void setCallerAddress(String callerAddress) {
        this.callerAddress = callerAddress;
    }

    public String getCallerEmail() {
        return callerEmail;
    }

    public void setCallerEmail(String callerEmail) {
        this.callerEmail = callerEmail;
    }

    public String getComments() {
        return comments;
    }

    public void setComments(String comments) {
        this.comments = comments;
    }

    public String getLossRemarks() {
        return lossRemarks;
    }

    public void setLossRemarks(String lossRemarks) {
        this.lossRemarks = lossRemarks;
    }

    public String getNearestTown() {
        return nearestTown;
    }

    public void setNearestTown(String nearestTown) {
        this.nearestTown = nearestTown;
    }

    public String getPlaceOfAccident() {
        return placeOfAccident;
    }

    public void setPlaceOfAccident(String placeOfAccident) {
        this.placeOfAccident = placeOfAccident;
    }

    public String getCauseOfLoss() {
        return causeOfLoss;
    }

    public void setCauseOfLoss(String causeOfLoss) {
        this.causeOfLoss = causeOfLoss;
    }

    public String getCauseDescription() {
        return causeDescription;
    }

    public void setCauseDescription(String causeDescription) {
        this.causeDescription = causeDescription;
    }

    public String getDriverName() {
        return driverName;
    }

    public void setDriverName(String driverName) {
        this.driverName = driverName;
    }

    public String getDriverLicenseNo() {
        return driverLicenseNo;
    }

    public void setDriverLicenseNo(String driverLicenseNo) {
        this.driverLicenseNo = driverLicenseNo;
    }

    public String getRelationshipToInsured() {
        return relationshipToInsured;
    }

    public void setRelationshipToInsured(String relationshipToInsured) {
        this.relationshipToInsured = relationshipToInsured;
    }

    public String getRelationshipDescription() {
        return relationshipDescription;
    }

    public void setRelationshipDescription(String relationshipDescription) {
        this.relationshipDescription = relationshipDescription;
    }

    public Character getInsuredAtFault() {
        return insuredAtFault;
    }

    public void setInsuredAtFault(Character insuredAtFault) {
        this.insuredAtFault = insuredAtFault;
    }

    public Character getClaimDoubtful() {
        return claimDoubtful;
    }

    public void setClaimDoubtful(Character claimDoubtful) {
        this.claimDoubtful = claimDoubtful;
    }

    public Character getPoliceReport() {
        return policeReport;
    }

    public void setPoliceReport(Character policeReport) {
        this.policeReport = policeReport;
    }

    public String getPoliceStation() {
        return policeStation;
    }

    public void setPoliceStation(String policeStation) {
        this.policeStation = policeStation;
    }

    public String getThirdPartyDetails() {
        return thirdPartyDetails;
    }

    public void setThirdPartyDetails(String thirdPartyDetails) {
        this.thirdPartyDetails = thirdPartyDetails;
    }

    public String getCalledTime() {
        return calledTime;
    }

    public void setCalledTime(String calledTime) {
        this.calledTime = calledTime;
    }

    public Double getLongitude() {
        return longitude;
    }

    public void setLongitude(Double longitude) {
        this.longitude = longitude;
    }

    public Double getLatitude() {
        return latitude;
    }

    public void setLatitude(Double latitude) {
        this.latitude = latitude;
    }

    public String getActionTaken() {
        return actionTaken;
    }

    public void setActionTaken(String actionTaken) {
        this.actionTaken = actionTaken;
    }

    public String getAssessorCode() {
        return assessorCode;
    }

    public void setAssessorCode(String assessorCode) {
        this.assessorCode = assessorCode;
    }

    public String getAssignmentTypeCode() {
        return assignmentTypeCode;
    }

    public void setAssignmentTypeCode(String assignmentTypeCode) {
        this.assignmentTypeCode = assignmentTypeCode;
    }

    public String getPromisedTime() {
        return promisedTime;
    }

    public void setPromisedTime(String promisedTime) {
        this.promisedTime = promisedTime;
    }

    public String getHandlerCode() {
        return handlerCode;
    }

    public void setHandlerCode(String handlerCode) {
        this.handlerCode = handlerCode;
    }

    public String getHandlerName() {
        return handlerName;
    }

    public void setHandlerName(String handlerName) {
        this.handlerName = handlerName;
    }

    public String getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(String createdBy) {
        this.createdBy = createdBy;
    }

    public String getModifiedBy() {
        return modifiedBy;
    }

    public void setModifiedBy(String modifiedBy) {
        this.modifiedBy = modifiedBy;
    }

    public byte[] getBaseString() {
        return baseString;
    }

    public void setBaseString(byte[] baseString) {
        this.baseString = baseString;
    }
}
