package com.infoinstech.claimsmodule.dto.claimnotification;


public class VehicleNoDTO {

    private String vehicleNo;

    public String getVehicleNo() {
        return vehicleNo;
    }

    public void setVehicleNo(String vehicleNo) {
        this.vehicleNo = vehicleNo;
    }
}
