package com.infoinstech.claimsmodule.dto.claimreference;

public class PolicyPerilsRequestDTO {

    private String policyNo;
    private String lossDate;
    private String riskOrder;

    public String getPolicyNo() {
        return policyNo;
    }

    public void setPolicyNo(String policyNo) {
        this.policyNo = policyNo;
    }

    public String getLossDate() {
        return lossDate;
    }

    public void setLossDate(String lossDate) {
        this.lossDate = lossDate;
    }

    public String getRiskOrder() {
        return riskOrder;
    }

    public void setRiskOrder(String riskOrder) {
        this.riskOrder = riskOrder;
    }
}
