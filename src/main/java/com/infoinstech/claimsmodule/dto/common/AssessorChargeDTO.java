package com.infoinstech.claimsmodule.dto.common;

public class AssessorChargeDTO {

    private String dateVisited;
    private String chargeTypeCode;
    private String unitTypeCode;
    private Double noOfUnits;
    private Double unitPrice;

    public String getDateVisited() {
        return dateVisited;
    }

    public void setDateVisited(String dateVisited) {
        this.dateVisited = dateVisited;
    }

    public String getChargeTypeCode() {
        return chargeTypeCode;
    }

    public void setChargeTypeCode(String chargeTypeCode) {
        this.chargeTypeCode = chargeTypeCode;
    }

    public String getUnitTypeCode() {
        return unitTypeCode;
    }

    public void setUnitTypeCode(String unitTypeCode) {
        this.unitTypeCode = unitTypeCode;
    }

    public Double getNoOfUnits() {
        return noOfUnits;
    }

    public void setNoOfUnits(Double noOfUnits) {
        this.noOfUnits = noOfUnits;
    }

    public Double getUnitPrice() {
        return unitPrice;
    }

    public void setUnitPrice(Double unitPrice) {
        this.unitPrice = unitPrice;
    }
}
