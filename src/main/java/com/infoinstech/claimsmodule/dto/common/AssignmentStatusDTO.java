package com.infoinstech.claimsmodule.dto.common;

public class AssignmentStatusDTO {

    private String jobNo;
    private String status;

    public String getJobNo() {
        return jobNo;
    }

    public void setJobNo(String jobNo) {
        this.jobNo = jobNo;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }
}
