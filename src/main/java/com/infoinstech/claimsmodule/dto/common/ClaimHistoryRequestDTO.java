package com.infoinstech.claimsmodule.dto.common;

public class ClaimHistoryRequestDTO {

    private String policySequenceNo;
    private String riskSequenceNo;

    public String getPolicySequenceNo() {
        return policySequenceNo;
    }

    public void setPolicySequenceNo(String policySequenceNo) {
        this.policySequenceNo = policySequenceNo;
    }

    public String getRiskSequenceNo() {
        return riskSequenceNo;
    }

    public void setRiskSequenceNo(String riskSequenceNo) {
        this.riskSequenceNo = riskSequenceNo;
    }
}
