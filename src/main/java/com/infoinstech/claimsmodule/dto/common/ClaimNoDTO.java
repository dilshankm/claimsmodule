package com.infoinstech.claimsmodule.dto.common;

public class ClaimNoDTO {

    private String claimNo;

    public String getClaimNo() {
        return claimNo;
    }

    public void setClaimNo(String claimNo) {
        this.claimNo = claimNo;
    }
}
