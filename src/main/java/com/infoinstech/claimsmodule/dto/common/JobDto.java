package com.infoinstech.claimsmodule.dto.common;

public class JobDto {

    private String jobno;

    private String customername;
    private String customernic;
    private String customerphonenumber;

    private String policynumber;
    private int policyperiod;
    private String policystatus;

    private String vehiclenumber;

//    Set<JobDto> jobdtolist = new HashSet<JobDto>();

    public String getJobno() {
        return jobno;
    }

    public void setJobno(String jobno) {
        this.jobno = jobno;
    }

    public String getCustomername() {
        return customername;
    }

    public void setCustomername(String customername) {
        this.customername = customername;
    }

    public String getCustomernic() {
        return customernic;
    }

    public void setCustomernic(String customernic) {
        this.customernic = customernic;
    }

    public String getCustomerphonenumber() {
        return customerphonenumber;
    }

    public void setCustomerphonenumber(String customerphonenumber) {
        this.customerphonenumber = customerphonenumber;
    }

    public String getPolicynumber() {
        return policynumber;
    }

    public void setPolicynumber(String policynumber) {
        this.policynumber = policynumber;
    }

    public int getPolicyperiod() {
        return policyperiod;
    }

    public void setPolicyperiod(int policyperiod) {
        this.policyperiod = policyperiod;
    }

    public String getPolicystatus() {
        return policystatus;
    }

    public void setPolicystatus(String policystatus) {
        this.policystatus = policystatus;
    }

    public String getVehiclenumber() {
        return vehiclenumber;
    }

    public void setVehiclenumber(String vehiclenumber) {
        this.vehiclenumber = vehiclenumber;
    }
//
//    public Set<JobDto> getJobdtolist() {
//        return jobdtolist;
//    }
//
//    public void setJobdtolist(Set<JobDto> jobdtolist) {
//        this.jobdtolist = jobdtolist;
//    }
}

