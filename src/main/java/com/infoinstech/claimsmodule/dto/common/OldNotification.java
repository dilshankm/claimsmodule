package com.infoinstech.claimsmodule.dto.common;

public class OldNotification {

    private String jobid;
    private String assessorcode;
    private String customername;
    private String location;
    private String accidenttime;
    private String status;
    private String policyid;
    private String customermobileno;
    private String vehicleno;
    private String actiontaken;

    public String getJobid() {
        return jobid;
    }

    public void setJobid(String jobid) {
        this.jobid = jobid;
    }

    public String getAssessorcode() {
        return assessorcode;
    }

    public void setAssessorcode(String assessorcode) {
        this.assessorcode = assessorcode;
    }

    public String getCustomername() {
        return customername;
    }

    public void setCustomername(String customername) {
        this.customername = customername;
    }

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    public String getAccidenttime() {
        return accidenttime;
    }

    public void setAccidenttime(String accidenttime) {
        this.accidenttime = accidenttime;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getPolicyid() {
        return policyid;
    }

    public void setPolicyid(String policyid) {
        this.policyid = policyid;
    }

    public String getCustomermobileno() {
        return customermobileno;
    }

    public void setCustomermobileno(String customermobileno) {
        this.customermobileno = customermobileno;
    }

    public String getVehicleno() {
        return vehicleno;
    }

    public void setVehicleno(String vehicleno) {
        this.vehicleno = vehicleno;
    }

    public String getActiontaken() {
        return actiontaken;
    }

    public void setActiontaken(String actiontaken) {
        this.actiontaken = actiontaken;
    }
}
