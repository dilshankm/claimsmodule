package com.infoinstech.claimsmodule.dto.common;

public class PolicyDetailSpecDTO {
    private String policyno;
    private String nicno;
    private String firstname;
    private String vehicleno;
    private String polstatus;
    private String polseqno;
    private String polriskid;

    public String getPolseqno() {
        return polseqno;
    }

    public void setPolseqno(String polseqno) {
        this.polseqno = polseqno;
    }

    public String getPolriskid() {
        return polriskid;
    }

    public void setPolriskid(String polriskid) {
        this.polriskid = polriskid;
    }

    public String getPolicyno() {
        return policyno;
    }

    public void setPolicyno(String policyno) {
        this.policyno = policyno;
    }

    public String getNicno() {
        return nicno;
    }

    public void setNicno(String nicno) {
        this.nicno = nicno;
    }

    public String getFirstname() {
        return firstname;
    }

    public void setFirstname(String firstname) {
        this.firstname = firstname;
    }

    public String getVehicleno() {
        return vehicleno;
    }

    public void setVehicleno(String vehicleno) {
        this.vehicleno = vehicleno;
    }

    public String getPolstatus() {
        return polstatus;
    }

    public void setPolstatus(String polstatus) {
        this.polstatus = polstatus;
    }
}
