package com.infoinstech.claimsmodule.dto.common;

public class PolicyDetailsDTO {

    private String policyNo;
    private String customerName;
    private String customerNIC;
    private String customerAddress;
    private String riskEffectiveDate;
    private String expiryDate;
    private String policyClass;
    private String product;
    private String branch;
    private String status;
    private String intermediaryType;
    private String intermediary;
    private String grossWrittenPremium;
    private String policyPremium;
    private String noClaimBonus;
    private String businessParty;

    private String vehicleNo;
    private String location;
    private String sumInsured;
    private String engineNo;
    private String chassisNo;
    private String cubicCapacity;
    private String yearOfManufacture;
    private String makeAndModel;
    private String vehicleUsage;
    private String vehicleCategory;

    public String getPolicyNo() {
        return policyNo;
    }

    public void setPolicyNo(String policyNo) {
        this.policyNo = policyNo;
    }

    public String getCustomerName() {
        return customerName;
    }

    public void setCustomerName(String customerName) {
        this.customerName = customerName;
    }

    public String getCustomerNIC() {
        return customerNIC;
    }

    public void setCustomerNIC(String customerNIC) {
        this.customerNIC = customerNIC;
    }

    public String getCustomerAddress() {
        return customerAddress;
    }

    public void setCustomerAddress(String customerAddress) {
        this.customerAddress = customerAddress;
    }

    public String getRiskEffectiveDate() {
        return riskEffectiveDate;
    }

    public void setRiskEffectiveDate(String riskEffectiveDate) {
        this.riskEffectiveDate = riskEffectiveDate;
    }

    public String getExpiryDate() {
        return expiryDate;
    }

    public void setExpiryDate(String expiryDate) {
        this.expiryDate = expiryDate;
    }

    public String getPolicyClass() {
        return policyClass;
    }

    public void setPolicyClass(String policyClass) {
        this.policyClass = policyClass;
    }

    public String getProduct() {
        return product;
    }

    public void setProduct(String product) {
        this.product = product;
    }

    public String getBranch() {
        return branch;
    }

    public void setBranch(String branch) {
        this.branch = branch;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getIntermediaryType() {
        return intermediaryType;
    }

    public void setIntermediaryType(String intermediaryType) {
        this.intermediaryType = intermediaryType;
    }

    public String getIntermediary() {
        return intermediary;
    }

    public void setIntermediary(String intermediary) {
        this.intermediary = intermediary;
    }

    public String getGrossWrittenPremium() {
        return grossWrittenPremium;
    }

    public void setGrossWrittenPremium(String grossWrittenPremium) {
        this.grossWrittenPremium = grossWrittenPremium;
    }

    public String getPolicyPremium() {
        return policyPremium;
    }

    public void setPolicyPremium(String policyPremium) {
        this.policyPremium = policyPremium;
    }

    public String getNoClaimBonus() {
        return noClaimBonus;
    }

    public void setNoClaimBonus(String noClaimBonus) {
        this.noClaimBonus = noClaimBonus;
    }

    public String getBusinessParty() {
        return businessParty;
    }

    public void setBusinessParty(String businessParty) {
        this.businessParty = businessParty;
    }

    public String getVehicleNo() {
        return vehicleNo;
    }

    public void setVehicleNo(String vehicleNo) {
        this.vehicleNo = vehicleNo;
    }

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    public String getSumInsured() {
        return sumInsured;
    }

    public void setSumInsured(String sumInsured) {
        this.sumInsured = sumInsured;
    }

    public String getEngineNo() {
        return engineNo;
    }

    public void setEngineNo(String engineNo) {
        this.engineNo = engineNo;
    }

    public String getChassisNo() {
        return chassisNo;
    }

    public void setChassisNo(String chassisNo) {
        this.chassisNo = chassisNo;
    }

    public String getCubicCapacity() {
        return cubicCapacity;
    }

    public void setCubicCapacity(String cubicCapacity) {
        this.cubicCapacity = cubicCapacity;
    }

    public String getYearOfManufacture() {
        return yearOfManufacture;
    }

    public void setYearOfManufacture(String yearOfManufacture) {
        this.yearOfManufacture = yearOfManufacture;
    }

    public String getMakeAndModel() {
        return makeAndModel;
    }

    public void setMakeAndModel(String makeAndModel) {
        this.makeAndModel = makeAndModel;
    }

    public String getVehicleUsage() {
        return vehicleUsage;
    }

    public void setVehicleUsage(String vehicleUsage) {
        this.vehicleUsage = vehicleUsage;
    }

    public String getVehicleCategory() {
        return vehicleCategory;
    }

    public void setVehicleCategory(String vehicleCategory) {
        this.vehicleCategory = vehicleCategory;
    }
}

