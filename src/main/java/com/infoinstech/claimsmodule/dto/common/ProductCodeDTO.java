package com.infoinstech.claimsmodule.dto.common;

public class ProductCodeDTO {

    private String productCode;

    public String getProductCode() {
        return productCode;
    }

    public void setProductCode(String productCode) {
        this.productCode = productCode;
    }
}
