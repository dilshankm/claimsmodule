package com.infoinstech.claimsmodule.dto.common;

import lombok.Data;

@Data
public class ReferenceDTO {

    private String code;
    private String description;

    public ReferenceDTO() {
    }

    public ReferenceDTO(String code, String description) {
        this.code = code;
        this.description = description;
    }
}
