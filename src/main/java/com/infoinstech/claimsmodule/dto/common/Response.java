package com.infoinstech.claimsmodule.dto.common;

public class Response {

    private Integer code;
    private Boolean status;
    private String message;
    private Object data;

    public Response() {
    }


    public Response(Integer code, Boolean status, Object data) {
        this.code = code;
        this.status = status;
        this.data = data;
        this.message = "";
    }

    public Response(Integer code, Boolean status, String message, Object data) {
        this.code = code;
        this.status = status;
        this.message = message;
        this.data = data;
    }

    public Response(Integer code, Boolean status, String message) {
        this.code = code;
        this.status = status;
        this.message = message;
    }

    public Integer getCode() {
        return code;
    }

    public void setCode(Integer code) {
        this.code = code;
    }

    public Boolean getStatus() {
        return status;
    }

    public void setStatus(Boolean status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public Object getData() {
        return data;
    }

    public void setData(Object data) {
        this.data = data;
    }
}

