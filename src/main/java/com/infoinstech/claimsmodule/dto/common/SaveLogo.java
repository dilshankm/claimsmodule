package com.infoinstech.claimsmodule.dto.common;

public class SaveLogo {

    private String logo;

    public String getLogo() {
        return logo;
    }

    public void setLogo(String logo) {
        this.logo = logo;
    }
}
