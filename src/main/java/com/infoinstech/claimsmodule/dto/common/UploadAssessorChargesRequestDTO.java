package com.infoinstech.claimsmodule.dto.common;

import java.util.List;

public class UploadAssessorChargesRequestDTO {

    private String jobNo;
    private String claimNo;
    private List<AssessorChargeDTO> chargesList;

    public String getJobNo() {
        return jobNo;
    }

    public void setJobNo(String jobNo) {
        this.jobNo = jobNo;
    }

    public String getClaimNo() {
        return claimNo;
    }

    public void setClaimNo(String claimNo) {
        this.claimNo = claimNo;
    }

    public List<AssessorChargeDTO> getChargesList() {
        return chargesList;
    }

    public void setChargesList(List<AssessorChargeDTO> chargesList) {
        this.chargesList = chargesList;
    }
}
