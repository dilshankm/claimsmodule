package com.infoinstech.claimsmodule.dto.common;

public class ViewPolicyRequestDTO {

    private String riskSequenceNo;
    private String policySequenceNo;

    public String getRiskSequenceNo() {
        return riskSequenceNo;
    }

    public void setRiskSequenceNo(String riskSequenceNo) {
        this.riskSequenceNo = riskSequenceNo;
    }

    public String getPolicySequenceNo() {
        return policySequenceNo;
    }

    public void setPolicySequenceNo(String policySequenceNo) {
        this.policySequenceNo = policySequenceNo;
    }
}
