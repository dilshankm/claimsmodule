package com.infoinstech.claimsmodule.dto.dashboard;

public class AccidentDTO {

    private String longitude;
    private String latitude;
    private String location;
    private String claimNo;
    private String vehicleNo;
    private String contactNo;
    private String assessorCode;


    public String getLongitude() {
        return longitude;
    }

    public void setLongitude(String longitude) {
        this.longitude = longitude;
    }

    public String getLatitude() {
        return latitude;
    }

    public void setLatitude(String latitude) {
        this.latitude = latitude;
    }

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    public String getClaimNo() {
        return claimNo;
    }

    public void setClaimNo(String claimNo) {
        this.claimNo = claimNo;
    }

    public String getVehicleNo() {
        return vehicleNo;
    }

    public void setVehicleNo(String vehicleNo) {
        this.vehicleNo = vehicleNo;
    }

    public String getContactNo() {
        return contactNo;
    }

    public void setContactNo(String contactNo) {
        this.contactNo = contactNo;
    }

    public String getAssessorCode() {
        return assessorCode;
    }

    public void setAssessorCode(String assessorCode) {
        this.assessorCode = assessorCode;
    }
}
