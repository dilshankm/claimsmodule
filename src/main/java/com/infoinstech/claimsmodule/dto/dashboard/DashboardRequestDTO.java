package com.infoinstech.claimsmodule.dto.dashboard;

public class DashboardRequestDTO {

    private String assessorCode;
    private String fromDate;
    private String toDate;
    private String status;

    public String getAssessorCode() {
        return assessorCode;
    }

    public void setAssessorCode(String assessorCode) {
        this.assessorCode = assessorCode;
    }

    public String getFromDate() {
        return fromDate;
    }

    public void setFromDate(String fromDate) {
        this.fromDate = fromDate;
    }

    public String getToDate() {
        return toDate;
    }

    public void setToDate(String toDate) {
        this.toDate = toDate;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }
}
