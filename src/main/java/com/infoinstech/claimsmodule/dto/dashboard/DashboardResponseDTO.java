package com.infoinstech.claimsmodule.dto.dashboard;

import com.infoinstech.claimsmodule.dto.claiminquiry.JobAssignmentDTO;

import java.util.List;

public class DashboardResponseDTO {

    private List<JobAssignmentDTO> jobAssignments;
    private AssignmentCountDTO assignmentCount;

    public List<JobAssignmentDTO> getJobAssignments() {
        return jobAssignments;
    }

    public void setJobAssignments(List<JobAssignmentDTO> jobAssignments) {
        this.jobAssignments = jobAssignments;
    }

    public AssignmentCountDTO getAssignmentCount() {
        return assignmentCount;
    }

    public void setAssignmentCount(AssignmentCountDTO assignmentCount) {
        this.assignmentCount = assignmentCount;
    }
}
