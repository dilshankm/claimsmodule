package com.infoinstech.claimsmodule.dto.deviceregistration;

public class AssessorCode {

    private String assessorCode;

    public String getAssessorCode() {
        return assessorCode;
    }

    public void setAssessorCode(String assessorCode) {
        this.assessorCode = assessorCode;
    }
}
