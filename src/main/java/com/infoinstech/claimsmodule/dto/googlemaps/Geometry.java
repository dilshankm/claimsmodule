package com.infoinstech.claimsmodule.dto.googlemaps;

public class Geometry {

    private Location location;

    public Location getLocation() {
        return location;
    }

    public void setLocation(Location location) {
        this.location = location;
    }
}
