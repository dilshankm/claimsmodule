package com.infoinstech.claimsmodule.dto.googlemaps;

public class PlaceDetailsResponse {

    private Result result;
    private String status;

    public Result getResult() {
        return result;
    }

    public void setResult(Result result) {
        this.result = result;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }
}
