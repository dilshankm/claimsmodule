package com.infoinstech.claimsmodule.dto.imageservice;

import java.util.List;

public class ImagesUploadRequestDTO {

    private String claimNo;
    private String jobNo;
    private String uploadType;
    private List<ImageDTO> imagesList;

    public String getClaimNo() {
        return claimNo;
    }

    public void setClaimNo(String claimNo) {
        this.claimNo = claimNo;
    }

    public String getJobNo() {
        return jobNo;
    }

    public void setJobNo(String jobNo) {
        this.jobNo = jobNo;
    }

    public String getUploadType() {
        return uploadType;
    }

    public void setUploadType(String uploadType) {
        this.uploadType = uploadType;
    }

    public List<ImageDTO> getImagesList() {
        return imagesList;
    }

    public void setImagesList(List<ImageDTO> imagesList) {
        this.imagesList = imagesList;
    }
}
