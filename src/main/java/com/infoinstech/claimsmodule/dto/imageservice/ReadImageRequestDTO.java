package com.infoinstech.claimsmodule.dto.imageservice;

public class ReadImageRequestDTO {

    private String jobNo;
    private String imageType;

    public String getJobNo() {
        return jobNo;
    }

    public void setJobNo(String jobNo) {
        this.jobNo = jobNo;
    }

    public String getImageType() {
        return imageType;
    }

    public void setImageType(String imageType) {
        this.imageType = imageType;
    }
}
