package com.infoinstech.claimsmodule.dto.imageservice;

public class ViewImageDTO {

    private String imageName;
    private String imageString;

    public String getImageName() {
        return imageName;
    }

    public void setImageName(String imageName) {
        this.imageName = imageName;
    }

    public String getImageString() {
        return imageString;
    }

    public void setImageString(String imageString) {
        this.imageString = imageString;
    }
}
