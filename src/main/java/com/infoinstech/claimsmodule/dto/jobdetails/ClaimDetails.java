package com.infoinstech.claimsmodule.dto.jobdetails;

public class ClaimDetails {

    private String claimNo;
    private String timeOfAccident;
    private String timeOfIntimation;
    private String placeOfAccident;
    private String causeOfLoss;
    private String driversName;
    private String driversLicenseNo;
    private String contactPersonNo;
    private Double latitude;
    private Double longitude;

    public String getClaimNo() {
        return claimNo;
    }

    public void setClaimNo(String claimNo) {
        this.claimNo = claimNo;
    }

    public String getTimeOfAccident() {
        return timeOfAccident;
    }

    public void setTimeOfAccident(String timeOfAccident) {
        this.timeOfAccident = timeOfAccident;
    }

    public String getTimeOfIntimation() {
        return timeOfIntimation;
    }

    public void setTimeOfIntimation(String timeOfIntimation) {
        this.timeOfIntimation = timeOfIntimation;
    }

    public String getPlaceOfAccident() {
        return placeOfAccident;
    }

    public void setPlaceOfAccident(String placeOfAccident) {
        this.placeOfAccident = placeOfAccident;
    }

    public String getCauseOfLoss() {
        return causeOfLoss;
    }

    public void setCauseOfLoss(String causeOfLoss) {
        this.causeOfLoss = causeOfLoss;
    }

    public String getDriversName() {
        return driversName;
    }

    public void setDriversName(String driversName) {
        this.driversName = driversName;
    }

    public String getDriversLicenseNo() {
        return driversLicenseNo;
    }

    public void setDriversLicenseNo(String driversLicenseNo) {
        this.driversLicenseNo = driversLicenseNo;
    }

    public String getContactPersonNo() {
        return contactPersonNo;
    }

    public void setContactPersonNo(String contactPersonNo) {
        this.contactPersonNo = contactPersonNo;
    }

    public Double getLatitude() {
        return latitude;
    }

    public void setLatitude(Double latitude) {
        this.latitude = latitude;
    }

    public Double getLongitude() {
        return longitude;
    }

    public void setLongitude(Double longitude) {
        this.longitude = longitude;
    }

}
