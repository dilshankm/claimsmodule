package com.infoinstech.claimsmodule.dto.jobdetails;

public class ExcessDetails {

    private String excessDescription;
    private String excessPercentage;
    private String excessRate;
    private String excessCode;
    private String excessSeqNo;
    private String riskSeqno;

    public String getExcessDescription() {
        return excessDescription;
    }

    public void setExcessDescription(String excessDescription) {
        this.excessDescription = excessDescription;
    }

    public String getExcessPercentage() {
        return excessPercentage;
    }

    public void setExcessPercentage(String excessPercentage) {
        this.excessPercentage = excessPercentage;
    }

    public String getExcessRate() {
        return excessRate;
    }

    public void setExcessRate(String excessRate) {
        this.excessRate = excessRate;
    }

    public String getExcessCode() {
        return excessCode;
    }

    public void setExcessCode(String excessCode) {
        this.excessCode = excessCode;
    }

    public String getExcessSeqNo() {
        return excessSeqNo;
    }

    public void setExcessSeqNo(String excessSeqNo) {
        this.excessSeqNo = excessSeqNo;
    }

    public String getRiskSeqno() {
        return riskSeqno;
    }

    public void setRiskSeqno(String riskSeqno) {
        this.riskSeqno = riskSeqno;
    }
}
