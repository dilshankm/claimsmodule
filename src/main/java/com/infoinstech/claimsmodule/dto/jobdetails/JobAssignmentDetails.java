package com.infoinstech.claimsmodule.dto.jobdetails;

import com.infoinstech.claimsmodule.dto.common.ClaimHistoryDTO;

import java.util.List;

public class JobAssignmentDetails {


    private JobDetails jobDetails;
    private ClaimDetails claimDetails;
    private PolicyDetails policyDetails;
    private CustomerDetails customerDetails;
    private VehicleDetails vehicleDetails;

    private List<CoverDetails> coverDetails;
    private List<ExcessDetails> excessDetails;
    private List<ClaimHistoryDTO> claimHistory;
    private List<String> vehicleSketches;

    public JobDetails getJobDetails() {
        return jobDetails;
    }

    public void setJobDetails(JobDetails jobDetails) {
        this.jobDetails = jobDetails;
    }

    public ClaimDetails getClaimDetails() {
        return claimDetails;
    }

    public void setClaimDetails(ClaimDetails claimDetails) {
        this.claimDetails = claimDetails;
    }

    public PolicyDetails getPolicyDetails() {
        return policyDetails;
    }

    public void setPolicyDetails(PolicyDetails policyDetails) {
        this.policyDetails = policyDetails;
    }

    public CustomerDetails getCustomerDetails() {
        return customerDetails;
    }

    public void setCustomerDetails(CustomerDetails customerDetails) {
        this.customerDetails = customerDetails;
    }

    public VehicleDetails getVehicleDetails() {
        return vehicleDetails;
    }

    public void setVehicleDetails(VehicleDetails vehicleDetails) {
        this.vehicleDetails = vehicleDetails;
    }

    public List<CoverDetails> getCoverDetails() {
        return coverDetails;
    }

    public void setCoverDetails(List<CoverDetails> coverDetails) {
        this.coverDetails = coverDetails;
    }

    public List<ExcessDetails> getExcessDetails() {
        return excessDetails;
    }

    public void setExcessDetails(List<ExcessDetails> excessDetails) {
        this.excessDetails = excessDetails;
    }

    public List<ClaimHistoryDTO> getClaimHistory() {
        return claimHistory;
    }

    public void setClaimHistory(List<ClaimHistoryDTO> claimHistory) {
        this.claimHistory = claimHistory;
    }

    public List<String> getVehicleSketches() {
        return vehicleSketches;
    }

    public void setVehicleSketches(List<String> vehicleSketches) {
        this.vehicleSketches = vehicleSketches;
    }
}
