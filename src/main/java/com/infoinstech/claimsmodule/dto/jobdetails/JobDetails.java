package com.infoinstech.claimsmodule.dto.jobdetails;

import java.util.List;

public class JobDetails {

    private String jobNo;
    private String jobType;
    private String timeOfAssignment;
    private String requiredDocuments;

    public String getJobNo() {
        return jobNo;
    }

    public void setJobNo(String jobNo) {
        this.jobNo = jobNo;
    }

    public String getJobType() {
        return jobType;
    }

    public void setJobType(String jobType) {
        this.jobType = jobType;
    }

    public String getTimeOfAssignment() {
        return timeOfAssignment;
    }

    public void setTimeOfAssignment(String timeOfAssignment) {
        this.timeOfAssignment = timeOfAssignment;
    }

    public String getRequiredDocuments() {
        return requiredDocuments;
    }

    public void setRequiredDocuments(String requiredDocuments) {
        this.requiredDocuments = requiredDocuments;
    }
}
