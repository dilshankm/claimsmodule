package com.infoinstech.claimsmodule.dto.jobdetails;

public class PolicyDetails {

    private String policyStatus;
    private String policyPeriod;
    private String policyNo;
    private String policySumInsured;
    private String outstandingAmount;
    private String otherInterests;

    public String getPolicyStatus() {
        return policyStatus;
    }

    public void setPolicyStatus(String policyStatus) {
        this.policyStatus = policyStatus;
    }

    public String getPolicyPeriod() {
        return policyPeriod;
    }

    public void setPolicyPeriod(String policyPeriod) {
        this.policyPeriod = policyPeriod;
    }

    public String getPolicyNo() {
        return policyNo;
    }

    public void setPolicyNo(String policyNo) {
        this.policyNo = policyNo;
    }

    public String getPolicySumInsured() {
        return policySumInsured;
    }

    public void setPolicySumInsured(String policySumInsured) {
        this.policySumInsured = policySumInsured;
    }

    public String getOutstandingAmount() {
        return outstandingAmount;
    }

    public void setOutstandingAmount(String outstandingAmount) {
        this.outstandingAmount = outstandingAmount;
    }

    public String getOtherInterests() {
        return otherInterests;
    }

    public void setOtherInterests(String otherInterests) {
        this.otherInterests = otherInterests;
    }
}
