package com.infoinstech.claimsmodule.dto.jobdetails;

public class VehicleDetails {

    private String vehicleNo;
    private String chassisNo;
    private String engineNo;
    private String vehicleSumInsured;
    private String makeAndModel;
    private String dateRegistered;
    private String yearOfManfucature;
    private String useOfVehicle;

    public String getVehicleNo() {
        return vehicleNo;
    }

    public void setVehicleNo(String vehicleNo) {
        this.vehicleNo = vehicleNo;
    }

    public String getChassisNo() {
        return chassisNo;
    }

    public void setChassisNo(String chassisNo) {
        this.chassisNo = chassisNo;
    }

    public String getEngineNo() {
        return engineNo;
    }

    public void setEngineNo(String engineNo) {
        this.engineNo = engineNo;
    }

    public String getVehicleSumInsured() {
        return vehicleSumInsured;
    }

    public void setVehicleSumInsured(String vehicleSumInsured) {
        this.vehicleSumInsured = vehicleSumInsured;
    }

    public String getMakeAndModel() {
        return makeAndModel;
    }

    public void setMakeAndModel(String makeAndModel) {
        this.makeAndModel = makeAndModel;
    }

    public String getDateRegistered() {
        return dateRegistered;
    }

    public void setDateRegistered(String dateRegistered) {
        this.dateRegistered = dateRegistered;
    }

    public String getYearOfManfucature() {
        return yearOfManfucature;
    }

    public void setYearOfManfucature(String yearOfManfucature) {
        this.yearOfManfucature = yearOfManfucature;
    }

    public String getUseOfVehicle() {
        return useOfVehicle;
    }

    public void setUseOfVehicle(String useOfVehicle) {
        this.useOfVehicle = useOfVehicle;
    }
}
