package com.infoinstech.claimsmodule.dto.policysearch;

import com.infoinstech.claimsmodule.dto.common.FieldDTO;

import java.util.List;

public class PolicyDetailsMoreDTO {

    private String policyNo;
    private String customerCode;
    private String customerName;
    private String customerNIC;
    private String customerAddress;
    private String customerContact;
    private String periodFrom;
    private String effectiveFrom;
    private String periodTo;
    private String productClass;
    private String productCode;
    private String productDescription;
    private String branch;
    private String status;
    private String intermediaryType;
    private String intermediaryName;
    private String totalSumInsured;
    private String totalPremium;
    private String businessParty;

    private String sumInsured;
    private String vehicleNo;
    private String riskOrder;
    private String location;
    private List<FieldDTO> additionalInformation;

    public String getPolicyNo() {
        return policyNo;
    }

    public void setPolicyNo(String policyNo) {
        this.policyNo = policyNo;
    }

    public String getCustomerCode() {
        return customerCode;
    }

    public void setCustomerCode(String customerCode) {
        this.customerCode = customerCode;
    }

    public String getCustomerName() {
        return customerName;
    }

    public void setCustomerName(String customerName) {
        this.customerName = customerName;
    }

    public String getCustomerNIC() {
        return customerNIC;
    }

    public void setCustomerNIC(String customerNIC) {
        this.customerNIC = customerNIC;
    }

    public String getCustomerAddress() {
        return customerAddress;
    }

    public void setCustomerAddress(String customerAddress) {
        this.customerAddress = customerAddress;
    }

    public String getCustomerContact() {
        return customerContact;
    }

    public void setCustomerContact(String customerContact) {
        this.customerContact = customerContact;
    }

    public String getPeriodFrom() {
        return periodFrom;
    }

    public void setPeriodFrom(String periodFrom) {
        this.periodFrom = periodFrom;
    }

    public String getEffectiveFrom() {
        return effectiveFrom;
    }

    public void setEffectiveFrom(String effectiveFrom) {
        this.effectiveFrom = effectiveFrom;
    }

    public String getPeriodTo() {
        return periodTo;
    }

    public void setPeriodTo(String periodTo) {
        this.periodTo = periodTo;
    }

    public String getProductClass() {
        return productClass;
    }

    public void setProductClass(String productClass) {
        this.productClass = productClass;
    }

    public String getProductCode() {
        return productCode;
    }

    public void setProductCode(String productCode) {
        this.productCode = productCode;
    }

    public String getProductDescription() {
        return productDescription;
    }

    public void setProductDescription(String productDescription) {
        this.productDescription = productDescription;
    }

    public String getBranch() {
        return branch;
    }

    public void setBranch(String branch) {
        this.branch = branch;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getIntermediaryType() {
        return intermediaryType;
    }

    public void setIntermediaryType(String intermediaryType) {
        this.intermediaryType = intermediaryType;
    }

    public String getIntermediaryName() {
        return intermediaryName;
    }

    public void setIntermediaryName(String intermediaryName) {
        this.intermediaryName = intermediaryName;
    }

    public String getTotalSumInsured() {
        return totalSumInsured;
    }

    public void setTotalSumInsured(String totalSumInsured) {
        this.totalSumInsured = totalSumInsured;
    }

    public String getTotalPremium() {
        return totalPremium;
    }

    public void setTotalPremium(String totalPremium) {
        this.totalPremium = totalPremium;
    }

    public String getBusinessParty() {
        return businessParty;
    }

    public void setBusinessParty(String businessParty) {
        this.businessParty = businessParty;
    }

    public String getSumInsured() {
        return sumInsured;
    }

    public void setSumInsured(String sumInsured) {
        this.sumInsured = sumInsured;
    }

    public String getVehicleNo() {
        return vehicleNo;
    }

    public void setVehicleNo(String vehicleNo) {
        this.vehicleNo = vehicleNo;
    }

    public String getRiskOrder() {
        return riskOrder;
    }

    public void setRiskOrder(String riskOrder) {
        this.riskOrder = riskOrder;
    }

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    public List<FieldDTO> getAdditionalInformation() {
        return additionalInformation;
    }

    public void setAdditionalInformation(List<FieldDTO> additionalInformation) {
        this.additionalInformation = additionalInformation;
    }
}
