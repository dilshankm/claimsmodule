package com.infoinstech.claimsmodule.dto.policysearch;

import lombok.Data;

@Data
public class PolicySearchDTO {
    private String policyNo;
    private String customerNIC;
    private String customerName;
    private String riskName;
    private String customerContact;
}
