package com.infoinstech.claimsmodule.dto.policysearch;

public class PolicySearchResultDTO {

    private String sequenceNo;
    private String policyNo;
    private String polPeriodFrom;
    private String polPeriodTo;
    private String customerCode;
    private String customerName;
    private String customerNIC;
    private String riskSequenceNo;
    private String riskName;
    private String status;
    private String classCode;

    public String getSequenceNo() {
        return sequenceNo;
    }

    public void setSequenceNo(String sequenceNo) {
        this.sequenceNo = sequenceNo;
    }

    public String getPolicyNo() {
        return policyNo;
    }

    public void setPolicyNo(String policyNo) {
        this.policyNo = policyNo;
    }

    public String getPolPeriodFrom() {
        return polPeriodFrom;
    }

    public void setPolPeriodFrom(String polPeriodFrom) {
        this.polPeriodFrom = polPeriodFrom;
    }

    public String getPolPeriodTo() {
        return polPeriodTo;
    }

    public void setPolPeriodTo(String polPeriodTo) {
        this.polPeriodTo = polPeriodTo;
    }

    public String getCustomerName() {
        return customerName;
    }

    public void setCustomerName(String customerName) {
        this.customerName = customerName;
    }

    public String getCustomerCode() {
        return customerCode;
    }

    public void setCustomerCode(String customerCode) {
        this.customerCode = customerCode;
    }

    public String getCustomerNIC() {
        return customerNIC;
    }

    public void setCustomerNIC(String customerNIC) {
        this.customerNIC = customerNIC;
    }

    public String getRiskSequenceNo() {
        return riskSequenceNo;
    }

    public void setRiskSequenceNo(String riskSequenceNo) {
        this.riskSequenceNo = riskSequenceNo;
    }

    public String getRiskName() {
        return riskName;
    }

    public void setRiskName(String riskName) {
        this.riskName = riskName;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getClassCode() {
        return classCode;
    }

    public void setClassCode(String classCode) {
        this.classCode = classCode;
    }
    
}
