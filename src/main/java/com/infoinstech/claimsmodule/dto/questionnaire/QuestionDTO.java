package com.infoinstech.claimsmodule.dto.questionnaire;

import java.util.List;

public class QuestionDTO {

    private String code;
    private String description;
    private String type;
    private String mandatory;
    private String hint;
    private String hintOn;
    private String defaultAnswers;
    private List<AnswerDTO> answerList;


    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getMandatory() {
        return mandatory;
    }

    public void setMandatory(String mandatory) {
        this.mandatory = mandatory;
    }

    public String getHint() {
        return hint;
    }

    public void setHint(String hint) {
        this.hint = hint;
    }

    public String getHintOn() {
        return hintOn;
    }

    public void setHintOn(String hintOn) {
        this.hintOn = hintOn;
    }

    public String getDefaultAnswers() {
        return defaultAnswers;
    }

    public void setDefaultAnswers(String defaultAnswers) {
        this.defaultAnswers = defaultAnswers;
    }

    public List<AnswerDTO> getAnswerList() {
        return answerList;
    }

    public void setAnswerList(List<AnswerDTO> answerList) {
        this.answerList = answerList;
    }
}
