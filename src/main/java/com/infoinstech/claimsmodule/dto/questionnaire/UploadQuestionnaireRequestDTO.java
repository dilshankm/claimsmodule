package com.infoinstech.claimsmodule.dto.questionnaire;

import java.util.List;

public class UploadQuestionnaireRequestDTO {

    private String jobNo;
    private String claimNo;
    private List<UploadQuestionDTO> questions;

    public String getJobNo() {
        return jobNo;
    }

    public void setJobNo(String jobNo) {
        this.jobNo = jobNo;
    }

    public String getClaimNo() {
        return claimNo;
    }

    public void setClaimNo(String claimNo) {
        this.claimNo = claimNo;
    }

    public List<UploadQuestionDTO> getQuestions() {
        return questions;
    }

    public void setQuestions(List<UploadQuestionDTO> questions) {
        this.questions = questions;
    }
}
