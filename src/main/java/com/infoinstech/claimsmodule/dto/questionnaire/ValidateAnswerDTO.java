package com.infoinstech.claimsmodule.dto.questionnaire;

public class ValidateAnswerDTO {

    private boolean validation;
    private Character defaultAnswer;
    private String message;

    public boolean isValidation() {
        return validation;
    }

    public void setValidation(boolean validation) {
        this.validation = validation;
    }

    public Character getDefaultAnswer() {
        return defaultAnswer;
    }

    public void setDefaultAnswer(Character defaultAnswer) {
        this.defaultAnswer = defaultAnswer;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}
