package com.infoinstech.claimsmodule.dto.reference;

public class EventRequestDTO {

    private String lossDate;
    private String intimationDate;

    public String getLossDate() {
        return lossDate;
    }

    public void setLossDate(String lossDate) {
        this.lossDate = lossDate;
    }

    public String getIntimationDate() {
        return intimationDate;
    }

    public void setIntimationDate(String intimationDate) {
        this.intimationDate = intimationDate;
    }
}
