package com.infoinstech.claimsmodule.dto.reports;

/**
 * Created by dushman on 3/20/18.
 */

public class ClaimNoDTOReport {

    private String claimNo;
    private String user;
    private String reportType;

    public String getClaimNo() {
        return claimNo;
    }

    public void setClaimNo(String claimNo) {
        this.claimNo = claimNo;
    }

    public String getUser() {
        return user;
    }

    public void setUser(String user) {
        this.user = user;
    }

    public String getReportType() {
        return reportType;
    }

    public void setReportType(String reportType) {
        this.reportType = reportType;
    }
}

