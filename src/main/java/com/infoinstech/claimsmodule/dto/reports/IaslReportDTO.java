package com.infoinstech.claimsmodule.dto.reports;

public class IaslReportDTO extends ReportEntityDTO {

    private String vehicleNo;
    private String chassisNo;
    private String engineNo;
    private String lastName;
    private String otherNames;
    private String address1;
    private String address2;
    private String city;
    private String dateOfAccident;
    private String timeOfAccident;
    private String policeDivision;
    private String policeStation;
    private String driverName;
    private String driverNIC;
    private String driverLicense;
    private String placeOfAccident;
    private String accidentDistrict;
    private String otherPartyDetails;
    private String remarks;

    public String getVehicleNo() {
        return vehicleNo;
    }

    public void setVehicleNo(String vehicleNo) {
        this.vehicleNo = vehicleNo;
    }

    public String getChassisNo() {
        return chassisNo;
    }

    public void setChassisNo(String chassisNo) {
        this.chassisNo = chassisNo;
    }

    public String getEngineNo() {
        return engineNo;
    }

    public void setEngineNo(String engineNo) {
        this.engineNo = engineNo;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getOtherNames() {
        return otherNames;
    }

    public void setOtherNames(String otherNames) {
        this.otherNames = otherNames;
    }

    public String getAddress1() {
        return address1;
    }

    public void setAddress1(String address1) {
        this.address1 = address1;
    }

    public String getAddress2() {
        return address2;
    }

    public void setAddress2(String address2) {
        this.address2 = address2;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getDateOfAccident() {
        return dateOfAccident;
    }

    public void setDateOfAccident(String dateOfAccident) {
        this.dateOfAccident = dateOfAccident;
    }

    public String getTimeOfAccident() {
        return timeOfAccident;
    }

    public void setTimeOfAccident(String timeOfAccident) {
        this.timeOfAccident = timeOfAccident;
    }

    public String getPoliceDivision() {
        return policeDivision;
    }

    public void setPoliceDivision(String policeDivision) {
        this.policeDivision = policeDivision;
    }

    public String getPoliceStation() {
        return policeStation;
    }

    public void setPoliceStation(String policeStation) {
        this.policeStation = policeStation;
    }

    public String getDriverName() {
        return driverName;
    }

    public void setDriverName(String driverName) {
        this.driverName = driverName;
    }

    public String getDriverNIC() {
        return driverNIC;
    }

    public void setDriverNIC(String driverNIC) {
        this.driverNIC = driverNIC;
    }

    public String getDriverLicense() {
        return driverLicense;
    }

    public void setDriverLicense(String driverLicense) {
        this.driverLicense = driverLicense;
    }

    public String getPlaceOfAccident() {
        return placeOfAccident;
    }

    public void setPlaceOfAccident(String placeOfAccident) {
        this.placeOfAccident = placeOfAccident;
    }

    public String getAccidentDistrict() {
        return accidentDistrict;
    }

    public void setAccidentDistrict(String accidentDistrict) {
        this.accidentDistrict = accidentDistrict;
    }

    public String getOtherPartyDetails() {
        return otherPartyDetails;
    }

    public void setOtherPartyDetails(String otherPartyDetails) {
        this.otherPartyDetails = otherPartyDetails;
    }

    public String getRemarks() {
        return remarks;
    }

    public void setRemarks(String remarks) {
        this.remarks = remarks;
    }
}
