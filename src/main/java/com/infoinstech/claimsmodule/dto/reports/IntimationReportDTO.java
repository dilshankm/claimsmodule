package com.infoinstech.claimsmodule.dto.reports;

public class IntimationReportDTO extends ReportEntityDTO {

    private String intimationDate;
    private String dateOfLoss;
    private String vehicleNo;
    private String claimNo;
    private String insured;
    private String approximateCauseOfLoss;
    private String typeOfCover;
    private String modeOfInformation;
    private String insuredAtFault;
    private String policeStation;
    private String driverName;
    private String accidentDistrict;
    private String policyNo;
    private String periodOfCover;
    private String branch;
    private String marketingExecutive;
    private Double sumInsured;
    private String makeAndModel;
    private String financialInterest;

    public String getIntimationDate() {
        return intimationDate;
    }

    public void setIntimationDate(String intimationDate) {
        this.intimationDate = intimationDate;
    }

    public String getDateOfLoss() {
        return dateOfLoss;
    }

    public void setDateOfLoss(String dateOfLoss) {
        this.dateOfLoss = dateOfLoss;
    }

    public String getVehicleNo() {
        return vehicleNo;
    }

    public void setVehicleNo(String vehicleNo) {
        this.vehicleNo = vehicleNo;
    }

    public String getClaimNo() {
        return claimNo;
    }

    public void setClaimNo(String claimNo) {
        this.claimNo = claimNo;
    }

    public String getInsured() {
        return insured;
    }

    public void setInsured(String insured) {
        this.insured = insured;
    }

    public String getApproximateCauseOfLoss() {
        return approximateCauseOfLoss;
    }

    public void setApproximateCauseOfLoss(String approximateCauseOfLoss) {
        this.approximateCauseOfLoss = approximateCauseOfLoss;
    }

    public String getTypeOfCover() {
        return typeOfCover;
    }

    public void setTypeOfCover(String typeOfCover) {
        this.typeOfCover = typeOfCover;
    }

    public String getModeOfInformation() {
        return modeOfInformation;
    }

    public void setModeOfInformation(String modeOfInformation) {
        this.modeOfInformation = modeOfInformation;
    }

    public String getInsuredAtFault() {
        return insuredAtFault;
    }

    public void setInsuredAtFault(String insuredAtFault) {
        this.insuredAtFault = insuredAtFault;
    }

    public String getPoliceStation() {
        return policeStation;
    }

    public void setPoliceStation(String policeStation) {
        this.policeStation = policeStation;
    }

    public String getDriverName() {
        return driverName;
    }

    public void setDriverName(String driverName) {
        this.driverName = driverName;
    }

    public String getAccidentDistrict() {
        return accidentDistrict;
    }

    public void setAccidentDistrict(String accidentDistrict) {
        this.accidentDistrict = accidentDistrict;
    }

    public String getPolicyNo() {
        return policyNo;
    }

    public void setPolicyNo(String policyNo) {
        this.policyNo = policyNo;
    }

    public String getPeriodOfCover() {
        return periodOfCover;
    }

    public void setPeriodOfCover(String periodOfCover) {
        this.periodOfCover = periodOfCover;
    }

    public String getBranch() {
        return branch;
    }

    public void setBranch(String branch) {
        this.branch = branch;
    }

    public String getMarketingExecutive() {
        return marketingExecutive;
    }

    public void setMarketingExecutive(String marketingExecutive) {
        this.marketingExecutive = marketingExecutive;
    }

    public Double getSumInsured() {
        return sumInsured;
    }

    public void setSumInsured(Double sumInsured) {
        this.sumInsured = sumInsured;
    }

    public String getMakeAndModel() {
        return makeAndModel;
    }

    public void setMakeAndModel(String makeAndModel) {
        this.makeAndModel = makeAndModel;
    }

    public String getFinancialInterest() {
        return financialInterest;
    }

    public void setFinancialInterest(String financialInterest) {
        this.financialInterest = financialInterest;
    }
}
