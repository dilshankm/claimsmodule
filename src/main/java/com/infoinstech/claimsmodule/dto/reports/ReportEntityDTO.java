package com.infoinstech.claimsmodule.dto.reports;

import com.infoinstech.claimsmodule.domain.model.common.Logo;

import java.io.InputStream;

public class ReportEntityDTO {

    private Logo logo;

    private java.io.InputStream image;

    public InputStream getImage() {
        return image;
    }

    public void setImage(InputStream image) {
        this.image = image;
    }

    private String user;

    public Logo getLogo() {
        return logo;
    }

    public void setLogo(Logo logo) {
        this.logo = logo;
    }

    public String getUser() {
        return user;
    }

    public void setUser(String user) {
        this.user = user;
    }
}
