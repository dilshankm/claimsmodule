package com.infoinstech.claimsmodule.dto.reports;

import com.infoinstech.claimsmodule.dto.common.Response;
import org.springframework.core.io.ByteArrayResource;

/**
 * Created by dushman on 4/9/18.
 */

public class ReportGenerateServiceDTO extends Response {

    private ByteArrayResource resource;
}
