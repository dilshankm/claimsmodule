package com.infoinstech.claimsmodule.dto.reports.claimRatioReport;

import com.infoinstech.claimsmodule.dto.reports.ReportEntityDTO;

import java.util.Date;

public class ClaimRatioDTO extends ReportEntityDTO {
    private String toDate;
    private String fromDate;
    private String cls;
    private String product;
    private String intermediaryType;
    private String intermediaryName;
    private String reportType;

    public String getToDate() {
        return toDate;
    }

    public void setToDate(String toDate) {
        this.toDate = toDate;
    }

    public String getFromDate() {
        return fromDate;
    }

    public void setFromDate(String fromDate) {
        this.fromDate = fromDate;
    }

    public String getCls() {
        return cls;
    }

    public void setCls(String cls) {
        this.cls = cls;
    }

    public String getProduct() {
        return product;
    }

    public void setProduct(String product) {
        this.product = product;
    }

    public String getIntermediaryType() {
        return intermediaryType;
    }

    public void setIntermediaryType(String intermediaryType) {
        this.intermediaryType = intermediaryType;
    }

    public String getIntermediaryName() {
        return intermediaryName;
    }

    public void setIntermediaryName(String intermediaryName) {
        this.intermediaryName = intermediaryName;
    }

    public String getReportType() {
        return reportType;
    }

    public void setReportType(String reportType) {
        this.reportType = reportType;
    }
}
