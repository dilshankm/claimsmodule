package com.infoinstech.claimsmodule.dto.reports.claimRatioReport;

import java.math.BigDecimal;

public class ClaimRatioReportDTO{

    private String branch;

    private String classType;

    private String product;

    private String interMediaryCode;

    private String interMediaryName;

    private String businessChannel;

    private BigDecimal basicPremium;

    private BigDecimal srcc;

    private BigDecimal tc;

    private BigDecimal vat;

    private BigDecimal claimPaidAmount;

    private BigDecimal claimRatio;

    public String getBranch() {
        return branch;
    }

    public void setBranch(String branch) {
        this.branch = branch;
    }

    public String getClassType() {
        return classType;
    }

    public void setClassType(String classType) {
        this.classType = classType;
    }

    public String getProduct() {
        return product;
    }

    public void setProduct(String product) {
        this.product = product;
    }

    public String getInterMediaryCode() {
        return interMediaryCode;
    }

    public void setInterMediaryCode(String interMediaryCode) {
        this.interMediaryCode = interMediaryCode;
    }

    public String getInterMediaryName() {
        return interMediaryName;
    }

    public void setInterMediaryName(String interMediaryName) {
        this.interMediaryName = interMediaryName;
    }

    public String getBusinessChannel() {
        return businessChannel;
    }

    public void setBusinessChannel(String businessChannel) {
        this.businessChannel = businessChannel;
    }

    public BigDecimal getBasicPremium() {
        return basicPremium;
    }

    public void setBasicPremium(BigDecimal basicPremium) {
        this.basicPremium = basicPremium;
    }

    public BigDecimal getSrcc() {
        return srcc;
    }

    public void setSrcc(BigDecimal srcc) {
        this.srcc = srcc;
    }

    public BigDecimal getTc() {
        return tc;
    }

    public void setTc(BigDecimal tc) {
        this.tc = tc;
    }

    public BigDecimal getVat() {
        return vat;
    }

    public void setVat(BigDecimal vat) {
        this.vat = vat;
    }

    public BigDecimal getClaimPaidAmount() {
        return claimPaidAmount;
    }

    public void setClaimPaidAmount(BigDecimal claimPaidAmount) {
        this.claimPaidAmount = claimPaidAmount;
    }

    public BigDecimal getClaimRatio() {
        return claimRatio;
    }

    public void setClaimRatio(BigDecimal claimRatio) {
        this.claimRatio = claimRatio;
    }
}
