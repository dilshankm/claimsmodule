package com.infoinstech.claimsmodule.dto.reports.claimRatioReport;

import com.infoinstech.claimsmodule.dto.reports.ReportEntityDTO;

import java.util.List;

public class ClaimRatioReportListDTO extends ReportEntityDTO {

    private List<ClaimRatioReportDTO> claimRatioReportDTOList;


    public List<ClaimRatioReportDTO> getClaimRatioReportDTOList() {
        return claimRatioReportDTOList;
    }

    public void setClaimRatioReportDTOList(List<ClaimRatioReportDTO> claimRatioReportDTOList) {
        this.claimRatioReportDTOList = claimRatioReportDTOList;
    }
}
