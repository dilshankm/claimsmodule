package com.infoinstech.claimsmodule.dto.reports.claimregistrationreport;

public class ClaimRegistrationReportDTO {

    private String branch;
    private String status;
    private String intSequence;
    private String currency;
    private String policyNo;
    private String periodFrom;
    private String periodTo;
    private String customer;
    private String cls;
    private String product;
    private Double sumInsured;
    private String claimNo;
    private String lossDate;
    private String intimationDate;
    //    private ArrayList<PayDetails> payDetailsList;
    private String comment;
    private String policyBranch;
    private Double estAmount;
    private String causeOfLoss;

    private int chequeId;
    private String chequeNo;
    private String payee;
    private Double paidAmount;
    private String paidDate;


    public String getBranch() {
        return branch;
    }

    public void setBranch(String branch) {
        this.branch = branch;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getCurrency() {
        return currency;
    }

    public void setCurrency(String currency) {
        this.currency = currency;
    }

    public String getPolicyNo() {
        return policyNo;
    }

    public void setPolicyNo(String policyNo) {
        this.policyNo = policyNo;
    }

    public String getPeriodFrom() {
        return periodFrom;
    }

    public void setPeriodFrom(String periodFrom) {
        this.periodFrom = periodFrom;
    }

    public String getPeriodTo() {
        return periodTo;
    }

    public void setPeriodTo(String periodTo) {
        this.periodTo = periodTo;
    }

    public String getCustomer() {
        return customer;
    }

    public void setCustomer(String customer) {
        this.customer = customer;
    }

    public String getCls() {
        return cls;
    }

    public void setCls(String cls) {
        this.cls = cls;
    }

    public String getProduct() {
        return product;
    }

    public void setProduct(String product) {
        this.product = product;
    }

    public Double getSumInsured() {
        return sumInsured;
    }

    public void setSumInsured(Double sumInsured) {
        this.sumInsured = sumInsured;
    }

    public String getClaimNo() {
        return claimNo;
    }

    public void setClaimNo(String claimNo) {
        this.claimNo = claimNo;
    }

    public String getLossDate() {
        return lossDate;
    }

    public void setLossDate(String lossDate) {
        this.lossDate = lossDate;
    }

    public String getIntimationDate() {
        return intimationDate;
    }

    public void setIntimationDate(String intimationDate) {
        this.intimationDate = intimationDate;
    }

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }

    public String getPolicyBranch() {
        return policyBranch;
    }

    public void setPolicyBranch(String policyBranch) {
        this.policyBranch = policyBranch;
    }

    public Double getEstAmount() {
        return estAmount;
    }

    public void setEstAmount(Double estAmount) {
        this.estAmount = estAmount;
    }

    public int getChequeId() {
        return chequeId;
    }

    public void setChequeId(int chequeId) {
        this.chequeId = chequeId;
    }

    public String getChequeNo() {
        return chequeNo;
    }

    public void setChequeNo(String chequeNo) {
        this.chequeNo = chequeNo;
    }

    public String getPayee() {
        return payee;
    }

    public void setPayee(String payee) {
        this.payee = payee;
    }

    public Double getPaidAmount() {
        return paidAmount;
    }

    public void setPaidAmount(Double paidAmount) {
        this.paidAmount = paidAmount;
    }

    public String getPaidDate() {
        return paidDate;
    }

    public void setPaidDate(String paidDate) {
        this.paidDate = paidDate;
    }

    public String getIntSequence() {
        return intSequence;
    }

    public void setIntSequence(String intSequence) {
        this.intSequence = intSequence;
    }

    public String getCauseOfLoss() {
        return causeOfLoss;
    }

    public void setCauseOfLoss(String causeOfLoss) {
        this.causeOfLoss = causeOfLoss;
    }
}
