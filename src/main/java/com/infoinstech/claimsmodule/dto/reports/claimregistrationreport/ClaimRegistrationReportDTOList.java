package com.infoinstech.claimsmodule.dto.reports.claimregistrationreport;

import com.infoinstech.claimsmodule.dto.reports.ReportEntityDTO;

import java.util.ArrayList;

/**
 * Created by dushman on 3/30/18.
 */
public class ClaimRegistrationReportDTOList extends ReportEntityDTO {

    private ArrayList<ClaimRegistrationReportDTO> claimRegistrationReportDTOArrayList;
    private String fromDate;
    private String toDate;
    private String user;


    public ArrayList<ClaimRegistrationReportDTO> getClaimRegistrationReportDTOArrayList() {
        return claimRegistrationReportDTOArrayList;
    }

    public void setClaimRegistrationReportDTOArrayList(ArrayList<ClaimRegistrationReportDTO> claimRegistrationReportDTOArrayList) {
        this.claimRegistrationReportDTOArrayList = claimRegistrationReportDTOArrayList;
    }

    public String getFromDate() {
        return fromDate;
    }

    public void setFromDate(String fromDate) {
        this.fromDate = fromDate;
    }

    public String getToDate() {
        return toDate;
    }

    public void setToDate(String toDate) {
        this.toDate = toDate;
    }

    public String getUser() {
        return user;
    }

    public void setUser(String user) {
        this.user = user;
    }
}
