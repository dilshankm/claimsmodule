package com.infoinstech.claimsmodule.dto.reports.claimregistrationsheet;

public class ClaimHistory {

    private String claimNo;
    private String dateOfAccident;
    private Double paidAmount;

    public String getClaimNo() {
        return claimNo;
    }

    public void setClaimNo(String claimNo) {
        this.claimNo = claimNo;
    }

    public String getDateOfAccident() {
        return dateOfAccident;
    }

    public void setDateOfAccident(String dateOfAccident) {
        this.dateOfAccident = dateOfAccident;
    }

    public Double getPaidAmount() {
        return paidAmount;
    }

    public void setPaidAmount(Double paidAmount) {
        this.paidAmount = paidAmount;
    }
}
