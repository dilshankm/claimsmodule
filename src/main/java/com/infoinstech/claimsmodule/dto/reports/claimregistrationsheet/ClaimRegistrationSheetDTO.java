package com.infoinstech.claimsmodule.dto.reports.claimregistrationsheet;

import com.infoinstech.claimsmodule.dto.reports.ReportEntityDTO;

import java.util.List;

public class ClaimRegistrationSheetDTO extends ReportEntityDTO {

    private String vehicleNo;
    private String nameOfInsured;
    private String financialInterest;
    private String claimNo;
    private String policyNo;
    private Double sumInsured;
    private ParticularsOfAccident particularsOfAccident;
    private List<ParticularsOfPremium> particularsOfPremium;
    private ParticularsOfPolicy particularsOfPolicy;
    private ParticularsOfIntimation particularsOfIntimation;
    private List<ClaimHistory> claimHistory;

    public String getVehicleNo() {
        return vehicleNo;
    }

    public void setVehicleNo(String vehicleNo) {
        this.vehicleNo = vehicleNo;
    }

    public String getNameOfInsured() {
        return nameOfInsured;
    }

    public void setNameOfInsured(String nameOfInsured) {
        this.nameOfInsured = nameOfInsured;
    }

    public String getFinancialInterest() {
        return financialInterest;
    }

    public void setFinancialInterest(String financialInterest) {
        this.financialInterest = financialInterest;
    }

    public String getClaimNo() {
        return claimNo;
    }

    public void setClaimNo(String claimNo) {
        this.claimNo = claimNo;
    }

    public String getPolicyNo() {
        return policyNo;
    }

    public void setPolicyNo(String policyNo) {
        this.policyNo = policyNo;
    }

    public Double getSumInsured() {
        return sumInsured;
    }

    public void setSumInsured(Double sumInsured) {
        this.sumInsured = sumInsured;
    }

    public ParticularsOfAccident getParticularsOfAccident() {
        return particularsOfAccident;
    }

    public void setParticularsOfAccident(ParticularsOfAccident particularsOfAccident) {
        this.particularsOfAccident = particularsOfAccident;
    }

    public List<ParticularsOfPremium> getParticularsOfPremium() {
        return particularsOfPremium;
    }

    public void setParticularsOfPremium(List<ParticularsOfPremium> particularsOfPremium) {
        this.particularsOfPremium = particularsOfPremium;
    }

    public ParticularsOfPolicy getParticularsOfPolicy() {
        return particularsOfPolicy;
    }

    public void setParticularsOfPolicy(ParticularsOfPolicy particularsOfPolicy) {
        this.particularsOfPolicy = particularsOfPolicy;
    }

    public ParticularsOfIntimation getParticularsOfIntimation() {
        return particularsOfIntimation;
    }

    public void setParticularsOfIntimation(ParticularsOfIntimation particularsOfIntimation) {
        this.particularsOfIntimation = particularsOfIntimation;
    }

    public List<ClaimHistory> getClaimHistory() {
        return claimHistory;
    }

    public void setClaimHistory(List<ClaimHistory> claimHistory) {
        this.claimHistory = claimHistory;
    }
}
