package com.infoinstech.claimsmodule.dto.reports.claimregistrationsheet;

public class ParticularsOfAccident {

    private String driversName;
    private String driverLicenseNo;
    private String dateOfIntimation;
    private String dateOfAccident;
    private String timeOfAccident;
    private String placeOfAccident;

    public String getDriversName() {
        return driversName;
    }

    public void setDriversName(String driversName) {
        this.driversName = driversName;
    }

    public String getDriverLicenseNo() {
        return driverLicenseNo;
    }

    public void setDriverLicenseNo(String driverLicenseNo) {
        this.driverLicenseNo = driverLicenseNo;
    }

    public String getDateOfIntimation() {
        return dateOfIntimation;
    }

    public void setDateOfIntimation(String dateOfIntimation) {
        this.dateOfIntimation = dateOfIntimation;
    }

    public String getDateOfAccident() {
        return dateOfAccident;
    }

    public void setDateOfAccident(String dateOfAccident) {
        this.dateOfAccident = dateOfAccident;
    }

    public String getTimeOfAccident() {
        return timeOfAccident;
    }

    public void setTimeOfAccident(String timeOfAccident) {
        this.timeOfAccident = timeOfAccident;
    }

    public String getPlaceOfAccident() {
        return placeOfAccident;
    }

    public void setPlaceOfAccident(String placeOfAccident) {
        this.placeOfAccident = placeOfAccident;
    }
}
