package com.infoinstech.claimsmodule.dto.reports.claimregistrationsheet;

public class ParticularsOfIntimation {

    private String lossAdjusterName;
    private String driverContactNo;
    private String insuredContactNo;
    private String approximateCostOfLoss;
    private String insuredAtFault;
    private String policeStation;
    private String policeReportNo;
    private String causeOfLoss;
    private String modeOfInformation;
    private String claimDoubtful;
    private String claimType;

    public String getLossAdjusterName() {
        return lossAdjusterName;
    }

    public void setLossAdjusterName(String lossAdjusterName) {
        this.lossAdjusterName = lossAdjusterName;
    }

    public String getDriverContactNo() {
        return driverContactNo;
    }

    public void setDriverContactNo(String driverContactNo) {
        this.driverContactNo = driverContactNo;
    }

    public String getInsuredContactNo() {
        return insuredContactNo;
    }

    public void setInsuredContactNo(String insuredContactNo) {
        this.insuredContactNo = insuredContactNo;
    }

    public String getApproximateCostOfLoss() {
        return approximateCostOfLoss;
    }

    public void setApproximateCostOfLoss(String approximateCostOfLoss) {
        this.approximateCostOfLoss = approximateCostOfLoss;
    }

    public String getInsuredAtFault() {
        return insuredAtFault;
    }

    public void setInsuredAtFault(String insuredAtFault) {
        this.insuredAtFault = insuredAtFault;
    }

    public String getPoliceStation() {
        return policeStation;
    }

    public void setPoliceStation(String policeStation) {
        this.policeStation = policeStation;
    }

    public String getPoliceReportNo() {
        return policeReportNo;
    }

    public void setPoliceReportNo(String policeReportNo) {
        this.policeReportNo = policeReportNo;
    }

    public String getCauseOfLoss() {
        return causeOfLoss;
    }

    public void setCauseOfLoss(String causeOfLoss) {
        this.causeOfLoss = causeOfLoss;
    }

    public String getModeOfInformation() {
        return modeOfInformation;
    }

    public void setModeOfInformation(String modeOfInformation) {
        this.modeOfInformation = modeOfInformation;
    }

    public String getClaimDoubtful() {
        return claimDoubtful;
    }

    public void setClaimDoubtful(String claimDoubtful) {
        this.claimDoubtful = claimDoubtful;
    }

    public String getClaimType() {
        return claimType;
    }

    public void setClaimType(String claimType) {
        this.claimType = claimType;
    }
}
