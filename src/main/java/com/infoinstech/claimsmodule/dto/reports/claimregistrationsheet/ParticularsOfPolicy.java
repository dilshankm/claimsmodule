package com.infoinstech.claimsmodule.dto.reports.claimregistrationsheet;

public class ParticularsOfPolicy {

    private String makeAndodel;
    private String yearOfMake;
    private String dateOfFirstRegistration;
    private String purposeOfUse;
    private String periodOfCover;
    private String typeOfCover;
    private String address;
    private String idNo;
    private String branch;
    private String marketingExecutive;
    private String chassisNo;

    public String getMakeAndodel() {
        return makeAndodel;
    }

    public void setMakeAndodel(String makeAndodel) {
        this.makeAndodel = makeAndodel;
    }

    public String getYearOfMake() {
        return yearOfMake;
    }

    public void setYearOfMake(String yearOfMake) {
        this.yearOfMake = yearOfMake;
    }

    public String getDateOfFirstRegistration() {
        return dateOfFirstRegistration;
    }

    public void setDateOfFirstRegistration(String dateOfFirstRegistration) {
        this.dateOfFirstRegistration = dateOfFirstRegistration;
    }

    public String getPurposeOfUse() {
        return purposeOfUse;
    }

    public void setPurposeOfUse(String purposeOfUse) {
        this.purposeOfUse = purposeOfUse;
    }

    public String getPeriodOfCover() {
        return periodOfCover;
    }

    public void setPeriodOfCover(String periodOfCover) {
        this.periodOfCover = periodOfCover;
    }

    public String getTypeOfCover() {
        return typeOfCover;
    }

    public void setTypeOfCover(String typeOfCover) {
        this.typeOfCover = typeOfCover;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getIdNo() {
        return idNo;
    }

    public void setIdNo(String idNo) {
        this.idNo = idNo;
    }

    public String getBranch() {
        return branch;
    }

    public void setBranch(String branch) {
        this.branch = branch;
    }

    public String getMarketingExecutive() {
        return marketingExecutive;
    }

    public void setMarketingExecutive(String marketingExecutive) {
        this.marketingExecutive = marketingExecutive;
    }

    public String getChassisNo() {
        return chassisNo;
    }

    public void setChassisNo(String chassisNo) {
        this.chassisNo = chassisNo;
    }
}
