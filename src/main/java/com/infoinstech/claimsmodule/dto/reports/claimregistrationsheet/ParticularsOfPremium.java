package com.infoinstech.claimsmodule.dto.reports.claimregistrationsheet;

public class ParticularsOfPremium {

    private Double debitAmount;
    private String debitNo;
    private String dateofPayment;
    private Double outstandingAmount;

    public Double getDebitAmount() {
        return debitAmount;
    }

    public void setDebitAmount(Double debitAmount) {
        this.debitAmount = debitAmount;
    }

    public String getDebitNo() {
        return debitNo;
    }

    public void setDebitNo(String debitNo) {
        this.debitNo = debitNo;
    }

    public String getDateofPayment() {
        return dateofPayment;
    }

    public void setDateofPayment(String dateofPayment) {
        this.dateofPayment = dateofPayment;
    }

    public Double getOutstandingAmount() {
        return outstandingAmount;
    }

    public void setOutstandingAmount(Double outstandingAmount) {
        this.outstandingAmount = outstandingAmount;
    }
}
