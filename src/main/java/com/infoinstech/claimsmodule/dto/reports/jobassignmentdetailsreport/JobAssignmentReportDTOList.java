package com.infoinstech.claimsmodule.dto.reports.jobassignmentdetailsreport;

import com.infoinstech.claimsmodule.dto.reports.ReportEntityDTO;

import java.util.ArrayList;

public class JobAssignmentReportDTOList extends ReportEntityDTO {

    private ArrayList<JobAssignmentReportDTO> jobAssignmentReportDTOArrayList;

    public ArrayList<JobAssignmentReportDTO> getJobAssignmentReportDTOArrayList() {
        return jobAssignmentReportDTOArrayList;
    }

    public void setJobAssignmentReportDTOArrayList(ArrayList<JobAssignmentReportDTO> jobAssignmentReportDTOArrayList) {
        this.jobAssignmentReportDTOArrayList = jobAssignmentReportDTOArrayList;
    }
}
