package com.infoinstech.claimsmodule.dto.reports.jobassignmentdetailsreport;

import com.infoinstech.claimsmodule.dto.reports.ReportEntityDTO;

import java.util.List;

/**
 * Created by dushman on 4/24/18.
 */
public class MainJobAssignment extends ReportEntityDTO {

    List<JobAssignmentReportDTO> jobAssignmentReportDTOList;
    private String fromDate;
    private String toDate;
    private String user;

    public List<JobAssignmentReportDTO> getJobAssignmentReportDTOList() {
        return jobAssignmentReportDTOList;
    }

    public void setJobAssignmentReportDTOList(List<JobAssignmentReportDTO> jobAssignmentReportDTOList) {
        this.jobAssignmentReportDTOList = jobAssignmentReportDTOList;
    }

    public String getFromDate() {
        return fromDate;
    }

    public void setFromDate(String fromDate) {
        this.fromDate = fromDate;
    }

    public String getToDate() {
        return toDate;
    }

    public void setToDate(String toDate) {
        this.toDate = toDate;
    }

    public String getUser() {
        return user;
    }

    public void setUser(String user) {
        this.user = user;
    }
}
