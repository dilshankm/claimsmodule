package com.infoinstech.claimsmodule.dto.reports.paymentDetailReport;

import com.infoinstech.claimsmodule.dto.reports.ReportEntityDTO;

import java.util.ArrayList;

/**
 * Created by dushman on 4/16/18.
 */

public class MainPaymentDTO extends ReportEntityDTO {

    ArrayList<PaymentResponseDTO> paymentResponseDTOArrayList;
    private String fromDate;
    private String toDate;
    private String user;

    public ArrayList<PaymentResponseDTO> getPaymentResponseDTOArrayList() {
        return paymentResponseDTOArrayList;
    }

    public void setPaymentResponseDTOArrayList(ArrayList<PaymentResponseDTO> paymentResponseDTOArrayList) {
        this.paymentResponseDTOArrayList = paymentResponseDTOArrayList;
    }

    public String getFromDate() {
        return fromDate;
    }

    public void setFromDate(String fromDate) {
        this.fromDate = fromDate;
    }

    public String getToDate() {
        return toDate;
    }

    public void setToDate(String toDate) {
        this.toDate = toDate;
    }

    @Override
    public String getUser() {
        return user;
    }

    @Override
    public void setUser(String user) {
        this.user = user;
    }
}
