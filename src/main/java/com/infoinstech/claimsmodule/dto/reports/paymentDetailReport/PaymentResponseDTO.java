package com.infoinstech.claimsmodule.dto.reports.paymentDetailReport;

public class PaymentResponseDTO {

    private String branch;
    private String cls;
    private String product;
    private String currency;

    private String requisitionNo;

    private String cancelledDate;
    private String claimNo;

    private String chequeInFavour ;
    private String intermediary ;
    private Double amount ;

    

    /////
 /*
    private String createdDate;
    private String value;
    private String intimationSequenceNo;
    private String riskName;
    private String intimationStatus;

    public String getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(String createdDate) {
        this.createdDate = createdDate;
    }
    
    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public String getIntimationSequenceNo() {
        return intimationSequenceNo;
    }

    public String getRiskName() {
        return riskName;
    }

    public void setRiskName(String riskName) {
        this.riskName = riskName;
    }

    public String getIntimationStatus() {
        return intimationStatus;
    }

    public void setIntimationStatus(String intimationStatus) {
        this.intimationStatus = intimationStatus;
    }

    public void setIntimationSequenceNo(String intimationSequenceNo) {
        this.intimationSequenceNo = intimationSequenceNo;
    }

*/
    
    public String getCancelledDate() {
        return cancelledDate;
    }

    public void setCancelledDate(String cancelledDate) {
        this.cancelledDate = cancelledDate;
    }

    public String getIntermediary() {
        return intermediary;
    }

    public void setIntermediary(String intermediary) {
        this.intermediary = intermediary;
    }

    public Double getAmount() {
        return amount;
    }

    public void setAmount(Double amount) {
        this.amount = amount;
    }

    public String getChequeInFavour() {
        return chequeInFavour;
    }

    public void setChequeInFavour(String chequeInFavour) {
        this.chequeInFavour = chequeInFavour;
    }

    public String getRequisitionNo() {
        return requisitionNo;
    }

    public void setRequisitionNo(String requisitionNo) {
        this.requisitionNo = requisitionNo;
    }

    public String getClaimNo() {
        return claimNo;
    }

    public void setClaimNo(String claimNo) {
        this.claimNo = claimNo;
    }

    public String getCurrency() {
        return currency;
    }

    public void setCurrency(String currency) {
        this.currency = currency;
    }

    public String getBranch() {
        return branch;
    }

    public void setBranch(String branch) {
        this.branch = branch;
    }

    public String getCls() {
        return cls;
    }

    public void setCls(String cls) {
        this.cls = cls;
    }

    public String getProduct() {
        return product;
    }

    public void setProduct(String product) {
        this.product = product;
    }
}
