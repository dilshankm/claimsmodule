package com.infoinstech.claimsmodule.dto.sketch;

import java.util.List;

public class SketchTypeDto {
    private String type;
    private List<String> model;

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public List<String> getModel() {
        return model;
    }

    public void setModel(List<String> model) {
        this.model = model;
    }
}
