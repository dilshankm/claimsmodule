package com.infoinstech.claimsmodule.exceptions;

/**
 * @author : Dushman Nalin
 * @date : 2020.12.14
 * @version : 1.0
 * @copyright : © 2010-2020 Information International Limited. All Rights Reserved
 */

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.LocalDateTime;

@AllArgsConstructor
@NoArgsConstructor
@Data
public class ErrorDetails {

    int status;
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd hh:mm:ss")
    LocalDateTime timestamp;
    private String error;
    private String error_description;

    public ErrorDetails(String errorCode, String errorMsg) {
        super();
        this.error = errorCode;
        this.error_description = errorMsg;
    }
}