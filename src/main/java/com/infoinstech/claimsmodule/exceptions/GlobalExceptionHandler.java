package com.infoinstech.claimsmodule.exceptions;

import com.infoinstech.claimsmodule.exceptions.types.*;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;

import java.time.LocalDateTime;

@RestControllerAdvice
public class GlobalExceptionHandler {

    @ExceptionHandler(value = ClaimGeneralException.class)
    public ResponseEntity<ErrorDetails> claimGenneralException(ClaimGeneralException e) {
        ErrorDetails error = new ErrorDetails("INTERNAL_SERVER_ERROR", e.getMessage());
        e.printStackTrace();
        error.setTimestamp(LocalDateTime.now());
        error.setStatus((HttpStatus.INTERNAL_SERVER_ERROR.value()));
        return new ResponseEntity<ErrorDetails>(error, HttpStatus.INTERNAL_SERVER_ERROR);
    }

    @ExceptionHandler({Exception.class, DBException.class})
    public ResponseEntity<ErrorDetails> exception(Exception e) {
        ErrorDetails error = new ErrorDetails("INTERNAL_SERVER_ERROR", e.getMessage());
        e.printStackTrace();
        error.setTimestamp(LocalDateTime.now());
        error.setStatus((HttpStatus.INTERNAL_SERVER_ERROR.value()));
        return new ResponseEntity<ErrorDetails>(error, HttpStatus.INTERNAL_SERVER_ERROR);
    }

    @ExceptionHandler(value = ClaimIntimateGeneralException.class)
    public ResponseEntity<ErrorDetails> claimIntimatexception(ClaimIntimateGeneralException e) {
        ErrorDetails error = new ErrorDetails("CLAIM_INTIMATE_EXCEPTION", e.getMessage());
        e.printStackTrace();
        error.setTimestamp(LocalDateTime.now());
        error.setStatus((HttpStatus.NOT_FOUND.value()));
        return new ResponseEntity<ErrorDetails>(error, HttpStatus.NOT_FOUND);
    }

    @ExceptionHandler(value = AssessorException.class)
    public ResponseEntity<ErrorDetails> assessorException(AssessorException e) {
        ErrorDetails error = new ErrorDetails("EXCEPTION", e.getMessage());
        e.printStackTrace();
        error.setTimestamp(LocalDateTime.now());
        error.setStatus((HttpStatus.NOT_FOUND.value()));
        return new ResponseEntity<ErrorDetails>(error, HttpStatus.NOT_FOUND);
    }

    @ExceptionHandler(value = DeviceRegisteredException.class)
    public ResponseEntity<ErrorDetails> deviceRegisteredException(DeviceRegisteredException e) {
        ErrorDetails error = new ErrorDetails("DEVICE_REGISTERED_EXCEPTION", e.getMessage());
        e.printStackTrace();
        error.setTimestamp(LocalDateTime.now());
        error.setStatus((HttpStatus.FOUND.value()));
        return new ResponseEntity<ErrorDetails>(error, HttpStatus.FOUND);
    }

    @ExceptionHandler({NoDeviceRegisteredException.class,
            DeviceTokenNotFoundException.class,
            AssessorChargesListNotAvaliableException.class,
            NotificationNotFoundException.class, NotFoundException.class})
    public ResponseEntity<ErrorDetails> noDeviceRegisteredException(NoDeviceRegisteredException e) {
        ErrorDetails error = new ErrorDetails("NOT_FOUND_EXCEPTION", e.getMessage());
        e.printStackTrace();
        error.setTimestamp(LocalDateTime.now());
        error.setStatus((HttpStatus.NOT_FOUND.value()));
        return new ResponseEntity<ErrorDetails>(error, HttpStatus.NOT_FOUND);
    }


    @ExceptionHandler({ChargeTypesInvalidException.class})
    public ResponseEntity<ErrorDetails> invalidException(ChargeTypesInvalidException e) {
        ErrorDetails error = new ErrorDetails("INVALID_EXCEPTION", e.getMessage());
        e.printStackTrace();
        error.setTimestamp(LocalDateTime.now());
        error.setStatus((HttpStatus.NOT_IMPLEMENTED.value()));
        return new ResponseEntity<ErrorDetails>(error, HttpStatus.NOT_IMPLEMENTED);
    }

    @ExceptionHandler({NotEnoughInfoException.class})
    public ResponseEntity<ErrorDetails> notEnoughInfoException(NotEnoughInfoException e) {
        ErrorDetails error = new ErrorDetails("NOT_ENOUGH_INFO_EXCEPTION", e.getMessage());
        e.printStackTrace();
        error.setTimestamp(LocalDateTime.now());
        error.setStatus((HttpStatus.NO_CONTENT.value()));
        return new ResponseEntity<ErrorDetails>(error, HttpStatus.NO_CONTENT);
    }

    @ExceptionHandler({ClaimNotificationException.class , InvalidInputExceptions.class})
    public ResponseEntity<ErrorDetails> invalidResponse(ClaimNotificationException e) {
        ErrorDetails error = new ErrorDetails("INVALID_RESPONSE_EXCEPTION", e.getMessage());
        e.printStackTrace();
        error.setTimestamp(LocalDateTime.now());
        error.setStatus((HttpStatus.BAD_REQUEST.value()));
        return new ResponseEntity<ErrorDetails>(error, HttpStatus.BAD_REQUEST);
    }


}
