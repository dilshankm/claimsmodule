package com.infoinstech.claimsmodule.exceptions.types;

public class AssessorChargesListNotAvaliableException extends RuntimeException{

    private static final long serialVersionUID = 1L;

    public AssessorChargesListNotAvaliableException(String message) {
        super(message);
    }

}
