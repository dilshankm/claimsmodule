package com.infoinstech.claimsmodule.exceptions.types;

public class AssessorException extends RuntimeException{

    private static final long serialVersionUID = 1L;

    public AssessorException(String message) {
        super(message);
    }

}
