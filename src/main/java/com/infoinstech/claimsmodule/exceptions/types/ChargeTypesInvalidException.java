package com.infoinstech.claimsmodule.exceptions.types;

public class ChargeTypesInvalidException extends RuntimeException{

    private static final long serialVersionUID = 1L;

    public ChargeTypesInvalidException(String message) {
        super(message);
    }

}
