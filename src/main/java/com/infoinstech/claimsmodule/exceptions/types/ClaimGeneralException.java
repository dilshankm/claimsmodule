package com.infoinstech.claimsmodule.exceptions.types;

public class ClaimGeneralException extends RuntimeException{

    private static final long serialVersionUID = 1L;

    public ClaimGeneralException(String message) {
        super(message);
    }

}
