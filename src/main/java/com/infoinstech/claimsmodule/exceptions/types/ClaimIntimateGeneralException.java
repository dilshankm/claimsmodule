package com.infoinstech.claimsmodule.exceptions.types;

public class ClaimIntimateGeneralException extends RuntimeException {

    private static final long serialVersionUID = 1L;

    public ClaimIntimateGeneralException(String message) {
        super(message);
    }

}
