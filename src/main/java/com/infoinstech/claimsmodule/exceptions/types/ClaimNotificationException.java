package com.infoinstech.claimsmodule.exceptions.types;

public class ClaimNotificationException extends RuntimeException{

    private static final long serialVersionUID = 1L;

    public ClaimNotificationException(String message) {
        super(message);
    }

}
