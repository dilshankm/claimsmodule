package com.infoinstech.claimsmodule.exceptions.types;

public class DeviceRegisteredException extends RuntimeException{

    private static final long serialVersionUID = 1L;

    public DeviceRegisteredException(String message) {
        super(message);
    }

}
