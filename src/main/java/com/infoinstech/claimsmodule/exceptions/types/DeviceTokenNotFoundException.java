package com.infoinstech.claimsmodule.exceptions.types;

public class DeviceTokenNotFoundException extends RuntimeException{

    private static final long serialVersionUID = 1L;

    public DeviceTokenNotFoundException(String message) {
        super(message);
    }

}
