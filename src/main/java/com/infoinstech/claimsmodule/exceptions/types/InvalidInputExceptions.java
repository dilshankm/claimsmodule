package com.infoinstech.claimsmodule.exceptions.types;

public class InvalidInputExceptions extends RuntimeException {

    private static final long serialVersionUID = 1L;

    public InvalidInputExceptions(String message) {
        super(message);
    }

}
