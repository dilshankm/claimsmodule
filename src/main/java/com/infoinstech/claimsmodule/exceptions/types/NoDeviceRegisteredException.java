package com.infoinstech.claimsmodule.exceptions.types;

public class NoDeviceRegisteredException extends RuntimeException{

    private static final long serialVersionUID = 1L;

    public NoDeviceRegisteredException(String message) {
        super(message);
    }
}
