package com.infoinstech.claimsmodule.exceptions.types;

public class NotEnoughInfoException extends RuntimeException{

    private static final long serialVersionUID = 1L;

    public NotEnoughInfoException(String message) {
        super(message);
    }

}
