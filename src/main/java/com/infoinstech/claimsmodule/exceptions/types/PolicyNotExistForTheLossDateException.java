package com.infoinstech.claimsmodule.exceptions.types;

public class PolicyNotExistForTheLossDateException extends RuntimeException{

    private static final long serialVersionUID = 1L;

    public PolicyNotExistForTheLossDateException(String message) {
        super(message);
    }
}
