package com.infoinstech.claimsmodule.exceptions.types;

public class PolicyNumberNotFoundException extends RuntimeException{

    private static final long serialVersionUID = 1L;

    public PolicyNumberNotFoundException(String message) {
        super(message);
    }


}
