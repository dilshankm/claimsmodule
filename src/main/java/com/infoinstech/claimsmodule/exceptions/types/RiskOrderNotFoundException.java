package com.infoinstech.claimsmodule.exceptions.types;

public class RiskOrderNotFoundException extends RuntimeException{

    private static final long serialVersionUID = 1L;

    public RiskOrderNotFoundException(String message) {
        super(message);
    }

}
