package com.infoinstech.claimsmodule.exceptions.types;


public class UserCodeNotFoundException extends RuntimeException  {

    private static final long serialVersionUID = 1L;

    public UserCodeNotFoundException(String message) {
        super(message);
    }

}
