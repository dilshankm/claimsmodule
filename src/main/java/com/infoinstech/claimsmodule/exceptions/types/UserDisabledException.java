package com.infoinstech.claimsmodule.exceptions.types;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

/**
 * user defined exception (exception class can be defined base on requirements)
 *
 * @author : Dushman Nalin
 * @version : 1.0
 * @date : 2020.12.14
 * @copyright : © 2010-2020 Information International Limited. All Rights Reserved
 */
@ResponseStatus(value = HttpStatus.LOCKED)
public class UserDisabledException extends Exception {
    private static final long serialVersionUID = 1L;

    public UserDisabledException(String message) {
        super(message);
    }
}

