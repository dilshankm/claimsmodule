package com.infoinstech.claimsmodule.services.api;

import com.infoinstech.claimsmodule.dto.common.Response;
import com.infoinstech.claimsmodule.dto.googlemaps.NearbySearchRequest;
import com.infoinstech.claimsmodule.dto.googlemaps.NearbySearchResponse;
import com.infoinstech.claimsmodule.dto.googlemaps.PlaceAutoCompleteResponse;
import com.infoinstech.claimsmodule.dto.googlemaps.PlaceDetailsResponse;
import com.infoinstech.claimsmodule.services.util.Settings;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

@Service
public class GoogleMapsAPIService {

    @Autowired
    private Environment env;
    @Autowired
    private Settings settings;

    private final String API_KEY = "AIzaSyBQtcGXohM6ZP-jPSVTq9zUMbBnHj-H9dc";


    public Response getPlaceAutoComplete(String input){

        settings.setProxy();

        String countryCode = (settings.getSite().equals("AMI")) ? "mm" : "lk";

        Response response;

        final String googleMapPlacesURL = "https://maps.googleapis.com/maps/api/place/autocomplete/json?";

        String urlWithParameters = googleMapPlacesURL + "input=" + input + "&types=geocode&components=country:" + countryCode + "&key=" + API_KEY;

        RestTemplate restTemplate = new RestTemplate();

        try{

            ResponseEntity<PlaceAutoCompleteResponse> result = restTemplate.getForEntity(urlWithParameters,PlaceAutoCompleteResponse.class);

            response = new Response(200, true, result.getBody());
        }
        catch (Exception e){


            response = new Response(400, false, e.getMessage());
        }


        return response;
    }

    public Response getPlaceDetails(String placeId){

        settings.setProxy();

        Response response;

        final String googleMapPlacesURL = "https://maps.googleapis.com/maps/api/place/details/json?";

        String urlWithParameters = googleMapPlacesURL + "placeid=" + placeId + "&key=" + API_KEY;

        RestTemplate restTemplate = new RestTemplate();

        try {

            ResponseEntity<PlaceDetailsResponse> result = restTemplate.getForEntity(urlWithParameters,PlaceDetailsResponse.class);

            response = new Response(200, true, result.getBody());
        }
        catch (Exception e){

            response = new Response(200, true, e.getMessage());
        }

        return response;
    }

    public Response searchNearbyPoliceStations(NearbySearchRequest nearbySearchRequest){
        settings.setProxy();
        String mapRadius = (settings.getSite().equals("AMI")) ? "40000" : "12000";
        Response response;
        final String googleMapPlacesURL = "https://maps.googleapis.com/maps/api/place/nearbysearch/json?";
        String type = "police";
        String urlWithParameters = googleMapPlacesURL + "location=" + nearbySearchRequest.getLat() + ","
                + nearbySearchRequest.getLng() + "&radius=" + mapRadius + "&type=" + type + "&key=" + API_KEY;
        RestTemplate restTemplate = new RestTemplate();
        ResponseEntity<NearbySearchResponse> result = restTemplate.getForEntity(urlWithParameters,NearbySearchResponse.class);
        response = new Response(200, true,result.getBody());
        return response;
    }


}
