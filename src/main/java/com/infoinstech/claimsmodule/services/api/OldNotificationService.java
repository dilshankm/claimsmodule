package com.infoinstech.claimsmodule.services.api;

import com.infoinstech.claimsmodule.dto.common.Response;
import com.infoinstech.claimsmodule.domain.model.claims.master.ExternalPersonDevice;
import com.infoinstech.claimsmodule.domain.model.claims.transaction.*;
import com.infoinstech.claimsmodule.domain.model.underwriting.master.Customer;
import com.infoinstech.claimsmodule.domain.packages.PK_CL_TAB_SEQUENCE;
import com.infoinstech.claimsmodule.domain.repository.claims.transaction.NotificationLogRepository;
import com.infoinstech.claimsmodule.services.business.CustomerService;
import com.infoinstech.claimsmodule.services.common.ClaimsCommonService;
import com.infoinstech.claimsmodule.services.parameters.ReceiptingParametersService;
import com.infoinstech.claimsmodule.services.util.DateConversion;
import com.infoinstech.claimsmodule.services.util.Formatter;
import com.infoinstech.claimsmodule.services.util.Settings;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.Date;

@Service
public class OldNotificationService {

    @Autowired
    private NotificationLogRepository notificationLogRepository;
    @Autowired
    private CustomerService customerService;
    @Autowired
    private PK_CL_TAB_SEQUENCE pk_cl_tab_sequence;
    @Autowired
    private DateConversion dateConversion;
    @Autowired
    private ClaimsCommonService claimsCommonService;
    @Autowired
    private ReceiptingParametersService receiptingParametersService;
    @Autowired
    private Settings settings;
    @Autowired
    private Formatter formatter;

    // Send Push Notification for Job Assignment based on NewJob, New Claim, Job No and Assessor code
    public Response sendJobNotification(ClaimIntimation claimIntimation, JobAssignment jobAssignment, AssignLossAdjuster assignLossAdjuster,
                                        ExternalPersonDevice externalPersonDevice, Customer customer) {

        Response response = new Response();

        //String serverkey = propertyConfig.getProperty("google.apikey");

        settings.setProxy();

        HttpURLConnection connection = null;

        try {
            String requestURI = "https://fcm.googleapis.com/fcm/send";

            URL url = new URL(requestURI);
            connection = (HttpURLConnection) url.openConnection();

            connection.setRequestMethod("POST");
            connection.setRequestProperty("Content-Type", "application/json");
            connection.setRequestProperty("Authorization", "key=" + settings.getFirebaseServerKey()); // + String.format("Basic %s", encoding));

            connection.setUseCaches(false);
            connection.setDoOutput(true);

            JSONObject obj = new JSONObject();
            obj.put("jobid", jobAssignment.getJobNo());
            obj.put("assessorcode", externalPersonDevice.getExternalPersonCode());
            obj.put("customername", customerService.getCustomerName(customer));
            obj.put("location", claimIntimation.getPlaceOfLoss());
            obj.put("accidenttime", dateConversion.convertDate(claimIntimation.getLossDate()));
            obj.put("status", assignLossAdjuster.getStatus());
            obj.put("policyid", claimIntimation.getPolicyNo());
            obj.put("customermobilenumber", formatter.replaceWithDash(customerService.getCustomerContact(customer).getContact()));
            obj.put("vehicleno", claimIntimation.getRiskName());
            obj.put("actiontaken", claimIntimation.getActionTaken());
            obj.put("latitude", claimIntimation.getLatitude());
            obj.put("longitude", claimIntimation.getLongitude());


            JSONObject parent = new JSONObject();

            parent.put("to", externalPersonDevice.getGcmKey());
            parent.put("body", "body string here");
            parent.put("data", obj);


            long requestTime = System.currentTimeMillis();
            OutputStreamWriter wr = new OutputStreamWriter(
                    connection.getOutputStream());
            wr.write(parent.toString());
            wr.close();

            int responseCode = connection.getResponseCode();

            System.out.println("response code " + responseCode);

            BufferedReader in = new BufferedReader(
                    new InputStreamReader(connection.getInputStream()));
            String inputLine;
            StringBuffer buffer = new StringBuffer();

            while ((inputLine = in.readLine()) != null) {
                buffer.append(inputLine);
            }
            in.close();
            long responseRecievedTime = System.currentTimeMillis();

            String responseTime = String.valueOf((responseRecievedTime - requestTime) / 1000);

            NotificationLog notificationLog = new NotificationLog();
            notificationLog.setSequenceNo(claimsCommonService.generateSequence(NotificationLog.SEQUENCE, receiptingParametersService.getBaseAccountBranch()));
            notificationLog.setJobSeqNo(jobAssignment.getJobAssignmentPK().getSequenceNo());
            notificationLog.setJobNo(jobAssignment.getJobNo());
            notificationLog.setClaimSequenceNo(claimIntimation.getSequenceNo());
            notificationLog.setClaimNo(claimIntimation.getClaimNo());
            notificationLog.setExternalPersonCode(externalPersonDevice.getExternalPersonCode());
            notificationLog.setDeviceSeqNo(externalPersonDevice.getSequenceNo());
            notificationLog.setDeviceToken(externalPersonDevice.getDeviceToken());
            notificationLog.setApiKey(externalPersonDevice.getGcmKey());
            notificationLog.setResponseCode(String.valueOf(responseCode));
            notificationLog.setResponseMessage(buffer.toString());
            notificationLog.setResponseTime(responseTime);
            notificationLog.setCreatedBy(claimIntimation.getCreatedBy());
            notificationLog.setCreatedDate(new Date());
            notificationLog.setModifiedBy(claimIntimation.getModifiedBy());
            notificationLog.setModifiedDate(new Date());

            notificationLogRepository.save(notificationLog);
            ;

            System.out.println("response " + response.toString());

            System.out.println("Http POST request sent!");

        } catch (
                Exception ex) { //InterruptedException

            ex.printStackTrace();

            response = new Response(400, false, ex.getMessage());
        }
        return response;
    }

    // Send Push Notification for Job Assignment based on NewJob, New Claim, Job No and Assessor code
    public Response sendAssignmentNotification(ClaimNotification claimNotification, NotificationAssignment notificationAssignment, NotificationLossAdjuster notificationLossAdjuster,
                                               ExternalPersonDevice externalPersonDevice) {


        Response response = new Response();

        String serverkey = "AIzaSyCt9pbxtBvzYbaVSadEjSZiaJNmbRnlW1I";

        //String serverkey = propertyConfig.getProperty("google.apikey");

        String proxy = "";
        String port = "";

//        System.setProperty("https.proxyHost", "172.20.100.100");
//        System.setProperty("https.proxyPort", "3128");

//        System.setProperty("https.proxyHost", "192.168.15.5");
//        System.setProperty("https.proxyPort", "8080");

        HttpURLConnection connection = null;

        try {
            String requestURI = "https://fcm.googleapis.com/fcm/send";

            URL url = new URL(requestURI);
            connection = (HttpURLConnection) url.openConnection();

            connection.setRequestMethod("POST");
            connection.setRequestProperty("Content-Type", "application/json");
            connection.setRequestProperty("Authorization", "key=" + serverkey); // + String.format("Basic %s", encoding));

            connection.setUseCaches(false);
            connection.setDoOutput(true);

            JSONObject obj = new JSONObject();
            obj.put("jobid", notificationAssignment.getJobNo());
            obj.put("assessorcode", externalPersonDevice.getExternalPersonCode());
            obj.put("customername", "COVER NOTE");
            obj.put("location", claimNotification.getPlaceOfLoss());
            obj.put("accidenttime", dateConversion.convertDate(claimNotification.getLossDate()));
            obj.put("status", notificationLossAdjuster.getStatus());
            obj.put("policyid", "-");
            obj.put("customermobilenumber", "-");
            obj.put("vehicleno", claimNotification.getRiskName());
            obj.put("actiontaken", claimNotification.getActionTaken());
            obj.put("latitude", claimNotification.getLatitude());
            obj.put("longitude", claimNotification.getLongitude());


            JSONObject parent = new JSONObject();

            parent.put("to", externalPersonDevice.getGcmKey());
            parent.put("body", "body string here");
            parent.put("data", obj);


            long requestTime = System.currentTimeMillis();
            OutputStreamWriter wr = new OutputStreamWriter(
                    connection.getOutputStream());
            wr.write(parent.toString());
            wr.close();

            int responseCode = connection.getResponseCode();

            System.out.println("response code " + responseCode);

            BufferedReader in = new BufferedReader(
                    new InputStreamReader(connection.getInputStream()));
            String inputLine;
            StringBuffer buffer = new StringBuffer();

            while ((inputLine = in.readLine()) != null) {
                buffer.append(inputLine);
            }
            in.close();
            long responseRecievedTime = System.currentTimeMillis();

            String responseTime = String.valueOf((responseRecievedTime - requestTime) / 1000);


            NotificationLog notificationLog = new NotificationLog();
            notificationLog.setSequenceNo(claimsCommonService.generateSequence(NotificationLog.SEQUENCE, receiptingParametersService.getBaseAccountBranch()));
            notificationLog.setAssignmentSeqNo(notificationAssignment.getNotificationAssignmentPK().getSequenceNo());
            notificationLog.setAssignmentNo(notificationAssignment.getJobNo());
            notificationLog.setNotificationSequenceNo(claimNotification.getSequenceNo());
            notificationLog.setNotificationNo(claimNotification.getNotificationNo());
            notificationLog.setExternalPersonCode(externalPersonDevice.getExternalPersonCode());
            notificationLog.setDeviceSeqNo(externalPersonDevice.getSequenceNo());
            notificationLog.setDeviceToken(externalPersonDevice.getDeviceToken());
            notificationLog.setApiKey(externalPersonDevice.getGcmKey());
            notificationLog.setResponseCode(String.valueOf(responseCode));
            notificationLog.setResponseMessage(buffer.toString());
            notificationLog.setResponseTime(responseTime);
            notificationLog.setCreatedBy(claimNotification.getCreatedBy());
            notificationLog.setCreatedDate(new Date());
            notificationLog.setModifiedBy(claimNotification.getModifiedBy());
            notificationLog.setModifiedDate(new Date());

            notificationLogRepository.save(notificationLog);
            ;

            System.out.println("response " + response.toString());

            System.out.println("Http POST request sent!");

        } catch (
                Exception ex) { //InterruptedException

            ex.printStackTrace();

            response = new Response(400, false, ex.getMessage());
        }
        return response;
    }

}
