package com.infoinstech.claimsmodule.services.business;

import com.infoinstech.claimsmodule.domain.model.common.LoginAttempt;
import com.infoinstech.claimsmodule.domain.model.salesmarketing.SystemUser;
import com.infoinstech.claimsmodule.domain.repository.common.LoginAttemptRepository;
import com.infoinstech.claimsmodule.domain.repository.salesmarketing.SystemUserRepository;
import com.infoinstech.claimsmodule.dto.admin.UpdateUserStatusDTO;
import com.infoinstech.claimsmodule.dto.common.Response;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class AdminService {

		@Autowired
		private SystemUserRepository systemUserRepository;
		@Autowired
		private LoginAttemptRepository loginAttemptRepository;

		public List<SystemUser> fetchSystemUsers(){
			return systemUserRepository.findByIsActiveAndSystemUser('Y', 'Y');
		}

	public String updateUserStatus(UpdateUserStatusDTO updateUserStatusDTO) {
		LoginAttempt loginAttempt = loginAttemptRepository.findByCode(updateUserStatusDTO.getCode());
		if (updateUserStatusDTO.getIsBlocked()) {
			loginAttempt.setActive("N");
			loginAttempt.setFailAttempts(3);
		} else {
			loginAttempt.setActive("Y");
			loginAttempt.setFailAttempts(0);
		}
		loginAttemptRepository.save(loginAttempt);
		return loginAttempt.getActive();
	}

}
