package com.infoinstech.claimsmodule.services.business;

import com.infoinstech.claimsmodule.dto.authorization.SessionDTO;
import com.infoinstech.claimsmodule.dto.common.Response;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

@Service
public class AuthorizationService {

    public Response validateUserSession(String sessionToken){

        final String URI = "http://localhost:8205/userauthentication/api/sessioncheck";

        SessionDTO sessionDTO = new SessionDTO();
        sessionDTO.setSessionToken(sessionToken);

        RestTemplate restTemplate = new RestTemplate();

        ResponseEntity<Response> responseEntity = restTemplate.postForEntity(URI, sessionDTO, Response.class);

        return responseEntity.getBody();

    }
}
