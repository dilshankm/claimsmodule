package com.infoinstech.claimsmodule.services.business;

import com.infoinstech.claimsmodule.dto.claiminquiry.*;
import com.infoinstech.claimsmodule.dto.claimintimation.CustomerContactDTO;
import com.infoinstech.claimsmodule.dto.common.Response;
import com.infoinstech.claimsmodule.domain.model.claims.master.ExternalPerson;
import com.infoinstech.claimsmodule.domain.model.claims.reference.RefLossCause;
import com.infoinstech.claimsmodule.domain.model.claims.transaction.*;
import com.infoinstech.claimsmodule.domain.model.underwriting.master.Customer;
import com.infoinstech.claimsmodule.domain.model.underwriting.temporary.TempPolicy;
import com.infoinstech.claimsmodule.domain.model.underwriting.temporary.TempPolicyRisk;
import com.infoinstech.claimsmodule.domain.model.underwriting.transaction.PolicyRisk;
import com.infoinstech.claimsmodule.domain.repository.claims.master.ExternalPersonRepository;
import com.infoinstech.claimsmodule.domain.repository.claims.reference.RefLossCauseRepository;
import com.infoinstech.claimsmodule.domain.repository.claims.transaction.*;
import com.infoinstech.claimsmodule.domain.repository.underwriting.master.CustomerRepository;
import com.infoinstech.claimsmodule.domain.repository.underwriting.temporary.TempPolicyRiskRepository;
import com.infoinstech.claimsmodule.domain.repository.underwriting.transaction.PolicyRepository;
import com.infoinstech.claimsmodule.domain.repository.underwriting.transaction.PolicyRiskRepository;
import com.infoinstech.claimsmodule.exceptions.types.DBException;
import com.infoinstech.claimsmodule.services.reference.ReferenceService;
import com.infoinstech.claimsmodule.services.util.DateConversion;
import com.infoinstech.claimsmodule.services.util.Formatter;
import oracle.jdbc.OracleTypes;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.sql.DataSource;
import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.ResultSet;
import java.util.*;
import java.util.stream.Collectors;

@Service
public class ClaimInquiryService {

    //Repositories
    @Autowired
    ClaimIntimationRepository claimIntimationRepository;
    @Autowired
    RefLossCauseRepository refLossCauseRepository;
    @Autowired
    private CustomerRepository customerRepository;
    @Autowired
    private PolicyRepository policyRepository;
    @Autowired
    private JobAssignmentRepository jobAssignmentRepository;
    @Autowired
    private ExternalPersonRepository externalPersonRepository;
    @Autowired
    private AssignLossAdjusterRepository assignLossAdjusterRepository;
    @Autowired
    private PolicyRiskRepository policyRiskRepository;
    @Autowired
    private ClaimNotificationRepository claimNotificationRepository;

    //Services
    @Autowired
    private CustomerService customerService;
    @Autowired
    private ReferenceService referenceService;
    @Autowired
    private ExternalPersonService externalPersonService;
    @Autowired
    private ImageService imageService;
    @Autowired
    private PolicyRiskInformationService policyRiskInformationService;
    @Autowired
    private QuestionnaireService questionnaireService;
    @Autowired
    private WrapperService wrapperService;
    //Utilities
    @Autowired
    private DateConversion dateConversion;
    @Autowired
    private Formatter formatter;
    @Autowired
    private DamageAreaRepository damageAreaRepository;
    @Autowired
    private TempPolicyRiskRepository tempPolicyRiskRepository;

    @Autowired
    private DataSource dataSource;

    @PersistenceContext
    private EntityManager entityManager;


    // Get Claim details based on Claim Sequence No
    public ClaimInquiryDTO getClaimInquiryDetails(String claimSequenceNo) {

        ClaimIntimation claimIntimation = claimIntimationRepository.findBySequenceNo(claimSequenceNo);
        ClaimInquiryDTO claimInquiryDTO = null;

        if (claimIntimation != null) {

            claimInquiryDTO = new ClaimInquiryDTO();
            claimInquiryDTO.setCustomerDetails(getCustomerInformation(claimIntimation));
            claimInquiryDTO.setPolicyDetails(getPolicyInformation(claimIntimation));
            claimInquiryDTO.setAccidentDetails(getAccidentDetails(claimIntimation));
            claimInquiryDTO.setIntimationDetails(getIntimationDetails(claimIntimation));
            claimInquiryDTO.setDriverDetails(getDriverDetails(claimIntimation));
        }

        return claimInquiryDTO;
    }


    // Get Customer details for Claim Seq No
    private CustomerDetails getCustomerInformation(ClaimIntimation claimIntimation) {

        Customer customer = customerRepository.findByCustomerCode(claimIntimation.getCustomerCode());

        CustomerDetails customerDetails = new CustomerDetails();
        customerDetails.setClaimNo(claimIntimation.getClaimNo());
        customerDetails.setInsuredName(customerService.getCustomerName(customer));
        customerDetails.setInsuredNIC(customerService.getCustomerNIC(customer));
        CustomerContactDTO customerContactDTO = customerService.getCustomerContact(customer);
        customerDetails.setContactNo(customerContactDTO.getContact());

        return customerDetails;
    }

    // Get Policy Information for Claim Seq No
    private PolicyDetails getPolicyInformation(ClaimIntimation claimIntimation) {

        TempPolicy tempPolicy = wrapperService.buildPolicy(claimIntimation.getPolicyNo(), claimIntimation.getLossDate());

        TempPolicyRisk tempPolicyRisk = tempPolicyRiskRepository.findByTempPolicyRiskPK_SequenceNo(claimIntimation.getRiskSequenceNo());

        PolicyDetails policyDetails = new PolicyDetails();

        policyDetails.setPolicyNo(tempPolicy.getPolicyNo());
        policyDetails.setDateFrom(dateConversion.convertFromDateToString(tempPolicy.getPeriodFrom()));
        policyDetails.setDateTo(dateConversion.convertFromDateToString(tempPolicy.getPeriodTo()));
        policyDetails.setVehicleNo(claimIntimation.getRiskName());
        policyDetails.setMakeAndModel(policyRiskInformationService.getMakeAndModel(tempPolicyRisk));
        policyDetails.setVehicleCategory(policyRiskInformationService.getVehicleCategory(tempPolicyRisk));
        policyDetails.setYearOfManufucture(policyRiskInformationService.getYearOfManufacture(tempPolicyRisk));

        return policyDetails;
    }

    // Get Accident details for Claim Seq No
    private AccidentDetails getAccidentDetails(ClaimIntimation claimIntimation) {

        AccidentDetails accidentDetails = new AccidentDetails();
        accidentDetails.setPlaceOfAccident(formatter.replaceWithDash(claimIntimation.getPlaceOfLoss()));
        accidentDetails.setLossRemarks(formatter.replaceWithDash(claimIntimation.getLossRemarks()));
        accidentDetails.setDateOfAccident(dateConversion.convertFromDateToString(claimIntimation.getLossDate()));
        accidentDetails.setNearestTown(formatter.replaceWithDash(claimIntimation.getPlaceOfLoss()));
        accidentDetails.setThirdPartyDetails(formatter.replaceWithDash(claimIntimation.getThirdPartyDetails()));
        accidentDetails.setLatitude(formatter.replaceWithDash(claimIntimation.getLatitude()));
        accidentDetails.setLongitude(formatter.replaceWithDash(claimIntimation.getLongitude()));
        accidentDetails.setDamageImages(getDamageAreas(claimIntimation.getClaimNo()));
        return accidentDetails;
    }

    // Get Damage sketch detail for Claim No
    public List<byte[]> getDamageAreas(String claimNo) {

        List<DamageArea> damageAreas;

        if (claimNo.substring(0, 2).equals("CL")) {

            damageAreas = damageAreaRepository.findAllByClaimNo(claimNo);
        } else {

            damageAreas = damageAreaRepository.findByNotificationNo(claimNo);
        }

        List<byte[]> imageArray = new ArrayList<>();
        for (DamageArea s : damageAreas) {
            imageArray.add(s.getImage());
        }

        return imageArray;
    }

    // Get Intimation details for Claim Seq No
    private IntimationDetails getIntimationDetails(ClaimIntimation claimIntimation) {


        RefLossCause refLossCause = refLossCauseRepository.findByCode(claimIntimation.getCauseOfLoss());

        IntimationDetails intimationDetails = new IntimationDetails();
        intimationDetails.setClaimNo(claimIntimation.getClaimNo());
        intimationDetails.setIntimationDate(dateConversion.convertFromDateToString(claimIntimation.getIntimationDate()));
        intimationDetails.setCompletionDate(dateConversion.convertFromDateToString(claimIntimation.getClaimFileClosureEffectiveDate()));
        intimationDetails.setPoliceStation(formatter.replaceWithDash(claimIntimation.getPoliceStation()));
        intimationDetails.setCallerName(formatter.replaceWithDash(claimIntimation.getContactPerson()));
        intimationDetails.setClaimStatus(referenceService.getClaimStatus(claimIntimation.getClaimStatus()));
        intimationDetails.setCauseOfLoss(refLossCause.getDescription());

        intimationDetails.setAcr(formatter.format(claimIntimation.getEstimatedAmount()));
        intimationDetails.setClaimDoubtful(claimIntimation.getClaimDoubtful() == null ? "-" : referenceService.getYesOrNo(claimIntimation.getClaimDoubtful()));
        intimationDetails.setCallPoliceReport(claimIntimation.getPoliceReport() == null ? "-" : referenceService.getYesOrNo(claimIntimation.getPoliceReport()));


        return intimationDetails;

    }

    // Get Driver details for Claim Seq No
    private DriverDetails getDriverDetails(ClaimIntimation claimIntimation) {

        DriverDetails driverDetails = new DriverDetails();
        driverDetails.setDriverName(formatter.replaceWithDash(claimIntimation.getDriverName()));
        driverDetails.setDlNumber(formatter.replaceWithDash(claimIntimation.getDriverLicenseNo()));
        driverDetails.setDlCategory(formatter.replaceWithDash(claimIntimation.getLicenseCategory()));
        driverDetails.setLicenseFrom(dateConversion.convertFromDateToString(claimIntimation.getLicenseFromDate()));
        driverDetails.setLicenseTo(dateConversion.convertFromDateToString(claimIntimation.getLicenseToDate()));
        driverDetails.setRelation(formatter.replaceWithDash(claimIntimation.getRelationshipToInsured()));
        driverDetails.setRelationDetails(formatter.replaceWithDash(claimIntimation.getRelationshipToInsuredDescription()));

        return driverDetails;
    }

    //Get All Job Assignments for the Claim Intimation
    public List<JobAssignmentDTO> getJobAssignments(ClaimIntimation claimIntimation) {

        HashSet<JobAssignment> jobAssignmentList = jobAssignmentRepository.findAllByJobAssignmentPK_IntimationSequenceNo(claimIntimation.getSequenceNo());

        List<JobAssignmentDTO> jobAssignmentDTOList =

                jobAssignmentList.stream().map(

                        jobAssignment -> {

                            AssignLossAdjuster assignLossAdjuster = assignLossAdjusterRepository.findByAssignLossAdjusterPK_JobSequenceNo(jobAssignment.getJobAssignmentPK().getSequenceNo());
                            ExternalPerson externalPerson = externalPersonRepository.findByCode(assignLossAdjuster.getExternalPersonCode());

                            JobAssignmentDTO jobAssignmentDTO = new JobAssignmentDTO();
                            jobAssignmentDTO.setSequenceNo(jobAssignment.getJobAssignmentPK().getSequenceNo());
                            jobAssignmentDTO.setJobNo(jobAssignment.getJobNo());
                            jobAssignmentDTO.setTypeCode(jobAssignment.getSurveyType().getSurveyCode());
                            jobAssignmentDTO.setType(jobAssignment.getSurveyType().getSurveyDescription());
                            String jobStatus = referenceService.getJobStatusDescriptionByCode((assignLossAdjuster.getStatus() == null) ? "" : String.valueOf(assignLossAdjuster.getStatus()));
                            jobAssignmentDTO.setStatus((jobStatus == null || jobStatus.equals("")) ? "CORE" : jobStatus);
                            jobAssignmentDTO.setExternalPersonCode(assignLossAdjuster.getExternalPersonCode());
                            jobAssignmentDTO.setExternalPersonName(externalPersonService.getExternalPersonName(externalPerson));
                            jobAssignmentDTO.setPromiseTime((assignLossAdjuster.getPromiseTime() == null) ? "" : assignLossAdjuster.getPromiseTime());
                            jobAssignmentDTO.setLatitude(jobAssignment.getLatitude() == null ? 0.00 : Double.parseDouble(jobAssignment.getLatitude()));
                            jobAssignmentDTO.setLongitude(jobAssignment.getLongitude() == null ? 0.00 : Double.parseDouble(jobAssignment.getLatitude()));
                            jobAssignmentDTO.setNearestTown(jobAssignment.getNearestTown());
                            jobAssignmentDTO.setAppointedDate(dateConversion.convertFromDateToString(assignLossAdjuster.getAppointedDate()));
                            jobAssignmentDTO.setVisitDueDate(dateConversion.convertFromDateToString(assignLossAdjuster.getVisitDueDate()));
                            jobAssignmentDTO.setReportDueDate(dateConversion.convertFromDateToString(assignLossAdjuster.getReportDueDate()));
                            jobAssignmentDTO.setReportSubmittedDate(dateConversion.convertFromDateToString(assignLossAdjuster.getSubmittedDate()));
                            jobAssignmentDTO.setQuestionnaire(questionnaireService.viewQuestionnaire(jobAssignment, claimIntimation));
                            jobAssignmentDTO.setClaimNo(claimIntimation.getClaimNo());

//                        jobAssignmentDTO.setAccidentImages(imageService.viewClaimImages("IM001",jobAssignment.getJobAssignmentPK().getSequenceNo()));
//                        jobAssignmentDTO.setDocumentImages(imageService.viewClaimImages("IM002",jobAssignment.getJobAssignmentPK().getSequenceNo()));


                            return jobAssignmentDTO;

                        }
                ).collect(Collectors.toList());

        return jobAssignmentDTOList;
    }

    public Response searchClaimIntimations(ClaimSearchDTO claimSearchDTO) {
        ClaimInquiryResultDTO claimInquiryResultDTO = new ClaimInquiryResultDTO();
        List<ClaimSearchResultDTO> claimSearchResultDTOList = new ArrayList<>();
        List<NotificationSearchResultDTO> notificationSearchResultDTOList = new ArrayList<>();
        String query = "CALL PK_CL_CALL_CENTER.PU_GET_CLAIM_DETAILS(?,?,?,?,?,?)";
        Response response = new Response();
        CallableStatement callableStatement;
        Connection connection;
        ResultSet resultSet;
        try {
            connection = dataSource.getConnection();
            callableStatement = connection.prepareCall(query);
            callableStatement.setString(1, claimSearchDTO.getClaimNo());
            callableStatement.setString(2, claimSearchDTO.getPolicyNo());
            callableStatement.setString(3, claimSearchDTO.getCustomerName());
            callableStatement.setString(4, claimSearchDTO.getVehicleNo());
            callableStatement.setString(5, claimSearchDTO.getContactNo());
            callableStatement.registerOutParameter(6, OracleTypes.CURSOR);
            callableStatement.execute();
            resultSet = (ResultSet) callableStatement.getObject(6);
            while (resultSet != null && resultSet.next()) {
                ClaimSearchResultDTO claimSearchResultDTO = new ClaimSearchResultDTO();
                claimSearchResultDTO.setSequenceNo(resultSet.getString("INT_SEQ_NO"));
                claimSearchResultDTO.setClaimNo(resultSet.getString("INT_CLAIM_NO"));
                claimSearchResultDTO.setPolicyNo(resultSet.getString("INT_POLICY_NO"));
                claimSearchResultDTO.setLossDate(dateConversion.convertFromDateToString(resultSet.getDate("INT_DATE_LOSS"), "yyyy-MMM-dd HH:mm:ss"));
                claimSearchResultDTO.setCustomerName(resultSet.getString("CUSTOMER_NAME"));
                claimSearchResultDTO.setVehicleNo(resultSet.getString("INT_PRS_NAME"));
                claimSearchResultDTO.setClaimStatus(referenceService.getClaimStatus(resultSet.getString("INT_STATUS").charAt(0)));
                claimSearchResultDTOList.add(claimSearchResultDTO);
            }
            claimSearchResultDTOList.sort(Comparator.comparing(ClaimSearchResultDTO::getLossDate).reversed());
            List<ClaimNotification> claimNotifications = claimNotificationRepository.findAllByNotificationNoContainingAndCoverNoteNoContainingAndRiskNameContainingAndContactNoContaining(
                    claimSearchDTO.getClaimNo(), claimSearchDTO.getPolicyNo(), claimSearchDTO.getVehicleNo(), claimSearchDTO.getContactNo());
            notificationSearchResultDTOList = claimNotifications
                    .stream()
                    .filter(p -> p.getStatus() == 'P')
                    .map(p -> {
                        NotificationSearchResultDTO notificationSearchResultDTO = new NotificationSearchResultDTO();
                        notificationSearchResultDTO.setSequenceNo(p.getSequenceNo());
                        notificationSearchResultDTO.setNotificationNo(p.getNotificationNo());
                        notificationSearchResultDTO.setCoverNoteNo(p.getCoverNoteNo());
                        notificationSearchResultDTO.setLossDate(dateConversion.convertFromDateToString(p.getLossDate()));
                        notificationSearchResultDTO.setVehicleNo(p.getRiskName());
                        notificationSearchResultDTO.setCallerName(p.getContactPerson());
                        return notificationSearchResultDTO;
                    })
                    .sorted(Comparator.comparing(NotificationSearchResultDTO::getLossDate).reversed())
                    .collect(Collectors.toList());
            claimInquiryResultDTO.setClaimIntimations(claimSearchResultDTOList);
            claimInquiryResultDTO.setClaimNotifications(notificationSearchResultDTOList);
            connection.close();
            response = new Response(200, true, claimInquiryResultDTO);
        } catch (Exception e) {
            throw new DBException(e.getMessage());
        }
        return response;
    }

}
