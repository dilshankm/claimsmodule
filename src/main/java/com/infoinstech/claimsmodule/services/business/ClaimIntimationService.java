package com.infoinstech.claimsmodule.services.business;

import com.infoinstech.claimsmodule.constants.INTIMATION_PROCESS;
import com.infoinstech.claimsmodule.domain.model.Session;
import com.infoinstech.claimsmodule.domain.model.underwriting.transaction.EndorsementRisk;
import com.infoinstech.claimsmodule.domain.model.underwriting.transaction.PolicyRisk;
import com.infoinstech.claimsmodule.domain.repository.SessionRepository;
import com.infoinstech.claimsmodule.domain.repository.underwriting.transaction.*;
import com.infoinstech.claimsmodule.dto.claimintimation.ClaimIntimationDTO;
import com.infoinstech.claimsmodule.dto.claimintimation.ClaimIntimationResponseDTO;
import com.infoinstech.claimsmodule.dto.claimintimation.ClaimProvisionDTO;
import com.infoinstech.claimsmodule.dto.claimintimation.RiskValidationDTO;
import com.infoinstech.claimsmodule.dto.common.ClaimHistoryDTO;
import com.infoinstech.claimsmodule.dto.common.ClaimHistoryRequestDTO;
import com.infoinstech.claimsmodule.dto.common.JobNotification;
import com.infoinstech.claimsmodule.dto.common.Response;
import com.infoinstech.claimsmodule.domain.model.claims.history.ClaimTaskHistory;
import com.infoinstech.claimsmodule.domain.model.claims.master.ExternalPersonDevice;
import com.infoinstech.claimsmodule.domain.model.claims.reference.RefLossCause;
import com.infoinstech.claimsmodule.domain.model.claims.transaction.*;
import com.infoinstech.claimsmodule.domain.model.underwriting.master.Customer;
import com.infoinstech.claimsmodule.domain.model.underwriting.temporary.TempPolicy;
import com.infoinstech.claimsmodule.domain.model.underwriting.temporary.TempPolicyRisk;
import com.infoinstech.claimsmodule.domain.packages.*;
import com.infoinstech.claimsmodule.domain.repository.claims.history.ClaimTaskHistoryRepository;
import com.infoinstech.claimsmodule.domain.repository.claims.master.ExternalPersonDeviceRepository;
import com.infoinstech.claimsmodule.domain.repository.claims.reference.RefLossCauseRepository;
import com.infoinstech.claimsmodule.domain.repository.claims.transaction.*;
import com.infoinstech.claimsmodule.domain.repository.underwriting.master.CustomerRepository;
import com.infoinstech.claimsmodule.domain.repository.underwriting.master.ProductPerilRepository;
import com.infoinstech.claimsmodule.domain.repository.underwriting.master.ProductRepository;
import com.infoinstech.claimsmodule.domain.repository.underwriting.temporary.TempPolicyLocationRepository;
import com.infoinstech.claimsmodule.domain.repository.underwriting.temporary.TempPolicyRepository;
import com.infoinstech.claimsmodule.domain.repository.underwriting.temporary.TempPolicyRiskRepository;
import com.infoinstech.claimsmodule.dto.imageservice.ImageDTO;
import com.infoinstech.claimsmodule.dto.policysearch.PolicySearchDTO;
import com.infoinstech.claimsmodule.exceptions.types.*;
import com.infoinstech.claimsmodule.services.api.OldNotificationService;
import com.infoinstech.claimsmodule.services.common.ClaimsCommonService;
import com.infoinstech.claimsmodule.services.converters.ClaimsConverter;
import com.infoinstech.claimsmodule.services.parameters.ReceiptingParametersService;
import com.infoinstech.claimsmodule.services.reference.ReferenceService;
import com.infoinstech.claimsmodule.services.util.DataValidator;
import com.infoinstech.claimsmodule.services.util.DateConversion;
import com.infoinstech.claimsmodule.services.util.Formatter;
import com.infoinstech.claimsmodule.services.util.Settings;
import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Service;

import javax.persistence.EntityManager;
import javax.persistence.ParameterMode;
import javax.persistence.PersistenceContext;
import javax.persistence.StoredProcedureQuery;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;

@Service
public class ClaimIntimationService {


    @Autowired
    Environment env;
    @Autowired
    Formatter formatter;

    @Autowired
    PK_CL_CALL_CENTER pk_cl_CALLCENTER;
    @Autowired
    PK_CL_TAB_SEQUENCE pk_cl_tab_sequence;
    @Autowired
    PK_CL_T_INTIMATION pk_cl_t_intimation;
    @Autowired
    PK_INFOINS_SMS_GENERATION pk_infoins_sms_generation;
    @Autowired
    PK_INFOINS_MAILS_GENERATION pk_infoins_mails_generation;

    //Repositories
    @Autowired
    ClaimIntimationRepository claimIntimationRepository;
    @Autowired
    PolicyRiskRepository policyRiskRepository;
    @Autowired
    CustomerRepository customerRepository;
    @Autowired
    JobAssignmentRepository jobAssignmentRepository;
    @Autowired
    AssignLossAdjusterRepository assignLossAdjusterRepository;
    @Autowired
    PolicyPerilRepository policyPerilRepository;
    @Autowired
    PolicyRepository policyRepository;
    @Autowired
    RefLossCauseRepository refLossCauseRepository;
    @Autowired
    PolicyExcessTypeRepository policyExcessTypeRepository;
    @Autowired
    ProductRepository productRepository;
    @Autowired
    ProductPerilRepository productPerilRepository;
    @Autowired
    ProvisionDetailsRepository provisionDetailsRepository;
    @Autowired
    TempPolicyRiskRepository tempPolicyRiskRepository;
    @Autowired
    TempPolicyRepository tempPolicyRepository;
    @Autowired
    TempPolicyLocationRepository tempPolicyLocationRepository;
    @Autowired
    ClaimHandlerRepository claimHandlerRepository;
    @Autowired
    ClaimCurrentTaskRepository claimCurrentTaskRepository;
    @Autowired
    ClaimTaskHistoryRepository claimTaskHistoryRepository;
    @Autowired
    ExternalPersonDeviceRepository externalPersonDeviceRepository;
    @Autowired
    private DamageAreaRepository damageAreaRepository;
    @Autowired
    private ClaimsCommonService claimsCommonService;
    @Autowired
    private Settings settings;
    //LOV Services
    @Autowired
    ReferenceService referenceService;
    //Parameter Services
    @Autowired
    ReceiptingParametersService receiptingParameterService;
    //Business
    @Autowired
    private WrapperService wrapperService;
    @Autowired
    private ClaimProvisionService claimProvisionService;
    @Autowired
    private JobAssignmentService jobAssignmentService;
    @Autowired
    private CustomerService customerService;
    @Autowired
    private PolicyRiskInformationService policyRiskInformationService;
    @Autowired
    private VehicleSketchService vehicleSketchService;
    //Utility
    @Autowired
    DateConversion dateConversion;
    //Converters
    @Autowired
    private ClaimsConverter claimsConverter;
    @Autowired
    private OldNotificationService oldNotificationService;
    @Autowired
    private ReceiptingParametersService receiptingParametersService;
    @Autowired
    private SessionRepository sessionRepository;
    @Autowired
    private EndorsementRiskRepository endorsementRiskRepository;

    private static final Logger logger = LogManager.getLogger(ClaimIntimationService.class);

    @Autowired
    private ImageRepository imageRepository;


    //Check whether the Risk is valid for the given Policy and Loss Date
    public Boolean validateRisk(RiskValidationDTO riskValidationDTO) {
            Optional<PolicyRisk> policyRisk =
                    policyRiskRepository.findByPolicyRiskPK_SequenceNo(riskValidationDTO.getRiskSequenceNo());
            EndorsementRisk endorsementRisk = policyRisk.isPresent() ? null :
                    endorsementRiskRepository.findByEndorsementRiskPK_SequenceNo(riskValidationDTO.getRiskSequenceNo());
            riskValidationDTO.setRiskOrder(policyRisk.isPresent() ? policyRisk.get().getRiskOrderNo() :
                    endorsementRisk.getRiskOrderNo());
            if (riskValidationDTO.getPolicyNo() == null) {
                throw new PolicyNumberNotFoundException("Policy number not found");
                //return new Response(400, false, "Policy number not found");
            } else if (riskValidationDTO.getRiskOrder() == null) {
                throw new RiskOrderNotFoundException("Risk order not found");
               // return new Response(400, false, "Risk order not found");
            } else
                wrapperService.deleteDataFromTempPolicyTables();
                TempPolicy tempPolicy = wrapperService.buildPolicy(riskValidationDTO.getPolicyNo(), riskValidationDTO.getLossDate());
                if (tempPolicy == null) {
                    throw new PolicyNotExistForTheLossDateException("Policy not exists for the given loss date / time");
                   // return new Response(400, false, "Policy not exists for the given loss date / time");
                } else {
                    String message = validateLossDate(tempPolicy, riskValidationDTO.getRiskOrder(), riskValidationDTO.getLossDate());
                    if (message.equalsIgnoreCase("success")) {
                        return true;
                        //return new Response(200, true, "Valid Loss Date");
                    } else {
                        throw new ClaimGeneralException("Error");
                       // return new Response(400, false, message);
                    }
                }
    }

    public String validateLossDate(TempPolicy tempPolicy,String riskOrder, Date lossDate){

        String tempPolicySequence = tempPolicy.getSequenceNo();
        Date tempPolicyFromDate = tempPolicy.getPeriodFrom();
        Date newLossDate = lossDate;
        Date tempPolicyToDate = tempPolicy.getPeriodTo();

        String message = null;

        if (newLossDate.after(new Date())) {
             message = "Accident Date is greater than the Current Date";

        } else if(!(newLossDate.after(tempPolicyFromDate) && newLossDate.before(tempPolicyToDate))){
            message = "Loss date not within policy period";

        } else if (tempPolicy != null) {
            Set<TempPolicyRisk> policyRisksSet = tempPolicyRiskRepository.findByTempPolicyRiskPK_PolicySequenceNo(tempPolicySequence);

            TempPolicyRisk tempPolicyRisk = policyRisksSet.stream()
                    .filter(f -> tempPolicy.getSequenceNo().equalsIgnoreCase(f.getTempPolicyRiskPK().getPolicySequenceNo()) && f.getRiskOrderNo2().equalsIgnoreCase(riskOrder))
                    .findAny()
                    .orElse(null);

            message = tempPolicyRisk == null ? "Risk not found" : "Success";
        } else{
            message = "Policy not found";
        }
        return message;
    }

    public ClaimIntimationResponseDTO intimateClaim(ClaimIntimationDTO claimIntimationDTO) {
        ClaimIntimationResponseDTO claimResponseDTO = null;
        INTIMATION_PROCESS process = INTIMATION_PROCESS.INITIAL;
        TempPolicy tempPolicy = wrapperService.buildPolicy(claimIntimationDTO.getPolicyNo(), claimIntimationDTO.getLossDate());
        TempPolicyRisk tempPolicyRisk = tempPolicyRiskRepository.findByTempPolicyRiskPK_PolicySequenceNoAndRiskOrderNo(
                tempPolicy.getSequenceNo(), claimIntimationDTO.getRiskOrder());
        String classCode = tempPolicy.getClassCode();
        String branchCode = (settings.getSite().equals("AMI")) ? tempPolicy.getBranchCode() : receiptingParameterService.getBaseAccountBranch();
        String productCode = tempPolicy.getProductCode();
        claimIntimationDTO.setClassCode(tempPolicy.getClassCode());
        //Generating the Sequences For Claim Tables - Core
        ClaimIntimation claimIntimation = new ClaimIntimation(dateConversion.convertDate(new Date()));
        String claimNo = pk_cl_CALLCENTER.getClaimNo(classCode, productCode, branchCode);
        //String claimSeqNo = pk_cl_tab_sequence.getIntimationSequenceNo(branchCode);
        String intimationSequenceNo = claimsCommonService.generateSequence(ClaimIntimation.SEQUENCE, branchCode);
        claimIntimation.setSequenceNo(intimationSequenceNo);
        claimIntimation.setClaimNo(claimNo);
        claimIntimation = claimsConverter.ClaimIntimationDTOToClaimIntimation.apply(claimIntimationDTO, claimIntimation);
        claimIntimation = claimsConverter.TemporaryPolicyToClaimIntimation.apply(tempPolicy, claimIntimation);
        claimIntimation = claimsConverter.temporaryPolicyRiskToClaimIntimation.apply(tempPolicyRisk, claimIntimation);
        List<ClaimProvisionDTO> claimProvisionDTOList = claimIntimationDTO.getProvisionList();
        Boolean defaultProvision = false;
        ProvisionDetails provisionDetails = null;
        List<ProvisionDetails> provisionDetailsList = new ArrayList<>();
        if (claimProvisionDTOList != null) {
            provisionDetailsList = claimProvisionService.claimIntimationProvisioning(claimIntimation, claimIntimationDTO);
            if (claimProvisionDTOList.size() == 0 || provisionDetailsList.size() == 0) {
                if (settings.getSite().equals("SANASA")) {
                    provisionDetails = claimProvisionService.defaultProvisioning(claimIntimation);
                    defaultProvision = true;
                }
            }
        }
        ClaimHandler claimHandler = new ClaimHandler();
        String claimHandlerSequenceNo = claimsCommonService.generateSequence(ClaimHandler.SEQUENCE, branchCode);
        claimHandler.setSequenceNo(claimHandlerSequenceNo);
        claimHandler = claimsConverter.claimIntimationToClaimHandler.apply(claimIntimation, claimHandler);
        // Added by Rovidu Udesh - AMI
/*        Optional<Session> session = sessionRepository.findBySessionTokenAndIsActive(authorizationHeader, 'Y');
        if (session.isPresent()) {*/
            claimIntimation.setBPartyCode(tempPolicy.getBPartyCode());
        //}
        claimIntimation.setCorCode("AD003");
        claimIntimation.setClaimFileClosureEffectiveDate(null);
        claimIntimation.setContactPerson(claimIntimationDTO.getContactName());
        String callerMobile = claimIntimationDTO.getCallerMobile();
        if (!DataValidator.validatePhoneNumber(callerMobile)) {
            throw new ClaimIntimateGeneralException("Invalid caller mobile");
          //  return new Response(400, false, "Invalid caller mobile");
        }
        claimIntimation.setContactNo(claimIntimationDTO.getContactMobile());
        claimIntimationRepository.save(claimIntimation);
        process = INTIMATION_PROCESS.CLAIM_INTIMATION;
        claimHandlerRepository.save(claimHandler);
        if (defaultProvision) {
            provisionDetailsRepository.save(provisionDetails);
        } else if (provisionDetailsList.size() != 0) {
            provisionDetailsRepository.saveAll(provisionDetailsList);
        }
        if (claimIntimation.getClassCode().equals("MT") && claimIntimationDTO.getBaseString() != null && !claimIntimationDTO.getBaseString().equals("")) {
            vehicleSketchService.saveDamageSketch(claimIntimation, null, claimIntimationDTO.getBaseString());
        }
        // Send Email and SMS for Claim Intimation to enabled persons in Core System. - for SANASA
        if (env.getProperty("insurance.site").equals("SANASA")) {
            pk_infoins_sms_generation.sendSMS("CL001", claimNo);
            pk_infoins_mails_generation.sendEmail("CL001", claimNo);
        }
        JobNotification jobNotification = new JobNotification();
        JobAssignment jobAssignment = null;
        ExternalPersonDevice externalPersonDevice;
        if (process == INTIMATION_PROCESS.CLAIM_INTIMATION) {
            if (claimIntimationDTO.getAssessorCode() != null && !claimIntimationDTO.getAssessorCode().equals("")) {
                externalPersonDevice = externalPersonDeviceRepository.
                        findByExternalPersonCodeAndRegistered(claimIntimationDTO.getAssessorCode(), 'Y');
                jobAssignment = claimsConverter.claimIntimationToJobAssignment.apply(claimIntimation);
                if (claimIntimationDTO.getAssignmentTypeCode() == null || claimIntimationDTO.getAssignmentTypeCode().equals("")) {
                    jobAssignment.setSurveyCode("ST008");
                } else {
                    jobAssignment.setSurveyCode(claimIntimationDTO.getAssignmentTypeCode());
                }
                jobAssignment.setStatus('N');
                AssignLossAdjuster assignLossAdjuster = new AssignLossAdjuster(dateConversion.convertDate(new Date()));

                String lossAdjusterSequenceNo = claimsCommonService.generateSequence(AssignLossAdjuster.SEQUENCE, branchCode);
                AssignLossAdjusterPK assignLossAdjusterPK = new AssignLossAdjusterPK();
                assignLossAdjusterPK.setSequenceNo(lossAdjusterSequenceNo);
                assignLossAdjusterPK.setJobSequenceNo(jobAssignment.getJobAssignmentPK().getSequenceNo());
                assignLossAdjusterPK.setIntimationSequenceNo(claimIntimation.getSequenceNo());

                assignLossAdjuster.setAssignLossAdjusterPK(assignLossAdjusterPK);
                assignLossAdjuster.setStatus('N');
                assignLossAdjuster.setExternalPersonCode(claimIntimationDTO.getAssessorCode());
                assignLossAdjuster.setPromiseTime(claimIntimationDTO.getPromisedTime());
                assignLossAdjuster.setCreatedBy(claimIntimationDTO.getCreatedBy().toUpperCase());
                assignLossAdjuster.setModifiedBy(claimIntimationDTO.getModifiedBy().toUpperCase());
                assignLossAdjuster.setModifiedDate(new Date());
                assignLossAdjuster.setAppointedDate(dateConversion.convertDate(new Date()));
                assignLossAdjuster.setReportDueDate(dateConversion.convertDate(new Date()));
                assignLossAdjuster.setVisitDueDate(dateConversion.convertDate(new Date()));
                assignLossAdjuster.setCurrency(tempPolicy.getCurrency());

                Customer customer = customerRepository.findByCustomerCode(claimIntimation.getCustomerCode());
                jobAssignmentRepository.save(jobAssignment);
                assignLossAdjusterRepository.save(assignLossAdjuster);
                if (claimIntimationDTO.getActionTaken().equals("CLAIM INSPECTION")) {

                    oldNotificationService.sendJobNotification(claimIntimation, jobAssignment, assignLossAdjuster, externalPersonDevice, customer);
                }
                process = INTIMATION_PROCESS.JOB_ASSIGNMENT;
            }
        }

        List<ImageDTO> accidentImages = claimIntimationDTO.getAccidentImages();
        List<Image> images;
        if (claimIntimationDTO.getAccidentImages() != null) {
            for (ImageDTO imageDTO : accidentImages) {
                Image image = new Image();
                image.setSequenceNo(claimsCommonService.generateSequence(Image.SEQUENCE, receiptingParametersService.getBaseAccountBranch()));
                image.setIntimationSequenceNo(claimIntimation.getSequenceNo());
                Date capturedDate;
                try {
                    capturedDate = new SimpleDateFormat("dd-MMM-yyyy HH:mm:ss").parse(imageDTO.getCapturedDate());
                    image.setCapturedDate(capturedDate);
                } catch (ParseException e) {
                    e.printStackTrace();
                }
                image.setClaimNo(claimIntimation.getClaimNo());
                image.setImageTypeCode(imageDTO.getTypeCode());
//                image.setJobNo(jobAssignment.getJobNo());
                image.setUploadTypeCode(claimIntimationDTO.getUploadType());
                image.setName(dateConversion.convertFromDateToString(new Date(), "yyyyMMddHHmmssSSSS"));
                String imageString = imageDTO.getImageString().replaceAll("\\s+", "");
                image.setImage(Base64.getDecoder().decode(imageString));
                image.setCreatedBy(claimIntimationDTO.getCreatedBy());
                image.setModifiedBy(claimIntimationDTO.getModifiedBy());
                imageRepository.save(image);
            }
        }
        if (process == INTIMATION_PROCESS.CLAIM_INTIMATION) {
            ClaimCurrentTask claimCurrentTask = new ClaimCurrentTask();
            claimCurrentTask.setSequenceNo(claimsCommonService.generateSequence(ClaimCurrentTask.SEQUENCE));
            claimCurrentTask.setTaskCode("CL001");
            claimCurrentTask = claimsConverter.claimIntimationToClaimCurrentTask.apply(claimIntimation, claimCurrentTask);
//test
            claimCurrentTaskRepository.save(claimCurrentTask);
           // return new Response(
                 //   200,
                 //   true,
                  //  "The Claim Intimation was successful, Claim No is " + claimNo + ".",
            claimResponseDTO = new ClaimIntimationResponseDTO(claimNo);
          //  );
        } else if (process == INTIMATION_PROCESS.JOB_ASSIGNMENT) {
            ClaimTaskHistory claimTaskHistory = new ClaimTaskHistory();
            claimTaskHistory.setSequenceneNo(claimsCommonService.generateSequence(ClaimTaskHistory.SEQUENCE));
            claimTaskHistory.setTaskCode("CL001");
            claimTaskHistory = claimsConverter.claimIntimationToClaimTaskHistory.apply(claimIntimation, claimTaskHistory);
            ClaimCurrentTask claimCurrentTask = new ClaimCurrentTask();
            claimCurrentTask.setSequenceNo(claimsCommonService.generateSequence(ClaimCurrentTask.SEQUENCE));
            claimCurrentTask.setTaskCode("CL004");
            claimCurrentTask = claimsConverter.claimIntimationToClaimCurrentTask.apply(claimIntimation, claimCurrentTask);
            claimTaskHistoryRepository.save(claimTaskHistory);
            claimCurrentTaskRepository.save(claimCurrentTask);
           // return new Response(
           //         200,
           //         true,
           //         "The Claim Intimation was successful, The Claim No is " + claimNo + ".",
            claimResponseDTO = new ClaimIntimationResponseDTO(claimNo);
          //  );
        }
        return claimResponseDTO;
    }


    // Get Claim History Details based on Policy No and Policy Risk Sequence
    public List<ClaimHistoryDTO> viewClaimHistoryDetails(ClaimHistoryRequestDTO claimHistoryRequestDTO) {

        List<ClaimIntimation> claimIntimationList = claimIntimationRepository.findByPolicySequenceNoAndRiskSequenceNo(claimHistoryRequestDTO.getPolicySequenceNo(),claimHistoryRequestDTO.getRiskSequenceNo());

        ClaimHistoryDTO claimHistoryDTO;
        List<ClaimHistoryDTO> claimHistoryList = new ArrayList<>();

        for (ClaimIntimation claimIntimation : claimIntimationList) {

            claimHistoryDTO = new ClaimHistoryDTO();

            claimHistoryDTO.setClaimNo(claimIntimation.getClaimNo());
            claimHistoryDTO.setIntimationDate(dateConversion.convertFromDateToString(claimIntimation.getIntimationDate()));
            claimHistoryDTO.setLossDate(dateConversion.convertFromDateToString(claimIntimation.getLossDate()));

            RefLossCause refLossCause = null;

            if(claimIntimation.getCauseOfLoss()!=null){

                refLossCause = refLossCauseRepository.findByCode(claimIntimation.getCauseOfLoss());
            }

            if(refLossCause !=null){

                claimHistoryDTO.setCauseOfLoss((refLossCause.getDescription()==null)? "-" : refLossCause.getDescription());
            }
            // 2017/08/29 - Chamith Perera - Loss description instead of loss ReferenceCode
            claimHistoryDTO.setStatus(referenceService.getClaimStatus(claimIntimation.getClaimStatus()));

            claimHistoryList.add(claimHistoryDTO);
        }

        return claimHistoryList;
    }
}
