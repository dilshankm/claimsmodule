package com.infoinstech.claimsmodule.services.business;

import com.infoinstech.claimsmodule.dto.claiminquiry.*;
import com.infoinstech.claimsmodule.domain.model.claims.master.ExternalPerson;
import com.infoinstech.claimsmodule.domain.model.claims.reference.RefLossCause;
import com.infoinstech.claimsmodule.domain.model.claims.transaction.ClaimNotification;
import com.infoinstech.claimsmodule.domain.model.claims.transaction.NotificationAssignment;
import com.infoinstech.claimsmodule.domain.model.claims.transaction.NotificationLossAdjuster;
import com.infoinstech.claimsmodule.domain.repository.claims.master.ExternalPersonRepository;
import com.infoinstech.claimsmodule.domain.repository.claims.reference.RefLossCauseRepository;
import com.infoinstech.claimsmodule.domain.repository.claims.transaction.ClaimNotificationRepository;
import com.infoinstech.claimsmodule.domain.repository.claims.transaction.NotificationAssignmentRepository;
import com.infoinstech.claimsmodule.domain.repository.claims.transaction.NotificationLossAdjusterRepository;
import com.infoinstech.claimsmodule.services.reference.ReferenceService;
import com.infoinstech.claimsmodule.services.util.DateConversion;
import com.infoinstech.claimsmodule.services.util.Formatter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

@Service
public class ClaimNotificationInquiryService {

    @Autowired
    private ClaimNotificationRepository claimNotificationRepository;
    @Autowired
    private NotificationAssignmentRepository notificationAssignmentRepository;
    @Autowired
    private NotificationLossAdjusterRepository notificationLossAdjusterRepository;
    @Autowired
    private RefLossCauseRepository refLossCauseRepository;
    @Autowired
    private ExternalPersonRepository externalPersonRepository;


    @Autowired
    private ClaimInquiryService claimInquiryService;
    @Autowired
    private ReferenceService referenceService;
    @Autowired
    private QuestionnaireService questionnaireService;
    @Autowired
    private ExternalPersonService externalPersonService;


    @Autowired
    private DateConversion dateConversion;
    @Autowired
    private Formatter formatter;

    // Get Claim details based on Claim Sequence No
    public NotificationInquiryDTO getNotificationInquiryDetails(String notificationSequenceNo) {

        ClaimNotification claimNotification = claimNotificationRepository.findBySequenceNo(notificationSequenceNo);
        NotificationInquiryDTO notificationInquiryDTO = null;

        if (claimNotification != null) {

            notificationInquiryDTO = new NotificationInquiryDTO();
            notificationInquiryDTO.setCoverNoteDetails(getCoverNoteDetails(claimNotification));
            notificationInquiryDTO.setNotificationDetails(getNotificationDetails(claimNotification));
            notificationInquiryDTO.setAccidentDetails(getAccidentDetails(claimNotification));
            notificationInquiryDTO.setDriverDetails(getDriverDetails(claimNotification));
        }

        return notificationInquiryDTO;
    }

    public CoverNoteDetails getCoverNoteDetails(ClaimNotification claimNotification) {

        CoverNoteDetails coverNoteDetails = new CoverNoteDetails();

        coverNoteDetails.setCoverNoteNo(claimNotification.getCoverNoteNo());
        coverNoteDetails.setVehicleNo(claimNotification.getRiskName());
        coverNoteDetails.setEffectiveFromDate(dateConversion.convertFromDateToString(claimNotification.getEffectiveStartDate()));
        coverNoteDetails.setMakeAndModel(claimNotification.getMakeAndModel());
        coverNoteDetails.setYearOfManufacture(claimNotification.getYearOfManufacture());

        return coverNoteDetails;
    }

    // Get Accident details for Claim Seq No
    private AccidentDetails getAccidentDetails(ClaimNotification claimNotification) {


        AccidentDetails accidentDetails = new AccidentDetails();
        accidentDetails.setPlaceOfAccident(formatter.replaceWithDash(claimNotification.getPlaceOfLoss()));
        accidentDetails.setLossRemarks(formatter.replaceWithDash(claimNotification.getLossRemarks()));
        accidentDetails.setDateOfAccident(dateConversion.convertFromDateToString(claimNotification.getLossDate()));
        accidentDetails.setNearestTown(formatter.replaceWithDash(claimNotification.getPlaceOfLoss()));
        accidentDetails.setThirdPartyDetails(formatter.replaceWithDash(claimNotification.getThirdPartyDetails()));
        accidentDetails.setLatitude(formatter.replaceWithDash(claimNotification.getLatitude()));
        accidentDetails.setLongitude(formatter.replaceWithDash(claimNotification.getLongitude()));
        accidentDetails.setDamageImages(claimInquiryService.getDamageAreas(claimNotification.getNotificationNo()));
        return accidentDetails;
    }

    // Get Intimation details for Claim Seq No
    private NotificationDetails getNotificationDetails(ClaimNotification claimNotification) {


        RefLossCause refLossCause = refLossCauseRepository.findByCode(claimNotification.getCauseOfLoss());

        NotificationDetails notificationDetails = new NotificationDetails();
        notificationDetails.setNotificationNo(claimNotification.getNotificationNo());
        notificationDetails.setNotifiedDate(dateConversion.convertFromDateToString(claimNotification.getNotifiedDate()));
        notificationDetails.setCompletionDate(dateConversion.convertFromDateToString(claimNotification.getClaimFileClosureEffectiveDate()));
        notificationDetails.setPoliceStation(formatter.replaceWithDash(claimNotification.getPoliceStation()));
        notificationDetails.setCallerName(formatter.replaceWithDash(claimNotification.getContactPerson()));
        notificationDetails.setNotificationStatus(referenceService.getClaimStatus(claimNotification.getStatus()));
        notificationDetails.setCauseOfLoss(refLossCause.getDescription());
        notificationDetails.setAcr(formatter.format(claimNotification.getEstimatedAmount()));
        notificationDetails.setClaimDoubtful(claimNotification.getClaimDoubtful() == null ? "-" : referenceService.getYesOrNo(claimNotification.getClaimDoubtful()));
        notificationDetails.setCallPoliceReport(claimNotification.getPoliceReport() == null ? "-" : referenceService.getYesOrNo(claimNotification.getPoliceReport()));

        return notificationDetails;

    }

    // Get Driver details for Claim Seq No
    private DriverDetails getDriverDetails(ClaimNotification claimNotification) {

        DriverDetails driverDetails = new DriverDetails();
        driverDetails.setDriverName(formatter.replaceWithDash(claimNotification.getDriverName()));
        driverDetails.setDlNumber(formatter.replaceWithDash(claimNotification.getDriverLicenseNo()));
        driverDetails.setDlCategory(formatter.replaceWithDash(claimNotification.getLicenseCategory()));
        driverDetails.setLicenseFrom(dateConversion.convertFromDateToString(claimNotification.getLicenseFromDate()));
        driverDetails.setLicenseTo(dateConversion.convertFromDateToString(claimNotification.getLicenseToDate()));
        driverDetails.setRelation(formatter.replaceWithDash(claimNotification.getRelationshipToInsured()));
        driverDetails.setRelationDetails(formatter.replaceWithDash(claimNotification.getRelationshipToInsuredDescription()));

        return driverDetails;
    }


    //Get All Job Assignments for the Claim Intimation
    public List<JobAssignmentDTO> getNotificationAssignments(String claimNotificationSequencenNo) {


        ClaimNotification claimNotification = claimNotificationRepository.findBySequenceNo(claimNotificationSequencenNo);

        List<NotificationAssignment> notificationAssignments = notificationAssignmentRepository.findByNotificationAssignmentPK_NotificationSequenceNo
                (claimNotificationSequencenNo);


        List<JobAssignmentDTO> jobAssignmentDTOList =

                notificationAssignments.stream().map(

                        notificationAssignment -> {

                            NotificationLossAdjuster notificationLossAdjuster = notificationLossAdjusterRepository.findByNotificationLossAdjusterPK_AssignmentSequenceNo(
                                    notificationAssignment.getNotificationAssignmentPK().getSequenceNo()
                            );
                            ExternalPerson externalPerson = externalPersonRepository.findByCode(notificationLossAdjuster.getExternalPersonCode());

                            JobAssignmentDTO jobAssignmentDTO = new JobAssignmentDTO();
                            jobAssignmentDTO.setSequenceNo(notificationAssignment.getNotificationAssignmentPK().getSequenceNo());
                            jobAssignmentDTO.setJobNo(notificationAssignment.getJobNo());
                            jobAssignmentDTO.setTypeCode(notificationAssignment.getRefSurveyType().getSurveyCode());
                            jobAssignmentDTO.setType(notificationAssignment.getRefSurveyType().getSurveyDescription());
                            String jobStatus = referenceService.getJobStatusDescriptionByCode((notificationLossAdjuster.getStatus() == null) ? "" : String.valueOf(notificationAssignment.getStatus()));
                            jobAssignmentDTO.setStatus((jobStatus == null || jobStatus.equals("")) ? "CORE" : jobStatus);
                            jobAssignmentDTO.setExternalPersonCode(notificationLossAdjuster.getExternalPersonCode());
                            jobAssignmentDTO.setExternalPersonName(externalPersonService.getExternalPersonName(externalPerson));
                            jobAssignmentDTO.setPromiseTime((notificationLossAdjuster.getPromiseTime() == null) ? "" : notificationLossAdjuster.getPromiseTime());
                            jobAssignmentDTO.setLatitude(notificationAssignment.getLatitude() == null ? 0.00 : Double.parseDouble(notificationAssignment.getLatitude()));
                            jobAssignmentDTO.setLongitude(notificationAssignment.getLongitude() == null ? 0.00 : Double.parseDouble(notificationAssignment.getLatitude()));
                            jobAssignmentDTO.setNearestTown(notificationAssignment.getNearestTown());
                            jobAssignmentDTO.setAppointedDate(dateConversion.convertFromDateToString(notificationLossAdjuster.getAppointedDate()));
                            jobAssignmentDTO.setVisitDueDate(dateConversion.convertFromDateToString(notificationLossAdjuster.getVisitDueDate()));
                            jobAssignmentDTO.setReportDueDate(dateConversion.convertFromDateToString(notificationLossAdjuster.getReportDueDate()));
                            jobAssignmentDTO.setReportSubmittedDate(dateConversion.convertFromDateToString(notificationLossAdjuster.getSubmittedDate()));
                            jobAssignmentDTO.setQuestionnaire(questionnaireService.viewQuestionnaire(notificationAssignment, claimNotification));
                            jobAssignmentDTO.setClaimNo(claimNotification.getNotificationNo());

                            return jobAssignmentDTO;

                        }
                ).collect(Collectors.toList());

        return jobAssignmentDTOList;
    }

}
