package com.infoinstech.claimsmodule.services.business;

import com.infoinstech.claimsmodule.constants.INTIMATION_PROCESS;
import com.infoinstech.claimsmodule.dto.claimnotification.ClaimNotificationDTO;
import com.infoinstech.claimsmodule.dto.claimnotification.NotificationConversionDTO;
import com.infoinstech.claimsmodule.dto.common.Response;
import com.infoinstech.claimsmodule.domain.model.claims.history.ClaimTaskHistory;
import com.infoinstech.claimsmodule.domain.model.claims.master.ExternalPersonDevice;
import com.infoinstech.claimsmodule.domain.model.claims.transaction.*;
import com.infoinstech.claimsmodule.domain.model.underwriting.temporary.TempPolicy;
import com.infoinstech.claimsmodule.domain.model.underwriting.temporary.TempPolicyRisk;
import com.infoinstech.claimsmodule.domain.model.underwriting.transaction.PolicyRisk;
import com.infoinstech.claimsmodule.domain.packages.PK_CL_CALL_CENTER;
import com.infoinstech.claimsmodule.domain.packages.PK_CL_TAB_SEQUENCE;
import com.infoinstech.claimsmodule.domain.packages.PK_INFOINS_MAILS_GENERATION;
import com.infoinstech.claimsmodule.domain.packages.PK_INFOINS_SMS_GENERATION;
import com.infoinstech.claimsmodule.domain.repository.claims.history.ClaimTaskHistoryRepository;
import com.infoinstech.claimsmodule.domain.repository.claims.master.ExternalPersonDeviceRepository;
import com.infoinstech.claimsmodule.domain.repository.claims.transaction.*;
import com.infoinstech.claimsmodule.domain.repository.underwriting.master.CustomerRepository;
import com.infoinstech.claimsmodule.domain.repository.underwriting.temporary.TempPolicyRepository;
import com.infoinstech.claimsmodule.domain.repository.underwriting.temporary.TempPolicyRiskRepository;
import com.infoinstech.claimsmodule.domain.repository.underwriting.transaction.PolicyRiskRepository;
import com.infoinstech.claimsmodule.exceptions.types.ClaimGeneralException;
import com.infoinstech.claimsmodule.services.api.OldNotificationService;
import com.infoinstech.claimsmodule.services.common.ClaimsCommonService;
import com.infoinstech.claimsmodule.services.common.SequenceGeneration;
import com.infoinstech.claimsmodule.services.converters.ClaimsConverter;
import com.infoinstech.claimsmodule.services.parameters.ReceiptingParametersService;
import com.infoinstech.claimsmodule.services.util.DateConversion;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Optional;

@Service
public class ClaimNotificationService {

    @Autowired
    private ClaimsConverter claimsConverter;

    @Autowired
    private PK_CL_TAB_SEQUENCE pk_cl_tab_sequence;

    @Autowired
    private ClaimNotificationRepository claimNotificationRepository;
    @Autowired
    private NotificationAssignmentRepository notificationAssignmentRepository;
    @Autowired
    private NotificationLossAdjusterRepository notificationLossAdjusterRepository;
    @Autowired
    private ClaimCurrentTaskRepository claimCurrentTaskRepository;
    @Autowired
    private ExternalPersonDeviceRepository externalPersonDeviceRepository;

    @Autowired
    private DateConversion dateConversion;
    @Autowired
    private SequenceGeneration sequenceGeneration;
    @Autowired
    private ClaimsCommonService claimsCommonService;
    @Autowired
    private ReceiptingParametersService receiptingParametersService;
    @Autowired
    private OldNotificationService oldNotificationService;

    // For Claim Notification Conversion

    @Autowired
    private TempPolicyRiskRepository tempPolicyRiskRepository;
    @Autowired
    private TempPolicyRepository tempPolicyRepository;
    @Autowired
    private ClaimIntimationRepository claimIntimationRepository;
    @Autowired
    private ProvisionDetailsRepository provisionDetailsRepository;
    @Autowired
    private ClaimHandlerRepository claimHandlerRepository;
    @Autowired
    private JobAssignmentRepository jobAssignmentRepository;
    @Autowired
    private AssignLossAdjusterRepository assignLossAdjusterRepository;
    @Autowired
    private ClaimTaskHistoryRepository claimTaskHistoryRepository;
    @Autowired
    private CustomerRepository customerRepository;
    @Autowired
    private DamageAreaRepository damageAreaRepository;
    @Autowired
    private AssignQuestionRepository assignQuestionRepository;
    @Autowired
    private LossAdjusterChargeRepository lossAdjusterChargeRepository;
    @Autowired
    private ImageRepository imageRepository;
    @Autowired
    private PolicyRiskRepository policyRiskRepository;

    @Autowired
    private PK_CL_CALL_CENTER pk_cl_CALLCENTER;
    @Autowired
    private PK_INFOINS_SMS_GENERATION pk_infoins_sms_generation;
    @Autowired
    private PK_INFOINS_MAILS_GENERATION pk_infoins_mails_generation;


    @Autowired
    private WrapperService wrapperService;
    @Autowired
    private ClaimProvisionService claimProvisionService;
    @Autowired
    private ClaimIntimationService claimIntimationService;
    @Autowired
    private PolicyRiskInformationService policyRiskInformationService;
    @Autowired
    private VehicleSketchService vehicleSketchService;

    @Autowired
    private Environment environment;

    public Response notifyClaim(ClaimNotificationDTO claimNotificationDTO) {
        Response response = null;
        ClaimNotification claimNotification = new ClaimNotification();
        String branchCode = receiptingParametersService.getBaseAccountBranch();
        String notificationSequenceNo = claimsCommonService.generateSequence(ClaimNotification.SEQUENCE, branchCode);
        claimNotification.setSequenceNo(notificationSequenceNo);
        claimNotification.setNotificationNo("NT" + notificationSequenceNo);
        claimNotification = claimsConverter.claimNotificationDTOToClaimNotification.apply(claimNotificationDTO, claimNotification);
            claimNotificationRepository.save(claimNotification);
            response = new Response(200, true, "Claim Notification was successful. Notification No is " + claimNotification.getNotificationNo());
        if (claimNotificationDTO.getAssessorCode() != null && !claimNotificationDTO.getAssessorCode().equals("")) {
            ExternalPersonDevice externalPersonDevice = externalPersonDeviceRepository.
                    findByExternalPersonCodeAndRegistered(claimNotificationDTO.getAssessorCode(), 'Y');
            NotificationAssignment notificationAssignment = new NotificationAssignment();
            String notificationAssignmentSequenceNo = claimsCommonService.generateSequence(NotificationAssignment.SEQUENCE, branchCode);
            NotificationAssignmentPK notificationAssignmentPK = new NotificationAssignmentPK();
            notificationAssignmentPK.setSequenceNo(notificationAssignmentSequenceNo);
            notificationAssignmentPK.setNotificationSequenceNo(claimNotification.getSequenceNo());
            notificationAssignment.setNotificationAssignmentPK(notificationAssignmentPK);
            notificationAssignment.setJobNo("AS" + notificationAssignmentSequenceNo);
            notificationAssignment = claimsConverter.claimNotificationToNotificationAssignment.apply(claimNotification, notificationAssignment);
            notificationAssignment.setStatus('N');
            NotificationLossAdjuster notificationLossAdjuster = new NotificationLossAdjuster();
            String notificationLossAdjusterSequenceNo = claimsCommonService.generateSequence(AssignLossAdjuster.SEQUENCE, branchCode);
            NotificationLossAdjusterPK notificationLossAdjusterPK = new NotificationLossAdjusterPK();
            notificationLossAdjusterPK.setSequenceNo(notificationLossAdjusterSequenceNo);
            notificationLossAdjusterPK.setAssignmentSequenceNo(notificationAssignment.getNotificationAssignmentPK().getSequenceNo());
            notificationLossAdjusterPK.setNotificationSequenceNo(claimNotification.getSequenceNo());
            notificationLossAdjuster.setNotificationLossAdjusterPK(notificationLossAdjusterPK);
            notificationLossAdjuster.setAuthorizedBy("N");
            notificationLossAdjuster.setStatus('N');
            notificationLossAdjuster.setExternalPersonCode(claimNotificationDTO.getAssessorCode());
            notificationLossAdjuster.setPromiseTime(claimNotificationDTO.getPromisedTime());
            notificationLossAdjuster.setCreatedBy(claimNotificationDTO.getCreatedBy().toUpperCase());
            notificationLossAdjuster.setCreatedDate(new Date());
            notificationLossAdjuster.setModifiedBy(claimNotificationDTO.getModifiedBy().toUpperCase());
            notificationLossAdjuster.setModifiedDate(new Date());
            notificationLossAdjuster.setAppointedDate(dateConversion.convertDate(new Date()));
            notificationLossAdjuster.setReportDueDate(dateConversion.convertDate(new Date()));
            notificationLossAdjuster.setVisitDueDate(dateConversion.convertDate(new Date()));
                notificationAssignmentRepository.save(notificationAssignment);
                notificationLossAdjusterRepository.save(notificationLossAdjuster);
                if (claimNotificationDTO.getBaseString() != null)
                    vehicleSketchService.saveDamageSketch(null, claimNotification, claimNotificationDTO.getBaseString());
                oldNotificationService.sendAssignmentNotification(claimNotification, notificationAssignment, notificationLossAdjuster, externalPersonDevice);
                response = new Response(200, true, "Claim Notification was successful. Notification No is " + claimNotification.getNotificationNo());
        }
        return response;
    }

    public String convertClaimNotification(NotificationConversionDTO notificationConversionDTO) {

        String claimNumber = "";

        ClaimNotification claimNotification = claimNotificationRepository.findByNotificationNo(notificationConversionDTO.getClaimNotificationNo());

        TempPolicy tempPolicy = wrapperService.buildPolicy(notificationConversionDTO.getPolicyNo(), claimNotification.getLossDate());

        String message = claimIntimationService.validateLossDate(tempPolicy, claimNotification.getRiskName(), claimNotification.getLossDate());

        if (message != null) {
            throw new ClaimGeneralException(message);
        } else {

            INTIMATION_PROCESS process = INTIMATION_PROCESS.INITIAL;

            claimNotification.setStatus('L');

            TempPolicyRisk tempPolicyRisk = tempPolicyRiskRepository.findByTempPolicyRiskPK_PolicySequenceNoAndName(tempPolicy.getSequenceNo(), claimNotification.getRiskName());

            String classCode = tempPolicy.getClassCode();
            //String branchCode = parametersService.getBaseAccountBranch();
            String branchCode = tempPolicy.getBranchCode();
            String productCode = tempPolicy.getProductCode();

            //Generating the Sequences For Claim Tables - Core

            ClaimIntimation claimIntimation = new ClaimIntimation(dateConversion.convertDate(new Date()));

            String claimNo = pk_cl_CALLCENTER.getClaimNo(classCode, productCode, branchCode);
            String claimSeqNo = claimsCommonService.generateSequence(ClaimIntimation.SEQUENCE, branchCode);

            claimIntimation.setSequenceNo(claimSeqNo);
            claimIntimation.setClaimNo(claimNo);
            claimIntimation = claimsConverter.claimNotificationToClaimIntimation.apply(claimNotification, claimIntimation);
            claimIntimation = claimsConverter.TemporaryPolicyToClaimIntimation.apply(tempPolicy, claimIntimation);
            claimIntimation = claimsConverter.temporaryPolicyRiskToClaimIntimation.apply(tempPolicyRisk, claimIntimation);

            ClaimHandler claimHandler = new ClaimHandler();

            String claimHandlerSequenceNo = claimsCommonService.generateSequence(ClaimHandler.SEQUENCE, branchCode);
            claimHandler.setSequenceNo(claimHandlerSequenceNo);
            claimHandler = claimsConverter.claimIntimationToClaimHandler.apply(claimIntimation, claimHandler);

            ProvisionDetails provisionDetails = claimProvisionService.defaultProvisioning(claimIntimation);

            List<DamageArea> damageAreas = damageAreaRepository.findByNotificationNo(claimNotification.getNotificationNo());

            for (DamageArea damageArea : damageAreas) {

                damageArea.setClaimNo(claimNo);
                damageArea.setIntimationSequenceNo(claimSeqNo);
            }

            claimIntimationRepository.save(claimIntimation);
            provisionDetailsRepository.save(provisionDetails);
            claimHandlerRepository.save(claimHandler);
            claimNotificationRepository.save(claimNotification);
            damageAreaRepository.saveAll(damageAreas);

            // Send Email and SMS for Claim Intimation to enabled persons in Core System. - for SANASA

            if (environment.getProperty("insurance.site").equals("SANASA")) {

//                    pk_infoins_sms_generation.sendSMS("CL001", claimNo);
//                    pk_infoins_mails_generation.sendEmail("CL001", claimNo);
            }

            process = INTIMATION_PROCESS.CLAIM_INTIMATION;
            List<NotificationAssignment> notificationAssignments = claimNotification.getNotificationAssignments();

            if (process == INTIMATION_PROCESS.CLAIM_INTIMATION) {

                if (claimNotification.getNotificationAssignments() != null) {


                    for (NotificationAssignment assignment : notificationAssignments) {

                        JobAssignment jobAssignment = claimsConverter.claimIntimationToJobAssignment.apply(claimIntimation);
                        jobAssignment.setSurveyCode(assignment.getSurveryCode());
                        jobAssignment.setStatus(assignment.getStatus());


                        List<NotificationLossAdjuster> lossAdjusters = assignment.getLossAdjusters();

                        NotificationLossAdjuster notificationLossAdjuster = new NotificationLossAdjuster();

                        for (NotificationLossAdjuster lossAdjuster : lossAdjusters) {

                            notificationLossAdjuster = lossAdjuster;
                        }

                        AssignLossAdjuster assignLossAdjuster = new AssignLossAdjuster();

                        String lossAdjusterSequenceNo = claimsCommonService.generateSequence(AssignLossAdjuster.SEQUENCE, branchCode);
                        AssignLossAdjusterPK assignLossAdjusterPK = new AssignLossAdjusterPK();
                        assignLossAdjusterPK.setSequenceNo(lossAdjusterSequenceNo);
                        assignLossAdjusterPK.setJobSequenceNo(jobAssignment.getJobAssignmentPK().getSequenceNo());
                        assignLossAdjusterPK.setIntimationSequenceNo(claimIntimation.getSequenceNo());

                        assignLossAdjuster.setAssignLossAdjusterPK(assignLossAdjusterPK);
                        assignLossAdjuster.setAuthorizedBy(notificationLossAdjuster.getAuthorizedBy());
                        assignLossAdjuster.setStatus(notificationLossAdjuster.getStatus());
                        assignLossAdjuster.setExternalPersonCode(notificationLossAdjuster.getExternalPersonCode());
                        assignLossAdjuster.setPromiseTime(notificationLossAdjuster.getPromiseTime());
                        assignLossAdjuster.setCreatedBy(notificationLossAdjuster.getCreatedBy().toUpperCase());
                        assignLossAdjuster.setCreatedDate(new Date());
                        assignLossAdjuster.setModifiedBy(notificationLossAdjuster.getModifiedBy().toUpperCase());
                        assignLossAdjuster.setModifiedDate(new Date());
                        assignLossAdjuster.setAppointedDate(dateConversion.convertDate(notificationLossAdjuster.getAppointedDate()));
                        assignLossAdjuster.setReportDueDate(dateConversion.convertDate(notificationLossAdjuster.getReportDueDate()));
                        assignLossAdjuster.setVisitDueDate(dateConversion.convertDate(notificationLossAdjuster.getVisitDueDate()));
                        assignLossAdjuster.setCurrency(tempPolicy.getCurrency());


                        List<AssignQuestion> assignQuestions = assignQuestionRepository.findByNotificationAssignmentSequenceNo(assignment.getNotificationAssignmentPK().getSequenceNo());

                        if (assignQuestions != null) {

                            for (AssignQuestion question : assignQuestions) {

                                question.setClaimSequenceNo(claimSeqNo);
                                question.setJobAssignmentSequenceNo(jobAssignment.getJobAssignmentPK().getSequenceNo());
                                question.setJobAssignmentNo(jobAssignment.getJobNo());
                            }
                        }

                        List<NotificationAdjusterCharge> notificationAdjusterCharges = notificationLossAdjuster.getNotificationAdjusterCharges();

                        List<LossAdjusterCharge> lossAdjusterCharges = new ArrayList<>();
                        LossAdjusterCharge lossAdjusterCharge;

                        if (notificationAdjusterCharges != null) {

                            for (NotificationAdjusterCharge notificationAdjusterCharge : notificationAdjusterCharges) {

                                lossAdjusterCharge = new LossAdjusterCharge();
                                lossAdjusterCharge.setSequenceNo(claimsCommonService.generateSequence(LossAdjusterCharge.SEQUENCE, receiptingParametersService.getBaseAccountBranch()));
                                lossAdjusterCharge.setIntimationSequenceNo(claimIntimation.getSequenceNo());
                                lossAdjusterCharge.setJobAssignmentSequenceNo(jobAssignment.getJobAssignmentPK().getSequenceNo());
                                lossAdjusterCharge.setAssignLossAdjusterSequenceNo(assignLossAdjuster.getAssignLossAdjusterPK().getSequenceNo());
                                lossAdjusterCharge.setChargeCode(notificationAdjusterCharge.getChargeTypeCode());
                                lossAdjusterCharge.setUnitTypeCode(notificationAdjusterCharge.getUnitTypeCode());
                                lossAdjusterCharge.setDateVisited(notificationAdjusterCharge.getDateVisited());
                                lossAdjusterCharge.setUnitPrice(notificationAdjusterCharge.getUnitPrice());
                                lossAdjusterCharge.setNoOfUnits(notificationAdjusterCharge.getNoOfUnits().intValue());
                                lossAdjusterCharge.setCharges(notificationAdjusterCharge.getUnitPrice() * notificationAdjusterCharge.getNoOfUnits());

                                lossAdjusterCharges.add(lossAdjusterCharge);

                            }
                        }

                        List<Image> images = imageRepository.findByNotificationNoAndAssignmentNo(claimNotification.getNotificationNo(), assignment.getJobNo());

                        if (images != null) {

                            for (Image image : images) {

                                image.setClaimNo(claimNo);
                                image.setIntimationSequenceNo(claimSeqNo);
                            }
                        }

                        assignment.setStatus('L');
                        notificationLossAdjuster.setStatus('L');

                        jobAssignmentRepository.save(jobAssignment);
                        assignLossAdjusterRepository.save(assignLossAdjuster);

                        if (assignQuestions != null) assignQuestionRepository.saveAll(assignQuestions);
                        if (lossAdjusterCharges != null) lossAdjusterChargeRepository.saveAll(lossAdjusterCharges);
                        if (images != null) imageRepository.saveAll(images);

                        notificationAssignmentRepository.save(assignment);
                        notificationLossAdjusterRepository.save(notificationLossAdjuster);

                        process = INTIMATION_PROCESS.JOB_ASSIGNMENT;
                    }

                }
            }

            if (process == INTIMATION_PROCESS.CLAIM_INTIMATION) {

                ClaimCurrentTask claimCurrentTask = new ClaimCurrentTask();
                claimCurrentTask.setSequenceNo(claimsCommonService.generateSequence(ClaimCurrentTask.SEQUENCE));
                claimCurrentTask.setTaskCode("CL001");
                claimCurrentTask = claimsConverter.claimIntimationToClaimCurrentTask.apply(claimIntimation, claimCurrentTask);

                claimCurrentTaskRepository.save(claimCurrentTask);
                claimNumber = claimNo;
                //response = new Response(200, true, "The Claim Intimation was successful, The Claim No is " + claimNo + ".");

            } else if (process == INTIMATION_PROCESS.JOB_ASSIGNMENT) {

                ClaimTaskHistory claimTaskHistory = new ClaimTaskHistory();
                claimTaskHistory.setSequenceneNo(claimsCommonService.generateSequence(ClaimTaskHistory.SEQUENCE));
                claimTaskHistory.setTaskCode("CL001");
                claimTaskHistory = claimsConverter.claimIntimationToClaimTaskHistory.apply(claimIntimation, claimTaskHistory);

                ClaimCurrentTask claimCurrentTask = new ClaimCurrentTask();
                claimCurrentTask.setSequenceNo(claimsCommonService.generateSequence(ClaimCurrentTask.SEQUENCE));
                claimCurrentTask.setTaskCode("CL004");
                claimCurrentTask = claimsConverter.claimIntimationToClaimCurrentTask.apply(claimIntimation, claimCurrentTask);

                claimTaskHistoryRepository.save(claimTaskHistory);
                claimCurrentTaskRepository.save(claimCurrentTask);
                claimNumber = claimNo;
                //response = new Response(200, true, "The Claim Notification was converted successfully, The Claim No is " + claimNo + ".");
            }
        }
        return claimNumber;
    }
}
