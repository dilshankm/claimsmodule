package com.infoinstech.claimsmodule.services.business;

import com.infoinstech.claimsmodule.dto.claimintimation.ClaimIntimationDTO;
import com.infoinstech.claimsmodule.dto.claimintimation.ClaimProvisionDTO;
import com.infoinstech.claimsmodule.domain.model.claims.transaction.ClaimIntimation;
import com.infoinstech.claimsmodule.domain.model.claims.transaction.ProvisionDetails;
import com.infoinstech.claimsmodule.domain.model.claims.transaction.RevisionDetails;
import com.infoinstech.claimsmodule.domain.model.underwriting.master.Product;
import com.infoinstech.claimsmodule.domain.model.underwriting.master.ProductPeril;
import com.infoinstech.claimsmodule.domain.repository.claims.transaction.ClaimIntimationRepository;
import com.infoinstech.claimsmodule.domain.repository.claims.transaction.ProvisionDetailsRepository;
import com.infoinstech.claimsmodule.domain.repository.claims.transaction.RevisionDetailsRepository;
import com.infoinstech.claimsmodule.domain.repository.underwriting.master.ProductPerilRepository;
import com.infoinstech.claimsmodule.domain.repository.underwriting.master.ProductRepository;
import com.infoinstech.claimsmodule.services.common.ClaimsCommonService;
import com.infoinstech.claimsmodule.services.parameters.ReceiptingParametersService;
import com.infoinstech.claimsmodule.services.util.DateConversion;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.*;

@Service
public class ClaimProvisionService {

    //Parameter Services
    @Autowired
    ReceiptingParametersService receiptingParametersService;

    //Repositories
    @Autowired
    private ProductRepository productRepository;
    @Autowired
    private ProductPerilRepository productPerilRepository;
    @Autowired
    private ProvisionDetailsRepository provisionDetailsRepository;
    @Autowired
    private RevisionDetailsRepository revisionDetailsRepository;
    @Autowired
    private ClaimIntimationRepository claimIntimationRepository;

    //Utils
    @Autowired
    DateConversion dateConversion;

    @Autowired
    private ClaimsCommonService claimsCommonService;


    //Default Provisioning for Claims
    public ProvisionDetails defaultProvisioning(ClaimIntimation claimIntimation) {

        String accountBranch = receiptingParametersService.getBaseAccountBranch();
        Product product = productRepository.
                findByCodeAndClassCode(claimIntimation.getProductCode(), claimIntimation.getClassCode());

        ProductPeril productPeril = productPerilRepository.
                findFirstByProductPerilPK_ProductCodeAndStatusOrderBySequenceAsc(claimIntimation.getProductCode(), 'D');

        if(productPeril==null){

            productPeril = productPerilRepository.
                    findFirstByProductPerilPK_ProductCodeAndStatusOrderBySequenceAsc(claimIntimation.getProductCode(),'N');
        }

        ProvisionDetails provisionDetails = new ProvisionDetails();
        String provisionSeqNo = claimsCommonService.generateSequence(ProvisionDetails.SEQUENCE, accountBranch);
        provisionDetails.setProvisionSeqNo(provisionSeqNo);
        provisionDetails.setComments("PROVISION BY ANDROID APP - ASSESSOR");
        provisionDetails.setClaimNo(claimIntimation.getClaimNo());
        provisionDetails.setProvisionValue(product.getDefaultProvision());
        provisionDetails.setIntimationSequenceNo(claimIntimation.getSequenceNo());
        provisionDetails.setProvisionType("TRA000");
        provisionDetails.setFunctionId("1.1.1");
        provisionDetails.setLocationCode(claimIntimation.getLocation());
        provisionDetails.setRiskSeqNo(claimIntimation.getRiskSequenceNo());
        provisionDetails.setRiskOrderNo(claimIntimation.getRiskOrderNo());
        provisionDetails.setProvisionCreatedDate(dateConversion.convertDate(new Date()));
        provisionDetails.setCreatedDate(dateConversion.convertDate(new Date()));
        provisionDetails.setCreatedBy(claimIntimation.getCreatedBy());
        provisionDetails.setPerilCode(productPeril.getPerilCode());
        provisionDetails.setMigratedRecord('N');

        return provisionDetails;
    }

    public List<ProvisionDetails> claimIntimationProvisioning(ClaimIntimation claimIntimation, ClaimIntimationDTO claimIntimationDTO) {

        List<ClaimProvisionDTO> claimProvisionDTOList = claimIntimationDTO.getProvisionList();

        String accountBranch = receiptingParametersService.getBaseAccountBranch();

        ProvisionDetails provisionDetails;
        List<ProvisionDetails> provisionDetailsList = new ArrayList<>();


        for (ClaimProvisionDTO claimProvisionDTO : claimProvisionDTOList) {


            if(claimProvisionDTO.getPeril()!=null && claimProvisionDTO.getProvisionType()!=null && claimProvisionDTO.getProvisionAmount()!=null){

                if(!claimProvisionDTO.getPeril().isEmpty() && !claimProvisionDTO.getProvisionType().isEmpty() && claimProvisionDTO.getProvisionAmount()!=0){

                    provisionDetails = new ProvisionDetails();
                    String provisionSeqNo = claimsCommonService.generateSequence(ProvisionDetails.SEQUENCE, accountBranch);
                    provisionDetails.setProvisionSeqNo(provisionSeqNo);
                    provisionDetails.setComments(claimProvisionDTO.getProvisionComment());
                    provisionDetails.setClaimNo(claimIntimation.getClaimNo());
                    provisionDetails.setProvisionValue(claimProvisionDTO.getProvisionAmount());
                    provisionDetails.setIntimationSequenceNo(claimIntimation.getSequenceNo());
                    provisionDetails.setProvisionType(claimProvisionDTO.getProvisionType());
                    provisionDetails.setFunctionId("1.1.1");
                    provisionDetails.setLocationCode(claimIntimation.getLocation());
                    provisionDetails.setRiskSeqNo(claimIntimation.getRiskSequenceNo());
                    provisionDetails.setRiskOrderNo(claimIntimation.getRiskOrderNo());
                    provisionDetails.setProvisionCreatedDate(dateConversion.convertDate(new Date()));
                    provisionDetails.setCreatedDate(dateConversion.convertDate(new Date()));
                    provisionDetails.setCreatedBy(claimIntimation.getCreatedBy());
                    provisionDetails.setPerilCode(claimProvisionDTO.getPeril());
                    provisionDetails.setMigratedRecord('N');

                    provisionDetailsList.add(provisionDetails);
                }
            }
        }

        return provisionDetailsList;
    }

    // Provisioning for Claims
    public void setProvision(ClaimIntimation claimIntimation, Double provisionAmount, String functionId, String comments) {


        String accountBranch = receiptingParametersService.getBaseAccountBranch();
        Product product = productRepository.
                findByCodeAndClassCode(claimIntimation.getProductCode(), claimIntimation.getClassCode());

        ProductPeril productPeril = productPerilRepository.
                findFirstByProductPerilPK_ProductCodeAndStatusOrderBySequenceAsc(claimIntimation.getProductCode(), 'D');

        if(productPeril==null){


            productPeril = productPerilRepository.findFirstByProductPerilPK_ProductCodeAndStatusOrderBySequenceAsc(
                    claimIntimation.getProductCode(), 'N');
        }

        ProvisionDetails provisionDetails = new ProvisionDetails();

        String provisionSeqNo = claimsCommonService.generateSequence(ProvisionDetails.SEQUENCE, accountBranch);
        provisionDetails.setProvisionSeqNo(provisionSeqNo);
        provisionDetails.setComments(comments);
        provisionDetails.setClaimNo(claimIntimation.getClaimNo());
        provisionDetails.setProvisionValue(provisionAmount);
        provisionDetails.setIntimationSequenceNo(claimIntimation.getSequenceNo());
        provisionDetails.setProvisionType("TRA000");
        provisionDetails.setFunctionId(functionId);
        provisionDetails.setCreatedDate(dateConversion.convertDate(new Date()));
        provisionDetails.setCreatedBy(claimIntimation.getCreatedBy());
        provisionDetails.setLocationCode(claimIntimation.getLocationSeqNo());
        provisionDetails.setPerilCode(productPeril.getPerilCode());
        provisionDetails.setRiskOrderNo(claimIntimation.getRiskOrderNo());

        try {

            provisionDetailsRepository.save(provisionDetails);
        } catch (Exception e) {

            e.printStackTrace();
        }




    }

    public void setRevision(ClaimIntimation claimIntimation, Double revisionAmount) {

        String accountBranch = receiptingParametersService.getBaseAccountBranch();
        String revisionSeqNo = claimsCommonService.generateSequence(RevisionDetails.SEQUENCE, accountBranch);

        ProductPeril productPeril = productPerilRepository.
                findFirstByProductPerilPK_ProductCodeAndStatusOrderBySequenceAsc(claimIntimation.getProductCode(), 'D');

        if(productPeril==null){

            productPeril = productPerilRepository.findFirstByProductPerilPK_ProductCodeAndStatusOrderBySequenceAsc(
                    claimIntimation.getProductCode(),'N');
        }

        RevisionDetails revisionDetails = new RevisionDetails();
        revisionDetails.setRevisionSeqNo(revisionSeqNo);
        revisionDetails.setIntimationSeqNo(claimIntimation.getSequenceNo());
        revisionDetails.setClaimNo(claimIntimation.getClaimNo());
        revisionDetails.setPerilCode(productPeril.getPerilCode());
        revisionDetails.setRevisionType("TRA000");
//        revisionDetails.setMigrated('N');
        revisionDetails.setFunctionId("2.2");
        revisionDetails.setComments("REVISION BY ANDROID APP - ASSESSOR");
        revisionDetails.setRevisionValue(revisionAmount);
        revisionDetails.setRiskOrderNo(claimIntimation.getRiskOrderNo());
        revisionDetails.setLocationCode(claimIntimation.getLocationSeqNo());
        revisionDetails.setCreatedBy(claimIntimation.getCreatedBy());
        revisionDetails.setCreatedDate(dateConversion.convertDate(new Date()));


        try {

            revisionDetailsRepository.save(revisionDetails);
        } catch (Exception e) {

            e.printStackTrace();
        }

    }


    public void changeProvisionAmount(String intimationSequenceNo, Double acrValue) {


        String errorMessage = "";

        ClaimIntimation claimIntimation = claimIntimationRepository.findBySequenceNo(intimationSequenceNo);

        if (claimIntimation != null) {

            HashSet<ProvisionDetails> provisionDetailsSet = provisionDetailsRepository.findAllByIntimationSequenceNo(intimationSequenceNo);

            HashSet<RevisionDetails> revisionDetailsSet = revisionDetailsRepository.findAllByIntimationSeqNo(intimationSequenceNo);

            Double provisionAmount = 0.00;
            Double revisionAmount = 0.00;
            Double currentValue = 0.00;

            if (provisionDetailsSet != null) {

                for (ProvisionDetails p : provisionDetailsSet) {

                    provisionAmount += p.getProvisionValue();
                }
            }

            if (revisionDetailsSet != null) {

                for (RevisionDetails r : revisionDetailsSet) {

                    revisionAmount += r.getRevisionValue();
                }
            }

            currentValue = provisionAmount - revisionAmount;

            if (acrValue - currentValue > 0.00) {


                String comment = "PROVISION BY ANDROID APP -ASSESSOR";

                setProvision(claimIntimation, acrValue - currentValue, "2.2", comment);

            } else if (acrValue - currentValue < 0.00) {

                setRevision(claimIntimation, currentValue - acrValue);
            }
        } else {

            errorMessage = "The Claim No does not exists";
        }
    }

    public Double getEstimatedAmount(String intimationSequenceNo) {

        List<String> functionIdList = new ArrayList<>(Arrays.asList("1.1", "1.1.1", "1.1.2"));

        List<ProvisionDetails> provisionDetailsList = provisionDetailsRepository.findAllByIntimationSequenceNoAndProvisionTypeAndFunctionIdIn(
                intimationSequenceNo, "TRA000", functionIdList);

        Optional<Double> estimatedAmount = provisionDetailsList.stream()
                .map(ProvisionDetails::getProvisionValue)
                .reduce((x, y) -> x + y);

        return estimatedAmount.orElse(0.00);
    }
}
