package com.infoinstech.claimsmodule.services.business;

import com.infoinstech.claimsmodule.dto.claimintimation.CustomerContactDTO;
import com.infoinstech.claimsmodule.domain.model.underwriting.master.Customer;
import com.infoinstech.claimsmodule.domain.model.underwriting.master.CustomerAddress;
import com.infoinstech.claimsmodule.domain.model.underwriting.master.CustomerContact;
import com.infoinstech.claimsmodule.domain.repository.underwriting.master.CustomerAddressRepository;
import com.infoinstech.claimsmodule.domain.repository.underwriting.master.CustomerContactRepository;
import com.infoinstech.claimsmodule.services.util.Formatter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class CustomerService {

    @Autowired
    private CustomerAddressRepository customerAddressRepository;
    @Autowired
    private Formatter formatter;

    public String getCustomerName(Customer customer) {

        String name = "";

        if (customer.getCustomerType() == 'I') {

            String title = customer.getIndvTitle() == null ? "" : customer.getIndvTitle() + " ";
            String initials = customer.getIndvInitials() == null ? "" : customer.getIndvInitials() + " ";
            String otherNames = customer.getIndvOtherNames() == null ? "" : customer.getIndvOtherNames() + " ";
            String surname = customer.getIndvSurname() == null ? "" : customer.getIndvSurname();

            name = title + initials + otherNames + surname;
        } else {

            name = customer.getCorpName();

        }

        return name;
    }

    public CustomerContactDTO getCustomerContact(Customer customer) {

        CustomerContactDTO customerContactDTO = null;

        List<CustomerContact> customerContacts = customer.getCustomerContactList();

        CustomerContact customerContact = null;

        for (CustomerContact c : customerContacts) {

            if(c.getPhoneNo()!=null){

                customerContact = c;
                break;
            }
        }

        if (customerContact == null) {

            customerContactDTO = new CustomerContactDTO();
            customerContactDTO.setName(getCustomerName(customer));
            customerContactDTO.setContact((customer.getPhoneNo1()!=null) ? customer.getPhoneNo1() : customer.getPhoneNo2());
            customerContactDTO.setEmail(customer.getEmailAddress());


        } else {

            customerContactDTO = new CustomerContactDTO();
            customerContactDTO.setName(customerContact.getContactName());
            customerContactDTO.setEmail(customerContact.getEmailAddress());
            customerContactDTO.setContact(customerContact.getPhoneNo());
        }

        customerContactDTO.setAddress(getCustomerAddress(customer));

        return customerContactDTO;
    }

    public String getCustomerNIC(Customer customer) {

        String nic = "-";

        if (customer.getCustomerType() == 'I' && customer.getIndvNIC()!=null) nic = customer.getIndvNIC();

        return nic;
    }

    public String getCustomerAddress(Customer customer) {


        CustomerAddress customerAddress = customerAddressRepository.findByCustomerCodeAndDefaultAddress(customer.getCustomerCode(),'Y');

        return (customerAddress==null)? "" : customerAddress.getLocationDescription();
    }

    public String getCustomerLastName(Customer customer) {

        String name = "";

        if (customer.getCustomerType() == 'I') {

            String surname = customer.getIndvSurname() == null ? "" : customer.getIndvSurname();

            name = surname;
        } else {

            name = customer.getCorpName();

        }

        return name;
    }

    public String getCustomerOtherNames(Customer customer) {

        String name = "";

        if (customer.getCustomerType() == 'I') {

            String otherNames = customer.getIndvOtherNames() == null ? "" : customer.getIndvOtherNames() + " ";

            name = otherNames;
        } else {

            name = customer.getCorpName();

        }

        return name;
    }

}
