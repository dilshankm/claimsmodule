package com.infoinstech.claimsmodule.services.business;

import com.infoinstech.claimsmodule.dto.claiminquiry.JobAssignmentDTO;
import com.infoinstech.claimsmodule.dto.dashboard.AccidentDTO;
import com.infoinstech.claimsmodule.dto.dashboard.AssignmentCountDTO;
import com.infoinstech.claimsmodule.dto.dashboard.DashboardRequestDTO;
import com.infoinstech.claimsmodule.dto.dashboard.DashboardResponseDTO;
import com.infoinstech.claimsmodule.domain.model.claims.master.ExternalPerson;
import com.infoinstech.claimsmodule.domain.model.claims.transaction.*;
import com.infoinstech.claimsmodule.domain.repository.claims.master.ExternalPersonRepository;
import com.infoinstech.claimsmodule.domain.repository.claims.transaction.*;
import com.infoinstech.claimsmodule.services.reference.ReferenceService;
import com.infoinstech.claimsmodule.services.util.DateCalculation;
import com.infoinstech.claimsmodule.services.util.DateConversion;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.*;
import java.util.stream.Collectors;

@Service
public class DashboardService {

    @Autowired
    private ExternalPersonRepository externalPersonRepository;
    @Autowired
    private AssignLossAdjusterRepository assignLossAdjusterRepository;
    @Autowired
    private NotificationLossAdjusterRepository notificationLossAdjusterRepository;
    private NotificationAssignmentRepository notificationAssignmentRepository;
    @Autowired
    private ClaimNotificationRepository claimNotificationRepository;

    @Autowired
    private DateCalculation dateCalculation;
    @Autowired
    private DateConversion dateConversion;

    @Autowired
    private ReferenceService referenceService;
    @Autowired
    private ExternalPersonService externalPersonService;
    @Autowired
    private QuestionnaireService questionnaireService;


    public List<AccidentDTO> getActiveAccidents() {

        List<AccidentDTO> activeAccidents = new ArrayList<>();
        AccidentDTO accidentDTO;

        List<Character> statuses = Arrays.asList('A', 'N');

        List<AssignLossAdjuster> lossAdjusters = assignLossAdjusterRepository.findByStatusIsIn(statuses);
        List<NotificationLossAdjuster> notificationLossAdjusters = notificationLossAdjusterRepository.findByStatusIsIn(statuses);

        if (lossAdjusters != null) {

            for (AssignLossAdjuster adjuster : lossAdjusters) {

                ClaimIntimation claimIntimation = adjuster.getClaimIntimation();
                JobAssignment jobAssignment = adjuster.getJobAssignment();

                if (claimIntimation.getLatitude() == null && jobAssignment.getLatitude() == null) continue;

                accidentDTO = new AccidentDTO();
                accidentDTO.setAssessorCode(adjuster.getExternalPersonCode());
                accidentDTO.setLocation(claimIntimation.getPlaceOfLoss());

                if (jobAssignment.getLatitude() == null) {

                    accidentDTO.setLatitude(claimIntimation.getLatitude());
                    accidentDTO.setLongitude(claimIntimation.getLongitude());
                } else {

                    accidentDTO.setLatitude(jobAssignment.getLatitude());
                    accidentDTO.setLongitude(jobAssignment.getLongitude());

                }

                accidentDTO.setClaimNo(claimIntimation.getClaimNo());
                accidentDTO.setVehicleNo(claimIntimation.getRiskName());
                accidentDTO.setContactNo(claimIntimation.getContactNo());

                activeAccidents.add(accidentDTO);

            }
        }

        if (notificationLossAdjusters != null) {

            for (NotificationLossAdjuster lossAdjuster : notificationLossAdjusters) {

                ClaimNotification claimNotification = lossAdjuster.getClaimNotification();
                NotificationAssignment notificationAssignment = lossAdjuster.getNotificationAssignment();

                if (claimNotification == null && notificationAssignment == null) continue;

                accidentDTO = new AccidentDTO();
                accidentDTO.setAssessorCode(lossAdjuster.getExternalPersonCode());
                accidentDTO.setLocation(claimNotification.getPlaceOfLoss());

                if (notificationAssignment.getLatitude() != null) {

                    accidentDTO.setLatitude(notificationAssignment.getLatitude());
                    accidentDTO.setLongitude(notificationAssignment.getLongitude());
                } else {

                    accidentDTO.setLatitude(claimNotification.getLatitude());
                    accidentDTO.setLongitude(claimNotification.getLongitude());
                }

                accidentDTO.setClaimNo(claimNotification.getNotificationNo());
                accidentDTO.setVehicleNo(claimNotification.getRiskName());
                accidentDTO.setContactNo(claimNotification.getContactNo());

                activeAccidents.add(accidentDTO);


            }
        }


        return activeAccidents;
    }

    public DashboardResponseDTO getDashboardDetails(DashboardRequestDTO dashboardRequestDTO) {

        List<AssignLossAdjuster> assignLossAdjusters;
        List<NotificationLossAdjuster> notificationLossAdjusters;
        List<JobAssignmentDTO> jobAssignmentDTOList = new ArrayList<>();
        AssignmentCountDTO assignmentCountDTO;

        DashboardResponseDTO dashboardResponseDTO = new DashboardResponseDTO();

        if (dashboardRequestDTO.getAssessorCode() == null || dashboardRequestDTO.getAssessorCode().equals("")) {

            if (dashboardRequestDTO.getStatus().equals("A") || dashboardRequestDTO.getStatus().equals("N")) {

                assignLossAdjusters = assignLossAdjusterRepository.findByStatus(dashboardRequestDTO.getStatus().charAt(0));
                notificationLossAdjusters = notificationLossAdjusterRepository.findByStatus(dashboardRequestDTO.getStatus().charAt(0));
                assignmentCountDTO = getAssignmentCount(null, null, null);

                if (assignLossAdjusters != null) jobAssignmentDTOList = getJobAssignments(assignLossAdjusters);
                if (notificationLossAdjusters != null) jobAssignmentDTOList.addAll(getNotificationAssignments(notificationLossAdjusters));

                dashboardResponseDTO.setAssignmentCount(assignmentCountDTO);
                dashboardResponseDTO.setJobAssignments(jobAssignmentDTOList);

            } else {

                Date fromDate = dateConversion.convertStringToDate(dashboardRequestDTO.getFromDate(), "yyyy-MMM-dd");
                Date toDate = dateCalculation.addDays(dateConversion.convertStringToDate(dashboardRequestDTO.getToDate(), "yyyy-MMM-dd"), 1);

                assignLossAdjusters = assignLossAdjusterRepository.findByCreatedDateBetweenAndStatus(fromDate, toDate , dashboardRequestDTO.getStatus().charAt(0));
                notificationLossAdjusters = notificationLossAdjusterRepository.findByCreatedDateBetweenAndStatus(fromDate, toDate, dashboardRequestDTO.getStatus().charAt(0));
                assignmentCountDTO = getAssignmentCount(null, fromDate, toDate);

                if (assignLossAdjusters != null) jobAssignmentDTOList = getJobAssignments(assignLossAdjusters);
                if (notificationLossAdjusters != null) jobAssignmentDTOList.addAll(getNotificationAssignments(notificationLossAdjusters));

                jobAssignmentDTOList.sort(Comparator.comparing(JobAssignmentDTO::getAppointedDate).reversed());

                dashboardResponseDTO.setAssignmentCount(assignmentCountDTO);
                dashboardResponseDTO.setJobAssignments(jobAssignmentDTOList);

            }

        } else {

            if (dashboardRequestDTO.getStatus().equals("A") || dashboardRequestDTO.getStatus().equals("N")) {

                assignLossAdjusters = assignLossAdjusterRepository.findByExternalPersonCodeAndStatus(
                        dashboardRequestDTO.getAssessorCode(), dashboardRequestDTO.getStatus().charAt(0));

                notificationLossAdjusters = notificationLossAdjusterRepository.findByExternalPersonCodeAndStatus(
                        dashboardRequestDTO.getAssessorCode(), dashboardRequestDTO.getStatus().charAt(0));

                assignmentCountDTO = getAssignmentCount(dashboardRequestDTO.getAssessorCode(), null, null);

                if (assignLossAdjusters != null) jobAssignmentDTOList = getJobAssignments(assignLossAdjusters);
                if (notificationLossAdjusters != null) jobAssignmentDTOList.addAll(getNotificationAssignments(notificationLossAdjusters));


                dashboardResponseDTO.setAssignmentCount(assignmentCountDTO);
                dashboardResponseDTO.setJobAssignments(jobAssignmentDTOList);

            } else {

                Date fromDate = dateConversion.convertStringToDate(dashboardRequestDTO.getFromDate(), "yyyy-MMM-dd");
                Date toDate = dateCalculation.addDays(dateConversion.convertStringToDate(dashboardRequestDTO.getToDate(), "yyyy-MMM-dd"), 1);

                assignLossAdjusters = assignLossAdjusterRepository.findByExternalPersonCodeAndCreatedDateBetweenAndStatus(
                        dashboardRequestDTO.getAssessorCode(), fromDate, toDate, dashboardRequestDTO.getStatus().charAt(0));

                notificationLossAdjusters = notificationLossAdjusterRepository.findByExternalPersonCodeAndCreatedDateBetweenAndStatus(
                        dashboardRequestDTO.getAssessorCode(), fromDate, toDate, dashboardRequestDTO.getStatus().charAt(0));

                assignmentCountDTO = getAssignmentCount(dashboardRequestDTO.getAssessorCode(), dateConversion.convertStringToDate(dashboardRequestDTO.getFromDate(), "yy-MMM-dd"),
                        dateCalculation.addDays(dateConversion.convertStringToDate(dashboardRequestDTO.getToDate(), "yyyy-MMM-dd"), 1));

                if (assignLossAdjusters != null) jobAssignmentDTOList = getJobAssignments(assignLossAdjusters);
                if (notificationLossAdjusters != null) jobAssignmentDTOList.addAll(getNotificationAssignments(notificationLossAdjusters));


                dashboardResponseDTO.setAssignmentCount(assignmentCountDTO);
                dashboardResponseDTO.setJobAssignments(jobAssignmentDTOList);
            }

        }

        return dashboardResponseDTO;

    }

    private AssignmentCountDTO getAssignmentCount(String assessorCode, Date fromDate, Date toDate) {

        AssignmentCountDTO assignmentCountDTO = new AssignmentCountDTO();

        long assignedCount;
        long acceptedCount;
        long rejectedCount;
        long completedCount;


        boolean assessorGiven = assessorCode != null;

        toDate = (toDate != null) ? dateCalculation.addDays(toDate, 1) : new Date();
        fromDate = (fromDate != null) ? fromDate : dateCalculation.addDays(toDate, -7);


        if (assessorGiven) {

            assignedCount = assignLossAdjusterRepository.countByExternalPersonCodeAndStatus(assessorCode, 'N');
            acceptedCount =  assignLossAdjusterRepository.countByExternalPersonCodeAndStatus(assessorCode, 'A');
            rejectedCount =  assignLossAdjusterRepository.countByExternalPersonCodeAndCreatedDateIsBetweenAndStatus(assessorCode, fromDate, toDate, 'R');
            completedCount = assignLossAdjusterRepository.countByExternalPersonCodeAndCreatedDateIsBetweenAndStatus(assessorCode, fromDate, toDate, 'C');

            assignedCount += notificationLossAdjusterRepository.countByExternalPersonCodeAndStatus(assessorCode,'N');
            acceptedCount += notificationLossAdjusterRepository.countByExternalPersonCodeAndStatus(assessorCode,'A');
            rejectedCount += notificationLossAdjusterRepository.countByExternalPersonCodeAndCreatedDateBetweenAndStatus(assessorCode, fromDate, toDate,'R');
            completedCount += notificationLossAdjusterRepository.countByExternalPersonCodeAndCreatedDateBetweenAndStatus(assessorCode, fromDate, toDate, 'C');


        } else {

            assignedCount = assignLossAdjusterRepository.countByStatus('N');
            acceptedCount = assignLossAdjusterRepository.countByStatus('A');
            rejectedCount = assignLossAdjusterRepository.countByCreatedDateIsBetweenAndStatus(fromDate, toDate, 'R');
            completedCount = assignLossAdjusterRepository.countByCreatedDateIsBetweenAndStatus(fromDate, toDate, 'C');

            assignedCount += notificationLossAdjusterRepository.countByStatus('N');
            acceptedCount += notificationLossAdjusterRepository.countByStatus('Y');
            rejectedCount += notificationLossAdjusterRepository.countByCreatedDateBetweenAndStatus(fromDate, toDate,'R');
            completedCount += notificationLossAdjusterRepository.countByCreatedDateBetweenAndStatus(fromDate, toDate, 'C');

        }

        assignmentCountDTO.setAssigned(assignedCount);
        assignmentCountDTO.setAccepted(acceptedCount);
        assignmentCountDTO.setRejected(rejectedCount);
        assignmentCountDTO.setCompleted(completedCount);
        assignmentCountDTO.setFromDate(fromDate);
        assignmentCountDTO.setToDate(toDate);

        return assignmentCountDTO;

    }


    private List<JobAssignmentDTO> getJobAssignments(List<AssignLossAdjuster> lossAdjusters) {

         return lossAdjusters.stream().map(

                        assignLossAdjuster -> {

                            JobAssignment jobAssignment = assignLossAdjuster.getJobAssignment();
                            ExternalPerson externalPerson = externalPersonRepository.findByCode(assignLossAdjuster.getExternalPersonCode());
                            ClaimIntimation claimIntimation = assignLossAdjuster.getClaimIntimation();

                            JobAssignmentDTO jobAssignmentDTO = new JobAssignmentDTO();
                            jobAssignmentDTO.setSequenceNo(jobAssignment.getJobAssignmentPK().getSequenceNo());
                            jobAssignmentDTO.setJobNo(jobAssignment.getJobNo());
                            jobAssignmentDTO.setTypeCode(jobAssignment.getSurveyCode());
                            jobAssignmentDTO.setType(jobAssignment.getSurveyType().getSurveyDescription());
                            String jobStatus = referenceService.getJobStatusDescriptionByCode((assignLossAdjuster.getStatus() == null) ? "" : String.valueOf(assignLossAdjuster.getStatus()));
                            jobAssignmentDTO.setStatus((jobStatus == null || jobStatus.equals("")) ? "CORE" : jobStatus);
                            jobAssignmentDTO.setExternalPersonCode(assignLossAdjuster.getExternalPersonCode());
                            jobAssignmentDTO.setExternalPersonName(externalPersonService.getExternalPersonName(externalPerson));
                            jobAssignmentDTO.setPromiseTime((assignLossAdjuster.getPromiseTime() == null) ? "" : assignLossAdjuster.getPromiseTime());
                            jobAssignmentDTO.setAppointedDate(dateConversion.convertFromDateToString(assignLossAdjuster.getAppointedDate()));
                            jobAssignmentDTO.setVisitDueDate(dateConversion.convertFromDateToString(assignLossAdjuster.getVisitDueDate()));
                            jobAssignmentDTO.setReportDueDate(dateConversion.convertFromDateToString(assignLossAdjuster.getReportDueDate()));
                            jobAssignmentDTO.setReportSubmittedDate(dateConversion.convertFromDateToString(assignLossAdjuster.getSubmittedDate()));
                            jobAssignmentDTO.setQuestionnaire(questionnaireService.viewQuestionnaire(jobAssignment, claimIntimation));
                            jobAssignmentDTO.setClaimNo(claimIntimation.getClaimNo());

                            return jobAssignmentDTO;

                        }
                ).sorted(Comparator.comparing(JobAssignmentDTO::getAppointedDate).reversed())
                        .collect(Collectors.toList());

    }

    private List<JobAssignmentDTO> getNotificationAssignments(List<NotificationLossAdjuster> notificationLossAdjusters) {


        return notificationLossAdjusters.stream().map(

                        notificationLossAdjuster -> {

                            NotificationAssignment notificationAssignment = notificationLossAdjuster.getNotificationAssignment();
                            ExternalPerson externalPerson = externalPersonRepository.findByCode(notificationLossAdjuster.getExternalPersonCode());
                            ClaimNotification claimNotification = notificationLossAdjuster.getClaimNotification();

                            JobAssignmentDTO jobAssignmentDTO = new JobAssignmentDTO();
                            jobAssignmentDTO.setSequenceNo(notificationAssignment.getNotificationAssignmentPK().getSequenceNo());
                            jobAssignmentDTO.setJobNo(notificationAssignment.getJobNo());
                            jobAssignmentDTO.setTypeCode(notificationAssignment.getSurveryCode());
                            jobAssignmentDTO.setType(notificationAssignment.getRefSurveyType().getSurveyDescription());
                            String jobStatus = referenceService.getJobStatusDescriptionByCode((notificationLossAdjuster.getStatus() == null) ? "" : String.valueOf(notificationAssignment.getStatus()));
                            jobAssignmentDTO.setStatus((jobStatus == null || jobStatus.equals("")) ? "CORE" : jobStatus);
                            jobAssignmentDTO.setExternalPersonCode(notificationLossAdjuster.getExternalPersonCode());
                            jobAssignmentDTO.setExternalPersonName(externalPersonService.getExternalPersonName(externalPerson));
                            jobAssignmentDTO.setPromiseTime((notificationLossAdjuster.getPromiseTime() == null) ? "" : notificationLossAdjuster.getPromiseTime());
                            jobAssignmentDTO.setAppointedDate(dateConversion.convertFromDateToString(notificationLossAdjuster.getAppointedDate()));
                            jobAssignmentDTO.setVisitDueDate(dateConversion.convertFromDateToString(notificationLossAdjuster.getVisitDueDate()));
                            jobAssignmentDTO.setReportDueDate(dateConversion.convertFromDateToString(notificationLossAdjuster.getReportDueDate()));
                            jobAssignmentDTO.setReportSubmittedDate(dateConversion.convertFromDateToString(notificationLossAdjuster.getSubmittedDate()));
                            jobAssignmentDTO.setQuestionnaire(questionnaireService.viewQuestionnaire(notificationAssignment, claimNotification));
                            jobAssignmentDTO.setClaimNo(claimNotification.getNotificationNo());

                            return jobAssignmentDTO;

                        }
                ).sorted(Comparator.comparing(JobAssignmentDTO::getAppointedDate).reversed())
                        .collect(Collectors.toList());

    }


}
