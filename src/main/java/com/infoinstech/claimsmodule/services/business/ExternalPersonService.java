package com.infoinstech.claimsmodule.services.business;

import com.infoinstech.claimsmodule.dto.common.AutoMotiveDetailsDTO;
import com.infoinstech.claimsmodule.dto.common.Response;
import com.infoinstech.claimsmodule.dto.deviceregistration.ActiveAssessor;
import com.infoinstech.claimsmodule.dto.deviceregistration.LocationUpdate;
import com.infoinstech.claimsmodule.domain.model.claims.master.ExternalPerson;
import com.infoinstech.claimsmodule.domain.model.claims.master.ExternalPersonDevice;
import com.infoinstech.claimsmodule.dto.deviceregistration.Assessor;
import com.infoinstech.claimsmodule.dto.deviceregistration.AssessorDevice;
import com.infoinstech.claimsmodule.domain.model.claims.transaction.DeviceStatusLog;
import com.infoinstech.claimsmodule.domain.model.claims.transaction.DeviceStatusLogPK;
import com.infoinstech.claimsmodule.domain.packages.PK_CL_TAB_SEQUENCE;
import com.infoinstech.claimsmodule.domain.repository.claims.master.ExternalPersonDeviceRepository;
import com.infoinstech.claimsmodule.domain.repository.claims.master.ExternalPersonRepository;
import com.infoinstech.claimsmodule.domain.repository.claims.transaction.DeviceStatusLogRepository;
import com.infoinstech.claimsmodule.exceptions.types.AssessorException;
import com.infoinstech.claimsmodule.exceptions.types.DeviceRegisteredException;
import com.infoinstech.claimsmodule.exceptions.types.DeviceTokenNotFoundException;
import com.infoinstech.claimsmodule.exceptions.types.NoDeviceRegisteredException;
import com.infoinstech.claimsmodule.services.common.ClaimsCommonService;
import com.infoinstech.claimsmodule.services.parameters.ClaimParametersService;
import com.infoinstech.claimsmodule.services.parameters.ReceiptingParametersService;
import com.infoinstech.claimsmodule.services.util.DateConversion;
import com.infoinstech.claimsmodule.services.util.Formatter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.*;
import java.util.stream.Collectors;

@Service
public class ExternalPersonService {

    // Repositories
    @Autowired
    private ExternalPersonRepository externalPersonRepository;
    @Autowired
    private ExternalPersonDeviceRepository externalPersonDeviceRepository;
    @Autowired
    private DeviceStatusLogRepository deviceStatusLogRepository;
    // Utilities
    @Autowired
    DateConversion dateConversion;
    @Autowired
    Formatter formatter;

    //Parameters
    @Autowired
    private ClaimParametersService claimParametersService;
    @Autowired
    private ReceiptingParametersService receiptingParametersService;
    @Autowired
    private ClaimsCommonService claimsCommonService;

    //DB Packages
    @Autowired
    private PK_CL_TAB_SEQUENCE pk_cl_tab_sequence;

    private static final int INITIAL_DEVICE_TOKEN = 8765431;

    private static final List<String> DEVICE_STATUS = Arrays.asList("ACTIVE", "INACTIVE", "PENDING");

    public List<Assessor> getUnregisteredAssessors(){

        List<Assessor> assessorList = new ArrayList<>();

        List<ExternalPerson> externalPersons = externalPersonRepository.findByCategoryCodeAndStatus("CAT08",'A');

        if(externalPersons!=null){

            for(ExternalPerson externalPerson : externalPersons){

                ExternalPersonDevice externalPersonDevice = externalPersonDeviceRepository.findByExternalPersonCodeAndRegistered(externalPerson.getCode(),'Y');

                if(externalPersonDevice==null){

                    Assessor assessor = new Assessor();
                    assessor.setCode(externalPerson.getCode());
                    assessor.setName(getExternalPersonName(externalPerson));
                    assessor.setAddress((externalPerson.getAddress()==null)? "-" : externalPerson.getAddress());
                    assessor.setContact1((externalPerson.getContact1()==null)? "-" : externalPerson.getContact1());
                    assessor.setContact2((externalPerson.getContact2()==null)? "-" : externalPerson.getContact2());

                    assessorList.add(assessor);
                }
            }


        }

        assessorList.sort(Comparator.comparing(Assessor::getName));

        return assessorList;
    }

    public List<ActiveAssessor> getCoreAssessorsList(){

        List<ExternalPerson> externalPersons = externalPersonRepository.findByCategoryCodeAndStatus("CAT08",'A');

        List<ActiveAssessor> activeAssessors = new ArrayList<>();

        if(externalPersons!=null){


            ActiveAssessor activeAssessor;

            for(ExternalPerson externalPerson : externalPersons){

                ExternalPersonDevice externalPersonDevice = externalPersonDeviceRepository.findByExternalPersonCodeAndRegistered(externalPerson.getCode(),'Y');

                if(externalPersonDevice==null){

                    activeAssessor = new ActiveAssessor();
                    activeAssessor.setCode(externalPerson.getCode());
                    activeAssessor.setName(getExternalPersonName(externalPerson));
                    activeAssessor.setLatitude("-");
                    activeAssessor.setLongitude("-");
                    activeAssessor.setStatus("-");

                    if(externalPerson.getContact1()!=null){

                        activeAssessor.setContactNo(externalPerson.getContact1());
                    }
                    else {

                        activeAssessor.setContactNo(externalPerson.getContact2());
                    }

                    activeAssessors.add(activeAssessor);

                }
            }

            activeAssessors.sort(Comparator.comparing(ActiveAssessor::getName));
        }


        return activeAssessors;
    }

    public List<ActiveAssessor> getActiveAssessorDeviceList(){

        List<Character> statusList = new ArrayList<>(Arrays.asList('A','I','N'));

        List<ExternalPersonDevice> externalPersonDevices = externalPersonDeviceRepository.
                findByRegisteredAndStatusIn('Y',statusList);

        List<ActiveAssessor> activeAssessors = new ArrayList<>();

        if(externalPersonDevices!=null){


            ActiveAssessor activeAssessor;

            for(ExternalPersonDevice externalPersonDevice : externalPersonDevices){

                ExternalPerson externalPerson = externalPersonRepository.findByCode(externalPersonDevice.getExternalPersonCode());

                activeAssessor = new ActiveAssessor();
                activeAssessor.setCode(externalPerson.getCode());
                activeAssessor.setName(getExternalPersonName(externalPerson));
                activeAssessor.setLatitude(externalPersonDevice.getLatitude().toString());
                activeAssessor.setLongitude(externalPersonDevice.getLongitude().toString());
                activeAssessor.setStatus(getExternalPersonDeviceStatus(externalPersonDevice.getStatus()));

                if(externalPerson.getContact1()!=null){

                    activeAssessor.setContactNo(externalPerson.getContact1());
                }
                else {

                    activeAssessor.setContactNo(externalPerson.getContact2());
                }

                activeAssessors.add(activeAssessor);

            }

            activeAssessors.sort(Comparator.comparingInt(o -> DEVICE_STATUS.indexOf(o.getStatus())));
        }


        return activeAssessors;
    }

    public List<AssessorDevice> getRegisteredAssessorDeviceList(){

        List<ExternalPerson> assessors = externalPersonRepository.findByCategoryCodeAndStatus("CAT08",'A');

        List<AssessorDevice> registeredAssessorDeviceList = new ArrayList<>();

        if(assessors!=null){

            registeredAssessorDeviceList =

                    assessors.stream()
                            .filter( e ->  {

                                ExternalPersonDevice externalPersonDevice =
                                        externalPersonDeviceRepository.findByExternalPersonCodeAndRegistered(e.getCode(),'Y');
                                return externalPersonDevice != null;
                            })
                            .map( e -> {

                                        AssessorDevice assessorDevice = new AssessorDevice();
                                        ExternalPersonDevice externalPersonDevice =
                                                externalPersonDeviceRepository.findByExternalPersonCodeAndRegistered(e.getCode(),'Y');
                                        assessorDevice.setDeviceToken(externalPersonDevice.getDeviceToken());
                                        assessorDevice.setCode(externalPersonDevice.getExternalPersonCode());
                                        assessorDevice.setName(getExternalPersonName(e));
                                        assessorDevice.setRegisteredDate(dateConversion.convertFromDateToString(externalPersonDevice.getCreatedDate()));
                                        assessorDevice.setStatus(getExternalPersonDeviceStatus(externalPersonDevice.getStatus()));
                                        assessorDevice.setLastUpdatedTime(dateConversion.convertFromDateToString(externalPersonDevice.getLastUpdatedTime()));

                                        return assessorDevice;
                                    }
                            )
                            .sorted(Comparator.comparing(AssessorDevice::getName))
                            .collect(Collectors.toList());
        }

        return registeredAssessorDeviceList;
    }



    public String registerAssessorDevice(String assessorCode){
        String token = "";
        ExternalPerson assessor = externalPersonRepository.findByCode(assessorCode);
        ExternalPersonDevice assessorDevice = externalPersonDeviceRepository.findByExternalPersonCodeAndRegistered(assessorCode,'Y');
        if (assessorDevice==null){
            assessorDevice = new ExternalPersonDevice();
            String sequenceNo = claimsCommonService.generateSequence(ExternalPersonDevice.SEQUENCE,receiptingParametersService.getBaseAccountBranch());
            assessorDevice.setSequenceNo(sequenceNo);
            assessorDevice.setExternalPersonCode(assessor.getCode());
            assessorDevice.setDeviceToken(generateDeviceToken());
            assessorDevice.setStatus('P');
            assessorDevice.setRegistered('Y');
            assessorDevice.setCreatedBy("");
            assessorDevice.setCreatedDate(new Date());
            assessorDevice.setModifiedBy("");
            assessorDevice.setModifiedDate(new Date());
            externalPersonDeviceRepository.save(assessorDevice);
            token = assessorDevice.getDeviceToken();
        }
        else {
           throw new DeviceRegisteredException("The device has been already registered");
        }
        return token;
    }

    public ExternalPersonDevice unregisterAssessorDevice(String assessorCode){
        ExternalPersonDevice assessorDevice =
                externalPersonDeviceRepository.findByExternalPersonCodeAndRegistered(assessorCode,'Y');
        if(assessorDevice!=null){
            assessorDevice.setRegistered('D');
            assessorDevice.setUnregisteredDate(new Date());
            assessorDevice.setUnregisteredBy("");
            assessorDevice.setUnregisteredDate(new Date());
            assessorDevice.setModifiedBy("");
            externalPersonDeviceRepository.save(assessorDevice);
            return assessorDevice;
        }
        else {
            throw new NoDeviceRegisteredException("No device has been registered for the given Assessor");
        }
    }

    public Response updateDeviceLocation(LocationUpdate locationUpdate){
        Response response = null;
        ExternalPersonDevice externalPersonDevice = externalPersonDeviceRepository.findByDeviceToken(locationUpdate.getDeviceToken());
        String gcmKey  = (externalPersonDevice == null) ? ""  : externalPersonDevice.getGcmKey();
        if(externalPersonDevice!=null){
                if(gcmKey == null || gcmKey.isEmpty()){
                    externalPersonDevice.setGcmKey(locationUpdate.getGcmKey());
                    externalPersonDevice.setLastUpdatedTime(dateConversion.convertDate(new Date()));
                    externalPersonDevice.setLatitude(Double.parseDouble(locationUpdate.getLatitude()));
                    externalPersonDevice.setLongitude(Double.parseDouble(locationUpdate.getLongitude()));
                    externalPersonDevice.setStatus('A');
                    DeviceStatusLog deviceStatusLog = new DeviceStatusLog();
                    DeviceStatusLogPK deviceStatusLogPK = new DeviceStatusLogPK();
                    deviceStatusLogPK.setSequenceNo(claimsCommonService.generateSequence(DeviceStatusLog.SEQUENCE,receiptingParametersService.getBaseAccountBranch()));
                    deviceStatusLogPK.setAssessorCode(externalPersonDevice.getExternalPersonCode());
                    deviceStatusLogPK.setDeviceSequenceNo(externalPersonDevice.getSequenceNo());
                    deviceStatusLog.setDeviceStatusLogPK(deviceStatusLogPK);
                    deviceStatusLog.setDeviceToken(Integer.parseInt(externalPersonDevice.getDeviceToken()));
                    deviceStatusLog.setLastStatus(getExternalPersonDeviceStatus(externalPersonDevice.getStatus()));
                    deviceStatusLog.setCurrentStatus("ACTIVE");
                    deviceStatusLog.setCurrentUpdatedTime(dateConversion.convertDate(new Date()));
                    deviceStatusLog.setLogOrder(1);
                    deviceStatusLog.setLatitude(locationUpdate.getLatitude());
                    deviceStatusLog.setLongitude(locationUpdate.getLongitude());
                    deviceStatusLogRepository.save(deviceStatusLog);
                    externalPersonDeviceRepository.save(externalPersonDevice);
                    response = new Response(200, true, "The device was successfully registered");
                }
                else if(gcmKey.equals(locationUpdate.getGcmKey())){
                    DeviceStatusLog deviceStatusLog = new DeviceStatusLog();
                    DeviceStatusLog lastStatus = deviceStatusLogRepository.
                            findFirstByDeviceStatusLogPK_AssessorCodeAndDeviceStatusLogPK_DeviceSequenceNoAndDeviceTokenOrderByLogOrderDesc(
                                    externalPersonDevice.getExternalPersonCode(),externalPersonDevice.getSequenceNo(), Integer.parseInt(externalPersonDevice.getDeviceToken()));
                    DeviceStatusLogPK deviceStatusLogPK = new DeviceStatusLogPK();
                    deviceStatusLogPK.setSequenceNo(claimsCommonService.generateSequence(DeviceStatusLog.SEQUENCE,receiptingParametersService.getBaseAccountBranch()));
                    deviceStatusLogPK.setAssessorCode(externalPersonDevice.getExternalPersonCode());
                    deviceStatusLogPK.setDeviceSequenceNo(externalPersonDevice.getSequenceNo());
                    deviceStatusLog.setDeviceStatusLogPK(deviceStatusLogPK);
                    deviceStatusLog.setDeviceToken(Integer.parseInt(externalPersonDevice.getDeviceToken()));
                    deviceStatusLog.setLastStatus(getExternalPersonDeviceStatus(externalPersonDevice.getStatus()));
                    deviceStatusLog.setCurrentStatus("ACTIVE");
                    deviceStatusLog.setLastUpdatedTime(externalPersonDevice.getLastUpdatedTime());
                    deviceStatusLog.setCurrentUpdatedTime(dateConversion.convertDate(new Date()));
                    deviceStatusLog.setLatitude(locationUpdate.getLatitude());
                    deviceStatusLog.setLongitude(locationUpdate.getLongitude());
                    externalPersonDevice.setLastUpdatedTime(dateConversion.convertDate(new Date()));
                    externalPersonDevice.setLatitude(Double.parseDouble(locationUpdate.getLatitude()));
                    externalPersonDevice.setLongitude(Double.parseDouble(locationUpdate.getLongitude()));
                    externalPersonDevice.setStatus('A');
                    if(lastStatus!=null){
                        deviceStatusLog.setLogOrder(lastStatus.getLogOrder() + 1);
                    }
                    else {
                        deviceStatusLog.setLogOrder(1);
                        deviceStatusLog.setLastStatus("PENDING");
                    }
                    deviceStatusLogRepository.save(deviceStatusLog);
                    externalPersonDeviceRepository.save(externalPersonDevice);
                    response = new Response(200, true, "The device location was successfully updated");
                }
                else {
                    throw  new NoDeviceRegisteredException("Re register the device if you re-install the application");
                }
        }
        else {
            throw new DeviceTokenNotFoundException("Device Token does not Exists");
        }
        return response;
    }

    public List<AutoMotiveDetailsDTO> getAutoMotiveDetails(){

            List<String> categoryCodes = claimParametersService.getCategoryAutomotiveCategoryList();

            List<ExternalPerson> externalPeople = externalPersonRepository.findAllByCategoryCodeInAndStatus(categoryCodes,'A');

            List<AutoMotiveDetailsDTO> automotiveDetailsList = externalPeople.stream().map(p -> {

                    AutoMotiveDetailsDTO autoMotiveDetailsDTO = new AutoMotiveDetailsDTO();
                    autoMotiveDetailsDTO.setName(getExternalPersonName(p));
                    autoMotiveDetailsDTO.setAddress(getExternalPersonAddress(p));
                    autoMotiveDetailsDTO.setCity(getExternalPersonCity(p));
                    autoMotiveDetailsDTO.setContactNo(p.getContact1()==null ? "-" : p.getContact1());

                    if(p.getCategoryCode().equals(categoryCodes.get(0))){

                        autoMotiveDetailsDTO.setType("1");
                    }
                    else if(p.getCategoryCode().equals(categoryCodes.get(1))){

                        autoMotiveDetailsDTO.setType("2");
                    }
                    else {

                        autoMotiveDetailsDTO.setType("3");
                    }

                    autoMotiveDetailsDTO.setLatitude(p.getLatitude());
                    autoMotiveDetailsDTO.setLongitude(p.getLongitude());


                    return autoMotiveDetailsDTO;
            }).collect(Collectors.toList());

            return automotiveDetailsList;
    }


    public String getExternalPersonName(ExternalPerson externalPerson){

        String name = "";

        String initials = externalPerson.getInitial()==null ? "" : externalPerson.getInitial() + " ";
        String surname = externalPerson.getSurname()==null ? "" : externalPerson.getSurname();

        name = initials + surname;

        return name;
    }

    public String getExternalPersonName(String externalPersonCode){

        String name = "";

        ExternalPerson externalPerson = externalPersonRepository.findByCode(externalPersonCode);

        if(externalPerson!=null){

            String initials = externalPerson.getInitial()==null ? "" : externalPerson.getInitial() + " ";
            String surname = externalPerson.getSurname()==null ? "" : externalPerson.getSurname();

            name = initials + surname;
        }

        return name;
    }

    public String getExternalPersonAddress(ExternalPerson externalPerson){

         String fullAddress = "";

         String address1 = externalPerson.getAddress()==null ? "" : externalPerson.getAddress() + " ";
         String address2 = externalPerson.getAddress2()==null ? "" : externalPerson.getAddress2() + " ";
         String address3 = externalPerson.getAddress3()==null ? "" : externalPerson.getAddress3();

         fullAddress = address1 + address2 + address3;

         return fullAddress;
    }

    public String getExternalPersonCity(ExternalPerson externalPerson){

        return externalPerson.getAddress3()==null ? "-" : externalPerson.getAddress3();
    }


    public String getExternalPersonDeviceStatus(Character statusCode){

        String deviceStatus;

        switch (statusCode){

            case 'P' :
                deviceStatus = "PENDING";
                break;
            case 'A' :
                deviceStatus = "ACTIVE";
                break;

            case 'I' :
                deviceStatus = "IDLE";
                break;
            case 'N' :
                deviceStatus = "INACTIVE";
                break;
            case 'D' :
                deviceStatus = "DEACTIVATED";
                break;
            default:
                deviceStatus = "NOT KNOWN";
        }

        return deviceStatus;
    }


    private String generateDeviceToken(){

        int deviceToken = INITIAL_DEVICE_TOKEN;

        ExternalPersonDevice assessorDevice = externalPersonDeviceRepository.findFirstByOrderByDeviceTokenDesc();

        if(assessorDevice!=null){

            deviceToken = Integer.parseInt(assessorDevice.getDeviceToken()) + 3;
        }
        
        return Integer.toString(deviceToken);

    }

}
