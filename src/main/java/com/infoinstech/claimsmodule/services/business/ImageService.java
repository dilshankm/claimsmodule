package com.infoinstech.claimsmodule.services.business;

import com.infoinstech.claimsmodule.dto.common.Response;
import com.infoinstech.claimsmodule.dto.imageservice.*;
import com.infoinstech.claimsmodule.domain.model.claims.transaction.*;
import com.infoinstech.claimsmodule.domain.packages.PK_CL_TAB_SEQUENCE;
import com.infoinstech.claimsmodule.domain.repository.claims.transaction.*;
import com.infoinstech.claimsmodule.services.common.ClaimsCommonService;
import com.infoinstech.claimsmodule.services.parameters.ReceiptingParametersService;
import com.infoinstech.claimsmodule.services.util.DateConversion;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Service;
//import sun.misc.BASE64Decoder;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.util.ArrayList;
import java.util.Base64;
import java.util.Date;
import java.util.List;

@Service
public class ImageService {

    @PersistenceContext
    private EntityManager entityManager;

    @Autowired
    Environment env;
    @Autowired
    private ClaimIntimationRepository claimIntimationRepository;
    @Autowired
    private JobAssignmentRepository jobAssignmentRepository;
    @Autowired
    private ClaimNotificationRepository claimNotificationRepository;
    @Autowired
    private NotificationAssignmentRepository notificationAssignmentRepository;
    @Autowired
    private ImageRepository imageRepository;

    @Autowired
    private PK_CL_TAB_SEQUENCE pk_cl_tab_sequence;

    @Autowired
    private DateConversion dateConversion;
    @Autowired
    private ClaimsCommonService claimsCommonService;
    @Autowired
    private ReceiptingParametersService receiptingParametersService;
    @Autowired
    private VehicleSketchService vehicleSketchService;

    public Response uploadClaimImages(ImagesUploadRequestDTO imagesUploadRequestDTO) {

        Response response;

        JobAssignment jobAssignment = null;
        ClaimIntimation claimIntimation = null;
        NotificationAssignment notificationAssignment = null;
        ClaimNotification claimNotification = null;

        boolean intimation = false;


        if (imagesUploadRequestDTO.getClaimNo().substring(0, 2).equals("CL")) {

            jobAssignment = jobAssignmentRepository.findByJobNo(imagesUploadRequestDTO.getJobNo());
            claimIntimation = claimIntimationRepository.findByClaimNo(imagesUploadRequestDTO.getClaimNo());

            intimation = true;
        } else {

            notificationAssignment = notificationAssignmentRepository.findByJobNo(imagesUploadRequestDTO.getJobNo());
            claimNotification = claimNotificationRepository.findByNotificationNo(imagesUploadRequestDTO.getClaimNo());
        }


        List<Image> imageList = new ArrayList<>();
        Image image;

        if (imagesUploadRequestDTO.getImagesList() != null) {

            for (ImageDTO imageDTO : imagesUploadRequestDTO.getImagesList()) {

                image = new Image();
                image.setSequenceNo(claimsCommonService.generateSequence(Image.SEQUENCE, receiptingParametersService.getBaseAccountBranch()));

                if (intimation) {

                    image.setIntimationSequenceNo(claimIntimation.getSequenceNo());
                    image.setClaimNo(claimIntimation.getClaimNo());
                    image.setAssignmentSequenceNo(jobAssignment.getJobAssignmentPK().getSequenceNo());
                    image.setJobNo(jobAssignment.getJobNo());
                } else {

                    image.setNotificationSequeneNo(claimNotification.getSequenceNo());
                    image.setNotificationNo(claimNotification.getNotificationNo());
                    image.setNotificationSequeneNo(notificationAssignment.getNotificationAssignmentPK().getSequenceNo());
                    image.setAssignmentSequenceNo(notificationAssignment.getNotificationAssignmentPK().getSequenceNo());
                    image.setAssignmentNo(notificationAssignment.getJobNo());
                }

                image.setUploadTypeCode(imagesUploadRequestDTO.getUploadType());
                image.setImageTypeCode(imageDTO.getTypeCode());

                if (imageDTO.getCapturedDate() == null) {

                    image.setCapturedDate(dateConversion.convertDate(new Date()));
                } else {

                    image.setCapturedDate(dateConversion.convertStringToDate(imageDTO.getCapturedDate()));
                }

                image.setName(dateConversion.convertFromDateToString(new Date(), "yyyyMMddHHmmssSSSS"));


//                BASE64Decoder base64Decoder = new BASE64Decoder();
                byte[] imageByte = new byte[0];
                try {
//                        imageByte = base64Decoder.decodeBuffer(imageDTO.getImageString());
                    imageByte = Base64.getDecoder().decode(imageDTO.getImageString());
                } catch (Exception e) {
                    e.printStackTrace();

                    response = new Response(400, false, e.getMessage());
                }
                image.setImage(imageByte);

                imageList.add(image);
            }

            try {

                imageRepository.saveAll(imageList);

                response = new Response(200, true, "The Images were uploaded successfully.");
            } catch (Exception e) {

                response = new Response(400, false, e.getMessage());
            }
        } else {
            response = new Response(400, false, "There is no adequate information in the Request");
        }

        return response;
    }

    public Response uploadClaimImageByImage(ImageUploadRequestDTO imageUploadRequestDTO) {

        Response response;

        JobAssignment jobAssignment = null;
        ClaimIntimation claimIntimation = null;
        NotificationAssignment notificationAssignment = null;
        ClaimNotification claimNotification = null;

        boolean notification = false;

        if (imageUploadRequestDTO.getJobNo().substring(0, 2).equals("JB")) {

            jobAssignment = jobAssignmentRepository.findByJobNo(imageUploadRequestDTO.getJobNo());
            claimIntimation = claimIntimationRepository.findByClaimNo(imageUploadRequestDTO.getClaimNo());

        } else {

            notificationAssignment = notificationAssignmentRepository.findByJobNo(imageUploadRequestDTO.getJobNo());
            claimNotification = claimNotificationRepository.findByNotificationNo(imageUploadRequestDTO.getClaimNo());

            notification = true;

        }


        Image image;

        if ((jobAssignment != null || notificationAssignment != null) & imageUploadRequestDTO.getImage() != null) {


            image = new Image();
            image.setSequenceNo(claimsCommonService.generateSequence(Image.SEQUENCE, receiptingParametersService.getBaseAccountBranch()));

            if (notification) {

                image.setNotificationSequeneNo(claimNotification.getSequenceNo());
                image.setNotificationNo(claimNotification.getNotificationNo());
                image.setNotificationSequeneNo(notificationAssignment.getNotificationAssignmentPK().getSequenceNo());
                image.setAssignmentNo(notificationAssignment.getJobNo());

            } else {

                image.setIntimationSequenceNo(claimIntimation.getSequenceNo());
                image.setClaimNo(claimIntimation.getClaimNo());
                image.setAssignmentSequenceNo(jobAssignment.getJobAssignmentPK().getSequenceNo());
                image.setJobNo(jobAssignment.getJobNo());

            }
            image.setUploadTypeCode("UT001");
//                image.setImageTypeCode(imageUploadRequestDTO.getImageType());
            image.setImageTypeCode(imageUploadRequestDTO.getImageType());
            image.setCapturedDate(dateConversion.convertDate(new Date()));
            image.setName(dateConversion.convertFromDateToString(new Date(), "yyyyMMddHHmmssSSSS"));

            byte[] imageByte = new byte[0];

            try {

                String imageString = imageUploadRequestDTO.getImage().toString().replaceAll("\\s+", "");
                imageByte = Base64.getDecoder().decode(imageString);

            } catch (Exception e) {

                e.printStackTrace();

                response = new Response(400, false, e.getMessage());
            }

            image.setImage(imageByte);

            try {

                imageRepository.save(image);

                response = new Response(200, true, "The Image was uploaded successfully.");
            } catch (Exception e) {

                response = new Response(400, false, e.getMessage());
            }
        } else {
            response = new Response(400, false, "There is no adequate information in the Request");
        }

        return response;
    }

    public Response viewClaimImages(ReadImageRequestDTO readImageRequestDTO) {

        List<Image> images = null;

        if (readImageRequestDTO.getJobNo().substring(0, 2).equals("JB")) {

            images = imageRepository.
                    findByJobNoAndImageTypeCode(readImageRequestDTO.getJobNo(), readImageRequestDTO.getImageType());
        } else {

            images = imageRepository.findByAssignmentNoAndImageTypeCode(readImageRequestDTO.getJobNo(), readImageRequestDTO.getImageType());

        }

        List<ViewImageDTO> imageDTOList = new ArrayList<>();
        ViewImageDTO viewImageDTO;

        for (Image image : images) {

            viewImageDTO = new ViewImageDTO();
            viewImageDTO.setImageName(image.getName());

            try {

                String imageString = Base64.getEncoder().encodeToString(image.getImage());

                viewImageDTO.setImageString(imageString);
            } catch (Exception e) {

                e.printStackTrace();

                return new Response(400, false, e.getMessage());
            }

            imageDTOList.add(viewImageDTO);
        }

        return new Response(200, true, imageDTOList);
    }

}
