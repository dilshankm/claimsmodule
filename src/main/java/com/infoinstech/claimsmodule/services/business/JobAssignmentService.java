package com.infoinstech.claimsmodule.services.business;

import com.google.gson.Gson;
import com.infoinstech.claimsmodule.dto.claimintimation.CustomerContactDTO;
import com.infoinstech.claimsmodule.dto.common.*;
import com.infoinstech.claimsmodule.dto.jobdetails.*;
import com.infoinstech.claimsmodule.dto.questionnaire.QuestionDTO;
import com.infoinstech.claimsmodule.domain.model.claims.history.ClaimTaskHistory;
import com.infoinstech.claimsmodule.domain.model.claims.master.ExternalPerson;
import com.infoinstech.claimsmodule.domain.model.claims.master.ExternalPersonDevice;
import com.infoinstech.claimsmodule.domain.model.underwriting.master.ProductVehicleSketch;
import com.infoinstech.claimsmodule.domain.model.claims.transaction.*;
import com.infoinstech.claimsmodule.domain.model.underwriting.master.Customer;
import com.infoinstech.claimsmodule.domain.model.underwriting.temporary.*;
import com.infoinstech.claimsmodule.dto.claiminquiry.JobAssignmentDTO;
import com.infoinstech.claimsmodule.domain.model.underwriting.transaction.PolicyRisk;
import com.infoinstech.claimsmodule.domain.packages.PK_CL_CALL_CENTER;
import com.infoinstech.claimsmodule.domain.packages.PK_CL_TAB_SEQUENCE;
import com.infoinstech.claimsmodule.domain.packages.PK_RC_DEBIT_NOTE;
import com.infoinstech.claimsmodule.domain.repository.claims.history.ClaimTaskHistoryRepository;
import com.infoinstech.claimsmodule.domain.repository.claims.master.ExternalPersonDeviceRepository;
import com.infoinstech.claimsmodule.domain.repository.claims.master.ExternalPersonRepository;
import com.infoinstech.claimsmodule.domain.repository.underwriting.master.ProductVehicleSketchRepository;
import com.infoinstech.claimsmodule.domain.repository.claims.reference.RefLossCauseRepository;
import com.infoinstech.claimsmodule.domain.repository.claims.transaction.*;
import com.infoinstech.claimsmodule.domain.repository.underwriting.master.CustomerRepository;
import com.infoinstech.claimsmodule.domain.repository.underwriting.master.ProductPerilRepository;
import com.infoinstech.claimsmodule.domain.repository.underwriting.master.ProductRepository;
import com.infoinstech.claimsmodule.domain.repository.underwriting.temporary.TempPolicyExcessTypeRepository;
import com.infoinstech.claimsmodule.domain.repository.underwriting.temporary.TempPolicyPerilRepository;
import com.infoinstech.claimsmodule.domain.repository.underwriting.temporary.TempPolicyRiskRepository;
import com.infoinstech.claimsmodule.domain.repository.underwriting.transaction.PolicyExcessTypeRepository;
import com.infoinstech.claimsmodule.domain.repository.underwriting.transaction.PolicyPerilRepository;
import com.infoinstech.claimsmodule.domain.repository.underwriting.transaction.PolicyRepository;
import com.infoinstech.claimsmodule.domain.repository.underwriting.transaction.PolicyRiskRepository;
import com.infoinstech.claimsmodule.exceptions.types.NotEnoughInfoException;
import com.infoinstech.claimsmodule.exceptions.types.NotFoundException;
import com.infoinstech.claimsmodule.services.api.AndroidPushNotificationService;
import com.infoinstech.claimsmodule.services.api.OldNotificationService;
import com.infoinstech.claimsmodule.services.common.ClaimsCommonService;
import com.infoinstech.claimsmodule.services.common.SequenceGeneration;
import com.infoinstech.claimsmodule.services.converters.ClaimsConverter;
import com.infoinstech.claimsmodule.services.parameters.ReceiptingParametersService;
import com.infoinstech.claimsmodule.services.reference.ReferenceService;
import com.infoinstech.claimsmodule.services.util.DateConversion;
import com.infoinstech.claimsmodule.services.util.Formatter;
import com.infoinstech.claimsmodule.services.util.Settings;
import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.util.*;
import java.util.stream.Collectors;

@Service
public class JobAssignmentService {

    @Autowired
    Environment env;
    @Autowired
    Formatter formatter;

    //Database Packages
    @PersistenceContext
    EntityManager entityManager;
    @Autowired
    PK_CL_CALL_CENTER pk_cl_CALLCENTER;
    @Autowired
    PK_CL_TAB_SEQUENCE pk_cl_tab_sequence;
    @Autowired
    PK_RC_DEBIT_NOTE pk_rc_debit_note;

    //Repositories
    @Autowired
    private ClaimIntimationRepository claimIntimationRepository;
    @Autowired
    private PolicyRiskRepository policyRiskRepository;
    @Autowired
    private CustomerRepository customerRepository;
    @Autowired
    private JobAssignmentRepository jobAssignmentRepository;
    @Autowired
    private PolicyPerilRepository policyPerilRepository;
    @Autowired
    private TempPolicyPerilRepository tempPolicyPerilRepository;
    @Autowired
    private PolicyRepository policyRepository;
    @Autowired
    private TempPolicyRiskRepository tempPolicyRiskRepository;
    @Autowired
    private RefLossCauseRepository refLossCauseRepository;
    @Autowired
    private PolicyExcessTypeRepository policyExcessTypeRepository;
    @Autowired
    private TempPolicyExcessTypeRepository tempPolicyExcessTypeRepository;
    @Autowired
    private ProductRepository productRepository;
    @Autowired
    private ProductPerilRepository productPerilRepository;
    @Autowired
    private ProvisionDetailsRepository provisionDetailsRepository;
    @Autowired
    private NotificationLogRepository notificationLogRepository;
    @Autowired
    private AssignLossAdjusterRepository assignLossAdjusterRepository;
    @Autowired
    private ExternalPersonDeviceRepository externalPersonDeviceRepository;
    @Autowired
    private ExternalPersonRepository externalPersonRepository;
    @Autowired
    private ClaimCurrentTaskRepository claimCurrentTaskRepository;
    @Autowired
    private ClaimTaskHistoryRepository claimTaskHistoryRepository;
    @Autowired
    private ClaimNotificationRepository claimNotificationRepository;
    @Autowired
    private NotificationLossAdjusterRepository notificationLossAdjusterRepository;
    @Autowired
    private NotificationAssignmentRepository notificationAssignmentRepository;
    @Autowired
    private ProductVehicleSketchRepository productVehicleSketchRepository;

    @Autowired
    private OldNotificationService oldNotificationService;
    @Autowired
    private WrapperService wrapperService;

    //LOV Services
    @Autowired
    ReferenceService referenceService;

    @Autowired
    ClaimsConverter claimsConverter;


    //Parameter Services
    @Autowired
    private ReceiptingParametersService receiptingParametersService;
    //Utility
    @Autowired
    DateConversion dateConversion;

    //Business Services
    @Autowired
    private ClaimIntimationService claimIntimationService;
    @Autowired
    private CustomerService customerService;
    @Autowired
    private PolicyRiskInformationService policyRiskInformationService;
    @Autowired
    private QuestionnaireService questionnaireService;
    @Autowired
    private ClaimsCommonService claimsCommonService;
    @Autowired
    private SequenceGeneration sequenceGeneration;
    @Autowired
    private Settings settings;


    @Autowired
    AndroidPushNotificationService androidPushNotificationService;

    private static final Logger logger = LogManager.getLogger(JobAssignmentService.class);

    //Create Job Assignments
    public Response createJobAssignment(JobAssignmentDTO jobAssignmentDTO) {

        Response response = new Response();
        DateConversion dateConversion = new DateConversion();

        //Get Claim Intimation Detail
        ClaimIntimation claimIntimation = claimIntimationRepository.findBySequenceNo(jobAssignmentDTO.getIntimationSequenceNo());
        Customer customer = customerRepository.findByCustomerCode(claimIntimation.getCustomerCode());

        //Check whether an Assessor (Loss Adjuster) is assigned
        if (jobAssignmentDTO.getExternalPersonCode() != null) {

            ExternalPerson externalPerson = externalPersonRepository.findByCode(jobAssignmentDTO.getExternalPersonCode());
            ExternalPersonDevice externalPersonDevice = externalPersonDeviceRepository.
                    findByExternalPersonCodeAndRegistered(externalPerson.getCode(), 'Y');

            if (externalPerson != null) {

                //Generate Sequences for Job Assignment Tables - Oracle
                String jobSeqNo = claimsCommonService.generateSequence(JobAssignment.SEQUENCE, receiptingParametersService.getBaseAccountBranch());
                String jobNo = claimsCommonService.generateSequence(JobAssignment.JOB_NO_PREFIX, JobAssignment.JOB_NO_SEQUENCE, claimIntimation.getPolicyBranchCode());
                String assignedLossSeqNo = claimsCommonService.generateSequence(AssignLossAdjuster.SEQUENCE, receiptingParametersService.getBaseAccountBranch());


                // Assign Loss Adjuster (Claim Assessor) Details - Core
                AssignLossAdjuster assignLossAdjuster = new AssignLossAdjuster(dateConversion.convertDate(new Date()));
                AssignLossAdjusterPK assignLossAdjusterPK = new AssignLossAdjusterPK();
                assignLossAdjusterPK.setIntimationSequenceNo(jobAssignmentDTO.getIntimationSequenceNo());
                assignLossAdjusterPK.setJobSequenceNo(jobSeqNo);
                assignLossAdjusterPK.setSequenceNo(assignedLossSeqNo);
                assignLossAdjuster.setStatus('N');
                assignLossAdjuster.setAssignLossAdjusterPK(assignLossAdjusterPK);
                assignLossAdjuster.setExternalPersonCode(externalPerson.getCode());
                assignLossAdjuster.setAppointedDate(dateConversion.convertStringToDate(jobAssignmentDTO.getAppointedDate()));
                assignLossAdjuster.setReportDueDate(dateConversion.convertStringToDate(jobAssignmentDTO.getReportDueDate()));
                assignLossAdjuster.setVisitDueDate(dateConversion.convertStringToDate(jobAssignmentDTO.getVisitDueDate()));
                assignLossAdjuster.setCreatedBy(jobAssignmentDTO.getCreatedBy().toUpperCase());
                assignLossAdjuster.setCreatedDate(new Date());
                assignLossAdjuster.setModifiedDate(new Date());
                assignLossAdjuster.setModifiedBy(jobAssignmentDTO.getCreatedBy().toUpperCase());


                // Job Assignment Details - Core
                JobAssignment jobAssignment = new JobAssignment();

                jobAssignment.setJobNo(jobNo);
                JobAssignmentPK jobAssignmentPK = new JobAssignmentPK();
                jobAssignmentPK.setIntimationSequenceNo(claimIntimation.getSequenceNo());
                jobAssignmentPK.setSequenceNo(jobSeqNo);
                jobAssignment.setStatus('N');
                jobAssignment.setJobAssignmentPK(jobAssignmentPK);

                if (jobAssignmentDTO.getTypeCode() == null || jobAssignmentDTO.getTypeCode().equals("")) {

                    jobAssignment.setSurveyCode("ST008");
                    logger.error("Job No :" + jobNo + " - SURVEY CODE not received ");

                } else {
                    jobAssignment.setSurveyCode(jobAssignmentDTO.getTypeCode());
                }

                jobAssignment.setLatitude(jobAssignmentDTO.getLatitude().toString());
                jobAssignment.setLongitude(jobAssignmentDTO.getLongitude().toString());
                jobAssignment.setNearestTown(jobAssignmentDTO.getNearestTown());
                jobAssignment.setCreatedBy(jobAssignmentDTO.getCreatedBy().toUpperCase());
                jobAssignment.setCreatedDate(new Date());
                jobAssignment.setCreatedDate(new Date());
                jobAssignment.setModifiedBy(jobAssignmentDTO.getCreatedBy().toUpperCase());

                JobNotification jobNotification = new JobNotification();
                jobNotification.setCustomerName(customerService.getCustomerName(claimIntimation.getCustomer()));
                jobNotification.setPlaceOfAccident(claimIntimation.getPlaceOfLoss());
                jobNotification.setAccidentTime(claimIntimation.getLossDate().toString().substring(0, 10));
                jobNotification.setVehicleNo(claimIntimation.getRiskName());
                CustomerContactDTO customerContactDTO = customerService.getCustomerContact(claimIntimation.getCustomer());
                jobNotification.setCustomerMobileNo(customerContactDTO.getContact());
                jobNotification.setActionTaken(claimIntimation.getActionTaken());
                jobNotification.setPolicyNo(claimIntimation.getPolicyNo());
                // Save Job Assignment Details to the Database
                jobAssignmentRepository.save(jobAssignment);
                assignLossAdjusterRepository.save(assignLossAdjuster);

                if (jobAssignmentDTO.getActionTaken().equals("CLAIM INSPECTION")) {

                    oldNotificationService.sendJobNotification(claimIntimation, jobAssignment, assignLossAdjuster, externalPersonDevice, customer);
                }

                /*
                 * Get the Current Claim Task (Claim Intimation task - CL001) - and update the Task ReferenceCode to
                 * (Assign Loss Adjuster - CL004) */

                // Update Claim Current Task Details - Oracle
                ClaimCurrentTask claimCurrentTask = claimCurrentTaskRepository.findByClaimNo(claimIntimation.getClaimNo());

                if (claimCurrentTask != null) {

                    if (claimCurrentTask.getTaskCode().equals("CL002")) {


                        claimCurrentTask.setTaskCode("CL004");

                        // Claim Task History Details - Oracle
                        ClaimTaskHistory claimTaskHistory = new ClaimTaskHistory();
                        claimTaskHistory.setSequenceneNo(claimsCommonService.generateSequence(ClaimTaskHistory.SEQUENCE));
                        claimTaskHistory.setClaimSequenceNo(claimIntimation.getSequenceNo());
                        claimTaskHistory.setClaimNo(claimIntimation.getClaimNo());
                        claimTaskHistory.setTaskCode("CL001");
                        claimTaskHistory.setLossComments(claimIntimation.getLossRemarks());
                        claimTaskHistory.setClaimComments(claimIntimation.getClaimNo());
                        claimTaskHistory.setCreatedBy(jobAssignmentDTO.getCreatedBy().toUpperCase());
                        claimTaskHistory.setCreatedDate(new Date());
                        claimTaskHistory.setModifiedBy(jobAssignmentDTO.getCreatedBy());
                        claimTaskHistory.setModifiedDate(new Date());

                        // Save Claim Task details to the Database - Core
                        claimCurrentTaskRepository.save(claimCurrentTask);
                        claimTaskHistoryRepository.save(claimTaskHistory);
                    }
                } else {

                    claimCurrentTask = new ClaimCurrentTask();
                    claimCurrentTask.setSequenceNo(claimsCommonService.generateSequence(ClaimCurrentTask.SEQUENCE));
                    claimCurrentTask.setTaskCode("CL004");
                    claimCurrentTask = claimsConverter.claimIntimationToClaimCurrentTask.apply(claimIntimation, claimCurrentTask);

                    ClaimTaskHistory claimTaskHistory = new ClaimTaskHistory();
                    claimTaskHistory.setSequenceneNo(claimsCommonService.generateSequence(ClaimTaskHistory.SEQUENCE));
                    claimTaskHistory.setTaskCode("CL001");
                    claimTaskHistory = claimsConverter.claimIntimationToClaimTaskHistory.apply(claimIntimation, claimTaskHistory);

                    claimCurrentTaskRepository.save(claimCurrentTask);
                    claimTaskHistoryRepository.save(claimTaskHistory);

                }

                    /*Send the Push Notification to the Claim Assessor (Assign Loss Adjuster)
                    for the Claim Intimation (FCM Service)*/
                // sendJobAssignmentNotification(jobNotification);

                response = new Response(200, true, "The Job Assignment was successful. The Job No is " + jobNo + ".");


            } else {
                throw new NotFoundException("The Assessor ReferenceCode does not exists");
            }
        } else {
            throw new NotEnoughInfoException("There is no adequate Information in the Request");
        }
        return response;
    }

    //Create Notification Assignments
    public Response createNotificationAssignment(JobAssignmentDTO jobAssignmentDTO) {

        Response response = new Response();
        DateConversion dateConversion = new DateConversion();

        //Get Claim Notifications Details
        ClaimNotification claimNotification = claimNotificationRepository.findBySequenceNo(jobAssignmentDTO.getIntimationSequenceNo());

        //Check whether an Assessor (Loss Adjuster) is assigned
        if (jobAssignmentDTO.getExternalPersonCode() != null) {

            ExternalPerson externalPerson = externalPersonRepository.findByCode(jobAssignmentDTO.getExternalPersonCode());
            ExternalPersonDevice externalPersonDevice = externalPersonDeviceRepository.
                    findByExternalPersonCodeAndRegistered(externalPerson.getCode(), 'Y');

            if (externalPerson != null) {

                String branchCode = receiptingParametersService.getBaseAccountBranch();

                NotificationAssignment notificationAssignment = new NotificationAssignment();

                String notificationAssignmentSequenceNo = claimsCommonService.generateSequence(NotificationAssignment.SEQUENCE, branchCode);

                NotificationAssignmentPK notificationAssignmentPK = new NotificationAssignmentPK();
                notificationAssignmentPK.setSequenceNo(notificationAssignmentSequenceNo);
                notificationAssignmentPK.setNotificationSequenceNo(claimNotification.getSequenceNo());
                notificationAssignment.setNotificationAssignmentPK(notificationAssignmentPK);
                notificationAssignment.setJobNo("AS" + notificationAssignmentSequenceNo);
                notificationAssignment.setLatitude(jobAssignmentDTO.getLatitude().toString());
                notificationAssignment.setLongitude(jobAssignmentDTO.getLongitude().toString());
                notificationAssignment.setNearestTown(claimNotification.getPlaceOfLoss());
                notificationAssignment.setCreatedDate(new Date());
                notificationAssignment.setCreatedBy(claimNotification.getCreatedBy().toUpperCase());
                notificationAssignment.setCreatedDate(new Date());
                notificationAssignment.setModifiedBy(claimNotification.getCreatedBy().toUpperCase());
                notificationAssignment.setSurveryCode(jobAssignmentDTO.getTypeCode());
                notificationAssignment.setStatus('N');


                NotificationLossAdjuster notificationLossAdjuster = new NotificationLossAdjuster();

                String notificationLossAdjusterSequenceNo = claimsCommonService.generateSequence(AssignLossAdjuster.SEQUENCE, branchCode);
                NotificationLossAdjusterPK notificationLossAdjusterPK = new NotificationLossAdjusterPK();
                notificationLossAdjusterPK.setSequenceNo(notificationLossAdjusterSequenceNo);
                notificationLossAdjusterPK.setAssignmentSequenceNo(notificationAssignment.getNotificationAssignmentPK().getSequenceNo());
                notificationLossAdjusterPK.setNotificationSequenceNo(claimNotification.getSequenceNo());

                notificationLossAdjuster.setNotificationLossAdjusterPK(notificationLossAdjusterPK);
                notificationLossAdjuster.setAuthorizedBy("N");
                notificationLossAdjuster.setStatus('N');
                notificationLossAdjuster.setExternalPersonCode(jobAssignmentDTO.getExternalPersonCode());
                notificationLossAdjuster.setPromiseTime(jobAssignmentDTO.getPromiseTime());
                notificationLossAdjuster.setCreatedBy(jobAssignmentDTO.getCreatedBy().toUpperCase());
                notificationLossAdjuster.setCreatedDate(new Date());
                notificationLossAdjuster.setModifiedBy(jobAssignmentDTO.getCreatedBy());
                notificationLossAdjuster.setModifiedDate(new Date());
                notificationLossAdjuster.setAppointedDate(dateConversion.convertStringToDate(jobAssignmentDTO.getAppointedDate()));
                notificationLossAdjuster.setReportDueDate(dateConversion.convertStringToDate(jobAssignmentDTO.getReportDueDate()));
                notificationLossAdjuster.setVisitDueDate(dateConversion.convertStringToDate(jobAssignmentDTO.getVisitDueDate()));


                // Save Job Assignment Details to the Database
                notificationAssignmentRepository.save(notificationAssignment);
                notificationLossAdjusterRepository.save(notificationLossAdjuster);

                if (jobAssignmentDTO.getActionTaken().equals("CLAIM_INSPECTION")) {

                    oldNotificationService.sendAssignmentNotification(claimNotification, notificationAssignment, notificationLossAdjuster, externalPersonDevice);
                }

                response = new Response(200, true, "The Job Assignment was successful. The Job No is " + notificationAssignment.getJobNo() + ".");

            } else {
                throw new NotFoundException("The Assessor ReferenceCode does not exists");
            }
        } else {
            throw new NotEnoughInfoException("There is no adequate Information in the Request");
        }
        return response;
    }

    public Response getQuestionnaireDetails(JobNoDTO jobNoDTO) {
        Response response = null;
        List<QuestionDTO> questionDTOList;
        if (jobNoDTO.getJobNo().substring(0, 2).equals(JobAssignment.JOB_NO_PREFIX)) {
            JobAssignment jobAssignment = jobAssignmentRepository.findByJobNo(jobNoDTO.getJobNo());
            if (jobAssignment != null) {
                ClaimIntimation claimIntimation = claimIntimationRepository.findBySequenceNo(jobAssignment.getJobAssignmentPK().getIntimationSequenceNo());
                questionDTOList = questionnaireService.getQuestionnaire(claimIntimation.getProductCode());
            } else {
                throw new NotFoundException("The Job No does not exists");
            }
        } else {
            NotificationAssignment notificationAssignment = notificationAssignmentRepository.findByJobNo(jobNoDTO.getJobNo());
            if (notificationAssignment != null) {
                ClaimNotification claimNotification = claimNotificationRepository.findBySequenceNo(notificationAssignment.getNotificationAssignmentPK().getNotificationSequenceNo());
                questionDTOList = questionnaireService.getQuestionnaire(claimNotification.getCoverNoteType());
            } else {
                throw new NotFoundException("The Job No does not exists");
            }
        }
        if (questionDTOList != null) {
            response = new Response(200, true, questionDTOList);
        } else {
            throw new NotFoundException("Could not get the questionnaire");
        }
        return response;
    }

    public JobAssignmentDetails viewJobAssignment(String jobNo) {

        JobAssignment jobAssignment = jobAssignmentRepository.findByJobNo(jobNo);
        ClaimIntimation claimIntimation = claimIntimationRepository.findBySequenceNo(jobAssignment.getJobAssignmentPK().getIntimationSequenceNo());
//        PolicyRisk policyRisk = policyRiskRepository.findByPolicyRiskPK_SequenceNo(claimIntimation.getRiskSequenceNo());
//        PolicyRiskPK policyRiskPK = policyRisk.getPolicyRiskPK();
//        Policy policy = policyRepository.findBySequenceNo(claimIntimation.getPolicySequenceNo());

        wrapperService.deleteDataFromTempPolicyTables();
        TempPolicy tempPolicy = wrapperService.buildPolicy(claimIntimation.getPolicyNo(), claimIntimation.getLossDate());
        TempPolicyRisk tempPolicyRisk = tempPolicyRiskRepository.findByTempPolicyRiskPK_SequenceNo(claimIntimation.getRiskSequenceNo());

        TempPolicyRiskPK tempPolicyRiskPK = tempPolicyRisk.getTempPolicyRiskPK();

        Customer customer = claimIntimation.getCustomer();
        String status = referenceService.getPolicyStatus(tempPolicy.getStatus());

        JobDetails jobDetails = new JobDetails();

        jobDetails.setJobNo(jobAssignment.getJobNo());
        jobDetails.setTimeOfAssignment(dateConversion.convertFromDateToString(jobAssignment.getCreatedDate()));
        jobDetails.setJobType(jobAssignment.getSurveyType().getSurveyDescription());
        jobDetails.setRequiredDocuments("-");


        PolicyDetails policyDetails = new PolicyDetails();

        String periodFrom = dateConversion.convertFromDateToString(tempPolicy.getPeriodFrom(), "yyyy-MMM-dd");
        String periodTo = dateConversion.convertFromDateToString(tempPolicy.getPeriodTo(), "yyyy-MMM-dd");
        Double debitOutstanding = pk_rc_debit_note.getDebitOutstandingAmountForPolicyNo(tempPolicy.getPolicyNo()).doubleValue();

        policyDetails.setPolicyStatus((status == null) ? "-" : status);
        policyDetails.setPolicyPeriod(periodFrom + " - " + periodTo);
        policyDetails.setPolicyNo(tempPolicy.getPolicyNo());
        policyDetails.setPolicySumInsured(formatter.format(tempPolicy.getSumInsured()));
        policyDetails.setOutstandingAmount(formatter.format(debitOutstanding));
        policyDetails.setOtherInterests("-");


        ClaimDetails claimDetails = new ClaimDetails();

        claimDetails.setClaimNo(claimIntimation.getClaimNo());
        claimDetails.setCauseOfLoss(claimIntimation.getRefLossCause().getDescription());
        claimDetails.setTimeOfAccident(dateConversion.convertFromDateToString(claimIntimation.getLossDate()));
        claimDetails.setTimeOfIntimation(dateConversion.convertFromDateToString(claimIntimation.getIntimationDate()));
        claimDetails.setPlaceOfAccident(formatter.replaceWithDash(claimIntimation.getPlaceOfLoss()));
        claimDetails.setLatitude(Double.parseDouble(claimIntimation.getLatitude() == null ? jobAssignment.getLatitude() : claimIntimation.getLatitude()));
        claimDetails.setLongitude(Double.parseDouble(claimIntimation.getLongitude() == null ? jobAssignment.getLongitude() : claimIntimation.getLongitude()));
        claimDetails.setDriversName(formatter.replaceWithDash(claimIntimation.getDriverName()));
        claimDetails.setDriversLicenseNo(formatter.replaceWithDash(claimIntimation.getDriverLicenseNo()));
        claimDetails.setContactPersonNo(formatter.replaceWithDash((settings.getSite().equals("AMI")) ? claimIntimation.getCliNo() : claimIntimation.getContactNo()));


        CustomerDetails customerDetails = new CustomerDetails();

        CustomerContactDTO customerContactDTO = customerService.getCustomerContact(customer);

        customerDetails.setCustomerName(customerService.getCustomerName(customer));
        customerDetails.setCustomerNIC(customerService.getCustomerNIC(customer));
        customerDetails.setContactNo(formatter.replaceWithDash(customerContactDTO.getContact()));

        VehicleDetails vehicleDetails = new VehicleDetails();

        vehicleDetails.setVehicleNo(tempPolicyRisk.getName());
        vehicleDetails.setChassisNo(policyRiskInformationService.getChassisNo(tempPolicyRisk));
        vehicleDetails.setEngineNo(policyRiskInformationService.getEngineNo(tempPolicyRisk));
        vehicleDetails.setVehicleSumInsured(formatter.format(tempPolicy.getSumInsured()));
        vehicleDetails.setMakeAndModel(policyRiskInformationService.getMakeAndModel(tempPolicyRisk));
        vehicleDetails.setDateRegistered(policyRiskInformationService.getDateRegistered(tempPolicyRisk));
        vehicleDetails.setYearOfManfucature(policyRiskInformationService.getYearOfManufacture(tempPolicyRisk));
        vehicleDetails.setUseOfVehicle(policyRiskInformationService.getUseOfVehicle(tempPolicyRisk));


        List<TempPolicyPeril> tempPolicyPerils = tempPolicyPerilRepository.findAllByTempPolicyPerilPK_PolicySequenceNoAndTempPolicyPerilPK_LocationSequenceNoAndTempPolicyPerilPK_RiskSequenceNo(
                tempPolicyRiskPK.getPolicySequenceNo(), tempPolicyRiskPK.getLocationSequenceNo(), tempPolicyRiskPK.getSequenceNo());

//        List<PolicyPeril> policyPerilList = policyPerilRepository.findAll();

        List<CoverDetails> coverDetails = new ArrayList<>();
        CoverDetails cover;

        for (TempPolicyPeril tempPolicyPeril : tempPolicyPerils) {

            if (env.getProperty("insurance.site").equals("SANASA")) {

                if (tempPolicyPeril.getPerilCode().equals("RSF")) continue;
            }

            //RSF peril code omitted for SICL
            cover = new CoverDetails();
            cover.setCoverName(tempPolicyPeril.getRefPeril().getDescription());
            cover.setSumInsured(formatter.format(tempPolicyPeril.getPerilSumInsured()));

            cover.setExcess(formatter.format(tempPolicyPeril.getExcess()));

            coverDetails.add(cover);
        }

        List<TempPolicyExcessType> tempPolicyExcessTypes = tempPolicyExcessTypeRepository.findByRiskSequenceNo(tempPolicyRiskPK.getSequenceNo());

        tempPolicyExcessTypes.addAll(tempPolicyExcessTypeRepository.findByPolicySequenceNoAndRiskSequenceNoIsNull(tempPolicy.getSequenceNo()));


        List<ExcessDetails> excessDetailsList = new ArrayList<>();
        ExcessDetails excessDetails;

        for (TempPolicyExcessType tempPolicyExcessType : tempPolicyExcessTypes) {

            excessDetails = new ExcessDetails();
            excessDetails.setExcessDescription(tempPolicyExcessType.getRefExcessType().getDescription());
            excessDetails.setExcessPercentage(formatter.formatPercentages(tempPolicyExcessType.getExcessPercentage()));
            excessDetails.setExcessRate(formatter.format(tempPolicyExcessType.getExcessRate()));
            excessDetails.setExcessCode("-");
            excessDetails.setExcessSeqNo("-");
            excessDetails.setRiskSeqno("-");

            excessDetailsList.add(excessDetails);
        }

        ClaimHistoryRequestDTO claimHistoryRequestDTO = new ClaimHistoryRequestDTO();
        claimHistoryRequestDTO.setPolicySequenceNo(tempPolicy.getPolicySequenceNo());
        claimHistoryRequestDTO.setRiskSequenceNo(claimIntimation.getRiskSequenceNo());

        List<ClaimHistoryDTO> claimHistory = claimIntimationService.viewClaimHistoryDetails(claimHistoryRequestDTO);

        List<QuestionDTO> questionDTOList = questionnaireService.getQuestionnaire(claimIntimation.getProductCode());

        List<String> vehicleSketches = getVehicleSketchesByProduct(claimIntimation.getProductCode());

        JobAssignmentDetails jobAssignmentDetails = new JobAssignmentDetails();
        jobAssignmentDetails.setJobDetails(jobDetails);
        jobAssignmentDetails.setClaimDetails(claimDetails);
        jobAssignmentDetails.setPolicyDetails(policyDetails);
        jobAssignmentDetails.setCustomerDetails(customerDetails);
        jobAssignmentDetails.setVehicleDetails(vehicleDetails);
        jobAssignmentDetails.setCoverDetails(coverDetails);
        jobAssignmentDetails.setExcessDetails(excessDetailsList);
        jobAssignmentDetails.setClaimHistory(claimHistory);
        jobAssignmentDetails.setVehicleSketches(vehicleSketches);

        return jobAssignmentDetails;

    }


    public JobAssignmentDetails viewNotificationAssignment(String jobNo) {

        NotificationAssignment notificationAssignment = notificationAssignmentRepository.findByJobNo(jobNo);
        ClaimNotification claimNotification = claimNotificationRepository.findBySequenceNo(notificationAssignment.getNotificationAssignmentPK().getNotificationSequenceNo());

        String status = "-";


        JobDetails jobDetails = new JobDetails();

        jobDetails.setJobNo(notificationAssignment.getJobNo());
        jobDetails.setTimeOfAssignment(dateConversion.convertFromDateToString(notificationAssignment.getCreatedDate()));
        jobDetails.setJobType(notificationAssignment.getRefSurveyType().getSurveyDescription());


        PolicyDetails policyDetails = new PolicyDetails();

        String periodFrom = dateConversion.convertFromDateToString(claimNotification.getEffectiveStartDate(), "yyyy-MMM-dd");
        String periodTo = "-";
        Double debitOutstanding = 0.00;

        policyDetails.setPolicyStatus((status == null) ? "-" : status);
        policyDetails.setPolicyPeriod(periodFrom + " - " + periodTo);
        policyDetails.setPolicyNo(claimNotification.getCoverNoteNo());
        policyDetails.setPolicySumInsured("-");
        policyDetails.setOutstandingAmount(formatter.format(debitOutstanding));
        policyDetails.setOtherInterests("");


        ClaimDetails claimDetails = new ClaimDetails();

        claimDetails.setClaimNo(claimNotification.getNotificationNo());
        claimDetails.setCauseOfLoss(claimNotification.getRefLossCause().getDescription());
        claimDetails.setTimeOfAccident(dateConversion.convertFromDateToString(claimNotification.getLossDate()));
        claimDetails.setTimeOfIntimation(dateConversion.convertFromDateToString(claimNotification.getNotifiedDate()));
        claimDetails.setPlaceOfAccident(formatter.replaceWithDash(claimNotification.getPlaceOfLoss()));
        claimDetails.setLatitude(Double.parseDouble(claimNotification.getLatitude()));
        claimDetails.setLongitude(Double.parseDouble(claimNotification.getLongitude()));
        claimDetails.setDriversName(formatter.replaceWithDash(claimNotification.getDriverName()));
        claimDetails.setDriversLicenseNo(formatter.replaceWithDash(claimNotification.getDriverLicenseNo()));
        claimDetails.setContactPersonNo(formatter.replaceWithDash(claimNotification.getContactNo()));

        CustomerDetails customerDetails = new CustomerDetails();

        customerDetails.setCustomerName("COVER NOTE");
        customerDetails.setCustomerNIC("-");
        customerDetails.setContactNo("-");

        VehicleDetails vehicleDetails = new VehicleDetails();

        vehicleDetails.setVehicleNo(claimNotification.getRiskName());
        vehicleDetails.setChassisNo("-");
        vehicleDetails.setEngineNo("-");
        vehicleDetails.setVehicleSumInsured("-");
        vehicleDetails.setMakeAndModel(formatter.replaceWithDash(claimNotification.getMakeAndModel()));
        vehicleDetails.setDateRegistered("-");
        vehicleDetails.setYearOfManfucature(formatter.replaceWithDash(claimNotification.getYearOfManufacture()));
        vehicleDetails.setUseOfVehicle("-");

        List<CoverDetails> coverDetails = new ArrayList<>();

        List<ExcessDetails> excessDetailsList = new ArrayList<>();

        List<ClaimHistoryDTO> claimHistory = new ArrayList<>();

        List<QuestionDTO> questionDTOList = questionnaireService.getQuestionnaire(claimNotification.getCoverNoteType());

        List<String> vehicleSketches = getVehicleSketchesByProduct(claimNotification.getCoverNoteType());

        JobAssignmentDetails jobAssignmentDetails = new JobAssignmentDetails();
        jobAssignmentDetails.setJobDetails(jobDetails);
        jobAssignmentDetails.setClaimDetails(claimDetails);
        jobAssignmentDetails.setPolicyDetails(policyDetails);
        jobAssignmentDetails.setCustomerDetails(customerDetails);
        jobAssignmentDetails.setVehicleDetails(vehicleDetails);
        jobAssignmentDetails.setCoverDetails(coverDetails);
        jobAssignmentDetails.setExcessDetails(excessDetailsList);
        jobAssignmentDetails.setClaimHistory(claimHistory);
        jobAssignmentDetails.setVehicleSketches(vehicleSketches);

        return jobAssignmentDetails;

    }

    public Response validateJobAssignment(JobAssignmentDTO jobAssignmentDTO) {

        Response response = new Response();

        ClaimIntimation claimIntimation = claimIntimationRepository.findBySequenceNo(jobAssignmentDTO.getIntimationSequenceNo());
        NotificationLog notificationLog = notificationLogRepository.findByClaimSequenceNo(jobAssignmentDTO.getIntimationSequenceNo());


        if (notificationLog == null) {

            String message = "Claim is Intimated from the Core System";
            response = new Response(400, false, message);
        } else {

            response = new Response(200, true, "");
        }

        return response;
    }

    // Update Job Assignment Details - App

    public Response updateJobAssignmentStatus(AssignmentStatusDTO assignmentStatusDTO) {
        Response response = null;
        boolean isIntimation = false;
        JobAssignment jobAssignment = null;
        AssignLossAdjuster assignLossAdjuster = null;
        NotificationAssignment notificationAssignment = null;
        NotificationLossAdjuster notificationLossAdjuster = null;
        if (assignmentStatusDTO.getJobNo().substring(0, 2).equalsIgnoreCase(JobAssignment.JOB_NO_PREFIX)) {
            isIntimation = true;
            jobAssignment = jobAssignmentRepository.findByJobNo(assignmentStatusDTO.getJobNo());
            assignLossAdjuster = assignLossAdjusterRepository.findByAssignLossAdjusterPK_JobSequenceNo(jobAssignment.getJobAssignmentPK().getSequenceNo());
            assignLossAdjuster.setStatus(assignmentStatusDTO.getStatus().charAt(0));
            jobAssignment.setStatus(assignmentStatusDTO.getStatus().charAt(0));
        } else {
            notificationAssignment = notificationAssignmentRepository.findByJobNo(assignmentStatusDTO.getJobNo());
            notificationLossAdjuster = notificationLossAdjusterRepository.findByNotificationLossAdjusterPK_AssignmentSequenceNo(notificationAssignment.getNotificationAssignmentPK().getSequenceNo());
            notificationAssignment.setStatus(assignmentStatusDTO.getStatus().charAt(0));
            notificationLossAdjuster.setStatus(assignmentStatusDTO.getStatus().charAt(0));
        }
        if (isIntimation) {
            jobAssignmentRepository.save(jobAssignment);
            assignLossAdjusterRepository.save(assignLossAdjuster);
        } else {
            notificationAssignmentRepository.save(notificationAssignment);
            notificationLossAdjusterRepository.save(notificationLossAdjuster);
        }
        String message = "Job Status has been successfully updated";
        response = new Response(200, true, message);
        return response;
    }

    public Response viewJobAssignmentsSummary(AssignmentSummaryRequestDTO assignmentSummaryRequestDTO) {

        Response response = null;

        AssignmentSummaryDTO assignmentSummaryDTO = new AssignmentSummaryDTO();

        int assigned = 0;
        int accepted = 0;
        int rejected = 0;
        int completed = 0;

        ExternalPersonDevice mobileUnit = externalPersonDeviceRepository.
                findByDeviceTokenAndGcmKey(assignmentSummaryRequestDTO.getDeviceToken(), assignmentSummaryRequestDTO.getGcmKey());

        if (mobileUnit != null) {

            ExternalPerson externalPerson = externalPersonRepository.findByCode(mobileUnit.getExternalPersonCode());

            List<AssignLossAdjuster> assignments = new ArrayList<>();
            List<NotificationLossAdjuster> notifications = new ArrayList<>();

            if (assignmentSummaryRequestDTO.getDaily().equals("Y")) {

                Date beforeDate = dateConversion.addDays(new Date(), -1);

                assignments = assignLossAdjusterRepository.findByExternalPersonCodeAndCreatedDateBetween(externalPerson.getCode(), beforeDate, new Date());
                notifications = notificationLossAdjusterRepository.findByExternalPersonCodeAndCreatedDateBetween(externalPerson.getCode(), beforeDate, new Date());
            } else {

                Date afterDate = new Date();
                Date beforeDate = dateConversion.addDays(new Date(), -30);

                assignments = assignLossAdjusterRepository.findByExternalPersonCodeAndCreatedDateBetween(externalPerson.getCode(), beforeDate, afterDate);
                notifications = notificationLossAdjusterRepository.findByExternalPersonCodeAndCreatedDateBetween(externalPerson.getCode(), beforeDate, afterDate);

            }

            if (assignments != null) {

                for (AssignLossAdjuster assignment : assignments) {

                    if (assignment.getStatus() != null) {

                        switch (assignment.getStatus()) {

                            case 'N':
                                assigned += 1;
                                break;
                            case 'A':
                                accepted += 1;
                                break;
                            case 'R':
                                rejected += 1;
                                break;
                            case 'C':
                                completed += 1;
                                break;
                        }
                    }
                }
            }

            if (notifications != null) {

                for (NotificationLossAdjuster notification : notifications) {

                    if (notification.getStatus() != null) {

                        switch (notification.getStatus()) {

                            case 'N':
                                assigned += 1;
                                break;
                            case 'A':
                                accepted += 1;
                                break;
                            case 'R':
                                rejected += 1;
                                break;
                            case 'C':
                                completed += 1;
                                break;
                        }
                    }
                }
            }

            assignmentSummaryDTO.setAccepted(accepted);
            assignmentSummaryDTO.setAssigned(assigned);
            assignmentSummaryDTO.setRejected(rejected);
            assignmentSummaryDTO.setCompleted(completed);

            response = new Response(200, true, assignmentSummaryDTO);

        } else {
            throw new NotFoundException("The Device Token or GCM Key does not exists");
        }
        return response;
    }

    public void sendJobAssignmentNotification(JobNotification jobNotification) {


        Gson gson = new Gson();
        String data = gson.toJson(jobNotification);

        JSONObject body = new JSONObject();
        body.put("to", "Job Assignment");
        body.put("priority", "high");

        JSONObject notification = new JSONObject();
        notification.put("title", "Job Assignment");
        notification.put("body", "body string here");

        body.put("notification", notification);
        body.put("data", data);

        ResponseEntity<String> response = androidPushNotificationService.sendPushNotification(body);

    }

    public List<String> getVehicleSketchesByProduct(String productCode) {

        List<ProductVehicleSketch> productVehicleSketches = productVehicleSketchRepository.findAllByProductCode(productCode);

        return productVehicleSketches.stream().map(ProductVehicleSketch::getRefVehicleSketchCode).collect(Collectors.toList());
    }

    public List<JobNotification> getJobNotificationsList(String deviceToken) {

        ExternalPersonDevice device = externalPersonDeviceRepository.findByDeviceToken(deviceToken);
        List<AssignLossAdjuster> assignLossAdjusters = assignLossAdjusterRepository.findByExternalPersonCode(device.getExternalPersonCode());

        List<JobNotification> jobNotificationList = new ArrayList<>();
        JobNotification jobNotification;

        int counter = 0;

        for (AssignLossAdjuster adjuster : assignLossAdjusters) {

            if (counter == 20) {
                break;
            }


            JobAssignment jobAssignment = jobAssignmentRepository.findByJobAssignmentPK_SequenceNo(adjuster.getAssignLossAdjusterPK().getJobSequenceNo());
            ClaimIntimation claimIntimation = claimIntimationRepository.findBySequenceNo(jobAssignment.getJobAssignmentPK().getIntimationSequenceNo());
            Customer customer = customerRepository.findByCustomerCode(claimIntimation.getCustomerCode());

            jobNotification = new JobNotification();

            jobNotification.setJobNo(jobAssignment.getJobNo());
            jobNotification.setAssessorCode(adjuster.getExternalPersonCode());
            jobNotification.setCustomerName(customerService.getCustomerName(customer));
            jobNotification.setPlaceOfAccident(claimIntimation.getPlaceOfLoss());
            jobNotification.setAccidentTime(dateConversion.convertFromDateToString(claimIntimation.getLossDate()));
            jobNotification.setStatus(referenceService.getJobStatusDescriptionByCode(jobAssignment.getStatus().toString()));
            CustomerContactDTO customerContactDTO = customerService.getCustomerContact(customer);
            jobNotification.setCustomerMobileNo(customerContactDTO.getContact());
            jobNotification.setPolicyNo(claimIntimation.getPolicyNo());
            jobNotification.setVehicleNo(claimIntimation.getRiskName());
            jobNotification.setActionTaken(claimIntimation.getActionTaken());
            jobNotification.setLatitude(claimIntimation.getLatitude());
            jobNotification.setLongitude(claimIntimation.getLongitude());

            jobNotificationList.add(jobNotification);

            counter += 1;
        }

        return jobNotificationList;
    }


}
