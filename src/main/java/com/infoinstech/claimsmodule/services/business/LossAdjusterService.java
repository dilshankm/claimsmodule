package com.infoinstech.claimsmodule.services.business;

import com.infoinstech.claimsmodule.dto.common.AssessorChargeDTO;
import com.infoinstech.claimsmodule.dto.common.ReferenceDTO;
import com.infoinstech.claimsmodule.dto.common.Response;
import com.infoinstech.claimsmodule.dto.common.UploadAssessorChargesRequestDTO;
import com.infoinstech.claimsmodule.domain.model.claims.reference.ChargeType;
import com.infoinstech.claimsmodule.domain.model.claims.reference.UnitType;
import com.infoinstech.claimsmodule.domain.model.claims.transaction.*;
import com.infoinstech.claimsmodule.domain.packages.PK_CL_TAB_SEQUENCE;
import com.infoinstech.claimsmodule.domain.repository.claims.reference.ChargeTypeRepository;
import com.infoinstech.claimsmodule.domain.repository.claims.reference.UnitTypeRepository;
import com.infoinstech.claimsmodule.domain.repository.claims.transaction.*;
import com.infoinstech.claimsmodule.exceptions.types.AssessorChargesListNotAvaliableException;
import com.infoinstech.claimsmodule.exceptions.types.ChargeTypesInvalidException;
import com.infoinstech.claimsmodule.exceptions.types.NotificationNotFoundException;
import com.infoinstech.claimsmodule.services.common.ClaimsCommonService;
import com.infoinstech.claimsmodule.services.parameters.ReceiptingParametersService;
import com.infoinstech.claimsmodule.services.util.DateConversion;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Service
public class LossAdjusterService {

    @PersistenceContext
    private EntityManager entityManager;

    @Autowired
    private ClaimIntimationRepository claimIntimationRepository;
    @Autowired
    private ClaimNotificationRepository claimNotificationRepository;
    @Autowired
    private JobAssignmentRepository jobAssignmentRepository;
    @Autowired
    private NotificationAssignmentRepository notificationAssignmentRepository;
    @Autowired
    private AssignLossAdjusterRepository assignLossAdjusterRepository;
    @Autowired
    private NotificationLossAdjusterRepository notificationLossAdjusterRepository;
    @Autowired
    private LossAdjusterChargeRepository lossAdjusterChargeRepository;
    @Autowired
    private NotificationAdjusterChargeRepository notificationAdjusterChargeRepository;
    @Autowired
    private ChargeTypeRepository chargeTypeRepository;
    @Autowired
    private UnitTypeRepository unitTypeRepository;

    @Autowired
    private DateConversion dateConversion;

    @Autowired
    private PK_CL_TAB_SEQUENCE pk_cl_tab_sequence;
    @Autowired
    private ReceiptingParametersService receiptingParametersService;
    @Autowired
    private ClaimsCommonService claimsCommonService;


    public List<ReferenceDTO> getAssessorChargeTypes() {

        List<ChargeType> chargeTypes = chargeTypeRepository.findAll();

        List<ReferenceDTO> referenceDTOList = new ArrayList<>();
        ReferenceDTO referenceDTO;

        for (ChargeType chargeType : chargeTypes) {

            referenceDTO = new ReferenceDTO();
            referenceDTO.setCode(chargeType.getCode());
            referenceDTO.setDescription(chargeType.getDescription());

            referenceDTOList.add(referenceDTO);

        }

        return referenceDTOList;

    }

    public List<ReferenceDTO> getUnitTypes() {

        List<UnitType> unitTypes = unitTypeRepository.findAll();

        List<ReferenceDTO> referenceDTOList = new ArrayList<>();
        ReferenceDTO referenceDTO;

        for (UnitType unitType : unitTypes) {

            referenceDTO = new ReferenceDTO();
            referenceDTO.setCode(unitType.getCode());
            referenceDTO.setDescription(unitType.getDescription());

            referenceDTOList.add(referenceDTO);

        }

        return referenceDTOList;

    }

    public Response saveLossAdjusterCharges(UploadAssessorChargesRequestDTO requestDTO) {
        String branchCode = receiptingParametersService.getBaseAccountBranch();
        Response response = null;
        boolean status = true;
        Boolean isIntimation = null;
        List<LossAdjusterCharge> lossAdjusterCharges = new ArrayList<>();
        List<NotificationAdjusterCharge> notificationAdjusterCharges = new ArrayList<>();
        List<AssessorChargeDTO> assessorChargeDTOList;
        if (requestDTO.getChargesList() == null) {
            throw new AssessorChargesListNotAvaliableException("The Assessor Charges List is not available");
        } else {
            if (requestDTO.getClaimNo().substring(0, 2).equals("CL")) {
                ClaimIntimation claimIntimation = claimIntimationRepository.findByClaimNo(requestDTO.getClaimNo());
                JobAssignment jobAssignment = jobAssignmentRepository.findByJobNo(requestDTO.getJobNo());
                if (claimIntimation != null && jobAssignment != null) {
                    isIntimation = true;
                    AssignLossAdjuster assignLossAdjuster =
                            assignLossAdjusterRepository.findByAssignLossAdjusterPK_JobSequenceNo(jobAssignment.getJobAssignmentPK().getSequenceNo());
                    assessorChargeDTOList = requestDTO.getChargesList();
                    LossAdjusterCharge lossAdjusterCharge;
                    for (AssessorChargeDTO assessorChargeDTO : assessorChargeDTOList) {
                        lossAdjusterCharge = new LossAdjusterCharge();
                        lossAdjusterCharge.setSequenceNo(claimsCommonService.generateSequence(LossAdjusterCharge.SEQUENCE, receiptingParametersService.getBaseAccountBranch()));
                        lossAdjusterCharge.setIntimationSequenceNo(claimIntimation.getSequenceNo());
                        lossAdjusterCharge.setJobAssignmentSequenceNo(jobAssignment.getJobAssignmentPK().getSequenceNo());
                        lossAdjusterCharge.setAssignLossAdjusterSequenceNo(assignLossAdjuster.getAssignLossAdjusterPK().getSequenceNo());
                        Optional<UnitType> unitType = unitTypeRepository.findById(assessorChargeDTO.getUnitTypeCode());
                        Optional<ChargeType> chargeType = chargeTypeRepository.findById(assessorChargeDTO.getChargeTypeCode());
                        if (unitType.get() == null || chargeType == null) {
                            status = false;
                            break;
                        }
/*                        status = !unitType.isPresent() ? false : true;
                        if (unitType == null || chargeType == null) {

                            status = false;
                            break;
                        }*/
                        lossAdjusterCharge.setChargeCode(assessorChargeDTO.getChargeTypeCode());
                        lossAdjusterCharge.setUnitTypeCode(assessorChargeDTO.getUnitTypeCode());
                        lossAdjusterCharge.setDateVisited(dateConversion.convertStringToDate(assessorChargeDTO.getDateVisited(), "dd-MM-yyyy HH:mm:ss"));
                        lossAdjusterCharge.setUnitPrice(assessorChargeDTO.getUnitPrice());
                        lossAdjusterCharge.setNoOfUnits(assessorChargeDTO.getNoOfUnits().intValue());
                        lossAdjusterCharge.setCharges(assessorChargeDTO.getUnitPrice() * assessorChargeDTO.getNoOfUnits());
                        lossAdjusterCharges.add(lossAdjusterCharge);
                    }
                } else {
                }
            } else {
                ClaimNotification claimNotification = claimNotificationRepository.findByNotificationNo(requestDTO.getClaimNo());
                NotificationAssignment notificationAssignment = notificationAssignmentRepository.findByJobNo(requestDTO.getJobNo());
                if (claimNotification != null && notificationAssignment != null) {
                    isIntimation = false;
                    NotificationLossAdjuster notificationLossAdjuster =
                            notificationLossAdjusterRepository.findByNotificationLossAdjusterPK_AssignmentSequenceNo(notificationAssignment.getNotificationAssignmentPK().getSequenceNo());
                    assessorChargeDTOList = requestDTO.getChargesList();
                    NotificationAdjusterCharge notificationAdjusterCharge;
                    for (AssessorChargeDTO assessorChargeDTO : assessorChargeDTOList) {
                        notificationAdjusterCharge = new NotificationAdjusterCharge();
                        notificationAdjusterCharge.setSequenceNo(claimsCommonService.generateSequence(NotificationAdjusterCharge.SEQUENCE, branchCode));
                        notificationAdjusterCharge.setNotificationSequenceNo(claimNotification.getSequenceNo());
                        notificationAdjusterCharge.setNotificationAssignmentSequenceNo(notificationAssignment.getNotificationAssignmentPK().getSequenceNo());
                        notificationAdjusterCharge.setNotificationLossAdjusterSequenceNo(notificationLossAdjuster.getNotificationLossAdjusterPK().getNotificationSequenceNo());
                        Optional<UnitType> unitType = unitTypeRepository.findById(assessorChargeDTO.getUnitTypeCode());
                        Optional<ChargeType> chargeType = chargeTypeRepository.findById(assessorChargeDTO.getChargeTypeCode());
                        if (unitType.get() == null || chargeType == null) {
                            status = false;
                            break;
                        }
/*                        if (unitType == null || chargeType == null) {

                            status = false;
                            break;
                        }*/
                        notificationAdjusterCharge.setChargeTypeCode(assessorChargeDTO.getChargeTypeCode());
                        notificationAdjusterCharge.setUnitTypeCode(assessorChargeDTO.getUnitTypeCode());
                        notificationAdjusterCharge.setDateVisited(dateConversion.convertStringToDate(assessorChargeDTO.getDateVisited(), "dd-MM-yyyy HH:mm:ss"));
                        notificationAdjusterCharge.setUnitPrice(assessorChargeDTO.getUnitPrice());
                        notificationAdjusterCharge.setNoOfUnits(assessorChargeDTO.getNoOfUnits().intValue());
                        notificationAdjusterCharge.setCharges(assessorChargeDTO.getUnitPrice() * assessorChargeDTO.getNoOfUnits());
                        notificationAdjusterCharges.add(notificationAdjusterCharge);
                    }
                } else {
                    throw new NotificationNotFoundException("The Notification No or Notification Assignment No is not available");
                }
            }
            if (status) {
                if (isIntimation != null) {
                    if (isIntimation) {
                        lossAdjusterChargeRepository.saveAll(lossAdjusterCharges);
                    } else {
                        notificationAdjusterChargeRepository.saveAll(notificationAdjusterCharges);
                    }
                    response = new Response(200, true, "Assessor Charges were uploaded successfully");
                }
            } else {
                throw new ChargeTypesInvalidException("\"Charge Types are invalid");
            }
        }
        return response;
    }

}
