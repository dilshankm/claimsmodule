package com.infoinstech.claimsmodule.services.business;

import com.infoinstech.claimsmodule.dto.claimintimation.CustomerContactDTO;
import com.infoinstech.claimsmodule.dto.common.Response;
import com.infoinstech.claimsmodule.dto.common.ViewPolicyRequestDTO;
import com.infoinstech.claimsmodule.dto.policysearch.PolicyDetailsMoreDTO;
import com.infoinstech.claimsmodule.dto.policysearch.PolicySearchDTO;
import com.infoinstech.claimsmodule.dto.policysearch.PolicySearchResultDTO;
import com.infoinstech.claimsmodule.domain.model.salesmarketing.SystemUser;
import com.infoinstech.claimsmodule.domain.model.salesmarketing.master.SalesLocation;
import com.infoinstech.claimsmodule.domain.model.salesmarketing.reference.BusinessType;
import com.infoinstech.claimsmodule.domain.model.underwriting.master.Customer;
import com.infoinstech.claimsmodule.domain.model.underwriting.master.CustomerAddress;
import com.infoinstech.claimsmodule.domain.model.underwriting.transaction.*;
import com.infoinstech.claimsmodule.domain.repository.salesmarketing.SystemUserRepository;
import com.infoinstech.claimsmodule.domain.repository.salesmarketing.master.SalesLocationRepository;
import com.infoinstech.claimsmodule.domain.repository.salesmarketing.reference.BusinessTypeRepository;
import com.infoinstech.claimsmodule.domain.repository.underwriting.master.CustomerAddressRepository;
import com.infoinstech.claimsmodule.domain.repository.underwriting.transaction.*;
import com.infoinstech.claimsmodule.services.reference.ReferenceService;
import com.infoinstech.claimsmodule.services.util.DateConversion;
import com.infoinstech.claimsmodule.services.util.Formatter;
import oracle.jdbc.OracleTypes;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

import javax.sql.DataSource;
import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.ResultSet;
import java.util.*;

@Service
public class PolicyInquiryService {

    @Autowired
    private DataSource dataSource;

    @Autowired
    private DateConversion dateConversion;
    @Autowired
    private Formatter formatter;

    @Autowired
    private PolicyRepository policyRepository;
    @Autowired
    private PolicyLocationRepository policyLocationRepository;
    @Autowired
    private PolicyRiskRepository policyRiskRepository;
    @Autowired
    private EndorsementRepository endorsementRepository;
    @Autowired
    private EndorsementLocationRepository endorsementLocationRepository;
    @Autowired
    private EndorsementRiskRepository endorsementRiskRepository;
    @Autowired
    private CustomerAddressRepository customerAddressRepository;
    @Autowired
    private SalesLocationRepository salesLocationRepository;
    @Autowired
    private SystemUserRepository systemUserRepository;
    @Autowired
    private BusinessTypeRepository businessTypeRepository;

    @Autowired
    private PolicyRiskInformationService policyRiskInformationService;
    @Autowired
    private CustomerService customerService;
    @Autowired
    private ReferenceService referenceService;
    @Autowired
    private SystemUserService systemUserService;


    // Get all the Available Policies for the given Search Criteria
    public Response viewPolicySearchDetails(PolicySearchDTO policySearchDTO) {

        List<PolicySearchResultDTO> policySearchResultDTOList = new ArrayList<>();


        String query = "CALL PK_CL_CALL_CENTER.PU_GET_POLICY_DETAILS(?,?,?,?,?,?)";

        Response response;
        CallableStatement callableStatement;
        Connection connection;
        ResultSet resultSet;

        try {
            connection = dataSource.getConnection();
            callableStatement = connection.prepareCall(query);
            callableStatement.setString(1, policySearchDTO.getPolicyNo());
            callableStatement.setString(2, policySearchDTO.getCustomerNIC());
            callableStatement.setString(3, policySearchDTO.getCustomerName());
            callableStatement.setString(4, policySearchDTO.getRiskName());
            callableStatement.setString(5, policySearchDTO.getCustomerContact());
            callableStatement.registerOutParameter(6, OracleTypes.CURSOR);
            callableStatement.execute();

            resultSet = (ResultSet) callableStatement.getObject(6);

            while (resultSet != null && resultSet.next()) {

                PolicySearchResultDTO policySearchResultDTO = new PolicySearchResultDTO();

                policySearchResultDTO.setSequenceNo(resultSet.getString("POL_SEQ_NO"));
                policySearchResultDTO.setPolicyNo(resultSet.getString("POL_POLICY_NO"));
                policySearchResultDTO.setPolPeriodFrom(dateConversion.convertFromDateToString(resultSet.getDate("POL_PERIOD_FROM")));
                policySearchResultDTO.setPolPeriodTo(dateConversion.convertFromDateToString(resultSet.getDate("POL_PERIOD_TO")));
                policySearchResultDTO.setCustomerCode(resultSet.getString("POL_CUS_CODE"));
                policySearchResultDTO.setCustomerName(resultSet.getString("CLIENT_NAME"));
                policySearchResultDTO.setCustomerNIC(resultSet.getString("CUS_INDV_NIC_NO"));
                policySearchResultDTO.setRiskSequenceNo(resultSet.getString("PRS_SEQ_NO"));
                policySearchResultDTO.setRiskName(resultSet.getString("PRS_NAME"));
                policySearchResultDTO.setClassCode(resultSet.getString("POL_CLA_CODE"));

                String cancelledType = resultSet.getString("POL_CANCELLED_TYPE");
                String transactionStatus = resultSet.getString("POL_STATUS");

                if (cancelledType == null) {

                    cancelledType = "-";
                }
                Date policyToDate = resultSet.getDate("POL_PERIOD_TO");

                if (cancelledType.equals("P") && transactionStatus.equals("9")) {

                    policySearchResultDTO.setStatus(" POLICY CANCELLED ");

                } else if (policyToDate.before(new Date())) {

                    policySearchResultDTO.setStatus(" POLICY EXPIRED ");
                } else {

                    policySearchResultDTO.setStatus(" POLICY ACTIVE ");
                }
                policySearchResultDTOList.add(policySearchResultDTO);
            }

            policySearchResultDTOList.sort(Comparator.comparing(PolicySearchResultDTO::getPolPeriodTo).reversed());

            response = new Response(200, true, policySearchResultDTOList);

            connection.close();

        } catch (Exception e) {

            e.printStackTrace();
            response = new Response(400, false, e.getMessage());
        }

        return response;
    }

    // Get Policy Details for the given Risk Seq No and Policy Seq No
    public PolicyDetailsMoreDTO getPolicyDetails(ViewPolicyRequestDTO viewPolicyRequestDTO) {


        Policy policy = policyRepository.findBySequenceNo(viewPolicyRequestDTO.getPolicySequenceNo());
        Optional<PolicyRisk> policyRisk = policyRiskRepository.findByPolicyRiskPK_SequenceNo(viewPolicyRequestDTO.getRiskSequenceNo());
        PolicyLocationPK policyLocationPK = policyRisk.isPresent() ? new PolicyLocationPK(policyRisk.get().getPolicyRiskPK().getLocationSequenceNo(), policy.getSequenceNo()) : null;
        Optional<PolicyLocation> policyLocation = policyRisk.isPresent() ? policyLocationRepository.findByPolicyLocationPK(policyLocationPK) : Optional.empty();


        EndorsementRisk endorsementRisk = policyRisk.isPresent() ? null : endorsementRiskRepository.findByEndorsementRiskPK_SequenceNo(viewPolicyRequestDTO.getRiskSequenceNo());
        Endorsement endorsement = policyRisk.isPresent() ? null : endorsementRepository.findBySequenceNo(endorsementRisk.getEndorsementRiskPK().getEndorsementSequenceNo());
        EndorsementLocationPK endorsementLocationPK = policyRisk.isPresent() ? null : new EndorsementLocationPK(endorsementRisk.getEndorsementRiskPK().getLocationSequenceNo(), endorsement.getSequenceNo());
        EndorsementLocation endorsementLocation = policyRisk.isPresent() ? null : endorsementLocationRepository.findByEndorsementLocationPK(endorsementLocationPK);


        SalesLocation branch = salesLocationRepository.findByCode(policyRisk.isPresent() ? policy.getBranchCode() : endorsement.getBranchCode());

        SystemUser intermediary = systemUserRepository.findByCode(policyRisk.isPresent() ? policy.getIntermediaryCode() : endorsement.getIntermediaryCode());
//        SystemUser businessParty = systemUserRepository.findByCode(policy.getBusinessPartyCode());
        BusinessType businessType = businessTypeRepository.findByCode(policyRisk.isPresent() ? policy.getIntermediaryTypeCode() : endorsement.getIntermediaryTypeCode());
        Customer customer = policyRisk.isPresent() ? policy.getCustomer() : endorsement.getCustomer();
        CustomerAddress customerAddress = customerAddressRepository.findByCustomerCodeAndDefaultAddress(customer.getCustomerCode(), 'Y');


        PolicyDetailsMoreDTO policyDetailsMoreDTO = new PolicyDetailsMoreDTO();

        policyDetailsMoreDTO.setIntermediaryName(systemUserService.getSystemUserName(intermediary));
        policyDetailsMoreDTO.setIntermediaryType(businessType.getDescription());
        policyDetailsMoreDTO.setPolicyNo(policy.getPolicyNo());
        policyDetailsMoreDTO.setProductCode(policyRisk.isPresent() ? policy.getProductCode() : endorsement.getProductCode());
        policyDetailsMoreDTO.setProductDescription(policyRisk.isPresent() ? policy.getProduct().getDescription() : endorsement.getProduct().getDescription());
        policyDetailsMoreDTO.setProductClass(policyRisk.isPresent() ? policy.getRefClass().getDescription() : endorsement.getRefClass().getDescription());
        policyDetailsMoreDTO.setStatus(referenceService.getPolicyStatus(policyRisk.isPresent() ? policy.getStatus() : endorsement.getStatus()));
        policyDetailsMoreDTO.setBranch(branch.getDescription());
        //      policyDetailsMoreDTO.setBusinessParty(systemUserService.getSystemUserName(businessParty));
        policyDetailsMoreDTO.setBusinessParty("-");
        policyDetailsMoreDTO.setEffectiveFrom(dateConversion.convertFromDateToString(policyRisk.isPresent() ? policy.getPeriodFrom() : endorsement.getPeriodFrom()));

        policyDetailsMoreDTO.setCustomerCode(customer.getCustomerCode());
        policyDetailsMoreDTO.setCustomerName(customerService.getCustomerName(customer));
        policyDetailsMoreDTO.setCustomerNIC(customerService.getCustomerNIC(customer));
        CustomerContactDTO customerContactDTO = customerService.getCustomerContact(customer);
        policyDetailsMoreDTO.setCustomerContact(customerContactDTO.getContact());
        policyDetailsMoreDTO.setCustomerAddress(customerAddress.getLocationDescription());
        policyDetailsMoreDTO.setPeriodFrom(dateConversion.convertFromDateToString(policyRisk.isPresent() ? policy.getPeriodFrom() : endorsement.getPeriodFrom()));
        policyDetailsMoreDTO.setPeriodTo(dateConversion.convertFromDateToString(policyRisk.isPresent() ? policy.getPeriodTo() : endorsement.getPeriodTo()));
        policyDetailsMoreDTO.setTotalPremium(formatter.format(policyRisk.isPresent() ? policy.getTotalPremium() : endorsement.getTotalPremium()));
        policyDetailsMoreDTO.setTotalSumInsured(formatter.format(policyRisk.isPresent() ? policy.getSumInsured() : endorsement.getSumInsured()));

        policyDetailsMoreDTO.setVehicleNo(policyRisk.isPresent() ? policyRisk.get().getName() : endorsementRisk.getName());
        policyDetailsMoreDTO.setRiskOrder(policyRisk.isPresent() ? policyRisk.get().getRiskOrderNo() : endorsementRisk.getRiskOrderNo());
        policyDetailsMoreDTO.setSumInsured(formatter.format(policyRisk.isPresent() ? policyRisk.get().getSumInsured() : endorsementRisk.getSumInsured()));
        policyDetailsMoreDTO.setLocation(policyLocation.isPresent() ? policyLocation.get().getRefLocation().getDescription() :
                (endorsementLocation !=null) ? endorsementLocation.getRefLocation().getDescription() : "");
        policyDetailsMoreDTO.setAdditionalInformation(policyRisk.isPresent() ? policyRiskInformationService.getRiskInformation(policyRisk.get()) : policyRiskInformationService.getRiskInformation(endorsementRisk));


        return policyDetailsMoreDTO;

    }
}

