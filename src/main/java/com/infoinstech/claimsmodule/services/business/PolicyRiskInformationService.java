package com.infoinstech.claimsmodule.services.business;

import com.infoinstech.claimsmodule.dto.common.FieldDTO;
import com.infoinstech.claimsmodule.domain.model.common.reference.CommonReference;
import com.infoinstech.claimsmodule.domain.model.underwriting.master.ProductInformation;
import com.infoinstech.claimsmodule.domain.model.underwriting.temporary.TempPolicyRisk;
import com.infoinstech.claimsmodule.domain.model.underwriting.temporary.TempPolicyRiskInformation;
import com.infoinstech.claimsmodule.domain.model.underwriting.temporary.TempPolicyRiskPK;
import com.infoinstech.claimsmodule.domain.model.underwriting.transaction.*;
import com.infoinstech.claimsmodule.domain.repository.common.reference.CommonReferenceRepository;
import com.infoinstech.claimsmodule.domain.repository.underwriting.temporary.TempPolicyRiskInformationRepository;
import com.infoinstech.claimsmodule.domain.repository.underwriting.transaction.PolicyRiskInformationRepository;
import com.infoinstech.claimsmodule.services.util.DateConversion;
import com.infoinstech.claimsmodule.services.util.Settings;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
public class PolicyRiskInformationService {

    @Autowired
    private PolicyRiskInformationRepository policyRiskInformationRepository;
    @Autowired
    private TempPolicyRiskInformationRepository tempPolicyRiskInformationRepository;
    @Autowired
    private CommonReferenceRepository commonReferenceRepository;

    @Autowired
    private DateConversion dateConversion;
    @Autowired
    private Settings settings;


    public String getEngineNo(TempPolicyRisk tempPolicyRisk) {

        return getRiskInformation(tempPolicyRisk, "ENGINE NO");
    }



    /*public String getChassisNo(TempPolicyRisk tempPolicyRisk) {

        return getRiskInformation(tempPolicyRisk, "CHASSIS NO");
    }*/

    public String getChassisNo(Object risk) {

        return (risk instanceof PolicyRisk) ? getRiskInformation((PolicyRisk) risk, "CHASSIS NO") :
                (risk instanceof TempPolicyRisk) ? getRiskInformation((TempPolicyRisk) risk, "CHASSIS NO") : "";

    }

    public String getCubicCapacity(TempPolicyRisk tempPolicyRisk) {

        return getRiskInformation(tempPolicyRisk, "CAPACITY");
    }

    public String getYearOfManufacture(TempPolicyRisk tempPolicyRisk) {

        String riskInformation = (settings.getSite().equals("AMI")) ? "YEAR OF BUILD" : "YEAR OF MANUFACTURE";

        return getRiskInformation(tempPolicyRisk, riskInformation);
    }

    public String getMakeAndModel(TempPolicyRisk tempPolicyRisk) {

        String makeAndModel = "-";

        if(settings.getSite().equals("AMI")){

            makeAndModel = getRiskInformation(tempPolicyRisk, "MAKE") + " " + getRiskInformation(tempPolicyRisk, "MODEL");

        }
        else if(settings.getSite().equals("SANASA")){

            makeAndModel = getRiskInformation(tempPolicyRisk, "MAKE & MODEL");
        }

        return makeAndModel;
    }

    public String getUseOfVehicle(TempPolicyRisk tempPolicyRisk) {

        return getRiskInformation(tempPolicyRisk, "USE OF VEHICLE");
    }

    public String getVehicleCategory(TempPolicyRisk tempPolicyRisk) {

        return getRiskInformation(tempPolicyRisk, "VEHICLE CATEGORY");
    }

    public String getDateRegistered(TempPolicyRisk tempPolicyRisk) {

        return getRiskInformation(tempPolicyRisk, "DATE REGISTERED");
    }

    public String getRiskInformation(TempPolicyRisk tempPolicyRisk, String description) {

        TempPolicyRiskPK tempPolicyRiskPK = tempPolicyRisk.getTempPolicyRiskPK();

        Optional<TempPolicyRiskInformation> tempPolicyRiskInformation = tempPolicyRiskInformationRepository
                .findByTempPolicyRiskInformationPK_PolicySequenceNoAndTempPolicyRiskInformationPK_LocationSequenceNoAndTempPolicyRiskInformationPK_RiskSequenceNoAndDescription(
                        tempPolicyRiskPK.getPolicySequenceNo(), tempPolicyRiskPK.getLocationSequenceNo(), tempPolicyRiskPK.getSequenceNo(), description);

        return (tempPolicyRiskInformation.isPresent()) ? getRiskInformationValue(tempPolicyRiskInformation.get(), tempPolicyRiskInformation.get().getProductInformation()) : "-";

    }

    public String getRiskInformation(PolicyRisk policyRisk, String description) {

        PolicyRiskPK policyRiskPK = policyRisk.getPolicyRiskPK();

        Optional<PolicyRiskInformation> policyRiskInformation = policyRiskInformationRepository
                .findByPolicyRiskInformationPK_PolicySequenceNoAndPolicyRiskInformationPK_LocationSequenceNoAndPolicyRiskInformationPK_RiskSequenceNoAndDescription
                        (policyRiskPK.getPolicySequenceNo(), policyRiskPK.getLocationSequenceNo(), policyRiskPK.getSequenceNo(), description);

        return (policyRiskInformation.isPresent()) ? getRiskInformationValue(policyRiskInformation.get(), policyRiskInformation.get().getProductInformation()) : "-";

    }

    public List<FieldDTO> getRiskInformation(PolicyRisk policyRisk) {

        List<PolicyRiskInformation> policyRiskInformationList = policyRisk.getPolicyRiskInformationList();

        List<FieldDTO> additionalInformation = policyRiskInformationList.stream()
                .map(p -> new FieldDTO(p.getDescription(), getRiskInformationValue(p, p.getProductInformation())))
                .collect(Collectors.toList());

        return additionalInformation;
    }

    public List<FieldDTO> getRiskInformation(EndorsementRisk endorsementRisk) {

        List<EndorsementRiskInformation> endorsementRiskInformationList = endorsementRisk.getEndorsementRiskInformationList();

        List<FieldDTO> additionalInformation = endorsementRiskInformationList.stream()
                .map(p -> new FieldDTO(p.getDescription(), getRiskInformationValue(p, p.getProductInformation())))
                .collect(Collectors.toList());

        return additionalInformation;
    }

    public String getRiskInformationValue(EndorsementRiskInformation endorsementRiskInformation, ProductInformation productInformation) {

        String description = "";

        if (productInformation.getType().equals('C')) {

            if (productInformation.getReferenceTypeCode() == null) {

                description += endorsementRiskInformation.getCharacterValue() == null ? "-" : endorsementRiskInformation.getCharacterValue();
            } else {

                if (endorsementRiskInformation.getCharacterValue() != null) {

                    CommonReference commonReference = commonReferenceRepository.findByTypeAndCode(productInformation.getReferenceTypeCode(), endorsementRiskInformation.getCharacterValue());

                    description += commonReference.getDescription();
                }
            }


        } else if (productInformation.getType().equals('N')) {

            description += endorsementRiskInformation.getNumberValue() == null ? "-" : endorsementRiskInformation.getNumberValue();
        } else {

            description += endorsementRiskInformation.getDateValue() == null ? "-" : endorsementRiskInformation.getDateValue();
        }

        return description;
    }

    public String getRiskInformationValue(TempPolicyRiskInformation tempPolicyRiskInformation, ProductInformation productInformation) {

        String description = "";

        if (productInformation.getType().equals('C')) {

            if (productInformation.getReferenceTypeCode() == null) {

                description += tempPolicyRiskInformation.getCharacterValue() == null ? "-" : tempPolicyRiskInformation.getCharacterValue();
            } else {

                if (tempPolicyRiskInformation.getCharacterValue() != null) {

                    CommonReference commonReference = commonReferenceRepository.findByTypeAndCode(productInformation.getReferenceTypeCode(), tempPolicyRiskInformation.getCharacterValue());

                    description += commonReference.getDescription();
                }
            }


        } else if (productInformation.getType().equals('N')) {

            description += tempPolicyRiskInformation.getNumberValue() == null ? "-" : tempPolicyRiskInformation.getNumberValue();
        } else {

            description += tempPolicyRiskInformation.getDateValue() == null ? "-" : tempPolicyRiskInformation.getDateValue();
        }

        return description;
    }


    public String getRiskInformationValue(PolicyRiskInformation policyRiskInformation, ProductInformation productInformation) {

        String description = "";

        if (productInformation.getType().equals('C')) {

            if (productInformation.getReferenceTypeCode() == null) {

                description += policyRiskInformation.getCharacterValue() == null ? "-" : policyRiskInformation.getCharacterValue();
            } else {

                if (policyRiskInformation.getCharacterValue() != null) {

                    CommonReference commonReference = commonReferenceRepository.findByTypeAndCode(productInformation.getReferenceTypeCode(), policyRiskInformation.getCharacterValue());

                    description += commonReference.getDescription();
                }
            }


        } else if (productInformation.getType().equals('N')) {

            description += policyRiskInformation.getNumberValue() == null ? "-" : policyRiskInformation.getNumberValue();
        } else {

            description += policyRiskInformation.getDateValue() == null ? "-" : policyRiskInformation.getDateValue();
        }

        return description;
    }

}
