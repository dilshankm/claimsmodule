package com.infoinstech.claimsmodule.services.business;

import com.infoinstech.claimsmodule.dto.common.Response;
import com.infoinstech.claimsmodule.dto.questionnaire.*;
import com.infoinstech.claimsmodule.domain.model.claims.reference.RefAnswer;
import com.infoinstech.claimsmodule.domain.model.claims.reference.RefQuestion;
import com.infoinstech.claimsmodule.domain.model.claims.reference.RefQuestionType;
import com.infoinstech.claimsmodule.domain.model.claims.transaction.*;
import com.infoinstech.claimsmodule.domain.model.underwriting.master.ProductQuestion;
import com.infoinstech.claimsmodule.domain.packages.PK_CL_TAB_SEQUENCE;
import com.infoinstech.claimsmodule.domain.repository.claims.reference.RefAnswerRepository;
import com.infoinstech.claimsmodule.domain.repository.claims.reference.RefQuestionRepository;
import com.infoinstech.claimsmodule.domain.repository.claims.transaction.*;
import com.infoinstech.claimsmodule.domain.repository.underwriting.master.ProductQuestionRepository;
import com.infoinstech.claimsmodule.exceptions.types.NotEnoughInfoException;
import com.infoinstech.claimsmodule.exceptions.types.NotFoundException;
import com.infoinstech.claimsmodule.services.common.ClaimsCommonService;
import com.infoinstech.claimsmodule.services.parameters.ReceiptingParametersService;
import com.infoinstech.claimsmodule.services.reference.ReferenceService;
import com.infoinstech.claimsmodule.services.util.DateConversion;
import com.infoinstech.claimsmodule.services.util.Formatter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Service
public class QuestionnaireService {

    @Autowired
    private RefQuestionRepository refQuestionRepository;

    @Autowired
    private RefAnswerRepository refAnswerRepository;
    @Autowired
    private ProductQuestionRepository productQuestionRepository;

    @Autowired
    private ClaimIntimationRepository claimIntimationRepository;
    @Autowired
    private JobAssignmentRepository jobAssignmentRepository;
    @Autowired
    private AssignQuestionRepository assignQuestionRepository;
    @Autowired
    private ClaimNotificationRepository claimNotificationRepository;
    @Autowired
    private NotificationAssignmentRepository notificationAssignmentRepository;


    @Autowired
    private ClaimsCommonService claimsCommonService;
    @Autowired
    private ReceiptingParametersService receiptingParametersService;
    @Autowired
    private ReferenceService referenceService;
    @Autowired
    private DateConversion dateConversion;
    @Autowired
    private Formatter formatter;
    @Autowired
    private PK_CL_TAB_SEQUENCE pk_cl_tab_sequence;

    @Autowired
    private ClaimProvisionService claimProvisionService;

    public List<QuestionDTO> getQuestionnaire(String productCode) {

        List<QuestionDTO> questionDTOList = new ArrayList<>();
        List<AnswerDTO> answerDTOList;
        QuestionDTO questionDTO;

        List<ProductQuestion> productQuestions = productQuestionRepository.findByProductCodeAndStatus(productCode, 'A');
        List<RefAnswer> refAnswers;

        for (ProductQuestion productQuestion : productQuestions) {

            RefQuestion refQuestion = productQuestion.getRefQuestion();

            questionDTO = new QuestionDTO();
            questionDTO.setCode(refQuestion.getCode());
            questionDTO.setType(refQuestion.getRefQuestionType().getDescription());
            questionDTO.setHintOn(referenceService.getYesOrNo(refQuestion.getHintOn()));
            questionDTO.setHint((refQuestion.getHintOn() == 'Y') ? refQuestion.getHint() : "-");
            questionDTO.setMandatory(referenceService.getYesOrNo(refQuestion.getMandatory()));
            questionDTO.setDescription(refQuestion.getDescription());
            questionDTO.setDefaultAnswers(referenceService.getYesOrNo(refQuestion.getDefaultAnswers()));

            if (refQuestion.getDefaultAnswers() == 'Y') {

                refAnswers = refQuestion.getDefaultAnswerList();
                answerDTOList = new ArrayList<>();
                AnswerDTO answerDTO;

                for (RefAnswer refAnswer : refAnswers) {

                    answerDTO = new AnswerDTO();
                    answerDTO.setCode(refAnswer.getCode());
                    answerDTO.setDescription(refAnswer.getAnswer());

                    answerDTOList.add(answerDTO);
                }

                questionDTO.setAnswerList(answerDTOList);
            }

            questionDTOList.add(questionDTO);
        }

        return questionDTOList;
    }

    public List<ViewQuestionDTO> viewQuestionnaire(JobAssignment jobAssignment, ClaimIntimation claimIntimation) {

        AssignQuestion questionWithMaxRevisionNo = assignQuestionRepository.findFirstByJobAssignmentSequenceNoOrderByRevisionNoDesc(jobAssignment.getJobAssignmentPK().getSequenceNo());

        List<ViewQuestionDTO> viewQuestions = new ArrayList<>();
        ViewQuestionDTO viewQuestionDTO;

        if (questionWithMaxRevisionNo != null) {

            List<AssignQuestion> assignQuestions = assignQuestionRepository.
                    findByJobAssignmentSequenceNoAndRevisionNo(jobAssignment.getJobAssignmentPK().getSequenceNo(), questionWithMaxRevisionNo.getRevisionNo());

            viewQuestions = getAssignedQuestions(assignQuestions);
        } else {

            List<ProductQuestion> productQuestions = productQuestionRepository.findByProductCodeAndStatus(claimIntimation.getProductCode(), 'A');

            viewQuestions = getProductQuestions(productQuestions);
        }

        return viewQuestions;
    }


    public List<ViewQuestionDTO> viewQuestionnaire(NotificationAssignment notificationAssignment, ClaimNotification claimNotification) {

        AssignQuestion questionWithMaxRevisionNo = assignQuestionRepository.findFirstByNotificationAssignmentSequenceNoOrderByRevisionNoDesc(notificationAssignment.getNotificationAssignmentPK().getSequenceNo());

        List<ViewQuestionDTO> viewQuestions = new ArrayList<>();
        ViewQuestionDTO viewQuestionDTO;

        if (questionWithMaxRevisionNo != null) {

            List<AssignQuestion> assignQuestions = assignQuestionRepository.
                    findByNotificationAssignmentSequenceNoAndRevisionNo(notificationAssignment.getNotificationAssignmentPK().getSequenceNo(), questionWithMaxRevisionNo.getRevisionNo());

            viewQuestions = getAssignedQuestions(assignQuestions);
        } else {

            List<ProductQuestion> productQuestions = productQuestionRepository.findByProductCodeAndStatus(claimNotification.getCoverNoteType(), 'A');

            viewQuestions = getProductQuestions(productQuestions);
        }

        return viewQuestions;
    }

    public Response saveQuestionnaire(UploadQuestionnaireRequestDTO uploadQuestionnaireRequestDTO) {

        Response response;
        int revisionNo = 1;

        if (uploadQuestionnaireRequestDTO.getClaimNo().substring(0, 2).equals("CL")) {

            ClaimIntimation claimIntimation = claimIntimationRepository.findByClaimNo(uploadQuestionnaireRequestDTO.getClaimNo());
            JobAssignment jobAssignment = jobAssignmentRepository.findByJobNo(uploadQuestionnaireRequestDTO.getJobNo());

            if (claimIntimation != null && jobAssignment != null && uploadQuestionnaireRequestDTO.getQuestions() != null) {

                AssignQuestion questionWithMaxRevisionNo = assignQuestionRepository.
                        findFirstByClaimSequenceNoAndJobAssignmentSequenceNoOrderByRevisionNoDesc(claimIntimation.getSequenceNo(), jobAssignment.getJobAssignmentPK().getSequenceNo());

                if (questionWithMaxRevisionNo != null) {
                    revisionNo = Integer.parseInt(questionWithMaxRevisionNo.getRevisionNo()) + 1;
                }

                List<UploadQuestionDTO> questionsList = uploadQuestionnaireRequestDTO.getQuestions();

                List<AssignQuestion> assignQuestions = new ArrayList<>();


                for (UploadQuestionDTO question : questionsList) {

                    ProductQuestion productQuestion =
                            productQuestionRepository.findByQuestionCodeAndProductCodeAndStatus(question.getQuestionCode(), claimIntimation.getProductCode(), 'A');

                    if (productQuestion != null) {

                        RefQuestion refQuestion = productQuestion.getRefQuestion();

                        ValidateAnswerDTO validateAnswerDTO = validateAnswer(question, refQuestion);

                        if (validateAnswerDTO.isValidation()) {

                            AssignQuestion assignQuestion = new AssignQuestion();

                            if (validateAnswerDTO.getDefaultAnswer() == 'Y') {

                                assignQuestion.setAnswerCode(question.getAnswerCode());

                                Optional<RefAnswer> refAnswer = refAnswerRepository.findById(question.getAnswerCode());

                                switch (question.getQuestionCode()) {

                                    case "QS002":
                                        claimIntimation.setClaimDoubtful(referenceService.reverseYesOrNo(refAnswer.get().getAnswer(), claimIntimation.getClaimDoubtful()));
                                        break;

                                    case "QS003":
                                        claimIntimation.setPoliceReport(referenceService.reverseYesOrNo(refAnswer.get().getAnswer(), claimIntimation.getPoliceReport()));
                                        break;

                                    case "QS005":
                                        claimIntimation.setInsuredAtFault(referenceService.reverseYesOrNo(refAnswer.get().getAnswer(), claimIntimation.getInsuredAtFault()));
                                        break;

                                    case "QS006":
                                        claimIntimation.setTotalLoss(referenceService.reverseYesOrNo(refAnswer.get().getAnswer(), claimIntimation.getTotalLoss()));
                                        break;
                                }

                            } else {
                                assignQuestion.setAnswer(question.getAnswerValue());
                            }

                            assignQuestion.setSequenceNo(claimsCommonService.generateSequence(AssignQuestion.SEQUENCE, receiptingParametersService.getBaseAccountBranch()));
                            assignQuestion.setQuestionCode(refQuestion.getCode());
                            assignQuestion.setDescription(refQuestion.getDescription());
                            assignQuestion.setJobAssignmentNo(jobAssignment.getJobNo());
                            assignQuestion.setJobAssignmentSequenceNo(jobAssignment.getJobAssignmentPK().getSequenceNo());
                            assignQuestion.setClaimSequenceNo(claimIntimation.getSequenceNo());
                            assignQuestion.setRevisionNo(Integer.toString(revisionNo));

                            assignQuestions.add(assignQuestion);

                            if (question.getQuestionCode().equals("QS001")) {

                                Double acrAmount = Double.parseDouble(question.getAnswerValue());

                                claimProvisionService.changeProvisionAmount(claimIntimation.getSequenceNo(), acrAmount);

                                claimIntimation.setEstimatedAmount(acrAmount);
                            }

                        } else {
                            throw new NotFoundException(validateAnswerDTO.getMessage());
                            //return new Response(400, true, validateAnswerDTO.getMessage());
                        }

                    } else {
                        throw new NotFoundException("The Question ReferenceCode " + question.getQuestionCode() + " does not exists");
                    }
                }


                assignQuestionRepository.saveAll(assignQuestions);
                claimIntimationRepository.save(claimIntimation);

                response = new Response(200, true, "The Questionnaire was uploaded successfully.");
            } else {

                throw new NotEnoughInfoException("There is no adequate Information in the Request");
            }


        } else {

            ClaimNotification claimNotification = claimNotificationRepository.findByNotificationNo(uploadQuestionnaireRequestDTO.getClaimNo());
            NotificationAssignment notificationAssignment = notificationAssignmentRepository.findByJobNo(uploadQuestionnaireRequestDTO.getJobNo());

            if (claimNotification != null && notificationAssignment != null && uploadQuestionnaireRequestDTO.getQuestions() != null) {

                AssignQuestion questionWithMaxRevisionNo = assignQuestionRepository.
                        findFirstByClaimSequenceNoAndJobAssignmentSequenceNoOrderByRevisionNoDesc(claimNotification.getSequenceNo(), notificationAssignment.getNotificationAssignmentPK().getSequenceNo());


                if (questionWithMaxRevisionNo != null) {
                    revisionNo = Integer.parseInt(questionWithMaxRevisionNo.getRevisionNo()) + 1;
                }

                List<UploadQuestionDTO> questionsList = uploadQuestionnaireRequestDTO.getQuestions();

                List<AssignQuestion> assignQuestions = new ArrayList<>();


                for (UploadQuestionDTO question : questionsList) {

                    ProductQuestion productQuestion =
                            productQuestionRepository.findByQuestionCodeAndProductCodeAndStatus(question.getQuestionCode(), claimNotification.getCoverNoteType(), 'A');

                    if (productQuestion != null) {

                        RefQuestion refQuestion = productQuestion.getRefQuestion();

                        ValidateAnswerDTO validateAnswerDTO = validateAnswer(question, refQuestion);

                        if (validateAnswerDTO.isValidation()) {

                            AssignQuestion assignQuestion = new AssignQuestion();


                            if (validateAnswerDTO.getDefaultAnswer() == 'Y') {

                                assignQuestion.setAnswerCode(question.getAnswerCode());
                            } else {
                                assignQuestion.setAnswer(question.getAnswerValue());
                            }

                            assignQuestion.setSequenceNo(claimsCommonService.generateSequence(AssignQuestion.SEQUENCE, receiptingParametersService.getBaseAccountBranch()));
                            assignQuestion.setQuestionCode(refQuestion.getCode());
                            assignQuestion.setDescription(refQuestion.getDescription());
                            assignQuestion.setNotificationAssignmentNo(notificationAssignment.getJobNo());
                            assignQuestion.setNotificationAssignmentSequenceNo(notificationAssignment.getNotificationAssignmentPK().getSequenceNo());
                            assignQuestion.setNotificationSequenceNo(claimNotification.getSequenceNo());
                            assignQuestion.setRevisionNo(Integer.toString(revisionNo));

                            assignQuestions.add(assignQuestion);

                        } else {

                            return new Response(400, true, validateAnswerDTO.getMessage());
                        }

                    } else {

                        return new Response(400, true, "The Question ReferenceCode " + question.getQuestionCode() + " does not exists");
                    }
                }


                assignQuestionRepository.saveAll(assignQuestions);

                response = new Response(200, true, "The Questionnaire was uploaded successfully.");

            } else {

                throw new NotEnoughInfoException("There is no adequate Information in the Request");
            }
        }

        return response;
    }


    public ValidateAnswerDTO validateAnswer(UploadQuestionDTO uploadQuestionDTO, RefQuestion refQuestion) {


        ValidateAnswerDTO validateAnswerDTO = new ValidateAnswerDTO();

        validateAnswerDTO.setValidation(false);
        validateAnswerDTO.setDefaultAnswer('N');
        validateAnswerDTO.setMessage("The Answer is not Valid");


        if (uploadQuestionDTO.getAnswerValue() != null || uploadQuestionDTO.getAnswerCode() != null) {

            RefQuestionType questionType = refQuestion.getRefQuestionType();


            if (questionType.getCode().equalsIgnoreCase("QT005") || questionType.getCode().equalsIgnoreCase("QT006") || questionType.getCode().equalsIgnoreCase("QT007")) {

                List<RefAnswer> defaultAnswers = refQuestion.getDefaultAnswerList();

                for (RefAnswer defaultAnswer : defaultAnswers) {

                    if (defaultAnswer.getCode().equalsIgnoreCase(uploadQuestionDTO.getAnswerCode())) {

                        validateAnswerDTO.setValidation(true);
                        validateAnswerDTO.setDefaultAnswer('Y');
                        break;
                    }
                }

                validateAnswerDTO.setMessage("The Answer ReferenceCode is not Valid or Not Available");
            } else if (questionType.getCode().equalsIgnoreCase("QT001") || questionType.getCode().equalsIgnoreCase("QT002")) {

                if (uploadQuestionDTO.getAnswerValue() != null || uploadQuestionDTO.getAnswerValue().equals("")) {

                    validateAnswerDTO.setValidation(true);
                } else {

                    validateAnswerDTO.setMessage("Not a Valid Text Answer");
                }
            } else if (questionType.getCode().equalsIgnoreCase("QT003")) {

                try {

                    Double.parseDouble(uploadQuestionDTO.getAnswerValue());
                    validateAnswerDTO.setValidation(true);

                } catch (Exception e) {

                    validateAnswerDTO.setValidation(false);
                    validateAnswerDTO.setMessage("Not a valid Number Text Answer");

                    e.printStackTrace();


                }


            } else if (questionType.getCode().equalsIgnoreCase("QT004")) {

                try {

                    dateConversion.convertStringToDate(uploadQuestionDTO.getAnswerValue());

                    validateAnswerDTO.setValidation(true);

                } catch (Exception e) {

                    e.printStackTrace();

                    validateAnswerDTO.setValidation(false);
                    validateAnswerDTO.setMessage("Not a Valid Date Answer");
                }

            }
        }

        return validateAnswerDTO;
    }

    public List<ViewQuestionDTO> getAssignedQuestions(List<AssignQuestion> assignQuestions) {


        ViewQuestionDTO viewQuestionDTO;
        List<ViewQuestionDTO> viewQuestions = new ArrayList<>();

        for (AssignQuestion assignQuestion : assignQuestions) {

            RefQuestionType refQuestionType = assignQuestion.getRefQuestion().getRefQuestionType();

            viewQuestionDTO = new ViewQuestionDTO();
            viewQuestionDTO.setQuestion(assignQuestion.getDescription());

            if (assignQuestion.getAnswer() == null) {

                Optional<RefAnswer> defaultAnswer = refAnswerRepository.findById(assignQuestion.getAnswerCode());

                viewQuestionDTO.setAnswer(defaultAnswer.get().getAnswer());

            } else {

                if (refQuestionType.getCode().equals("QT003")) {

                    viewQuestionDTO.setAnswer(formatter.format(Double.parseDouble(assignQuestion.getAnswer())));
                } else {

                    viewQuestionDTO.setAnswer(assignQuestion.getAnswer());
                }


            }

            viewQuestions.add(viewQuestionDTO);

        }

        return viewQuestions;
    }

    public List<ViewQuestionDTO> getProductQuestions(List<ProductQuestion> productQuestions) {

        ViewQuestionDTO viewQuestionDTO;
        List<ViewQuestionDTO> viewQuestions = new ArrayList<>();

        for (ProductQuestion productQuestion : productQuestions) {

            RefQuestion refQuestion = productQuestion.getRefQuestion();

            viewQuestionDTO = new ViewQuestionDTO();
            viewQuestionDTO.setQuestion(refQuestion.getDescription());
            viewQuestionDTO.setAnswer("NOT ANSWERED");

            viewQuestions.add(viewQuestionDTO);
        }

        return viewQuestions;
    }

}
