package com.infoinstech.claimsmodule.services.business;

import com.infoinstech.claimsmodule.domain.model.salesmarketing.SystemUser;
import com.infoinstech.claimsmodule.domain.repository.salesmarketing.SystemUserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class SalesForceService {

    @Autowired
    private SystemUserRepository systemUserRepository;

    public String getSystemUserName(SystemUser systemUser) {

        String initials = systemUser.getInitials() == null ? "" : systemUser.getInitials() + " ";
        String surname = systemUser.getSurname() == null ? "" : systemUser.getSurname();

        return initials + surname;
    }
}
