package com.infoinstech.claimsmodule.services.business;

import com.infoinstech.claimsmodule.domain.model.salesmarketing.SystemUser;
import org.springframework.stereotype.Service;

@Service
public class SystemUserService {

    public String getSystemUserName(SystemUser systemUser) {

        String initials = systemUser.getInitials() == null ? "" : systemUser.getInitials() + " ";
        String surname = systemUser.getSurname() == null ? "" : systemUser.getSurname();

        return initials + surname;
    }
}
