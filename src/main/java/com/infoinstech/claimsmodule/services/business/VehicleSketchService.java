package com.infoinstech.claimsmodule.services.business;

import com.infoinstech.claimsmodule.dto.common.ReferenceDTO;
import com.infoinstech.claimsmodule.dto.common.Response;
import com.infoinstech.claimsmodule.domain.model.underwriting.master.ProductVehicleSketch;
import com.infoinstech.claimsmodule.domain.model.claims.reference.RefVehicleSketch;
import com.infoinstech.claimsmodule.domain.model.claims.transaction.ClaimIntimation;
import com.infoinstech.claimsmodule.domain.model.claims.transaction.ClaimNotification;
import com.infoinstech.claimsmodule.domain.model.claims.transaction.DamageArea;
import com.infoinstech.claimsmodule.domain.packages.PK_CL_TAB_SEQUENCE;
import com.infoinstech.claimsmodule.domain.repository.underwriting.master.ProductVehicleSketchRepository;
import com.infoinstech.claimsmodule.domain.repository.claims.reference.RefVehicleSketchRepository;
import com.infoinstech.claimsmodule.domain.repository.claims.transaction.DamageAreaRepository;
import com.infoinstech.claimsmodule.exceptions.types.NotFoundException;
import com.infoinstech.claimsmodule.services.common.ClaimsCommonService;
import com.infoinstech.claimsmodule.services.parameters.ReceiptingParametersService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.*;
import java.util.stream.Collectors;

/**
 * Created by thilina on 21/12/17.
 */
@Service
public class VehicleSketchService {

    @Autowired
    private RefVehicleSketchRepository refVehicleSketchRepository;
    @Autowired
    private PK_CL_TAB_SEQUENCE pkClTabSequence;
    @Autowired
    private DamageAreaRepository damageAreaRepository;
    @Autowired
    private ProductVehicleSketchRepository productVehicleSketchRepository;

    @Autowired
    private ClaimsCommonService claimsCommonService;
    @Autowired
    private ReceiptingParametersService receiptingParametersService;

    public Response saveSketchType(RefVehicleSketch refVehicleSketch) {
        Response response = null;
        refVehicleSketch.setCode(refVehicleSketch.getCode());
        refVehicleSketchRepository.save(refVehicleSketch);
        response = new Response(200, true, "saved");
        return response;
    }

    public Response saveDamageSketch(DamageArea damageArea) {
        Response response = null;
        damageArea.setSequenceNo(claimsCommonService.generateSequence(DamageArea.SEQUENCE));
        damageArea.setUserType("Intimation");
        damageAreaRepository.save(damageArea);
        response = new Response(200, true, "saved");
        return response;
    }

    public Response updateSketch(DamageArea damageArea) {
        Response response = null;
        DamageArea damageAreaInDb = damageAreaRepository.findByClaimNoAndUserType(damageArea.getClaimNo(), damageArea.getUserType());
        damageAreaInDb.setImage(damageArea.getImage());
        damageAreaRepository.save(damageAreaInDb);
        response = new Response(200, true, "updated");
        return response;
    }


    public Response viewSketchType(ProductVehicleSketch productVehicleSketch) {
        Response response = null;
        List<ProductVehicleSketch> productVehicleSketches =
                productVehicleSketchRepository.findAllByProductCode(productVehicleSketch.getProductCode());
//            Map<String, byte[]> skType = new HashMap<>();
        ArrayList<ReferenceDTO> vehicleSketchesList = new ArrayList<>();
        for (ProductVehicleSketch prdSketch : productVehicleSketches) {
            if (Base64.getEncoder().encodeToString(prdSketch.getRefVehicleSketch().getImage()) != null) {
                ReferenceDTO referenceDTO = new ReferenceDTO(
                        Base64.getEncoder().encodeToString(prdSketch.getRefVehicleSketch().getImage()),
                        prdSketch.getRefVehicleSketch().getDescription()
                );
                vehicleSketchesList.add(referenceDTO);
            }
//                skType.put(prdSketch.getRefVehicleSketch().getDescription(),
//                        prdSketch.getRefVehicleSketch().getImage());
        }
        if (vehicleSketchesList.size() > 0) {
            response = new Response(200, true, "", vehicleSketchesList);
        } else {
            throw new NotFoundException("No records available");
        }
        return response;
    }

    public Response viewSketch(DamageArea damageArea) {
        Response response = null;
        List<DamageArea> damageAreas = damageAreaRepository.findAllByClaimNo(damageArea.getClaimNo());
        response = new Response(200, true, damageAreas);
        return response;
    }

    public Response viewAllSketchType() {
        Response response = null;
            /*List<RefVehicleSketch> sketchTypes = refVehicleSketchRepository.findAll();
            List<SketchTypeDto> sketchTypeDtos = new ArrayList<>();
            // or
            Map<String, List<String>> skType = new HashMap<>();

            List<String> sketchTypeArray = new ArrayList<>();


            for (RefVehicleSketch sketchType: sketchTypes ) {
                if(sketchTypeArray.stream().noneMatch(sketchType.getVehicleType()::equals)) {
                    SketchTypeDto sketchTypeDto = new SketchTypeDto();
                    sketchTypeDto.setType(sketchType.getVehicleType());
                    List<String> listModel = sketchTypes.stream().filter(s -> s.getVehicleType()
                            .equalsIgnoreCase(sketchType.getVehicleType()))
                            .map(RefVehicleSketch ::getVehicleModel)
                            .collect(Collectors.toList());
                    sketchTypeDto.setModel(listModel);

                    sketchTypeDtos.add(sketchTypeDto);
                    // or
                    skType.put(sketchType.getVehicleType(), listModel);
                }
                sketchTypeArray.add(sketchType.getVehicleType());
            }*/
        List<RefVehicleSketch> sketchTypes = refVehicleSketchRepository.findAll();
        List<String> sketchType = sketchTypes.stream().map(RefVehicleSketch::getDescription).collect(Collectors.toList());
        response = new Response(200, true, sketchType);
        return response;
    }

    public String getSketchType(ClaimIntimation claimIntimation, ClaimNotification claimNotification) {

        String sketchType;
        String productCode = claimIntimation != null ? claimIntimation.getProductCode() : claimNotification.getCoverNoteType();


        return null;

    }

    public ReferenceDTO getVehicleSketch(String referenceCode) {

        Optional<RefVehicleSketch> refVehicleSketch = refVehicleSketchRepository.findById(referenceCode);

        ReferenceDTO referenceDTO = null;

        if (refVehicleSketch.get() != null) {

            referenceDTO = new ReferenceDTO();
            referenceDTO.setCode(refVehicleSketch.get().getCode());
            referenceDTO.setDescription(Base64.getEncoder().encodeToString(refVehicleSketch.get().getImage()));
        }

        return referenceDTO;
    }

    public void saveDamageSketch(ClaimIntimation claimIntimation, ClaimNotification claimNotification, byte[] image) {

        DamageArea damageArea = new DamageArea();


        damageArea.setUserType("Intimation");

        if (claimIntimation != null) {

            damageArea.setSequenceNo(claimsCommonService.generateSequence(DamageArea.SEQUENCE, claimIntimation.getBranchCode()));

            damageArea.setIntimationSequenceNo(claimIntimation.getSequenceNo());
            damageArea.setClaimNo(claimIntimation.getClaimNo());
        } else {

            damageArea.setSequenceNo(claimsCommonService.generateSequence(DamageArea.SEQUENCE, receiptingParametersService.getBaseAccountBranch()));

            damageArea.setNotificationSequenceNo(claimNotification.getSequenceNo());
            damageArea.setNotificationNo(claimNotification.getNotificationNo());
        }

        if (image != null) {

            damageArea.setImage(image);
            damageAreaRepository.save(damageArea);
        }
    }

}
