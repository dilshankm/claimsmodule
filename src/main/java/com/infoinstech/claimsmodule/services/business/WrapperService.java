package com.infoinstech.claimsmodule.services.business;

import com.infoinstech.claimsmodule.domain.model.underwriting.temporary.TempPolicy;
import com.infoinstech.claimsmodule.domain.packages.PK_CL_T_INTIMATION;
import com.infoinstech.claimsmodule.domain.repository.underwriting.temporary.*;
import com.infoinstech.claimsmodule.services.common.ClaimsCommonService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.StoredProcedureQuery;
import javax.sql.DataSource;
import java.math.BigDecimal;
import java.util.Date;
import java.util.HashMap;

@Service
public class WrapperService {

    @Autowired
    private DataSource dataSource;

    @PersistenceContext
    private EntityManager entityManager;

    @Autowired
    private PK_CL_T_INTIMATION pk_cl_t_intimation;

    @Autowired
    private TempPolicyRepository tempPolicyRepository;
    @Autowired
    private TempPolicyLocationRepository tempPolicyLocationRepository;
    @Autowired
    private TempPolicyRiskRepository tempPolicyRiskRepository;
    @Autowired
    private TempPolicyRiskInformationRepository tempPolicyRiskInformationRepository;
    @Autowired
    private TempPolicyPerilRepository tempPolicyPerilRepository;
    @Autowired
    TempPolicyExcessTypeRepository tempPolicyExcessTypeRepository;

    @Autowired
    private ClaimsCommonService claimsCommonService;


    //Get the Policy Picture at time of the given Loss Time
    public TempPolicy buildPolicy(String policyNo, Date lossDate) {

        String sessionId = "";
//        String policySequenceNo = pk_cl_t_intimation.getPolicySequenceNo(policyNo, lossDate);
       String policySequenceNo = claimsCommonService.getPolicySequenceNo(policyNo, lossDate);
        HashMap<Integer, String> hashMap = getPolicyRiskCurrentPicture(policySequenceNo, lossDate);

        String policySeqNo = hashMap.get(1);
        sessionId = hashMap.get(2);

        TempPolicy tempPolicy =
                tempPolicyRepository.findBySequenceNo(policySeqNo);

        return tempPolicy;
    }

    public HashMap getPolicyRiskCurrentPicture(String policySequence, Date lossDate) {
        HashMap<Integer, String> hashMap = new HashMap<Integer, String>();
        deleteDataFromTempPolicyTables();
        try {
            StoredProcedureQuery query = this.entityManager.createNamedStoredProcedureQuery("PU_GET_POLICY_RISK_CURRENT_PICTURE");

            query.setParameter("wkpolseqno", policySequence);
            query.setParameter("wkdate", lossDate);
            query.setParameter("wkbase", "R");
            query.setParameter("wkins_locations", "Y");
            query.setParameter("wkins_risks", "Y");
            query.setParameter("wkins_perils", "Y");
            query.setParameter("wkins_sub_perils", "Y");
            query.setParameter("wkins_peril_inv", "Y");
            query.setParameter("wkins_peril_inv_cat", "Y");
            query.setParameter("wkins_inventory", "Y");
            query.setParameter("wkins_inv_det", "Y");
            query.setParameter("wkins_occupations", "Y");
            query.setParameter("wkins_sub_occ", "Y");
            query.setParameter("wkins_funds", "Y");
            query.setParameter("wkins_comm_funds", "Y");
            query.setParameter("wkins_oth_chg", "Y");
            query.setParameter("wkins_taxes", "Y");
            query.setParameter("wkins_tax_funds", "Y");
            query.setParameter("wkins_com_info", "Y");
            query.setParameter("wkins_com_info_det", "Y");
            query.setParameter("wkins_fin_int", "Y");
            query.setParameter("wkins_docs", "Y");
            query.setParameter("wkins_gen_perils", "Y");
            query.setParameter("wkins_risk_fin_int", "Y");
            query.setParameter("wkins_info", "Y");
            query.setParameter("wkins_info_det", "Y");
            query.setParameter("wkins_clauses", "Y");
            query.setParameter("wkins_warranties", "Y");
            query.setParameter("wkins_conditions", "Y");
            query.setParameter("wkins_others", "Y");
            query.setParameter("wkins_commissions", "Y");
            query.setParameter("wkins_targets", "Y");
            query.setParameter("wkins_rel_pol", "Y");
            query.execute();

            String policySeqNo = (String) query.getOutputParameterValue("wknewpolseq");
            BigDecimal session = (BigDecimal) query.getOutputParameterValue("wksessionid");

            String sessionId = null;

            if (session != null) {

                sessionId = Integer.toString(session.intValue());
            }


            hashMap.put(1, policySeqNo);
            hashMap.put(2, sessionId);



        } catch (Exception ex) {

            ex.printStackTrace();
        }
        finally {

            this.entityManager.close();
        }
        return hashMap;
    }

    public void deleteDataFromTempPolicyTables() {

        tempPolicyRepository.deleteAllInBatch();
        tempPolicyLocationRepository.deleteAllInBatch();
        tempPolicyRiskInformationRepository.deleteAllInBatch();
        tempPolicyRiskRepository.deleteAllInBatch();
        tempPolicyExcessTypeRepository.deleteAllInBatch();
        tempPolicyPerilRepository.deleteAllInBatch();

    }
}
