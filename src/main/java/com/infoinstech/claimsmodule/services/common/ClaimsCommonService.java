package com.infoinstech.claimsmodule.services.common;

import com.infoinstech.claimsmodule.domain.model.underwriting.transaction.Policy;
import com.infoinstech.claimsmodule.domain.repository.underwriting.transaction.PolicyRepository;
import com.infoinstech.claimsmodule.services.parameters.UnderwritingParametersService;
import com.infoinstech.claimsmodule.services.util.DateConversion;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.*;

@Service
public class ClaimsCommonService {

    @Autowired
    private PolicyRepository policyRepository;

    @Autowired
    private DateConversion dateConversion;
    @Autowired
    private SequenceGeneration sequenceGeneration;
    @Autowired
    private UnderwritingParametersService underwritingParametersService;

    public String generateSequence(String sequence) {


        int nextValue = sequenceGeneration.getNextValue(sequence);

        String formattedSequence = String.format("%012d", nextValue);

        String formattedDate = dateConversion.convertFromDateToString(new Date(), "yyyyMMdd");

        return formattedDate + formattedSequence;

    }

    public String generateSequence(String sequence, String branchCode) {

        int nextValue = sequenceGeneration.getNextValue(sequence);

        String formattedSequence = String.format("%08d", nextValue);

        String formattedBranchCode = String.format("%5s", branchCode);

        String formattedDate = dateConversion.convertFromDateToString(new Date(), "yy");

        return formattedBranchCode.replaceAll(" ", "0") + formattedDate + formattedSequence;

    }

    public String generateSequence(String prefix, String sequence, String branchCode) {


        String formattedSequence = String.format("%08d", sequenceGeneration.getNextValue(sequence));

        String formattedBranchCode = String.format("%5s", branchCode);

        String formattedDate = dateConversion.convertFromDateToString(new Date(), "yy");

        String sequenceNo = prefix + formattedBranchCode.replaceAll(" ", "0") + formattedDate + formattedSequence;

        return sequenceNo;

    }

    public String getPolicySequenceNo(String policyNo, Date lossDate){

        String policySequenceNo = "";

        List<String> statuses = new ArrayList<>(Arrays.asList(
                underwritingParametersService.getPolicyCancelledStatus(),underwritingParametersService.getPolicyClosedStatus()));

        Optional<Policy> policy = policyRepository.findByPolicyNoAndStatusNotIn(policyNo,statuses);

        if(policy.isPresent()){

            policySequenceNo = policy.get().getSequenceNo();
        }
        else {

            policy = policyRepository.findByPolicyNoAndCancelledTypeAndStatusInAndCancelEffectiveDateGreaterThanEqualOrderByCancelEffectiveDateDesc(
                    policyNo,'P',statuses,lossDate);

            if(policy.isPresent()){

                policySequenceNo = policy.get().getSequenceNo();
            }
        }

        return policySequenceNo;
    }



/*    FUNCTION FN_MAKE_SEQUENCE (WkSeqNo IN NUMBER,WkBranchCode IN sm_m_salesloc.slc_brn_code%type) RETURN VARCHAR2 IS

    WkNewSeqNo  VARCHAR2(15);
    WkNewBranch sm_m_salesloc.slc_brn_code%type;
    WKCurrYear  VARCHAR2(2);

    BEGIN

    SELECT TO_CHAR(SYSDATE,'YY') INTO WkCurrYear FROM DUAL;

    WkNewSeqNo  := TO_CHAR(WkSeqNo);
    WkNewSeqNo  := LPAD(WkNewSeqNo, 8, '0');
    WkNewBranch := LPAD(WkBranchCode, 5, '0');
    WkNewSeqNo := WkNewBranch||WkCurrYear||WkNewSeqNo;

    RETURN WkNewSeqNo;

    END;*/
}
