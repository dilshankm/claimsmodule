package com.infoinstech.claimsmodule.services.common;

import org.springframework.stereotype.Service;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import java.math.BigDecimal;

@Service
public class SequenceGeneration {

    @PersistenceContext
    private EntityManager entityManager;

    public int getNextValue(String sequence) {

        Query q = entityManager.createNativeQuery("SELECT " + sequence + ".nextval from DUAL");
        BigDecimal nextValue = (BigDecimal) q.getSingleResult();
        return nextValue.intValue();
    }
}
