package com.infoinstech.claimsmodule.services.common;

import com.infoinstech.claimsmodule.domain.model.claims.paramater.ClaimsNumberFormat;
import com.infoinstech.claimsmodule.domain.repository.claims.parameter.ClaimsNumberFormatRepository;
import com.infoinstech.claimsmodule.services.parameters.ReceiptingParametersService;
import com.infoinstech.claimsmodule.services.util.DateConversion;
import com.infoinstech.claimsmodule.services.util.Formatter;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.Optional;

public class Test {

    @Autowired
    private ClaimsNumberFormatRepository claimsNumberFormatRepository;

    @Autowired
    private Formatter formatter;
    @Autowired
    private DateConversion dateConversion;

    @Autowired
    private ReceiptingParametersService receiptingParametersService;

    private String generateSequenceNo() {

        String sequenceNo = "0";

        Optional<ClaimsNumberFormat> claimsNumberFormat = claimsNumberFormatRepository.findById("00005");

        if (claimsNumberFormat.get() != null) sequenceNo = formatter.leftPadding("0", claimsNumberFormat.get().getLength(), "0");

        return sequenceNo;
    }

    private String generateClaimNo(String branchCode, String classCode, String productCode) {

        boolean Error = false;

        String year = String.valueOf(receiptingParametersService.getFinancialYear()).substring(2);

        return null;


    }

}
