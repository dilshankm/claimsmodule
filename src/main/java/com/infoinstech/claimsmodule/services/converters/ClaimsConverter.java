package com.infoinstech.claimsmodule.services.converters;

import com.infoinstech.claimsmodule.dto.claimintimation.ClaimIntimationDTO;
import com.infoinstech.claimsmodule.dto.claimintimation.CustomerContactDTO;
import com.infoinstech.claimsmodule.dto.claimnotification.ClaimNotificationDTO;
import com.infoinstech.claimsmodule.domain.model.claims.history.ClaimTaskHistory;
import com.infoinstech.claimsmodule.domain.model.claims.master.ExternalPerson;
import com.infoinstech.claimsmodule.domain.model.claims.transaction.*;
import com.infoinstech.claimsmodule.domain.model.salesmarketing.SystemUser;
import com.infoinstech.claimsmodule.domain.model.underwriting.master.Customer;
import com.infoinstech.claimsmodule.domain.model.underwriting.temporary.TempPolicy;
import com.infoinstech.claimsmodule.domain.model.underwriting.temporary.TempPolicyRisk;
import com.infoinstech.claimsmodule.domain.packages.PK_CL_TAB_SEQUENCE;
import com.infoinstech.claimsmodule.domain.repository.claims.master.ExternalPersonRepository;
import com.infoinstech.claimsmodule.domain.repository.salesmarketing.SystemUserRepository;
import com.infoinstech.claimsmodule.domain.repository.underwriting.master.CustomerRepository;
import com.infoinstech.claimsmodule.services.business.CustomerService;
import com.infoinstech.claimsmodule.services.common.ClaimsCommonService;
import com.infoinstech.claimsmodule.services.parameters.ReceiptingParametersService;
import com.infoinstech.claimsmodule.services.util.DateConversion;
import com.infoinstech.claimsmodule.services.util.Settings;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.math.BigInteger;
import java.util.Date;
import java.util.function.BiFunction;
import java.util.function.Function;

@Service
public class ClaimsConverter {

    @Autowired
    private DateConversion dateConversion;

    @Autowired
    private ExternalPersonRepository externalPersonRepository;
    @Autowired
    private SystemUserRepository systemUserRepository;
    @Autowired
    private CustomerRepository customerRepository;

    //DB Packages
    @Autowired
    private PK_CL_TAB_SEQUENCE pk_cl_tab_sequence;

    //Services
    @Autowired
    private ReceiptingParametersService receiptingParametersService;
    @Autowired
    private CustomerService customerService;
    @Autowired
    private Settings settings;

    @Autowired
    private ClaimsCommonService claimsCommonService;

    public BiFunction<ClaimIntimationDTO, ClaimIntimation, ClaimIntimation> ClaimIntimationDTOToClaimIntimation =

            (claimIntimationDTO, claimIntimation) -> {

                String classCode = claimIntimationDTO.getClassCode();

                if (classCode.equals("MT")) {

                    claimIntimation.setPoliceStation(claimIntimationDTO.getPoliceStation());
                    claimIntimation.setDriverName(claimIntimationDTO.getDriverName());
                    claimIntimation.setDriversAge(null);
                    claimIntimation.setDriversSex(null);
                    claimIntimation.setDriverLicenseNo(claimIntimationDTO.getDriverLicenseNo());
                }

                claimIntimation.setLossDate(claimIntimationDTO.getLossDate());
                claimIntimation.setIntimationDate(dateConversion.convertDate(new Date()));
                claimIntimation.setDateOfLossSpecifiedAtClaim(claimIntimationDTO.getLossDate());
                claimIntimation.setModeOfInformation(claimIntimationDTO.getIntimationType());
                claimIntimation.setCauseOfLoss(claimIntimationDTO.getCauseOfLoss());
                claimIntimation.setEventCode(claimIntimationDTO.getEventCode());
//                claimIntimation.setEstimatedAmount(claimIntimationDTO.getEstimatedAmount());
                claimIntimation.setPlaceOfLoss(claimIntimationDTO.getPlaceOfAccident());
//                claimIntimation.setContactPerson(claimIntimationDTO.getCallerName());
//                claimIntimation.setContactAddress(claimIntimationDTO.getCallerAddress());
//                claimIntimation.setContactNo(claimIntimationDTO.getCallerMobile());
//                claimIntimation.setContactEmail(claimIntimationDTO.getCallerEmail());
                claimIntimation.setAccidentDescription(claimIntimationDTO.getCauseDescription());
                claimIntimation.setInformPerson(claimIntimationDTO.getCallerName());
                claimIntimation.setCliNo(claimIntimationDTO.getCallerMobile());
                claimIntimation.setComments(claimIntimationDTO.getComments());
                claimIntimation.setLossRemarks(claimIntimationDTO.getLossRemarks());
                claimIntimation.setSurveyType(""); //no data at the moment

//                claimIntimation.setDriverId(claimIntimationDTO.getDriverId());
//                claimIntimation.setLicenseCategory(claimIntimationDTO.getLicenseCategory());
//                claimIntimation.setLicenseFromDate(claimIntimationDTO.getLicensePeriodFrom());
//                claimIntimation.setLicenseToDate(claimIntimationDTO.getLicensePeriodTo());

                if (claimIntimationDTO.getLossTime() != null) {

                    claimIntimation.setLossTime(claimIntimationDTO.getLossTime());
                }

                claimIntimation.setInjuredPersons(claimIntimationDTO.getLossRemarks());
                claimIntimation.setInsuredAtFault(claimIntimationDTO.getInsuredAtFault());
                claimIntimation.setPoliceReport(claimIntimationDTO.getPoliceReport());
                claimIntimation.setClaimDoubtful(claimIntimationDTO.getClaimDoubtful());
                claimIntimation.setTotalLoss(claimIntimationDTO.getTotalLoss());
                claimIntimation.setRelationshipToInsured(claimIntimationDTO.getRelationshipToInsured());
                claimIntimation.setRelationshipToInsuredDescription(claimIntimationDTO.getRelationshipDescription());
                claimIntimation.setThirdPartyDetails(claimIntimationDTO.getThirdPartyDetails());
                claimIntimation.setCallCenterFlag('Y');
                claimIntimation.setActionTaken(claimIntimationDTO.getActionTaken());

                claimIntimation.setCreatedDate(new Date());
                if (claimIntimationDTO.getCreatedBy()!=null) claimIntimation.setCreatedBy(claimIntimationDTO.getCreatedBy().toUpperCase());
                if(claimIntimationDTO.getModifiedBy()!=null) claimIntimation.setModifiedBy(claimIntimationDTO.getCreatedBy().toUpperCase());
                claimIntimation.setModifiedDate(new Date());

                claimIntimation.setLatitude(claimIntimationDTO.getLatitude().toString());
                claimIntimation.setLongitude(claimIntimationDTO.getLongitude().toString());

                return claimIntimation;

            };

    public BiFunction<ClaimNotification, ClaimIntimation, ClaimIntimation> claimNotificationToClaimIntimation =

            (claimNotification, claimIntimation) -> {



                claimIntimation.setNotificationSequenceNo(claimNotification.getSequenceNo());
                claimIntimation.setNotificationNo(claimNotification.getNotificationNo());
                claimIntimation.setLossDate(claimNotification.getLossDate());
                claimIntimation.setIntimationDate(dateConversion.convertDate(new Date()));
                claimIntimation.setDateOfLossSpecifiedAtClaim(claimNotification.getLossDate());
                claimIntimation.setModeOfInformation(claimNotification.getModeOfInformation());
//                claimIntimation.setEstimatedAmount(claimIntimationDTO.getEstimatedAmount());
                claimIntimation.setPlaceOfLoss(claimNotification.getPlaceOfLoss());
//                claimIntimation.setContactPerson(claimNotification.getContactPerson());
//                claimIntimation.setContactAddress(claimNotification.getContactAddress());
//                claimIntimation.setContactNo(claimNotification.getContactNo());
//                claimIntimation.setContactEmail(claimNotification.getContactEmail());
                claimIntimation.setInformPerson(claimIntimation.getContactPerson());
                claimIntimation.setCliNo(claimIntimation.getContactNo());
                claimIntimation.setAccidentDescription(claimNotification.getAccidentDescription());
                claimIntimation.setComments(claimNotification.getComments());
                claimIntimation.setLossRemarks(claimNotification.getLossRemarks());
                claimIntimation.setCauseOfLoss(claimNotification.getCauseOfLoss());
                claimIntimation.setSurveyType(""); //no data at the moment
                claimIntimation.setPoliceStation(claimNotification.getPoliceStation());
//                claimIntimation.setDriverId(claimIntimationDTO.getDriverId());
                claimIntimation.setDriverName(claimNotification.getDriverName());
                claimIntimation.setDriversAge(null);
                claimIntimation.setDriversSex(null);
                claimIntimation.setDriverLicenseNo(claimNotification.getDriverLicenseNo());
//                claimIntimation.setLicenseCategory(claimIntimationDTO.getLicenseCategory());
//                claimIntimation.setLicenseFromDate(claimIntimationDTO.getLicensePeriodFrom());
//                claimIntimation.setLicenseToDate(claimIntimationDTO.getLicensePeriodTo());
                if (claimNotification.getLossTime() != null) {

                    claimIntimation.setLossTime(claimNotification.getLossTime());
                }

                claimIntimation.setInjuredPersons(claimNotification.getLossRemarks());
                claimIntimation.setInsuredAtFault(claimNotification.getInsuredAtFault());
                claimIntimation.setPoliceReport(claimNotification.getPoliceReport());
                claimIntimation.setRelationshipToInsured(claimNotification.getRelationshipToInsured());
                claimIntimation.setRelationshipToInsuredDescription(claimNotification.getRelationshipToInsuredDescription());
                claimIntimation.setThirdPartyDetails(claimNotification.getThirdPartyDetails());
                claimIntimation.setCallCenterFlag('Y');
                claimIntimation.setActionTaken(claimNotification.getActionTaken());

                claimIntimation.setClaimDoubtful(claimNotification.getClaimDoubtful());
                claimIntimation.setCreatedDate(new Date());
                claimIntimation.setCreatedBy(claimNotification.getCreatedBy().toUpperCase());
                claimIntimation.setModifiedBy(claimNotification.getCreatedBy().toUpperCase());
                claimIntimation.setModifiedDate(new Date());
                claimIntimation.setLatitude(claimNotification.getLatitude().toString());
                claimIntimation.setLongitude(claimNotification.getLongitude().toString());

                return claimIntimation;

            };

    public BiFunction<TempPolicy, ClaimIntimation, ClaimIntimation> TemporaryPolicyToClaimIntimation =

            (temporaryPolicy, claimIntimation) -> {

                Customer customer = customerRepository.findByCustomerCode(temporaryPolicy.getCustomerCode());
                CustomerContactDTO customerContactDTO = customerService.getCustomerContact(customer);

                claimIntimation.setContactNo(customerContactDTO.getContact());
                claimIntimation.setContactPerson(customerContactDTO.getName());
                claimIntimation.setContactEmail(customerContactDTO.getEmail());
                claimIntimation.setContactAddress(customerContactDTO.getAddress());

                claimIntimation.setBusinessPartyTypeCode(temporaryPolicy.getBusinessChannelCode());
                claimIntimation.setPolicyBranchCode(temporaryPolicy.getBranchCode());
                claimIntimation.setEndorsementNo(temporaryPolicy.getEndorsementNo());
                claimIntimation.setSumInsured(temporaryPolicy.getSumInsured());
                claimIntimation.setPeriodFrom(temporaryPolicy.getPeriodFrom());
                claimIntimation.setPeriodTo(temporaryPolicy.getPeriodTo());
                claimIntimation.setPolicyNo(temporaryPolicy.getPolicyNo());
                claimIntimation.setClassCode(temporaryPolicy.getClassCode());
                claimIntimation.setProductCode(temporaryPolicy.getProductCode());
                claimIntimation.setCurrency(temporaryPolicy.getCurrency());
                claimIntimation.setPolicySequenceNo(temporaryPolicy.getPolicySequenceNo());
                claimIntimation.setCustomerCode(temporaryPolicy.getCustomerCode());
                claimIntimation.setBranchCode((settings.getSite().equalsIgnoreCase("AMI")) ?
                temporaryPolicy.getBranchCode() : receiptingParametersService.getBaseAccountBranch());


                claimIntimation.setMarketingExecutiveCode(temporaryPolicy.getIntermediaryCode());

                return claimIntimation;
            };


    public BiFunction<TempPolicyRisk, ClaimIntimation, ClaimIntimation> temporaryPolicyRiskToClaimIntimation =

            (tempPolicyRisk, claimIntimation) -> {


                if (tempPolicyRisk != null) {

                    claimIntimation.setRiskSumInsured(tempPolicyRisk.getSumInsured());
//                    claimIntimation.setRiskOrder(tempPolicyRisk.getRiskId());
                    claimIntimation.setRiskOrderNo(tempPolicyRisk.getRiskOrderNo());
                    claimIntimation.setRiskName(tempPolicyRisk.getName());
                    claimIntimation.setRiskSequenceNo(tempPolicyRisk.getTempPolicyRiskPK().getSequenceNo());
                    claimIntimation.setLocation(tempPolicyRisk.getLocationCode());
                    claimIntimation.setLocationSeqNo(tempPolicyRisk.getTempPolicyRiskPK().getLocationSequenceNo());
                }

                return claimIntimation;

            };


    public BiFunction<ClaimIntimationDTO, AssignLossAdjuster, AssignLossAdjuster> claimIntimationDTOAssignLossAdjusterAssignLossAdjuster =

            (claimIntimationDTO, assignLossAdjuster) -> {

                //Getting Assessor code from External Persons Table
                ExternalPerson externalPerson = externalPersonRepository.findByCode(claimIntimationDTO.getAssessorCode());

                if (externalPerson != null) {

                    assignLossAdjuster.setExternalPersonCode(externalPerson.getCode());
                    assignLossAdjuster.setPromiseTime(claimIntimationDTO.getPromisedTime());
                    assignLossAdjuster.setAppointedDate(dateConversion.convertDate(new Date()));
                    assignLossAdjuster.setReportDueDate(dateConversion.convertDate(new Date()));
                    assignLossAdjuster.setVisitDueDate(dateConversion.convertDate(new Date()));
                    assignLossAdjuster.setCreatedDate(new Date());
                    assignLossAdjuster.setCreatedBy(claimIntimationDTO.getCreatedBy().toUpperCase());
                    assignLossAdjuster.setModifiedDate(new Date());
                    assignLossAdjuster.setModifiedBy(claimIntimationDTO.getCreatedBy().toUpperCase());
                }

                return assignLossAdjuster;

            };


    public BiFunction<TempPolicy, AssignLossAdjuster, AssignLossAdjuster> temporaryPolicyAssignLossAdjusterAssignLossAdjuster =

            (temporaryPolicy, assignLossAdjuster) -> {

                assignLossAdjuster.setCurrency(temporaryPolicy.getCurrency());
                return assignLossAdjuster;
            };


    public BiFunction<ClaimIntimationDTO, JobAssignment, JobAssignment> claimIntimationDTOToJobAssignment =

            (claimIntimationDTO, jobAssignment) -> {

                jobAssignment.setSurveyCode("ST002");
                jobAssignment.setCreatedDate(new Date());
                jobAssignment.setCreatedBy(claimIntimationDTO.getCreatedBy().toUpperCase());
                jobAssignment.setCreatedDate(new Date());
                jobAssignment.setModifiedBy(claimIntimationDTO.getCreatedBy().toUpperCase());

                return jobAssignment;
            };

    public BiFunction<ClaimIntimation, ClaimHandler, ClaimHandler> claimIntimationToClaimHandler =

            (claimIntimation, claimHandler) -> {

                SystemUser systemUser = systemUserRepository.findByUserNameAndIsActiveAndSystemUser(claimIntimation.getCreatedBy().toUpperCase(), 'Y', 'Y');

                claimHandler.setHandlerCode((systemUser != null) ? systemUser.getCode() : "SICL");
                claimHandler.setClaimNo(claimIntimation.getClaimNo());
                claimHandler.setAssignedDate(new Date());
                claimHandler.setClaimNo(claimIntimation.getClaimNo());
                claimHandler.setIntimationSequenceNo(claimIntimation.getSequenceNo());
                claimHandler.setHandlerStatus(BigInteger.valueOf(1));
                claimHandler.setHandlerName(claimIntimation.getCreatedBy().toUpperCase());
                claimHandler.setCreatedBy(claimIntimation.getCreatedBy().toUpperCase());
                claimHandler.setCreatedDate(new Date());
                claimHandler.setModifiedBy(claimIntimation.getCreatedBy().toUpperCase());
                claimHandler.setModifiedDate(new Date());

                return claimHandler;
            };

    public BiFunction<ClaimIntimation, ClaimCurrentTask, ClaimCurrentTask> claimIntimationToClaimCurrentTask =

            (claimIntimation, claimCurrentTask) -> {

                claimCurrentTask.setClaimSequenceNo(claimIntimation.getSequenceNo());
                claimCurrentTask.setClaimNo(claimIntimation.getClaimNo());
                claimCurrentTask.setLossComments(claimIntimation.getLossRemarks());
                claimCurrentTask.setClaimComments(claimIntimation.getComments());
                claimCurrentTask.setCreatedBy(claimIntimation.getCreatedBy().toUpperCase());
                claimCurrentTask.setCreatedDate(new Date());
                claimCurrentTask.setModifiedBy(claimCurrentTask.getCreatedBy().toUpperCase());
                claimCurrentTask.setModifiedDate(new Date());

                return claimCurrentTask;
            };

    public BiFunction<ClaimIntimation, ClaimTaskHistory, ClaimTaskHistory> claimIntimationToClaimTaskHistory =

            (claimIntimation, claimTaskHistory) -> {

                claimTaskHistory.setClaimSequenceNo(claimIntimation.getSequenceNo());
                claimTaskHistory.setClaimNo(claimIntimation.getClaimNo());
                claimTaskHistory.setLossComments(claimIntimation.getLossRemarks());
                claimTaskHistory.setClaimComments(claimIntimation.getComments());
                claimTaskHistory.setCreatedBy(claimIntimation.getCreatedBy().toUpperCase());
                claimTaskHistory.setCreatedDate(new Date());
                claimTaskHistory.setModifiedBy(claimIntimation.getCreatedBy());
                claimTaskHistory.setModifiedDate(new Date());

                return claimTaskHistory;
            };


    public Function<ClaimIntimation, JobAssignment> claimIntimationToJobAssignment =

            (claimIntimation) -> {

                JobAssignment jobAssignment = new JobAssignment();

                String jobSeqNo = claimsCommonService.generateSequence(JobAssignment.SEQUENCE, claimIntimation.getPolicyBranchCode());
                String jobNo = claimsCommonService.generateSequence(jobAssignment.JOB_NO_PREFIX, JobAssignment.JOB_NO_SEQUENCE, claimIntimation.getPolicyBranchCode());

                JobAssignmentPK jobAssignmentPK = new JobAssignmentPK();
                jobAssignmentPK.setIntimationSequenceNo(claimIntimation.getSequenceNo());
                jobAssignmentPK.setSequenceNo(jobSeqNo);

                jobAssignment.setJobNo(jobNo);
                jobAssignment.setJobAssignmentPK(jobAssignmentPK);
                jobAssignment.setLatitude(claimIntimation.getLatitude());
                jobAssignment.setLongitude(claimIntimation.getLongitude());
                jobAssignment.setNearestTown(claimIntimation.getPlaceOfLoss());
                jobAssignment.setCreatedDate(new Date());
                jobAssignment.setCreatedBy(claimIntimation.getCreatedBy().toUpperCase());
                jobAssignment.setCreatedDate(new Date());
                jobAssignment.setModifiedBy(claimIntimation.getCreatedBy().toUpperCase());

                return jobAssignment;

            };

    public BiFunction<ClaimNotificationDTO, ClaimNotification, ClaimNotification> claimNotificationDTOToClaimNotification =

            (claimNotificationDTO, claimNotification) -> {

                claimNotification.setCoverNoteNo(claimNotificationDTO.getCoverNoteNo());
                claimNotification.setCoverNoteType(claimNotificationDTO.getCoverNoteType());
                claimNotification.setEffectiveStartDate(dateConversion.convertStringToDate(claimNotificationDTO.getEffectiveStartDate()));

                claimNotification.setRiskName(claimNotificationDTO.getRiskName());
                claimNotification.setMakeAndModel(claimNotificationDTO.getMakeAndModel());
                claimNotification.setYearOfManufacture(claimNotificationDTO.getYearOfManufacture());
                claimNotification.setLossDate(claimNotificationDTO.getLossDate());
                claimNotification.setNotifiedDate(dateConversion.convertDate(new Date()));
                claimNotification.setHandlerCode(claimNotificationDTO.getHandlerCode());
                claimNotification.setHandlerName(claimNotification.getHandlerName());

                claimNotification.setDateOfLossSpecifiedAtClaim(claimNotificationDTO.getLossDate());
                claimNotification.setModeOfInformation(claimNotificationDTO.getIntimationType());
//                claimIntimation.setEstimatedAmount(claimIntimationDTO.getEstimatedAmount());
                claimNotification.setPlaceOfLoss(claimNotificationDTO.getPlaceOfAccident());
                claimNotification.setContactPerson(claimNotificationDTO.getCallerName());
                claimNotification.setContactAddress(claimNotificationDTO.getCallerAddress());
                claimNotification.setContactNo(claimNotificationDTO.getCallerMobile());
                claimNotification.setContactEmail(claimNotificationDTO.getCallerEmail());
                claimNotification.setAccidentDescription(claimNotification.getLossRemarks());
                claimNotification.setInformPerson(claimNotification.getContactPerson());
                claimNotification.setCliNo(claimNotification.getContactNo());
                claimNotification.setComments(claimNotificationDTO.getComments());
                claimNotification.setLossRemarks(claimNotificationDTO.getLossRemarks());
                claimNotification.setCauseOfLoss(claimNotificationDTO.getCauseOfLoss());
                claimNotification.setAccidentDescription(claimNotificationDTO.getCauseDescription());
                claimNotification.setSurveyType(""); //no data at the moment
                claimNotification.setPoliceStation(claimNotificationDTO.getPoliceStation());
//                claimIntimation.setDriverId(claimIntimationDTO.getDriverId());
                claimNotification.setDriverName(claimNotificationDTO.getDriverName());
                claimNotification.setDriversAge(null);
                claimNotification.setDriversSex(null);
                claimNotification.setDriverLicenseNo(claimNotificationDTO.getDriverLicenseNo());
//                claimIntimation.setLicenseCategory(claimIntimationDTO.getLicenseCategory());
//                claimIntimation.setLicenseFromDate(claimIntimationDTO.getLicensePeriodFrom());
//                claimIntimation.setLicenseToDate(claimIntimationDTO.getLicensePeriodTo());
                if (claimNotificationDTO.getLossTime() != null) {

                    claimNotification.setLossTime(claimNotificationDTO.getLossTime());
                }

                claimNotification.setInjuredPersons(claimNotificationDTO.getLossRemarks());
                claimNotification.setInsuredAtFault(claimNotificationDTO.getInsuredAtFault());
                claimNotification.setPoliceReport(claimNotificationDTO.getPoliceReport());
                claimNotification.setRelationshipToInsured(claimNotificationDTO.getRelationshipToInsured());
                claimNotification.setRelationshipToInsuredDescription(claimNotificationDTO.getRelationshipDescription());
                claimNotification.setThirdPartyDetails(claimNotificationDTO.getThirdPartyDetails());
                claimNotification.setCallCenterFlag('Y');
                claimNotification.setActionTaken(claimNotificationDTO.getActionTaken());


                claimNotification.setClaimDoubtful(claimNotificationDTO.getClaimDoubtful());
                claimNotification.setCreatedDate(new Date());
                claimNotification.setCreatedBy(claimNotificationDTO.getCreatedBy().toUpperCase());
                claimNotification.setModifiedBy(claimNotificationDTO.getCreatedBy().toUpperCase());
                claimNotification.setModifiedDate(new Date());
                claimNotification.setLatitude(claimNotificationDTO.getLatitude().toString());
                claimNotification.setLongitude(claimNotificationDTO.getLongitude().toString());
                claimNotification.setSurveyType(claimNotificationDTO.getAssignmentTypeCode());
                claimNotification.setStatus('P');

                return claimNotification;

            };

    public BiFunction<ClaimNotification, NotificationAssignment, NotificationAssignment> claimNotificationToNotificationAssignment =

            (claimNotification, notificationAssignment) -> {

                notificationAssignment.setLatitude(claimNotification.getLatitude());
                notificationAssignment.setLongitude(claimNotification.getLongitude());
                notificationAssignment.setNearestTown(claimNotification.getPlaceOfLoss());
                notificationAssignment.setCreatedDate(new Date());
                notificationAssignment.setCreatedBy(claimNotification.getCreatedBy().toUpperCase());
                notificationAssignment.setCreatedDate(new Date());
                notificationAssignment.setModifiedBy(claimNotification.getCreatedBy().toUpperCase());
                notificationAssignment.setSurveryCode(claimNotification.getSurveyType());


                return notificationAssignment;

            };

}
