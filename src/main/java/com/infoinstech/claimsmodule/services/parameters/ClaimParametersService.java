package com.infoinstech.claimsmodule.services.parameters;

import com.infoinstech.claimsmodule.domain.model.claims.paramater.ClaimsSystemParameter;
import com.infoinstech.claimsmodule.domain.repository.claims.parameter.ClaimsSystemParameterRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

@Service
public class ClaimParametersService {

    @Autowired
    ClaimsSystemParameterRepository claimsSystemParameterRepository;

    public double getAssessorIdleTime() {

        ClaimsSystemParameter claimParameter = claimsSystemParameterRepository.findBySpmIndexNo("200");

        return claimParameter.getNumberValue();
    }

    public double getAssessorInactiveTime() {

        ClaimsSystemParameter claimParameter = claimsSystemParameterRepository.findBySpmIndexNo("201");

        return claimParameter.getNumberValue();
    }

    public List<String> getCategoryAutomotiveCategoryList() {

        List<String> indices = Arrays.asList("202", "203", "204");
        List<ClaimsSystemParameter> claimsSystemParameters = claimsSystemParameterRepository.findAllBySpmIndexNoIn(indices);

        return claimsSystemParameters.stream()
                .map(ClaimsSystemParameter::getCharacterValue)
                .collect(Collectors.toList());

    }

    public Boolean getUploadGalleryImagesParameter() {

        ClaimsSystemParameter claimParameter = claimsSystemParameterRepository.findBySpmIndexNo("205");

        return claimParameter.getCharacterValue().equalsIgnoreCase("Y");

    }


}
