package com.infoinstech.claimsmodule.services.parameters;

import com.infoinstech.claimsmodule.domain.model.receipting.parameter.ReceiptingSystemParameter;
import com.infoinstech.claimsmodule.domain.repository.receipting.parameter.ReceiptingSystemParameterRepository;
import com.infoinstech.claimsmodule.services.util.DateConversion;
import com.infoinstech.claimsmodule.services.util.Settings;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.Optional;

@Service
public class ReceiptingParametersService {

    @Autowired
    private ReceiptingSystemParameterRepository receiptingSystemParameterRepository;
    @Autowired
    private DateConversion dateConversion;

    @Autowired
    private Settings settings;


    public String getBaseAccountBranch() {

        Optional<ReceiptingSystemParameter> receiptingSystemParameter = receiptingSystemParameterRepository.findByIndexNo("76");

        return receiptingSystemParameter
                .map(ReceiptingSystemParameter::getCharacterValue)
                .orElse((settings.getSite().equalsIgnoreCase("AMI"))? "AMI" : "HEAD");

    }

    public int getFinancialYear() {

        int financialYear;

        int startingMonth = getFinancialYearStartingMonth();
        int currentMonth = Integer.parseInt(dateConversion.convertFromDateToString(new Date(), "MM"));

        if (currentMonth < startingMonth) {

            financialYear = Integer.parseInt(dateConversion.convertFromDateToString(new Date(), "yyyy")) - 1;
        } else {

            financialYear = Integer.parseInt(dateConversion.convertFromDateToString(new Date(), "yyyy"));
        }

        return financialYear;
    }

    private int getFinancialYearStartingMonth() {

        Optional<ReceiptingSystemParameter> receiptingSystemParameter = receiptingSystemParameterRepository.findByIndexNo("105");

        return receiptingSystemParameter.
                map(p -> Integer.parseInt(p.getCharacterValue())).
                orElse(1);
    }



}
