package com.infoinstech.claimsmodule.services.parameters;

import com.infoinstech.claimsmodule.domain.model.underwriting.parameter.UnderwritingSystemParameter;
import com.infoinstech.claimsmodule.domain.repository.underwriting.parameter.UnderwritingSystemParameterRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class UnderwritingParametersService {

    @Autowired
    private UnderwritingSystemParameterRepository underwritingSystemParameterRepository;

    public String getVehicleNoFormatValidationMessage() {

        UnderwritingSystemParameter systemParameter = underwritingSystemParameterRepository.getOne("201");

        return systemParameter.getCharacterValue();

    }

    public String getPolicyCancelledStatus() {

        UnderwritingSystemParameter systemParameter = underwritingSystemParameterRepository.getOne("10");

        return systemParameter.getNumberValue().toString();

    }

    public String getPolicyClosedStatus() {

        UnderwritingSystemParameter systemParameter = underwritingSystemParameterRepository.getOne("25");

        return systemParameter.getNumberValue().toString();

    }


}
