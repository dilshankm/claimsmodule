package com.infoinstech.claimsmodule.services.reference;

import com.infoinstech.claimsmodule.dto.common.ReferenceCode;
import com.infoinstech.claimsmodule.dto.common.ReferenceDTO;
import com.infoinstech.claimsmodule.dto.claimreference.PolicyPerilsRequestDTO;
import com.infoinstech.claimsmodule.domain.model.claims.master.ExternalPerson;
import com.infoinstech.claimsmodule.domain.model.claims.reference.*;
import com.infoinstech.claimsmodule.domain.model.claims.transaction.ClaimIntimation;
import com.infoinstech.claimsmodule.domain.model.common.reference.CommonReferenceType;
import com.infoinstech.claimsmodule.domain.model.payments.ProvisionType;
import com.infoinstech.claimsmodule.domain.model.salesmarketing.SystemUser;
import com.infoinstech.claimsmodule.domain.model.salesmarketing.master.SalesLocation;
import com.infoinstech.claimsmodule.domain.model.salesmarketing.reference.BusinessType;
import com.infoinstech.claimsmodule.domain.model.underwriting.master.Product;
import com.infoinstech.claimsmodule.domain.model.underwriting.master.ProductClass;
import com.infoinstech.claimsmodule.domain.model.underwriting.master.ProductLossCause;
import com.infoinstech.claimsmodule.domain.model.underwriting.reference.RefClass;
import com.infoinstech.claimsmodule.domain.model.underwriting.reference.RefPeril;
import com.infoinstech.claimsmodule.domain.model.underwriting.temporary.TempPolicy;
import com.infoinstech.claimsmodule.domain.model.underwriting.temporary.TempPolicyPeril;
import com.infoinstech.claimsmodule.domain.model.underwriting.temporary.TempPolicyRisk;
import com.infoinstech.claimsmodule.domain.model.underwriting.transaction.PolicyRisk;
import com.infoinstech.claimsmodule.domain.repository.claims.master.ExternalPersonRepository;
import com.infoinstech.claimsmodule.domain.repository.claims.reference.*;
import com.infoinstech.claimsmodule.domain.repository.claims.transaction.ClaimIntimationRepository;
import com.infoinstech.claimsmodule.domain.repository.common.reference.CommonReferenceTypeRepository;
import com.infoinstech.claimsmodule.domain.repository.payments.ProvisionTypeRepository;
import com.infoinstech.claimsmodule.domain.repository.salesmarketing.SystemUserRepository;
import com.infoinstech.claimsmodule.domain.repository.salesmarketing.master.SalesLocationRepository;
import com.infoinstech.claimsmodule.domain.repository.salesmarketing.reference.BusinessTypeRepository;
import com.infoinstech.claimsmodule.domain.repository.underwriting.master.ProductLossCauseRepository;
import com.infoinstech.claimsmodule.domain.repository.underwriting.master.ProductRepository;
import com.infoinstech.claimsmodule.domain.repository.underwriting.reference.RefClassRepository;
import com.infoinstech.claimsmodule.domain.repository.underwriting.temporary.TempPolicyPerilRepository;
import com.infoinstech.claimsmodule.domain.repository.underwriting.temporary.TempPolicyRiskRepository;
import com.infoinstech.claimsmodule.domain.repository.underwriting.transaction.PolicyRiskRepository;
import com.infoinstech.claimsmodule.services.business.ExternalPersonService;
import com.infoinstech.claimsmodule.services.business.PolicyRiskInformationService;
import com.infoinstech.claimsmodule.services.business.SalesForceService;
import com.infoinstech.claimsmodule.services.business.WrapperService;
import com.infoinstech.claimsmodule.services.util.DateConversion;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.util.*;
import java.util.concurrent.locks.ReentrantReadWriteLock;
import java.util.stream.Collectors;

@Service
public class ClaimsReference {

    @Autowired
    private ProductLossCauseRepository productLossCauseRepository;
    @Autowired
    private RefLossCauseRepository refLossCauseRepository;
    @Autowired
    private RefSurveyTypeRepository refSurveyTypeRepository;
    @Autowired
    private RefInformationModeRepository refInformationModeRepository;
    @Autowired
    private CommonReferenceTypeRepository commonReferenceTypeRepository;
    @Autowired
    private SystemUserRepository systemUserRepository;

    @Autowired
    private SalesForceService salesForceService;
    @Autowired
    private ProductRepository productRepository;
    @Autowired
    private ProvisionTypeRepository provisionTypeRepository;

    @Autowired
    private TempPolicyRiskRepository tempPolicyRiskRepository;
    @Autowired
    private TempPolicyPerilRepository tempPolicyPerilRepository;
    @Autowired
    private RefVehicleSketchRepository refVehicleSketchRepository;
    @Autowired
    private ClaimIntimationRepository claimIntimationRepository;
    @Autowired
    private RefClassRepository refClassRepository;
    @Autowired
    private RefActionTakenRepository refActionTakenRepository;
    @Autowired
    private ExternalPersonRepository externalPersonRepository;
    @Autowired
    private SalesLocationRepository salesLocationRepository;
    @Autowired
    private BusinessTypeRepository businessTypeRepository;
    @Autowired
    private PolicyRiskRepository policyRiskRepository;

    //Services
    @Autowired
    private DateConversion dateConversion;
    @Autowired
    private WrapperService wrapperService;
    @Autowired
    private PolicyRiskInformationService policyRiskInformationService;
    @Autowired
    private ExternalPersonService externalPersonService;
    @PersistenceContext
    private EntityManager entityManager;

    public static final Character[] claimStatus = {'C', 'N', 'R', 'L', 'P', 'S', 'O'};


    // Get the ReferenceDTO Cause of Damages - Details
    public List<RefLossCause> getProductWiseLossCauses(String productCode) {

        List<ProductLossCause> productLossCauses = productLossCauseRepository.findByProductLossCausePK_ProductCode(productCode);

        return productLossCauses.stream()
                .filter(p -> p.getProductLossCausePK().getProductCode().equals(productCode))
                .map(ProductLossCause::getRefLossCause)
                .collect(Collectors.toList());

    }

    //Get the ReferenceDTO Modes of Information for Claim Intimation
    public List<RefInformationMode> getModesOfInformation() {

         return refInformationModeRepository.findAll();

    }


    // Get the ReferenceDTO Job Assignment Types;
    public List<RefSurveyType> getJobAssignmentTypes() {

        return refSurveyTypeRepository.findAll();

    }

    // Get Relationship Types
    public List<ReferenceDTO> getRelationshipTypesToInsured() {

        List<CommonReferenceType> commonReferenceTypes = commonReferenceTypeRepository.findByTableName("RELATIONSHIP-TO-INSURED");

        return commonReferenceTypes.
                stream().map(p ->  new ReferenceDTO(p.getSequenceNo(), p.getDescription()))
                .collect(Collectors.toList());
    }

    public List<ReferenceDTO> getClaimHandlers() {

        List<SystemUser> systemUsers = systemUserRepository.findByIsActive('Y');

        return systemUsers.
                stream().map(p -> new ReferenceDTO(p.getCode(), salesForceService.getSystemUserName(p))).
                collect(Collectors.toList());

    }

    public List<ReferenceDTO> getCoverTypes() {

        List<Product> products = productRepository.findByClassCodeAndStatus("MT", 'Y');

      return products.stream().
              map(p -> new ReferenceDTO(p.getCode(), p.getDescription()))
              .collect(Collectors.toList());

    }

    public List<ReferenceDTO> getProvisionTypes() {

        List<ProvisionType> provisionTypes = provisionTypeRepository.findByCodeStartingWithAndCodeNotIn("TRA", Arrays.asList("TRA016", "TRA017"));

        return provisionTypes.stream()
                .map( p -> new ReferenceDTO(p.getCode(),p.getDescription()))
                .sorted(Comparator.comparing(ReferenceDTO::getDescription))
                .collect(Collectors.toList());

    }

    public List<ReferenceDTO> getPolicyPerils(PolicyPerilsRequestDTO policyPerilsRequestDTO) {

        String sessionId = null;
        Date lossDate = dateConversion.convertStringToDate(policyPerilsRequestDTO.getLossDate());

        wrapperService.deleteDataFromTempPolicyTables();
        TempPolicy tempPolicy = wrapperService.buildPolicy(policyPerilsRequestDTO.getPolicyNo(), lossDate);
        TempPolicyRisk tempPolicyRisk = tempPolicyRiskRepository.findByTempPolicyRiskPK_PolicySequenceNoAndRiskOrderNo(tempPolicy.getSequenceNo(), policyPerilsRequestDTO.getRiskOrder());

        List<TempPolicyPeril> policyPerils = tempPolicyPerilRepository.
                findByTempPolicyRisk_TempPolicyLocation_TempPolicy_SequenceNoAndTempPolicyRisk_TempPolicyLocation_LocationCodeAndTempPolicyRisk_RiskOrderNo(
                        tempPolicy.getSequenceNo(), tempPolicyRisk.getLocationCode(), tempPolicyRisk.getRiskOrderNo());

        return policyPerils.stream()
                .map(TempPolicyPeril::getRefPeril)
                .map(p -> new ReferenceDTO(p.getCode(), p.getDescription()))
                .sorted(Comparator.comparing(ReferenceDTO::getDescription))
                .collect(Collectors.toList());

    }

    public List<ReferenceDTO> getVehicleSketches() {

        List<RefVehicleSketch> sketches = refVehicleSketchRepository.findAll();

        return sketches.stream()
                .map(p -> new ReferenceDTO(p.getCode(), Base64.getEncoder().encodeToString(p.getImage())))
                .sorted(Comparator.comparing(ReferenceDTO::getDescription))
                .collect(Collectors.toList());

    }

    public List<String> getClaimNos() {

        List<String> claimNos = entityManager.createNativeQuery("SELECT DISTINCT INT_CLAIM_NO FROM CL_T_INTIMATION ORDER BY INT_CLAIM_NO")
                .getResultList();

        return claimNos;
    }

    public List<String> getPolicyNos() {


        List<String> policyNos = entityManager.createNativeQuery("SELECT DISTINCT INT_POLICY_NO FROM CL_T_INTIMATION INT_POLICY_NO")
                .getResultList();

        return policyNos;
    }

    public List<String> getRiskNames() {

        List<String> riskNames = entityManager.createNativeQuery("SELECT DISTINCT INT_PRS_NAME FROM CL_T_INTIMATION ORDER BY INT_PRS_NAME")
                .getResultList();

        return riskNames;
    }

    public List<String> getCustomerNames() {

        List<String> customerNames = entityManager.createNativeQuery("SELECT DECODE(CUS_TYPE, 'I',CUS_INDV_TITLE || CUS_INDV_INITIALS || CUS_INDV_FIRST_NAME || CUS_INDV_OTHER_NAMES || CUS_INDV_SURNAME, CUS_CORP_NAME)\n " +
                "FROM UW_M_CUSTOMERS").getResultList();

        return customerNames;
    }

    public List<ReferenceDTO> getClassNames() {

        List<RefClass> refClasses = refClassRepository.findAll();

        return  refClasses.stream()
                .map(p -> new ReferenceDTO(p.getCode(), p.getDescription()))
                .collect(Collectors.toList());

    }

    public List<ReferenceDTO> getProducts(String code) {
        List<Product> result = productRepository.findByClassCodeContainingAndStatus(code, 'Y');
        return result.stream()
                .map(p -> new ReferenceDTO(p.getCode(), p.getDescription()))
                .collect(Collectors.toList());
    }

    public List<ReferenceDTO> getActionTakenList() {

        List<RefActionTaken> result = refActionTakenRepository.findByStatus('A');

        return result.stream()
                .map(p -> new ReferenceDTO(p.getCode(), p.getDescription()))
                .sorted(Comparator.comparing(ReferenceDTO::getDescription))
                .collect(Collectors.toList());

    }

    public List<ReferenceDTO> getAssessorList() {

        List<ExternalPerson> externalPeople = externalPersonRepository.findByCategoryCodeAndStatus("CAT08", 'A');

        return externalPeople.stream()
                .map(p -> new ReferenceDTO(p.getCode(), externalPersonService.getExternalPersonName(p)))
                .collect(Collectors.toList());

    }

    public List<ReferenceDTO> getBranches() {

        List<SalesLocation> salesLocations = salesLocationRepository.findByIsActiveOrderByDescription('Y');

        return salesLocations.stream()
                .map(p -> new ReferenceDTO(p.getCode(), p.getDescription()))
                .collect(Collectors.toList());

    }

    public List<ReferenceDTO> getIntermediaryTypes() {

        List<BusinessType> businessTypes = businessTypeRepository.findAll();

        return businessTypes.stream()
                .map(p -> new ReferenceDTO(p.getCode(), p.getDescription()))
                .collect(Collectors.toList());

    }

    public List<ReferenceDTO> getIntermediaries(String code) {

        List<SystemUser> systemUsers = systemUserRepository.findByIsActiveAndBusinessTypeCodeContaining('Y', code);

        return systemUsers.stream()
                .map(p -> new ReferenceDTO(p.getCode(), salesForceService.getSystemUserName(p)))
                .collect(Collectors.toList());

    }


}