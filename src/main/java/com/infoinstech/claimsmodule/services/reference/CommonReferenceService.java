package com.infoinstech.claimsmodule.services.reference;

import com.infoinstech.claimsmodule.dto.common.ReferenceDTO;
import com.infoinstech.claimsmodule.domain.model.common.reference.CommonReference;
import com.infoinstech.claimsmodule.domain.repository.common.reference.CommonReferenceRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

@Service
public class CommonReferenceService {

    @Autowired
    private CommonReferenceRepository commonReferenceRepository;


    public List<ReferenceDTO> getVehicleMakeAndModels() {

        List<CommonReference> commonReferences = commonReferenceRepository.findAllByTypeOrderByDescription("VM");

        return  commonReferences
                .stream()
                .map(p -> new ReferenceDTO(p.getCode(), p.getDescription()))
                .sorted(Comparator.comparing(ReferenceDTO::getDescription))
                .collect(Collectors.toList());

    }
}
