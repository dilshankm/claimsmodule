package com.infoinstech.claimsmodule.services.reference;

import org.springframework.stereotype.Service;

@Service
public class ReferenceService {


    //Get Yes or No for Characters Y and N
    public String getYesOrNo(Character yesOrNo) {

        return yesOrNo== 'Y' ? "YES" : yesOrNo == 'N' ? "NO" :  "";

    }

    public Character reverseYesOrNo(String yesOrNo,Character value){

        return  yesOrNo.equalsIgnoreCase("YES") ? 'Y' : yesOrNo.equalsIgnoreCase("NO") ? 'N'
                : value;

    }

    // Get Policy Transaction Status Description for given Status ReferenceCode
    public String getPolicyStatus(String status) {

        String description;

        switch (status) {

            case "0":
                description = "POLICY UN-AUTHORIZED";
                break;
            case "1":
                description = "POLICY EXAMINED";
                break;
            case "2":
                description = "POLICY FINAL AUTHORIZED";
                break;
            case "3":
                description = "POLICY REINSURANCE AUTHORIZED";
                break;
            case "4":
                description = "POLICY PAYMENT AUTHORIZED";
                break;
            case "5":
                description = "POLICY COVER NOTE";
                break;
            case "6":
                description = "POLICY SCHEDULE PRINTED";
                break;
            case "7":
                description = "XXXXXXXXXXXXX";
                break;
            case "8":
                description = "POLICY CLOSED";
                break;
            case "9":
                description = "POLICY CANCELLED";
                break;
            default:
                description = "NOT KNOWN";
                break;
        }

        return description;
    }

    // Get Claim Status Description for given Status ReferenceCode
    public String getClaimStatus(Character statusFlag) {

        String claimStatus;

        switch (statusFlag) {

            case 'C':
                claimStatus = "CANCELLED";
                break;
            case 'N':
                claimStatus = "NOT CLAIMED";
                break;
            case 'R':
                claimStatus = "REJECTED";
                break;
            case 'L':
                claimStatus = "CLOSED";
                break;
            case 'P':
                claimStatus = "OPEN";
                break;
            case 'S':
                claimStatus = "SETTLED";
                break;
            case 'O':
                claimStatus = "OUTSTANDING";
                break;
            default:
                claimStatus = "NOT KNOWN";
                break;

        }

        return claimStatus;
    }

    //Get Job Status Description for Status ReferenceCode
    public String getJobStatusDescriptionByCode(String statusCode) {


        String statusDesc = "";

        switch (statusCode) {

            case "A":
                statusDesc = "ACCEPTED";
                break;
            case "R":
                statusDesc = "REJECTED";
                break;
            case "N":
                statusDesc = "ASSIGNED";
                break;
            case "C":
                statusDesc = "COMPLETED";
                break;
        }

        return statusDesc;
    }

    //Get Assessor Status Description
    public String getAssessorStatusDescription(Character status) {

        String statusDescription = "";

        switch (status) {

            case 'P':
                statusDescription = "PENDING";
                break;
            case 'A':
                statusDescription = "ACTIVE";
                break;
            case 'N':
                statusDescription = "INACTIVE";
                break;
            case 'I':
                statusDescription = "IDLE";
                break;
            default:
                statusDescription = "NOT KNOWN";

        }

        return statusDescription;
    }

}
