package com.infoinstech.claimsmodule.services.reference;

import com.infoinstech.claimsmodule.dto.common.ReferenceDTO;
import com.infoinstech.claimsmodule.domain.model.reinsurance.transaction.GeneratedEvent;
import com.infoinstech.claimsmodule.domain.repository.reinsurance.transaction.GeneratedEventRepository;
import com.infoinstech.claimsmodule.services.util.DateConversion;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

@Service
public class ReinsuranceReferenceService {

    @Autowired
    private GeneratedEventRepository generatedEventRepository;
    @Autowired
    private DateConversion dateConversion;

    public List<ReferenceDTO> getEventsForClaimIntimation(String lossDate, String intimationDate) {

        List<GeneratedEvent> generatedEvents = generatedEventRepository.findByAppliedFromDateIsBeforeAndAppliedToDateAfterAndStatus(
                dateConversion.convertStringToDate(lossDate), dateConversion.convertStringToDate(lossDate), '2');

        return generatedEvents.stream()
                .filter(p -> p.getAllocatedDate() == null || (p.getAllocatedDate().before(dateConversion.convertStringToDate(intimationDate))))
                .map(k -> new ReferenceDTO(k.getEventNo(), k.getRemarks()))
                .collect(Collectors.toList());

    }
}
