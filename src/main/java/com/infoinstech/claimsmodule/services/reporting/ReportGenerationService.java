package com.infoinstech.claimsmodule.services.reporting;

import com.infoinstech.claimsmodule.dto.common.Response;
import com.infoinstech.claimsmodule.dto.reports.*;
import com.infoinstech.claimsmodule.dto.reports.claimRatioReport.*;
import com.infoinstech.claimsmodule.dto.reports.claimregistrationreport.ClaimRegistrationReportDTO;
import com.infoinstech.claimsmodule.dto.reports.claimregistrationreport.ClaimRegistrationReportDTOList;
import com.infoinstech.claimsmodule.dto.reports.claimregistrationreport.ClaimRegistrationRequestDTO;
import com.infoinstech.claimsmodule.dto.reports.claimregistrationsheet.ClaimRegistrationSheetDTO;
import com.infoinstech.claimsmodule.dto.reports.jobassignmentdetailsreport.JobAssignmentReportDTO;
import com.infoinstech.claimsmodule.dto.reports.jobassignmentdetailsreport.JobAssignmentReportRequestDTO;
import com.infoinstech.claimsmodule.dto.reports.jobassignmentdetailsreport.MainJobAssignment;
import com.infoinstech.claimsmodule.dto.reports.paymentDetailReport.MainPaymentDTO;
import com.infoinstech.claimsmodule.dto.reports.paymentDetailReport.PaymentResponseDTO;
import com.infoinstech.claimsmodule.dto.reports.paymentDetailReport.PaymentRequestDTO;
import com.infoinstech.claimsmodule.domain.model.common.Logo;
import com.infoinstech.claimsmodule.domain.repository.claims.master.ExternalPersonRepository;
import com.infoinstech.claimsmodule.domain.repository.claims.transaction.AssignLossAdjusterRepository;
import com.infoinstech.claimsmodule.domain.repository.claims.transaction.ClaimIntimationRepository;
import com.infoinstech.claimsmodule.domain.repository.claims.transaction.JobAssignmentRepository;
import com.infoinstech.claimsmodule.domain.repository.claims.transaction.RequisitionPayeeRepository;
import com.infoinstech.claimsmodule.exceptions.types.InvalidInputExceptions;
import net.sf.jasperreports.engine.*;
import net.sf.jasperreports.engine.data.JRBeanArrayDataSource;
import net.sf.jasperreports.engine.export.JRPdfExporter;
import net.sf.jasperreports.engine.export.ooxml.JRXlsxExporter;
import net.sf.jasperreports.export.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.core.io.ByteArrayResource;
import org.springframework.core.io.Resource;
import org.springframework.core.io.ResourceLoader;
import org.springframework.stereotype.Service;
import com.infoinstech.claimsmodule.services.business.ExternalPersonService;
import com.infoinstech.claimsmodule.services.reference.ReferenceService;
import com.infoinstech.claimsmodule.services.util.DateConversion;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.math.BigDecimal;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;

import static java.lang.Boolean.FALSE;
import static java.lang.Boolean.TRUE;

/**
 * Created by dushman on 3/20/18.
 */

@Service
public class ReportGenerationService {

    @Autowired
    private ReportingService reportingService;

    @Autowired
    private ExternalPersonService externalPersonService;

    @Autowired
    private DateConversion dateConversion;

    @Autowired
    private ReferenceService referenceService;

    @Autowired
    private ResourceLoader resourceLoader;

    @Autowired
    private JobAssignmentRepository jobAssignmentRepository;

    @Autowired
    private AssignLossAdjusterRepository assignLossAdjusterRepository;
    @Autowired
    private ExternalPersonRepository externalPersonRepository;

    @Autowired
    private ClaimIntimationRepository claimIntimationRepository;
    ;

    @Autowired
    private RequisitionPayeeRepository requisitionPayeeRepository;

    @Autowired
    private Environment env;


    @PersistenceContext
    private EntityManager entityManager;

    private ByteArrayResource byteArrayResource;

    private Response response;

    private ByteArrayResource reportGenarateMethode(String jrxmlFileName, JRBeanArrayDataSource jrBeanArrayDataSource, String reportType) throws JRException, ClassNotFoundException, IOException {

        Resource res = resourceLoader.getResource("classpath:" + jrxmlFileName);
        InputStream targetStream = res.getInputStream();
        JasperReport jasperReport = JasperCompileManager.compileReport(targetStream);

        HashMap<String, Object> nownParameeter = new HashMap<String, Object>();

        JasperPrint print = JasperFillManager.fillReport(jasperReport, nownParameeter, jrBeanArrayDataSource);
        File outDir = new File(env.getProperty("jasper.report.path"));
        outDir.mkdirs();

        // PDF Exportor.
        if (reportType.equals("pdf")) {

            JRPdfExporter exporter = new JRPdfExporter();

            ExporterInput exporterInput = new SimpleExporterInput(print);
            exporter.setExporterInput(exporterInput);

            OutputStreamExporterOutput exporterOutput = new SimpleOutputStreamExporterOutput(
                    env.getProperty("jasper.report.path") + "/FirstJasperReport.pdf");

            exporter.setExporterOutput(exporterOutput);
            SimplePdfExporterConfiguration configuration = new SimplePdfExporterConfiguration();
            exporter.setConfiguration(configuration);
            exporter.exportReport();
            byteArrayResource = fileToInputStreamConverterPDF();
            System.out.println("Done!");

        } else if (reportType.equals("xls")) {

            JRXlsxExporter exporter = new JRXlsxExporter();
            ExporterInput exporterInput = new SimpleExporterInput(print);
            exporter.setExporterInput(exporterInput);

            OutputStreamExporterOutput exporterOutput = new SimpleOutputStreamExporterOutput(
                    env.getProperty("jasper.report.path") + "/FirstJasperReport.xls");

            exporter.setExporterOutput(exporterOutput);
            SimpleXlsxReportConfiguration reportConfig = new SimpleXlsxReportConfiguration();
            reportConfig.setSheetNames(new String[]{"sheet"});
            exporter.setConfiguration(reportConfig);
            exporter.exportReport();

            System.out.println("Done!");
            byteArrayResource = fileToInputStreamConverterExcel();
        }
        return byteArrayResource;
    }

    private ByteArrayResource fileToInputStreamConverterPDF() throws IOException {

        ByteArrayResource resource;
        File filePdf = new File(env.getProperty("jasper.report.path") + "/FirstJasperReport.pdf");
        Path path = Paths.get(filePdf.getAbsolutePath());
        resource = new ByteArrayResource(Files.readAllBytes(path));
        return resource;
    }

    private ByteArrayResource fileToInputStreamConverterExcel() throws IOException {

        ByteArrayResource resource;
        File filePdf = new File(env.getProperty("jasper.report.path") + "/FirstJasperReport.xls");
        Path path = Paths.get(filePdf.getAbsolutePath());
        resource = new ByteArrayResource(Files.readAllBytes(path));
        return resource;
    }


    public Response generateClaimRegistrationSheet(ClaimNoDTOReport claimNoDTOReport) throws ClassNotFoundException, IOException, JRException {
        String jrxmlFileName = "ClaimRegistrationSheet/ClaimRegistrationSheet.jrxml";
        if (!Objects.equals(claimNoDTOReport.getClaimNo(), "") && !Objects.equals(claimNoDTOReport.getUser(), "") && !Objects.equals(claimNoDTOReport.getReportType(), "")) {
            ClaimRegistrationSheetDTO claimRegistrationSheetDTO = reportingService.getClaimRegistrationSheet(claimNoDTOReport.getClaimNo(), claimNoDTOReport.getUser());
            JRBeanArrayDataSource jrBeanArrayDataSource = new JRBeanArrayDataSource(new ClaimRegistrationSheetDTO[]{claimRegistrationSheetDTO});
            byteArrayResource = reportGenarateMethode(jrxmlFileName, jrBeanArrayDataSource, claimNoDTOReport.getReportType());
            response = new Response(2, TRUE, "sucess", byteArrayResource);
        } else {
            throw new InvalidInputExceptions("Null input values");
        }
        return response;
    }

    public Response generateClaimRegistrationReport(ClaimRegistrationRequestDTO claimRegistrationRequestDTO) throws ClassNotFoundException, IOException, JRException {
//        String jrxmlFileName = "ClaimRegistrationSheet/ClaimRegistrationSheet.jrxml";
        String jrxmlFileName = "ClaimRegistrationReport/ClaimRegistrationReport.jrxml";
        if (!Objects.equals(claimRegistrationRequestDTO.getFromDate(), "") && !Objects.equals(claimRegistrationRequestDTO.getToDate(), "") && !Objects.equals(claimRegistrationRequestDTO.getReportType(), "") && !Objects.equals(claimRegistrationRequestDTO.getCurrency(), "") && !Objects.equals(claimRegistrationRequestDTO.getUser(), "")) {
            ArrayList<ClaimRegistrationReportDTO> claimRegistrationReportDTOArrayList = reportingService.getClaimRegistrationReportDetails(claimRegistrationRequestDTO);
            ClaimRegistrationReportDTOList claimRegistrationReportDTOList = new ClaimRegistrationReportDTOList();
            claimRegistrationReportDTOList.setClaimRegistrationReportDTOArrayList(claimRegistrationReportDTOArrayList);
            claimRegistrationReportDTOList.setFromDate(claimRegistrationRequestDTO.getFromDate());
            claimRegistrationReportDTOList.setToDate(claimRegistrationRequestDTO.getToDate());
            claimRegistrationReportDTOList.setUser(claimRegistrationRequestDTO.getUser());
            Logo reportLogo = reportingService.getLogo();
            ByteArrayInputStream input = new ByteArrayInputStream(reportLogo.getCmgLogo());
            claimRegistrationReportDTOList.setImage(input);
            JRBeanArrayDataSource jrBeanArrayDataSource = new JRBeanArrayDataSource(new ClaimRegistrationReportDTOList[]{claimRegistrationReportDTOList});
            byteArrayResource = reportGenarateMethode(jrxmlFileName, jrBeanArrayDataSource, claimRegistrationRequestDTO.getReportType());
            response = new Response(2, TRUE, "sucess", byteArrayResource);
        } else {
            throw new InvalidInputExceptions("Null input values");
        }
        return response;
    }

    public Response getIaslReportPDF(ClaimNoDTOReport claimNoDTOReport) throws ClassNotFoundException, IOException, JRException {
        String jrxmlFileName = "IASLReport/IaslReport.jrxml";
        if (!Objects.equals(claimNoDTOReport.getClaimNo(), "") && !Objects.equals(claimNoDTOReport.getUser(), "") && !Objects.equals(claimNoDTOReport.getReportType(), "")) {
            IaslReportDTO iaslReportDTO = reportingService.getIASLReportDTO(claimNoDTOReport.getClaimNo(), claimNoDTOReport.getUser());
            JRBeanArrayDataSource jrBeanArrayDataSource = new JRBeanArrayDataSource(new IaslReportDTO[]{iaslReportDTO});
            byteArrayResource = reportGenarateMethode(jrxmlFileName, jrBeanArrayDataSource, claimNoDTOReport.getReportType());
            response = new Response(2, TRUE, "sucess", byteArrayResource);
        } else {
            throw new InvalidInputExceptions("Null input values");
        }
        return response;
    }

    public Response getIntimationReport(ClaimNoDTOReport claimNoDTOReport) throws ClassNotFoundException, IOException, JRException {
        String jrxmlFileName = "IntimationReport/IntimationReport.jrxml";
        if (!Objects.equals(claimNoDTOReport.getClaimNo(), "") && !Objects.equals(claimNoDTOReport.getUser(), "") && !Objects.equals(claimNoDTOReport.getReportType(), "")) {
            Response responce = reportingService.getIntimationReportDTO(claimNoDTOReport.getClaimNo(), claimNoDTOReport.getUser());
            IntimationReportDTO intimationReportDTO = (IntimationReportDTO) responce.getData();
            JRBeanArrayDataSource jrBeanArrayDataSource = new JRBeanArrayDataSource(new IntimationReportDTO[]{(IntimationReportDTO) responce.getData()});
            byteArrayResource = reportGenarateMethode(jrxmlFileName, jrBeanArrayDataSource, claimNoDTOReport.getReportType());
            response = new Response(2, TRUE, "sucess", byteArrayResource);
        } else {
            throw new InvalidInputExceptions("Null input values");
        }
        return response;
    }


    public Response paymentDetailReport(PaymentRequestDTO paymentRequestDTO) throws ClassNotFoundException, IOException, JRException {
//        String jrxmlFileName = "PaymentDetailReport/PaymentDetailReport.jrxml";
        String jrxmlFileName = "PaymentDetail/PaymentDetailReport.jrxml";
//        String jrxmlFileName = "PaymentDetail/PaymentDetailReport.jrxml";
        if (!Objects.equals(paymentRequestDTO.getFromDate(), "") && !Objects.equals(paymentRequestDTO.getToDate(), "") && !Objects.equals(paymentRequestDTO.getReportType(), "") && !Objects.equals(paymentRequestDTO.getCurrency(), "") && !Objects.equals(paymentRequestDTO.getUser(), "")) {
            ArrayList<PaymentResponseDTO> paymentDetailsList = (ArrayList<PaymentResponseDTO>) reportingService.getPaymentDetailsList(paymentRequestDTO).getData();
            MainPaymentDTO mainPaymentDTO = new MainPaymentDTO();
            mainPaymentDTO.setPaymentResponseDTOArrayList(paymentDetailsList);
            mainPaymentDTO.setFromDate(paymentRequestDTO.getFromDate());
            mainPaymentDTO.setToDate(paymentRequestDTO.getToDate());
            mainPaymentDTO.setUser(paymentRequestDTO.getUser());
            Logo reportLogo = reportingService.getLogo();
            ByteArrayInputStream input = new ByteArrayInputStream(reportLogo.getCmgLogo());
            mainPaymentDTO.setImage(input);
            JRBeanArrayDataSource jrBeanArrayDataSource = new JRBeanArrayDataSource(new MainPaymentDTO[]{mainPaymentDTO});
            byteArrayResource = reportGenarateMethode(jrxmlFileName, jrBeanArrayDataSource, paymentRequestDTO.getReportType());
            response = new Response(2, TRUE, "sucess", byteArrayResource);
        } else {
            throw new InvalidInputExceptions("Null input values");
        }
        return response;
    }

//    public Response generateJobAssignmentReportDTO(JobNoDTO jobNoDTO){
//
//        String jrxmlFileName = "JobAssignmentReport/JobAssignmentReport.jrxml";
//        Response response;
//
//        JobAssignmentReportDTO jobAssignmentReportDTO = new JobAssignmentReportDTO();
//
//        JobAssignment jobAssignment = jobAssignmentRepository.findByJobNo(jobNoDTO.getJobNo());
//
//        if (jobAssignment != null) {
//
//            ClaimIntimation claimIntimation = claimIntimationRepository.findBySequenceNo(jobAssignment.getJobAssignmentPK().getIntimationSequenceNo());
//            AssignLossAdjuster lossAdjuster = assignLossAdjusterRepository.findByAssignLossAdjusterPK_JobSequenceNo(jobAssignment.getJobAssignmentPK().getSequenceNo());
//            ExternalPerson externalPerson = externalPersonRepository.findByCode(lossAdjuster.getExternalPersonCode());
//
//            jobAssignmentReportDTO.setJobNo(jobAssignment.getJobNo());
//            jobAssignmentReportDTO.setJobType(jobAssignment.getSurveyType().getSurveyDescription());
//            jobAssignmentReportDTO.setAppointedDate(dateConversion.convertFromDateToString(lossAdjuster.getAppointedDate(), "dd-MMM-yyyy"));
//            jobAssignmentReportDTO.setVehicleNo(claimIntimation.getRiskName());
//            jobAssignmentReportDTO.setLossAdjusterName(externalPersonService.getExternalPersonName(externalPerson));
//            jobAssignmentReportDTO.setClaimNo(claimIntimation.getClaimNo());
//            jobAssignmentReportDTO.setClaimStatus(referenceService.getClaimStatus(claimIntimation.getClaimStatus()));
//            jobAssignmentReportDTO.setStartTime(dateConversion.convertFromDateToString(lossAdjuster.getAppointedDate()));
//            jobAssignmentReportDTO.setEndTime(dateConversion.convertFromDateToString(lossAdjuster.getSubmittedDate()));
//
//            Logo reportLogo = reportingService.getLogo();
//
//            ByteArrayInputStream input = new ByteArrayInputStream(reportLogo.getCmgLogo());
//            jobAssignmentReportDTO.setImage(input);
//
//
//            jobAssignmentReportDTO.setUser(jobNoDTO.getUser());
//
//            try {
//                JRBeanArrayDataSource jrBeanArrayDataSource = new JRBeanArrayDataSource(new JobAssignmentReportDTO[]{jobAssignmentReportDTO});
//
//                byteArrayResource = reportGenarateMethode(jrxmlFileName, jrBeanArrayDataSource, jobNoDTO.getReportType());
//
//                response = new Response(2, TRUE, "sucess", byteArrayResource);
//
//
//            } catch (NullPointerException e) {
//                e.printStackTrace();
//                response = new Response(2, FALSE, "No data to generate the Report", "");
//            } catch (Exception e) {
//                e.printStackTrace();
//                response = new Response(3, FALSE, "Technical failure", "");
//            }
//        }
//        else {
//
//            response = new Response(400, false, "The Job Assignment does not exists");
//
//        }
//
//        return response;
//
//    }


    public Response generateClaimRatioReport(ClaimRatioDTO claimRatioDTO) throws ParseException, ClassNotFoundException, IOException, JRException {
        String jrxmlFileName = "ClaimRatioReport/ClaimRatioReport.jrxml";
        if (claimRatioDTO != null) {
            String wk_FromDate = claimRatioDTO.getFromDate() == null || claimRatioDTO.getFromDate().equals("") ? "" : claimRatioDTO.getFromDate();
            String wk_ToDate = claimRatioDTO.getToDate() == null || claimRatioDTO.getToDate().equals("") ? "" : claimRatioDTO.getToDate();
            String wk_Class = claimRatioDTO.getCls() == null || claimRatioDTO.getCls().isEmpty() ? "A" : claimRatioDTO.getCls();
            String wk_Product = claimRatioDTO.getProduct() == null || claimRatioDTO.getProduct().isEmpty() ? "B" : claimRatioDTO.getProduct();
            String wk_IntmType = claimRatioDTO.getIntermediaryType() == null || claimRatioDTO.getIntermediaryType().isEmpty() ? "C" : claimRatioDTO.getIntermediaryType();
            String wk_IntmName = claimRatioDTO.getIntermediaryName() == null || claimRatioDTO.getIntermediaryName().isEmpty() ? "D" : claimRatioDTO.getIntermediaryName();
            DateFormat df1 = new SimpleDateFormat("dd-MMM-yyyy"); // for parsing input
            DateFormat df2 = new SimpleDateFormat("yyyy/MMM/dd");  // for formatting output
            String inputFromDate = claimRatioDTO.getFromDate();
            String inputToDate = claimRatioDTO.getToDate();
            Date date1 = df1.parse(inputFromDate);
            Date date2 = df1.parse(inputToDate);
            String formDate = df2.format(date1);
            String toDate = df2.format(date2);
//            String query = "SELECT crn.CRN_BRN_CODE,crn.CRN_CLA_CODE,crn.CRN_PRD_CODE,uwt.POL_MARKETING_EXECUTIVE_CODE, "+
//                    "smm.SFC_INITIALS || ' ' || smm.SFC_FIRST_NAME || ' ' || smm.SFC_SURNAME as INTRNAME,  smm.SFC_INT_EXT,"+
//                    " NVL((SELECT FND_AMONT FROM RC_T_FUNDS funds WHERE FND_FUN_CODE='01' AND funds.FND_CRN_SEQ_NO=crn.CRN_SEQ_NO ),0) as BASICPREMIUM, " +
//                    " NVL((SELECT FND_AMONT FROM RC_T_FUNDS funds WHERE FND_FUN_CODE='02' AND funds.FND_CRN_SEQ_NO=crn.CRN_SEQ_NO ),0) as SRCC," +
//                    " NVL((SELECT FND_AMONT FROM RC_T_FUNDS funds WHERE FND_FUN_CODE='03' AND funds.FND_CRN_SEQ_NO=crn.CRN_SEQ_NO ),0) as TC, " +
//                    " NVL(tax.TXS_AMONT,0) AS VAT, NVL(payee.RPQ_AMOUNT,0), ROUND((NVL(payee.RPQ_AMOUNT,0)/NVL(uwt.POL_PREMIUM,1)*100),2) AS CLAIMRATIO " +
//                    "FROM RC_T_CREDIT_NOTE crn, CL_T_INTIMATION clt , CL_T_REQ_PAYEES payee, UW_T_POLICIES uwt, SM_M_SALES_FORCE smm, rc_t_taxes tax "+
//                    "WHERE clt.INT_POLICY_SEQ = crn.CRN_POL_SEQ_NO "+
//                    "AND NVL(uwt.POL_CLA_CODE,'A') = ?"+
//                    "AND NVL(uwt.POL_PRD_CODE,'B') = ?"+
//                    "AND smm.SFC_CODE = uwt.POL_MARKETING_EXECUTIVE_CODE "+
//                    "AND NVL(smm.SFC_INT_EXT,'C') = ?"+
//                    "AND NVL(smm.SFC_INITIALS || ' ' || smm.SFC_FIRST_NAME || ' ' || smm.SFC_SURNAME,'D') = ?" +
//                    "AND payee.RPQ_INT_SEQ = clt.INT_SEQ_NO "+
//                    "AND uwt.POL_SEQ_NO =  crn.CRN_POL_SEQ_NO " +
//                    "AND tax.TXS_TAX_CODE = '01' " +
//                    "AND tax.TXS_CRN_SEQ_NO = crn.CRN_SEQ_NO "+
//                    "AND TRUNC(crn.CRN_TRN_DATE) >= TO_DATE(?,'DD-MON-YYYY') " +
//                    "AND TRUNC(crn.CRN_TRN_DATE) <= TO_DATE(?,'DD-MON-YYYY') ";


//            List<Object[]> list = (List<Object[]>)  entityManager.createNativeQuery(query)
//                    .setParameter(1,wk_Class)
//                    .setParameter(2,wk_Product)
//                    .setParameter(3,wk_IntmType)
//                    .setParameter(4,wk_IntmName)
//                    .setParameter(5,wk_FromDate)
//                    .setParameter(6,wk_ToDate)
////                    .setParameter(5,formDate)
////                    .setParameter(6,toDate)
//                    .getResultList();
            String query = "SELECT crn.CRN_BRN_CODE,crn.CRN_CLA_CODE,crn.CRN_PRD_CODE,uwt.POL_MARKETING_EXECUTIVE_CODE, smm.SFC_INITIALS || ' ' || smm.SFC_FIRST_NAME || ' ' || smm.SFC_SURNAME as INTRNAME, " +
                    " smm.SFC_INT_EXT, NVL((SELECT FND_AMONT FROM RC_T_FUNDS funds WHERE FND_FUN_CODE='01' AND funds.FND_CRN_SEQ_NO=crn.CRN_SEQ_NO ),0) as BASICPREMIUM, " +
                    " NVL((SELECT FND_AMONT FROM RC_T_FUNDS funds WHERE FND_FUN_CODE='02' AND funds.FND_CRN_SEQ_NO=crn.CRN_SEQ_NO ),0) as SRCC, " +
                    " NVL((SELECT FND_AMONT FROM RC_T_FUNDS funds WHERE FND_FUN_CODE='03' AND funds.FND_CRN_SEQ_NO=crn.CRN_SEQ_NO ),0) as TC, " +
                    " NVL(tax.TXS_AMONT,0) AS VAT, NVL(payee.RPQ_AMOUNT,0), ROUND((NVL(payee.RPQ_AMOUNT,0)/NVL(uwt.POL_PREMIUM,1)*100),2) AS CLAIMRATIO " +
                    "FROM RC_T_CREDIT_NOTE crn, CL_T_INTIMATION clt , CL_T_REQ_PAYEES payee, UW_T_POLICIES uwt, SM_M_SALES_FORCE smm, rc_t_taxes tax " +
                    "WHERE clt.INT_POLICY_SEQ = crn.CRN_POL_SEQ_NO " +
                    "AND NVL(uwt.POL_CLA_CODE,'A') = decode(?,'A',NVL(uwt.POL_CLA_CODE,'A'),?) " +
                    "AND NVL(uwt.POL_PRD_CODE,'B') = decode(?,'B',NVL(uwt.POL_PRD_CODE,'B'),?) " +
                    "AND smm.SFC_CODE = uwt.POL_MARKETING_EXECUTIVE_CODE " +
                    "AND NVL(smm.SFC_INT_EXT,'C') = decode(?,'C',NVL(smm.SFC_INT_EXT,'C'),?) " +
                    "AND NVL(smm.SFC_INITIALS || ' ' || smm.SFC_FIRST_NAME || ' ' || smm.SFC_SURNAME,'D') = decode(?,'D',NVL(smm.SFC_INITIALS || ' ' || smm.SFC_FIRST_NAME || ' ' || smm.SFC_SURNAME,'D'),?) " +
                    "AND payee.RPQ_INT_SEQ = clt.INT_SEQ_NO " +
                    "AND uwt.POL_SEQ_NO =  crn.CRN_POL_SEQ_NO " +
                    "AND tax.TXS_TAX_CODE = '01' " +
                    "AND tax.TXS_CRN_SEQ_NO = crn.CRN_SEQ_NO " +
                    "AND TRUNC(crn.CRN_TRN_DATE) >= TO_DATE(?,'DD-MON-YYYY') " +
                    "AND TRUNC(crn.CRN_TRN_DATE) <= TO_DATE(?,'DD-MON-YYYY') ";
            List<Object[]> list = (List<Object[]>) entityManager.createNativeQuery(query)
                    .setParameter(1, wk_Class)
                    .setParameter(2, wk_Class)
                    .setParameter(3, wk_Product)
                    .setParameter(4, wk_Product)
                    .setParameter(5, wk_IntmType)
                    .setParameter(6, wk_IntmType)
                    .setParameter(7, wk_IntmName)
                    .setParameter(8, wk_IntmName)
                    .setParameter(9, wk_FromDate)
                    .setParameter(10, wk_ToDate)
                    .getResultList();

            List<ClaimRatioReportDTO> jobAssignmentReportDTOList = new ArrayList<>();
            ClaimRatioReportDTO claimRatioReportDTO2;
            for (Object[] jobDetailsList : list) {
                int x = 0;
                claimRatioReportDTO2 = new ClaimRatioReportDTO();
                for (Object value : jobDetailsList) {
                    switch (x) {
                        case 0:
                            claimRatioReportDTO2.setBranch((String) value);
                            break;
                        case 1:
                            claimRatioReportDTO2.setClassType((String) value);
                            break;
                        case 2:
                            claimRatioReportDTO2.setProduct((String) value);
                            break;
                        case 3:
                            claimRatioReportDTO2.setInterMediaryCode((String) value);
                            break;
                        case 4:
                            claimRatioReportDTO2.setInterMediaryName((String) value);
                            break;
                        case 5:
                            claimRatioReportDTO2.setBusinessChannel((String) value);
                            break;
                        case 6:
                            claimRatioReportDTO2.setBasicPremium((BigDecimal) value);
                            break;
                        case 7:
                            claimRatioReportDTO2.setSrcc((BigDecimal) value);
                            break;
                        case 8:
                            claimRatioReportDTO2.setTc((BigDecimal) value);
                            break;
                        case 9:
                            claimRatioReportDTO2.setVat((BigDecimal) value);
                            break;
                        case 10:
                            claimRatioReportDTO2.setClaimPaidAmount((BigDecimal) value);
                            break;
                        case 11:
                            claimRatioReportDTO2.setClaimRatio((BigDecimal) value);
                            break;
                    }
                    x += 1;
                }
                jobAssignmentReportDTOList.add(claimRatioReportDTO2);
            }
            Logo reportLogo = reportingService.getLogo();
            ClaimRatioReportListDTO claimRatioReportListDTO = new ClaimRatioReportListDTO();
            claimRatioReportListDTO.setClaimRatioReportDTOList(jobAssignmentReportDTOList);
            ByteArrayInputStream input = new ByteArrayInputStream(reportLogo.getCmgLogo());
            claimRatioReportListDTO.setImage(input);
            claimRatioReportListDTO.setUser(claimRatioDTO.getUser());

            JRBeanArrayDataSource jrBeanArrayDataSource = new JRBeanArrayDataSource(new ClaimRatioReportListDTO[]{claimRatioReportListDTO});

            byteArrayResource = reportGenarateMethode(jrxmlFileName, jrBeanArrayDataSource, claimRatioDTO.getReportType());

            response = new Response(2, TRUE, "sucess", byteArrayResource);
        } else {
            throw new InvalidInputExceptions("Null input values");
        }
        return response;
    }


    public Response JobAssignmentReport(JobAssignmentReportRequestDTO jobAssignmentReportRequestDTO) throws ClassNotFoundException, IOException, JRException, ParseException {
        String jrxmlFileName = "JobAssignmentDetailReport/JobAssignmentReport.jrxml";
        if (!Objects.equals(jobAssignmentReportRequestDTO.getFromDate(), "") && !Objects.equals(jobAssignmentReportRequestDTO.getToDate(), "") && !Objects.equals(jobAssignmentReportRequestDTO.getReportType(), "") && !Objects.equals(jobAssignmentReportRequestDTO.getUser(), "")) {
            List<JobAssignmentReportDTO> paymentDetailsList = reportingService.generateJobAssignmentReportDTO(jobAssignmentReportRequestDTO);
            MainJobAssignment mainJobAssignment = new MainJobAssignment();
            mainJobAssignment.setJobAssignmentReportDTOList(paymentDetailsList);
            mainJobAssignment.setFromDate(jobAssignmentReportRequestDTO.getFromDate());
            mainJobAssignment.setToDate(jobAssignmentReportRequestDTO.getToDate());
            mainJobAssignment.setUser(jobAssignmentReportRequestDTO.getUser());
            Logo reportLogo = reportingService.getLogo();
            ByteArrayInputStream input = new ByteArrayInputStream(reportLogo.getCmgLogo());
            mainJobAssignment.setImage(input);
            JRBeanArrayDataSource jrBeanArrayDataSource = new JRBeanArrayDataSource(new MainJobAssignment[]{mainJobAssignment});
            byteArrayResource = reportGenarateMethode(jrxmlFileName, jrBeanArrayDataSource, jobAssignmentReportRequestDTO.getReportType());
            response = new Response(2, TRUE, "sucess", byteArrayResource);
        } else {
            throw new InvalidInputExceptions("Null input values");
        }
        return response;
    }

}



