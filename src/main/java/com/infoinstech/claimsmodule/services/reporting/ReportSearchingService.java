package com.infoinstech.claimsmodule.services.reporting;

import com.infoinstech.claimsmodule.dto.claiminquiry.ClaimInquiryResultDTO;
import com.infoinstech.claimsmodule.dto.claiminquiry.ClaimSearchResultDTO;
import com.infoinstech.claimsmodule.dto.claiminquiry.NotificationSearchResultDTO;
import com.infoinstech.claimsmodule.dto.common.Response;
import com.infoinstech.claimsmodule.dto.reports.ClaimIndividualReportsDTO;
import com.infoinstech.claimsmodule.domain.model.claims.transaction.ClaimNotification;
import com.infoinstech.claimsmodule.exceptions.types.DBException;
import oracle.jdbc.OracleTypes;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.sql.DataSource;
import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;

@Service
public class ReportSearchingService {

    @Autowired
    private DataSource dataSource;

    public Response searchClaimsForReportGeneration (ClaimIndividualReportsDTO requestDTO){
        ClaimIndividualReportsDTO claimInquiryResultDTO = new ClaimIndividualReportsDTO();
        List<ClaimIndividualReportsDTO> claimSearchResultDTOList = new ArrayList<>();
        String query = "CALL PK_CL_ANDROID_APP_DEV.PU_CLAIM_INDV_REPORTS_SEARCH(?,?,?,?,?,?,?)";
        Response response = new Response();
        CallableStatement callableStatement;
        Connection connection;
        ResultSet resultSet;
        try {
            connection = dataSource.getConnection();
            callableStatement = connection.prepareCall(query);
            callableStatement.setString(1, requestDTO.getClassCode());
            callableStatement.setString(2, requestDTO.getProductCode());
            callableStatement.setString(3, requestDTO.getCustomerName());
            callableStatement.setString(4, requestDTO.getPolicyNo());
            callableStatement.setString(5, requestDTO.getRiskName());
            callableStatement.setString(6,requestDTO.getClaimNo());
            callableStatement.registerOutParameter(7, OracleTypes.CURSOR);
            callableStatement.execute();
            resultSet = (ResultSet) callableStatement.getObject(7);
            while (resultSet != null && resultSet.next()) {
                ClaimIndividualReportsDTO individualReportsDTO = new ClaimIndividualReportsDTO();
                individualReportsDTO.setClassCode(resultSet.getString("CLA_PREFIX"));
                individualReportsDTO.setProductCode(resultSet.getString("PRD_DESCRIPTION"));
                individualReportsDTO.setCustomerName(resultSet.getString("CUS_NAME"));
                individualReportsDTO.setPolicyNo(resultSet.getString("INT_POLICY_NO"));
                individualReportsDTO.setRiskName(resultSet.getString("INT_PRS_NAME"));
                individualReportsDTO.setClaimNo(resultSet.getString("INT_CLAIM_NO"));
                claimSearchResultDTOList.add(individualReportsDTO);
            }
            response = new Response(200, true, claimSearchResultDTOList);
        } catch (Exception e) {
            e.printStackTrace();
            throw new DBException(e.getMessage());
        }
        return response;
    }

}

