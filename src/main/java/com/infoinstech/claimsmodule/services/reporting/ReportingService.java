package com.infoinstech.claimsmodule.services.reporting;

import com.infoinstech.claimsmodule.dto.common.Response;
import com.infoinstech.claimsmodule.dto.reports.*;
import com.infoinstech.claimsmodule.dto.reports.claimregistrationreport.ClaimRegistrationReportDTO;
import com.infoinstech.claimsmodule.dto.reports.claimregistrationreport.ClaimRegistrationRequestDTO;
import com.infoinstech.claimsmodule.dto.reports.claimregistrationreport.PayDetails;
import com.infoinstech.claimsmodule.dto.reports.claimregistrationsheet.*;
import com.infoinstech.claimsmodule.dto.reports.paymentDetailReport.PaymentResponseDTO;
import com.infoinstech.claimsmodule.dto.reports.paymentDetailReport.PaymentRequestDTO;
import com.infoinstech.claimsmodule.domain.model.claims.history.OtherChargesPaymentHistory;
import com.infoinstech.claimsmodule.domain.model.claims.history.RequisitionHistory;
import com.infoinstech.claimsmodule.domain.model.claims.history.RequisitionPayeeHistory;
import com.infoinstech.claimsmodule.domain.model.claims.transaction.*;
import com.infoinstech.claimsmodule.domain.model.common.Logo;
import com.infoinstech.claimsmodule.domain.model.receipting.transaction.DebitNote;
import com.infoinstech.claimsmodule.domain.model.salesmarketing.SystemUser;
import com.infoinstech.claimsmodule.domain.model.underwriting.master.Customer;
import com.infoinstech.claimsmodule.domain.model.underwriting.master.CustomerAddress;
import com.infoinstech.claimsmodule.domain.model.underwriting.temporary.TempPolicy;
import com.infoinstech.claimsmodule.domain.model.underwriting.temporary.TempPolicyFinancialInterest;
import com.infoinstech.claimsmodule.domain.model.underwriting.temporary.TempPolicyRisk;
import com.infoinstech.claimsmodule.domain.model.underwriting.transaction.PolicyRisk;
import com.infoinstech.claimsmodule.domain.packages.PK_CL_T_INTIMATION;
import com.infoinstech.claimsmodule.domain.repository.claims.history.OtherChargesPaymentHistoryRepository;
import com.infoinstech.claimsmodule.domain.repository.claims.history.RequisitionHistoryRepository;
import com.infoinstech.claimsmodule.domain.repository.claims.history.RequisitionPayeeHistoryRepository;
import com.infoinstech.claimsmodule.domain.repository.claims.master.ExternalPersonRepository;
import com.infoinstech.claimsmodule.domain.repository.claims.reference.PolicyStationRepository;
import com.infoinstech.claimsmodule.domain.repository.claims.transaction.*;
import com.infoinstech.claimsmodule.domain.repository.common.LogoRepository;
import com.infoinstech.claimsmodule.domain.repository.receipting.transaction.DebitNoteRepository;
import com.infoinstech.claimsmodule.domain.repository.salesmarketing.SystemUserRepository;
import com.infoinstech.claimsmodule.domain.repository.underwriting.master.CustomerAddressRepository;
import com.infoinstech.claimsmodule.domain.repository.underwriting.master.CustomerRepository;
import com.infoinstech.claimsmodule.domain.repository.underwriting.temporary.TempPolicyFinancialInterestRepository;
import com.infoinstech.claimsmodule.domain.repository.underwriting.temporary.TempPolicyRiskRepository;
import com.infoinstech.claimsmodule.domain.repository.underwriting.transaction.PolicyRiskRepository;
import com.infoinstech.claimsmodule.exceptions.types.NotFoundException;
import com.infoinstech.claimsmodule.services.business.*;
import com.infoinstech.claimsmodule.services.reference.ReferenceService;
import com.infoinstech.claimsmodule.services.util.DateConversion;
import com.infoinstech.claimsmodule.services.util.Formatter;
import org.springframework.beans.factory.annotation.Autowired;
import com.infoinstech.claimsmodule.dto.reports.jobassignmentdetailsreport.JobAssignmentReportDTO;
import com.infoinstech.claimsmodule.dto.reports.jobassignmentdetailsreport.JobAssignmentReportRequestDTO;
import org.springframework.core.io.ResourceLoader;
import org.springframework.stereotype.Service;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import java.io.ByteArrayInputStream;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Optional;

@Service
public class ReportingService {

    @Autowired
    private ClaimIntimationRepository claimIntimationRepository;
    @Autowired
    private JobAssignmentRepository jobAssignmentRepository;
    @Autowired
    private AssignLossAdjusterRepository assignLossAdjusterRepository;
    @Autowired
    private ExternalPersonRepository externalPersonRepository;
    @Autowired
    private TempPolicyRiskRepository tempPolicyRiskRepository;
    @Autowired
    private TempPolicyFinancialInterestRepository tempPolicyFinancialInterestRepository;
    @Autowired
    private DebitNoteRepository debitNoteRepository;
    @Autowired
    private CustomerRepository customerRepository;
    @Autowired
    private RequisitionPayeeRepository requisitionPayeeRepository;
    @Autowired
    private OtherChargePaymentsRepository otherChargePaymentsRepository;
    @Autowired
    private LogoRepository logoRepository;

    @Autowired
    private RequisitionRepository requisitionRepository;

    //Dumz  14/03/2018
    @Autowired
    private PolicyStationRepository policyStationRepository;
    @Autowired
    private CustomerAddressRepository customerAddressRepository;
    @Autowired
    private ProvisionPaymentDetailsRepository provisionPaymentDetailsRepository;
    @Autowired
    private RequisitionPayeeHistoryRepository requisitionPayeeHistoryRepository;
    @Autowired
    private OtherChargesPaymentHistoryRepository otherChargesPaymentHistoryRepository;
    @Autowired
    private RequisitionHistoryRepository requisitionHistoryRepository;
    @Autowired
    private SystemUserRepository systemUserRepository;
    @Autowired
    private PolicyRiskRepository policyRiskRepository;
    @Autowired
    private SystemUserService systemUserService;

    @Autowired
    private DateConversion dateConversion;
    @Autowired
    private Formatter formatter;
    @Autowired
    private ReferenceService referenceService;
    @Autowired
    private WrapperService wrapperService;
    @Autowired
    private PolicyRiskInformationService policyRiskInformationService;
    @Autowired
    private CustomerService customerService;
    @Autowired
    private ClaimIntimationService claimIntimationService;
    @Autowired
    private ExternalPersonService externalPersonService;
    @Autowired
    private ClaimProvisionService claimProvisionService;
    @Autowired
    private PK_CL_T_INTIMATION pk_cl_t_intimation;

    @Autowired
    private ResourceLoader resourceLoader;
    @Autowired
    private ReportingService reportingService;

    @PersistenceContext
    private EntityManager entityManager;

    private Response response;


    public Response getIntimationReportDTO(String claimNo, String user) {

        ClaimIntimation claimIntimation = claimIntimationRepository.findByClaimNo(claimNo);

        Response response;

        if (claimIntimation != null) {

            wrapperService.deleteDataFromTempPolicyTables();
            TempPolicy tempPolicy = wrapperService.buildPolicy(claimIntimation.getPolicyNo(), claimIntimation.getLossDate());
            TempPolicyRisk tempPolicyRisk = tempPolicyRiskRepository.findByTempPolicyRiskPK_PolicySequenceNoAndRiskOrderNo(tempPolicy.getSequenceNo(), claimIntimation.getRiskOrderNo());

            List<TempPolicyFinancialInterest> financialInterestList = tempPolicyFinancialInterestRepository.findByTempPolicyFinancialInterestPK_PolicySequenceNo(
                    tempPolicy.getSequenceNo());

            IntimationReportDTO intimationReportDTO = new IntimationReportDTO();

//            Logo reportLogo = getLogo();
//            ByteArrayInputStream input = new ByteArrayInputStream(reportLogo.getCmgLogo());
//            intimationReportDTO.setImage(input);
            intimationReportDTO.setUser(user);

            intimationReportDTO.setIntimationDate(dateConversion.convertFromDateToString(claimIntimation.getIntimationDate(), "dd-MMM-yyyy HH:mm:ss"));
            intimationReportDTO.setDateOfLoss(dateConversion.convertFromDateToString(claimIntimation.getLossDate(), "dd-MMM-yyyy HH:mm:ss"));
            intimationReportDTO.setVehicleNo(claimIntimation.getRiskName());
            intimationReportDTO.setClaimNo(claimIntimation.getClaimNo());

            intimationReportDTO.setInsured(customerService.getCustomerName(claimIntimation.getCustomer()));
            intimationReportDTO.setApproximateCauseOfLoss(formatter.format(claimIntimation.getEstimatedAmount()));
            intimationReportDTO.setTypeOfCover(claimIntimation.getProduct().getDescription());
            intimationReportDTO.setModeOfInformation(claimIntimation.getRefInformationMode().getDescription());
            intimationReportDTO.setInsuredAtFault(referenceService.getYesOrNo(claimIntimation.getInsuredAtFault()));
            intimationReportDTO.setPoliceStation(claimIntimation.getPoliceStation());
            intimationReportDTO.setDriverName(claimIntimation.getDriverName());
            intimationReportDTO.setAccidentDistrict("");
            intimationReportDTO.setPolicyNo(tempPolicy.getPolicyNo());
            intimationReportDTO.setPeriodOfCover(
                    dateConversion.convertFromDateToString(tempPolicy.getPeriodFrom(), "dd-MMM-yyyy") + "  -  " + dateConversion.convertFromDateToString(tempPolicy.getPeriodTo(), "dd-MMM-yyyy"));
            intimationReportDTO.setBranch(claimIntimation.getPolicyBranch().getDescription());

            SystemUser intermediary = systemUserRepository.findByCode(tempPolicy.getIntermediaryCode());

            intimationReportDTO.setMarketingExecutive(null);
            intimationReportDTO.setSumInsured(tempPolicyRisk.getSumInsured());
            intimationReportDTO.setMakeAndModel(policyRiskInformationService.getMakeAndModel(tempPolicyRisk));

            String financialInterest = "";


            for (TempPolicyFinancialInterest tempPolicyFinancialInterest : financialInterestList) {
                if (tempPolicyFinancialInterest == null)
                    continue;
                if (tempPolicyFinancialInterest.getRefFinancialInterest() == null)
                    continue;

                financialInterest += tempPolicyFinancialInterest.getRefFinancialInterest().getDescription();
            }


            intimationReportDTO.setFinancialInterest(financialInterest);

            response = new Response(200, true, intimationReportDTO);

        } else {
            throw new NotFoundException("The Claim No does not exists");
            //response = new Response(400, false, "The Claim No does not exists");
        }


        return response;
    }

    public Response getIntimationReportDTOForAgent(String claimNo, String user) {

        ClaimIntimation claimIntimation = claimIntimationRepository.findByClaimNo(claimNo);

        Response response;

        if (claimIntimation != null) {

            wrapperService.deleteDataFromTempPolicyTables();
            TempPolicy tempPolicy = wrapperService.buildPolicy(claimIntimation.getPolicyNo(), claimIntimation.getLossDate());
            TempPolicyRisk tempPolicyRisk = tempPolicyRiskRepository.findByTempPolicyRiskPK_PolicySequenceNoAndRiskOrderNo(tempPolicy.getSequenceNo(), claimIntimation.getRiskOrderNo());

            List<TempPolicyFinancialInterest> financialInterestList = tempPolicyFinancialInterestRepository.findByTempPolicyFinancialInterestPK_PolicySequenceNo(
                    tempPolicy.getSequenceNo());

            IntimationReportDTO intimationReportDTO = new IntimationReportDTO();

//            Logo reportLogo = getLogo();
//            ByteArrayInputStream input = new ByteArrayInputStream(reportLogo.getCmgLogo());
//            intimationReportDTO.setImage(input);
            intimationReportDTO.setUser(user);

            intimationReportDTO.setIntimationDate(dateConversion.convertFromDateToString(claimIntimation.getIntimationDate(), "dd-MMM-yyyy HH:mm:ss"));
            intimationReportDTO.setDateOfLoss(dateConversion.convertFromDateToString(claimIntimation.getLossDate(), "dd-MMM-yyyy HH:mm:ss"));
            intimationReportDTO.setVehicleNo(claimIntimation.getRiskName());
            intimationReportDTO.setClaimNo(claimIntimation.getClaimNo());

            intimationReportDTO.setInsured(customerService.getCustomerName(claimIntimation.getCustomer()));
            intimationReportDTO.setApproximateCauseOfLoss(formatter.format(claimIntimation.getEstimatedAmount()));
            intimationReportDTO.setTypeOfCover(claimIntimation.getProduct().getDescription());
            intimationReportDTO.setModeOfInformation(claimIntimation.getRefInformationMode().getDescription());
            intimationReportDTO.setInsuredAtFault(referenceService.getYesOrNo(claimIntimation.getInsuredAtFault()));
            intimationReportDTO.setPoliceStation(claimIntimation.getPoliceStation());
            intimationReportDTO.setDriverName(claimIntimation.getDriverName());
            intimationReportDTO.setAccidentDistrict("");
            intimationReportDTO.setPolicyNo(tempPolicy.getPolicyNo());
            intimationReportDTO.setPeriodOfCover(
                    dateConversion.convertFromDateToString(tempPolicy.getPeriodFrom(), "dd-MMM-yyyy") + "  -  " + dateConversion.convertFromDateToString(tempPolicy.getPeriodTo(), "dd-MMM-yyyy"));
            intimationReportDTO.setBranch(claimIntimation.getPolicyBranch().getDescription());

            SystemUser intermediary = systemUserRepository.findByCode(tempPolicy.getIntermediaryCode());

            intimationReportDTO.setMarketingExecutive(null);
            intimationReportDTO.setSumInsured(tempPolicyRisk.getSumInsured());
            intimationReportDTO.setMakeAndModel(policyRiskInformationService.getMakeAndModel(tempPolicyRisk));

            String financialInterest = "";


            for (TempPolicyFinancialInterest tempPolicyFinancialInterest : financialInterestList) {
                if (tempPolicyFinancialInterest == null)
                    continue;
                if (tempPolicyFinancialInterest.getRefFinancialInterest() == null)
                    continue;

                financialInterest += tempPolicyFinancialInterest.getRefFinancialInterest().getDescription();
            }


            intimationReportDTO.setFinancialInterest(financialInterest);

            response = new Response(200, true, intimationReportDTO);

        } else {
            throw new NotFoundException("The Claim No does not exists");
            //response = new Response(400, false, "The Claim No does not exists");
        }


        return response;
    }

    public IaslReportDTO getIASLReportDTO(String claimNo, String user) {

        ClaimIntimation claimIntimation = claimIntimationRepository.findByClaimNo(claimNo);
        //PolicyStation policyStation = policyStationRepository.findAllByCpsCode(claimIntimation.getPoliceStationCode());
        wrapperService.deleteDataFromTempPolicyTables();

        if (claimIntimation != null) {

            TempPolicy tempPolicy = wrapperService.buildPolicy(claimIntimation.getPolicyNo(), claimIntimation.getLossDate());
            TempPolicyRisk tempPolicyRisk = tempPolicyRiskRepository.findByTempPolicyRiskPK_PolicySequenceNoAndRiskOrderNo(tempPolicy.getSequenceNo(), claimIntimation.getRiskOrderNo());

            CustomerAddress customerAddress = customerAddressRepository.findByCustomerCodeAndDefaultAddress(claimIntimation.getCustomer().getCustomerCode(), 'Y');

            IaslReportDTO iaslReportDTO = new IaslReportDTO();

            Logo reportLogo = getLogo();
            ByteArrayInputStream input = new ByteArrayInputStream(reportLogo.getCmgLogo());
            iaslReportDTO.setImage(input);

            iaslReportDTO.setUser(user);
            iaslReportDTO.setVehicleNo(formatter.replaceWithDash(claimIntimation.getRiskName()));
            iaslReportDTO.setChassisNo(formatter.replaceWithDash(policyRiskInformationService.getChassisNo(tempPolicyRisk)));
            iaslReportDTO.setEngineNo(formatter.replaceWithDash(policyRiskInformationService.getEngineNo(tempPolicyRisk)));
            iaslReportDTO.setLastName(formatter.replaceWithDash(customerService.getCustomerLastName(claimIntimation.getCustomer())));
            iaslReportDTO.setOtherNames(formatter.replaceWithDash(customerService.getCustomerOtherNames(claimIntimation.getCustomer())));
            iaslReportDTO.setAddress1(formatter.replaceWithDash(claimIntimation.getCustomer().getAddress1()));
            iaslReportDTO.setAddress2(formatter.replaceWithDash(claimIntimation.getCustomer().getAddress2()));
            iaslReportDTO.setCity(formatter.replaceWithDash(claimIntimation.getCustomer().getCity()));
            iaslReportDTO.setDateOfAccident(dateConversion.convertFromDateToString(claimIntimation.getLossDate(), "dd/MM/yyyy"));
            iaslReportDTO.setTimeOfAccident(claimIntimation.getLossTime());
            iaslReportDTO.setPoliceDivision(formatter.replaceWithDash(claimIntimation.getPoliceDivision()));//check claimIntimation.getPoliceDivision() OR policyStation.getCpsPoliceDivision()
            iaslReportDTO.setPoliceStation(formatter.replaceWithDash(claimIntimation.getPoliceStation()));
            iaslReportDTO.setDriverName(formatter.replaceWithDash(claimIntimation.getDriverName()));
            iaslReportDTO.setDriverNIC(formatter.replaceWithDash(claimIntimation.getDriverId()));
            iaslReportDTO.setDriverLicense(formatter.replaceWithDash(claimIntimation.getDriverLicenseNo()));
            iaslReportDTO.setPlaceOfAccident(formatter.replaceWithDash(claimIntimation.getPlaceOfLoss()));
            iaslReportDTO.setAccidentDistrict(formatter.replaceWithDash(customerAddress.getDistrict()));
            iaslReportDTO.setOtherPartyDetails(formatter.replaceWithDash(claimIntimation.getThirdPartyDetails()));//check
            iaslReportDTO.setRemarks(formatter.replaceWithDash(claimIntimation.getLossRemarks()));

            return iaslReportDTO;
        } else {
            return null;
        }
    }

    public ClaimRegistrationSheetDTO getClaimRegistrationSheet(String claimNo, String user) {
        ClaimRegistrationSheetDTO claimRegistrationSheetDTO = new ClaimRegistrationSheetDTO();
        ClaimIntimation claimIntimation = claimIntimationRepository.findByClaimNo(claimNo);
        if (claimIntimation != null) {
            wrapperService.deleteDataFromTempPolicyTables();
            TempPolicy tempPolicy = wrapperService.buildPolicy(claimIntimation.getPolicyNo(), claimIntimation.getLossDate());
            TempPolicyRisk tempPolicyRisk = tempPolicyRiskRepository.findByTempPolicyRiskPK_PolicySequenceNoAndRiskOrderNo(tempPolicy.getSequenceNo(), claimIntimation.getRiskOrderNo());

            List<TempPolicyFinancialInterest> financialInterestList = tempPolicyFinancialInterestRepository.findByTempPolicyFinancialInterestPK_PolicySequenceNo(
                    tempPolicy.getSequenceNo());

            Logo reportLogo = getLogo();
            ByteArrayInputStream input = new ByteArrayInputStream(reportLogo.getCmgLogo());
            claimRegistrationSheetDTO.setImage(input);


            claimRegistrationSheetDTO.setUser(user);

            claimRegistrationSheetDTO.setVehicleNo(claimIntimation.getRiskName());
            claimRegistrationSheetDTO.setNameOfInsured(customerService.getCustomerName(claimIntimation.getCustomer()));
            String financialInterest = "";
            for (TempPolicyFinancialInterest tempPolicyFinancialInterest : financialInterestList) {

                financialInterest += tempPolicyFinancialInterest.getRefFinancialInterest().getDescription();
            }
            claimRegistrationSheetDTO.setFinancialInterest(formatter.replaceWithDash(financialInterest));
            claimRegistrationSheetDTO.setClaimNo(claimIntimation.getClaimNo());
            claimRegistrationSheetDTO.setPolicyNo(claimIntimation.getPolicyNo());
            claimRegistrationSheetDTO.setSumInsured(claimIntimation.getSumInsured());

            ParticularsOfAccident particularsOfAccident = new ParticularsOfAccident();
            particularsOfAccident.setDriversName(claimIntimation.getDriverName());
            particularsOfAccident.setDriverLicenseNo(claimIntimation.getDriverLicenseNo());
            particularsOfAccident.setDateOfIntimation(dateConversion.convertFromDateToString(claimIntimation.getIntimationDate(), "dd-MMM-yyyy"));
            particularsOfAccident.setDateOfAccident(dateConversion.convertFromDateToString(claimIntimation.getLossDate(), "dd-MMM-yyyy"));
            particularsOfAccident.setTimeOfAccident(claimIntimation.getLossTime());
            particularsOfAccident.setPlaceOfAccident(claimIntimation.getPlaceOfLoss());
            claimRegistrationSheetDTO.setParticularsOfAccident(particularsOfAccident);

            ArrayList<DebitNote> debitNoteList = debitNoteRepository.findByCustomerCodeAndPolicyNoAndStatusAndDebitSettled(claimIntimation.getCustomerCode(),
                    claimIntimation.getPolicyNo(),
                    'A',
                    'Y');
            List<ParticularsOfPremium> particularsOfPremiumList = new ArrayList<>();
            ParticularsOfPremium particularsOfPremium = null;
            for (DebitNote debitNote : debitNoteList) {
                particularsOfPremium = new ParticularsOfPremium();
                particularsOfPremium.setDebitAmount(debitNote.getTotalAmount());
                particularsOfPremium.setDebitNo(debitNote.getDebitNoteNo());
                particularsOfPremium.setDateofPayment(dateConversion.convertFromDateToString(debitNote.getTransactionDate(), "dd-MMM-yyyy"));
                particularsOfPremium.setOutstandingAmount(debitNote.getCustomerOutstanding());
                particularsOfPremiumList.add(particularsOfPremium);
            }
            claimRegistrationSheetDTO.setParticularsOfPremium(particularsOfPremiumList);

            ParticularsOfPolicy particularsOfPolicy = new ParticularsOfPolicy();
            particularsOfPolicy.setMakeAndodel(policyRiskInformationService.getMakeAndModel(tempPolicyRisk));
            String yearOfMake = policyRiskInformationService.getYearOfManufacture(tempPolicyRisk);

            if (yearOfMake != null && yearOfMake.contains(".")) {
                particularsOfPolicy.setYearOfMake(yearOfMake.substring(0, 4));
            } else {
                particularsOfPolicy.setYearOfMake(yearOfMake);
            }

            Date registeredDate = dateConversion.convertStringToDate(policyRiskInformationService.getDateRegistered(tempPolicyRisk).substring(0, 10), "yyyy-MM-dd");
            particularsOfPolicy.setDateOfFirstRegistration(dateConversion.convertFromDateToString(registeredDate, "dd-MMM-yyyy"));
            particularsOfPolicy.setPurposeOfUse(policyRiskInformationService.getUseOfVehicle(tempPolicyRisk));
            particularsOfPolicy.setPeriodOfCover(dateConversion.convertFromDateToString(tempPolicy.getPeriodFrom(), "dd-MMM-yyyy") + " - " + dateConversion.convertFromDateToString(tempPolicy.getPeriodTo(), "dd-MMM-yyyy"));
            particularsOfPolicy.setTypeOfCover(claimIntimation.getProduct().getDescription());
            CustomerAddress customerAddress = customerAddressRepository.findByCustomerCodeAndDefaultAddress(tempPolicy.getCustomerCode(), 'Y');
            particularsOfPolicy.setAddress(customerAddress == null ? "-" : customerAddress.getLocationDescription());
            Customer customer = customerRepository.findByCustomerCode(tempPolicy.getCustomerCode());
            particularsOfPolicy.setIdNo(customer == null ? "-" : customer.getIndvNIC());
            particularsOfPolicy.setBranch(claimIntimation.getPolicyBranch().getDescription());
            particularsOfPolicy.setMarketingExecutive(tempPolicy.getIntermediaryCode());
            particularsOfPolicy.setChassisNo(policyRiskInformationService.getChassisNo(tempPolicyRisk));
            claimRegistrationSheetDTO.setParticularsOfPolicy(particularsOfPolicy);

            ParticularsOfIntimation particularsOfIntimation = new ParticularsOfIntimation();

            List<AssignLossAdjuster> assignLossAdjustersList = assignLossAdjusterRepository.findByAssignLossAdjusterPK_IntimationSequenceNo(claimIntimation.getSequenceNo());
            String name = null;

            if (assignLossAdjustersList == null || assignLossAdjustersList.isEmpty()) {
                name = null;
            } else {
                name = externalPersonService.getExternalPersonName(assignLossAdjustersList.get(0).getExternalPersonCode());
            }

            particularsOfIntimation.setLossAdjusterName(name);
            particularsOfIntimation.setDriverContactNo(formatter.replaceWithDash(claimIntimation.getDriverContactNo()));
            particularsOfIntimation.setInsuredContactNo(formatter.replaceWithDash(claimIntimation.getContactNo()));
            particularsOfIntimation.setApproximateCostOfLoss(formatter.format(claimIntimation.getEstimatedAmount()));
            particularsOfIntimation.setInsuredAtFault(formatter.replaceWithDash(claimIntimation.getInsuredAtFault().toString()));
            particularsOfIntimation.setPoliceStation(formatter.replaceWithDash(claimIntimation.getPoliceStation()));
            particularsOfIntimation.setCauseOfLoss(formatter.replaceWithDash(claimIntimation.getCauseOfLoss()));
            particularsOfIntimation.setModeOfInformation(formatter.replaceWithDash(claimIntimation.getModeOfInformation()));
            particularsOfIntimation.setClaimDoubtful(formatter.replaceWithDash(claimIntimation.getClaimDoubtful().toString()));
            particularsOfIntimation.setClaimType(null);
            claimRegistrationSheetDTO.setParticularsOfIntimation(particularsOfIntimation);

            List<ClaimIntimation> claimIntimationList = claimIntimationRepository.findByPolicySequenceNoAndRiskSequenceNo(tempPolicy.getPolicySequenceNo(), claimIntimation.getRiskSequenceNo());
            List<ClaimHistory> claimHistoryList = new ArrayList<>();
            ClaimHistory claimHistory = null;
            for (ClaimIntimation claim : claimIntimationList) {
                claimHistory = new ClaimHistory();
                claimHistory.setClaimNo(claim.getClaimNo());
                if (claim.getClaimedAmount() == null) {
                    claim.setClaimedAmount(0.0);
                }
                claimHistory.setPaidAmount(claim.getClaimedAmount());
                claimHistory.setDateOfAccident(dateConversion.convertFromDateToString(claim.getLossDate(), "dd-MMM-yyyy"));
                claimHistoryList.add(claimHistory);
            }
            claimRegistrationSheetDTO.setClaimHistory(claimHistoryList);
        }
        return claimRegistrationSheetDTO;
    }

    public List<JobAssignmentReportDTO> generateJobAssignmentReportDTO(JobAssignmentReportRequestDTO requestDTO) throws ParseException {

        DateFormat df1 = new SimpleDateFormat("dd-MMM-yyyy"); // for parsing input
        DateFormat df2 = new SimpleDateFormat("yyyy/MMM/dd");  // for formatting output

        String inputFromDate = requestDTO.getFromDate();
        String inputToDate = requestDTO.getToDate();

        Date date1 = df1.parse(inputFromDate);
        Date date2 = df1.parse(inputToDate);

        String formDate = df2.format(date1);
        String toDate = df2.format(date2);


        String statement = "SELECT A.AAD_JOB_NO, E.SUR_DESC," +
                " NVL(TO_CHAR(A.CREATED_DATE,'YYYY/MON/DD HH24:MI:SS'),'-')," +
                " C.INT_PRS_NAME, D.EXP_INITIAL || ' ' || D.EXP_SURNAME AS ASSESSOR_NAME," +
                " C.INT_CLAIM_NO, " +
                " DECODE(C.INT_STATUS,'C','CANCELLED','N','NOT CLAIMED','R','REJECTED','L','CLOSED','P','OPEN','S','SETTLED'," +
                " 'O','OUTSTANDING') CLAIM_STATUS," +
                "NVL(TO_CHAR(A.CREATED_DATE,'YYYY/MON/DD HH24:MI:SS'),'-') AS START_TIME," +
                " NVL(TO_CHAR(B.AAS_SUBMIT_DT,'YYYY/MON/DD HH24:MI:SS'),'-') AS END_TIME" +
                " FROM CL_T_REP_MASTER A, CL_T_ASSIGN_LOSS B, CL_T_INTIMATION C, CL_M_EXTERNAL_PER D, CL_R_SURVEY E" +
                " WHERE " +
                " A.AAD_INT_SEQ = C.INT_SEQ_NO" +
                " AND A.AAD_SEQ_NO = B.AAS_REP_SEQ " +
                " AND A.AAD_SURVEY_CODE = E.SUR_CODE" +
                " AND B.AAS_CODE = D.EXP_CODE" +
                " AND NVL(UPPER(REPLACE(C.INT_PRS_NAME, CHR(32), '')),'A')  LIKE " +
                "DECODE(UPPER(?),NULL, NVL(UPPER(REPLACE(C.INT_PRS_NAME, CHR(32), '')),'A'), '%' || UPPER(?) || '%')" +
                " AND A.CREATED_DATE >= TO_DATE( TO_CHAR(?) ,'YYYY/MON/DD HH24:MI:SS')" +
                " AND A.CREATED_DATE <= TO_DATE( TO_CHAR(?) ,'YYYY/MON/DD HH24:MI:SS')" +
                " AND NVL(UPPER(D.EXP_INITIAL || ' ' || D.EXP_SURNAME),'A') " +
                "LIKE DECODE(UPPER(?), NULL, NVL(UPPER(D.EXP_INITIAL || ' ' || D.EXP_SURNAME),'A'), '%' || UPPER(?) || '%')" +
                "ORDER BY D.EXP_INITIAL || ' ' || D.EXP_SURNAME, C.INT_PRS_NAME, C.INT_CLAIM_NO ";


        Query query = entityManager.createNativeQuery(statement)
                .setParameter(1, requestDTO.getVehicleNo())
                .setParameter(2, requestDTO.getVehicleNo())
                .setParameter(3, formDate)
                .setParameter(4, toDate)
                .setParameter(5, requestDTO.getAssessorName())
                .setParameter(6, requestDTO.getAssessorName());


        List<Object[]> list = (List<Object[]>) query.getResultList();

        List<JobAssignmentReportDTO> jobAssignmentReportDTOList = new ArrayList<>();
        JobAssignmentReportDTO jobAssignmentReportDTO;

        for (Object[] jobDetailsList : list) {

            jobAssignmentReportDTO = new JobAssignmentReportDTO();

            int i = 0;

            for (Object value : jobDetailsList) {

                switch (i) {

                    case 0:
                        jobAssignmentReportDTO.setJobNo((String) value);
                        break;

                    case 1:
                        jobAssignmentReportDTO.setJobType((String) value);
                        break;

                    case 2:
                        jobAssignmentReportDTO.setAppointedDate((String) value);
                        break;

                    case 3:
                        jobAssignmentReportDTO.setRiskName((String) value);
                        break;

                    case 4:
                        jobAssignmentReportDTO.setAssessorName((String) value);
                        break;

                    case 5:
                        jobAssignmentReportDTO.setClaimNo((String) value);
                        break;

                    case 6:
                        jobAssignmentReportDTO.setClaimStatus((String) value);
                        break;

                    case 7:
                        jobAssignmentReportDTO.setStartTime((String) value);
                        break;

                    case 8:
                        jobAssignmentReportDTO.setEndTime((String) value);
                        break;
                }

                i += 1;
            }

            jobAssignmentReportDTOList.add(jobAssignmentReportDTO);

        }


        return jobAssignmentReportDTOList;
    }

    public ArrayList<ClaimRegistrationReportDTO> getClaimRegistrationReportDetails(ClaimRegistrationRequestDTO claimRegistrationRequestDTO) {

        ArrayList<ClaimRegistrationReportDTO> claimRegistrationReportDTOList = new ArrayList<>();

        if (claimRegistrationRequestDTO == null || claimRegistrationRequestDTO.getFromDate() == null || claimRegistrationRequestDTO.getToDate() == null || claimRegistrationRequestDTO.getCurrency() == null) {
            return claimRegistrationReportDTOList;
        } else {

            Date fromDate = dateConversion.convertStringToDate(claimRegistrationRequestDTO.getFromDate(), "dd-MMM-yyyy");
            Date toDate = dateConversion.convertStringToDate(claimRegistrationRequestDTO.getToDate(), "dd-MMM-yyyy");
            String currency = claimRegistrationRequestDTO.getCurrency();
            Character status = claimRegistrationRequestDTO.getStatus() == null || claimRegistrationRequestDTO.getStatus().isEmpty() ? null : claimRegistrationRequestDTO.getStatus().charAt(0);
            String branch = claimRegistrationRequestDTO.getBranch() == null || claimRegistrationRequestDTO.getBranch().isEmpty() ? "" : claimRegistrationRequestDTO.getBranch();
            String cls = claimRegistrationRequestDTO.getCls() == null || claimRegistrationRequestDTO.getCls().isEmpty() ? "" : claimRegistrationRequestDTO.getCls();
            String product = claimRegistrationRequestDTO.getProduct() == null || claimRegistrationRequestDTO.getProduct().isEmpty() ? "" : claimRegistrationRequestDTO.getProduct();

            ClaimRegistrationReportDTO claimRegistrationReportDTO = null;
            PayDetails payDetails = null;
            ArrayList<PayDetails> payDetailsList = null;
            ArrayList<ClaimIntimation> claimIntimationList = claimIntimationRepository.findByIntimationDateBetweenAndCurrencyAndBranchCodeContainingAndClassCodeContainingAndProductCodeContainingOrderByBranchCodeAscClaimStatusAscCurrencyAscPolicyNoAsc(fromDate, toDate, currency, branch, cls, product);

            if (status != null) {
                for (ClaimIntimation claimIntimation : claimIntimationList) {
                    if (claimIntimation.getClaimStatus().toString().equalsIgnoreCase(status.toString())) {
                        claimIntimationList.remove(claimIntimation);
                    }
                }
            }
            payDetailsList = new ArrayList<>();
            for (ClaimIntimation claimIntimation : claimIntimationList) { // 1329

                if (claimIntimation != null) {

                    ArrayList<RequisitionPayee> requisitionPayeeList = requisitionPayeeRepository.findByRequisitionPayeePK_IntimationSequenceNoOrderByChequeFav(claimIntimation.getSequenceNo());
                    for (RequisitionPayee requisitionPayee : requisitionPayeeList) {
                        payDetails = new PayDetails();
                        payDetails.setPayee(requisitionPayee.getChequeFav());
                        payDetails.setPaidAmount(requisitionPayee.getAmount());
                        payDetails.setPaidDate(requisitionPayee.getEffectDdate());
                        payDetails.setChequeNo(requisitionPayee.getChequeNo());
                        payDetailsList.add(payDetails);
                    }

                    ArrayList<OtherChargesPayment> otherChargesPaymentList = otherChargePaymentsRepository.findByIntimationSequenceOrderByPayeeName(claimIntimation.getSequenceNo());
                    for (OtherChargesPayment otherChargesPayment : otherChargesPaymentList) {
                        payDetails = new PayDetails();
                        payDetails.setPayee(otherChargesPayment.getPayeeName());
                        payDetails.setPaidAmount(otherChargesPayment.getPayAmount());
                        payDetails.setPaidDate(otherChargesPayment.getCreatedDate());
                        payDetails.setChequeNo(otherChargesPayment.getChequeNo());
                        payDetailsList.add(payDetails);
                    }

                    if (payDetailsList.isEmpty()) {
                        continue;
                    } else {
                        int chequeId = 0;
                        for (PayDetails payDetail : payDetailsList) {
                            claimRegistrationReportDTO = new ClaimRegistrationReportDTO();
                            chequeId++;
                            claimRegistrationReportDTO.setBranch(claimIntimation.getPolicyBranch().getDescription());
                            claimRegistrationReportDTO.setPeriodFrom(dateConversion.convertFromDateToString(claimIntimation.getPeriodFrom(), "dd-MMM-yyyy"));
                            claimRegistrationReportDTO.setPeriodTo(dateConversion.convertFromDateToString(claimIntimation.getPeriodTo(), "dd-MMM-yyyy"));
                            claimRegistrationReportDTO.setCustomer(customerService.getCustomerName(claimIntimation.getCustomer()));
                            claimRegistrationReportDTO.setPolicyNo(claimIntimation.getPolicyNo());
                            claimRegistrationReportDTO.setIntSequence(claimIntimation.getSequenceNo());
                            claimRegistrationReportDTO.setCls(claimIntimation.getRefClass().getDescription());
                            claimRegistrationReportDTO.setProduct(claimIntimation.getProduct().getDescription());
                            claimRegistrationReportDTO.setSumInsured(claimIntimation.getSumInsured());
                            claimRegistrationReportDTO.setClaimNo(claimIntimation.getClaimNo());
                            claimRegistrationReportDTO.setLossDate(dateConversion.convertFromDateToString(claimIntimation.getLossDate(), "dd-MMM-yyyy"));
                            claimRegistrationReportDTO.setIntimationDate(dateConversion.convertFromDateToString(claimIntimation.getIntimationDate(), "dd-MMM-yyyy"));
                            claimRegistrationReportDTO.setStatus(referenceService.getClaimStatus(claimIntimation.getClaimStatus()));
                            claimRegistrationReportDTO.setCurrency(claimIntimation.getCurrency());
//                            claimRegistrationReportDTO.setPayDetailsList(payDetailsList);
                            claimRegistrationReportDTO.setComment(claimIntimation.getComments());
                            claimRegistrationReportDTO.setPolicyBranch(claimIntimation.getPolicyBranch().getDescription());

                            //Double estAamount = pk_cl_t_intimation.getEstAmt(claimIntimation.getSequenceNo());
                            Double estimatedAmount = claimProvisionService.getEstimatedAmount(claimIntimation.getSequenceNo());
                            claimRegistrationReportDTO.setEstAmount(estimatedAmount);

                            claimRegistrationReportDTO.setChequeId(chequeId);
                            claimRegistrationReportDTO.setChequeNo(payDetail.getChequeNo());
                            claimRegistrationReportDTO.setPayee(payDetail.getPayee());
                            claimRegistrationReportDTO.setPaidAmount(payDetail.getPaidAmount());
                            claimRegistrationReportDTO.setPaidDate(dateConversion.convertFromDateToString(payDetail.getPaidDate(), "dd-MMM-yyyy"));
                            claimRegistrationReportDTOList.add(claimRegistrationReportDTO);
                        }
//                            wrapperService.deleteDataFromTempPolicyTables();
//                            TempPolicy tempPolicy = wrapperService.buildPolicy(claimIntimation.getPolicyNo(), claimIntimation.getLossDate());
//                            TempPolicyRisk tempPolicyRisk = tempPolicyRiskRepository.findByTempPolicyRiskPK_PolicySequenceNoAndName(tempPolicy.getSequenceNo(), claimIntimation.getRiskName());


                        payDetailsList.clear();

                    }
                }
            }
            return claimRegistrationReportDTOList;
        }
    }

    public Response saveLogo(Logo logoDto) {
        Response response = null;
        Logo logo = new Logo();
        logo.setCmgActive(logoDto.getCmgActive());
        logo.setCmgLogo(logoDto.getCmgLogo());
        logo.setCmgRepType(logoDto.getCmgRepType());
        logo.setCmgSeqNo(logoDto.getCmgSeqNo());
        logoRepository.save(logo);
        response = new Response(200, true, "Logo saved");
        return response;
    }

    public Logo getLogo() {

        Logo logo = new Logo();

        logo = logoRepository.findByCmgActiveAndCmgRepType("Y", "H");

        return logo;
    }


    public Response getPaymentDetailsList(PaymentRequestDTO paymentRequestDTO) {

//        Response response;

        ArrayList<ProvisionPaymentDetails> provisionPaymentDetailsArrayList;
        ArrayList<ClaimIntimation> claimIntimationArrayList = new ArrayList<>();
        ArrayList<PaymentResponseDTO> paymentResponseDTOArrayList = new ArrayList<>();

        if (paymentRequestDTO == null || paymentRequestDTO.getFromDate() == null || paymentRequestDTO.getToDate() == null || paymentRequestDTO.getCurrency() == null) {
            response = new Response(3, Boolean.FALSE, "fail", "");

        } else {

            Date fromDate = dateConversion.convertStringToDate(paymentRequestDTO.getFromDate(), "dd-MMM-yyyy");
            Date toDate = dateConversion.convertStringToDate(paymentRequestDTO.getToDate(), "dd-MMM-yyyy");
            String currency = paymentRequestDTO.getCurrency();
//            Character status = claimRegistrationRequestDTO.getIsActive() == null || claimRegistrationRequestDTO.getIsActive().isEmpty() ? null : claimRegistrationRequestDTO.getIsActive().charAt(0);
            String branch = paymentRequestDTO.getBranch() == null || paymentRequestDTO.getBranch().isEmpty() ? "" : paymentRequestDTO.getBranch();
            String cls = paymentRequestDTO.getCls() == null || paymentRequestDTO.getCls().isEmpty() ? "" : paymentRequestDTO.getCls();
            String productCode = paymentRequestDTO.getProduct() == null || paymentRequestDTO.getProduct().isEmpty() ? "" : paymentRequestDTO.getProduct();

            String intermediary = paymentRequestDTO.getIntermediary() == null || paymentRequestDTO.getProduct().isEmpty() ? "" : paymentRequestDTO.getProduct();

            ClaimRegistrationReportDTO claimRegistrationReportDTO = null;
            PayDetails payDetails = null;
            ArrayList<PayDetails> payDetailsList = null;
//                provisionPaymentDetailsArrayList = provisionPaymentDetailsRepository.findByClaimIntimation_IntimationDateBetweenAndClaimIntimation_CurrencyAndClaimIntimation_BranchCodeContainingAndClaimIntimation_ClassCodeContainingAndClaimIntimation_ProductCodeContainingClaimIntimation_businessPartyTypeCodeContainingOrderByClaimIntimation_BranchCodeAscClaimIntimation_ClaimStatusAscClaimIntimation_CurrencyAscClaimIntimation_PolicyNoAscfindByClaimIntimation_IntimationDateBetweenAndClaimIntimation_CurrencyAndClaimIntimation_BranchCodeContainingAndClaimIntimation_ClassCodeContainingAndClaimIntimation_ProductCodeContainingClaimIntimation_businessPartyTypeCodeContainingOrderByClaimIntimation_BranchCodeAscClaimIntimation_ClaimStatusAscClaimIntimation_CurrencyAscClaimIntimation_PolicyNoAsc(fromDate, toDate, currency, branch, cls, product, intermediary);
            provisionPaymentDetailsArrayList = provisionPaymentDetailsRepository.findByPaymentCreatedDateBetweenAndClaimIntimation_CurrencyAndClaimIntimation_BranchCodeContainingAndClaimIntimation_ClassCodeContainingAndClaimIntimation_ProductCodeContainingAndClaimIntimation_businessPartyTypeCodeContainingOrderByClaimIntimation_BranchCodeAscClaimIntimation_ClaimStatusAscClaimIntimation_CurrencyAscClaimIntimation_PolicyNoAsc(fromDate, toDate, currency, branch, cls, productCode, intermediary);

            for (int i = 0; i < provisionPaymentDetailsArrayList.size(); i++) {
                PaymentResponseDTO paymentResponseDTO = new PaymentResponseDTO();

                if (provisionPaymentDetailsArrayList.get(i).getClaimIntimation() != null) {
                    // this code get the Intermediary code.
                    SystemUser systemUser = systemUserRepository.findByCode(provisionPaymentDetailsArrayList.get(i).getClaimIntimation().getMarketingExecutiveCode());

                    String initials = systemUser.getInitials() == null || systemUser.getInitials().isEmpty() ? "" : systemUser.getInitials() + " ";
                    String firstName = systemUser.getFirstName() == null || systemUser.getFirstName().isEmpty() ? "" : systemUser.getFirstName() + " ";
                    String surname = systemUser.getSurname() == null || systemUser.getSurname().isEmpty() ? "" : systemUser.getSurname();


                    paymentResponseDTO.setIntermediary(initials + firstName + surname);
//                         paymentResponseDTO.setIntermediary( systemUser.getInitials() == null ||systemUser.getInitials().isEmpty() ? "" : systemUser.getInitials() + " "+systemUser.getFirstName() == null ||systemUser.getFirstName().isEmpty() ? "" : systemUser.getFirstName()+ systemUser.getFirstName() +" "+ systemUser.getSurname());

                }


                paymentResponseDTO.setBranch(provisionPaymentDetailsArrayList.get(i).getClaimIntimation().getBranchCode());
                paymentResponseDTO.setCls(provisionPaymentDetailsArrayList.get(i).getClaimIntimation().getRefClass().getDescription());
                paymentResponseDTO.setProduct(provisionPaymentDetailsArrayList.get(i).getClaimIntimation().getProduct().getDescription());
                paymentResponseDTO.setCurrency(provisionPaymentDetailsArrayList.get(i).getClaimIntimation().getCurrency());

                Optional<RequisitionPayee> requisitionPayeeOptional = requisitionPayeeRepository.findByRequisitionPayeePK_SequenceNo(provisionPaymentDetailsArrayList.get(i).getRequisitionSequenceNo());


                if (requisitionPayeeOptional.isPresent()) {
                    Optional<Requisition> requisitionOptional = requisitionRepository.findByRequisitionPK_SequenceNo(requisitionPayeeOptional.get().getRequisitionPayeePK().getRequisitionNo());
                    requisitionOptional.ifPresent(requisition -> paymentResponseDTO.setRequisitionNo(requisition.getRequisitioNo()));
                    paymentResponseDTO.setChequeInFavour(requisitionPayeeOptional.get().getChequeFav());
                }

                Optional<OtherChargesPayment> otherChargePaymentsOptional = otherChargePaymentsRepository.findBySequence(provisionPaymentDetailsArrayList.get(i).getRequisitionSequenceNo());

                if (otherChargePaymentsOptional.isPresent()) {
                    Optional<Requisition> requisition = requisitionRepository.findByRequisitionPK_SequenceNo(otherChargePaymentsOptional.get().getReqSeqNo());
                    requisition.ifPresent(requisitionObj -> paymentResponseDTO.setRequisitionNo(requisitionObj.getRequisitioNo()));
                    paymentResponseDTO.setChequeInFavour(otherChargePaymentsOptional.get().getPayeeName());
                }

                Optional<RequisitionPayeeHistory> requisitionPayeeHisOptional = requisitionPayeeHistoryRepository.findByReqNo(provisionPaymentDetailsArrayList.get(i).getRequisitionSequenceNo());

                if (requisitionPayeeHisOptional.isPresent()) {
                    Optional<RequisitionHistory> requisitionHis = requisitionHistoryRepository.findBySequenceNo(requisitionPayeeHisOptional.get().getReqNo());
                    requisitionHis.ifPresent(requisitionHistoryObj -> paymentResponseDTO.setRequisitionNo(requisitionHistoryObj.getRequisitioNo()));
                    paymentResponseDTO.setChequeInFavour(requisitionPayeeHisOptional.get().getChequeFav());
                }


                Optional<OtherChargesPaymentHistory> otherChargesPaymentHisOptional = otherChargesPaymentHistoryRepository.findByReqSequenceNo(provisionPaymentDetailsArrayList.get(i).getRequisitionSequenceNo());

                if (otherChargesPaymentHisOptional.isPresent()) {
                    Optional<RequisitionHistory> requisitionHis2 = requisitionHistoryRepository.findBySequenceNo(otherChargesPaymentHisOptional.get().getReqSequenceNo());
                    requisitionHis2.ifPresent(requisitionHistoryObj2 -> paymentResponseDTO.setRequisitionNo(requisitionHistoryObj2.getRequisitioNo()));
                    paymentResponseDTO.setChequeInFavour(otherChargesPaymentHisOptional.get().getPayeeName());
                }

                paymentResponseDTO.setCancelledDate(dateConversion.convertFromDateToString(provisionPaymentDetailsArrayList.get(i).getClaimIntimation().getCreatedDate(), "dd-MMM-yyyy"));
                paymentResponseDTO.setClaimNo(provisionPaymentDetailsArrayList.get(i).getClaimIntimation().getClaimNo());
                paymentResponseDTO.setAmount(provisionPaymentDetailsArrayList.get(i).getValue());
                paymentResponseDTOArrayList.add(paymentResponseDTO);
            }
            response = new Response(1, Boolean.TRUE, "success", paymentResponseDTOArrayList);
        }
        return response;
    }

}
