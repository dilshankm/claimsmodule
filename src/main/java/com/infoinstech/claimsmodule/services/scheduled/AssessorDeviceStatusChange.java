package com.infoinstech.claimsmodule.services.scheduled;

import com.infoinstech.claimsmodule.domain.model.claims.master.ExternalPersonDevice;
import com.infoinstech.claimsmodule.domain.model.claims.transaction.DeviceStatusChangeJobLog;
import com.infoinstech.claimsmodule.domain.repository.claims.master.ExternalPersonDeviceRepository;
import com.infoinstech.claimsmodule.domain.repository.claims.transaction.DeviceStatusChangeJobLogRepository;
import com.infoinstech.claimsmodule.services.common.ClaimsCommonService;
import com.infoinstech.claimsmodule.services.parameters.ClaimParametersService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import java.util.Date;
import java.util.List;

@Component
public class AssessorDeviceStatusChange {

    @Autowired
    private DeviceStatusChangeJobLogRepository deviceStatusChangeJobLogRepository;

    @Autowired
    private ExternalPersonDeviceRepository externalPersonDeviceRepository;
    @Autowired
    private ClaimParametersService claimParametersService;
    @Autowired
    private ClaimsCommonService claimsCommonService;

    @Scheduled(fixedRate = 3 * 60 * 1000)
    public void updateAssessorsStatus(){

        List<ExternalPersonDevice> externalPersonDevices = externalPersonDeviceRepository.findAll();

        for (ExternalPersonDevice externalPersonDevice : externalPersonDevices){

            long idleTime = (long) claimParametersService.getAssessorIdleTime();
            long inactiveTime = (long) claimParametersService.getAssessorInactiveTime();



            if(externalPersonDevice.getLastUpdatedTime()!=null){

                long lastUpdatedTime = externalPersonDevice.getLastUpdatedTime().getTime();
                long currentTime =  new Date().getTime();

                long timeGapInMins = (currentTime - lastUpdatedTime) /(1000 * 60) ;

                if(timeGapInMins >= inactiveTime){

                    externalPersonDevice.setStatus('N');
                }
                else if( timeGapInMins >= idleTime){

                    externalPersonDevice.setStatus('I');
                }
                else {

                    externalPersonDevice.setStatus('A');
                }

                try {

                    externalPersonDeviceRepository.save(externalPersonDevice);
                }
                catch (Exception e){

                    e.printStackTrace();
                }
            }
        }

        DeviceStatusChangeJobLog deviceStatusChangeJobLog = new DeviceStatusChangeJobLog();
        deviceStatusChangeJobLog.setSequenceNo(claimsCommonService.generateSequence(deviceStatusChangeJobLog.SEQUENCE));
        deviceStatusChangeJobLog.setRunningTime(new Date());
        deviceStatusChangeJobLogRepository.save(deviceStatusChangeJobLog);
    }




}
