package com.infoinstech.claimsmodule.services.util;

import java.util.regex.Pattern;

public class DataValidator {
    public static boolean validatePhoneNumber(String phoneNumber) {
        System.out.println("Validating phone number : " + phoneNumber);
        String phoneNumberRegEx = "^[0-9]{12}$";
        Pattern phoneNumberPattern = Pattern.compile(phoneNumberRegEx);
        return phoneNumberPattern.matcher(phoneNumber).matches();
    }
}
