package com.infoinstech.claimsmodule.services.util;

import org.springframework.stereotype.Service;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;

@Service
public class DateConversion {

    // Convert the Date to a  given Date Format
    public Date convertDate(Date date) {

        DateFormat df = new SimpleDateFormat("yyyy-MMM-dd HH:mm:ss", Locale.ENGLISH);
        Date date1 = null;

        if (date == null) {
            date1 = null;
        } else {
            try {
                String formatedDate = df.format(date);
                date1 = df.parse(formatedDate);
                return date1;
            } catch (ParseException e) {
                e.printStackTrace();
                return null;
            }
        }
        return date1;
    }

    public Date convertStringToDate(String dateToBeConverted) {

        DateFormat df = new SimpleDateFormat("yyyy-MMM-dd HH:mm:ss", Locale.ENGLISH);
        Date date = null;

        if (dateToBeConverted != null) {

            try {

                date = df.parse(dateToBeConverted);
            } catch (Exception e) {

                e.printStackTrace();
            }

        }
        return date;
    }

    public Date convertStringToDate(String dateToBeConverted, String pattern) {

        DateFormat df = new SimpleDateFormat(pattern, Locale.ENGLISH);
        Date date = null;

        if (dateToBeConverted != null) {

            try {

                date = df.parse(dateToBeConverted);
            } catch (Exception e) {

                e.printStackTrace();
            }

        }
        return date;
    }

    // Get formatted (hh-mm-ss) Time Component of the given Date
    public String getTime(Date date) {

        date = new Date();

        DateFormat df = new SimpleDateFormat("hh-mm-ss", Locale.ENGLISH);
        String formatedDate = df.format(date);
        return formatedDate;

    }

    // Conversion of the given Date to a String in the given Date Format - Overloading methods
    public String convertFromDateToString(Date date) {

        DateFormat df = new SimpleDateFormat("yyyy-MMM-dd HH:mm:ss", Locale.ENGLISH);
        String formattedDate;

        if (date == null) {

            return "-";

        } else {
            try {
                formattedDate = df.format(date);

            } catch (Exception e) {
                e.printStackTrace();
                return null;
            }
        }
        return formattedDate;
    }

    public String convertFromDateToString(Date date, String pattern) {

        DateFormat df = new SimpleDateFormat(pattern, Locale.ENGLISH);
        String formattedDate;

        if (date == null) {

            return "";

        } else {
            try {
                formattedDate = df.format(date);

            } catch (Exception e) {
                e.printStackTrace();
                return null;
            }
        }
        return formattedDate;
    }

    public boolean checkSameDay(Date firstDate, Date secondDate) {

        Calendar cal1 = Calendar.getInstance();
        Calendar cal2 = Calendar.getInstance();
        cal1.setTime(firstDate);
        cal2.setTime(secondDate);
        boolean sameDay =
                cal1.get(Calendar.YEAR) == cal2.get(Calendar.YEAR) &&
                        cal1.get(Calendar.MONTH) == cal2.get(Calendar.MONTH) &&
                        cal1.get(Calendar.DAY_OF_YEAR) == cal2.get(Calendar.DAY_OF_YEAR);

        return sameDay;
    }

    public Date formatTimeToZero(Date date) {

        Calendar calendar = Calendar.getInstance();
        calendar.setTime(date);
        calendar.set(Calendar.HOUR, 0);
        calendar.set(Calendar.MINUTE, 0);
        calendar.set(Calendar.SECOND, 0);

        return calendar.getTime();
    }

    public Date addDays(Date date, int noOfDays) {

        Calendar calendar = Calendar.getInstance();
        calendar.setTime(date);
        calendar.add(Calendar.DATE, noOfDays);

        return calendar.getTime();
    }


}
