package com.infoinstech.claimsmodule.services.util;

import org.springframework.stereotype.Service;

import java.text.DecimalFormat;
import java.util.Date;

@Service
public class Formatter {

    public String format(Double amount) {

        if (amount != null) {

            DecimalFormat formatter = new DecimalFormat("#,###.00");

            return formatter.format(amount);
        } else {

            return "-";
        }
    }

    public String format(Double amount, String pattern) {

        if (amount != null) {

            DecimalFormat formatter = new DecimalFormat(pattern);

            return formatter.format(amount);
        } else {

            return "-";
        }
    }

    public String formatPercentages(Double percentage) {

        if (percentage != null) {

            DecimalFormat formatter = new DecimalFormat("#.##");

            return formatter.format(percentage) + "%";
        } else {

            return "-";
        }
    }

    public String replaceWithDash(String field) {

        if (field == null || field.equals("")) {
            return "-";
        } else return field;
    }

    public String replaceWithDash(Date date) {

        String convertedDate = "-";

        if (date != null) {

            convertedDate = date.toString();
        }

        return convertedDate;
    }


    public String replaceWithDash(Double number) {

        String convertedDate = "-";

        if (number != null) {

            convertedDate = number.toString();
        }

        return convertedDate;
    }

    public String leftPadding(String value, int paddingLength, String paddingValue) {

        String paddedValue = String.format("%" + paddingLength + "s", value);

        return paddedValue.replaceAll(" ", paddedValue);
    }

    public String leftPadding(int value, int paddingLength, String paddingValue) {

        String paddedValue = String.format("%" + paddingValue + paddingLength + "d", value);

        return paddedValue;

    }
}
