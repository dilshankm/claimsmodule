package com.infoinstech.claimsmodule.services.util;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Service;

@Service
public class Settings {

    @Autowired
    private Environment env;

    private static final String FIREBASE_SERVER_KEY = "AIzaSyCt9pbxtBvzYbaVSadEjSZiaJNmbRnlW1I";

    public void setProxy(){


        String build = env.getProperty("source.build","internal");
        String site = getSite();



        if(build.equals("production")){

            if(site.equals("AMI")){

                System.setProperty("https.proxyHost", "172.20.100.100");
                System.setProperty("https.proxyPort", "3128");
            }
        }

        else if(build.equalsIgnoreCase("local")){

            System.setProperty("https.proxyHost", "192.168.15.5");
            System.setProperty("https.proxyPort", "8080");
        }
    }

    public String getSite(){

        return env.getProperty("insurance.site","SANASA");

    }

    public String getFirebaseServerKey(){

        return FIREBASE_SERVER_KEY;
    }


}
