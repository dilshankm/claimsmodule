package com.infoinstech.claimsmodule.services.validation;

import com.infoinstech.claimsmodule.domain.model.receipting.transaction.DebitNote;
import com.infoinstech.claimsmodule.domain.repository.receipting.transaction.DebitNoteRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;

@Service
public class ClaimIntimationValidation {

    @Autowired
    private DebitNoteRepository debitNoteRepository;

    public boolean checkDebitOutstandingExists(String customerCode, String policyNo) {
        ArrayList<DebitNote> debitNoteList = debitNoteRepository.findByCustomerCodeAndPolicyNoAndStatusAndDebitSettled(
                customerCode, policyNo, 'A', 'N');
        return debitNoteList.isEmpty() ? false : true;
    }

}

