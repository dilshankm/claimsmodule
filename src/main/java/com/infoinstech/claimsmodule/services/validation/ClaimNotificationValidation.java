package com.infoinstech.claimsmodule.services.validation;

import com.infoinstech.claimsmodule.dto.claimnotification.CoverNoteValidationDTO;
import com.infoinstech.claimsmodule.dto.claimnotification.VehicleNoDTO;
import com.infoinstech.claimsmodule.dto.common.Response;
import com.infoinstech.claimsmodule.domain.model.claims.transaction.ClaimNotification;
import com.infoinstech.claimsmodule.domain.repository.claims.transaction.ClaimNotificationRepository;
import com.infoinstech.claimsmodule.exceptions.types.ClaimNotificationException;
import com.infoinstech.claimsmodule.services.parameters.UnderwritingParametersService;
import com.infoinstech.claimsmodule.services.util.Settings;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.List;
import java.util.regex.Pattern;

@Service
public class ClaimNotificationValidation {

    @Autowired
    private ClaimNotificationRepository claimNotificationRepository;

    @Autowired
    private UnderwritingParametersService underwritingParametersService;
    @Autowired
    private Settings settings;

    public Response validateCoverNoteNoAndLossDateForDuplication(CoverNoteValidationDTO coverNoteValidationDTO) {
        Response response;
        if (coverNoteValidationDTO.getCoverNoteNo() != null && coverNoteValidationDTO.getLossDate() != null && coverNoteValidationDTO.getEffectiveFromDate() != null) {
            if (coverNoteValidationDTO.getLossDate().getTime() > coverNoteValidationDTO.getEffectiveFromDate().getTime() &&
                    coverNoteValidationDTO.getLossDate().getTime() < new Date().getTime()) {
                List<ClaimNotification> claimNotifications = claimNotificationRepository.findByCoverNoteNoAndLossDate(coverNoteValidationDTO.getCoverNoteNo(), coverNoteValidationDTO.getLossDate());
                if (claimNotifications.size() == 0) {
                    response = new Response(200, true, "Valid Loss Date");
                } else {
                    throw new ClaimNotificationException("A Claim Notification has been issued on the given Loss Date for the Cover Note No");
                }
            } else {
                if (coverNoteValidationDTO.getLossDate().getTime() > new Date().getTime()) {
                    throw new ClaimNotificationException("Loss Date is greater than Current Date");
                } else {
                    throw new ClaimNotificationException("Loss Date is greater than Effective From Date");
                }
            }
        } else {
            throw new ClaimNotificationException("Cover Note No or Loss Date is missing");
        }
        return response;
    }

    public Response validateVehicleNo(VehicleNoDTO vehicleNoDTO) {

        Response response;

        if(!checkVehicleNoForAllowedCharacters(vehicleNoDTO.getVehicleNo())){

            response = new Response(400, false, "Only Alphanumeric, Space and Dash characters are allowed");

        }
        else if(settings.getSite().equals("SANASA")){

            if(checkVehicleNoFormat(vehicleNoDTO.getVehicleNo())){

                response = new Response(200, true, "Valid Vehicle No Format");
            }
            else {

                response = new Response(400, false, underwritingParametersService.getVehicleNoFormatValidationMessage());
            }
        }
        else {

            response = new Response(200, true, "Valid Vehicle No Format");
        }

        return response;
    }

    public boolean checkVehicleNoForAllowedCharacters(String vehicleNo){

        String trimmedVehicleNo = vehicleNo.replaceAll(" ","");

        return trimmedVehicleNo.matches(settings.getSite().equals("AMI") ? "[-/()[\\w]]+" : "[-[\\w]]+");

    }

    public boolean checkVehicleNoFormat(String vehicleNo) {

        if (vehicleNo.substring(0, 3).equals("U/R")) {

            return vehicleNo.charAt(3) == ' ' & Pattern.matches("\\d+", (vehicleNo.substring(4)));

        } else if (vehicleNo.length() == 11) {

            if (vehicleNo.charAt(2) == ' ' && vehicleNo.charAt(6) == ' '
                    && Pattern.matches("[A-Z]+", vehicleNo.substring(0, 2))
                    && Pattern.matches("[A-Z]+", (vehicleNo.substring(3, 6)))
                    && Pattern.matches("\\d+", (vehicleNo.substring(7)))) {

                return true;
            } else if (vehicleNo.charAt(2) == ' ' && vehicleNo.charAt(5) == ' ' && vehicleNo.charAt(6) == ' '
                    && Pattern.matches("[A-Z]+", vehicleNo.substring(0, 2))
                    && Pattern.matches("[A-Z]+", vehicleNo.substring(3, 5))
                    && Pattern.matches("\\d+", vehicleNo.substring(7))) {

                return true;
            } else {
                return false;
            }

        } else if (vehicleNo.length() == 8) {

            if (vehicleNo.charAt(3) == ' ' && Pattern.matches("\\d++", vehicleNo.substring(0, 3))
                    && Pattern.matches("\\d+", vehicleNo.substring(4))) {

                return true;
            } else if (vehicleNo.charAt(2) == ' ' && vehicleNo.charAt(3) == ' '
                    && Pattern.matches("\\d++", vehicleNo.substring(0, 2))
                    && Pattern.matches("\\d+", vehicleNo.substring(4))) {

                return true;
            } else if (vehicleNo.charAt(1) == ' ' && vehicleNo.charAt(2) == ' ' && vehicleNo.charAt(3) == ' '
                    && Pattern.matches("\\d++", vehicleNo.substring(0, 1))
                    && Pattern.matches("\\d+", vehicleNo.substring(4))) {

                return true;
            } else {
                return false;
            }

        } else {

            return false;
        }

    }
}
