package com.infoinstech.claimsmodule.urls;

public class ADMIN_CONFIGURATION {

		public static final String baseURL = "claims-module/api/admin-configuration/";

		public static final String systemUsers = baseURL+"view/system-users";

		public static final String userStatus = baseURL+"update/user-status";

}
