package com.infoinstech.claimsmodule.urls;

public class ASSIGN_LOSS_ADJUSTER {

    public static final String baseURL = "claims-module/api/assessor-charges/";

    public static final String ChargeTypes = baseURL + "lov/charge-types/";

    public static final String UnitTypes = baseURL + "lov/unit-types/";

    public static final String UploadAssessorCharges = baseURL + "upload/assessor-charges/";
}
