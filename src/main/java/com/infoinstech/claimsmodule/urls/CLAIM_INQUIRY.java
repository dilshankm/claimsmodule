package com.infoinstech.claimsmodule.urls;

public class CLAIM_INQUIRY {

    public static final String baseURL = "claims-module/api/claim-inquiry/";

    public static final String SearchClaimDetails = baseURL + "search/claim-details/";

    public static final String ViewJobAssignments = baseURL + "view/job-assignments/";

    public static final String ViewNotificationAssignments = baseURL + "view/notification-assignments/";

    public static final String ViewClaimIntimationDetails = baseURL + "view/claim-intimation-details/";

    public static final String ViewClaimNotificationDetails = baseURL + "view/claim-notification-details/";

}
