package com.infoinstech.claimsmodule.urls;


public class CLAIM_INTIMATION {

    public static final String baseURL = "claims-module/api/claim-intimation/";

    public static final String ViewClaimHistoryDetails = baseURL + "view/claim-history-details";

    public static final String ValidateLossDate = baseURL + "validation/loss-date/";

    public static final String ModesOfInformation = baseURL + "lov/modes-of-information/";

    public static final String LossCauses = baseURL + "lov/causes-of-loss/";

    public static final String SurveyTypes = baseURL + "lov/types-of-survey/";

    public static final String RelationshipTypes = baseURL + "lov/relationship-types/";

    public static final String ClaimHandlers = baseURL + "lov/claim-handlers/";

    public static final String ProvisionTypes = baseURL + "lov/provision-types/";

    public static final String PolicyPerils = baseURL + "lov/policy-perils/";

    public static final String ClaimNumbers = baseURL + "lov/claim-no/";

    public static final String PolicyNumbers = baseURL + "lov/policy-no/";

    public static final String RiskNames = baseURL + "lov/risk-name/";

    public static final String ClassNames = baseURL + "lov/classes/";

    public static final String ProductNames = baseURL + "lov/products/";

    public static final String CustomerNames = baseURL + "lov/customer-names/";

    public static final String ActionsTaken = baseURL + "lov/actions-taken/";

    public static final String Assessors = baseURL + "lov/assessors/";

    public static final String Branches = baseURL + "lov/branches/";

    public static final String Intermediaries = baseURL + "lov/intermediaries/";

    public static final String IntermediaryTypes = baseURL + "lov/intermediary-types/";

    public static final String SaveIntimation = baseURL + "create/intimation/";

    public static final String DebitOutstandingCheck = baseURL + "validation/debit-outstanding-check/";

    public static final String UploadImages = baseURL + "upload/images/";

    public static final String UploadImageByImage = baseURL + "upload/image-by-image/";

    public static final String ReadImages = baseURL + "read/images/";

    public static final String CreateJobAssignment = baseURL + "create/job-assignment/";

    public static final String ViewJobAssignmentDetails = baseURL + "view/job-assignment/";

    public static final String UpdateJobAssignmentStatus = baseURL + "update/job-assignment-status/";

    public static final String ViewQuestionnaire = baseURL + "view/questionnaire/";

    public static final String UploadQuestionnaire = baseURL + "upload/questionnaire/";

    public static final String UploadSketch = baseURL + "upload/sketch";

    public static final String UploadDamageSketch = baseURL + "upload/damage-sketch";

    public static final String UpdateDamageSketch = baseURL + "update/damage-sketch";

    public static final String ViewSketch = baseURL + "view/sketch";

    public static final String ViewDamageSketch = baseURL + "view/damage-sketch";

    public static final String ViewAllSketch = baseURL + "view/all-sketch";

    public static final String ConvertClaimNotification = baseURL + "convert/claim-notification/";

    public static final String ViewPlacesAutoComplete = baseURL + "google-maps-api/get/places-auto-complete/";

    public static final String GetPlaceDetails = baseURL + "google-maps-api/get/place-details/";

    public static final String NearByPoliceStations = baseURL + "google-maps-api/nearby-search/police-stations/";

    public static final String Events = baseURL + "lov/generated-events/";
}
