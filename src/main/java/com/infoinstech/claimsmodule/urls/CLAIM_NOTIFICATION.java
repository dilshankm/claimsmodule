package com.infoinstech.claimsmodule.urls;

public class CLAIM_NOTIFICATION {

    public static final String baseURL = "claims-module/api/claim-notification/";

    public static final String SaveNotification = baseURL + "create/notification/";

    public static final String ValidateVehicleNo = baseURL + "validation/vehicle-no-format/";

    public static final String ValidateCoverNoteNoAndLossDate = baseURL + "validation/loss-date/";

    public static final String CoverTypes = baseURL + "lov/cover-types/";

    public static final String VehicleMakeAndModels = baseURL + "lov/vehicle-make-models/";

    public static final String CreateNotificationAssignment = baseURL + "create/notification-assignment/";


}
