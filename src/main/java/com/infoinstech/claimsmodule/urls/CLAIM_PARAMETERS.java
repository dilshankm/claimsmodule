package com.infoinstech.claimsmodule.urls;

public class CLAIM_PARAMETERS {

    public static final String baseURL = "claims-module/api/claim-parameter/";

    public static final String ViewUploadGalleryImagesParamater = baseURL + "view/upload-gallery-images/";

}
