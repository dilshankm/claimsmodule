package com.infoinstech.claimsmodule.urls;

public class DEVICE_REGISTRATION {

    public static final String baseURL = "claims-module/api/external-persons-maintenance/";

    public static final String GenerateDeviceToken = baseURL + "generate/device-token/";

    public static final String RegisterDevice = baseURL + "registration/mobile-device/";

    public static final String RemoveDevice = baseURL + "de-registration/mobile-device/";

    public static final String RegisteredDevices = baseURL + "view/registered-assessors/";

    public static final String ActiveDevices = baseURL + "view/active-assessors/";

    public static final String CoreDevices = baseURL + "view/core-assessors/";

    public static final String UnRegisteredDevices = baseURL + "view/unregistered-assessors/";

    public static final String UpdateDeviceLocation = baseURL + "update/device-location/";

}
