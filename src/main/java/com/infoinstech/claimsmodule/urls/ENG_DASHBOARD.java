package com.infoinstech.claimsmodule.urls;

public class ENG_DASHBOARD {

    public static final String baseURL = "claims-module/api/dashboard/";

    public static final String ViewDashboardSummary = baseURL + "view/assignments-summary/";

    public static final String ViewActiveAccidents = baseURL + "view/active-accidents/";

}
