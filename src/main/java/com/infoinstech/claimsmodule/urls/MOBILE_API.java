package com.infoinstech.claimsmodule.urls;

public class MOBILE_API {

    public static final String baseURL = "claims-module/api/mobile-device/";

    public static final String ViewAutomotiveDetails = baseURL + "reference/automotive-details";

    public static final String GetVehicleSketches = baseURL + "reference/vehicle-sketches/";

    public static final String ViewVehicleSketch = baseURL + "view/vehicle-sketch/";

    public static final String ViewAssignmentSummary = baseURL + "view/assignments-summary";

    public static final String TestConnection = baseURL + "test/connection/";

}
