package com.infoinstech.claimsmodule.urls;

public class POLICY_INQUIRY {

    public static final String baseURL = "claims-module/api/policy-inquiry/";

    public static final String SearchPolicyDetails = baseURL + "search/policy-details/";

    public static final String ViewPolicyDetails = baseURL + "view/policy-details/";
}
