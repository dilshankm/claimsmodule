package com.infoinstech.claimsmodule.urls;

public class REPORT_GENERATION {

    public static final String baseURL = "claims-module/api/reports-generation/";

    public static final String IntimationReport = baseURL + "intimation-report/";

    public static final String PaymentDetailReport = baseURL + "payment-detail-report/";

    public static final String IASLReport = baseURL + "iasl-report/";

    public static final String ClaimRegistrationReport = baseURL + "claim-registration-report/";

    public static final String ClaimRegistrationSheet = baseURL + "claim-registration-sheet/";

    public static final String JobAssignmentDetailReport = baseURL + "job-assignment-detail-report/";

    public static final String IndividualClaimReportsSearch = baseURL + "search/individual-claims-report/";

    public static final String ClaimRatioReports = baseURL + "claimRatio/";


}
